
#include<iostream>

int main(){
	
	std::cout << "Start Main" << std::endl;

	try{
		throw "Kartik";
	}catch(std::string str){
		
		std::cout << "Exceptional Handling : "<< str << std::endl;
	}

	std::cout << "End Main" << std::endl;
}

/*
 	Start Main
	terminate called after throwing an instance of 'char const*'
	Aborted (core dumped)
*/
