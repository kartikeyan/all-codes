
//	We can catch any type of exception using eclipses

#include<iostream>

int main(){
	
	std::cout << "Start Main" << std::endl;
	
	try{
		
		throw 10;
	}catch(...){
		
		std::cout << "Exeption handling" << std::endl;
	}

	std::cout << "End Main" << std::endl;
}

/*
 	Start Main
	Exeption handling
	End Main	
*/

//	There is a special catch block called the ‘catch all’ block, written as catch(…), that can be used to catch all types of exceptions.
