
#include<iostream>

int main(){
	
	std::cout << "Start Main" << std::endl;

	try{
		throw 10;

	}catch(int x){
		
		std::cout << "Exceptional Handling" << std::endl;
	}

	std::cout << "End Main" << std::endl;
}

/*
 	Start Main
	Exceptional Handling
	End Main
*/
