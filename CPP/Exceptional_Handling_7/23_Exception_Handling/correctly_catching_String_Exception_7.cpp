
#include<iostream>

int main(){
	
	std::cout << "Start Main" << std::endl;

	try{
		throw "Kartik";
	}catch(const char *str){
		
		std::cout << "Exceptional Handling : "<< str << std::endl;
	}

	std::cout << "End Main" << std::endl;
}

/*
 	Start Main
	Exceptional Handling : Kartik
	End Main
*/
