
#include<iostream>

void fun(int x) throw (int, double){
	
	if(x == 1){
		
		throw 1;
	}else if(x == 2){
		throw 'A';
	}else if(x == 3){
		throw 7.5;
	}
}

int main(){
	
	int x;
	std::cin >> x;

	try{
		
		fun(x);
	}catch(int){
		std::cout << "int" << std::endl;
	}catch(double){
		std::cout << "double" << std::endl;
	}catch(char){
		std::cout << "char" << std::endl;
	}
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Exceptional_Handling_7/23_Exception_Handling$ g++ -std=c++03 deprecated_Feature.cpp 
	katikeyan@kartikeyan:~/all-codes/CPP/Exceptional_Handling_7/23_Exception_Handling$ ./a.out 
	2
	terminate called after throwing an instance of 'char'
	Aborted (core dumped)
*/
