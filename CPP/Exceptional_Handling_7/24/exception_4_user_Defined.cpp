
//	1] Copy Const Scenario
//	2] User Defined Exception

#include<iostream>

class InvalidIndex{
	
	std::string excep;

	public:
		InvalidIndex(std::string excep){
			
			this->excep = excep;
		}

		InvalidIndex(const InvalidIndex& ref){

                        std::cout << "Copy Const" << std::endl;
                }

                std::string getException(){

                        return excep;
                }
};

class Demo{
	
	int arr[5] = {1,2,3,4,5};	
	
	int arrLen(){
		
		return (sizeof(arr) / sizeof(arr[0]));
	}

	public:
		int operator[](int index){
			
			if(index < 0 || index >= arrLen()){

				throw InvalidIndex("Bad Index");
			}

			return arr[index];
		}
};

int main(){
	
	Demo obj;

	try{
		std::cout << obj[5] << std::endl;

	}catch(InvalidIndex obj){
		
		std::cout << "Exception Occured " << obj.getException() << std::endl;
	}
}

/*
 	Copy Const
	Exception Occured 
*/
