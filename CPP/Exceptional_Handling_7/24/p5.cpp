

#include<iostream>

class Demo{
	
	int arr[] = {1,2,3,4,5};	// ithe array cha size dyalya pahije
					// karan compile time la arr[] bagitlajato 

	public:
		int operator[](int index){
			
			return arr[index];
		}
};

int main(){
	
	Demo obj;

	std::cout << obj[3] << std::endl;
}

/*
 	p5.cpp:8:13: error: flexible array member ‘Demo::arr’ in an otherwise empty ‘class Demo’
	    8 |         int arr[] = {1,2,3,4,5};
	      |             ^~~
	p5.cpp:8:13: error: initializer for flexible array member ‘int Demo::arr []’
*/
