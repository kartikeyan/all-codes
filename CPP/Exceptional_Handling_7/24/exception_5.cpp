
#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}
};

int main(){
	
	Child obj;

	try{
		throw Child();

	}catch(Parent& obj){
		
		std::cout << "Parent Catch" << std::endl;

	}catch(Child& obj){

		std::cout << "Child Catch" << std::endl;
	}
}

/*
 	exception_5.cpp: In function ‘int main()’:
	exception_5.cpp:33:10: warning: exception of type ‘Child’ will be caught by earlier handler [-Wexceptions]
	   33 |         }catch(Child& obj){
	      |          ^~~~~
	exception_5.cpp:29:10: note: for type ‘Parent’
	   29 |         }catch(Parent& obj){
	      |          ^~~~~
*/

/*
 	Parent Const
	Child Const
	Parent Const
	Child Const
	Parent Catch
*/
