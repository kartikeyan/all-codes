
//	1] Copy Const Scenario
//	2] User Defined Exception

#include<iostream>
#include<exception>

class InvalidIndex : public std::runtime_error{

	public:
		InvalidIndex(std::string excep) :std::runtime_error{excep}{
			
			
		}
};

class Demo{
	
	int arr[5] = {1,2,3,4,5};	
	
	int arrLen(){
		
		return (sizeof(arr) / sizeof(arr[0]));
	}

	public:
		int operator[](int index){
			
			if(index < 0 || index >= arrLen()){

				throw InvalidIndex("Bad Index");
			}

			return arr[index];
		}
};

int main(){
	
	Demo obj;

	try{
		std::cout << obj[5] << std::endl;

	}catch(InvalidIndex obj){
		
		std::cout << "Exception Occured " << obj.what() << std::endl;
	}
}

/*
 	Exception Occured Bad Index 
*/
