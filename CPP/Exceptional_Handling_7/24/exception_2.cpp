
#include<iostream>

class Demo{
	
	int arr[5] = {1,2,3,4,5};	
	
	int arrLen(){
		
		return (sizeof(arr) / sizeof(arr[0]));
	}

	public:
		int operator[](int index){
			
			if(index < 0 || index >= arrLen()){

				throw "Bad Index";
			}

			return arr[index];
		}
};

int main(){
	
	Demo obj;

	try{
		std::cout << obj[5] << std::endl;

	}catch(std::string s){
		
		std::cout << "Exception Occured " << s << std::endl;
	}
}

/*
 	terminate called after throwing an instance of 'char const*'
	Aborted (core dumped)
*/
