
#include<iostream>

class Demo{
	
	int arr[5] = {1,2,3,4,5};	
	
	int arrLen(){
		
		return (sizeof(arr) / sizeof(arr[0]));
	}

	public:
		int operator[](int index){
			
			if(index < 0 || index >= arrLen()){

				throw index;
			}

			return arr[index];
		}
};

int main(){
	
	Demo obj;

	try{
		std::cout << obj[5] << std::endl;

	}catch(int x){
		
		std::cout << "Exception Occured " << x << std::endl;
	}
}

//	Exception Occured 5
