
#include<iostream>

template<typename T>
T min(T x, T y){
	
	return (x < y) ? x : y;
}

int main(){
	
	std::cout << min(10, 20) << std::endl;

	std::cout << min('A', 'B') << std::endl;

	std::cout << min(20.5f, 30.5f) << std::endl;

	std::cout << min(10.5, 20.5) << std::endl;
}

/*
 	10
	A
	20.5
	10.5
*/

/*
 	Aplyala vatat asel ki ithe 1 function ahet pan ithe 4 ahet karan 
	compiler swatah add karo
*/
