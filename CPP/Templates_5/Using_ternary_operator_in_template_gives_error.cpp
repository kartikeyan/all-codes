
//	That is why In Templates ternary operator is used

#include<iostream>

template <typename T, typename U>
auto min(T x, U y){
	
	
	if(x < y){
		
		return x;
	}else
		return y;
	

	//return (x < y) ? x : y;
}

int main(){
	
	std::cout << min(15, 7.5f) << std::endl;	
	std::cout << min('A', 100) << std::endl;	
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Templates_5$ g++ Using_ternary_operator_in_template_gives_error.cpp 
	Using_ternary_operator_in_template_gives_error.cpp: In instantiation of ‘auto min(T, U) [with T = int; U = float]’:
	Using_ternary_operator_in_template_gives_error.cpp:22:18:   required from here
	Using_ternary_operator_in_template_gives_error.cpp:14:24: error: inconsistent deduction for auto return type: ‘int’ and then ‘float’
	   14 |                 return y;
	      |                        ^
	Using_ternary_operator_in_template_gives_error.cpp: In instantiation of ‘auto min(T, U) [with T = char; U = int]’:
	Using_ternary_operator_in_template_gives_error.cpp:23:18:   required from here
	Using_ternary_operator_in_template_gives_error.cpp:14:24: error: inconsistent deduction for auto return type: ‘char’ and then ‘int’

*/

