
#include<iostream>

class AddDemo{

	public:
		int add(int x, int y){
			
			return x + y;
		}

		float add(float x, float y){
			return x + y;
		}
};

int main(){
	
	AddDemo obj;

	std::cout << obj.add(10, 20) << std::endl;

	std::cout << obj.add(10.5f, 20.5f) << std::endl;
}

/*
 	30
	31
*/
