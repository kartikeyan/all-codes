
#include<iostream>

template<typename T>

struct Node{
	
	T data;

	Node *next;
};

template<class T>
class LinkedList{
	
	Node<T> *head = NULL;

	public:
		Node<T>* createNode(T data){
			
			Node<T> *newNode = (Node<T> *)malloc(sizeof(Node<T>));

			newNode->data = data;

			newNode->next = NULL;

			return newNode;
		}

		void addLast(T data){
			
			Node<T> *temp = createNode(data);

			if(head == NULL){
				
				head = temp;
			}else{
				Node<T> *var = head;

				while(var->next != NULL){
					
					var = var->next;
				}

				var->next = temp;
			}

		}

		void printData(){
			
			Node<T> *temp = head;

			while(temp != NULL){
				
				std::cout << temp->data << std::endl;

				temp = temp->next;
			}
		}
};

int main(){
	
	LinkedList<int> obj;

	obj.addLast(10);
	obj.addLast(20);
	obj.addLast(30);
	
	obj.printData();
}

//	10 20 30
