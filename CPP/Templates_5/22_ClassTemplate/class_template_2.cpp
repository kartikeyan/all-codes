#include<iostream>

template<class T>
class Template{
	
	T data;

	public:
		Template(){
			
			std::cout << "In Template Const" << std::endl;
		}
};

//	Compiler generated code
template<>
class Template<int>{

	int data;

	public:
		Template(){
			
			std::cout << "Int Template Const" << std::endl;
		}
};


template<>
class Template<double>{

	double data;

	public:
		Template(){		// specialized data : exact data type
			
			std::cout << "Double Template Const" << std::endl;
		}
};

int main(){
	
	Template<int> obj1;	// class template la compulsory data type sangayla pahije

	Template<double> obj2;	

	Template<char> obj3;
}

/*
	Int Template Const
	Double Template Const
	In Template Const
*/
