
#include<iostream>

class Employee{
	
	double sal;

	public:
		Employee(double sal){
			
			this->sal = sal;
		}

		int max(int x, int y){
			
			if(x > y)
				return x;
			else 
				return y;
		}


};

int main(){
	
	Employee ashish(200000.0);

	Employee Kanha(150000.0);
	
//	std::cout << ashish.max(10,20) << std::endl;

	std::cout << ashish.max(ashish, Kanha) << std::endl;
}

/*
 	member_function_as_Template_p3.cpp: In function ‘int main()’:
	member_function_as_Template_p3.cpp:33:33: error: cannot convert ‘Employee’ to ‘int’
	   33 |         std::cout << ashish.max(ashish, Kanha) << std::endl;
	      |                                 ^~~~~~
	      |                                 |
	      |                                 Employee
	member_function_as_Template_p3.cpp:14:29: note:   initializing argument 1 of ‘int Employee::max(int, int)’
	   14 |                 int max(int x, int y){
	      |                         ~~~~^
*/
