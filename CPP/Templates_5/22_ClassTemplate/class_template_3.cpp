
//	ithe actually 3 classes ahet

#include<iostream>

template<class T>
class Template{

	T data;
	public:
		Template(T data){
			
			this->data = data;
		}

		void disp(){
			
			std::cout << data << std::endl;
		}
};

int main(){
	
	Template<int> obj1(10);	// class template la compulsory data type sangayla pahije
	
	obj1.disp();

	Template<double> obj2(10.5);	

	obj2.disp();

	Template<char> obj3('a');

	obj3.disp();

}

/*
	10
	10.5
	a
*/
