
#include<iostream>

class Employee{
	
	double sal;

	public:
		Employee(double sal){
			
			this->sal = sal;
		}
		
		template<typename T>
		T& max(T& x, T& y){	// data copy kartana "&" pahije

					// Jar class Template asel, ani parameter jar class chya object yet astil compulsory
					// template ha "reference &" type cha asto
			if(x > y)
				return x;
			else 
				return y;
		}
	
		Employee& max(Employee& x, Employee& y){

			if(x > y)
				return x;
			else
				return y;
		}

		friend bool operator > (Employee& obj1, Employee& obj2){
			
			return (obj1.sal > obj2.sal);
		}

		friend std::ostream& operator << (std::ostream& out, Employee& obj){
			
			out << obj.sal;

			return out;
		}
};

int main(){
	
	Employee ashish(200000.0);

	Employee Kanha(150000.0);

	std::cout << ashish.max(ashish, Kanha) << std::endl;
	
	int x = 10, y = 50;

	std::cout << ashish.max(x, y) << std::endl;
}

/*
 	200000
	50
*/
