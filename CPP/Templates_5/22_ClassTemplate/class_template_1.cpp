
//	Ithe template 1 ahe, pan ithe 2 classes ahet ,compiler ek class add karto

#include<iostream>

template<class T>
class Template{
	
	T data;

	public:
		Template(){
			
			std::cout << "In Template Const" << std::endl;
		}
};

int main(){
	
	Template<int> obj1;	// class template la compulsory data type sangayla pahije

	Template<double> *obj2 = new Template<double>();

	Template<double> obj3;

	std::cout << sizeof(obj1) << std::endl;

	std::cout << sizeof(obj2) << std::endl;

	std::cout << sizeof(obj3) << std::endl;
}

/*
 	In Const
	In Const
	In Const
	4
	8
	8
*/
