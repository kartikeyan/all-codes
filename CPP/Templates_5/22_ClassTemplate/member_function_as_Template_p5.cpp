
#include<iostream>

class Employee{
	
	double sal;

	public:
		Employee(double sal){
			
			this->sal = sal;
		}

		int max(int x, int y){
			
			if(x > y)
				return x;
			else 
				return y;
		}
	
		Employee& max(Employee& x, Employee& y){

			if(x > y)
				return x;
			else
				return y;
		}

		friend bool operator > (Employee& obj1, Employee& obj2){
			
			return (obj1.sal > obj2.sal);
		}
};

int main(){
	
	Employee ashish(200000.0);

	Employee Kanha(150000.0);

	std::cout << ashish.max(ashish, Kanha) << std::endl;
}

/*
 	class_template_2.cpp: In function ‘int main()’:
	class_template_2.cpp:42:19: error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Employee’)
	   42 |         std::cout << ashish.max(ashish, Kanha) << std::endl;
	      |         ~~~~~~~~~ ^~ ~~~~~~~~~~~~~~~~~~~~~~~~~
	      |              |                 |
	      |              |                 Employee
	      |              std::ostream {aka std::basic_ostream<char>}
	
*/
