//	Topic : Abstract Classes and Interfaces

//			Pure Virtual Function

#include<iostream>

class Parent{
	
	public:
		virtual void marry() = 0;		// abstract class  ===> abstract class asla tari virtual table asto

		virtual void property(){
			
			std::cout << "Gold" << std::endl;
		}
};

void Parent::marry(){			//	body dila tari complier consider karto tar pahila tar abstract class ahe
	
	std::cout << "Kirti" << std::endl;
}

class Child : public Parent{
					// marry method override nahi kela tar error yeto
/*	public:						
		void marry(){
			
			std::cout << "Anupama" << std::endl;
		}
*/
};

int main(){
	
	Parent *obj = new Child();

	obj->marry();
	obj->property();
}

/*
 	pure_Virtual_Function_6_Interview_Question.cpp: In function ‘int main()’:
	pure_Virtual_Function_6_Interview_Question.cpp:36:33: error: invalid new-expression of abstract class type ‘Child’
	   36 |         Parent *obj = new Child();
	      |                                 ^
	pure_Virtual_Function_6_Interview_Question.cpp:24:7: note:   because the following virtual functions are pure within ‘Child’:
	   24 | class Child : public Parent{
	      |       ^~~~~
	pure_Virtual_Function_6_Interview_Question.cpp:19:6: note:     ‘virtual void Parent::marry()’
	   19 | void Parent::marry(){
	      |      ^~~~~~
*/
