
//	Topic : Abstract Classes and Interfaces

//			Pure Virtual Function

#include<iostream>

class Parent{
	
	public:
		virtual void marry() = 0;	// pure virtual

		virtual void property(){
			
			std::cout << "Gold" << std::endl;
		}
};

void Parent::marry(){
	
	std::cout << "Kirti" << std::endl;
}

class Child : public Parent{
	
	public:						
		void marry(){
			
			std::cout << "Anupama" << std::endl;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->marry();
	obj->property();
}

/*
	Anupama
	Gold
*/
