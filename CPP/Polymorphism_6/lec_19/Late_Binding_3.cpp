
/*
 	Early Binding : Compile time binding, static binding

	Late binding : Runtime binding, dynamic binding
*/

//	Late Binding Program - Ithe compile time la kalat nahiye ki kutla code bind honar ahe function pointer mule

#include<iostream>

void add(int x, int y){
	
	std::cout << x + y << std::endl;
}

void sub(int x, int y){
	
	std::cout << x - y << std::endl;
}

void mult(int x, int y){
	
	std::cout << x * y << std::endl;
}

int main(){
	
	std::cout << "1.add" << std::endl;
	std::cout << "2.sun" << std::endl;
	std::cout << "3.mult" << std::endl;
	
	int ch;
	std::cout << "Enter Choice" << std::endl;
	std::cin >> ch;

	void (*funPtr[3])(int,int) = {add,sub,mult};	// Array of function pointer  ==> virtual table works like this

	for(int i = 0; i < 3; i++){
		
		funPtr[i](10,20);
	}

/*
	switch(ch){
		case 1:
			funPtr = add;
			break;
		case 2:
			funPtr = sub;
			break;
		case 3:
			funPtr = mult;
			break;
	}
	*/
}

