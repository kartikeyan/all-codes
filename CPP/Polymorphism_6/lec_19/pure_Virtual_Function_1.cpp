
//	Topic : Abstract Classes and Interfaces
//			
//			Pure Virtual Function

#include<iostream>

class Parent{			// abstract class
	
	public:
		virtual void marray() = 0;	// pure virtual function
};

class Child : public Parent{
	
	public:
		void marray(){
			
			std::cout << "Anupama" << std::endl;
		}
};

int main(){
	
	Parent obj;		// abstract class cha object banat nahi

	obj.marray();
}

/*
 	pure_Virtual_Function_1.cpp: In function ‘int main()’:
	pure_Virtual_Function_1.cpp:25:16: error: cannot declare variable ‘obj’ to be of abstract type ‘Parent’
	   25 |         Parent obj;
	      |                ^~~
	pure_Virtual_Function_1.cpp:8:7: note:   because the following virtual functions are pure within ‘Parent’:
	    8 | class Parent{
	      |       ^~~~~~
	pure_Virtual_Function_1.cpp:11:30: note:     ‘virtual void Parent::marray()’
	   11 |                 virtual void marray() = 0;              // abstract class
	      |                              ^~~~~~
*/
