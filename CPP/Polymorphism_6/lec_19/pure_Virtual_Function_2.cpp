
//	Topic : Abstract Classes and Interfaces
//			
//			Pure Virtual Function

#include<iostream>

class Parent{
	
	public:
		virtual void marray() = 0;		// abstract class
};

class Child : public Parent{
	
	public:						// marry method la override karayla pahije nahitar error
		void fun(){
			
			std::cout << "Anupama" << std::endl;
		}
};

int main(){
	
	Child obj;		
}

/*
 	pure_Virtual_Function_2.cpp: In function ‘int main()’:
	pure_Virtual_Function_2.cpp:25:15: error: cannot declare variable ‘obj’ to be of abstract type ‘Child’
	   25 |         Child obj;
	      |               ^~~
	pure_Virtual_Function_2.cpp:14:7: note:   because the following virtual functions are pure within ‘Child’:
	   14 | class Child : public Parent{
	      |       ^~~~~
	pure_Virtual_Function_2.cpp:11:30: note:     ‘virtual void Parent::marray()’
	   11 |                 virtual void marray() = 0;              // abstract class
*/
