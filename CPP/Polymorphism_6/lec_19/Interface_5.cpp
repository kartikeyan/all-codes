
//	Topic : Abstract Classes and Interfaces
//			
//			Pure Virtual Function

#include<iostream>

class IParent{		// protocol asto ki jar Class Interface asel tar tyacha naav I pasun start karava
	
	public:
		virtual void marry() = 0;		// abstract class  ===> abstract class asla tari virtual table asto

		virtual void property() = 0;
};

class Child : public Parent{
	
	public:						
		void marry(){
			
			std::cout << "Anupama" << std::endl;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->marry();
	obj->property();
}

/*
	Anupama
	Gold
*/
