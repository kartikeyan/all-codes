
#include<iostream>

class Parent{
	
	public:
		virtual void getData(){					// VIRTUAL POINTER "_VPTR" gets added. That is why size is 8
			
			std::cout << "Parent getData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		void getData(){
			
			std::cout << "Child getData" << std::endl;
		}
};

int main(){
	
	Parent *obj1 = new Child();

	obj1->getData();		// Late Binding mule Child la call jato array of function pointer mule

	
	std::cout << sizeof(Parent) << std::endl;
	std::cout << sizeof(Child) << std::endl;
}

/*
 	Child getData
	8
	8
*/
