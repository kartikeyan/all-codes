
//	Topic : Abstract Classes and Interfaces
//			
//			Pure Virtual Function

#include<iostream>

class Parent{
	
	public:
		void marry() = 0;		// normal member function

						// should be declared virtual
};

class Child : public Parent{
	
	public:						
		void marry(){
			
			std::cout << "Anupama" << std::endl;
		}
};

int main(){
	

}

/*
 	pure_Virtual_Function_3.cpp:11:22: error: initializer specified for non-virtual method ‘void Parent::marry()’
	   11 |                 void marry() = 0;               // normal member function
	      | 
*/
