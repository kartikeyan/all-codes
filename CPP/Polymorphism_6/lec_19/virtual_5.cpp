
//	In version 7.5 it shows "_vptr" as pointer

#include<iostream>

class Parent{
	
	public:				// void * _vptr , it will intializes in constructor because it is instance variable
		void getData(){			
						// virtual aslya mule virtual table pan add hoil
			
			std::cout << "Parent getData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		void getData(){
			
			std::cout << "Child getData" << std::endl;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();		// obj->_vptr->getData()   ==> internal call
	
//	std::cout << obj->_vptr << std::endl;
}

/*
 	virtual_5.cpp: In function ‘int main()’:
	virtual_5.cpp:28:27: error: ‘class Parent’ has no member named ‘_vptr’
	   28 |         std::cout << obj->_vptr << std::endl;
	      |                           ^~~~~
*/
