
//	Topic : Abstract Classes and Interfaces
//			
//			Pure Virtual Function

#include<iostream>

class Parent{
	
	public:		// pure virtual function

		virtual void marry() = 0;	// abstract class  ===> abstract class asla tari virtual table asto

		virtual void property(){
			
			std::cout << "Gold" << std::endl;
		}
};

class Child : public Parent{
	
	public:						
		void marry(){
			
			std::cout << "Anupama" << std::endl;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->marry();
	obj->property();
}

/*
	Anupama
	Gold
*/
