
/*
 	Early Binding : Compile time binding, static binding

	Late binding : Runtime binding, dynamic binding
*/

//	Early Binding Program

#include<iostream>

void add(int x, int y){
	
	std::cout << x + y << std::endl;
}

void sub(int x, int y){
	
	std::cout << x - y << std::endl;
}

void mult(int x, int y){
	
	std::cout << x * y << std::endl;
}

int main(){
	
	std::cout << "1.add" << std::endl;
	std::cout << "2.sun" << std::endl;
	std::cout << "3.mult" << std::endl;
	
	int ch;
	std::cout << "Enter Choice" << std::endl;
	std::cin >> ch;

	switch(ch){
		
		case 1:
			add(10,20);
			break;
		case 2:
			sub(30,20);
			break;
		case 3:
			mult(10,20);
			break;
	}
}
