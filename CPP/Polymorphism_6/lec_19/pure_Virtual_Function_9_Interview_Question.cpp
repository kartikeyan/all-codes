
//	Topic : Abstract Classes and Interfaces
//			
//			Pure Virtual Function

#include<iostream>

class Parent{
	
	public:
		virtual void marry() = 0;		// abstract class  ===> abstract class asla tari virtual table asto

		virtual void property(){
			
			std::cout << "Gold" << std::endl;
		}
};

void Parent::marry(){				// same as default in java
	
	std::cout << "Kirti" << std::endl;		// brought due to command methods
}

class Child : public Parent{
	
	public:						
		void marry(){
			
			// std::cout << "Anupama" << std::endl;
			
			Parent::marry();
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->marry();
	obj->property();
}

/*
	Kirti
	Gold
*/
