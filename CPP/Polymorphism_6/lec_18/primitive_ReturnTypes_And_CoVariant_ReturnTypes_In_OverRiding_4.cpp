//	Co-Variant types madhe returntype similar pahije

#include<iostream>

class Parent{
	
	public:
		virtual Parent getData(){
			
			std::cout << "Parent getData" << std::endl;

			Parent obj;

			return obj;
		}
};

class Child : public Parent{

	public:
		Child getData(){
			
			std::cout << "Child getData" << std::endl;
			
			Child obj;

			return obj;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
}

/*
 	primitive_ReturnTypes_And_CoVariant_ReturnTypes_In_OverRiding_4.cpp:22:23: error: invalid covariant return type for ‘virtual Child Child::getData()’
	   22 |                 Child getData(){
	      |                       ^~~~~~~
	primitive_ReturnTypes_And_CoVariant_ReturnTypes_In_OverRiding_4.cpp:9:32: note: overridden function is ‘virtual Parent Parent::getData()’
	    9 |                 virtual Parent getData(){
	      |                                ^~~~~~~
*/
