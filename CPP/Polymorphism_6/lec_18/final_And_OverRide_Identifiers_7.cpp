
//	final and overRide identifiers in cpp

//	heh special identifiers ahet tyana meaning ahe

#include<iostream>

class Parent{
	
	public:
		virtual void getData(int x){
			
			std::cout << "get-Data Parent" << std::endl; 
		}	
};

class Child : public Parent{		
	
	public:
		void getData(int x) override{
			
			std::cout << "get-Data Child" << std::endl; 
		}	
};

int main(){
	
	Parent *obj = new Child();
	obj->getData(10);
}

/*
 	get-Data Child
*/
