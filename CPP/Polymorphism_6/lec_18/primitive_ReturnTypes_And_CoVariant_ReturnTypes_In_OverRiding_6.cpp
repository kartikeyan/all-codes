
//	Co-Variant : type madhe similar returntype pahije

#include<iostream>

class Parent{
	
	public:
		virtual Parent* getData(){
			
			std::cout << "Parent getData" << std::endl;

			return this;	// ithe child cha this ahe
		}
};

class Child : public Parent{

	public:
		Child* getData(){
			
			std::cout << "Child getData" << std::endl;

			return this;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
}

/*
 	Child getData
*/
