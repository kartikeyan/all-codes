#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		void getData(){
			
			std::cout << "Parent getData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(){
			
			std::cout << "Child getData" << std::endl;
		}
};

int main(){
	
	Parent obj1;
	obj1.getData();

	Child obj2;
	obj2.getData();

	Parent *obj3 = new Child();	// compile time la bind zalela ghost tich runtime la execute hoto
	obj3->getData();
}

/*
 	Parent Const
	Parent getData

	Parent Const
	Child Const
	Child getData

	Parent Const
	Child Const
	Parent getData
*/
