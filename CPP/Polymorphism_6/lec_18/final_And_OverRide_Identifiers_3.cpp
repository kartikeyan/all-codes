
//	final and overRide identifiers in cpp

//	heh special identifiers ahet tyana meaning ahe

#include<iostream>

class Parent{
	
	public:
		virtual void getData() final{	// member function is final : cannot overRide
			
			std::cout << "get-Data Parent" << std::endl; 
		}	
};

class Child : public Parent{
	
	public:
		void getData(){
			
			std::cout << "get-Data Child" << std::endl; 
		}	
};

int main(){
	
	Parent *obj = new Child();
	obj->getData();
}

/*
 	final_And_OverRide_Identifiers_3.cpp:20:22: error: virtual function ‘virtual void Child::getData()’ overriding final function
	   20 |                 void getData(){
	      |                      ^~~~~~~
	final_And_OverRide_Identifiers_3.cpp:11:30: note: overridden function is ‘virtual void Parent::getData()’
	   11 |                 virtual void getData() final{
	      |                              ^~~~~~~
*/
