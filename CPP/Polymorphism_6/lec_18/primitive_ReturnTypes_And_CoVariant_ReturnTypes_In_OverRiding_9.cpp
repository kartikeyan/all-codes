
//	Co-Variant : type madhe similar returntype pahije

#include<iostream>

class Child;

class Parent{
	
	public:
		virtual Child* getData(){
			
			std::cout << "Parent getData" << std::endl;

			return new Child();	
		}
};

class Child : public Parent{

	public:
		Parent* getData(){
			
			std::cout << "Child getData" << std::endl;

			return new Child();
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
}

/*
 	Child getData
*/
