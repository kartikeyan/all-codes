
#include<iostream>

class Parent{
	
	public:
		int getData(){
			
			std::cout << "Parent getData" << std::endl;
			return 20;
		}
};

class Child : public Parent{

	public:
		virtual int getData(){
			
			std::cout << "Child getData" << std::endl;
			return 10;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
	
	std::cout << sizeof(Parent) << std::endl;	// 1 ===> shows minimum bytes
	
	std::cout << sizeof(Child) << std::endl;	// 8
}

//	virtual is actually pointer

/*
 	Parent getData
	1
	8
*/
