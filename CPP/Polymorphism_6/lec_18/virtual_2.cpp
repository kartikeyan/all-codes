
//	virtual mhnje most derived i.e (most recent) vala print karto 

//	virtual mhnje late binding

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		virtual void getData(){
			
			std::cout << "Parent getData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(){			// Parent madhe virtual lihila tar child madhe pan toh implicit yeto.
			
			std::cout << "Child getData" << std::endl;
		}
};

int main(){
	
	Parent *obj3 = new Child();	
	obj3->getData();

}

/*
 	Parent Const
	Child Const
	Child getData
*/
