//	Co-Variant : type madhe similar returntype pahije

#include<iostream>

class Parent{
	
	public:
		virtual Parent* getData(){
			
			std::cout << "Parent getData" << std::endl;

			return new Parent();	

			// return this;		// it is also valid
		}
};

class Child : public Parent{

	public:
		Child* getData(){	// Parent* getData(){}		=>> it is also valid
			
			std::cout << "Child getData" << std::endl;

			return new Child();

			// return this;		// it is also avlid
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
}

/*
 	Child getData
*/
