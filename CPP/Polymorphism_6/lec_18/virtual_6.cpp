
//	virtual mhnje most derived version i.e (most recent) vala print karto 

//	virtual mhnje late binding

//	Protocol ahe ki Parent class madhe method virtual asel tar emplicitly child class madhe pan virtual lihayla pahije

//	method signature, return type ========>  same (overRiding)

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		virtual void getData(int x){
			
			std::cout << "Parent getData" << std::endl;
		}
			
		virtual void printData(float x){

			std::cout << "Parent printData" << std::endl;
		}


};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(short int x){	// implicitly virtual
			
			std::cout << "Child getData" << std::endl;
		}

		float printData(float x){	// implicitly virtual
			
			std::cout << "Child printData" << std::endl;
		}
};

int main(){
	
	Parent *obj3 = new Child();	
	obj3->getData(10);
	obj3->printData(10.5f);
}

//	virtual chya scenario madhe same return type lagto nahitar error

/*
 	virtual_6.cpp:46:23: error: conflicting return type specified for ‘virtual float Child::printData(float)’
	   46 |                 float printData(float x){       // implicitly virtual
	      |                       ^~~~~~~~~
	virtual_6.cpp:25:30: note: overridden function is ‘virtual void Parent::printData(float)’
	   25 |                 virtual void printData(float x){
	      |                              ^~~~~~~~~
	virtual_6.cpp: In member function ‘virtual float Child::printData(float)’:
	virtual_6.cpp:49:17: warning: no return statement in function returning non-void [-Wreturn-type]
	   49 |                 }
	      |                 ^
*/
