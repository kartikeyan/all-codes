//	virtual mhnje most derived version i.e (most recent) vala print karto 

//	virtual mhnje late binding

//	Protocol ahe ki Parent class madhe method virtual asel tar emplicitly child class madhe pan virtual lihayla pahije

//	method signature, return type ========>  same (overRiding)

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		virtual void getData(int x){		
			
			std::cout << "Parent getData" << std::endl;
		}
			
		virtual void printData(float x){	

			std::cout << "Parent printData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(short int x){	
			
			std::cout << "Child getData" << std::endl;
		}

		void printData(float x){	
			
			std::cout << "Child printData" << std::endl;
		}
};

int main(){
	
	Parent& obj3 = new Child();	

	obj3.getData(10);
	obj3.printData(10.5f);
}

//	Reference kadhi pan address store karat nahi

/*
	virtual_7.cpp: In function ‘int main()’:
	virtual_7.cpp:54:24: error: invalid initialization of non-const reference of type ‘Parent&’ from an rvalue of type ‘Child*’
	   54 |         Parent& obj3 = new Child();
	      |                        ^~~~~~~~~~~
*/
