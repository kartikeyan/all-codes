
//	final and overRide identifiers in cpp

//	heh special identifiers ahet tyana meaning ahe

#include<iostream>

class Parent{
	
	public:
		void getData() final{
			
			std::cout << "get-Data Parent" << std::endl; 
		}	
};

class Child : public Parent{
	
	public:
		void getData(){
			
			std::cout << "get-Data Child" << std::endl; 
		}	
};

int main(){
	
	Parent *obj = new Child();
	obj->getData();
}

/*
 	final_And_OverRide_Identifiers_2.cpp:11:22: error: ‘void Parent::getData()’ marked ‘final’, but is not virtual
	   11 |                 void getData() final{
	      |                      ^~~~~~~
*/
