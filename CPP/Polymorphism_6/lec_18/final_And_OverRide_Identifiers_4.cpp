
//	final and overRide identifiers in cpp

//	heh special identifiers ahet tyana meaning ahe

#include<iostream>

class Parent{
	
	public:
		virtual void getData(){
			
			std::cout << "get-Data Parent" << std::endl; 
		}	
};

class Child final : public Parent{		// class is final
	
	public:
		void getData(){
			
			std::cout << "get-Data Child" << std::endl; 
		}	
};

class Child2 : public Child{
	
};

int main(){
	
	Parent *obj = new Child();
	obj->getData();
}

/*
 	final_And_OverRide_Identifiers_4.cpp:26:7: error: cannot derive from ‘final’ base ‘Child’ in derived type ‘Child2’
	   26 | class Child2 : public Child{
	      |       ^~~~~~
*/
