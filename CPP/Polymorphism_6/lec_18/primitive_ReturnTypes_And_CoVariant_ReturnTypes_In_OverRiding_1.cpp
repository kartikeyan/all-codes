#include<iostream>

class Parent{
	
	public:
		virtual int getData(){
			
			std::cout << "Parent getData" << std::endl;
			return 20;
		}
};

class Child : public Parent{

	public:
		virtual float getData(){
			
			std::cout << "Child getData" << std::endl;
			return 10.5f;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
}

//	Return type same nahiye mahnun conflicting types cha error yeto.

//	Parent class madhe sahmat nahiye tar overRide kela tar returntype same de.

/*
 	primitive_ReturnTypes_And_CoVariant_ReturnTypes_In_OverRiding_1.cpp:17:31: error: conflicting return type specified for ‘virtual float Child::getData()’
	   17 |                 virtual float getData(){
	      |                               ^~~~~~~
	primitive_ReturnTypes_And_CoVariant_ReturnTypes_In_OverRiding_1.cpp:7:29: note: overridden function is ‘virtual int Parent::getData()’
	    7 |                 virtual int getData(){
	      |                             ^~~~~~~
*/
