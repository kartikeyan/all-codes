
//	virtual mhnje most derived version i.e (most recent) vala print karto 

//	virtual mhnje late binding

//	Protocol ahe ki Parent class madhe method virtual asel tar emplicitly child class madhe pan virtual lihayla pahije

//	method signature, return type ========>  same (overRiding)

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		virtual void getData(int x){		
			
			std::cout << "Parent getData" << std::endl;
		}
			
		virtual void printData(float x){	

			std::cout << "Parent printData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(short int x){	
			
			std::cout << "Child getData" << std::endl;
		}

		void printData(float x){	
			
			std::cout << "Child printData" << std::endl;
		}
};

int main(){
/*	
	Child obj1;

	Parent *obj2 = &obj1;		// stack frame var ahe jaga
	
	obj2->getData(10);
        obj2->printData(10.5f);
*/

/*
	Parent *obj3 = new Child();	// heap var ahe jaga

	obj3->getData(10);
	obj3->printData(10.5f);
*/
	Child obj5;
	Parent& obj4 = obj5;		// stack frame var ahe jaga

	obj4.getData(10);
	obj4.printData(10.5f);
}

//	Reference kadhi pan address store karat nahi

/*
 	Parent Const
	Child Const
	Parent getData
	Child printData
*/
