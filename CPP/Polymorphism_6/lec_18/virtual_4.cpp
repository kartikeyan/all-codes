
//	virtual mhnje most derived version i.e (most recent) vala print karto 

//	virtual mhnje late binding

//	Protocol ahe ki Parent class madhe method virtual asel tar emplicitly child class madhe pan virtual lihayla pahije

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		virtual void getData(int x){
			
			std::cout << "Parent getData" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(short int x){		
			
			std::cout << "Child getData" << std::endl;
		}
};

int main(){
	
	Parent *obj3 = new Child();	
	obj3->getData(10);
}

/*
 	Parent Const
	Child Const
	Parent getData
*/
