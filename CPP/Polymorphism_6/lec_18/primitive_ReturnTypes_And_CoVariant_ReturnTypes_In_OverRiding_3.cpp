//	Co-Variant

#include<iostream>

class Parent{
	
	public:
		Parent getData(){
			
			std::cout << "Parent getData" << std::endl;

			Parent obj;

			return obj;
		}
};

class Child : public Parent{

	public:
		Child getData(){
			
			std::cout << "Child getData" << std::endl;
			
			Child obj;

			return obj;
		}
};

int main(){
	
	Parent *obj = new Child();

	obj->getData();
}

/*
 	Parent getData
*/
