
//	virtual mhnje most derived version i.e (most recent) vala print karto 

//	virtual mhnje late binding

//	Protocol ahe ki Parent class madhe method virtual asel tar explicitly child class madhe pan virtual lihayla pahije

//	method signature, return type ========>  same (overRiding)

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		virtual void getData(int x){	// method signature : getData(int)
			
			std::cout << "Parent getData" << std::endl;
		}
			
		virtual void printData(float x){		// printData(float)

			std::cout << "Parent printData" << std::endl;
		}


};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		void getData(short int x){	// implicitly virtual		// getData(short int)
			
			std::cout << "Child getData" << std::endl;
		}

		void printData(float x){	// implicitly virtual		// printData(float)
			
			std::cout << "Child printData" << std::endl;
		}
};

int main(){
	
	Parent *obj3 = new Child();	
	obj3->getData(10);
	obj3->printData(10.5f);
}

/*
 	Parent Const
	Child Const
	Parent getData
	Child printData
*/
