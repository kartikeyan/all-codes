
#include<iostream>

class Demo{
	
	public:
//		virtual static void getData(){}			// Error

		static void getData(){
			
			std::cout << "Demo::GetData" << std::endl;
		}
};

class DemoChild : public Demo{
	
	public:
		static void getData(){
			
			std::cout << "DemoChild::GetData" << std::endl;
		}
};

int main(){
	
	Demo *obj = new DemoChild();

	obj->getData();
}

//	Demo::GetData
