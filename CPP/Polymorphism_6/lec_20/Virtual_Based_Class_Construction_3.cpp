
//	1] C++ has solved Diamond Problem using Virtual Based Class.

//	2] Class Construction should not be called repeatedly, in this case Virtual Based Class is used.
//		i.e to avoid multiple constructions of Base class : virtual Bases Class is used.

#include<iostream>

class Demo{

	public:
		Demo(){
			
			std::cout << "Contructor Demo" << std::endl;
		}
};

class DemoChild1 : virtual public Demo{

	public:
		DemoChild1(){
			
			std::cout << "Contructor DemoChild1" << std::endl;
		}
};		
							
class DemoChild2 : virtual public Demo{

	public:
		DemoChild2(){
			
			std::cout << "Contructor DemoChild2" << std::endl;
		}
};

// class Child : public Demo, public DemoChild1, public DemoChild2{}	// implicitly asa asto

class Child : public DemoChild1, public DemoChild2{	// ithe direct demo chya constructor la call kela jato

	public:
		Child(){
			
			std::cout << "Contructor Child" << std::endl;
		}
};

int main(){
	
	Child obj;
}

/*
 	Contructor Demo
	Contructor DemoChild1
	Contructor DemoChild2
	Contructor Child
*/
