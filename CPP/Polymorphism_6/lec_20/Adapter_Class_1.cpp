
#include<iostream>

class IDemo{		// interface

	public:
		virtual void fun1() = 0;	// pure virtual function
		virtual void fun2() = 0;	// pure virtual function
};

class Adapter : public IDemo{
	
	public:	
		void fun1(){
		
		}

		void fun2(){
		
		}
};

class DemoChild1 : public Adapter{

	public:
		void fun1(){
			
			std::cout << "DemoChild1" << std::endl;
		}
};

class DemoChild2 : public Adapter{

	public:
		void fun2(){
			
			std::cout << "DemoChild2" << std::endl;
		}
};

int main(){
	
	DemoChild1 obj1;
	DemoChild2 obj2;
}
