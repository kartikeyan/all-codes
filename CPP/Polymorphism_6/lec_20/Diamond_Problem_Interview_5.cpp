
//	1] C++ has solved Diamond Problem using Virtual Based Class
//
//	2] 

#include<iostream>

class Demo{

	public:
		void getData(){
			
			std::cout << "Demo::getData" << std::endl;
		}
};

class DemoChild1 : virtual public Demo{


};		
							// virtual kelyamule data eka bajune jail
class DemoChild2 : virtual public Demo{


};

class Child : public DemoChild1, public DemoChild2{	// fakt 1 copy ithe yetoy

	
};

int main(){
	
	Child obj;

	obj.getData();
}

/*
 	Demo::getData
*/
