
//	C++ has solved Diamond Problem using Virtual Based Class

#include<iostream>

class Demo{

	public:
		void getData(){
			
			std::cout << "Demo::getData" << std::endl;
		}
};

class DemoChild1 : public Demo{


};

class DemoChild2 : public Demo{


};

class Child : public DemoChild1, public DemoChild2{

	
};

int main(){
	
	Child obj;

	obj.getData();
}

/*
 	Diamond_Problem_Interview_4.cpp: In function ‘int main()’:
	Diamond_Problem_Interview_4.cpp:32:13: error: request for member ‘getData’ is ambiguous
	   32 |         obj.getData();
	      |             ^~~~~~~
	Diamond_Problem_Interview_4.cpp:7:22: note: candidates are: ‘void Demo::getData()’
	    7 |                 void getData(){
	      |                      ^~~~~~~
	Diamond_Problem_Interview_4.cpp:7:22: note:                 ‘void Demo::getData()’
*/
