
#include<iostream>

class Demo{

	private:

		Demo(){
		
			std::cout << "In Constructor" << std::endl;
		}

	public:	
		static Demo *obj;

		static Demo* getObject(){
			
			return obj;
		}

		void getData(){
			
			std::cout << "This " << this << std::endl;
		}
};

Demo* Demo::obj = new Demo();		// most important

int main(){
	
	Demo *ptr1 = Demo::getObject();
	Demo *ptr2 = Demo::getObject();
	Demo *ptr3 = Demo::getObject();
	
	std::cout << ptr1 << std::endl;
	std::cout << ptr2 << std::endl;
	std::cout << ptr3 << std::endl;
	
	ptr1->getData();
	ptr2->getData();
	ptr3->getData();

	return 0;
}

/*
 	In Constructor
	0x555d0f433eb0
	0x555d0f433eb0
	0x555d0f433eb0
	This 0x558f49b1eeb0
	This 0x558f49b1eeb0
	This 0x558f49b1eeb0
*/
