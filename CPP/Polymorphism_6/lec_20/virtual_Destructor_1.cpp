
#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}
		
		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}
		
		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}
};

int main(){
	
	Child obj;
}

/*
 	Parent Const
	Child Const
	Child Dest
	Parent Dest
*/
