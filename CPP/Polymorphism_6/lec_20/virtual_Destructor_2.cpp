
/*
 	Virtual Destructor

	1] Deleting a derived class object using a pointer of base class type that has a non-virtual destructor results in undefined behavior. 

	2] To correct this situation, the base class should be defined with a virtual destructor.

	3] Making base class destructor virtual guarantees that the object of derived class is destructed properly, i.e., both base class and derived class destructors are called.

	4] As a guideline, any time you have a virtual function in a class, you should immediately add a virtual destructor (even if it does nothing). This way, you ensure against any surprises later. 

*/

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}
		
		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}
};

class Child : public Parent{
	
	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}
		
		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}
};

int main(){
	
	Parent *obj = new Child;

	delete obj;
}

/*
 	Parent Const
	Child Const
	Parent Dest	
*/
