
#include<iostream>

class Parent{
	
	int x = 10;
	
	protected:
		int y = 20;

	public:
		int z = 30;

		void getData(){
			
			std::cout << x << " " << y << " " << z << std::endl;
		}
};

class Child : Parent{		// we have inherited Parent class as Private

	public:
		void getInfo(){
				
			std::cout << y << " " << z << std::endl;
		}
};

int main(){
	
	Child obj;

	obj.getInfo();		// 20 30
	
	obj.getData();		// getData() is private we cannot access from outside
}

/*
	access_Specifiers_In_Inheritance_3.cpp: In function ‘int main()’:
	access_Specifiers_In_Inheritance_3.cpp:35:20: error: ‘void Parent::getData()’ is inaccessible within this context
	   35 |         obj.getData();
	      |         ~~~~~~~~~~~^~
	access_Specifiers_In_Inheritance_3.cpp:14:22: note: declared here
	   14 |                 void getData(){
	      |                      ^~~~~~~
	access_Specifiers_In_Inheritance_3.cpp:35:20: error: ‘Parent’ is not an accessible base of ‘Child’
	   35 |         obj.getData();
	      |         ~~~~~~~~~~~^~
*/
