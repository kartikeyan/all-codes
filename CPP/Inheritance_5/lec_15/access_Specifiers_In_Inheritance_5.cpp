
#include<iostream>

class Parent{
	
	int x = 10;
	
	protected:
		int y = 20;

	public:
		int z = 30;
		
		Parent(int x, int y, int z){
			
			this->x = x;
			this->y = y;
			this->z = z;
		}

		void getData(){
			
			std::cout << x << " " << y << " " << z << std::endl;
		}
};

class Child : Parent{		// we have inherited Parent class as Private

	public:

		Child(int x, int y, int z){

			Parent(x,y,z);		// ithun compiler default() constructor la pan call karto
		}

		void getInfo(){
				
			std::cout << y << " " << z << std::endl;

			getData();
		}
};

int main(){
	
	Child obj(40,50,60);

	obj.getInfo();		
}

/*
	access_Specifiers_In_Inheritance_5.cpp: In constructor ‘Child::Child(int, int, int)’:
	access_Specifiers_In_Inheritance_5.cpp:31:43: error: no matching function for call to ‘Parent::Parent()’
	   31 |                 Child(int x, int y, int z){
	      |                                           ^
	access_Specifiers_In_Inheritance_5.cpp:14:17: note: candidate: ‘Parent::Parent(int, int, int)’
	   14 |                 Parent(int x, int y, int z){
	      |                 ^~~~~~
	access_Specifiers_In_Inheritance_5.cpp:14:17: note:   candidate expects 3 arguments, 0 provided
	access_Specifiers_In_Inheritance_5.cpp:4:7: note: candidate: ‘constexpr Parent::Parent(const Parent&)’
	    4 | class Parent{
	      |       ^~~~~~
	access_Specifiers_In_Inheritance_5.cpp:4:7: note:   candidate expects 1 argument, 0 provided
	access_Specifiers_In_Inheritance_5.cpp:4:7: note: candidate: ‘constexpr Parent::Parent(Parent&&)’
	access_Specifiers_In_Inheritance_5.cpp:4:7: note:   candidate expects 1 argument, 0 provided
*/
