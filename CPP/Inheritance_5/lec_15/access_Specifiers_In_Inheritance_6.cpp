
#include<iostream>

class Parent{
	
	int x = 10;
	
	protected:
		int y = 20;

	public:
		int z = 30;
		
		Parent(){
		
		}

		Parent(int x, int y, int z){
			
			this->x = x;
			this->y = y;
			this->z = z;
		}

		void getData(){
			
			std::cout << x << " " << y << " " << z << std::endl;
		}
};

class Child : Parent{		// we have inherited Parent class as Private

	public:

		Child(int x, int y, int z){

			Parent(x,y,z);
		}

		void getInfo(){
				
			std::cout << y << " " << z << std::endl;

			getData();
		}
};

int main(){
	
	Child obj(40,50,60);

	obj.getInfo();		
}

/*
	20 30
	10 20 30
*/
