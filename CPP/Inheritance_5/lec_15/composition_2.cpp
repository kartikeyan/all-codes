
//	Compositon or Aggregation Code

#include<iostream>

class Employee{
	
	std::string ename;
	int empId;

	public:
		Employee(std::string ename, int empid){
			
			std::cout << "Constructor" << std::endl;
			this->ename = ename;
			this->empid = empid;
		}

		void getInfo(){
			
			std::cout << ename << " " << empId << std::endl;
		}

		~Employee(){
			
			std::cout << "Employee Destructor" << std::endl;
		}
};

class Company{
	
	std::string cname = "Veritas";
	int stremp = 100;

//	Employee obj;		// Instance Variable

	public:
		Company(std::string cname, int stremp = 0, std::string ename, int empId){
			
			std::cout << "Company Const" << std::endl;			
			this->cname = cname;
			this->stremp = stremp;

			Employee obj(ename, empId);
		}

		void getInfo(){
			
			std::cout << cname << " " << stremp << std::endl;

			obj.getInfo();	
		}
		
		~Company(){
			
			std::cout << "Company Destructor" << std::endl;
		}
};

int main(){
	
	Company obj("pubmatic",5000, "Rahul", 300);

	obj.getInfo();
}

/*
 	Constructor
	Company Const
	pubmatic 5000
	kanha 255
	Company Destructor
	Employee Destructor
*/
