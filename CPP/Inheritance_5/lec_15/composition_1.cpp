
//	Compositon or Aggregation Code

#include<iostream>

class Employee{
	
	std::string ename = "kanha";
	int empId = 255;

	public:
		Employee(){
			
			std::cout << "Constructor" << std::endl;
		}

		void getInfo(){
			
			std::cout << ename << " " << empId << std::endl;
		}

		~Employee(){
			
			std::cout << "Employee Destructor" << std::endl;
		}
};

class Company{
	
	std::string cname = "Veritas";
	int stremp = 100;

	Employee obj;		// Instance Variable

	public:
		Company(std::string cname, int stremp = 0){
			
			std::cout << "Company Const" << std::endl;			
			this->cname = cname;
			this->stremp = stremp;
		}

		void getInfo(){
			
			std::cout << cname << " " << stremp << std::endl;

			obj.getInfo();	
		}
		
		~Company(){
			
			std::cout << "Company Destructor" << std::endl;
		}
};

int main(){
	
	Company obj("pubmatic",5000);

	obj.getInfo();
}

/*
 	Constructor
	Company Const
	pubmatic 5000
	kanha 255
	Company Destructor
	Employee Destructor
*/
