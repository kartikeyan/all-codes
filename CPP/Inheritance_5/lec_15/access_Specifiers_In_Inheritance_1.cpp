
#include<iostream>

class Parent{
	
	int x = 10;
	
	protected:
		int y = 20;

	public:
		int z = 30;
};

class Child{


};

int main(){
	
	Parent obj;

	std::cout << obj.y << std::endl;	

	std::cout << obj.z << std::endl;	
}

/*
 	access_Specifiers_In_Inheritance_1.cpp: In function ‘int main()’:
	access_Specifiers_In_Inheritance_1.cpp:24:26: error: ‘int Parent::y’ is protected within this context
	   24 |         std::cout << obj.y << std::endl;        // 20
	      |                          ^
	access_Specifiers_In_Inheritance_1.cpp:9:21: note: declared protected here
	    9 |                 int y = 20;
	      |                     ^
*/
