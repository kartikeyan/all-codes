
//	By default inheritance private asto

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}	
};

class Child : public Parent{

	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}	
};

int main(){
	
	Child obj;
}

/*
 	Parent Const
	Child Const
*/
