
//	By default inheritance private asto

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}	
};

class Child : public Parent{

	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}	
		
		friend void* operator new(size_t size){
			
			std::cout << "Child new" << std::endl;

			void *ptr = malloc(size);

			return ptr;
		}
};

int main(){

	Child *obj = new Child();	// tevha vapraycha jevha object saglyana disayla pahije	
	
	delete obj;	
}

/*
 	Child new
	Parent Const
	Child Const
	Child Dest
	Parent Dest
*/
