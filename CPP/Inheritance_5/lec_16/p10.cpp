
#include<iostream>

class Parent{
	
	int x = 10;
	int y = 20;

	public:
		int a = 30;

		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}
		
		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}

		void getData(){
			
			std::cout << x << " " << y << " "<< a << std::endl;
		}
};

class Child : public Parent{
	
	int z = 40;

	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}
		
		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}
		
		void printData(){
			
			std::cout << z << " " << a << std::endl;
		}
};

int main(){
	
	Child obj2;

	obj2.getData();
	obj2.printData();
}

/*
 	Parent Const
	Child Const
	10 20 30
	40 30
	Child Dest
	Parent Dest
*/

