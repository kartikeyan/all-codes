
#include<iostream>

class Parent{
	
	int x = 10;
	int y = 20;

	public:
		Parent(){

			std::cout << "Parent Const" << std::endl;

			std::cout << "this Para " << this << std::endl;
		}

		Parent(int x, int y){
			
			std::cout << "Parent Const" << std::endl;
			std::cout << "this para " << this  << std::endl;

			this->x = x;
			this->y = y;
			
		}
		
		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}

		void getData(){
			
			std::cout << x << " " << y << std::endl;
		}
};

class Child : public Parent{
	
	int z = 40;

	public:
		Child(int x ,int y,int z){
						// ithe Parent() pan ahe
			Parent(x,y);
			std::cout << "this Child " << this  << std::endl;

			std::cout << "Child Const" << std::endl;
		}
		
		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}
		
		void printData(){
			
			std::cout << z  << std::endl;
		}
};

int main(){
	
	Child obj(40,50,60);

	obj.getData();
//	obj.printData();
}

/*
	Parent Const
	this Para 0x7fff3255a93c
	Parent Const
	this para 0x7fff3255a900
	Parent Dest
	this Child 0x7fff3255a93c
	Child Const
	10 20
	Child Dest
	Parent Dest
*/

