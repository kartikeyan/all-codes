
//	By default inheritance private asto

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}	
};

class Child : public Parent{

	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}	
		
		friend void* operator new(size_t size){		// prototype
			
			std::cout << "Child new" << std::endl;

			void *ptr = malloc(size);

			return ptr;
		}

		void operator delete(void *ptr){	// prototype
			
			std::cout << "delete child" << std::endl;

			free(ptr);
		}
};

int main(){

	Child *obj = new Child();	// tevha vapraycha jevha object saglyana disayla pahije	
	
	// operator new(sizeof(Child))
	// obj = ptr
	// Child(obj)
	// constructor
		// constructor parent
		// constructor child
	
	delete obj;	
	// desctructor			==== notify
		// destructor child	
		// desrtcutor paremt
	// operator delete(obj)
}

//	first notification i.e destructor and second delete operator

/*
 	Child new
	Parent Const
	Child Const
	Child Dest
	Parent Dest
	delete child
*/
