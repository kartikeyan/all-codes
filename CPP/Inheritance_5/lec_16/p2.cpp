
//	By default inheritance private asto

#include<iostream>

class Parent{
	
	public:
		Parent(){
			
			std::cout << "Parent Const" << std::endl;
		}

		~Parent(){
			
			std::cout << "Parent Dest" << std::endl;
		}	
};

class Child : public Parent{

	public:
		Child(){
			
			std::cout << "Child Const" << std::endl;
		}

		~Child(){
			
			std::cout << "Child Dest" << std::endl;
		}	
};

int main(){
	
	Child obj;
}

/*
 	Parent Const
	Child Const
	Child Dest
	Parent Dest
*/

/*
 	Child destructor is called first because is Parent destructor is called first then there is possibility that child
	class may call parent class methods, variabels but when child destructor is called first then parent class already 
	cannot call child class methods or variables that is why child constructor is called first
*/
