
#include<iostream>

class Parent{
	int x = 10;

	protected:
		int y = 20;

	public:
		int z = 30;

		void getData(){
			
			std::cout << "In getData" << std::endl;
		}
};

class Child : public Parent{
	
	void getData() = delete;
};

int main(){
	
	Child obj;

	std::cout <<  obj.z << std::endl;

	obj.getData();
}

/*
 	changing_scope_of_Access_Specifier_code_No26_from_public_In_Parent_Class_to_Deleting_In_Child_Class.cpp: In function ‘int main()’:
	changing_scope_of_Access_Specifier_code_No26_from_public_In_Parent_Class_to_Deleting_In_Child_Class.cpp:30:20: error: use of deleted function ‘void Child::getData()’
	   30 |         obj.getData();
	      |         ~~~~~~~~~~~^~
	changing_scope_of_Access_Specifier_code_No26_from_public_In_Parent_Class_to_Deleting_In_Child_Class.cpp:21:14: note: declared here
	   21 |         void getData() = delete;
	      |              ^~~~~~~
*/
