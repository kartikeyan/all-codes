
/*
 	Friend function is not inherited in Child Class
*/

#include<iostream>

class Parent{
	
	int x = 10;

	public:
		friend std::ostream& operator << (std::ostream& out, const Parent& obj){
			
			out << "In Parent" << std::endl;

			out << obj.x;

			return out;
		}

		Parent(Parent& obj){
			
			std::cout << "Parent Copy" << std::endl;
		}

		Parent(){
			std::cout << "Parent Const" << std::endl;
		}
};


class Child : public Parent{
	
	int x = 20;

	public:
	/*
		friend std::ostream& operator << (std::ostream& out, const Child& obj){
			
			out << "In Child" << std::endl;

			out << obj.x;

			return out;
		}
	*/
		Child(){
			std::cout << "Child Const" << std::endl;
		}
};


int main(){
	
	Child obj;

	std::cout << (const Parent&)obj << std::endl;

	// operator << (ostream&, Child&)
}

/*
 	Parent Const
	Child Const
	In Parent
	10
*/
