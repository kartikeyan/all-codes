#include<iostream>

class Parent{
	int x = 10;

	protected:
		int y = 20;

	public:
		int z = 30;
};

class Child : public Parent{
	
	public:
		Parent::y;	// deprecated : because we are using Parent namespace
};

int main(){
	
	Child obj;

	std::cout << obj.x << obj.y << obj.z << std::endl;
}

/*
 	changing_scope_of_Access_Specifier_code_No23.cpp:16:17: warning: access declarations are deprecated in favour of using-declarations; suggestion: add the ‘using’ keyword [-Wdeprecated]
	   16 |                 Parent::y;      // deprecated : because we are using Parent namespace
	      |                 ^~~~~~
	changing_scope_of_Access_Specifier_code_No23.cpp: In function ‘int main()’:
	changing_scope_of_Access_Specifier_code_No23.cpp:23:26: error: ‘int Parent::x’ is private within this context
	   23 |         std::cout << obj.x << obj.y << obj.z << std::endl;
	      |                          ^
	changing_scope_of_Access_Specifier_code_No23.cpp:4:13: note: declared private here
	    4 |         int x = 10;
	      |             ^

