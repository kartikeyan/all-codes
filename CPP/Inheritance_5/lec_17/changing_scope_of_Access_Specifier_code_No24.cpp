#include<iostream>

class Parent{
	int x = 10;

	protected:
		int y = 20;

	public:
		int z = 30;
};

class Child : public Parent{
	
	public:
		using Parent::y;	
};

int main(){
	
	Child obj;

	std::cout << obj.x << obj.y << obj.z << std::endl;
}

/*
 	changing_scope_of_Access_Specifier_code_No24.cpp: In function ‘int main()’:
	changing_scope_of_Access_Specifier_code_No24.cpp:25:26: error: ‘int Parent::x’ is private within this context
	   25 |         std::cout << obj.x << obj.y << obj.z << std::endl;
	      |                          ^
	changing_scope_of_Access_Specifier_code_No24.cpp:4:13: note: declared private here
	    4 |         int x = 10;
	      |             ^
*/
