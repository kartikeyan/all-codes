
#include<iostream>

class Parent1{
	
	public:
		Parent1(){
			
			std::cout << "Const Parent1" << std::endl;
		}
};

class Parent2{
	
	public:
		Parent2(){
			
			std::cout << "Const Parent2" << std::endl;
		}
};

class Child : public Parent1, public Parent2{
	
	public:
		Child(){
			
			std::cout <<" COnst Child" << std::endl;
		}
};

int main(){
	
	Child obj;
}

/*
 	Const Parent1
	Const Parent2
	COnst Child
*/
