
#include<iostream>

class Parent{
	
	int x = 10;

	public:
		Parent(){
			
			std::cout << "Const parent" << std::endl;
		}

		void getData(){
			
			std::cout << "Parent x : " << x << std::endl;
		}

		Parent(Parent& obj){
			
			std::cout << "Copy " << std::endl;
		}
};

class Child : public Parent{
	
	int x = 20;

	public:
		Child(){
			
			std::cout << "Const child" << std::endl;
		}

		void getData(){
			
			Parent::getData();	// 1st
			std::cout << "Child x : " << x << std::endl;

		}
};

int main(){
	
	Child obj;
//	obj.getData();

	
	(Parent(obj)).getData();	// 2nd		: Don't use this because new object is created
	
	/*
	obj.Parent::getData();		// 3rd

	(static_cast<Parent&>(obj)).getData();	// 4th
	
	*/
}

/*
 	Const parent
	Const child
	Copy 
	Parent x : 10
*/
