/*
 	Public Inheritance

		Parent madla public variable, method child class madhe public ch rahto same for protected, private
*/


#include<iostream>

class Parent{
	
	int x = 10;

	protected:
		int y = 20;

	public:
		int z = 30;

	Parent(){
		
		std::cout << "Parent Const" << std::endl;
	}
};

class Child : public Parent{		// Public Inheritance
	
	public:
		Child(){
			
			std::cout << "Child const" << std::endl;
		}
};

int main(){
	
	Child obj;

	std::cout << obj.x << obj.y << obj.z << std::endl;
}

/*
 	p1.cpp: In function ‘int main()’:
	p1.cpp:39:26: error: ‘int Parent::x’ is private within this context
	   39 |         std::cout << obj.x << obj.y << obj.z << std::endl;
	      |                          ^
	p1.cpp:12:13: note: declared private here
	   12 |         int x = 10;
	      |             ^
	p1.cpp:39:35: error: ‘int Parent::y’ is protected within this context
	   39 |         std::cout << obj.x << obj.y << obj.z << std::endl;
	      |                                   ^
	p1.cpp:15:21: note: declared protected here
	   15 |                 int y = 20;
	      |                     ^
*/
