
#include<iostream>

class Parent1{
	
	int x = 10;

	public:
		Parent1(){
			
			std::cout << "Const Parent1" << std::endl;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}
};

class Parent2{
	
	int y = 20;

	public:
		Parent2(){
			
			std::cout << "Const Parent2" << std::endl;
		}
		
		void printData(){
			
			std::cout << y << std::endl;
		}
};

class Child : public Parent1, public Parent2{
	
	public:
		Child(){
			
			std::cout << "COnst Child" << std::endl;
		}
};

int main(){
	
	Child obj;

	obj.getData();

	obj.printData();
}

/*
 	Const Parent1
	Const Parent2
	COnst Child
	10
	20
*/
