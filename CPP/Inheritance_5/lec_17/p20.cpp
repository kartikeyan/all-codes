
/*
	
	Friend function is not inherited in Child class

	But in this case it implicit typecastes and upcasts and calls Parent class

	Fakt Member function inherit hotat
*/

#include<iostream>

class Parent{
	
	int x = 10;

	public:
		friend std::ostream& operator << (std::ostream& out, const Parent& obj){
			
			out << "In Parent" << std::endl;

			out << obj.x;

			return out;
		}
};


class Child : public Parent{
	
};


int main(){
	
	Child obj;
	std::cout << obj << std::endl;		// std::cout << (const Parent&)obj << std::endl;  ==> Internal call
	
	std::cout << (const Parent&)obj << std::endl;

	// operator << (ostream&, Child&)
}

/*
 	In Parent
	10
	In Parent
	10
*/
