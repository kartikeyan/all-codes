
#include<iostream>

class Parent{
	
	int x = 10;

	public:
		Parent(){
			
			std::cout << "Const parent" << std::endl;
		}

		void getData(){
			
			std::cout << "Parent x : " << x << std::endl;
		}
};

class Child : public Parent{
	
	int y = 20;

	public:
		Child(){
			
			std::cout << "Const child" << std::endl;
		}

		void printData(){
			
			std::cout << "Child y : " << y << std::endl;
		}
};

int main(){
	
	Child obj;
	obj.getData();
	obj.printData();	
}

/*
 	Const parent
	Const child
	Parent x : 10
	Child y : 20
*/
