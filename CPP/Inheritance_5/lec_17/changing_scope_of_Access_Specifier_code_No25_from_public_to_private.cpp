
#include<iostream>

class Parent{
	int x = 10;

	protected:
		int y = 20;

	public:
		int z = 30;

		void getData(){
			
			std::cout << "In getData" << std::endl;
		}
};

class Child : public Parent{
	
	using Parent::getData;

	public:
		using Parent::y;	
};

int main(){
	
	Child obj;

	std::cout << obj.y << " " << obj.z << std::endl;

	obj.getData();
}

/*
 	changing_scope_of_Access_Specifier_code_No25.cpp: In function ‘int main()’:
	changing_scope_of_Access_Specifier_code_No25.cpp:32:20: error: ‘void Parent::getData()’ is inaccessible within this context
	   32 |         obj.getData();
	      |         ~~~~~~~~~~~^~
	changing_scope_of_Access_Specifier_code_No25.cpp:12:22: note: declared here
	   12 |                 void getData(){
	      |                      ^~~~~~~
*/
