
#include<iostream>

class Parent1{
	
	int x = 10;

	public:
		Parent1(){
			
			std::cout << "Const Parent1" << std::endl;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}
};

class Parent2{
	
	int x = 20;

	public:
		Parent2(){
			
			std::cout << "Const Parent2" << std::endl;
		}
		
		void getData(){
			
			std::cout << x << std::endl;
		}
};

class Child : public Parent1, public Parent2{
	
	public:
		Child(){
			
			std::cout << "COnst Child" << std::endl;
		}
		
		void getData(){
						
			std::cout << "Child getData" << std::endl;
			Parent1::getData();
			Parent2::getData();
		}
};

int main(){
	
	Child obj;

	obj.getData();
}

/*
 	Const Parent1
	Const Parent2
	COnst Child
	Child getData
	10
	20
*/
