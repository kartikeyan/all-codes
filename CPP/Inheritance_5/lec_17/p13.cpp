
#include<iostream>

class Parent{
	
	int x = 10;

	public:
		Parent(){
			
			std::cout << "Const parent" << std::endl;
		}

		void getData(){
			
			std::cout << "Parent x : " << x << std::endl;
		}
};

class Child : public Parent{
	
	int x = 20;

	public:
		Child(){
			
			std::cout << "Const child" << std::endl;
		}

		void getData(){
			
			std::cout << "Child x : " << x << std::endl;

		}
};

int main(){
	
	Child obj;
	obj.getData();

	obj.Parent::getData();
}

/*
 	Const parent
	Const child
	Child x : 20
	Parent x : 10
*/
