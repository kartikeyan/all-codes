
#include<iostream>

class Parent1{
	
	int x = 10;

	public:
		Parent1(){
			
			std::cout << "Const Parent1" << std::endl;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}
};

class Parent2{
	
	int x = 20;

	public:
		Parent2(){
			
			std::cout << "Const Parent2" << std::endl;
		}
		
		void getData(){
			
			std::cout << x << std::endl;
		}
};

class Child : public Parent1, public Parent2{
	
	public:
		Child(){
			
			std::cout << "COnst Child" << std::endl;
		}
/*		
		void getData(){
						
			std::cout << "Child getData" << std::endl;
			Parent1::getData();
			Parent2::getData();
		}
	*/
};

int main(){
	
	Child obj;

	obj.getData();
}

/*
 	multiple_Inheritance_31.cpp: In function ‘int main()’:
	multiple_Inheritance_31.cpp:57:13: error: request for member ‘getData’ is ambiguous
	   57 |         obj.getData();
	      |             ^~~~~~~
	multiple_Inheritance_31.cpp:30:22: note: candidates are: ‘void Parent2::getData()’
	   30 |                 void getData(){
	      |                      ^~~~~~~
	multiple_Inheritance_31.cpp:14:22: note:                 ‘void Parent1::getData()’
	   14 |                 void getData(){
	      |                      ^~~~~~~
*/
