
/*
 	Friend function is not inherited in Child Class
*/

#include<iostream>

class Parent{
	
	int x = 10;

	public:
		friend std::ostream& operator << (std::ostream& out, const Parent& obj){
			
			out << "In Parent" << std::endl;

			out << obj.x;

			return out;
		}

		Parent(Parent& obj){
			
			std::cout << "Parent Copy" << std::endl;
		}

		Parent(){
			std::cout << "Parent Const" << std::endl;
		}
};


class Child : public Parent{
	
	int x = 20;

	public:
	/*
		friend std::ostream& operator << (std::ostream& out, const Child& obj){
			
			out << "In Child" << std::endl;

			out << obj.x;

			return out;
		}
	*/
		Child(){
			std::cout << "Child Const" << std::endl;
		}
};

int main(){
	
	Child obj1;

	std::cout << (const Parent)obj1 << std::endl;	// call goes to copy const

//	std::cout << (const Parent&)obj1 << std::endl;	// call not goes to copy const

//	std::cout << ( Parent)obj1 << std::endl;	// const is compulsory otherwise error

	Child obj2;

	std::cout << obj2 << std::endl;

	// operator << (ostream&, Child&)
}

/*
 	Parent Const
	Child Const
	Parent Copy
	In Parent
	10
	Parent Const
	Child Const
	In Parent
	10
*/
