
#include<iostream>

int main(){
	
	int x = 10;		// copy initialization

	int y(20);		// direct initialization

	std::cout << x << std::endl;	// 10

	std::cout << y << std::endl;	// 20
	
	int z{30};		// Uniform initialization, list or brace initialization
				// Came after 11th version

	std::cout << z << std::endl;	// 30
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Variables_2$ g++ -std=c++03 p1.cpp 
p1.cpp: In function ‘int main()’:
p1.cpp:14:14: warning: extended initializer lists only available with ‘-std=c++11’ or ‘-std=gnu++11’
   14 |         int z{30};              // Uniform initialization, list or brace initialization
      |              ^

*/

//	We can compile with other versions using command "g++ -std=c++20 filename.cpp" 
//			Here 20 is version
