
#include<iostream>

int main(){
	
	int x = 10;
	int y = 20;

	std::cout << x << std::endl;
	std::cout << y << std::endl;
	
	int a{10};
	int b{10.5f};

	std::cout << a << std::endl;
	std::cout << b << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Variables_2$ g++ uniformInitialization.cpp
uniformInitialization.cpp: In function ‘int main()’:
uniformInitialization.cpp:13:20: error: narrowing conversion of ‘1.05e+1f’ from ‘float’ to ‘int’ [-Wnarrowing]
   13 |         int b{10.5f};
      |           

*/
