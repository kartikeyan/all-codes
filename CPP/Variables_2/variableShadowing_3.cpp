
/*
 	Variable Shadowing
		
		1] Local Variable Shadowing
		2] Global Variable Shadowing
*/

//	Global Variable Shadowing

#include<iostream>

int x = 500;

int main(){
	
	int x = 10;

	std::cout << x << std::endl;	

	{
		int x = 20;

		std::cout << x << std::endl;	

		std::cout << ::x << std::endl;	// scope resolution(::) global variable asto tevhach upyog hoto

		x = 30;

		std::cout << x << std::endl;	

	}

	std::cout << x << std::endl;		
}

/*
 	10
	20
	500
	30
	10
*/
