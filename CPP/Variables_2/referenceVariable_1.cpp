
/*
 	Reference Variables (Allias)
*/


#include<iostream>

int main(){
	
	int x = 10;

	int &y;

	std::cout << x << std::endl;	

	std::cout << y << std::endl;	
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Variables_2$ g++ referenceVariable_1.cpp 
	referenceVariable_1.cpp: In function ‘int main()’:
	referenceVariable_1.cpp:13:14: error: ‘y’ declared as reference but not initialized
	   13 |         int &y;
	      |      
*/
