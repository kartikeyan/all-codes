
/*
 	Reference Variables (Allias)
*/


#include<iostream>

int main(){
	
	int x = 10;

	int &y = x;

	std::cout << x << std::endl;	// 10

	std::cout << y << std::endl;	// 10

	std::cout << &x << std::endl;	
					
	std::cout << &y << std::endl;	
}

/*
 	10
	10
	0x7ffc20e73b6c
	0x7ffc20e73b6c
*/
