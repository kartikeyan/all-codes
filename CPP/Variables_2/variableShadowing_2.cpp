
/*
 	Variable Shadowing
		
		1] Local Variable Shadowing
		2] Global Variable Shadowing
*/

//	Local Variable Shadowing

#include<iostream>

int main(){
	
	int x = 10;

	std::cout << x << std::endl;	

	{
		int x = 20;

		std::cout << x << std::endl;	

		std::cout << ::x << std::endl;	// scope resolution(::) global variable asto tevhach upyog hoto

		x = 30;

		std::cout << x << std::endl;	

	}

	std::cout << x << std::endl;		
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Variables_2$ g++ variableShadowing_2.cpp
	variableShadowing_2.cpp: In function ‘int main()’:
	variableShadowing_2.cpp:22:32: error: ‘::x’ has not been declared
	   22 |                 std::cout << ::x << std::endl;
	      |  

*/
