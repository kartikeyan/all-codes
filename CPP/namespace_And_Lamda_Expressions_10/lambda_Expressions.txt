
	C++ 11 introduced lambda expressions to allow inline functions which can be used for short snippets of code that are not going to be reused and therefore do not require a name. In their simplest form a lambda expression can be defined as follows: 

	[ capture clause ] (parameters) -> return-type {   

		   definition of method   
	} 

	Generally, the return-type in lambda expressions is evaluated by the compiler itself and we don’t need to specify it explicitly. Also the -> return-type part can be ignored.  However, in some complex cases e.g. conditional statements, the compiler can’t determine the return type and explicit specification is required. 

	A lambda expression can have more power than an ordinary function by having access to variables from the enclosing scope. We can capture external variables from the enclosing scope in three ways : 

	Capture by reference 
	Capture by value 
      	Capture by both (mixed capture)

	Syntax used for capturing variables : 

	      [&] : capture all external variables by reference 
	      [=] : capture all external variables by value 
	      [a, &b] : capture a by value and b by reference

	A lambda with an empty capture clause [ ] can only access variables which are local to it. 
