
/*
 	1] Lambda Expression is an Annonymous Function

	2] Lambda Expression is used because

		a] for 1 time use

		b] if we have to send Object as a parameter

	3] Lambda Expression Parameters 

		a] Capture []

		b] function parameter ()

		c] return value ->    ==> optional

		d] function body

		e] Function object (functors) ==> optional
*/

#include<iostream>
#include<functional>

int main(){
	
	int a = 10;

	std::string name = "Kartik";

	// int(int,int) ===> indicates returntype and parameters
	std::function<int(int, int)> add = [&](int x, int y)-> int{

		std::cout << a << ":" << name << std::endl;

		return x + y;
	};

	std::cout << add(10,20) << std::endl;
}


/*
 	class for above code

	class Demo{
		
		int a;

		std::string name;

		Demo(int* a, const char* name){
			
			this->a = a;
			this->name = name;
		}

		int operator(int x, int y){
			
			return x + y;
		}
	}
*/

/*
 	10:Kartik
	30
*/
