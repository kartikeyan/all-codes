
/*
 	1] Lambda Expression is an Annonymous Function

	2] Lambda Expression is used because

		a] for 1 time use

		b] if we have to send Object as a parameter

	3] Lambda Expression Parameters 

		a] Capture []

		b] function parameter ()

		c] return value ->    ==> optional

		d] function body

		e] Function object (functors) ==> optional
*/

#include<iostream>

int main(){
	
	int a = 10;

	std::string name = "Kartik";

        std::cout << "Before Lambda " << a << std::endl;

	auto add = [&](auto x, auto y)mutable-> auto{
		
		++a;
		std::cout << a << ":" << name << std::endl;

		return x + y;
	};

	std::cout << add(10,20) << std::endl;

	std::cout << "After Lambda " << a << std::endl;
}

/*
 	Before Lambda 10
	11:Kartik
	30	
	After Lamda 11
*/


