
/*
 	1] Lambda Expression is an Annonymous Function

	2] Lambda Expression is used because

		a] for 1 time use

		b] if we have to send Object as a parameter

	3] Lambda Expression Parameters 

		a] Capture []

		b] function parameter ()

		c] return value ->    ==> optional

		d] function body

		e] Function object (functors) ==> optional
*/

#include<iostream>

int main(){
	
	[](int x, int y){
		
		std::cout << x+y << std::endl;

	}(10,20);	// to call lambda function we have to give open brackets
}

//	30
