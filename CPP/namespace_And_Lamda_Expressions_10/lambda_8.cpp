
/*
 	1] Lambda Expression is an Annonymous Function

	2] Lambda Expression is used because

		a] for 1 time use

		b] if we have to send Object as a parameter

	3] Lambda Expression Parameters 

		a] Capture []

		b] function parameter ()

		c] return value ->    ==> optional

		d] function body

		e] Function object (functors) ==> optional
*/

#include<iostream>

int main(){
	
	int a = 10;

	std::string name = "Kartik";

        std::cout << "Before Lambda " << a << std::endl;

	auto add = [=](int x, int y)-> int{
		
		++a;
		std::cout << a << ":" << name << std::endl;

		return x + y;
	};

	std::cout << add(10,20) << std::endl;

	std::cout << "After Lamda " << a << std::endl;
}

/*
 	lambda_8.cpp: In lambda function:
	lambda_8.cpp:36:19: error: increment of read-only variable ‘a’
	   36 |                 ++a;
	      |                   ^
*/
