
#include<iostream>

namespace company{
	
	int empCount = 1000;

	float revenue = 200.00f;
}

namespace company{
	
	void compInfo(){
		
		std::cout << empCount << ":" << revenue << std::endl;
	}
}

int main(){
	
	std::cout << company::empCount << std::endl;
       
	std::cout << company::revenue << std::endl;
       
	company::compInfo();
}

/*
 	1000
	200
	1000:200
*/
