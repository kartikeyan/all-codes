
#include<iostream>

namespace amazon{
	
	int empCount = 1000;

	void compInfo(){
		
		std::cout << "Amazon empCount = " << empCount << std::endl;
	}
}

namespace microsoft{
	
	int empCount = 2000;

	void compInfo(){
		
		std::cout << "Microsoft empCount = " << empCount << std::endl;
	}
}

int main(){
	
	amazon::compInfo();

	microsoft::compInfo();
}

/*
 	Amazon empCount = 1000
	Microsoft empCount = 2000
*/
