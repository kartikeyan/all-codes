
namespace amazon{

        int empCount = 1000;
	
	namespace projects{
		
		void cloud(){
			
			std::cout << "Amazon Cloud projects" << std::endl;
		}

		void app(){
			
			std::cout << "Amazon App projects" << std::endl;
		}

		void web(){

			std::cout << "Amazon Web projects" << std::endl;
		}
	}

        void compInfo(){

                std::cout << "Amazon empCount = " << empCount << std::endl;
        }
}

namespace microsoft{

        int empCount = 2000;

	namespace projects{
		
		void cloud(){
			
			std::cout << "Microsoft Cloud projects" << std::endl;
		}

		void app(){
			
			std::cout << "Microsoft App projects" << std::endl;
		}

		void web(){

			std::cout << "Microsoft Web projects" << std::endl;
		}
	}

        void compInfo(){

                std::cout << "Microsoft empCount = " << empCount << std::endl;
        }
}

