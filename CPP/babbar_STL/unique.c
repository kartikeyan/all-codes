
//	set madhe sagle unique elements store hotat

#include<iostream>
#include<map>
using namespace std;

int main(){
	
	map <int, string> m;

	m[1] = "babbar";
	m[23] = "love";
	m[3] = "kumar";

	for(auto i : m){
		
		cout << i.first << " " << i.second <<  endl;	// will be sorted output
	}

	cout << "finding 13 " << m.count(13) << endl;

	cout << "After erase " << endl;

	m.erase(23);
	
	for(auto i : m){
		cout << i.first << "  " << i.second << endl;
	}
}
