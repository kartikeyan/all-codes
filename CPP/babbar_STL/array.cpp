
#include<iostream>
#include<array>
using namespace std;

int main(){
	
	int basic[] = {1,2,3};

	int size = sizeof(basic) / sizeof(basic[0]);
	
	printf("Array 1\n");
	for(int i = 0; i < size; i++){
		
		cout << basic[i] << endl;
	}

	array < int, 4 > a = {1,2,3,4};

	int size1 = a.size();
	
	printf("Array 2\n");
	for(int i = 0; i < size1; i++){
		
		cout << a[i] << endl;
	}	

	cout << "Element at 2nd index is " << a.at(2) << endl;

	cout << "Empty or not " << a.empty() << endl;

	cout << "First Element " << a.front() << endl;

	cout << "End Element " << a.back() << endl;
}
