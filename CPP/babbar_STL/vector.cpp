
#include<iostream>
#include<vector>
using namespace std;

int main(){
	
	vector <int> v;

	cout << "Capacity " << v.capacity() << endl;

	v.push_back(1);
	cout << "Capacity " << v.capacity() << endl;

	v.push_back(2);
	cout << "Capacity " << v.capacity() << endl;

        v.push_back(3);
        cout << "Capacity " << v.capacity() << endl;	// how many elements the size has been assigned

	cout << "Size " << v.size() << endl;	// no of element in vector
	
	cout << "Element at 2nd index " << v.at(2) << endl;

	cout << "front " << v.front() << endl;

	cout << "back " << v.back() << endl;

	cout << "Before pop" << endl;
	for(int i : v){
		
		cout << i << " ";
	}

	v.pop_back();

	cout << "\nAfter pop" << endl;
	for(int i : v){
		
		cout << i << " ";
	}

	cout << "\n";

	cout << "before clear size " << v.size() << endl;

	v.clear();

	cout << "after clear size " << v.size() << endl;

	cout << "after clear capacity " << v.capacity() << endl;

	vector<int> a(5, 1);		// initialize size 5 of an array, and every element will be 1
	cout << "print a " << endl;
	for(int i : a){
		
		cout << i << endl;
	}

	vector <int> last(a);
	cout << "print a " << endl;
        for(int i : last){

                cout << i << endl;
        }
}
