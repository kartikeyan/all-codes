
#include<iostream>
#include<list>
using namespace std;

int main(){
	
	list<int> l;

	l.push_back(1);

	l.push_front(2);

	for(int i : l){
		
		cout << i << "\n";
	}

	l.erase(l.begin());

	cout << "after erase " << endl;
	for(int i : l){
		
		cout << i << "\n";
	}

	cout << "size of list " << l.size() << endl;

	// copying list in another list
	list<int> n(5,100);		// 5 is sizeof list and initialize everyone with 100
	
	for(int i : n){
		
		cout << i << "\n";
	}
}
