
//	set madhe sagle unique elements store hotat

#include<iostream>
#include<set>
using namespace std;

int main(){
	
	set <int> s;

	s.insert(5);
	s.insert(5);
	s.insert(1);
	s.insert(2);
	s.insert(2);
	s.insert(3);
	s.insert(3);
	s.insert(3);	// set insertion T.C = O(log n)

	for(auto i : s){
		
		cout << i << endl;
	}
	
	cout << endl;

	s.erase(s.begin());	// deletes first ele	

	for(auto i : s){

                cout << i << endl;
        }
	
	set<int> ::iterator it = s.begin();
	it++;

	s.erase(it);

	cout << endl;

	for(auto i : s){

                cout << i << endl;
        }

	cout << "5 is present or not " << s.count(5) << endl;

	set<int>::iterator itr = s.find(5);

	cout << "value present at itr " << *it << endl;

}
