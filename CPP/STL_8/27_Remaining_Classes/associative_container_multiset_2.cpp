
//	multiset allows duplicate elements

#include<iostream>
#include<set>

int main(){
	
	std::multiset<int> s = {30,20,40,10,20,10,40,30};

	std::multiset<int>::iterator itr;

	for(itr = s.begin(); itr != s.end(); itr++){
		
		std::cout << *itr << std::endl;
	}
}

/*
 	10
	10
	20
	20
	30
	30
	40
	40
*/
