
//	Set : Duplicate Elements chalat nahi

#include<iostream>
#include<set>

int main(){
	
	std::set<int> s = {10,30,40,10,20,30,50};

	std::set<int>::reverse_iterator itr;
/*
	for(itr = s.begin(); itr != s.end(); itr++){
		
		std::cout << *itr << std::endl;
	}
*/
/*
	for(itr = s.rbegin(); itr != s.rend(); itr++){

                std::cout << *itr << std::endl;
        }
*/
	// sorts order in descending order
	std::set<int, std::greater<int>> s2 = {10,30,40,10,20,30,40};

	std::set<int>::iterator itr2;

	for(itr2 = s2.begin(); itr2 != s2.end(); itr2++){

                std::cout << *itr2 << std::endl;
        }
}

/*
 	10
	20
	30
	40
	50
*/

/*
 	50
	40
	30
	20
	10
*/

/*
 	40
	30
	20
	10
*/
