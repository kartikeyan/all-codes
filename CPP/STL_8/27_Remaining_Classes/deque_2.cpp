#include<iostream>
#include<deque>

int main(){
	
	std::deque<int> dq = {40,10,20,30};

	for(int& data : dq){
		
		std::cout << data << std::endl;
	}

	std::deque<int>::iterator itr;

	for(itr = dq.begin(); itr != dq.end(); itr++){
		
		std::cout << *itr << std::endl;
	}
}

/*
 	40
	10
	20
	30
	40
	10
	20
	30
*/
