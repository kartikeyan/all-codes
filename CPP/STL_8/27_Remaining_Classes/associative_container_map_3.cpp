
#include<iostream>
#include<map>

int main(){
	
	// way - 1	
	std::map<int, std::string> player;

	player[18] = "Virat";
	player[45] = "Rohit";
	player[18] = "Virat-Dup";
	player[7] = "MSD";

	std::map<int, std::string>::iterator itr;

	for(itr = player.begin(); itr != player.end(); itr++){
	
		std::cout << itr->first << ":";

		std::cout << (*itr).second << std::endl;
	}

	// way - 2
	std::map<std::string, std::string> employee = {{"Onkar", "TIAA"},{"Rahul", "JPMorgan"}};
	
	std::map<std::string, std::string>::iterator itr1;


	for(itr1 = employee.begin(); itr1 != employee.end(); itr1++){

                std::cout << itr1->first << ":";

                std::cout << (*itr1).second << std::endl;
        }

	// way - 3
	std::map<int, std::string> color;

	std::map<int, std::string>::iterator itr2;

	color.insert(std::pair<int, std::string> {1, "Red"});
	
	color.insert(std::pair<int, std::string> {2, "Orange"});
	
	color.insert(std::pair<int, std::string> {3, "Red"});
	
	for(itr2 = color.begin(); itr2 != color.end(); itr2++){

                std::cout << itr2->first << ":";

                std::cout << (*itr2).second << std::endl;
        }
}

/*
 	7:MSD
	18:Virat-Dup
	45:Rohit
*/

/*
 	Onkar:TIAA
	Rahul:JPMorgan
*/

/*
 	1:Red
	2:Orange
	3:Red
*/
