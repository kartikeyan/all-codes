//	queue has no iterators because it does not follow queue rule
#include<iostream>
#include<queue>

void showingQueue(std::priority_queue<int>& obj){
	
	int k = obj.size();

	for(int i = 0; i < k; i++){
		
		std::cout << obj.top() << std::endl;

		obj.pop();
	}
}

int main(){
	
	std::priority_queue<int> q;

	q.push(30);
	q.push(20);
	q.push(40);
	q.push(10);

	showingQueue(q);
}

/*
 	30
	20
	40
	10
*/
