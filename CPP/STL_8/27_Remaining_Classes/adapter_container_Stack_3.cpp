//	queue has no iterators because it does not follow queue rule

#include<iostream>
#include<stack>

void showingQueue(std::stack<int>& obj){
	
	int k = obj.size();

	for(int i = 0; i < k; i++){
		
		std::cout << obj.top() << std::endl;

		obj.pop();
	}
}

int main(){
	
	std::stack<int> s;

	s.push(30);
	s.push(20);
	s.push(40);
	s.push(10);

	showingQueue(s);
}

/*
 	10
	40
	20
	30
*/
