
#include<iostream>
#include<unordered_map>

int main (){

  	std::unordered_map<std::string,std::string> mymap;

  	mymap["Bakery"]="Barbara";  // new element inserted
  	mymap["Seafood"]="Lisa";    // new element inserted
  	mymap["Produce"]="John";    // new element inserted
	mymap["Bakery"] = "Barbara-dup";	

	for (auto& x: mymap) {
    		std::cout << x.first << ": " << x.second << std::endl;
  	}
}

/*
 	Produce: John
	Seafood: Lisa
	Bakery: Barbara-dup
*/
