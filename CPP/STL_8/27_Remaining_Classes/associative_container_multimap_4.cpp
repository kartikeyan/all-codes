
#include<iostream>
#include<map>

int main(){

	// way - 1
	std::multimap<std::string, std::string> employee = {{"Onkar", "TIAA"},{"Rahul", "JPMorgan"}, {"Onkar", "TIAA"}, {"Shree","Euclid"},{"Harshal","XXX"}};
	
	std::multimap<std::string, std::string>::iterator itr1;


	for(itr1 = employee.begin(); itr1 != employee.end(); itr1++){

                std::cout << itr1->first << ":";

                std::cout << (*itr1).second << std::endl;
        }

	// way - 2
	std::multimap<int, std::string> color;

	std::multimap<int, std::string>::iterator itr2;

	color.insert(std::pair<int, std::string> {1, "Red"});
	
	color.insert(std::pair<int, std::string> {1, "Red"});
	
	color.insert(std::pair<int, std::string> {3, "Red"});
	
	color.insert(std::pair<int, std::string> {1, "Orange"});
	
	for(itr2 = color.begin(); itr2 != color.end(); itr2++){

                std::cout << itr2->first << ":";

                std::cout << (*itr2).second << std::endl;
        }
}

/*
 	Harshal:"XXX"
	Onkar:TIAA
	Onkar:TIAA
	Rahul:JPMorgan
	Shree:Euclid
	1:Red
	1:Red
	1:Orange
	3:Red
*/


