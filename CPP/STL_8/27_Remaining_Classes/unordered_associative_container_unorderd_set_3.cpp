
#include<iostream>
#include<unordered_set>

int main (){

	std::unordered_set<std::string> myset = {"yellow","green","blue"};

	for (const std::string& x: myset) std::cout << " " << x << std::endl;
}

/*
	blue
	green
	yellow
*/
