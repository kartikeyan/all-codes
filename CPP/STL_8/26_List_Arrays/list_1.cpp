
#include<iostream>
#include<list>

int main(){
	
	std::list<int> lst (5, 100);	// {100,100,100,100,100};
	
	std::list<int>::iterator itr;

	for(itr = lst.begin(); itr != lst.end(); itr++){
		
		std::cout << *itr << std::endl;
	}
}
