
#include<iostream>
#include<list>

int main(){
	
	std::list<int> lst = {100,200,300,400,500};
	
	std::list<int>::iterator itr;

	for(itr = lst.begin(); itr != lst.end(); itr++){
		
		std::cout << *itr << std::endl;
	}

//	1 - Capacity
	
	// 1 - empty
	
	std::cout << "Is empty "<< lst.empty() << std::endl;	// 0
	// 2 - size
        std::cout << "Size is "<< lst.size() << std::endl;    // 5
	
	// 3 - max_size
	std::cout << "Max_Size is "<< lst.max_size() << std::endl;    // 384307168202282325


//	2 - Element access
	// 1 - front
	std::cout << "Front is "<< lst.front() << std::endl;    // 100
	// 2 - back
	std::cout << "Back is "<< lst.back() << std::endl;    // 500

//	3 - Modifiers

	// 1 - assign
	
	std::list<int> first;
	first.assign(4, 100);	// 4 ints with value 100

	std::cout << "Size is "<< first.size() << std::endl;    // 4
								
	// 2 - emplace_front
	std::cout << "Front is "<< first.emplace_front(200) << std::endl;    // 200

	// 3 - emplace_back
	std::cout << "Back is "<< first.emplace_back(300) << std::endl;    // 300

	// 4 - push_front
	first.push_front(400);
	
	std::list<int>::iterator itr1;

        for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;	// 400 200 100 100 100 100 300
        }


	// 5 - pop_front
	first.pop_front();
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 200 100 100 100 100 300
        }

	
	// 6 - push_back
  	first.push_back(500);
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 200 100 100 100 100 300 500
        }
	
	// 7 - pop_back 
    	first.pop_back();
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 200 100 100 100 100 300
        }
	
	// 8 - emplace
	first.emplace(first.begin(), 500);
        for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 500 200 100 100 100 100 300
        }
	
	// 9 - insert

	first.insert(itr1, 20);
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 500 200 100 100 100 100 300 20
        }

	// 10 - erase
	itr1 = first.begin();
	first.erase(itr1);	
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 200 100 100 100 100 300 20
        }
	
	// 11 - swap
	
  	std::list<int> third (3,100);   // three ints with a value of 100
  	std::list<int> second (5,200);  // five ints with a value of 200

  	third.swap(second);
	
	std::list<int>::iterator itr2;

  	std::cout << "third contains:";
  	for (std::list<int>::iterator it=third.begin(); it!=third.end(); it++)
    		std::cout << ' ' << *it;
  	std::cout << '\n';

  	std::cout << "second contains:";
  	for (std::list<int>::iterator it=second.begin(); it!=second.end(); it++)
    		std::cout << ' ' << *it;
	std::cout << '\n';

	// 12 - resize
	first.resize(5);
	std::cout << "Size is " << first.size() << std::endl;	// 5	

	// 13 - clear
//	first.clear();

//	4 - Operations
	
	// 1 - splice

	// 2 - remove
	first.remove(200);
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 100 100 100 100
        }

	first.push_back(20);
	first.push_back(30);
	first.push_back(40);
	first.push_back(50);


	// 3 - remove_if
	struct triple_digit{
		bool operator() (const int& value) { return (value > 100);}
	};

	first.remove_if(triple_digit());	
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 20 30 40 50
        }

	// 4 - unique
/*
	bool compare(int a, int b){
		
		return (a == b);
	}

	first.unique(compare);
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 20 30 40 50
        }	
*/
	// 5 - merge
	first.merge(second);
	std::cout << "Merge Start" << std::endl;
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 100 100 100 100 20 30 40 50 100 100 100

        }
	std::cout << "Merge End" << std::endl;

	// 6 - sort
	first.sort();
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;        // 20 30 40 50 100 100 100 100 100 100 100
        }

	// 7 - reverse
	first.reverse();
	for(itr1 = first.begin(); itr1 != first.end(); itr1++){

                std::cout << *itr1 << std::endl;       // 100 100 100 100 100 100 100 50 40 30 20
        }
}
