
#include<iostream>
#include<array>

int main(){
	
	std::array<int, 4> arr = {10,20,30,40};		// size should be compulsory given

	for(int i = 0; i < arr.size(); i++){
		
		std::cout << arr[i] << std::endl;
	}
}
