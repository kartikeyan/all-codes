
#include<iostream>
#include<array>

int main(){
	
	std::array<int, 4> arr = {10,20,30,40};		// size should be compulsory given

	for(int i = 0; i < arr.size(); i++){
		
		std::cout << arr[i] << std::endl;
	}

//	1 - Capacity
	
	// 1 - size
	std::cout << arr.size() << std::endl;	// 4

	// 2 - max_size
        std::cout << arr.max_size() << std::endl;	// 4
	
	// 3 - empty
	std::cout << arr.empty() << std::endl;	// 0


//	2 - Element access
	
	// 1 - operator[]
	for(int i = 0; i < arr.size(); i++){

                std::cout << arr[i] << std::endl;
        }

	// 2 - at
	for(int i = 0; i < arr.size(); i++){
		
		arr.at(i) = arr[i] + 1;
                std::cout << arr[i] << std::endl;
        }
	
	// 3 - front
	std::cout << arr.front() << std::endl;       // 11

	// 4 - back
        td::cout << arr.back() << std::endl;       // 41

	// 5 - data
        std::cout << arr.data() << std::endl;       // 0x7ffc98a68d50
	
//	3 - Modifiers
	
	// 1 - fill
	std::array<int, 4> myarr;

	myarr.fill(5);
	for(int i = 0; i < myarr.size(); i++){

                std::cout << myarr[i] << std::endl;
        }

	// 2 - swap
	arr.swap(myarr);
	std::cout << "start arr" << std::endl;
	for(int i = 0; i < arr.size(); i++){

                std::cout << arr[i] << std::endl;
        }
        std::cout << " end arr " << std::endl;

	std::cout << "start myarr" << std::endl;
	for(int i = 0; i < myarr.size(); i++){

                std::cout << myarr[i] << std::endl;
        }	
	std::cout << "end myarr" << std::endl;
}
