
#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {10,20,30,40,50};

//	std::vector<int>::iterator itr;

	for(auto itr = v.begin(); itr != v.end(); itr++){
		
		*itr = 100;		
		std::cout << *itr << std::endl;
	}
}

/*
 	100
	100
	100
	100
	100
*/
