
#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {10,20,30,40,50};

//	std::vector<int>::iterator itr;

	for(auto itr = v.cbegin(); itr != v.cend(); itr++){
		
		*itr = 100;		
		std::cout << *itr << std::endl;		// only can print and cannot change data
	}
}

/*
 	Constant_Iterator_In_vector_4.cpp: In function ‘int main()’:
Constant_Iterator_In_vector_4.cpp:13:22: error: assignment of read-only location ‘itr.__gnu_cxx::__normal_iterator<const int*, std::vector<int> >::operator*()’
   13 |                 *itr = 100;
      |                 ~~~~~^~~~~
*/
