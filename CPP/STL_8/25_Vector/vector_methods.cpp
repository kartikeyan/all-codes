
#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {10,20,30,40,50};

	std::vector<int>::iterator itr;

	for(itr = v.begin(); itr != v.end(); itr++){
		
		std::cout << *itr << std::endl;
	}

//	1 - Capacity

	// 1 - size()
	std::cout << "Size is " << v.size() << std::endl;	// 5


	// 2 - max_size()
        std::cout << "Max_Size is " << v.max_size() << std::endl;	// 2305843009213693951

	// 3 - resize()
	
	v.resize(10);
        std::cout << "ReSize is " << v.size() << std::endl;       // 10

	// 4 - capacity()
        std::cout << "Capacity is " << v.capacity() << std::endl;       // 10
	
	// 5 - empty()
        std::cout << "Is Empty " << v.empty() << std::endl;       // 0
	
	// 6 - clear()
        //v.clear();

	// 5 - empty()
        std::cout << "Is Empty " << v.empty() << std::endl;       // 1 - if clear() executed
	
	// 7 - reserve()
	
	v.reserve(100);
        std::cout << "Capacity is " << v.capacity() << std::endl;       // 100
	
	// 8 - shrink_to_fit()
	
	v.shrink_to_fit();
	std::cout << "Capacity is " << v.size() << std::endl;       // 10



//	2 - Element access

	
	// 1 - operator[]
	for(int i = 0; i < v.size(); i++){
		
		std::cout << v[i] << std::endl;		// 10 20 30 40 50
	}

	// 2 - at()
       	for(int i = 0; i < v.size(); i++){
		
		v.at(i) = 100 + i;
                std::cout << v[i] << std::endl;         // 100 101 102 103 104
        }
	
	// 3 - front()
        std::cout << "Front is " << v.front() << std::endl;	// 100
	
	// 4 - back()
        std::cout << "Back is " << v.back() << std::endl;	// 104
	
	// 5 - data()
	
	int *k = v.data();

	*k = 10;
	++k;
	*k = 20;

        for(int i = 0; i < v.size(); i++){

                std::cout << v[i] << std::endl;         // 10 20 103 104 105
        }


//	3 - Modifiers
	
	// 1 - assign()
	
	std::vector<int> first;

	first.assign (3,100);             // 3 ints with a value of 100
	
	for(int i = 0; i < first.size(); i++){

                std::cout << first[i] << std::endl;         // 100 100 100
        }

	std::cout << first.size() << std::endl;		// 3

	// 2 - push_back
	first.push_back(200);
	for(int i = 0; i < first.size(); i++){

                std::cout << first[i] << std::endl;         // 100 100 100 200
        }

	// 3 - pop_back
        first.pop_back();

        for(int i = 0; i < first.size(); i++){

                std::cout << first[i] << std::endl;         // 100 100 100 
        }

	// 4 - insert
        
	std::vector<int>::iterator itr;

	first.insert(first.begin() + 2, 200);

        for(int i = 0; i < first.size(); i++){

                std::cout << first[i] << std::endl;         // 100 100 200 100 
        }

	// 5 - erase()
	first.erase(first.begin() + 3);	  // erase the 3th element

	for(int i = 0; i < first.size(); i++){

                std::cout << first[i] << std::endl;         // 100 100 200 
        }
	
	// 6 - clear()
	first.clear();
  	for(int i = 0; i < first.size(); i++){
			
               std::cout << first[i] << std::endl;        
        }

	std::cout << "Size is " << first.size() << std::endl;	// 0

	// 7 - emplace
	first.emplace(first.begin(), 400);
	first.emplace(first.end(), 500);

	for(int i = 0; i < first.size(); i++){

               std::cout << first[i] << std::endl;	// 400 500
        }
	
	// 8 - emplace_back
	first.emplace_back(600);
        first.emplace_back(700);

        for(int i = 0; i < first.size(); i++){

               std::cout << first[i] << std::endl;      // 400 500 600 700
        }
	
	// 9 - swap
	std::vector<int> foo (3,100);   // three ints with a value of 100
  	std::vector<int> bar (5,200);   // five ints with a value of 200

  	foo.swap(bar);

  	std::cout << "foo contains:";
  	for (int i=0; i<foo.size(); i++)
    		std::cout << ' ' << foo[i];
  	std::cout << '\n';

  	std::cout << "bar contains:";
  	for (int i=0; i<bar.size(); i++)
    		std::cout << ' ' << bar[i];
  	std::cout << '\n';
}
