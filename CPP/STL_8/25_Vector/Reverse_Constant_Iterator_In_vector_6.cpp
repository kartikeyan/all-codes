
#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {10,20,30,40,50};

//	std::vector<int>::iterator itr;

	for(auto itr = v.crbegin(); itr != v.crend(); itr++){	// iterates reverse
		
		*itr = 100;		
		std::cout << *itr << std::endl;		// can print and cannot change data
	}
}

/*
 	Reverse_Constant_Iterator_In_vector_6.cpp: In function ‘int main()’:
Reverse_Constant_Iterator_In_vector_6.cpp:13:22: error: assignment of read-only location ‘itr.std::reverse_iterator<__gnu_cxx::__normal_iterator<const int*, std::vector<int> > >::operator*()’
   13 |                 *itr = 100;
      |                 ~~~~~^~~~~
*/
