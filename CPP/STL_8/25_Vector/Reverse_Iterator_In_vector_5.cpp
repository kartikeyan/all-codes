
#include<iostream>
#include<vector>

int main(){
	
	std::vector<int> v = {10,20,30,40,50};

//	std::vector<int>::iterator itr;

	for(auto itr = v.rbegin(); itr != v.rend(); itr++){	// iterates reverse
		
		*itr = 100;		
		std::cout << *itr << std::endl;		// can print and can change data
	}
}

/*
 	100
	100
	100
	100
	100
*/
