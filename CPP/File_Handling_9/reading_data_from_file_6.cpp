
#include<iostream>
#include<fstream>

int main(){
	
	std::ifstream infile("Incubator.txt");

	std::string inputData;
	
	// Execute a loop until EOF (End of File)
	while(infile){
		
		getline(infile, inputData);

		std::cout << inputData << std::endl;
	}

	infile.close();
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/File_Handling_8$ ./a.out 
	Flutter
	Frontend
	Backend
	Salesforce

*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
