
#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile("c2w.txt");

	std::string inputData;

	getline(std::cin, inputData);

	outfile << inputData;

	outfile.close();
}

/*
 	output : Created new file c2w.txt and we took input from user 
		 C CPP Java
		 
		 and in file "c2w.txt" we inserted C CPP Java 
*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
