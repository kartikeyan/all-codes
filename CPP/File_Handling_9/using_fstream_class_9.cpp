
#include<iostream>
#include<fstream>

int main(){
	
	std::fstream inout("Incubator.txt");

	std::string str;

	while(inout){
		
		getline(inout, str);

		std::cout << str << std::endl;
	}

	inout.close();
}

/*
 	Flutter
	Frontend
	Backend
	Salesforce
*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
