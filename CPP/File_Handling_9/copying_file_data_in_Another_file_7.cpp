
#include<iostream>
#include<fstream>

int main(){
	
	std::ifstream infile("Incubator.txt");
	
	std::ofstream outfile("Biencaps.txt");

	std::string inputData;
	
	// Execute a loop until EOF (End of File)
	while(infile){
		
		getline(infile, inputData);

		outfile << inputData;
	}

	infile.close();
}

/*
 	we copied contents from Incubator.txt from Biencaps.txt

	and now in Biencaps.txt file we have data as

	FlutterFrontendBackendSalesforce
*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
