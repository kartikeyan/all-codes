
//	Writing in file using ofstream constructor

#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile("Incubator.txt");

	outfile << "Flutter" << std::endl;	// outfile << is overloaded

	outfile << "Frontend\n";

	outfile << "Backend\n";

	outfile.close();
}

/*
 	output : Created new file Incubator.txt and in this file we wrote 
		 Flutter
		 Frontend
		 Backend
*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
