
//	Writing in file using open() function

#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile;

	outfile.open("kartik.txt");

	outfile << "Flutter" << std::endl;	// outfile << is overloaded

	outfile << "Frontend\n";

	outfile << "Backend\n";

	outfile.close();
}

/*
 	output : Created new file kartik.txt and in this file we wrote 
		 Flutter
		 Frontend
		 Backend
*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
