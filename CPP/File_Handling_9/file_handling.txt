

	ios class("input output stream")


		Child - 1 ==> istream

				istream functions ==> get, getline, read

		Child - 2 ==> ostream

				ostream functions ==> put, write

	--------------------------------------------------------

	fstreambase class (open(), close() functions)

		Child - 1 ==> fstream (both input,output)

				fstream parent ==> iostream

				iostream parents ==> 1] istream 2] ostream

		Child - 2 ==> ifstream(only input)

				ifstream parent ==> istream

							istream parent ==> ios

				ifstream functions ==> get, getlime, read
		
		Child - 3 ==> ofstream(only output)

				ofstream parent ==> ostream

							ostream parent ==> ios

				ostream functions ==> put, write
