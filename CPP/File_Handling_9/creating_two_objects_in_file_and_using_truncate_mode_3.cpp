
#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile("Incubator.txt");

	outfile << "Flutter" << std::endl;	// outfile << is overloaded

	outfile << "Frontend\n";

	outfile << "Backend\n";
	
	// trunc == > truncate ==> Any contents that existed in the file before it is open are discarded.

	std::ofstream outfile1("Incubator.txt", std::ios::trunc);

	outfile1 << "salesforce\n";

	outfile.close();
}

/*
 	output : Created new file Incubator.txt and in this file we have 
		 Flutter
		 Frontend
		 Backend

		 After this we have created new object and are updating in Incubator.txt file
		 using truncate mode

		 Therefore output is 
		 salesforce
		 ntend		
		 Backend

		 Frontend is changed because there is change in the difference of length in salesforce = 10  and frontend = 8, difference is 2 and therefore we have output ==> ntend 
*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
