
#include<iostream>
#include<fstream>

int main(){
	
	std::ofstream outfile("Incubator.txt");

	outfile << "Flutter" << std::endl;	// outfile << is overloaded

	outfile << "Frontend\n";

	outfile << "Backend\n";
	
	// trunc == > truncate ==> Any contents that existed in the file before it is open are discarded.

	std::ofstream outfile1("Incubator.txt");	// here we are not using any mode, so by default it is truncate mode

	outfile1 << "salesforce\n";

	outfile.close();
}

/*
 	output : Created new file Incubator.txt and in this file we wrote 
		 Flutter
		 Frontend
		 Backend

		 After this we have created new object and are updating in Incubator.txt file
		 but in a file we can only use 1 object to update delete, etc

		 We can create objects but it will not do any operations in the file.

		 Therefore output is 
		 Flutter
                 Frontend
                 Backend

		 To change output you can use append mode

*/


/*
 	outfile << is overloaded as

	operator << (outfile, string)

		write(outfile, string);

	}

	class ostream{
		
		ostream(String str){
			
			operator << (outfile, string){
				
				write(outfile, string);
			}
		}
	}

*/
