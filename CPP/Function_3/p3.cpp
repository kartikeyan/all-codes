
#include<iostream>

int main(){
	
	int x = 10;
	int y = 20;

	std::cout << x << std::endl;	// 10

	std::cout << y << std::endl;	// 20

	int const *ptr = &x;		// ptr is not constant

	std::cout << *ptr << std::endl;	// 10
	
	x = 50;

	std::cout << x << std::endl;	// 50

	ptr = &y;

	std::cout << *ptr << std::endl;	// 20
}
