
/*
 	Inline Functions in C++

	C++ provides inline functions to reduce the function call overhead.
       	An inline function is a function that is expanded in line when it is called. 
	When the inline function is called whole code of the inline function gets inserted or substituted at the point of the inline function call. 
	This substitution is performed by the C++ compiler at compile time. 
	An inline function may increase efficiency if it is small.

	Syntax:

	inline return-type function-name(parameters)
	{
	    // function code
	}

	Remember, inlining is only a request to the compiler, not a command. 
	The compiler can ignore the request for inlining.

	The compiler may not perform inlining in such circumstances as:

	    If a function contains a loop. (for, while and do-while)
	    If a function contains static variables.
	    If a function is recursive.
	    If a function return type is other than void, and the return statement doesn’t exist in a function body.
	    If a function contains a switch or goto statement.
*/

#include<iostream>

inline void sum(int x, int y){
	
	std::cout << x + y << std::endl;
}

int main(){
	
	sum(10,20);
}
