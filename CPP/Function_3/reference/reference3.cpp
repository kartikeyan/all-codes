
#include<iostream>

int main(){
	
	int x = 10;
	
	// ref is reference to x
	int &ref;

	std::cout << x << std::endl;

	std::cout << ref << std::endl;

	std::cout << &x << std::endl;

	std::cout << &ref << std::endl;
}

/*
	reference3.cpp: In function ‘int main()’:
	reference3.cpp:9:14: error: ‘ref’ declared as reference but not initialized
	    9 |         int &ref;
	      |              ^~~
*/
