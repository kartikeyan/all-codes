
#include<iostream>

int* fun(int val){
	
	val = val + 30;

	std::cout << val << std::endl;

	return &val;
}

int main(){
	
	int x = 10;

	int *ret = fun(x);

	std::cout << *ret << std::endl;
}

/*
	p2.cpp: In function ‘int* fun(int)’:
p2.cpp:10:16: warning: address of local variable ‘val’ returned [-Wreturn-local-addr]
   10 |         return &val;
      |                ^~~~
p2.cpp:4:14: note: declared here
    4 | int* fun(int val){
      |          ~~~~^~~
*/

/*
 	40
	Segmentation fault (core dumped)
*/
