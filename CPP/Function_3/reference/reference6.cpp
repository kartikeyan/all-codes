
#include<iostream>

void fun(int &ref1, int &ref2){
	
	int temp = ref1;
	ref1 = ref2;
	ref2 = temp;
}

void fun(int x, int y){
	
	int temp = x;
	x = y;
	y = temp;
}

int main(){
	
	int x = 10;
	
	int y = 20;

	std::cout << x << " " << y << std::endl;

	fun(x, y);

	std::cout << x << " " << y << std::endl;
}

/*
	reference6.cpp: In function ‘int main()’:
	reference6.cpp:26:12: error: call of overloaded ‘fun(int&, int&)’ is ambiguous
	   26 |         fun(x, y);
	      |         ~~~^~~~~~
	reference6.cpp:4:6: note: candidate: ‘void fun(int&, int&)’
	    4 | void fun(int &ref1, int &ref2){
	      |      ^~~
	reference6.cpp:11:6: note: candidate: ‘void fun(int, int)’
	   11 | void fun(int x, int y){
	      |      ^~~
*/


