
#include<iostream>

int main(){
	
	int x = 10;
	
	// ref is reference to x
	int &ref = x;

	std::cout << x << std::endl;

	std::cout << ref << std::endl;

	std::cout << &x << std::endl;

	std::cout << &ref << std::endl;
}

/*
 	10
	10
	0x7ffe80be829c
	0x7ffe80be829c
*/
