
#include<iostream>

void fun(int *val){
	
	*val = 30;

	std::cout << val << std::endl;
}

int main(){
	
	int x = 10;

	std::cout << x << std::endl;

	fun(x);

	std::cout << x << std::endl;
}

/*
	p1.cpp: In function ‘int main()’:
	p1.cpp:17:13: error: invalid conversion from ‘int’ to ‘int*’ [-fpermissive]
	   17 |         fun(x);
	      |             ^
	      |             |
	      |             int
	p1.cpp:4:15: note:   initializing argument 1 of ‘void fun(int*)’
	    4 | void fun(int *val){
	      |          ~~~~~^~~
*/
