
#include<iostream>

int main(){
	
	int x = 10;

	int &y = x;		// internally &y = &x (address of x jato)
	
	int *ptr = &x;

	std::cout << x << std::endl;	// 10

	std::cout << y << std::endl;	// 10	// internally compiler y chya jagi *y (valueAt(y))

	std::cout << ptr << std::endl;	// 100 Address

	std::cout << sizeof(x) << std::endl;	// 4

	std::cout << sizeof(y) << std::endl;	// 4	// internally y chya jagi *y (valueAt(y))

	std::cout << sizeof(ptr) << std::endl;	// 8

	std::cout << &x << std::endl;	// 100 Address

	std::cout << &y << std::endl;	// 100 Address	// internally y chya jagi *y (valueAt(y))

	std::cout << &ptr << std::endl; // 200 Address
}

/*
 	10
	10
	0x7ffda0f09734
	4
	4
	8
	0x7ffda0f09734
	0x7ffda0f09734
	0x7ffda0f09738
*/

/*
 	Internally Reference Pointer Asto

	int &ptr = x; 

	compiler internally int *ptr = &x karto
*/


