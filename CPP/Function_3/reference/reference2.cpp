
#include<iostream>

int main(){
	
	int x = 10;
	
	// ref is reference to x
	int &ref = 20;

	std::cout << x << std::endl;

	std::cout << ref << std::endl;

	std::cout << &x << std::endl;

	std::cout << &ref << std::endl;
}

/*
	reference2.cpp: In function ‘int main()’:
	reference2.cpp:9:20: error: cannot bind non-const lvalue reference of type ‘int&’ to an rvalue of type ‘int’
	    9 |         int &ref = 20;
	      |                    ^~
*/
