
#include<iostream>

int& fun(int x){
	
	int y = x;

	return y;
}

int main(){
	
	int a = 50;

	int ret = fun(a);

	std::cout << ret << std::endl;
}

/*
 	reference7.cpp: In function ‘int& fun(int)’:
	reference7.cpp:8:16: warning: reference to local variable ‘y’ returned [-Wreturn-local-addr]
	    8 |         return y;
	      |                ^
	reference7.cpp:6:13: note: declared here
	    6 |         int y = x;
	      |             ^
	
	Segmentation fault (core dumped)
*/
