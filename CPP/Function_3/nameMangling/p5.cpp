
#include<iostream>

void fun(int x, int y){
	
	std::cout << "int-int " << x << y << std::endl;			
}

void fun(int x, float y){
	
	std::cout << "int-float " << x << y << std::endl;	
}

void fun(float x, float y){
	
	std::cout << "float-float " << x << y << std::endl;	
}
	
int main(){
	
//	fun(10,20);
//	fun(10,20.5f);
//	fun(10.5f,20.5f);
	fun(10.5,20.5);			// toh kutlya pan function madhe jau shakto tyamule error ahe
}	

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ g++ p5.cpp 
	p5.cpp: In function ‘int main()’:
	p5.cpp:24:12: error: call of overloaded ‘fun(double, double)’ is ambiguous
	   24 |         fun(10.5,20.5);
	      |         ~~~^~~~~~~~~~~
	p5.cpp:4:6: note: candidate: ‘void fun(int, int)’
	    4 | void fun(int x, int y){
	      |      ^~~
	p5.cpp:9:6: note: candidate: ‘void fun(int, float)’
	    9 | void fun(int x, float y){
	      |      ^~~
	p5.cpp:14:6: note: candidate: ‘void fun(float, float)’
	   14 | void fun(float x, float y){
	      |      ^~~

*/
