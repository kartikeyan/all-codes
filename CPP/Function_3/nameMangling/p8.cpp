
#include<iostream>

void fun(int x, int y){
	
	std::cout << "int-int " << x << y << std::endl;		
}

/*
void fun(int x, float y){
	
	std::cout << "int-float " << x << y << std::endl;	
}
*/

void fun(float x, float y){
	
	std::cout << "float-float " << x << y << std::endl;	
}
	
int main(){
	
//	fun(10,20);
	fun(10,20.5f);			
//	fun(10.5f,20.5f);
//	fun(10.5,20.5);
}

/*
 	function chya paramters check karto , ani ek parameter equal bhetla tar dusrya functions chya parameter check karto ani equal nasel tar output deto, ani jar doni function madhe ek paramter equal bhetla tar errot deto.
*/	

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ g++ p8.cpp 
p8.cpp: In function ‘int main()’:
p8.cpp:25:12: error: call of overloaded ‘fun(int, float)’ is ambiguous
   25 |         fun(10,20.5f);
      |         ~~~^~~~~~~~~~
p8.cpp:5:6: note: candidate: ‘void fun(int, int)’
    5 | void fun(int x, int y){
      |      ^~~
p8.cpp:17:6: note: candidate: ‘void fun(float, float)’
   17 | void fun(float x, float y){
      |      ^~~

*/
