
#include<iostream>

/*
void fun(int x, int y){
	
	std::cout << "int-int " << x << y << std::endl;		
}
*/

/*
void fun(int x, float y){
	
	std::cout << "int-float " << x << y << std::endl;	
}
*/


void fun(float x, float y){
	
	std::cout << "float-float " << x << y << std::endl;	// float-float 1020.5	
}


int main(){
	
//	fun(10,20);
	fun(10,20.5f);			
//	fun(10.5f,20.5f);
//	fun(10.5,20.5);
}


