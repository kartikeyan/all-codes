
#include<iostream>

int add(int x, int y){		// internally addii / add2i jato
	
	return x + y;
}

int add(int x, int y, int z){	// internally addiii / add3i jato
	
	return x + y + z;
}

int main(){
	
	std::cout << add(10,20) << std::endl;		// 30

	std::cout << add(10,20,30) << std::endl;	// 60
}

//	Assembly Code can be generated as g++ -S filename

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ vim p1.s
	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ ls
	a.out  nameMangling.txt  p1.cpp  p1.s
	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ g++ -S p1.cpp 
	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ vim p1.s
	katikeyan@kartikeyan:~/all-codes/CPP/Function_3/nameMangling$ 

        .file   "p1.cpp"
        .text
        .local  _ZStL8__ioinit
        .comm   _ZStL8__ioinit,1,1
        .globl  _Z3addii
        .type   _Z3addii, @function
_Z3addii:
.LFB1731:
        .cfi_startproc
        endbr64
        pushq   %rbp
        .cfi_def_cfa_offset 16
        .cfi_offset 6, -16
        movq    %rsp, %rbp
        .cfi_def_cfa_register 6
        movl    %edi, -4(%rbp)
        movl    %esi, -8(%rbp)
        movl    -4(%rbp), %edx
        movl    -8(%rbp), %eax
        addl    %edx, %eax
        popq    %rbp
        .cfi_def_cfa 7, 8
        ret
        .cfi_endproc
.LFE1731:
        .size   _Z3addii, .-_Z3addii
        .globl  _Z3addiii
        .type   _Z3addiii, @function

*/	
