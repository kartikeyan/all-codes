
#include<iostream>

int main(){
	
	int x = 10;
	int y = 20;

	std::cout << x << std::endl;	

	std::cout << y << std::endl;	

	int const *const ptr = &x;		

	std::cout << *ptr << std::endl;	
	
	x = 50;

	std::cout << x << std::endl;	

	ptr = &y;

	std::cout << *ptr << std::endl;	
}

/*
	katikeyan@kartikeyan:~/all-codes/CPP/Function_3$ g++ p4.cpp 
	p4.cpp: In function ‘int main()’:
	p4.cpp:21:13: error: assignment of read-only variable ‘ptr’
	   21 |         ptr = &y;
	      |         ~~~~^~~~
*/
