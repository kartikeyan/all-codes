
//	Function Returning address of local variable

#include<stdio.h>

int* fun(int x){
	
	int y = x + 10;

	return &y;
}

void main(){
	
	int x = 10;

	int *ptr = fun(x);

	printf("%d\n", *ptr);		// dereferencing 
}

/*	
 	function_Returning_Address_2.c: In function ‘fun’:
	function_Returning_Address_2.c:10:16: warning: function returns address of local variable [-Wreturn-local-addr]
   10 |         return &y;
      |                ^~
	katikeyan@kartikeyan:~/cpp/Introduction_1/lec_2$ ./a.out 
	Segmentation fault (core dumped)
*/

/*	
 	first u should not return address of local variable

	Answer depends on IDE
*/	
