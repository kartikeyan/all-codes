
#include<stdio.h>

void fun(int *arr,int size){
	
	for(int i = 0; i < size; i++){
		
		arr[i] = arr[i] + 10;
	}
}

void main(){
	
	int arr[] = {1,2,3,4,5};

	int size = sizeof(arr) / sizeof(arr[0]);

	fun(arr, size);

	for(int i = 0; i < size; i++){
		
		printf("%d\n", *(arr+i));
	}
}
