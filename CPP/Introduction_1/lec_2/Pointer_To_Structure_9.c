
#include<stdio.h>

struct Batsman{
	
	char name[20];
	int jerNo;
	float avg;
}vk = {"Virat",18,48.5};

void main(){
	
	struct Batsman obj = {"Mahi",10,45};

	struct Batsman *vptr = &vk;

	struct Batsman *mptr = &obj;

	printf("%s\n", vptr->name);
	printf("%d\n", vptr->jerNo);
	printf("%f\n", vptr->avg);

	printf("%s\n", (*mptr).name);
	printf("%d\n", (*mptr).jerNo);
	printf("%f\n", (*mptr).avg);

}
