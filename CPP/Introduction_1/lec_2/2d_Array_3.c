
#include<stdio.h>

void fun(int arr[][3]){
	
	for(int i = 0; i < 3; i++){
		
		for(int j = 0; j < 3; j++){
			
			printf("%d ", *(*(arr + i) + j));
		}

		printf("\n");
	}
}

void main(){
	
	int arr[][3] = {1,2,3,4,5,6,7,8,9};

	fun(arr);
}

/*	Internal of 2d array
 
	*(*(arr + i) + j)

		for i = 1 and j = 1

		= *(*(arr + 1 * (sizeof whole 1D array)) + 1)

		= *(*(100 + 1 * (12)) + 1)

		= (*(*112) + 1)

		= *(112 + 1 * (sizeof(Data Type of Pointer)))

		= *(112 + 1 (4))

		= *(116)

		= 5
