
#include<stdio.h>
#include<stdlib.h>
#include<string.h>

struct Lang{
	
	char langName[10];
	char founder[10];
};

void main(){
	
	struct Lang *ptr = (struct Lang*)malloc(sizeof(struct Lang));

	printf("Enter Language Name\n");
	strcpy(ptr->langName, "C");
//	scanf("%s", ptr->langName);
	printf("Lang Name is : %s\n", (*ptr).langName);

	printf("Enter Founder Name\n");
//	scanf("%s", ptr->founder);
	strcpy(ptr->founder, "Denis");
	printf("Founder is : %s\n", (*ptr).founder);
}
