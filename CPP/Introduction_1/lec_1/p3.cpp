
//	Checking if cpp can run 2 programs at a time

#include<iostream>

void fun(){
	
	std::cout << "In fun1" << std::endl;
}

int main(){
	
	fun();

	return 0;
}

/*
 	katikeyan@kartikeyan:~/cpp/Introduction_1$ vim p4.cpp
	katikeyan@kartikeyan:~/cpp/Introduction_1$ g++ p3.cpp p4.cpp
	/usr/bin/ld: /tmp/ccwpYzzF.o: in function `fun()':
	p4.cpp:(.text+0x0): multiple definition of `fun()'; /tmp/ccTgoPOa.o:p3.cpp:(.text+0x0): first defined here
	collect2: error: ld returned 1 exit status
	katikeyan@kartikeyan:~/cpp/Introduction_1$ 

*/
