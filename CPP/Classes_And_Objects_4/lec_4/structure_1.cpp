
#include<iostream>

struct Player{
	
	int jerNo = 18;
	char name[20] = "Virat Kohli";

	void display(){
		
		std::cout << jerNo << std::endl;
		std::cout << name << std::endl;
	}
};

int main(){
	
	Player obj;

	std::cout << obj.jerNo << std::endl;
	std::cout << obj.name << std::endl;
	
//	std::cout << obj << std::endl;			// error,  object cha address kadi pan print nahi karaycha

	obj.display();
}

/*
 	18
	Virat Kohli
	18
	Virat Kohli
*/

/*
	
   katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4$ g++ structure_1.cpp
structure_1.cpp: In function ‘int main()’:
structure_1.cpp:23:19: error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Player’)
   23 |         std::cout << obj << std::endl;
      |         ~~~~~~~~~ ^~ ~~~
      |              |       |
      |              |       Player
      |              std::ostream {aka std::basic_ostream<char>}

*/
