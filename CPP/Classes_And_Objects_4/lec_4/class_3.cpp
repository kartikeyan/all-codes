#include<iostream>

class Player{
	
	public:
	int jerNo = 18;					// Class madhe default sagla private ahe, tyala vaprayla public karava lagto

	std::string name = "kohli";
		
	void display(){
		
		std::cout << jerNo << std::endl;
		std::cout << name << std::endl;
	}
};

int main(){
	
	Player obj;

	std::cout << obj.jerNo << std::endl;
	std::cout << obj.name << std::endl;

	obj.display();

	std::cout << obj << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4$ g++ class_3.cpp
class_3.cpp: In function ‘int main()’:
class_3.cpp:26:19: error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Player’)
   26 |         std::cout << obj << std::endl;
      |         ~~~~~~~~~ ^~ ~~~
      |              |       |
      |              |       Player
      |              std::ostream {aka std::basic_ostream<char>}
*/
