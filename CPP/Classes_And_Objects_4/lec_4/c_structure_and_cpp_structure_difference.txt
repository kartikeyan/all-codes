
Lets discuss some of the above mentioned differences and similarities one by one:

	1]. Member functions inside the structure: Structures in C cannot have member functions inside a structure but Structures in C++ can have member functions along with data members.

	2]. Static Members: C structures cannot have static members but are allowed in C++. 

	3]. Constructor creation in structure: Structures in C cannot have a constructor inside a structure but Structures in C++ can have Constructor creation.

	4]. Direct Initialization: We cannot directly initialize structure data members in C but we can do it in C++. 

	5]. Using struct keyword: In C, we need to use a struct to declare a struct variable. In C++, a struct is not necessary. For example, let there be a structure for Record. In C, we must use “struct Record” for Record variables. In C++, we need not use struct, and using ‘Record‘ only would work.

	6]. Access Modifiers: C structures do not have access modifiers as these modifiers are not supported by the language. C++ structures can have this concept as it is inbuilt in the language.  

	7]. Pointers and References: In C++, there can be both pointers and references to a struct in C++, but only pointers to structs are allowed in C. 

	8]. sizeof operator: This operator will generate 0 for an empty structure in C whereas 1 for an empty structure in C++.  
	9]. Data Hiding: C structures do not allow the concept of Data hiding but are permitted in C++ as it is an object-oriented language whereas C is not. 

	10]. Constant Members: C struct may allow to declare constant members, but no way to initialize. But in C++, you can initialize using constructor initializer list




