#include<iostream>

class Player{
	
	int jerNo = 18;					// Class madhe default sagla private ahe

	std::string name = "kohli";

	void display(){
		
		std::cout << jerNo << std::endl;
		std::cout << name << std::endl;
	}
};

int main(){
	
	Player obj;

	std::cout << obj.jerNo << std::endl;
	std::cout << obj.name << std::endl;

	obj.display();
}

/*
 	
   	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4$ g++ class_1.cpp
	class_1.cpp: In function ‘int main()’:
	class_1.cpp:20:26: error: ‘int Player::jerNo’ is private within this context
	   20 |         std::cout << obj.jerNo << std::endl;
	      |                          ^~~~~
	class_1.cpp:5:13: note: declared private here
	    5 |         int jerNo = 18;
	      |             ^~~~~
	class_1.cpp:21:26: error: ‘std::string Player::name’ is private within this context
	   21 |         std::cout << obj.name << std::endl;
	      |                          ^~~~
	class_1.cpp:7:21: note: declared private here
	    7 |         std::string name = "kohli";
	      |                     ^~~~
	class_1.cpp:23:20: error: ‘void Player::display()’ is private within this context
	   23 |         obj.display();
	      |         ~~~~~~~~~~~^~
	class_1.cpp:9:14: note: declared private here
	    9 |         void display(){
	      |              ^~~~~~~

*/
