/*
 	Construtor :-
		
		1] No Argument Constructor

		2] Parameterised Constructor

		3] Copy Constructor
*/

#include<iostream>

class Demo{
	
	public:	
	Demo(){
		
		std::cout << "In Constructor" << std::endl;

		std::cout << this << std::endl;
	}
};

int main(){
	
	Demo obj;

	std::cout << &obj << std::endl;
}

/*
 	In Constructor
	0x7fff2e958147
	0x7fff2e958147
*/
