
/*
 	Construtor :-
		
		1] No Argument Constructor

		2] Parameterised Constructor

		3] Copy Constructor
*/

#include<iostream>

class Demo{
	
	Demo(){
		
		std::cout << "In Constructor" << std::endl;
	}
};

int main(){
	
	Demo obj;

	std::cout << &obj << std::endl;
}

/*
 	constructor_1.cpp: In function ‘int main()’:
	constructor_1.cpp:24:14: error: ‘Demo::Demo()’ is private within this context
	   24 |         Demo obj;
	      |              ^~~
	constructor_1.cpp:16:9: note: declared private here
	   16 |         Demo(){
	      |         ^~~~
*/
