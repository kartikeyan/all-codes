
#include<iostream>

class Demo{
	
	public:	
	int x = 10;

	static inline int y = 20;	// came after 17 version
};

int main(){
	
	Demo obj;

	std::cout << obj.x << std::endl;	// 10
	std::cout << Demo::y << std::endl;	// 20
}
