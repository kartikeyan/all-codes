#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "No-args Constructor" << std::endl;
		}

		Demo(int x){
				
			std::cout << "Parameterised Constructor" << std::endl;

			std::cout << x << std::endl;
		}
};

int main(){

	Demo obj5{};
};

/*
 	No-args Constructor
*/
