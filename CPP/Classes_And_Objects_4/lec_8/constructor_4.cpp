
#include<iostream>

class Demo{
	
	public:
		Demo(int x){
			
			std::cout << "In Constructor" << std::endl;	
		}
};

int main(){
	
	Demo obj;
}

/*
 	constructor_4.cpp: In function ‘int main()’:
	constructor_4.cpp:15:14: error: no matching function for call to ‘Demo::Demo()’
	   15 |         Demo obj;
	      |              ^~~
	constructor_4.cpp:7:17: note: candidate: ‘Demo::Demo(int)’
	    7 |                 Demo(int x){
	      |                 ^~~~
	constructor_4.cpp:7:17: note:   candidate expects 1 argument, 0 provided
	constructor_4.cpp:4:7: note: candidate: ‘constexpr Demo::Demo(const Demo&)’
	    4 | class Demo{
	      |       ^~~~
	constructor_4.cpp:4:7: note:   candidate expects 1 argument, 0 provided
	constructor_4.cpp:4:7: note: candidate: ‘constexpr Demo::Demo(Demo&&)’
	constructor_4.cpp:4:7: note:   candidate expects 1 argument, 0 provided
*/
