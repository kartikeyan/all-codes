
#include<iostream>

class Demo{
	
	public:	
	int x = 10;

	static const int y = 20;
};

int main(){
	
	Demo obj;

	std::cout << obj.x << std::endl;	// 10
	std::cout << Demo::y << std::endl;	// 20
}
