#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	public:
		Demo(){
			std::cout << "No Args Const" << std::endl;
		}

		Demo(int x, int y){
				
			this -> x = x;
			this -> y = y;

			std::cout << "Para Const" << std::endl;
		}

		Demo(const Demo &xyz){
			
			std::cout << "Copy Const" << std::endl;
		}
};

int main(){

	Demo obj1;

	Demo obj2(1000, 2000);

	Demo obj3 = obj1;

	Demo obj4;

	obj4 = obj1;
}

/*
 	No Args Const
	Para Const
	Copy Const
	No Args Const
*/
