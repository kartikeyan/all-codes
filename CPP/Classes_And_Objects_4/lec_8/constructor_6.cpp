#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "No-args Constructor" << std::endl;
		}

		Demo(int x){
				
			std::cout << "Parameterised Constructor" << std::endl;

			std::cout << x << std::endl;
		}
};

int main(){

	Demo obj5;		// it is a function declaration
};

/*
 	constructor_6.cpp: In function ‘int main()’:
	constructor_6.cpp:21:18: warning: empty parentheses were disambiguated as a function declaration [-Wvexing-parse]
	   21 |         Demo obj5();            // it is a function declaration
	      |                  ^~
	constructor_6.cpp:21:18: note: remove parentheses to default-initialize a variable
	   21 |         Demo obj5();            // it is a function declaration
	      |                  ^~
	      |                  --
*/
