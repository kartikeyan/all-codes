#include<iostream>

class Demo{
	
	int x = 10;

	static int y;

	public:
	void disp(){
		
		std::cout << x << std::endl;

		std::cout << y << std::endl;
	}

	static void info(){
		
		std::cout << x << std::endl;

		std::cout << y << std::endl;
	}
};

int Demo :: y = 20;

int main(){
	
	Demo obj;
	obj.disp();

	obj.info();
	Demo::info();
}

/*
 	static_member_Function_1.cpp: In static member function ‘static void Demo::info()’:
	static_member_Function_1.cpp:19:30: error: invalid use of member ‘Demo::x’ in static member function
	   19 |                 std::cout << x << std::endl;
	      |                              ^
	static_member_Function_1.cpp:5:13: note: declared here
	    5 |         int x = 10;
	      |             ^
*/
