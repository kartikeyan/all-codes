
//	Protected fakt child class madhe disto main() madhe pan nahi

#include<iostream>

class Demo{
	
	int x = 10;

	public:
	int y = 20;

	protected:
	int z = 30;		
};

int main(){

	Demo obj;

	std::cout << obj.x << obj.y << obj.z << std::endl;
}

/*
 	access_specifiers_2.cpp: In function ‘int main()’:
	access_specifiers_2.cpp:19:26: error: ‘int Demo::x’ is private within this context
	   19 |         std::cout << obj.x << obj.y << obj.z << std::endl;
	      |                          ^
	access_specifiers_2.cpp:6:13: note: declared private here
	    6 |         int x = 10;
	      |             ^
	access_specifiers_2.cpp:19:44: error: ‘int Demo::z’ is protected within this context
	   19 |         std::cout << obj.x << obj.y << obj.z << std::endl;
	      |                                            ^
	access_specifiers_2.cpp:12:13: note: declared protected here
	   12 |         int z = 30;
	      |             ^
*/
