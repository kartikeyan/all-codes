
#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	public:
	void info(){
		
		std::cout << this << std::endl;

		std::cout << this->x << std::endl;

		std::cout << this->y << std::endl;
	}
};

int main(){
	
	Demo obj;

	// std::cout << obj << std::endl;	// error - operator overloading 
	
	std::cout << &obj << std::endl;

	obj.info();
}

/*
 	0x7ffd73c0ef30
	0x7ffd73c0ef30
	10
	20
*/
