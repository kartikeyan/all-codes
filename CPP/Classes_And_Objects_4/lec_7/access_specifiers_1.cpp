
#include<iostream>

class Demo{
	
	int x = 10;

	public:
	int y = 20;

	protected:
	int z = 30;
};

int main(){
	
	std::cout << x << y << z << std::endl;
}

/*
 	access_specifiers_1.cpp: In function ‘int main()’:
	access_specifiers_1.cpp:17:22: error: ‘x’ was not declared in this scope
	   17 |         std::cout << x << y << z << std::endl;
	      |                      ^
	access_specifiers_1.cpp:17:27: error: ‘y’ was not declared in this scope
	   17 |         std::cout << x << y << z << std::endl;
	      |                           ^
	access_specifiers_1.cpp:17:32: error: ‘z’ was not declared in this scope
	   17 |         std::cout << x << y << z << std::endl;
	      |                                ^
*/
