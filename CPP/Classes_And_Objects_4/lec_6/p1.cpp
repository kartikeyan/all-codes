
//	1] Class heh ek "DataType" ahe

//	2] Constructor bydefault "public" asto

#include<iostream>

class Demo{
	
	int x = 10;			// Class bydefault private asto

	void fun(){
		
		std::cout << x << std::endl;
	}
};

int main(){
	
	Demo obj;

	obj.fun();

	return 0;
}

/*
 	p1.cpp: In function ‘int main()’:
	p1.cpp:18:16: error: ‘void Demo::fun()’ is private within this context
	   18 |         obj.fun();
	      |         ~~~~~~~^~
	p1.cpp:8:14: note: declared private here
	    8 |         void fun(){
	      |              ^~~
*/
