
/*
	Variables   

		a] Instance
		b] Static :- Cannot be initialized in class
*/

#include<iostream>

class Demo{
	
	int x = 10;

	static int y = 20;

	public:
	void fun(){
		
		std::cout << x << std::endl;

		std::cout << y << std::endl;

	}
};

int main(){
	
	Demo obj;

	obj.fun();	

	return 0;
}

/*
 	variables_3.cpp:15:20: error: ISO C++ forbids in-class initialization of non-const static member ‘Demo::y’
	   15 |         static int y = 20;
	      |                    ^
*/
