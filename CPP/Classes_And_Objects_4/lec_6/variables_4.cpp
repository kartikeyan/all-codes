
/*
	Variables   

		a] Instance
		b] Static :- Cannot be initialized in class
*/

#include<iostream>

class Demo{
	
	int x = 10;

	static int y;

	public:
	void fun(){
		
		std::cout << x << std::endl;

		std::cout << y << std::endl;

	}
};

Demo :: y = 20;

int main(){
	
	Demo obj;

	obj.fun();	

	return 0;
}

/*
 	variables_4.cpp:27:9: error: ‘int Demo::y’ is private within this context
	   27 | Demo :: y = 20;
	      |         ^
	variables_4.cpp:15:20: note: declared private here
	   15 |         static int y;
	      |                    ^
	variables_4.cpp:27:9: error: ‘y’ in ‘class Demo’ does not name a type
	   27 | Demo :: y = 20;
	      |         ^
	variables_4.cpp:15:20: note: ‘Demo::y’ declared here
	   15 |         static int y;
	      |                    ^
*/
