
#include<iostream>

class Demo{
	
	int x = 10;

	int y = 20;

	public:
	Demo(){
		
		std::cout << "In Constructor" << std::endl;
	}

	void fun(){
		
		std::cout << x << std::endl;

		std::cout << y << std::endl;
	}
};

int main(){
	
	Demo obj;

	obj.fun();

	Demo *obj2 = new Demo();

	obj2->fun();		// internally (*obj2).fun()

	return 0;
}

/*
 	In Constructor
	10
	20
	In Constructor
	10
	20
*/
