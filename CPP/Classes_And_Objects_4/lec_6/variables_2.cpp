
/*
	Variables   

		a] Instance
		b] Static :- Cannot be initialized in class
*/

#include<iostream>

class Demo{
	
	int x = 10;

	static int y;

	public:
	void fun(){
		
		std::cout << x << std::endl;
	}
};

int main(){
	
	Demo obj;

	obj.fun();	// 10

	return 0;
}


