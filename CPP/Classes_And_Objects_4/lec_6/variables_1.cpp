
/*
	Variables   

		a] Instance
		b] Static :- Cannot be initialized in class
*/

#include<iostream>

class Demo{
	
	int x = 10;

	static int y;

	public:
	void fun(){
		
		std::cout << x << std::endl;

		std::cout << y << std::endl;
	}
};

int main(){
	
	Demo obj;

	obj.fun();

	return 0;
}

/*
 	/usr/bin/ld: /tmp/cc7BP3dZ.o: warning: relocation against `_ZN4Demo1yE' in read-only section `.text._ZN4Demo3funEv[_ZN4Demo3funEv]'
	/usr/bin/ld: /tmp/cc7BP3dZ.o: in function `Demo::fun()':
	variables_1.cpp:(.text._ZN4Demo3funEv[_ZN4Demo3funEv]+0x3b): undefined reference to `Demo::y'
	/usr/bin/ld: warning: creating DT_TEXTREL in a PIE
	collect2: error: ld returned 1 exit status
*/
