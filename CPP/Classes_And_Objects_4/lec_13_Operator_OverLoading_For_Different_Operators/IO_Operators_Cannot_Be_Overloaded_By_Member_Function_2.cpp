
#include<iostream>

class Demo{
	
	int x = 10;

	std::ostream& operator << (std::ostream& out){
		
		out << this->x;

		return out;
	}
};

int main(){
	
	Demo obj;

	std::cout << obj << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_13_Operator_OverLoading_For_Different_Operators$ g++ IO_Operators_Cannot_Be_Overloaded_By_Member_Function_2.cpp
	IO_Operators_Cannot_Be_Overloaded_By_Member_Function_2.cpp: In function ‘int main()’:
	IO_Operators_Cannot_Be_Overloaded_By_Member_Function_2.cpp:20:19: error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Demo’)
	   20 |         std::cout << obj << std::endl;
	      |         ~~~~~~~~~ ^~ ~~~
	      |              |       |
	      |              |       Demo
	      |              std::ostream {aka std::basic_ostream<char>}
*/
