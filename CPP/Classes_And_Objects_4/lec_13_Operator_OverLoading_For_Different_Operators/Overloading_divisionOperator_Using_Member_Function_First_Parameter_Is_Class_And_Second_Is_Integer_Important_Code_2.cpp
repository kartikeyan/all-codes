
//	Member Function only be used when first parameter is Object

#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}
	
		int operator / (const Demo& obj2){
			
			std::cout << "Member" << std::endl;

			return this->x / obj2.x;
		}

};



int main(){
	
	Demo obj1(100);

	std::cout <<  obj1 / 100 << std::endl;
}

/*
 	Member
	1
*/
