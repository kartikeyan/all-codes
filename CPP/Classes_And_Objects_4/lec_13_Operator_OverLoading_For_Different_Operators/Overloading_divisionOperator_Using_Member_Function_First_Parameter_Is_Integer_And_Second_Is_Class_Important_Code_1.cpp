
//	Member Function only be used when first parameter is Object

#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}
	
		int operator / (const Demo& obj2){
			
			std::cout << "Member" << std::endl;

			return this->x / obj2.x;
		}

};



int main(){
	
	Demo obj1(100);

	std::cout << 100 / obj1  << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_13_Operator_OverLoading_For_Different_Operators$ g++ Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_4.cpp
	Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_4.cpp: In function ‘int main()’:
	Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_4.cpp:45:26: error: no match for ‘operator/’ (operand types are ‘int’ and ‘Demo’)
	   45 |         std::cout << 100 / obj1  << std::endl;
	      |                      ~~~ ^ ~~~~
	      |                      |     |
	      |                      int   Demo
*/
