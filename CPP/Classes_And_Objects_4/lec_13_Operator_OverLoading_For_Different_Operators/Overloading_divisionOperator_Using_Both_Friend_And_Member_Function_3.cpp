
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}
	
		int operator / (const Demo& obj2){
			
			std::cout << "Member" << std::endl;

			return this->x / obj2.x;
		}

		friend int operator / (const Demo& obj1, const Demo& obj2){
		
			std::cout << "Friend" << std::endl;
			return obj1.x / obj2.x;
		}

		int getData() const{
			
			return x;
		}
};

/*
int operator / (const Demo& obj1, const Demo& obj2){
			
	return obj1.getData() / obj2.getData();
}
*/

int main(){
	
	Demo obj1(100);
//	Demo obj2(20);

	std::cout << 100 / obj1  << std::endl;
}

/*
 	Friend
	1
*/
