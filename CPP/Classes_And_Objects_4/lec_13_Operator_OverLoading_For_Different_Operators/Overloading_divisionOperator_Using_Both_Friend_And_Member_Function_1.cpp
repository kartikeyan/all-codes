
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}
/*	
		int operator / (const Demo& obj2){
			
			std::cout << "Member" << std::endl;

			return this->x / obj2.x;
		}
*/
		friend int operator / (const Demo& obj1, const Demo& obj2){
			
			return obj1.x / obj2.x;
		}

		int getData() const{
			
			return x;
		}
};


int operator / (const Demo& obj1, const Demo& obj2){
			
	return obj1.getData() / obj2.getData();
}


int main(){
	
	Demo obj1(100);
	Demo obj2(20);

	std::cout << obj1 / obj2 << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_13_Operator_OverLoading_For_Different_Operators$ g++ Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_1.cpp
	Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_1.cpp:33:5: error: redefinition of ‘int operator/(const Demo&, const Demo&)’
	   33 | int operator / (const Demo& obj1, const Demo& obj2){
	      |     ^~~~~~~~
	Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_1.cpp:21:28: note: ‘int operator/(const Demo&, const Demo&)’ previously defined here
	   21 |                 friend int operator / (const Demo& obj1, const Demo& obj2){
	      |                            ^~~~~~~~
*/
