
#include<iostream>

class Demo{
	
	int x = 10;

	std::ostream& operator << (std::ostream& out, const Demo& obj1){
		
		out << obj1.x;

		return out;
	}
};

int main(){
	
	Demo obj;

	std::cout << obj << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_13_Operator_OverLoading_For_Different_Operators$ g++ IO_Operators_Cannot_Be_Overloaded_By_Member_Function_1.cpp
	IO_Operators_Cannot_Be_Overloaded_By_Member_Function_1.cpp:8:23: error: ‘std::ostream& Demo::operator<<(std::ostream&, const Demo&)’ must have exactly one argument
	    8 |         std::ostream& operator << (std::ostream& out, const Demo& obj1){
	      |                       ^~~~~~~~
	IO_Operators_Cannot_Be_Overloaded_By_Member_Function_1.cpp: In function ‘int main()’:
	IO_Operators_Cannot_Be_Overloaded_By_Member_Function_1.cpp:20:19: error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Demo’)
	   20 |         std::cout << obj << std::endl;
	      |         ~~~~~~~~~ ^~ ~~~
	      |              |       |
	      |              |       Demo
	      |              std::ostream {aka std::basic_ostream<char>}
*/
