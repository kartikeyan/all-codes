
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}

		void* operator new (size_t size){
			
			// void *ptr = ::operator new (size);		// internally cpp asa call karto

			void *ptr = malloc(size);

			return ptr;
		}
};

int main(){
	
	std::cout << sizeof(Demo) << std::endl;		// 4

	Demo *obj = new Demo(50);

	obj->getData();		// 50
				
}
