
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}

		~Demo(){
			
			std::cout << "Destructor" << std::endl;
		}

		void operator delete (void *ptr){
			
			free(ptr);
		}
};

int main(){

	Demo *obj = new Demo(50);

	obj->getData();		// 50
	
	delete obj;	
}
