
/*
 	Overloading Relational Operators (< , > , <<, >>, ==, != )

*/

//	<

#include<iostream>

class Demo{
	
	int x;
	int y;

	public:
		Demo(int x, int y){
			
			this->x = x;
			this->y = y;
		}

		int getX() const{
			
			return x;
		}

		int getY() const{
			
			return y;
		}
};

int operator < (const Demo &obj1 , const Demo &obj2){
	
	if((obj1.getX() < obj2.getX()) && (obj1.getY() < obj2.getY())){
		
		return 1;		
	}

	return 0;
}

int main(){
	
	Demo obj1(10, 20);
	Demo obj2(20, 30);

	std::cout << (obj1 < obj2) << std::endl;		// 1
}
