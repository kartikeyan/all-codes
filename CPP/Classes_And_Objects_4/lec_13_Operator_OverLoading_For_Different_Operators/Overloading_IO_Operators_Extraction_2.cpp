
/*
 	a] Overloading IO operators : Insertion (<<), Extraction (>>)

		ostream, istream  ==>  class
		
		cout , cin  ==> object of ostream, istream

		<<, >>  ==> function
*/

/*
 	ithe 2 operators overloading che call ahet, 1 apla ahe ani dusra predefined la call karto.
*/

#include<iostream>

class Demo{
	
	public:
	int x;
	int y;

	void printData(){
			
		std::cout << x << " " << y << std::endl;
	}

	friend std::istream& operator >> (std::istream& in, Demo& obj){		// don't include const Demo &obj here because operator>>() function is not defined for the Demo class. This function is used to read data from an input stream into a class object.
										// And if we take const Demo &obj, it is constant object we cannot change i.e we cannot take input from it
		
		in >> obj.x;
		
		in >> obj.y;

		return in;
	}
};

int main(){
	
	Demo obj1;
	Demo obj2;

	std::cout << "Enter value" << std::endl;
	std::cin >> obj1 >> obj2;		

	obj1.printData();
	obj2.printData();
}

/*
 	Enter value
	1
	2
	5
	6
	1 2
	5 6
*/
