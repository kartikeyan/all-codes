
/*
 	a] Overloading IO operators : Insertion (<<), Extraction (>>)

		ostream, istream  ==>  class
		
		cout , cin  ==> object of ostream, istream

		<<, >>  ==> function
*/

/*
 	ithe 2 operators overloading che call ahet, 1 apla ahe ani dusra predefined la call karto.
*/

#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	friend std::ostream& operator << (std::ostream& out, const Demo& obj){		// don't write const because std::ostream is predefined and write const only for our object
		
		out << obj.x;	// ostream& operator << (ostream &out, int obj.x),  ithun parat predefined la call jato

		return out;	// returning out for endl
	}
};

int main(){
	
	Demo obj1;

	std::cout << obj1 << std::endl;		// 10
}

//	IO Operators cannot be overloaded by Member Function
