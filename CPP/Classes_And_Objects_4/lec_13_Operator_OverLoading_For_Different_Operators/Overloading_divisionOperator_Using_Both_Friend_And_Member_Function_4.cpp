
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}
	
		int operator / (const Demo& obj2){
			
			std::cout << "Member" << std::endl;

			return this->x / obj2.x;
		}
/*
		friend int operator / (const Demo& obj1, const Demo& obj2){
		
			std::cout << "Friend" << std::endl;
			return obj1.x / obj2.x;
		}
*/
		int getData() const{
			
			return x;
		}
};

/*
int operator / (const Demo& obj1, const Demo& obj2){
			
	return obj1.getData() / obj2.getData();
}
*/

int main(){
	
	Demo obj1(100);
//	Demo obj2(20);

	std::cout << 100 / obj1  << std::endl;
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_13_Operator_OverLoading_For_Different_Operators$ g++ Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_4.cpp
	Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_4.cpp: In function ‘int main()’:
	Overloading_divisionOperator_Using_Both_Friend_And_Member_Function_4.cpp:45:26: error: no match for ‘operator/’ (operand types are ‘int’ and ‘Demo’)
	   45 |         std::cout << 100 / obj1  << std::endl;
	      |                      ~~~ ^ ~~~~
	      |                      |     |
	      |                      int   Demo
*/
