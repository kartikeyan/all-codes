
/*
 	a] Overloading IO operators : Insertion (<<), Extraction (>>)

		ostream, istream  ==>  class
		
		cout , cin  ==> object of ostream, istream

		<<, >>  ==> function
*/

/*
 	ithe 2 operators overloading che call ahet, 1 apla ahe ani dusra predefined la call karto.
*/

#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	public:
		int getData() const {
			
			return x;	
		}
};

std::ostream& operator << (std::ostream &out, const Demo &obj){
	
	out << obj.getData();

	return out;
}

int main(){
	
	Demo obj1;

	std::cout << obj1 << std::endl;		// 10
}
