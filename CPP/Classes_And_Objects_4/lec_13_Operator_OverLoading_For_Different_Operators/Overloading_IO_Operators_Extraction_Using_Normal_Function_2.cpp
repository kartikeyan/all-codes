
/*
 	a] Overloading IO operators : Insertion (<<), Extraction (>>)

		ostream, istream  ==>  class
		
		cout , cin  ==> object of ostream, istream

		<<, >>  ==> function
*/

/*
 	ithe 2 operators overloading che call ahet, 1 apla ahe ani dusra predefined la call karto.
*/

#include<iostream>

class Demo{
	
	public:
	int x;
	int y;

	void printData(){
		
		std::cout << x << " " << y << std::endl;
	}
};

std::istream& operator >> (std::istream &in, Demo &obj){
	
	in >> obj.x;

	in >> obj.y;

	return in;
}

int main(){
	
	Demo obj1;

	std::cout << "Enter Values " << std::endl;

	std::cin >> obj1;

	obj1.printData();
}

/*
 	Enter Values 
	10
	20
	10 20
*/
