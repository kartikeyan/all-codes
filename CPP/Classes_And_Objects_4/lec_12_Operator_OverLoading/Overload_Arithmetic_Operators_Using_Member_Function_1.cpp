
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}

		int operator + (const Demo &obj1, const Demo &obj2){
			return obj1.x + obj2.x;
		}
};

int main(){
	
	Demo obj1(30);

	Demo obj2(40);

	std::cout << obj1 + obj2 << std::endl;
}

/*
 	member function madhe first paramter this pointer asto, mahnun toh error deto 
*/

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_12_Operator_OverLoading$ g++ Overload_Arithmetic_Operators_Using_Member_Function_1.cpp

	Overload_Arithmetic_Operators_Using_Member_Function_1.cpp:14:21: error: ‘int Demo::operator+(const Demo&, const Demo&)’ must have either zero or one argument
	   14 |                 int operator + (const Demo &obj1, const Demo &obj2){
	      |                     ^~~~~~~~
	Overload_Arithmetic_Operators_Using_Member_Function_1.cpp: In function ‘int main()’:
	Overload_Arithmetic_Operators_Using_Member_Function_1.cpp:25:27: error: no match for ‘operator+’ (operand types are ‘Demo’ and ‘Demo’)
	   25 |         std::cout << obj1 + obj2 << std::endl;
	      |                      ~~~~ ^ ~~~~
	      |                      |      |
	      |                      Demo   Demo
*/
