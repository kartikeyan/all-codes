
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;			
		}

		int getData(){		// ithe constant pahije
			
			return x;
		}
};

int operator + (const Demo& obj1, const Demo  &obj2){
	
	return obj1.getData() + obj2.getData();
}

int main(){
	
	Demo obj1(30);
	Demo obj2(40);

	std::cout << obj1 + obj2  << std::endl;
}

//	const object ahe tar, const method pahije

/*
 	12_Operator_OverLoading$ g++ Overload_Arithmetic_Operators_Using_Normal_Function.cpp

	Overload_Arithmetic_Operators_Using_Normal_Function.cpp: In function ‘int operator+(const Demo&, const Demo&)’:
	Overload_Arithmetic_Operators_Using_Normal_Function.cpp:22:28: error: passing ‘const Demo’ as ‘this’ argument discards qualifiers [-fpermissive]
	   22 |         return obj1.getData() + obj2.getData();
	      |                ~~~~~~~~~~~~^~
	Overload_Arithmetic_Operators_Using_Normal_Function.cpp:14:21: note:   in call to ‘int Demo::getData()’
	   14 |                 int getData(){
	      |                     ^~~~~~~
	Overload_Arithmetic_Operators_Using_Normal_Function.cpp:22:45: error: passing ‘const Demo’ as ‘this’ argument discards qualifiers [-fpermissive]
	   22 |         return obj1.getData() + obj2.getData();
	      |                                 ~~~~~~~~~~~~^~
	Overload_Arithmetic_Operators_Using_Normal_Function.cpp:14:21: note:   in call to ‘int Demo::getData()’
	   14 |                 int getData(){
	      |                     ^~~~~~~
*/
