
#include<iostream>

class Demo{
	
	int x = 10;
	
	int y = 20;

	public:
		Demo(int x){
			
			this->x = x;
		}
		
		Demo(int x, int y){
			
			this->x = x;
			this->y = y;
		}

		friend int operator * (const Demo&, const Demo&);
};

int operator * (const Demo& obj1, const Demo& obj2){
	
	return obj1.x * obj2.y;
}

int main(){
	
	Demo obj1(30, 40);
	Demo obj2(50, 60);

	std::cout << obj1 * obj2 << std::endl;	// 1800
						// 600
	std::cout << obj1 * 70 << std::endl;	
	
//	std::cout << 70 * obj2 << std::endl;	
	
/*	Demo obj3 = 70;

	std::cout << obj1 * obj3 << std::endl;	 

	std::cout << obj3 * obj3 << std::endl;	 

	std::cout << obj3 * obj2 << std::endl;	 

	Demo obj4 = Demo(90);

	std::cout << obj4 * obj3 << std::endl;	
*/
}

//	typecasting hoto, mhnje primitive datatype class madhe typecasting hoto
