
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		Demo(int x){
			
			this->x = x;
		}

		friend int operator + (const Demo&, const Demo&);
};

int operator + (const Demo& obj1, const Demo& obj2){
	
	return obj1.x + obj2.x;
}

int main(){
	
	Demo obj1(30);
	Demo obj2(40);

//	std::cout << obj1 + obj2 << std::endl;	// 70

//	std::cout << obj1 + 70 << std::endl;	// 100
	
//	std::cout << 70 + obj2 << std::endl;	// 110
	
	Demo obj3 = 70;

	std::cout << obj1 + obj3 << std::endl;	 // 100

	std::cout << obj3 + obj3 << std::endl;	 // 140

	std::cout << obj3 + obj2 << std::endl;	 // 110

	Demo obj4 = Demo(90);

	std::cout << obj4 + obj3 << std::endl;	// 160
}

//	typecasting hoto, mhnje primitive datatype class madhe typecasting hoto
