
#include<iostream>

class Demo{
	
	int a = 10;

	public:
		Demo(){
			
		}
};

int main(){
	
	Demo obj;

	std::cout << obj << std::endl;	// function call => operator << (cout, obj)

       // prototype => ostream& operator << (ostream&, Demo&) => not predefined	
}

/*
 	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_12_Operator_OverLoading$ g++ p2.cpp
	p2.cpp: In function ‘int main()’:
	p2.cpp:18:19: error: no match for ‘operator<<’ (operand types are ‘std::ostream’ {aka ‘std::basic_ostream<char>’} and ‘Demo’)
	   18 |         std::cout << obj << std::endl;  
	      |         ~~~~~~~~~ ^~ ~~~
	      |              |       |
	      |              |       Demo
	      |              std::ostream {aka std::basic_ostream<char>}

*/
