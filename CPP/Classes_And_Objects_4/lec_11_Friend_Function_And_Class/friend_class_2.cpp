
#include<iostream>

class One;

class Two{
	
	public:
		Two(){
			
			std::cout << "Two const" << std::endl;
		}
	public:
		void accessData(const One &obj);
};

class One{
	
	int x = 10;

	protected:
		int y = 20;

	public:
		One(){
			
			std::cout << "One const" << std::endl;
		}

		friend void Two::accessData(const One &obj);
};

void Two::accessData(const One &obj){
	
	std::cout << obj.x << std::endl;
	std::cout << obj.y << std::endl;
}

int main(){
	
	One obj1;
	Two obj2;

	obj2.accessData(obj1);
}

/*
 	One const
	Two const
	10
	20
*/
