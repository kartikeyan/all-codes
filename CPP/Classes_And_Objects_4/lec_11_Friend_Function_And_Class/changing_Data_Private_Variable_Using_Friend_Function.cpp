
/*
 	Friend Function ha asa function ahe jo private ani protected data fakt read only access karnyasathi alay
*/

#include<iostream>

class Demo{
	
	int x = 10;

	protected:
		int y = 20;

	public:
		Demo(){
			
			std::cout << "Const" << std::endl;
		}

		void getData(){
			
			std::cout << "x = " << x << std::endl;
			
			std::cout << "y = " << y << std::endl;
		}

		friend void accessData(Demo& obj);	// ithe asa protocol ahe ki const reference pahije									       // nahitar private variables cha data change hou shakto
};

void accessData(Demo& obj){
	
	int temp = obj.x;
	obj.x = obj.y;
	obj.y = temp;

	std::cout << obj.x << std::endl;
	std::cout << obj.y << std::endl;
}

int main(){
	
	Demo obj;

	obj.getData();

	accessData(obj);

	obj.getData();
}

/*
 	Const
	x = 10
	y = 20
	20
	10
	x = 20
	y = 10
*/
