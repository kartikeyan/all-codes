
#include<iostream>

class Two;	// optional ahe

class One{
	
	int x = 10;

	protected:
		int y = 20;

	public:
		One(){
			
			std::cout << "One const" << std::endl;
		}

		friend class Two();
};

class Two{
	
	public:
		Two(){
			
			std::cout << "Two const" << std::endl;
		}

	private:
		void getData(const One &obj){
			
			std::cout << obj.x << std::endl;
			std::cout << obj.y << std::endl;
		}

	public:
		void accessData(const One &obj){
			
			std::cout << obj.x << std::endl;
			std::cout << obj.y << std::endl;

			getData(obj);
		}
};

int main(){
	
	One obj1;
	Two obj2;

	obj2.accessData(obj1);
}
