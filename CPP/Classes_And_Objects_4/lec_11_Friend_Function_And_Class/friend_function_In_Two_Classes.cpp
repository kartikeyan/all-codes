
#include<iostream>

class Two;		// forward declaration

class One{
	
	int x = 10;

	protected:
		int y = 20;

	public:
		One(){
			
			std::cout << "One const" << std::endl;
		}

	private:
		void getData(){
			
			std::cout << "x = " << x << std::endl;
		       	std::cout << "y = " << y << std::endl;
		}

		friend void accessData(const One& obj1, const Two &obj2);
};

class Two{
	
	int a = 10;

	protected:
		int b = 20;

	public:
		Two(){
			
			std::cout << "Two Const" << std::endl;
		}

	private:
		void getData(){
			
			std::cout << a << std::endl;
			std::cout << b << std::endl;
		}

		friend void accessData(const One &obj1, const Two &obj2);
	// private asla tari friend function la kahi farak padat nahi, private variable access karu shalto
};

void accessData(const One &obj1, const Two &obj2){
	
	obj1.getData();
	obj2.getData();
}

int main(){
	
	One obj1;
	Two obj2;

	accessData(obj1, obj2);
}
