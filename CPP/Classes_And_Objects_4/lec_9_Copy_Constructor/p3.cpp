
#include<iostream>

class Demo {
	
	public:
		int x = 10;

	public:
/*		Demo(){
			std::cout << "No Args" << std::endl;
		}
*/
		Demo(int x){					// Jar parametric constructor dila tar, compiler samajto ki jar tu paramertic deu shakla tar, Default pan dyala pahije
			std::cout << "Para " << std::endl;
		}		

		Demo(Demo &obj){				// Copy constructor nahi dila tar default yeto
			std::cout << "Copy " << std::endl;
		}

		void fun(){

			std::cout << x << std::endl;
			std::cout << this->x << std::endl;
		}
};

int main(){
	
	Demo obj1;

	Demo obj2 = obj1;

	std::cout << obj1.x << std::endl;

	std::cout << obj2.x << std::endl;
}

/*
	p3.cpp: In function ‘int main()’:
	p3.cpp:31:14: error: no matching function for call to ‘Demo::Demo()’
	   31 |         Demo obj1;
	      |              ^~~~
	p3.cpp:18:17: note: candidate: ‘Demo::Demo(Demo&)’
	   18 |                 Demo(Demo &obj){                                
	      |                 ^~~~
	p3.cpp:18:17: note:   candidate expects 1 argument, 0 provided
	p3.cpp:14:17: note: candidate: ‘Demo::Demo(int)’
	   14 |                 Demo(int x){
	      |                 ^~~~
	p3.cpp:14:17: note:   candidate expects 1 argument, 0 provided
*/
