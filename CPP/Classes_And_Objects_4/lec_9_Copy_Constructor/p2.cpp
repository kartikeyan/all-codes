
#include<iostream>

class Demo {
	
	public:
		int x = 10;

	public:
		Demo(){
			std::cout << "No Args" << std::endl;
		}

		Demo(int x){
			std::cout << "Para " << std::endl;
		}

/*		Demo(Demo &obj){				// Copy constructor nahi dila tar default yeto
			std::cout << "Copy " << std::endl;
		}
*/
		void fun(){

			std::cout << x << std::endl;
			std::cout << this->x << std::endl;
		}
};

int main(){
	
	Demo obj1;

	Demo obj2 = obj1;

	std::cout << obj1.x << std::endl;

	std::cout << obj2.x << std::endl;
}

/*
	No Args
	10
	10
*/
