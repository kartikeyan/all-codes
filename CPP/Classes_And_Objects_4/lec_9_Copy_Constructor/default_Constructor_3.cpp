
#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	public:
	Demo(){
		
		std::cout << "No Args" << std::endl;
	}

	Demo(int x = 50, int y = 30){		// default constructor la right pasun parameter dyala pahije
		
		this->x = x;
		this->y = y;

		std::cout << "Para" << std::endl;
		std::cout << x << " "<< y << std::endl;
	} 
};

int main(){

	Demo obj1;

	Demo obj2(100);
}

/*
 	default_Constructor_3.cpp: In function ‘int main()’:
	default_Constructor_3.cpp:27:14: error: call of overloaded ‘Demo()’ is ambiguous
	   27 |         Demo obj1;
	      |              ^~~~
	default_Constructor_3.cpp:15:9: note: candidate: ‘Demo::Demo(int, int)’
	   15 |         Demo(int x = 50, int y = 30){           
	      |         ^~~~
	default_Constructor_3.cpp:10:9: note: candidate: ‘Demo::Demo()’
	   10 |         Demo(){
	      |         ^~~~
*/
