
#include<iostream>

class Demo {
	
	public:
		int x = 10;

	public:
		Demo(){
			std::cout << "No Args" << std::endl;
		}

/*		Demo(int x){					// Jar parametric constructor dila tar, compiler samajto ki jar tu paramertic deu shakla tar, Default pan dyala pahije
			std::cout << "Para " << std::endl;
		}		
*/
		Demo(Demo &obj){				// Copy constructor nahi dila tar default yeto
			std::cout << "Copy " << std::endl;
		}

		void fun(){

			std::cout << x << std::endl;
			std::cout << this->x << std::endl;
		}
};

int main(){
	
	Demo obj1;
	
	Demo obj2 = obj1;
	
	Demo obj3(10);

	std::cout << obj1.x << std::endl;

	std::cout << obj2.x << std::endl;
}

/*
	p5.cpp: In function ‘int main()’:
	p5.cpp:35:21: error: no matching function for call to ‘Demo::Demo(int)’
	   35 |         Demo obj3(10);
	      |                     ^
	p5.cpp:18:17: note: candidate: ‘Demo::Demo(Demo&)’
	   18 |                 Demo(Demo &obj){                                
	      |                 ^~~~
	p5.cpp:18:28: note:   no known conversion for argument 1 from ‘int’ to ‘Demo&’
	   18 |                 Demo(Demo &obj){                                
	      |                      ~~~~~~^~~
	p5.cpp:10:17: note: candidate: ‘Demo::Demo()’
	   10 |                 Demo(){
	      |                 ^~~~
	p5.cpp:10:17: note:   candidate expects 0 arguments, 1 provided
*/
