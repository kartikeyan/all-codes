
#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	public:
	Demo(){
		
		std::cout << "No Args" << std::endl;
	}

	Demo(int x, int y = 30){		// default constructor la right pasun parameter dyala pahije
		
		this->x = x;
		this->y = y;

		std::cout << "Para" << std::endl;
		std::cout << x << " "<< y << std::endl;
	} 
};

int main(){

	Demo obj1;

	Demo obj2(100);
}

/*
 	No Args
	Para
	100 30
*/
