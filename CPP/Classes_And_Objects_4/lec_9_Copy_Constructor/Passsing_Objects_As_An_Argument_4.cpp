#include<iostream>

class Demo{
	
	int x = 10;
	int y = 20;

	public:
	Demo(){
		
		std::cout << "No Args" << std::endl;
	}

	Demo(int x, int y){
		
		this->x = x;
		this->y = y;

		std::cout << "Para" << std::endl;
	}

	Demo(Demo &ref){
		
		std::cout << "Copy Const" << std::endl;
	}

	void access(){
		
		std::cout << x << " " << y << std::endl;
	}

	Demo& info(Demo &obj){		// Demo &obj = (&obj1) ==> ithe copy const. la call nahi janar.
		
		obj.x = 700;
		obj.y = 800;

		return obj;
	}
};
		
int main(){

	Demo obj1;
	obj1.access();

	Demo obj2(100, 200);
	obj2.access();

	Demo &obj3 = obj2.info(obj1);		// ithe pan copy const. la call janar
}

/*
 	No Args
	10 20
	Para
	100 200
	700 800
*/
