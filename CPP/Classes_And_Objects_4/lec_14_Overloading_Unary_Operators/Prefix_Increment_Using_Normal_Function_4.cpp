
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

		int getData() {
			
			return ++x;
		}
};

int operator ++ (Demo& obj){

	return obj.getData();
}

int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
}

