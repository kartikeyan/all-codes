
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:
		
		int operator ++(){
			
			return ++x;
		}

		int operator ++ (int){		// int parameter makes the difference that it is postfix increment
			
			int temp = x;

			++x;

                        return temp;
               }
};


int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
						
	std::cout << obj++ << std::endl;	// 11
}

