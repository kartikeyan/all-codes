
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

/*		int operator ++ (){
			
			return ++x;		// 11
		}
*/


		friend int operator ++ (Demo& obj){

                        return ++obj.x;
                }

		int operator ++ (int){

			int temp = x;

			++x;

			return temp;
		}
	
		friend std::ostream& operator << (std::ostream &out, const Demo &obj){
			
			out << obj.x;
			return out;
		}
/*
		int getData() {
			
			return ++x;
		}
*/
};

/*
int operator ++ (Demo& obj){

	return obj.getData();
}
*/

int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
	std::cout << obj++ << std::endl;	// 11
	
	std::cout << obj << std::endl;		// 12
}

