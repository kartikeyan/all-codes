
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

/*		int operator ++ (){
			
			return ++x;		// 11
		}
*/


		friend int operator ++ (Demo& obj){

                        return ++obj.x;
                }
/*
		int getData() {
			
			return ++x;
		}
*/
};

/*
int operator ++ (Demo& obj){

	return obj.getData();
}
*/

int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
	std::cout << obj++ << std::endl;	// 11
}

/*
 	unary_Operators_3.cpp: In function ‘int main()’:
	unary_Operators_3.cpp:43:25: error: no ‘operator++(int)’ declared for postfix ‘++’ [-fpermissive]
	   43 |         std::cout << obj++ << std::endl;        
	      |     
*/
