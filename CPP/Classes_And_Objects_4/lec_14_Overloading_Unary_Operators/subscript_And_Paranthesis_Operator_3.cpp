
#include<iostream>

class Demo{	
	
	int arr[5] = {10,20,30,40,50};	// array size should be declared first
	
	public:
		void getArray(){
			
			for(int i = 0; i < (sizeof(arr) / sizeof(int)); i++){
				
				std::cout << arr[i] << " ";
			}

			std::cout << std::endl;
		}

		int& operator[] (int index){
			
			return arr[index];
		}

		int operator() (int x, int y){
			
			return x + y;
		}
};

int main(){
	
	Demo obj;
		
	obj[2] = 70;		// arr[2] = 70
				
	obj.getArray();

	int res = obj(50, 70);

	std::cout << res << std::endl;
}

/*
 	10 20 70 40 50 
	120
*/
