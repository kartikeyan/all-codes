
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

		int operator ++ (){

                        return ++x;
                }

};

int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
}

