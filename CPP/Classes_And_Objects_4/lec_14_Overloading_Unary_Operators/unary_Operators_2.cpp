
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

/*		int operator ++ (){
			
			return ++x;		// 11
		}
*/

/*
		friend int operator ++ (Demo& obj){

                        return ++obj.x;
                }
*/

		int getData() {
			
			return ++x;
		}
};

int operator ++ (Demo& obj){

	return obj.getData();
}

int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
}

/*
 	unary_Operators.cpp: In function ‘int operator++(const Demo&)’:
	unary_Operators.cpp:13:38: error: increment of member ‘Demo::x’ in read-only object
	   13 |                         return ++obj.x;
	      |                                  ~~~~^
*/
