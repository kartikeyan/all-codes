
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

		int operator ++ (int){
			
			int temp = x;

			++x;

			return temp;		
		}

};



int main(){
	
	Demo obj;

	std::cout << obj++ << std::endl;	// 10
}


