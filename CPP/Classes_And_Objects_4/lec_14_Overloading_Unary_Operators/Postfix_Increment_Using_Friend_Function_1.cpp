
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:

		int operator ++ (){

                        return ++x;
               }
};


int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;	// 11
						
	std::cout << obj++ << std::endl;	// 11
}

/*
 	Postfix_Increment_Using_Friend_Function_1.cpp: In function ‘int main()’:
	Postfix_Increment_Using_Friend_Function_1.cpp:25:25: error: no ‘operator++(int)’ declared for postfix ‘++’ [-fpermissive]
	   25 |         std::cout << obj++ << std::endl;        // 11
	      |  
*/
