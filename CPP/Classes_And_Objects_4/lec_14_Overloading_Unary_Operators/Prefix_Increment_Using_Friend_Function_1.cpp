
//	unary Operators prefix Increment/Decrement And postfix Increment/Decrement

#include<iostream>

class Demo{
	
	int x = 10;

	public:
		friend int operator ++ (const Demo& obj){
			
			return ++obj.x;
		}
};

int main(){
	
	Demo obj;

	std::cout << ++obj << std::endl;
}

/*
 	unary_Operators.cpp: In function ‘int operator++(const Demo&)’:
	unary_Operators.cpp:13:38: error: increment of member ‘Demo::x’ in read-only object
	   13 |                         return ++obj.x;
	      |                                  ~~~~^
*/
