
#include<iostream>

class Demo{
	
	int x = 10;

	public:
		friend void* operator new(size_t size){
		
			std::cout << "Here" << std::endl;

			void *ptr = malloc(size);

			return ptr;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}

		void operator delete(void *ptr){
			
			std::cout << "Destructor" <<std::endl;
			free(ptr);
		}
};

int main(){
	
	Demo *obj = new Demo();		// internally ==>  Demo *obj = operator new (sizeof(Demo))
					// 			
					// 			==> void *operator new(size_t size)
					//
					// 				==> void *ptr = malloc(size)

	obj->getData();

	delete obj;
}

/*
 	Here
	10
	Destructor
*/
