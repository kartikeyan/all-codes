
#include<iostream>

class Demo{	
	
	int arr[5] = {10,20,30,40,50};	// array size should be declared first
	
	public:
		void getArray(){
			
			for(int i = 0; i < (sizeof(arr) / sizeof(int)); i++){
				
				std::cout << arr[i] << " ";
			}

			std::cout << std::endl;
		}
};

int main(){
	
	Demo obj;
		
	obj.getArray();
}
