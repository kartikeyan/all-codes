
/*
 	
    Default and Parametrized Constructor madhun dusrya constructor la call kela tar navin object banto

*/

#include<iostream>

class Demo{
	
	public:
		int x = 10;

		Demo(){
			
			std::cout << "In No-Args" << std::endl;
			std::cout << x << std::endl;
		}

		Demo(int x){
			
			this->x = x;

			std::cout << "In Const Para" << std::endl;

			std::cout << x << std::endl;

			Demo();		// ithe navin object banto, ani default value yeto x cha, navin nahi.
		}

		void getData(){
			
			std::cout << x << std::endl;
		}

		~Demo(){
			
			std::cout << "Destructor" << std::endl;
		}
};

int main(){
	
	Demo obj(50);

	std::cout << "End Main" << std::endl;
}

/*
 	In Const Para
	50
	In No-Args
	10
	Destructor
	End Main
	Destructor
*/

//	Destructor don yetat mhnje object don ahet.
