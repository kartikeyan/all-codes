
/*
 	1] Baherchya jagasathi object constant ahe, pan class madhe consatnt nahi ahe

	2] Jevha object constant asto, tevha object la restrictions ahet
	
	3] Instance variable cha data change hau naye mahnun constant objects vaarto
*/

#include<iostream>

class Demo{
	
	public:
		int x = 10;

		Demo(){
			
			std::cout << "No Args Const" << std::endl;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}
};

int main(){
	
	const Demo obj;

	obj.getData();
}

/*
 	constant_Objects_2.cpp: In function ‘int main()’:
	constant_Objects_2.cpp:24:20: error: passing ‘const Demo’ as ‘this’ argument discards qualifiers [-fpermissive]
	   24 |         obj.getData();
	      |         ~~~~~~~~~~~^~
	constant_Objects_2.cpp:14:22: note:   in call to ‘void Demo::getData()’
	   14 |                 void getData(){
	      |                      ^~~~~~~
*/
