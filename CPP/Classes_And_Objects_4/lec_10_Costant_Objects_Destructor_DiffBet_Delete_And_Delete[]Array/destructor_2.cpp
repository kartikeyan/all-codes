
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "In Constructor" << std::endl;
		}

		~Demo(){
			
			std::cout << "In Destructor" << std::endl;
		}
};

int main(){
	
	Demo *obj2 = new Demo();

	std::cout << "End Main" << std::endl;
}

/*	
 *	new ne object banavlya var object heap var memory banto, tar main sampla tar destructor la implicitly call jat nahi.
 *
 *	Tar aplyala delete la call karava lagto
*/	

/*
	In Constructor
	End Main
*/
