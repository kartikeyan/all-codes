
/*
 	1] Baherchya jagasathi object constant ahe, pan class madhe consatnt nahi ahe

	2] Jevha object constant asto, tevha object la restrictions ahet
	
	3] Instance variable cha data change hau naye mahnun constant objects vaarto
*/

#include<iostream>

class Demo{
	
	public:
		int x = 10;

		Demo(){
			
			std::cout << "No Args Const" << std::endl;
		}

		void getData() const{
			
			std::cout << x << std::endl;
		}
};

int main(){
	
	const Demo obj;

	obj.getData();
}

/*
 	No Args Const
	10
*/
