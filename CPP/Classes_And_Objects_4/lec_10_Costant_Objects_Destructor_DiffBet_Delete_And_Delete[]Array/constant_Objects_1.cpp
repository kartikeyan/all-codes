
#include<iostream>

class Demo{
	
	public:
		int x = 10;

		Demo(){
			
			std::cout << "No Args Const" << std::endl;
		}

		void getData(){
			
			std::cout << x << std::endl;
		}
};

int main(){
	
	const Demo obj;

	std::cout << obj.x << std::endl;
}

/*
 	No Args Const
	10
*/
