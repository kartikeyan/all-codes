
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "In Constructor" << std::endl;
		}

		~Demo(){
			
			std::cout << "In Destructor" << std::endl;
		}
};

int main(){
	
	Demo obj1;

	std::cout << "End Main" << std::endl;
}

//	main sampla tar destructor la call implicitly call jato 

/*
 	In Constructor
	End Main
	In Destructor
*/
