
/*
 	1] Baherchya jagasathi object constant ahe, pan class madhe consatnt nahi ahe

	2] Jevha object constant asto, tevha object la restrictions ahet
	
	3] Instance variable cha data change hau naye mahnun constant objects vaarto
*/

#include<iostream>

class Demo{
	
	public:
		int x = 10;

		Demo(){
			
			std::cout << "No Args Const" << std::endl;
		}

		void getData() const{
			
			std::cout << x << std::endl;
		}
};

int main(){
	
	const Demo obj;

	obj.getData();

	obj.x = 50;

	obj.getData();
}

/*
 	constant_Objects_4.cpp: In function ‘int main()’:
	constant_Objects_4.cpp:34:15: error: assignment of member ‘Demo::x’ in read-only object
	   34 |         obj.x = 50;
	      |         ~~~~~~^~~~
*/
