#include<iostream>

class Demo{
	
	int *ptrArray = NULL;

	public:
		Demo(){
			
			ptrArray = new int[50];

			for(int i = 0; i < 50; i++){
				
				ptrArray[i] = 0;
			}

			std::cout << "In Const" << std::endl;
		}

		~Demo(){
			
			delete[] ptrArray;
			std::cout << "In Dest" << std::endl;
		}
};

int main(){
	
	Demo obj;
}

/*
 	In Const
	In Dest
*/
