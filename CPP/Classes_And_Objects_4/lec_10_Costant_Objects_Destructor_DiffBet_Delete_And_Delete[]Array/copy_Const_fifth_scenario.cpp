
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "No Args" << std::endl;
		}

		Demo(int x){
			
			std::cout << "Para" << std::endl;
		}

		Demo(Demo &ref){
			
			std::cout << "copy" << std::endl;
		}
};

int main(){
	
	Demo obj1;

	Demo obj2;

	Demo obj3;

	Demo arr[] = {obj1, obj2, obj3};
}

/*
 	No Args
	No Args
	No Args
	copy
	copy
	copy
*/
