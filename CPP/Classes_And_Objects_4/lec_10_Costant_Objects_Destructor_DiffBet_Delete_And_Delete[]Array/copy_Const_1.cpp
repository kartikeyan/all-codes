
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "No Args Const" << std::endl;
		}

		Demo(int x){
			
			std::cout << "Para Const" << std::endl;
		}

		Demo(Demo &obj){
			
			std::cout << "Copy Const" << std::endl;
		}

		Demo fun(){
			
			Demo obj3(10);

			return obj3;
		}
};

int main(){
	
	Demo obj1;

	Demo obj2 = obj1;

	obj2.fun();
}

/*
 	No Args Const
	Copy Const
	Para Const
*/
