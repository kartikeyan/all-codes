
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "No Args Const" << std::endl;
		}

		Demo(int x){
			
			std::cout << "Para Const" << std::endl;
		}

		Demo(Demo &obj){
			
			std::cout << "Copy Const" << std::endl;
		}

		Demo& fun(){			
			
			Demo obj3(10);

			return obj3;
		}
};

int main(){
	
	Demo obj1;

	Demo obj2 = obj1;

	Demo obj3 = obj2.fun();
}

/*
 	copy_Const_3.cpp: In member function ‘Demo& Demo::fun()’:
	copy_Const_3.cpp:26:32: warning: reference to local variable ‘obj3’ returned [-Wreturn-local-addr]
	   26 |                         return obj3;
	      |                                ^~~~
	copy_Const_3.cpp:24:30: note: declared here
	   24 |                         Demo obj3(10);
	      |                              ^~~~
	
	
	katikeyan@kartikeyan:~/all-codes/CPP/Classes_And_Objects_4/lec_10_Costant_Objects_Destructor_DiffBet_Delete_And_Delete[]Array$ ./a.out 
	No Args Const
	Copy Const
	Para Const
	Copy Const
*/
