
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "In Constructor" << std::endl;
		}

		~Demo(){
			
			std::cout << "In Destructor" << std::endl;
		}
};

int main(){
	
	Demo obj1;

	std::cout << "End Main" << std::endl;

	delete obj1;	
}

/*	
 	destructor_4.cpp: In function ‘int main()’:
	destructor_4.cpp:24:16: error: type ‘class Demo’ argument given to ‘delete’, expected pointer
	   24 |         delete obj1;
	      |                ^~~~
*/	

//	fakt new ne object banavlyavar delete vapru shakto.
