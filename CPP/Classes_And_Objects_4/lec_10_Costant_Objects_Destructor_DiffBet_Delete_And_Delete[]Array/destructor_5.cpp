
#include<iostream>

class Demo{
	
	public:
		Demo(){
			
			std::cout << "In Constructor" << std::endl;
		}

		~Demo(){
			
			std::cout << "In Destructor" << std::endl;
		}
};

int main(){
	
	Demo obj1;
	
	Demo *obj2 = new Demo();

	std::cout << "End Main" << std::endl;

	delete obj2;	
}

/*	
 	In Constructor
	In Constructor
	End Main
	In Destructor
	In Destructor
*/	

//	fakt new ne object banavlyavar delete vapru shakto.
