/*	Program 2 : WAP to calculate the count of even and odd elements.
 *		    Take array size and array elements from the user
 *
 *		    i/p : enter array : 10 12 13 15 16 17 19 20 22 23
*/

#include<stdio.h>
void main(){
	int size;
	printf("Enter array size :\n");
	scanf("%d", &size);

	int arr[size];
	printf("Enter array :\n");
	for(int i = 0; i < size; i++){
		scanf("%d", &arr[i]);
	}

	int ecount = 0;
	int ocount = 0;

	for(int i = 0; i < size; i++){
		if(arr[i] % 2 == 0){
			ecount++;
		}else{
			ocount++;
		}
	}

	printf("even count is :%d\n",ecount);
	printf("odd count is :%d\n",ocount);
}
