/*	Program 10 :
*/

#include<stdio.h>
void main(){
	
	int arr1[] = {10,20,30,40,50};
	int arr2[] = {70,70,80,90,100};

	int *ptr1 = NULL;
	int *ptr2 = NULL;

	ptr1 = arr1 + 3;
	ptr2 = arr2 + 2;

	*ptr1 = 35;
	
	printf("array1 is :\n");
	for(int i = 0; i < 5; i++){
		printf("%d ", arr1[i]);
	}
	
	printf("Array2 is :\n");
	for(int i = 0; i < 5; i++){
		printf("%d ", arr2[i]);
	}
}
