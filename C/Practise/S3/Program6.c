/*	Program 6 : WAP to swap values of two numbers using a pointer.
 *			
 *		    i/p : x = 10
 *		    	  y = 20
 *		    
 *		    o/p : After swapping
 *		    	  x = 20
 *		    	  y = 10
*/
#include<stdio.h>
void main(){
	int x = 10;
	int y = 20;

	int *iptr1 = &x;
	int *iptr2 = &y;
	
	printf("%d\n", *iptr1);
	printf("%d\n", *iptr2);

	iptr1++;
	iptr2--;

	printf("%d\n", *iptr1);
	printf("%d\n", *iptr2);
}
