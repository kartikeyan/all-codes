/*	Program 5 :
*/

#include<stdio.h>
void main(){
	int x = 10;
	int *ptr = &x;
	char *cptr = &x; 	 /* warning : initialization of "char *" to
       					      incompatible pointer type "int *"
				*/

	printf("%d\n", *ptr);   // 10
	printf("%d\n",cptr); 	/* warning : format %d expects argument of type int,
       				             but argument 2 has type "char *"  
				*
				* o/p : half address as format specifier is "%d"
				*/
}
