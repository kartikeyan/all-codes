/*	Program 3 : WAP to add two different array of the same size.
 *		    Take array size and array elements from the user
 *
 *		    i/p : enter 1st array : 10 12 13 15
 *		    	  enter 2nd array : 1 2 3 4
 *		    o/p : 11 14 16 19
*/

#include<stdio.h>
void main(){
	int size1;
	printf("Enter array1 size :\n");
	scanf("%d", &size1);

	int arr1[size1];
	printf("Enter array1 :\n");
	for(int i = 0; i < size1; i++){
		scanf("%d", &arr1[i]);
	}

	int size2;
	printf("Enter array2 size :\n");
	scanf("%d", &size2);

	int arr2[size2];
	printf("Enter array2 :\n");
	for(int i = 0; i < size2; i++){
		scanf("%d", &arr2[i]);
	}
	
	int add = 0;
	
	add = arr1[i] + arr2[i];

	printf("%d is:\t", add);
}
