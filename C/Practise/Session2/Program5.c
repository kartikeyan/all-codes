/*	Program 5:WAP to print all even numbers in reverse order and odd numbers the standard way.
 *		  Both separately.Wthin a range.
 *
 *		 Input : start : 2
 *		 	 end : 9
 *		 Output : 8 6 4 2 
 *		 	  3 5 7 9
 *
*/

#include<stdio.h>

void main(){
	int x, y;
	printf("number start and end is :\n");
	scanf("%d %d", &x, &y);

	if(x < y){
//		printf("Even numbers are:");

		for(int i = y; i >= x; i--){
			if(i % 2 == 0){
				printf("%d", i);
			}
		}
		
		printf("\n");
//		printf("odd numbers are :");

		for(int i=x; i<=y; i++){
			if(i % 2 != 0){
				printf("%d", i);
			}
		}
	}
}
