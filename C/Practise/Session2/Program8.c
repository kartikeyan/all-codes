#include<stdio.h>

void main(){
	int x,rem;
	int prod = 1;

	printf("Enter number:\n");
	scanf("%d", &x);

	while(x != 0){
		rem = x % 10;
		prod = prod * rem;
		x = x / 10;
	}
	printf("product of digits is %d\n", prod);
}
