/*	Program 2:WAP to print addition of 1 to 10 with 10 to 1
 *	
 *	output : 1 + 10 = 11
 *		 2 + 9 = 11
 *		 3 + 8 = 11
 *		 .
 *		 .
 *		 .
 *		 .
 *		 10 + 1 = 11
*/

#include<stdio.h>

void main(){
	
	int x,y = 0;
	printf("Enter value of x :\n");
	scanf("%d", &x);
	y = x;
	for(int i=1; i<=x; i++) {
		printf("%d + %d = %d\n",i, y,(i+y));
		y--;

	}
}
