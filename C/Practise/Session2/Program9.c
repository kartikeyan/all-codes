#include<stdio.h>

void main(){

	int x, rem, rev = 0;

	printf("Enter number x :\n");
	scanf("%d", &x);

	while(x > 0){
		rem = x % 10;
		rev = rev * 10 + rem;
		x = x / 10;
	}
	printf("%d\n", rev);
}
