/*	Program 8 :  Write a program that accepts a singly Linked List from the user.
 *		     Take a number from the user and only keep the elements that are equal
 *		     in length to that number and delete other elements.And print the linked list.
 *		     Length of Shashi = 6
 *
 *		     Input LInked List : |Shashi|->|Ashish|->|kanha|->|Rahul|->|Badhe|
 *
 *		     Ouput Linked List : |Shashi|->|Ashish|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;

	char str[20];

	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter String\n");
	
	getchar();

	int ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
	
		(*newNode).str[i] = ch;
		i++;
	}

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

int mystrlen(char *str){

	int count = 0;

	while(*str != '\0'){
	
		count++;
		str++;
	}
	return count;
}



void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%s|->", temp->str);

		temp = temp->next;
	}

	printf("|%s|\n", temp->str);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	rev();

	printLL();

}
