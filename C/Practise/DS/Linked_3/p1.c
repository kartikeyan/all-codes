/*	Program 1 :  Write a program that searches for the first occurance of a particular
 *		     element from a singly Linked List.
 *
 *		     Input LInked List : |10|->|20|->|30|->|40|->|50|->|60|->|70|
 *
 *		     Input : Enter Element : 30
 *
 *		     Ouput : 3
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;
	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

int firstOcc(){

	int data;
	printf("Enter Element\n");
	scanf("%d", &data);

	int count = 1;

	demo *temp = head;

	while(temp != NULL){
	
		if(head == NULL){
			printf("LL is empty\n");
			return -1;
		}else{
		
			if(temp->data == data){
			
				printf("%d\n", count);
				break;
			}

			count++;
		}

		temp = temp->next;
	}
}

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	firstOcc();
}
