/*	Program 7 :  Write a program that accepts a singly Linked List from the user.
 *		     Reverse the data elements from the linked list.
 *
 *		     Input Linked List : |Shashi|->|Ashish|->|kanha|->|Rahul|->|Badhe|
 *
 *		     Ouput Linked List : |ihsahS|->|hsihsA|->|ahnak|->|luhaR|->|ehdaB|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;

	char str[20];

	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter String\n");

	int ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
	
		(*newNode).str[i] = ch;
		i++;
	}

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

int mystrlen(char *str){

	int count = 0;

	while(*str != '\0'){
	
		count++;
		str++;
	}
	return count;
}

char *mystrrev(char *str){

	char *temp = str;

	while(*temp != '\0'){
	
		temp++;
	}

	temp--;

	while(str < temp){
	
		char x = *str;
		*str = *temp;
		*temp = x;

		str++;
		temp--;
	}

	return str;
}

void reverse(){

	demo *temp = head;

	while(temp != NULL){
		
		mystrrev(temp->str);

		temp = temp->next;
	}

	printf("\n");
}

/*void revData(){
	
	demo *temp = head;
	demo *temp1 = NULL;

	while(temp != NULL){
	
		int len = mystrlen(temp->str);

		int y ;
		if(len % 2 == 0 ){
			y = len;
		}else{
			y = len-1;
		}

		for(int i = 0; i < y/2; i++){
		
			char ch = (*temp1).str[len];

			(*temp1).str[len] = (*temp).str[i];

			(*temp).str[i] = ch;

			len--;
		}

		temp = temp->next;
	}
}*/

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%s|->", temp->str);

		temp = temp->next;
	}

	printf("|%s|\n", temp->str);
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);
	
	getchar();

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	if(head != NULL){
	
		reverse();

		printf("After reverse the elements\n");
		printLL();
	}else{
	
		printf("There is no nodes add some nodes\n");
	}
}
