/*	Program 5 : Write a demo structure consisting of integer data.
 *		    Take the number of nodes from the user & print
 *		    the addition of the integer data.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;

	struct Demo *next;
}demo;

demo *head = NULL;

int sum = 0;

void addNodes(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Integer data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void printLL(){
	
	int sum = 0;

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Integer data is : %d |", temp->data);

		sum += temp->data;

		temp = temp->next;
	}

	printf("\n");
	printf("Sum of integer data is :%d", sum);
	printf("\n");
}

void main(){

	int nodes;
	printf("Enter No. of Nodes\n");
	scanf("%d", &nodes);

	for(int i = 0; i < nodes; i++){
	
		addNodes();
	}
	
	printLL();
}
