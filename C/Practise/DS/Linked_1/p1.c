/*	Program 1 : WAP for the LinkedList of malls consisting of its name, number of shops, & revenue
 *		    Connect 3 malls in the LinkedList & print their data
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Malls{

	char mName[20];
	int noShops;
	float revenue;	

	struct Malls *next;
}mall;

mall *head = NULL;

void addNodes(){

	mall *newNode = (mall*)malloc(sizeof(mall));

	printf("Enter Mall Name\n");
//	fgets(newNode->mName, 15, stdin);
	scanf("%s", newNode->mName);

	printf("Enter Number of Shops\n");
	scanf("%d", &newNode->noShops);

	printf("Enter Mall Revenue\n");
	scanf("%f", &newNode->revenue);

	getchar();

	newNode->next = NULL;

	if(head == NULL){
	
		head = newNode;
	}else{
	
		mall *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void printLL(){

	mall *temp = head;

	while(temp != NULL){
	
		printf("|Mall Name is :%s ->", temp->mName);
		printf("No. of shops :%d ->", temp->noShops);
		printf("Mall revenue :%f |", temp->revenue);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodes;
	printf("Enter No. of Nodes\n");
	scanf("%d", &nodes);

	for(int i = 0; i < nodes; i++){
	
		addNodes();
	}

	printLL();
}
