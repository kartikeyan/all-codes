
/*	Program 3 : Given an array with a DISTINCT element, which is formed by.
 *		    roatating array K times
 *		    Given an X element.
 *		    Find its index of it in the given array.
 *		    [Note : k value not given]
 *
 *		    input : arr[4,5,6,7,0,1,2]
 *
 *		    Ouput:  k = 0,   index = 4
 *		    	    k = 3,   index = -1
 *		    	    
*/

#include<stdio.h>

int linearSearch(int arr[], int size, int search){

	for(int i = 0; i < size; i++){
		
		if(arr[i] == search){
			
			return i;
		}
	}

	return -1;
}

int binarySearch(int arr[], int size, int search){

	int start = 0, end = size - 1, mid;

	while(start <= mid){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] < arr[start]){
			
			if(arr[mid] < search && arr[end] >= search){
				
				start = mid + 1;
			}else{
				
				end = mid - 1;
			}
		}

		if(arr[mid] > arr[start]){
			
			if(arr[mid] > search && arr[start] <= search){
				
				end = mid - 1;
			}else{
				start = mid + 1;
			}
		}
	}

	return -1;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret1 = linearSearch(arr, size, search);

	int ret2 = binarySearch(arr, size, search);

	printf("%d %d\n", ret1,  ret2);
}
