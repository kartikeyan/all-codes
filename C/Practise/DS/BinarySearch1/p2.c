
/*	Program 2 : Given an sorted array Arr.
 *		    Find the first Occurance k.
 *		    If found then return index else return -1.
 *
 *		    input : arr[2,3,9,9,10,10,10,18]
 *
 *		    Ouput:  k = 9,   index = 2
 *		    	    k = 10,   index = 4
 *		    	    
*/

#include<stdio.h>

int linearSearch(int arr[], int size, int search){

	for(int i = 0; i < size; i++){
		
		if(arr[i] == search){
			
			return i;
		}
	}

	return -1;
}

int binarySearch(int arr[], int size, int search){

	int start = 0, end = size - 1, mid;
	
	int store = -1;

	while(start <= mid){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			store = mid;

			if(arr[mid - 1] != search){
				
				return store;
			}

			end = mid - 1;
		}

		if(arr[mid] > search){
			
//			store = arr[mid];
			end = mid - 1;
		}

		if(arr[mid] < search){
			
			start = mid + 1;
		}
	}

	return store;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret1 = linearSearch(arr, size, search);

	int ret2 = binarySearch(arr, size, search);

	printf("%d %d\n", ret1,  ret2);
}
