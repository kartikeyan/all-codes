/*	PRogram 1 : Write a program that searches for the first occurance of a particular
 *		    a particular element from a singly Linked List
 *
 *		    Input Linked List : |10|->|20|->|30|->|40|->|50|->|60|->|70|
 *
 *		    Input : Enter Element : 30
 *
 *		    Output : 3
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{
	
	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void FindOcc(){

	int data;
	printf("Enter Data to find occurances\n");
	scanf("%d", &data);

	node *temp = head;

	int count = 1;

	while(temp != NULL){
	
		if(head == NULL){
			
			printf("Linked List is Empty\n");
		}else{
		
			if(temp->data == data){
			
				printf("%d\n", count);
				break;
			}

			count++;
		}

		temp = temp->next;
	}
}

void main(){

	int nodeCount;
	printf("Enter Node COunt\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	FindOcc();
}
