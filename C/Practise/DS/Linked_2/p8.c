/*	PRogram 8 : Write a program that accepts a singly linear linked list from the user.
 *		    Take a number from the user and only keep the elements that are equal in
 *		    length to that number and delete other elements. And print the Linked list.
 *		    Length of Shashi = 6
 *
 *		    Input Linked List : |Shashi|->|Ashish|->|Kanha|->|Rahul|->|Badhe|
 *
 *		    Output Linked List : |Shashi|->|Ashish|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct node{

	char str[20];
	struct node *next; 
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter String\n");

	getchar();

	int ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
		
		(*newNode).str[i] = ch;
		i++;
	}

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int mystrlen(char *str){

	int count = 0;

	while(*str != '\0'){
	
		count++;
		str++;
	}

	return count;
}

int countNodes(){

	int count = 0;

	node *temp = head;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	return count;
}

int deleteFirst(){

	if(head == NULL){
		printf("Nothing to delete\n");
		return -1;
	}else{
	
		if(head->next == NULL){
		
			free(head);
			head = NULL;
		}else{
		
			node *temp = head;

			head = head->next;
			free(temp);
		}
		return 0;
	}
}

int deleteLast(){

	if(head == NULL){
	
		printf("Nothing to delete\n");
		return -1;
	}else{
	
		if(head->next == NULL){
		
			free(head);
			head = NULL;
		}else{
		
			node *temp = head;

			while(temp->next->next != NULL){
			
				temp = temp->next;
			}

			free(temp->next);
			temp->next = NULL;
		}

		return 0;
	}
}

int deleteAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos >= count + 2){
	
		printf("Invalid pos\n");
		return -1;
	}else{
	
		if(pos == 1){
		
			deleteFirst();
		}else if(pos == count){
		
			deleteLast();
		}else{
		
			node *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			node *temp2 = temp->next;
			temp->next = temp->next->next;
			free(temp2);
		}

		return 0;
	}
}

void compare(int length){

	int pos = 0;

	node *temp = head;

	while(temp != NULL){
	
		pos++;

		int strlen = mystrlen(temp->str);

		if(strlen != length){
		
			deleteAtPos(pos);
			pos--;
		}

		temp = temp->next;
	}

	if(pos == 0){
	
		printf("There is no node of same length %d\n", length);
	}
}

int printLL(){

	if(head == NULL){
	
		printf("Nothing to print\n");
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			printf("|%s|->", temp->str);

			temp = temp->next;
		}

		printf("|%s|\n", temp->str);
	}
}

void main(){

	int nodeCount, length;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);
	
	getchar();

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	if(head != NULL){

		printf("\nEnter Length to compare :\n");
		scanf("%d", &length);

		compare(length);
		printLL();
	}else{
	
		printf("There is no nodes to compare\n");
	}
}
