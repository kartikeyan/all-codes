/*	
 *	Program 2 : WAP that searches all accepts two singly linear
 *		    lists from the user and concate source linked list
 *		    after destination linked list
 *
 *	Input LL : 
 *		       Source LL  : |30|->|30|->|70|
 *		   Destination LL : |10|->|20|->|30|->|40|
 *
 *	Output LL : 
 *		   Destination LL : |10|->|20|->|30|->|40|->|30|->|30|->|70|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int no;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter No\n");
	scanf("%d", &newNode->no);

	newNode->next = NULL;

	return newNode;
}

void addNode(struct Node **head){

	node *newNode = createNode();

	if(*head == NULL){
		*head = newNode;
	}else{
	
		node *temp = *head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void concateLL(){

	node *temp = head2;

	while(temp->next != NULL){
	
		temp = temp->next;
	}

	temp->next = head1;
}

int printLL(node *head){

	if(head == NULL){
		
		printf("LL is empty\n");
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->no);

			temp = temp->next;
		}

		printf("|%d|\n", temp->no);
	}
}

void main(){

	int nodeCount;
	printf("Enter Node Count : SourceLL\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head1);
	}

	printf("Enter Node Count : DestinationLL\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head2);
	}

	printLL(head1);

	printLL(head2);

	concateLL();

	printLL(head2);
}
