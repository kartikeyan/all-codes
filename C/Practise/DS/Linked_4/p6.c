/*	
 *	Program 6 : WAP a program that copies the first N contents 
 *		    of a singly linear source linked list to a
 *		    destination singly linear linked list
 *
 *	Input LL : 
 *		       Source LL  : |30|->|30|->|70|->|80|->|90|->|100|
 *
 *	Input no : 4
 *
 *	Output LL : 
 *		   Destination LL : |30|->|30|->|70|->|80|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int no;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;
int flag = 0;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter No\n");
	scanf("%d", &newNode->no);

	newNode->next = NULL;

	return newNode;
}

int addNode(){

	node *newNode = createNode();
	node *temp = NULL;

	if(flag == 1){
		
		*temp = head1;

		if(head1 == NULL){
			head1 = newNode;
		}
	}else{
		
		*temp = head2;

		if(head2 == NULL){
			head2 = newNode;
		}
	}

	if(temp != NULL){
		
		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}

	return 0;
}

int countNodes(node *temp){

	node *temp = head1;
	
	int count = 0;
	
	if(temp == NULL){
		
		printf("Empty LinkedList\n");
		return 0;
	}else{

		while(temp != NULL){
		
			count++;

			temp = temp->next;
		}

		return count;
	}
}

node *concat(int no){

	if(head1 == NULL){
		
		printf("Invalid operation\n");
		return NULL;
	}else{
		
		node *temp = NULL;

		if(head2 != NULL){
		
			temp = head2;

			while(temp->next != NULL){
				
				temp = temp->next;
			}
		}

		node *temp2 = head1;

		while(no--){
			
			node *newNode = (node*)malloc(sizeof(node));

			newNode->no = temp2->no;

			if(head2 == NULL){
			
				head2 = newNode;
				temp = newNode;
			}else{
				temp->next = newNode;
				temp = temp->next;
			}

			temp2 = temp2->next;
		}
	}

	return head2;
}

int printLL(node *temp){

	if(temp == NULL){
		
		printf("LL is empty\n");
		return -1;
	}else{

		while(temp->next != NULL){
		
			printf("|%d|->", temp->no);

			temp = temp->next;
		}

		printf("|%d|\n", temp->no);
	}
}

void main(){

	printf("Copy First\n");

	char ch;

	do{
		printf("1.Add Nodes\n2.Copy First N\n3.printLL\n");
		printf("Enter Your Choice\n");

		int choice;
		printf("Enter Choice\n");
		scanf("%d", &choice);

		switch(choice){
		
			case 1:
				{
					int no1 = 0;
					printf("In which LinkedList do u want to add node?\n1.Source\n2.Destination\n");
					scanf("%d", &no1);

					for(int i = 0; i < no1; i++){
						addNode();
					}
				}
				break;
			case 2:
				{
					if(countNodes(head1) <= 0){
						
						printf("Source LinkedList is empty, can't copy\n");
					}else{
						printf("How many nodes do u want to copy\n");
						int no;
						scanf("%d", &no);

						if(no <= 0 || no >countNodes(head1)){
							
							do{
								printf("IN valid range, please re-enter the range :");
								scanf("%d", &no);
							}while(no <= 0 || no >countNodes(head1));
						}

						head2 = concat(no);
					}
					
				}
				break;

			case 3:
				{
					int no;
					printf("Which LL do u want to print?\n1.source\n2.dest\n");
					scanf("%d", &no);

					if(no == 1){
					
						printf("Source LL : \n");
						printLL(head1);
					}else{
						
						printf("Dest LL : \n");
						printLL(head2);
					}
				}
				break;
			default:
				{
					printf("Invalid choice, please re-enter the choice\n");
					main();
				}
		}
		
		getchar();
		printf("Do u want to continue ? [press enter]");
		scanf("%c", &ch);
	}while(ch == '\n');
}
