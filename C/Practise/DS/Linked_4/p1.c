/*	
 *	Program 1 : WAP that searches all occurances of a particular element
 *		    from a singly linear linked list
 *
 *	Input LL : |10|->|20|->|30|->|40|->|30|->|30|->|70|
 *
 *	Input element : 30
 *
 *	Output : 3
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int no;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter No\n");
	scanf("%d", &newNode->no);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	node *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int search(){

	if(head == NULL){
		
		printf("LL is empty\n");
		return -1;
	}else{
		
		int element;
		printf("Enter Input Element\n");
		scanf("%d", &element);
		
		int count = 1;

		node *temp = head;

		while(temp != NULL){
		
			if(temp->no == element){
			
				printf("%d\n", count);
				break;
			}

			count++;

			temp = temp->next;
		}

		return 0;
	}

}

int printLL(){

	if(head == NULL){
		
		printf("LL is empty\n");
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->no);

			temp = temp->next;
		}

		printf("|%d|\n", temp->no);
	}
}

void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	search();
}
