/*	
 *	Program 3 : WAP that accepts two singly linear lists from the user and 
 *		    concate the first N elements of the source linked list
 *		    after destination linked list
 *
 *	Input LL : 
 *		       Source LL  : |30|->|30|->|70|
 *		   Destination LL : |10|->|20|->|30|->|40|
 *
 *	Input Number of Elements : 2
 *
 *	Output LL : 
 *		   Destination LL : |10|->|20|->|30|->|40|->|30|->|30|
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int no;
	struct Node *next;
}node;

node *head1 = NULL;
node *head2 = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter No\n");
	scanf("%d", &newNode->no);

	newNode->next = NULL;

	return newNode;
}

void addNode(struct Node **head){

	node *newNode = createNode();

	if(*head == NULL){
		*head = newNode;
	}else{
	
		node *temp = *head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int countNodes(){

	node *temp = head1;
	
	int count = 0;

	while(temp != NULL){
		
		count++;

		temp = temp->next;
	}

	return count;
}

void concateLL(){

	node *temp = head2;

	while(temp->next != NULL){
	
		temp = temp->next;
	}

	temp->next = head1;
}

int concateFirstN(int num){
	
	int count = countNodes();

	if(num < 0 || num > count){
		
		printf("Invalid\n");
		return -1;

	}else if(num == 0){
		
		concateLL();
	}else{

		node *temp = head2;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		int val = num - 1;

		node *temp2 = head1;

		while(val){
		
			temp2 = temp2->next;
			val--;
		}

		free(temp2->next);
		temp2->next = NULL;

		temp->next = head1;
	}
}

int printLL(node *head){

	if(head == NULL){
		
		printf("LL is empty\n");
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->no);

			temp = temp->next;
		}

		printf("|%d|\n", temp->no);
	}
}

void main(){

	int nodeCount;
	printf("Enter Node Count : SourceLL\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head1);
	}

	printf("Enter Node Count : DestinationLL\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode(&head2);
	}

	printLL(head1);

	printLL(head2);

	int num;
	printf("Enter No of Nodes to connect\n");
	scanf("%d", &num);

	concateFirstN(num);

	printLL(head2);
}
