/* 	Program 5:WAP take a number from 0 to 5 and prints its spelling,
 *  		  if the no. is greater than 5 print entered no. is greater than 5
*/

#include<stdio.h>

void main(){
	
	int num;
	printf("Enter number from 0 to 5\n");
	scanf("%d", &num);

	switch(num){
	
		case 0:
			printf("ZERO\n");
			break;
		case 1:
			printf("ONE\n");
			break;
		case 2:
			printf("Two\n");
			break;
		case 3:
			printf("Three\n");
			break;
		case 4:
			printf("Four\n");
			break;
		case 5:
			printf("Five\n");
			break;
		case 6:
			printf("SIX\n");
			break;
		default:
			printf("Wrong input\n");

	}
}
