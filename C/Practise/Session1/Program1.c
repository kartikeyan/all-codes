/*	WAP : WAP to take check whether the given input 
 *	      is leap year or not
*/	      	      

#include<stdio.h>

void main(){

	int ly;

	printf("Enter value of ly :\n");
	scanf("%d", &ly);
	
	if(ly % 4 == 0){
		printf("%d is a leap year\n", ly);
	}else{
		printf("%d is not a leap year\n", ly);
	}
}
