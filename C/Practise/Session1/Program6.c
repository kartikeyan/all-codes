/*	Program 6: WAP to get 10 numbers from users and find their sum and average
*/

#include<stdio.h>

void main(){

	int x,y;
	int z = 0;
	int sum = 0;
	float avg = 0;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	for(int i=x; i<=y; i++){
		
		scanf("%d",&z);

		sum = sum + z;
		avg = sum / z;
	}

	printf("Sum is :%d\n", sum);
	printf("Avg is :%f\n", avg);
}
