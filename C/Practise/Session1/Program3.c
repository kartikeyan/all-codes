/*	Program 3: WAP to find min among 3 numbers
*/

#include<stdio.h>

void main(){

	int x,y,z;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	printf("Enter value of z :\n");
	scanf("%d", &z);

	if(x < y && x < z){
		printf("x is less than y and z\n");
	}else if(y < x && y < z){
		printf("y is less than x and z\n");
	}else if(z < x && z < y){
		printf("z is less than x and y\n");
	}else if(x == y == z){
		printf("x,y,z are all equal\n");
	}else{
		printf("Wrong input\n");
	}
}

