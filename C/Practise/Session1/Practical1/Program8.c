/*	Program 8:WAP that takes angles of a triangle from the user
 *		  and print whether that triangle is valid or not
*/

#include<stdio.h>

void main(){
	int a1, a2, a3;

	printf("Enter anngles of a1, a2, a3 :\n");
	scanf("%d %d %d", &a1,&a2,&a3);

	int sum = a1 + a2 + a3;

	if(sum == 180){
		printf("Triangle is valid\n");
	}else{
		printf("Triangle is invalid\n");
	}
}
