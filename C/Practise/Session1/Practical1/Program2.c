/*	Program 2 : WAP to find max among 3 numbers
*/

#include<stdio.h>

void main(){

	int x,y,z;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	printf("Enter value of z :\n");
	scanf("%d", &z);

	if(x > y && x > z){
		printf("%d is largest number\n", x);
	}else if(y > x && y > z){
		printf("%d is largest number\n", y);
	}else{
		printf("%d is largest number\n", z);
	}
}
