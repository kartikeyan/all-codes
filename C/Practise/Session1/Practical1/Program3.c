/*	Program 3:WAP to find min among 3 numbers
*/

#include<stdio.h>

void main(){
	int x,y,z;

	printf("Enter values of x,y,z :\n");
	scanf("%d %d %d", &x, &y, &z);

	if(x < y && x < z){
		printf("%d is smallest number\n", x);
	}else if(y < x && y < z){
		printf("%d is smallest number\n", y);
	}else{
		printf("%d is smallest number\n", z);
	}
}
