/*	Program 5:WAP that takes a number from 1 to 5 and
 *		  prints its speeling, if the number is greater than 5
 *		  print entered number is greater than 5.
*/

#include<stdio.h>

void main(){

	int num;

	printf("Number from 1 to 5 :\n");
	scanf("%d", &num);

	switch(num){
	
		case 1:
			printf("One\n");
			break;
		case 2:
			printf("Two\n");
			break;
		case 3:
			printf("Three\n");
			break;
		case 4:
			printf("Four\n");
			break;
		case 5:
			printf("Five\n");
			break;
		default:
			printf("Entered number is greater than 5\n");
	}
}
