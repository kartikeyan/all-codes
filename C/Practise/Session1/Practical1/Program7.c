/*	Program 7 :WAP to check whether the given input is a 
 *		   pythagorian triplet or not
*/

#include<stdio.h>

void main(){
	int a,b,c;

	printf("Enter value of a,b,c :\n");
	scanf("%d %d %d", &a, &b, &c);

	if(a * a + b * b == c*c){
		printf("%d %d %d are pythagorian triplets\n", a, b, c);
	}else{
		printf("%d %d %d are not pythagorian triplets\n", a, b, c);
	}

}
