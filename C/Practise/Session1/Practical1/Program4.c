/*	Program 4:WAP in which according to the month number
 *		  print the no. of days in that month
*/

#include<stdio.h>

void main(){
	
	int month;

	printf("Enter month number :\n");
	scanf("%d", &month);

	switch(month){
	
		case 1 :
			printf("31 days\n");
			break;

		case 2 :
			printf("28/29 days\n");
			break;

		case 3 :
			printf("31\n");
			break;

		case 4 :
			printf("30\n");
			break;

		case 5 :
			printf("31\n");
			break;

		case 6 :
			printf("30\n");
			break;

		case 7 :
			printf("31\n");
			break;

		case 8 :
			printf("31\n");
			break;

		case 9 :
			printf("30\n");
			break;

		case 10 :
			printf("31\n");
			break;

		case 11 :
			printf("30\n");
			break;

		case 12 :
			printf("31\n");
			break;
		default:
			printf("Invalid Month\n");

	}
	
}
