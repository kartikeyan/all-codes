/*	Program 26 : Take no. of rows from the user
 *		     
 *		     3 2 1
 *		     c b a
 *		     3 2 1
 *		     c b a
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		int x = 3;
		char ch = 'c';

		for(int j = 1; j <= row; j++){
		
			if(i % 2 == 0){
				
				printf("%c ", ch);
				ch--;
			}else{
				printf("%d ", x);
				x--;
			}
		}

		printf("\n");
	}
}
