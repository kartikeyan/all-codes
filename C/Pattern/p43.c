/*	Program 43 :
 *		     a B c D
 *		     b C d E
 *		     c D e F
 *		     d E f G
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);
	
	/*char ch = 'A';

	for(int i = 1; i <= row; i++){
		
		
		for(int j = 1; j <= row; j++){
			
			if(j % 2 != 0){
				printf("%c ", ch+32);
				ch++;
			}else{
				printf("%c ", ch);
				ch++;
			}
			
		}
		printf("\n");
	}*/

	for(int i=0; i<row; i++){
		
		for(int j=0; j<row;j++){
			
			if(i % 2 == 0){
				
				if((i+j) % 2 == 0){
					printf("%c ", (63+i+j+32);
				}else{
					printf("%c ", (63+i+j);
				}
			}else{
			
				if((i+j) % 2 == 0){
					printf("%c ", (63+i+j+32);
				}else{
					printf("%c ", (63+i+j);
				}
			}
		}
		printf("\n");
	}
}
