/*	Program 36 : Take no. of rows from the user
 *		     
 *		     1 3 5
 *		     7 9
 *		     11
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	int x = 1;

	for(int i = 1; i <= row; i++){
	
		for(int j = row; j >= i; j--){
		
			printf("%d ", x);
			x += 2;
		}

		printf("\n");
	}
}
