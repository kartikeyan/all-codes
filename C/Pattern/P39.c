/*	Program 39 : Take no. of rows from the user
 *		     
 *		     - - - - A
 *		     - - - A B
 *		     - - A B C
 *		     - A B C D
 *		     A B C D E
 *
 *
 * 	It is combination of
 *
 * 	1] - - - - -
 * 	   - - - -
 * 	   - - -
 * 	   - 
 *
 * 	2] A 
 * 	   A B
 *	   A B C
 *	   A B C D
 *	   A B C D E
*/ 	   

/* 1]

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int j = row; j >= i; j--){

			printf("- ");
		}
		printf("\n");
	}
}*/

/* 2]

#include<stdio.h>

void main(){

	int row;
	printf("Enter row :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		char ch = 'A';

		for(int j = 1; j <= i; j++){
		
			printf("%c ", ch);
			ch++;
		}

		printf("\n");
	}
}*/

// Now combination of both

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
		
		char ch = 'A';

		for(int sp = row-1; sp >= i; sp--){
		
			printf("_ ");
		}

		for(int j = 1; j <= i ; j++){
		
			printf("%c ", ch);
			ch++;
		}

		printf("\n");
	}
}
