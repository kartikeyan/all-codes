/*	Program 7 : 1
 *		    2 2
 *		    3 3 3
 *		    4 4 4 4
*/

#include<stdio.h>

void main(){

	int x = 1;

	for(int i = 1; i <= 4; i++){
	
		for(int j = 1; j <= i; j++){
		
			printf("%d ", x);
		}

		printf("\n");
		x++;
	}
}
