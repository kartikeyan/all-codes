/*	Program 21 : Take no. of rows from the user
 *		     
 *		     1   2  9   4
 *		     25  6  49  8
 *		     81  10 121 12
 *		     169 14 225 16
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	int x = 1;

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
		
			if(j % 2 == 0){
			
				printf("%d ", x);
			}else{
			
				printf("%d ", x * x);
			}
			x++;
		}

		printf("\n");
	}
}
