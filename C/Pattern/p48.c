/*	Program 48 :
 *		     0  1  1  2
 *		     3  5  8  13
 *		     21 34 55 89
 *		     144 233 377 610
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);
	
	int x = 0;
	int y = 1;
	int nextTerm = 0;

	for(int i = 1; i <= row; i++){

		for(int j = 1; j <= row; j++){
			
			printf("%d ", x);
			nextTerm = x + y;
			x = y;
			y = nextTerm;
		}
		printf("\n");
	}
}
