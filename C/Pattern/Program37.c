/*	Program 37 : Take no . of rows from the user
 *			
 *		     1 A 2 B
 *		     1 A 2
 *		     1 A
 *		     1
*/
#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = row; i >= 1; i--){
	
		char ch = 'A';
		int x  = 1;

		for(int j = 1; j <= i; j++){
		
			if(j % 2 == 0){
			
				printf("%c ", ch);
				ch++;
			}else{
			
				printf("%d ", x);
				x++;
			}
		}

		printf("\n");
	}
}
