/*	Program 35 : Take no. of rows from the user
 *		     
 *		     A B C
 *		     D E
 *		     F
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	char ch = 'A';

	for(int i = 1; i <= row; i++){
	
		for(int j = row; j >= i; j--){
		
			printf("%c ", ch);
			ch++;
		}

		printf("\n");
	}
}
