/*	Program 28 : Take no. of rows from the user
 *		     
 *		     I H G
 *		     F E D
 *		     C B A
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	char ch = 'I';

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
		
			printf("%c ", ch);
			ch--;
		}

		printf("\n");
	}
}
