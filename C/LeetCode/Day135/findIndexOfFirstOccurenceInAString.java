
/*
 	28. Find the Index of the First Occurrence in a String

	Given two strings needle and haystack, return the index of the first occurrence of needle in haystack, or -1 if needle is not part of haystack.

	Example 1:

	Input: haystack = "sadbutsad", needle = "sad"
	Output: 0
	Explanation: "sad" occurs at index 0 and 6.
	The first occurrence is at index 0, so we return 0.

	Example 2:

	Input: haystack = "leetcode", needle = "leeto"
	Output: -1
	Explanation: "leeto" did not occur in "leetcode", so we return -1.

	Constraints:

	    1 <= haystack.length, needle.length <= 104
	    haystack and needle consist of only lowercase English characters.

*/

class Sol{

	static int strStr(String haystack, String needle){
		
		System.out.println(haystack.indexOf(needle));

		int ret = haystack.indexOf(needle);

		return ret;
	}
	
	public static void main(String[] args){
		
		String haystack = "sadbutsad", needle = "sad";

		int ret = strStr(haystack, needle);

		System.out.println(ret);
	}
}
