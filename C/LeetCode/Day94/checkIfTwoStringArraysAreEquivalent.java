
/*
 	1662. Check If Two String Arrays are Equivalent

	Given two string arrays word1 and word2, return true if the two arrays represent the same string, and false otherwise.

	A string is represented by an array if the array elements concatenated in order forms the string.

	Input: word1 = ["ab", "c"], word2 = ["a", "bc"]
	Output: true
	Explanation:
		word1 represents string "ab" + "c" -> "abc"
		word2 represents string "a" + "bc" -> "abc"
		The strings are the same

	Input: word1 = ["a", "cb"], word2 = ["ab", "c"]
	Output: false

	Input: word1  = ["abc", "d", "defg"], word2 = ["abcddefg"]
	Output: true

	Constraints:

    		1 <= word1.length, word2.length <= 103
    		1 <= word1[i].length, word2[i].length <= 103
    		1 <= sum(word1[i].length), sum(word2[i].length) <= 103
	    	word1[i] and word2[i] consist of lowercase letters.

*/

class Sol{
	
	static boolean arrayEqual(String w1[], String w2[]){
		
		String s1 = "";
		String s2 = "";

		for(int i = 0; i < w1.length; i++){
		
			s1 = s1 + w1[i];
		}

		for(int i = 0; i < w2.length; i++){
		
			s2 = s2 + w2[i];
		}

		return s1.equals(s2);
	}

	public static void main(String[] args){
		
		String w1[] = {"ab","c"}, w2[] = {"a", "bc"};

		boolean ret = arrayEqual(w1, w2);

		if(ret == true){
			
			System.out.println("String equal");
		}else{
			
			System.out.println("Not equal");
		}
	}
}
