
/*	Prog : Count Odd Numbers in an interval Range
 *
 *	Given two non-negative integers low and high.Return the count of odd numbers between low and high(inclusive).
 *
 *	Input : low = 3, high = 7
 *	Ouput : 3
 *	Expla : The odd numbers between 3 and 7 are [3,5,7]
 *
 *	Input : low = 8, high = 10
 *	Ouput : 1
 *	Expla : The odd numbers between 8 and 10 are [9]
 *
 *	Constraints :
 *
 *		0 <= low <= high <= 10^9
*/

#include<stdio.h>

int countOdds(int low, int high){
	
	// 	1 - return (high + 1) / 2 - low / 2;
	
	int N = (high - low) / 2;

	// if either high or low is odd
	
	if(high % 2 != 0 || low % 2 != 0){
		
		N = N + 1;
	}

	return N;
}

void main(){
	
	int low = 3, high = 7;

	int ret = countOdds(low, high);

	printf("%d\n", ret);
}
