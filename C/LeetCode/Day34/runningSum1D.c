
/*	Prog : Running Sum of 1d Array
 *
 *	Given an array nums.We define a running sum of an arrays as runningSum[i] = sum(nums[0].......nums[i]).
 *
 *	Return the running sum of nums
 *
 *	Input : nums = [1,2,3,4]
 *	Ouput : [1,3,6,10]
 *	Expla : Running sum is obtained as follows : [1, 1+2, 1+2+3, 1+2+3+4].
 *
 *	Input : nums = [1,1,1,1,1]
 *	Ouput : [1,2,3,4,5]
 *	Expla : Running sum is obtained as follows : [1, 1+1, 1+1+1, 1+1+1+1]
 *
 *	Input : nums = [3,1,2,10,1]
 *	Ouput : [3,4,6,16,17]
 *
 *
 *	Constraints :
 *		
 *		1 <= nums.length <= 1000
 *
 *		-10^6 <= nums[i] <= 10^6
*/

//	Note : The returned array must be malloced, assume caller calls free()

#include<stdio.h>
#include<stdlib.h>

void runningSum(int *arr, int arrSize, int *new){
	
	int sum = 0;

	new[0] = arr[0];

	for(int i = 1; i < arrSize; i++){
		
		new[i] = new[i-1] + arr[i];
	}
}

void main(){
	
	int arr[] = {1,2,3,4};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int *new = (int*)calloc(sizeof(int), arrSize);

	runningSum(arr, size, new);

	for(int i = 0; i < size; i++){
		
		printf("%d\n", new[i]);
(	}
}
