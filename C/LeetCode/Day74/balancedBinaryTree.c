
/*
 	110. Balanced Binary Tree

	Given a binary tree, determine if it is
height-balanced
.

	Input: root = [3,9,20,null,null,15,7]
	Output: true

	Input: root = [1,2,2,3,3,null,null,4,4]
	Output: false

	Input: root = []
	Output: true

	Constraints:

	    The number of nodes in the tree is in the range [0, 5000].
	    -104 <= Node.val <= 104

*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct Node{
	
	int data;
	struct Node *left;
	struct Node *right;
};

struct Node *newNode(int val){
	
	struct Node *node = (struct Node*)malloc(sizeof(struct Node));

	node->data = val;
	node->left = NULL;
	node->right = NULL;

	return node;
}

int max(int x, int y){

	if(x > y){
		
		return x;
	}else{
		
		return y;
	}
}

int height(struct Node *root){
	
	if(root == NULL){
		
		return 0;
	}

	int lh = height(root->left);
	int rh = height(root->right);

	if(lh == -1 || rh == -1 || (lh - rh) > 1){
		
		return -1;
	}

	return max(lh, rh) + 1;
}

bool isBalanced(struct Node *root){
	
	if(root == NULL){
		
		return true;
	}

	if(height(root) == -1){
		
		return false;
	}

	return true;
}

void main(){
	
	struct Node* root = newNode(1);
	root->left = newNode(2);
	root->right = newNode(3);
	
	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("IsBalanced \n");
	bool ret = isBalanced(root);

	if(ret){
		
		printf("Yes\n");
	}else{
		printf("NO\n");
	}
}
