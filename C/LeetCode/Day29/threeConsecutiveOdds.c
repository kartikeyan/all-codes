
/*	Prog : Consecutive Odds
 *
 *	Given an array arr, return true if there are three consecutive odd numbers in the array.Otherwise return false
 *
 *	Input : arr = [2,6,4,1]
 *	Ouput : false
 *	Expla : There are no three consecutive odds
 *
 *	Input : arr = [1,2,34,3,4,5,7,23,12]
 *	Ouput : true
 *	Expla : [5,7,23] are three consecutive odds
*/

#include<stdio.h>
#include<stdbool.h>

bool threeOdds(int arr[], int size){
	
	//Approach 1
/*	for(int i = 0; i < size - 2; i++){
		
		if(arr[i] % 2 != 0){
			
			if(arr[i+1] % 2 != 0){
				
				if(arr[i+2] % 2 != 0){
					
					return true;
				}
			}
		}
	}

	return false;
*/	
	// Approach 2
	
/*	if(size < 3){
		
		return false;
	}

	for(int i = 2; i < size; i++){
		
		if(arr[i] % 2 != 0 && arr[i- 1] % 2 != 0 && arr[i-2] != 0){
			
			return true;				
		}
	}

	return false;
*/
	//  Approach 3
	
	
	int count = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] % 2 == 1){
			
			count++;
		}else{
			
			count = 0;
		}

		if(count == 3){
			
			return true;
		}
	}

	return false;

}

void main(){
	
	int arr[] = {1,2,34,3,4,5,7,23,12};

	int size = sizeof(arr) / sizeof(arr[0]);

	bool ret = threeOdds(arr, size);

	if(ret == true){
		
		printf("Yes Present\n");
	}else{
		
		printf("Not Present\n");
	}
}
