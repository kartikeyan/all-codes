

/*	Prog : Count Equal and Divisible Pairs in an Array
 *
 *	Given an 0-indexed integer array nums of length n and an integer k, return the "number of pairs" (i, j) where 0 <= i < j < n, such that 
 *	nums[i] == nums[j] and (i * j) is divisble by k
 *
 *	Input : nums = [3,1,2,2,2,1,3], k = 2
 *	Ouput : 4
 *	Expla :
 *		There are 4 pairs that meet all the conditions :
 *		- nums[0] == nums[6] and 0 * 6 == 0, which is divisible by 2		
 *		- nums[2] == nums[3] and 2 * 3 == 6, which is divisible by 2,
 *		- nums[2] == nums[4] and 2 * 4 == 8, whcih is divisible by 
 *		- nums[3] == nums[4] and 3 * 4 == 12, which is divisible by 2
 *
*/

#include<stdio.h>

int countPairs(int arr[], int size, int k){
	
	int count = 0;

	for(int i = 0; i < size; i++){
		
		for(int j = i + 1; j < size; j++){
			
			if(arr[i] == arr[j] && (i * j) % k == 0){
				
				count++;
			}
		}
	}

	return count;
}

void main(){
	
	int arr[] = {3,1,2,2,2,1,3};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int k = 2;

	int ret = countPairs(arr, size, k);

	printf("Ret is : %d\n", ret);
}
