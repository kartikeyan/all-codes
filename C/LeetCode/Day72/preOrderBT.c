
/*
 	145. Binary Tree Postorder Traversal

	Given the root of a binary tree, return the postorder traversal of its nodes' values.


	Input: root = [1,null,2,3]
	Output: [3,2,1]

	Input: root = []
	Output: []

	Input: root = [1]
	Output: [1]

	Constraints:

	    The number of the nodes in the tree is in the range [0, 100].
	    -100 <= Node.val <= 100
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{
	
	int data;
	struct Node *left;
	struct Node *right;
};

struct Node *newNode(int val){
	
	struct Node *node = (struct Node*)malloc(sizeof(struct Node));

	node->data = val;
	node->left = NULL;
	node->right = NULL;

	return node;
}

void printPostOrder(struct Node *root){
	
	if(root == NULL){
		
		return;
	}

	printPostOrder(root->left);

	printPostOrder(root->right);

	printf("%d\n", root->data);

}

void main(){
	
	struct Node* root = newNode(1);
	root->left = newNode(2);
	root->right = newNode(3);
	
	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("Postrder traversal of binary tree is \n");
	printPostOrder(root);
}
