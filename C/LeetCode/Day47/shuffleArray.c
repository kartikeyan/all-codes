/*

1470. Shuffle the Array

Given the array nums consisting of 2n elements in the form [x1,x2,...,xn,y1,y2,...,yn].

Return the array in the form [x1,y1,x2,y2,...,xn,yn].

Input: nums = [2,5,1,3,4,7], n = 3
Output: [2,3,5,4,1,7]
Explanation: Since x1=2, x2=5, x3=1, y1=3, y2=4, y3=7 then the answer is [2,3,5,4,1,7].

Input: nums = [1,2,3,4,4,3,2,1], n = 4
Output: [1,4,2,3,3,2,4,1]

Input: nums = [1,1,2,2], n = 2
Output: [1,2,1,2]

*/

#include<stdio.h>
#include<stdlib.h>

void shuffle(int *arr, int size, int n, int *new){
	
	int i, j = 0;

	for(i = 0; i < size; i++){
		
		new[j] = arr[i];
		new[j+1] = arr[i+n];

		j += 2;
	}
}

void main(){
	
	int arr[] = {2,5,1,3,4,7};

	int size = sizeof(arr) / sizeof(arr[0]);

	printf("Size is %d\n", size);

	int n = 3;

	int *new = (int*)calloc(sizeof(int), size);

	shuffle(arr, size, n, new);

	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
