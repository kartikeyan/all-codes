
/*
 	889. Construct Binary Tree from Preorder and Postorder Traversal

	Given two integer arrays, preorder and postorder where preorder is the preorder traversal of a binary tree of distinct values and postorder is the postorder traversal of the same tree, reconstruct and return the binary tree.

	If there exist multiple answers, you can return any of them.

	Input: preorder = [1,2,4,5,3,6,7], postorder = [4,5,2,6,7,3,1]
	Output: [1,2,3,4,5,6,7]

	Input: preorder = [1], postorder = [1]
	Output: [1]

	Constraints:

	    1 <= preorder.length <= 30
	    1 <= preorder[i] <= preorder.length
	    All the values of preorder are unique.
	    postorder.length == preorder.length
	    1 <= postorder[i] <= postorder.length
	    All the values of postorder are unique.
	    It is guaranteed that preorder and postorder are the preorder traversal and postorder traversal of the same binary tree.

*/

#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode{
	
	int data;

	struct TreeNode *left;
	struct TreeNode *right;
}TreeNode;

void inOrderPrint(TreeNode *root){
	
	if(root == NULL){
		return;
	}

	inOrderPrint(root->left);

	printf("%d ", root->data);

	inOrderPrint(root->right);
}

TreeNode *buildTree(int preOrder[], int postOrder[], int preStart, int preEnd, int postStart, int postEnd){
	
	if(preStart > preEnd){
		
		return NULL;
	}

	int rootData = preOrder[preStart];

	TreeNode *temp = (TreeNode*)calloc(sizeof(TreeNode),1);

	temp->data = rootData;

	if(preStart == preEnd){
		
		return temp;
	}

	int idx;
	for(idx = postStart; idx <= postEnd; idx++){
		
		if(postOrder[idx] != preOrder[preStart + 1]){
			
			break;
		}
	}

	int lLength = idx - postStart + 1;

	temp->left = buildTree(preOrder, postOrder, preStart + 1, preStart + lLength , postStart, idx);

	temp->right = buildTree(preOrder, postOrder, preStart + lLength + 1, preEnd, idx + 1, postEnd);

	return temp;
}

void main(){
	
	int preOrder[] = {1,2,4,5,3,6,7};

	int postOrder[] = {4,5,2,6,7,3,1};

	int size = sizeof(preOrder) / sizeof(preOrder[0]);

	int preStart = 0, preEnd = size - 1;
	int postStart = 0, postEnd = size - 1;

	TreeNode *root = buildTree(preOrder, postOrder, preStart, preEnd, postStart, postEnd);

	inOrderPrint(root);

	printf("\n");
}
