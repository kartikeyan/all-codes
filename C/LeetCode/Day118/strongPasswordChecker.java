
/*
 	2299. Strong Password Checker II

	A password is said to be strong if it satisfies all the following criteria:

	    It has at least 8 characters.
	    It contains at least one lowercase letter.
	    It contains at least one uppercase letter.
	    It contains at least one digit.
	    It contains at least one special character. The special characters are the characters in the following string: "!@#$%^&*()-+".
	    It does not contain 2 of the same character in adjacent positions (i.e., "aab" violates this condition, but "aba" does not).

	Given a string password, return true if it is a strong password. Otherwise, return false.

	Input: password = "IloveLe3tcode!"
	Output: true
	Explanation: The password meets all the requirements. Therefore, we return true.

	Input: password = "Me+You--IsMyDream"
	Output: false
	Explanation: The password does not contain a digit and also contains 2 of the same character in adjacent positions. Therefore, we return false.
	
	Input: password = "1aB!"
	Output: false
	Explanation: The password does not meet the length requirement. Therefore, we return false.
	
	Constraints:

	    1 <= password.length <= 100
	    password consists of letters, digits, and special characters: "!@#$%^&*()-+".
	
*/

class Sol{

	static boolean passwordChecker(String password){

		if (password.length() < 8) {

	            return false;
        	}

	        char arr[] = password.toCharArray();

        	int lower = 0, upper = 0, digit = 0, special = 0;

		for (int i = 0; i < arr.length; i++) {

	            if (Character.isLowerCase(arr[i])) {

        	        lower++;

	            } else if (Character.isUpperCase(arr[i])) {

        	        upper++;

	            } else if (Character.isDigit(arr[i])) {

        	        digit++;

	            } else if ("!@#$%^&*()-+".contains(Character.toString(arr[i]))) {

	                special++;
        	    }

	            if (i < arr.length - 1 && arr[i] == arr[i + 1]) {
        	        return false;
	            }
	        }

        	if (lower == 0 || upper == 0 || digit == 0 || special == 0) {
	            return false;
	        }

        	return true;
	}

	public static void main(String[] args){
		
		String password = "IloveLe3tcode!";

		boolean ret = passwordChecker(password);

		if(ret == true){
			
			System.out.println("Strong Password");
		}else{
			System.out.println("Not a Strong Password");
		}
	}
}
