/*	Program 1 : Search in Rotated Sorted Array
 *
 *	Input : nums = [4,5,6,7,0,1,2],  target = 0
 *	Ouput : 4
 *		
 *		nums = [4,5,6,7,0,1,2], target = 3
 *		Ouput : -1
*/			


#include<stdio.h>

int search(int arr[], int size, int search){

	int start = 0, end = size - 1, mid;

	while(start <= end){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] < arr[start]){
			
			if(arr[mid] < search && arr[end] >= search){
				
				start = mid + 1;
			}else{
				end = mid - 1;
			}
		}else{
			if(arr[mid] > search && arr[start] <= search){
				
				end = mid - 1;
			}else{
				start = mid + 1;
			}
		}
	}

	return -1;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}
	
	int searchEle;
	printf("Enter Search Element\n");
	scanf("%d", &searchEle);

	int ret = search(arr, size, searchEle);
	printf("Index is %d\n", ret);
}
