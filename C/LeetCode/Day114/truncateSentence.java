
/*
 	1816. Truncate Sentence

	A sentence is a list of words that are separated by a single space with no leading or trailing spaces. Each of the words consists of only uppercase and lowercase English letters (no punctuation).

    	For example, "Hello World", "HELLO", and "hello world hello world" are all sentences.

	You are given a sentence s and an integer k. You want to truncate such that it contains only the first k words. Return after truncating it.

	Input: s = "Hello how are you Contestant", k = 4
	Output: "Hello how are you"
	Explanation:
		The words in s are ["Hello", "how" "are", "you", "Contestant"].
		The first 4 words are ["Hello", "how", "are", "you"].
		Hence, you should return "Hello how are you".


	Input: s = "What is the solution to this problem", k = 4
	Output: "What is the solution"
	Explanation:
		The words in s are ["What", "is" "the", "solution", "to", "this", "problem"].
		The first 4 words are ["What", "is", "the", "solution"].
		Hence, you should return "What is the solution".
	Input: s = "chopper is not a tanuki", k = 5
	Output: "chopper is not a tanuki"


	Constraints:

	    1 <= s.length <= 500
	    k is in the range [1, the number of words in s].
	    s consist of only lowercase and uppercase English letters and spaces.
	    The words in s are separated by a single space.
	    There are no leading or trailing spaces.
*/

import java.util.*;

class Sol{

	static String truncateString(String s, int k){
		
		StringTokenizer str = new StringTokenizer(s, " ");

		StringBuilder str1 = new StringBuilder();

		while(str.hasMoreTokens() && k != 0){
			
			str1.append(str.nextToken());

			if(k != 1){
				
				str1 = str1.append(" ");
			}

			k--;
		}

		return str1.toString();
	}
	
	public static void main(String[] args){
		
		String s = "Hello how are you Contestant";

		int k = 4;

		String ret = truncateString(s, k);

		System.out.println("Ret is : " + ret);
	}
}
