per
/*	Prog : Perfext Number
 *
 *	A "perfect Number" is a poisitive integer that is equal to the sum of its "positive divisors" excluding the number itself.
 *	A "divisor" of an integer 'x' is an integer that can divide x evenly
 *
 *	Given an integer "n", return true if n is a perfect number, otherwise return false.
 *
 *	Input : nums = 20
 *	Ouput : true
 *	Expla : 28 = 1 + 2 + 4 + 7 + 14
 *		1 , 2, 4, 7 and 14 are divisors of 28
 *
 *	Input : nums = 7
 *	Ouput : false
*/

#include<stdio.h>
#include<stdbool.h>

bool checkPerfectNumber(int num){
	
	if(num % 2 != 0){
		
		return false;
	}

	int sum = 0;

	for(int i = 1; i < num; i++){
		
		if(num % i == 0){
			
			sum += i;
		}
	}

	return sum == num;
}

void main(){
	
	int num = 28;

	int ret = checkPerfectNumber(num);

	printf("%d\n", ret);
}
