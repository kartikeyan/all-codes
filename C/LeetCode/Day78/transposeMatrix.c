
/*
 	867. Transpose Matrix

	Given a 2D integer array matrix, return the transpose of matrix.

	The transpose of a matrix is the matrix flipped over its main diagonal, switching the matrix's row and column indices.

	Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
	Output: [[1,4,7],[2,5,8],[3,6,9]]

	Input: matrix = [[1,2,3],[4,5,6]]
	Output: [[1,4],[2,5],[3,6]]

	Constraints:

	    m == matrix.length
	    n == matrix[i].length
	    1 <= m, n <= 1000
	    1 <= m * n <= 105
	    -109 <= matrix[i][j] <= 109
*/

#include<stdio.h>

void transpose(int arr[][3]){
		
	int newArr[3][3]; 

	for(int i = 0; i < 3; i++){
			
		for(int j = 0; j < 3; j++){
				
			newArr[j][i] = arr[i][j];
		}
	}
		
	for(int i = 0; i < 3; i++){

        	for(int j = 0; j < 3; j++){
 
               		printf("%d\n",newArr[i][j]);
                }
	}
}

void main(){
		
	int arr[][3] = {{1,2,3},{4,5,6},{7,8,9}};

	transpose(arr);
}
