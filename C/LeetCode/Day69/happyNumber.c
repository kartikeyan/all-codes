
/*
 	202. Happy Number


	Write an algorithm to determine if a number n is happy.

	A happy number is a number defined by the following process:

    	Starting with any positive integer, replace the number by the sum of the squares of its digits.

    	Repeat the process until the number equals 1 (where it will stay), or it loops endlessly in a cycle which does not include 1.

    	Those numbers for which this process ends in 1 are happy.

	Return true if n is a happy number, and false if not.

	Input: n = 19
	Output: true
	Explanation:
		12 + 92 = 82
		82 + 22 = 68
		62 + 82 = 100
		12 + 02 + 02 = 1

	Input: n = 2
	Output: false

	Constraints:

	    1 <= n <= 231 - 1
*/

#include<stdio.h>
#include<stdbool.h>

int sumOfSquares(int n){
		
	int sum = 0;

	while(n > 0){
			
		int digit = n % 10;
		sum = sum + (digit * digit);
		n /= 10;
	}

	return sum;
}

//	Approach 1

/*

bool isHappy(int n){
		
        int slow = n, fast = n;

        do{

        	slow = sumOfSquares(slow);
                fast = sumOfSquares(sumOfSquares(fast));

        }while(fast != 1 && slow != fast);


        if(fast == 1){

                return true;
        }

	return false;
}

*/
	
bool isHappy(int n){

        int slow = n, fast = n;

        do{

                slow = sumOfSquares(slow);
                fast = sumOfSquares(sumOfSquares(fast));

        }while(slow != fast);


        if(slow == 1){

                return true;
        }

        return false;
}

void main(){
		
	int n = 19;

	bool ret = isHappy(n);

	if(ret == true){
			
		printf("Happy Number\n");
	}else{

		printf("Not Happy\n");
	}

}
