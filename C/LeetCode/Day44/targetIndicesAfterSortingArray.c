
/*	Prog : Find Target Indices After Soring Array
 *
 *	You are given a 0-indexed integer array nums and a target element target
 *
 *	A target index is an index i such that nums[i] == target
 *
 *	Return a list of the target indices of nums after sorting nums in non-decreasing order.If there
 *	are no target indices, return an empty list.The returned list must be sorted in increasing order.
 *
 *
 *	Input : nums = [1,2,5,2,3], target = 2
 *	Ouput : [1,2]
 *	Expla : After sorting, nums is [1,2,2,3,5].
 *		The indices where nums[i] == 2 are 1 and 2
 *
 *
 *	Input : nums = [1,2,5,2,3], target = 3
 *	Ouput : [3]
 *	Expla : After sorting, nums is [1,2,2,3,5]
 *		The index where nums[i] == 3 is 3
 *
 *
 *	Input : nums = [1,2,5,2,3], target = 5
 *	Ouput : [4]
 *	Expla : After sorting, nums is [1,2,2,3,5]
 *		The index where nums[i] == 5 is 4
 *
 *
 *	Constraints :
 *
 *		1 <= nums.length <= 100
 *
 *		1 <= nums[i] , target <= 100
*/

#include<stdio.h>
#include<stdlib.h>

void merge(int *arr, int start, int mid, int end){
	
	int ele1 = mid - start + 1, ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int i = 0; i < ele1; i++){
		
		printf("%d ", arr1[i]);
	}

	printf("\n");

	for(int i = 0; i < ele2; i++){
		
		arr2[i] = arr[mid + 1 + i];
	}

	int itr1 = 0 , itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}
		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int *arr, int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid+1, end);

		merge(arr, start, mid, end);
	}
}

void targetIndices(int *arr, int size, int *returnSize, int target){

	int start = 0, end = size - 1;

	mergeSort(arr, start, end);

	for(int i = 0; i < size; i++){
	
		printf("%d\n", arr[i]);
	}	
}

void main(){
	
	int arr[] = {1,2,5,2,3};

	int size = sizeof(arr) / sizeof(arr[0]);

	int target = 2;

	int *returnSize;

	int count = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] == target){
			
			count++;
		}
	}

	int *nums = (int *)calloc(sizeof(int), count);
	
	*returnSize = count;
	
	targetIndices(arr, size, returnSize, target);

	int k = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] == target){
			
			arr[k++] = i;
		}
	}

	for(int i = 0; i < count; i++){
		
		printf("%d ", nums[i]);
	}

	printf("\n");
}
