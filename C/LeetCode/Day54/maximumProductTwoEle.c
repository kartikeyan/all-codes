
/*	
 	1464. Maximum Product of Two Elements in an Array

	Given the array of integers nums, you will choose two different indices i and j of that array. Return the maximum value of (nums[i]-1)*(nums[j]-1).

	Input: nums = [3,4,5,2]
	Output: 12
	Explanation: If you choose the indices i=1 and j=2 (indexed from 0), you will get the maximum value, that is, (nums[1]-1)*(nums[2]-1) = (4-1)*(5-1) = 3*4 = 12.

	Input: nums = [1,5,4,5]
	Output: 16
	Explanation: Choosing the indices i=1 and j=3 (indexed from 0), you will get the maximum value of (5-1)*(5-1) = 16.

	Input: nums = [3,7]
	Output: 12

	Constraints:

	    2 <= nums.length <= 500
	    1 <= nums[i] <= 10^3
*/

#include<stdio.h>

void merge(int nums[], int start, int mid, int end){

	int ele1 = mid - start + 1, ele2 = end - mid;

    	int nums1[ele1], nums2[ele2];

    	for(int i = 0; i < ele1; i++){
        	nums1[i] = nums[start + i];
    	}

    	for(int j = 0; j < ele2; j++){
        	nums2[j] = nums[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

    	while(itr1 < ele1 && itr2 < ele2){

        	if(nums1[itr1] < nums2[itr2]){
            		nums[itr3] = nums1[itr1];
  	          	itr1++;
        	}else{
            		nums[itr3] = nums2[itr2];
	            itr2++;
        	}

	        itr3++;
	 }

    	while(itr1 < ele1){

        	nums[itr3] = nums1[itr1];
        	itr1++;
        	itr3++;
    	}

        while(itr2 < ele2){
        	nums[itr3] = nums2[itr2];
	        itr2++;
        	itr3++;
	}
}

void mergeSort(int nums[], int start, int end){
	
	if(start < end){

        	int mid = start + (end - start) / 2;

        	mergeSort(nums, start, mid);
        	mergeSort(nums, mid+1, end);

        	merge(nums, start, mid, end);
    	}
}

int maxProduct(int arr[], int size){
	
	mergeSort(arr, 0, size-1);

	return (arr[size-2] - 1) * (arr[size-1] - 1);
}

void main(){
	
	int arr[] = {1,5,4,5};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = maxProduct(arr, size);

	printf("%d\n", ret);
}
