/*	From Today we are going to start daily LeetCode Solving 2 problems
 *
 *	Program 1 : LeetCode No : 1342 : 
 *
 *		Q] "Number of Steps to Reduce a Numebr to Zero"
 *
 *	Given an integer num, return the number of steps to reduce it to zero
 *
 *	In one step, if the current number is even, you have to divide it by 2,
 *	otherwise, you have to subtract 1 from it
 *
 *	Input : num = 14
 *	Output : 6
*/

#include<stdio.h>

int numberofSteps(int num){
	
	int count = 0;

	while(num){
	
		if(num % 2 == 0){
			
			num = num / 2;
		}else{
			num = num - 1;
		}

		count++;
	}

	return count;
}

void main(){
	
	int num;
	printf("Enter Num\n");
	scanf("%d", &num);

	int ret = numberofSteps(num);
	printf("Number of steps are : %d\n", ret);
}
