/*
 	199. Binary Tree Right Side View

	Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.

	Input: root = [1,2,3,null,5,null,4]
	Output: [1,3,4]

	Input: root = [1,null,3]
	Output: [1,3]

	Input: root = []
	Output: []

	Constraints:

	    The number of nodes in the tree is in the range [0, 100].
	    -100 <= Node.val <= 100
*/

#include<iostream>
#include<queue>

struct Node{
	
	int data;

	struct Node *left;
	struct Node *right;
};

Node* newNode(int val){
	
	Node *temp = new Node;

	temp->data = val;

	temp->left = temp->right = NULL;

	return temp;
}

void printRightView(Node *root){
	
	if(root == NULL){
		
		return;
	}

	std::queue < Node*> q;

	q.push(root);

	while(!q.empty()){
		
		int n = q.size();

		int data = 0;

		for(int i = 0; i < n; i++){
			
			Node *temp = q.front();

			q.pop();

			if(i == n-1){
				
				std::cout << temp->data << " ";
			}

			if(temp->left != NULL){
				
				q.push(temp->left);
			}

			if(temp->right != NULL){
				
				q.push(temp->right);
			}
		}
	}
}

int main(){
	
	Node *root = newNode(1);

	root->left = newNode(2);
	root->right = newNode(3);

	root->left->left = newNode(4);
	root->left->right = newNode(5);

	root->right->left = newNode(6);
	root->right->right = newNode(7);

	printRightView(root);

	printf("\n");
}
