
/*	
 	2574. Left and Right Sum Differences

	Given a 0-indexed integer array nums, find a 0-indexed integer array answer where:

    answer.length == nums.length.
    answer[i] = |leftSum[i] - rightSum[i]|.

    Where:

    leftSum[i] is the sum of elements to the left of the index i in the array nums. If there is no such element, leftSum[i] = 0.
    rightSum[i] is the sum of elements to the right of the index i in the array nums. If there is no such element, rightSum[i] = 0.

Return the array answer.

Input: nums = [10,4,8,3]
Output: [15,1,11,22]
Explanation: The array leftSum is [0,10,14,22] and the array rightSum is [15,11,3,0].
The array answer is [|0 - 15|,|10 - 11|,|14 - 3|,|22 - 0|] = [15,1,11,22].

Input: nums = [1]
Output: [0]
Explanation: The array leftSum is [0] and the array rightSum is [0].
The array answer is [|0 - 0|] = [0].

Constraints:

    1 <= nums.length <= 1000
    1 <= nums[i] <= 105

*/

#include<stdio.h>
#include<stdlib.h>	// abs is in stdlib.h header file

void leftRight(int *arr, int size, int *main){
	
	int *left = (int*)calloc(sizeof(int), size);

	int *right = (int*)calloc(sizeof(int), size);

	left[0] = 0;

	for(int i = 1; i < size; i++){
		
		left[i] = left[i-1] + arr[i-1];
	}
	
	printf("Left\n");
	for(int i = 0; i < size; i++){
		
		printf("%d ", left[i]);
	}

	right[size-1] = 0;

	for(int i = size; i > 1; i--){
		
		right[i-2] = right[i-1] + arr[i-1];
	}
	
	printf("\nRight\n");
	for(int i = 0; i < size; i++){
		
		printf("%d ", right[i]);
	}
	
	for(int i = 0; i < size; i++){
		
		main[i] = abs(left[i] - right[i]);
	}
}

void main(){

	int arr[] = {10,4,8,3};

	int size = sizeof(arr) / sizeof(arr[0]);

	int *main = (int*)calloc(sizeof(int), size);

	leftRight(arr, size, main);
	
	printf("\nMain\n");
	for(int i = 0; i < size; i++){
		
		printf("%d ", main[i]);
	}
	printf("\n");
}

