/*	
 *	Program 1 : Given a "non-empty" array of integers nums.every element appears twice except for one.
 *		    Find That single one.
 *
 *		    You must implement a sloution with a linear runtime complexity and use only constant extra space
 *
 *		    Input : nums = [2,2,1]
 *		    Ouput : 1
 *
 *		    Input : nums = [4,1,2,1,2]
 *		    Output : 4
*/		  

#include<stdio.h>

int singleNumber(int arr[], int size){
	
	int store = 0;

	for(int i = 0; i < size; i++){
		
		store = store ^ arr[i];
	}	
	return store;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int ret = singleNumber(arr, size);
	printf("NUmber is : %d\n", ret);
}
