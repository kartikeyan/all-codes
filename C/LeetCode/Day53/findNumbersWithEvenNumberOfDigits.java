
/*
 	1295. Find Numbers with Even Number of Digits
	
	Given an array nums of integers, return how many of them contain an even number of digits.

	Input: nums = [12,345,2,6,7896]
	Output: 2
	Explanation:
		12 contains 2 digits (even number of digits).
		345 contains 3 digits (odd number of digits).
		2 contains 1 digit (odd number of digits).
		6 contains 1 digit (odd number of digits).
		7896 contains 4 digits (even number of digits).
		Therefore only 12 and 7896 contain an even number of digits.

	Input: nums = [555,901,482,1771]
	Output: 1 
	Explanation: 
		Only 1771 contains an even number of digits.

	Constraints:

	    1 <= nums.length <= 500
	    1 <= nums[i] <= 105
*/

class sol{

	static int findNumbers(int arr[], int size){
		
		int count1 = 0, count2 = 0, rem;

		for(int i = 0; i < size; i++){
			
			int n = arr[i];

			while(n != 0){
				
				count1++;
				n = n / 10;
			}

			if(count1 % 2 == 0){
				
				count2++;
			}

			count1 = rem = 0;
		}

		return count2;
	}
	
	public static void main(String[] s){
		
		int arr[] = {555,901,482,1771};

		int size = arr.length;

		int ret = findNumbers(arr, size);

		System.out.println(ret);
	}
}
