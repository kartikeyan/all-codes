
/*	Prog : A Number After a Double Reversal
 *
 *	Reversing an integer means to reverse all its digits.
 *
 *	for eg, reversing 2021 gives 1202. Reversing 12300 gives 321 qs the "leading zeros are not retained".
 *
 *	Given an integer num, "reverse" num to get reversed1, then "reverse" reversed1 to get reversed2.Retunr "true" if reversed2 equals "num".Otherwise return "false".
 *
 *	Input : num = 526
 *	Ouput : true
 *	Expla : Reverse num to get 625, then reverse 625 to get 526,which 
 *		equals num.
 *
 *	Input : nums = 1800
 *	Ouput : false
 *	Expla : Reverse num to get 81, then reverse 81 to get 18,which 
 *		does not equal num.
*/

#include<stdio.h>
#include<stdbool.h>
/*
bool isSameAfterReversals(int num){

	if(num == 0){
		
		return true;
	}
	return (num % 10 != 0);
}
*/

/*
bool isSameAfterReversals(int num){

	long rnum = 0;
	long x = 0;
	long e = num;

	while(num > 0){
		
		rnum = (rnum * 10) + (num % 10);
		num = num / 10;
	}

	printf("%d\n", rnum);

	while(rnum > 0){
		
		x = (x * 10) + (rnum % 10);
		rnum = rnum / 10;

		printf("%d\n", x);
	}

	printf("%d\n", x);
	printf("%d\n", e);

	if(x == e){
		
		return true;
	}else{
		return false;
	}
}
*/

bool isSameAfterReversals(int num){

	int n = num, rev1 = 0, rev2 = 0;

	long last = 0;

	while(num > 0){
		
		last = num % 10;
		rev1 = rev1 * 10 + last;
		num /= 10;
	}

	last = 0;

	while(rev1 > 0){
		
		last = rev1 % 10;
		rev2 = rev2 * 10 + last;
		rev1 /= 10;
	}

	if(n == rev2){
		
		return true;
	}
	return false;
}
void main(){
	
	int num = 1800;

	int ret = isSameAfterReversals(num);

	printf("Ret is %d\n", ret);
}
