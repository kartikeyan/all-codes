
/*
 	1961. Check If String Is a Prefix of Array

	Given a string s and an array of strings words, determine whether s is a prefix string of words.

	A string s is a prefix string of words if s can be made by concatenating the first k strings in words for some positive k no larger than words.length.

	Return true if s is a prefix string of words, or false otherwise.

	Input: s = "iloveleetcode", words = ["i","love","leetcode","apples"]
	Output: true
	Explanation:
		s can be made by concatenating "i", "love", and "leetcode" together.

	Input: s = "iloveleetcode", words = ["apples","i","love","leetcode"]
	Output: false
	Explanation:
		It is impossible to make s using a prefix of arr.

	Constraints:

	    1 <= words.length <= 100
	    1 <= words[i].length <= 20
	    1 <= s.length <= 1000
	    words[i] and s consist of only lowercase English letters.
*/

class Demo{

	static boolean isPrefixString(String s, String[] words){
		
		StringBuilder str = new StringBuilder();

		for(int i = 0; i < words.length; i++){
			
			str.append(words[i]);

			if(s.equals(str.toString())){
				
				return true;
			}
		}

		return false;
	}
	
	public static void main(String[] args){
		
		String s = "iloveleetcode";

		String words[] = {"i","love","leetcode","apples"};

		boolean ret = isPrefixString(s, words);

		if(ret == true){
			
			System.out.println("yes string is prefix of array");
		}else{
			System.out.println("Not");
		}
	}
}
