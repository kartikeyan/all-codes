
/*
 	1502. Can Make Arithmetic Progression From Sequence

	A sequence of numbers is called an arithmetic progression if the difference between any two consecutive elements is the same.

	Given an array of numbers arr, return true if the array can be rearranged to form an arithmetic progression. Otherwise, return false.

	Input: arr = [3,5,1]
	Output: true
	Explanation: We can reorder the elements as [1,3,5] or [5,3,1] with differences 2 and -2 respectively, between each consecutive elements.

	Input: arr = [1,2,4]
	Output: false
	Explanation: There is no way to reorder the elements to obtain an arithmetic progression.

	Constraints:

	    2 <= arr.length <= 1000
	    -106 <= arr[i] <= 106
*/

import java.util.Arrays;

class Sol{

	static boolean canMakeArithPro(int arr[]){
		
		Arrays.sort(arr);

		int diff = -1;

		for(int i = 0; i < arr.length - 1; i++){
			
			if(i == 0){
				
				diff = arr[i+1] - arr[i];
			}

			if(arr[i+1] - arr[i] != diff){

				return false;
			}
		}

		return true;
	}
	
	public static void main(String[] args){
		
		int arr[] = {3,5,1};

		boolean ret = canMakeArithPro(arr);

		if(ret == true){
			
			System.out.println("Yes AP");
		}else
			System.out.println("Not AP");
	}
}
