
/*
 	152. Maximum Product Subarray

	Given an integer array nums, find a
subarray that has the largest product, and return the product.

	The test cases are generated so that the answer will fit in a 32-bit integer.

	Input: nums = [2,3,-2,4]
	Output: 6
	Explanation: [2,3] has the largest product 6.

	Input: nums = [-2,0,-1]
	Output: 0
	Explanation: The result cannot be 2, because [-2,-1] is not a subarray.

	Constraints:

	    1 <= nums.length <= 2 * 104
	    -10 <= nums[i] <= 10
	    The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

*/

#include<stdio.h>
#include<limits.h>

int max(int x, int y){
		
	if(x > y){
			
		return x;
	}

	return y;
}

int maxProduct(int arr[], int size){
		
	int pre = 1, suff = 1;

	int ans = INT_MIN;

	for(int i = 0; i < size; i++){
			
		if(pre == 0){
				
			pre = 1;
		}

		if(suff == 0){
				
			suff = 1;
		}

		pre *= arr[i];

		suff *= arr[size - i - 1];

		ans = max(ans, max(pre, suff));
	}

	return ans;
}
	
void main(){
		
	int arr[] = {2,3,-2,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = maxProduct(arr, size);

	printf("%d\n",ret);
}
