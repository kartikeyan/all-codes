
/*	Prog : Given an array of nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
 *
 *	Input : nums = [-4,-1,0,3,10]
 *	Ouput : [0,1,9,16,100]
 *	Expla : After squaring, the array becomes [16,1,0,9,100].
 *		After sorting, it becomes [0,1,9,16,100]
 *
 *
 *	Input : nums = [-7,-3,2,3,11]
 *	Ouput : [4,9,9,49,121]
 *
 *	Constraints : 
 *		
 *		1 <= nums.length <= 10^4
 *
 *		-10^4 <= nums[i] <= 10^4
 *
 *		nums is sorted in non-decreasing order.
*/

#include<stdio.h>
#include<stdlib.h>

void merge(int *new, int start, int mid, int end){
	
	int ele1 = mid - start + 1, ele2 = end - mid;

	int new1[ele1], new2[ele2];

	for(int i = 0; i < ele1; i++){
		
		new1[i] = new[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		new2[j] = new[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(new1[itr1] < new2[itr2]){
			
			new[itr3] = new1[itr1];
			itr1++;
		}else{
			
			new[itr3] = new2[itr2];
			itr2++;
		}
	}

	while(itr1 < ele1){
		
		new[itr3] = new1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		new[itr3] = new2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int new[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(new, start, mid);
		mergeSort(new, mid+1, end);

		merge(new, start, mid, end);
	}
}

int *sortedSquare(int *arr, int size, int *returnSize){
	
	int *new = (int*)calloc(sizeof(int), size);

	*returnSize = size;

	for(int i = 0; i < size; i++){
		
		new[i] = arr[i] * arr[i];
	}

	mergeSort(new, 0, size-1);

	return new;
}

void main(){
	
	int arr[] = {-4,-1,0,3,10};

	int size = sizeof(arr) / sizeof(arr[0]);

	int *returnSize;

	int *ptr = sortedSquare(arr, size, returnSize);
}
