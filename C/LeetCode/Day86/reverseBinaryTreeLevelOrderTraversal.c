
/*
 	107. Binary Tree Level Order Traversal II

	Given the root of a binary tree, return the bottom-up level order traversal of its nodes' values. (i.e., from left to right, level by level from leaf to root).

	Input: root = [3,9,20,null,null,15,7]
	Output: [[15,7],[9,20],[3]]

	Input: root = [1]
	Output: [[1]]

	Input: root = []
	Output: []

	Constraints:

	    The number of nodes in the tree is in the range [0, 2000].
	    -1000 <= Node.val <= 1000
*/

//	Using Recursion


#include <stdio.h>
#include <stdlib.h>

struct node
{
	int data;
	struct node* left;
	struct node* right;
};

void printGivenLevel(struct node* root, int level);
int height(struct node* node);
struct node* newNode(int data);

void reverseLevelOrder(struct node* root)
{
	int h = height(root);
	int i;
	for (i=h; i>=1; i--) 

		printGivenLevel(root, i);
}

void printGivenLevel(struct node* root, int level)
{
	if (root == NULL)
		return;
	if (level == 1)
		printf("%d ", root->data);
	else if (level > 1)
	{
		printGivenLevel(root->left, level-1);
		printGivenLevel(root->right, level-1);
	}
}

int height(struct node* node)
{
	if (node==NULL)
		return 0;
	else
	{
		
		int lheight = height(node->left);
		int rheight = height(node->right);

		if (lheight > rheight)
			return(lheight+1);
		else return(rheight+1);
	}
}

struct node* newNode(int data)
{
	struct node* node = (struct node*)malloc(sizeof(struct node));
	node->data = data;
	node->left = NULL;
	node->right = NULL;

	return(node);
}

int main()
{
	struct node *root = newNode(1);
	root->left	 = newNode(2);
	root->right	 = newNode(3);
	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("Level Order traversal of binary tree is \n");
	reverseLevelOrder(root);
	
	printf("\n");

	return 0;
}


