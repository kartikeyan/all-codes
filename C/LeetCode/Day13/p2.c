
/*	PRogram 2 : Sort Colors
 *
 *	Given an array "nums" with "n" objects colored red, white, or blue, sort them "in-place" so that 
 *	objects of the same color are adjacent, with the colors in he order red, white, and blue
 *
 *	We will use the intgers 0, 1, and 2 to represent the color red, white, and blue, respectively
 *
 *	You must solve this problem without using the library's sort function
 *
 *	Input : nums = [2,0,2,1,1,0]
 *	Output : [0,0,1,1,2,2]
 *
 *	Input : nums = [2,0,1]
 *	Ouput :  [0,1,2]
*/

#include<stdio.h>

void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr[itr1] < arr[ele1]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

void sortColors(int arr[], int size){
	
	int start = 0, end = size - 1;

	mergeSort(arr, start, end);
}

void main(){
	
	int arr[] = {3,2,1,5,6,7};

	int size = sizeof(arr) / sizeof(arr[0]);

	sortColors(arr, size);

	printf("Sorted Array is\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
