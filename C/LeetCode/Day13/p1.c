
/*	PRogram 1 : Kth Largest Element in an Array
 *
 *	Given an integer array "nums" and an integer "k", return the "Kth" largest element in the array
 *	Not that it is the "Kth" largest element in the sorted order, not the "Kth" distinct element.
 *	You must solve it in "0(n)" time complexity
 *
 *	Input : nums = [3,2,1,5,6,4],  k = 2
 *	Output : 5
 *
 *	Input : nums = [3,2,3,1,2,4,5,5,6], k = 4
 *	Ouput :  4
*/

#include<stdio.h>

void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr[itr1] < arr[ele1]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

int ksmallest(int arr[], int size, int k){
	
	return arr[k - 1];
}

int klargest(int arr[], int size, int k){
	
	return arr[size - k];
}

void main(){
	
	int arr[] = {3,2,1,5,6,7};

	int size = sizeof(arr) / sizeof(arr[0]);

	mergeSort(arr, 0, size - 1);

	printf("Sorted Array is\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
	
	int k = 2;

	int ret1 = ksmallest(arr, size, k);
	printf("Kth Smallest is : %d\n", ret1);


	int ret2 = klargest(arr, size, k);
	printf("Kth largest is : %d\n", ret2);
}
