
/*	Program 1 : Number of Good Pairs
 *
 *		    Given an array of integers nums, return the number of "good pairs".
 *
 *		    A pair (i,j) is called good if nums[i] == nums[j] and i < j.
 *
 *		    Input : nums = [1,2,3,1,1,3]  ,   Input : nums = [1,1,1,1]
 *		    Ouput : 4			  ,   Output : 6
*/

#include<stdio.h>

int numIdenticalPairs(int arr[], int size){
	
	int count = 0;

	for(int i = 0; i < size; i++){
		
		for(int j = i + 1; j < size; j++){
			
			if(arr[i] == arr[j]){
				
				count++;
			}
		}
	}

	return count;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	printf("Array Elements are\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}

	int ret = numIdenticalPairs(arr, size);
	printf("Count is : %d\n", ret);
}
