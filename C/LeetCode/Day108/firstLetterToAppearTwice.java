
/*
 	
   	Intuition

	To find the first letter that appears twice in the string, we can iterate over the characters in the string and keep track of which letters have been seen. Once we encounter a letter that has already been seen, we return it as the answer.

	Approach

	1] We can use an array of booleans, 'letters', of size 26 to represent each letter of the alphabet. The index of each letter is calculated by subtracting 'a' from the character value, as 'a' has an ASCII value of 97.

	2] Initialize all elements of the 'letters' array to 'false'.

	3] Iterate through each character in the input string 's'.

	4] For each character, check if the corresponding element in the 'letters' array is 'true'. If it is, return the character as it is the first letter that appears twice.

	5] If the corresponding element is 'false', set it to 'true' to mark that the letter has been seen.

	6] If no letter appears twice, return a space character ' '.

	Complexity

	Time complexity:

	1] The solution iterates through each character in the input string s, which takes O(n) time, where n is the length of the string. Accessing and updating the boolean array elements also takes constant time. Therefore, the overall time complexity is O(n).

	Space complexity:
	
	2] The solution uses a boolean array of size 26 to represent each letter of the alphabet. Hence, the space complexity is O(26), which simplifies to O(1) as it is a constant size.

*/

/*
 	2351. First Letter to Appear Twice

	Given a string s consisting of lowercase English letters, return the first letter to appear twice.

	Note:

    	A letter a appears twice before another letter b if the second occurrence of a is before the second occurrence of b.
    	s will contain at least one letter that appears twice.

	Input: s = "abccbaacz"
	Output: "c"
	Explanation:
		The letter 'a' appears on the indexes 0, 5 and 6.
		The letter 'b' appears on the indexes 1 and 4.
		The letter 'c' appears on the indexes 2, 3 and 7.
		The letter 'z' appears on the index 8.
		The letter 'c' is the first letter to appear twice, because out of all the letters the index of its second occurrence is the smallest.

	Input: s = "abcdd"
	Output: "d"
	Explanation:
		The only letter that appears twice is 'd' so we return 'd'.

	Constraints:

	    2 <= s.length <= 100
	    s consists of lowercase English letters.
	    s has at least one repeated letter.
*/

class Demo{

	static char repeatedChar(String s){
		
		boolean count[] = new boolean[26];

		for(int i = 0; i < s.length(); i++){
			
			int index = s.charAt(i) - 'a';

			if(count[index]){
				
				return s.charAt(i);
			}else{
				
				count[index] = true;
			}
		}

		return ' ';
	}
	
	public static void main(String[] args){
		
		String str = "abccbaaz";

		char ret = repeatedChar(str);

		System.out.println("ret is " + ret);
	}
}
