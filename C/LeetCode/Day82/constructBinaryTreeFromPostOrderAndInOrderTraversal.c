
/*
 	106. Construct Binary Tree from Inorder and Postorder Traversal

	Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, construct and return the binary tree.

	Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
	Output: [3,9,20,null,null,15,7]

	Input: inorder = [-1], postorder = [-1]
	Output: [-1]

	Constraints:

    	1 <= inorder.length <= 3000
    	postorder.length == inorder.length
    	-3000 <= inorder[i], postorder[i] <= 3000
    	inorder and postorder consist of unique values.
    	Each value of postorder also appears in inorder.
    	inorder is guaranteed to be the inorder traversal of the tree.
    	postorder is guaranteed to be the postorder traversal of the tree.
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct TreeNode{
	
	int data;

	struct TreeNode *left;
	struct TreeNode *right;
}TreeNode;

void inOrderPrint(TreeNode *root){
	
	if(root == NULL){
		return;
	}

	inOrderPrint(root->left);

	printf("%d ", root->data);

	inOrderPrint(root->right);
}

TreeNode *buildTree(int inOrder[], int postOrder[], int inStart, int inEnd, int postStart, int postEnd){
	
	if(inStart > inEnd){
		
		return NULL;
	}

	int rootData = postOrder[postEnd];

	TreeNode *temp = (TreeNode*)malloc(sizeof(TreeNode));

	temp->data = rootData;

	int idx;
	for(idx = inStart; idx <= inEnd; idx++){
		
		if(rootData == inOrder[idx]){
			
			break;
		}
	}

	int lLength = idx - inStart;

	temp->left = buildTree(inOrder, postOrder, inStart, idx - 1, postStart, postStart + lLength - 1);

	temp->right = buildTree(inOrder, postOrder, idx + 1, inEnd, postStart + lLength, postEnd - 1);

	return temp;
}

void main(){
	
	int inOrder[] = {9,3,15,20,7};

	int postOrder[] = {9,15,7,20,3};

	int size = sizeof(inOrder) / sizeof(inOrder[0]);

	int inStart = 0, inEnd = size - 1;
	int postStart = 0, postEnd = size - 1;

	TreeNode *root = buildTree(inOrder, postOrder, inStart, inEnd, postStart, postEnd);

	inOrderPrint(root);

	printf("\n");
}
