
/*
 	628. Maximum Product of Three Numbers

	Given an integer array nums, find three numbers whose product is maximum and return the maximum product.

	Input: nums = [1,2,3]
	Output: 6

	Input: nums = [1,2,3,4]
	Output: 24

	Input: nums = [-1,-2,-3]
	Output: -6

	Constraints:

	    3 <= nums.length <= 104
	    -1000 <= nums[i] <= 1000
*/

/*	1 - Brute
 *
 *	 Try to find all the triplets in the array while maintaining the maximum value of their products.
*/

/*

#include<stdio.h>

int maximum(int arr[], int size){
		
	int maxProduct = -10000;

	for(int i = 0; i < size/2; i++){
			
		for(int j = i + 1; j < size-1; j++){
				
			for(int k = j + 1; k < size; k++){
					
				int tripleProduct = arr[i] * arr[j] * arr[k];
				maxProduct = Math.max(maxProduct, tripleProduct);
			}
		}
	}

	return maxProduct;
}
	
void main(){
		
	int arr[] = {1,2,3,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = maximum(arr, size);

	printf("%d\n", ret);
}

*/

/*	 Time Complexity: O(n³)

	Space Complexity: O(1) 

*/

/*	2] Better
 *
 *	 Using sorting: Sort the input array. The maximum product would be the product of the last three elements or the starting two elements with the last element.
*/

/*
 *
 * 	 If you will think for a second, you may realize that the maximum product of any three numbers would be the product of the greatest three numbers or the smallest two numbers with the greatest number from the nums array.

	The reason why the maximum product of any triplet could have two smallest numbers because there could be negative numbers in the nums array and the product of the two smallest numbers could be largely positive if the numbers are greater than the 2nd and 3rd largest number of the array.

	So if we will sort the array in ascending order, then the largest three numbers will be available at the end of the array. Also, the smallest two numbers will be available at the beginning of the array.

*/
/*

int maximum(int arr[], int size){
		
	int maxProduct = -10000;

	sort(arr);

	maxProduct = Math.max(maxProduct, arr[0] * arr[1] * arr[size-1]);

	maxProduct = Math.max(maxProduct, arr[size-3] * arr[size-2] * arr[size-1]);

	return maxProduct;
}

*/

/*
 *	 Time Complexity: O(n log n), due to sorting.
	Space Complexity: O(log n), Sorting stack space using merge sort or quicksort. 
 *
*/ 

/*	3] Optimal
 *
 *	Single scan: Create 5 variables to maintain the largest, second-largest, third-largest, smallest, and second smallest. The combination of three would be the maximum product.
*/

#include<stdio.h>

int max(int x, int y){
	
	if(x > y){
		
		return x;
	}

	return y;
}
	
int maximum(int arr[], int size){
		
	int min1 = 10000, min2 = 10000;
	int max1 = -10000, max2 = -10000, max3 = -10000;

	for(int i = 0; i < size; i++){
			
		if(arr[i] <= min1){
				
			min2 = min1;
			min1 = arr[i];
		}else if(arr[i] <= min2){
				
			min2 = arr[i];
		}
		if(arr[i] >= max1){
				
			max3 = max2;
			max2 = max1;
			max1 = arr[i];
		}else if(arr[i] >= max2){
				
			max3 = max2;
			max2 = arr[i];
		}else if(arr[i] >= max3){
				
			max3 = arr[i];
		}
	}

	return max(min1 * min2 * max1, max1 * max2 * max3);
}

void main(){
		
	int arr[] = {1,2,3,4};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = maximum(arr, size);

	printf("%d\n",ret);
}

