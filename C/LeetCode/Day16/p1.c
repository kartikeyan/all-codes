
/*	Prog : Count Square Sum triplets
 *
 *	A Square triplets (a,b,c) is a triple where a,b,and c are integers
 *	and a*a + b*b = c*c
 *
 *	Given an integer n, return the number of sqaure triplets such that
 *	1 <= a , b , c <= n
 *
 *
 *	Input : n = 5
 *	Output : 2
 *	Expla : The square triplets are (3,4,5) and (4,3,5)
 *
 *	Input : n = 10
 *	Output : 4
 *	Expla : The square triplets are (3,4,5), (4,3,5), (6,8,10) and (8,6,10)
*/

#include<stdio.h>

/*
int countTriplets(int n){
	
	int count = 0;

	for(int i = 1; i <= n; i++){
		
		for(int j = i + 1; j <= n ; j++){
			
			for(int k = j + 1; k <= n; k++){
				
				if(i * i + j * j == k * k){
					
					count = count+2;
				}
			}
		}
	}

	return count;
}

*/
int countTriplets(int n){
	
	int count = 0;

	for(int i = 1; i <= n; i++){
		
		for(int j = 1; j <= n ; j++){
			
			for(int k =  1; k <= n; k++){
				
				if(i * i + j * j == k * k){
					
					count++;
				}
			}
		}
	}

	return count;
}

void main(){
	
	int n = 10;

	int ret = countTriplets(n);

	printf("%d\n", ret);
}
