/*	Program 1 : Power of Two
 *
 *	Given an integer n, return "true" if it is a power of two. Otherwise return "false".
 *	An integer n is a power of two, if there exists an integer x such that n == 2^x.
 *
 *	Input : n == 1			Input : n == 16			Input : n = 3
 *	Output : true			Output : true			Output : false
 *	Explanation : 2^0 = 1 		Explanation : 2^4 = 16
*/

#include<stdio.h>
#include<stdbool.h>

bool isPowerOfTwo(int n){

	if(n == 1){
		return true;
	}
	if(n < 1){
		return false;
	}

	return isPowerOfTwo(n / 2);
}

/*
bool isPowerOfTwo(int n){
	
	if(n <= 0){
		return false;
	}

	if(n == 1){
		return true;
	}

	if(n % 2 != 0){
		return false;
	}

	return isPowerOfTwo(n / 2);
}
*/
void main(){
	
	int num;
	printf("Enter Num\n");
	scanf("%d", &num);

	bool ret = isPowerOfTwo(num);
	
	printf("%d\n", ret);
}
