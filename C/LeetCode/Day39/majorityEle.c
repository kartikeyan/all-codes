
/*	Prog : Given an array nums of size n, return the majority element
 *
 *	The majority element is the element that appears more than [n/2] times. You may assume that majority element always exists in the array
 *
 *	Input : arr = [3,2,3]
 *	Ouput : 3
 *
 *	Input : nums = [2,2,1,1,1,2,2]
 *	Ouput : 2
 *
 *	Constraints :
 *		
 *		n == nums.length
 *
 *		1 <= n <= 5 * 10^4
 *
 *		-10^9 <= nums[i] <= 10^9
*/

#include<stdio.h>

/*
int majorityEle(int arr[], int size){
	
	int count1 = 0, ele;

	for(int i = 0; i < size; i++){
		
		if(count1 == 0){

			count1 = 1;
			ele = arr[i];

		}else if(arr[i] == ele){
			
			count1++;
		}else{
			
			count1--;
		}
	}

	int  count2 = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] == ele){
			
			count2++;
		}
	}

	if(count2 > (size/2)){
		
		return ele;
	}
	return -1;
}
*/

int majorityEle(int *arr, int size){
	
	int count = 0, num = 0;

	for(int i = 0; i < size; i++){
		
		if(count == 0){
			
			count = 1;
			num = arr[i];
		}else{
			
			if(arr[i] == num){
				
				count++;
			}else
				count--;
		}
	}

	return num;
}

void main(){
	
	int arr[] = {2,2,1,1,1,2,2};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = majorityEle(arr, size);

	printf("%d\n", ret);
}
