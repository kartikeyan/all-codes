
/*
 	100. Same Tree

	Given the roots of two binary trees p and q, write a function to check if they are the same or not.

	Two binary trees are considered the same if they are structurally identical, and the nodes have the same value.

	Input: p = [1,2,3], q = [1,2,3]
	Output: true

	Input: p = [1,2], q = [1,null,2]
	Output: false

	Input: p = [1,2,1], q = [1,1,2]
	Output: false

	Constraints:

	    The number of nodes in both trees is in the range [0, 100].
	    -104 <= Node.val <= 104
*/


#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct Node{
	
	int data;
	struct Node *left;
	struct Node *right;
};

struct Node *newNode(int val){
	
	struct Node *node = (struct Node*)malloc(sizeof(struct Node));

	node->data = val;
	node->left = NULL;
	node->right = NULL;

	return node;
}

bool sameTree(struct Node *root1, struct Node *root2){
	
	if(root1 == NULL && root2 == NULL){
		
		return true;
	}

	if(root1 == NULL || root2 == NULL){

		return false;
	}

	if(root1->data != root2->data){
		
		return false;
	}

	return sameTree(root1->left, root2->left) && sameTree(root1->right, root2->right);
}

void main(){
	
	struct Node* root1 = newNode(3);

	root1->left = newNode(9);
	root1->right = newNode(20);
	
	root1->right->left = newNode(15);
	root1->right->right = newNode(7);

	struct Node* root2 = newNode(3);

	root2->left = newNode(9);
	root2->right = newNode(20);
	
	root2->right->left = newNode(15);
	root2->right->right = newNode(7);

	
	bool ret = sameTree(root1, root2);

	if(ret == true){
		
		printf("Same Tree\n");
	}else{
		printf("Not Same tree\n");
	}
}
