
//	Program 1 : Count Opeations to obtain Zero

#include<stdio.h>

int countOp(int num1, int num2){
	
	if(num1 == 0 || num2 == 0){
		
		return 0;
	}

	if(num1 == num2){
		
		return 1;
	}

	int count = 0;

	while(1){
		
		if(num1 >= num2){
			
			num1 = num1 - num2;
			count++;
		}else{
			
			num2 = num2 - num1;
			count++;
		}

		if(num1 == 0 || num2 == 0){
			
			return count;
		}
	}
}

void main(){
	
	int num1 = 2, num2 = 3;
	
	int ret = countOp(num1, num2);
	printf("%d\n", ret);
}
