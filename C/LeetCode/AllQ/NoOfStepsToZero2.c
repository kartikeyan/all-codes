
//	Program 2 : Number of Steps to Reduce a Number to Zero

#include<stdio.h>

int numberofSteps(int num){
	
	int count = 0;

	while(num){
		
		if(num % 2 == 0){
			
			num = num / 2;
		}else{
			
			num = num - 1;
		}

		count++;
	}

	return count;
}

void main(){
	
	int num = 14;

	int ret = numberofSteps(num);
	printf("%d\n", ret);
}
