
/*	Prog 4 : Square Root
	
	Input : 8	, Input : 4
	Ouput : 2	, Ouput : 2
*/

#include<stdio.h>

int mySqrt(int num){
	
	int start = 0, end = num;

	long long ans =  -1, mid;

	while(start <= end){
		
		mid = start + (end - start) / 2;

		long int square = mid * mid;

		if(square == num){
			
			return mid;
		}

		if(square > num){
			
			end = mid - 1;
		}

		if(square < num){
			
			ans = mid;
			start = mid + 1;
		}
	}

	return ans;
}

void main(){
	
	int num = 8;

	int ret = mySqrt(num);

	printf("Sqaure Root is : %d\n", ret);
}
