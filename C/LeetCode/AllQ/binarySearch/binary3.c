
//	Prog 3 : Binary Search

#include<stdio.h>

int binary(int arr[], int size, int search){
	
	int start = 0, end = size - 1, mid;

	while(start <= end){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] > search){
			
			end = mid - 1;
		}

		if(arr[mid] < search){
			
			start = mid + 1;
		}
	}
}

void main(){
	
	int arr[] = {1,2,4,5,6,7,8};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int search = 8;

	int ret = binary(arr, size, search);
	printf("%d\n", ret);
}
