
/*	Program 5 : Valid Perfect Square
 *
 *	Input : num = 16   ,  num = 14
 *	Ouput : true	   ,  Ouput : false
*/

#include<stdio.h>
#include<stdbool.h>

bool isPerfect(int n){
	
	int start = 0, end = n;

	while(start <= end){
		
		long mid = start + (end - start) / 2;

		long square = mid * mid;

		if(square == n){
			
			return true;

		}else if(square < n){
			
			start = mid + 1;

		}else{

			end = mid - 1;
		}
	}

	return true;
}

void main(){
	
	int n = 16;

	bool ret = isPerfect(n);
	printf("%d\n", ret);
}
