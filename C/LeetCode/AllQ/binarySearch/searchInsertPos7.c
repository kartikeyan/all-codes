
/*	Program 7 : Search Insert Position
 *
 *	Input : nums[] = {1,3,5,7},  target = 6
 *	Ouput : 3
 *
 *	Input :	nums[] = {1,3,5,6}, target = 2
 *	Ouput : 1
*/

#include<stdio.h>

int search(int arr[], int size, int target){
	
	int start = 0, end = size - 1, mid;

	if(target < arr[start]){
		
		return 0;
	}

	if(target > arr[end]){
		
		return size;
	}

	while(start <= end){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == target){
			
			return mid;
		}

		if(arr[mid] < target){
			
			start = mid + 1;
		}

		if(arr[mid] > target){
			
			end = mid - 1;
		}
	}

	return end + 1;
}

void main(){
	
	int arr[] = {1,3,5,6};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int target = 2;

	int ret = search(arr, size, target);
	printf("%d\n", ret);
}

