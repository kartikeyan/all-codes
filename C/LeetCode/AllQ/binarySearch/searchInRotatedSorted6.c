
/*	Program 6 : Search In Rotated Sorted Array
 *
 *	Input : nums[] = {4,5,6,7,0,1,2},  target = 0
 *	Ouput : 4
 *
 *	Input :	nums[] = {4,5,6,7,0,1,2}, target = 3
 *	Ouput : -1
*/

#include<stdio.h>

int search(int arr[], int size, int target){
	
	int start = 0, end = size - 1, mid;

	while(start <= end){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == target){
			
			return mid;
		}

		if(arr[mid] < arr[start]){
			
			if(arr[mid] < target && arr[end] >= target){
				
				start = mid + 1;
			}else{
				end = mid - 1;
			}
		}else{
			
			if(arr[mid] > target && arr[start] <= target){
				
				end = mid - 1;
			}else{
				
				start = mid + 1;
			}
		}
	}

	return -1;
}

void main(){
	
	int arr[] = {4,5,6,7,0,1,2};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	int target = 0;

	int ret = search(arr, size, target);
	printf("%d\n", ret);
}

