
/*	Prog : Contains Duplicate
 *
 *	Given an integer array "nums", return true if any value appears "at least twice" in the array, and return "false" if every element is distinct
 *
 *	Input : nums = [1,2,3,1]	Input : nums = [1,1,1,3,3,4,3,2,4,2]
 *	Output : true			Output : true 
 *
 *	Input : nums = [1,2,3,4]
 *	Ouput : false
*/	

/*	Approach : 1] Sort the array then check if array conatins any duplicates
*/

#include<stdio.h>
#include<stdbool.h>

void merge(int arr[], int start, int mid, int end){

	int ele1 = mid - start + 1;
	int ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + j + 1];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2  < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

bool containsDuplicates(int arr[], int arrSize){

	int start = 0, end = arrSize - 1;

	mergeSort(arr, start, end);

	for(int i = 0; i < arrSize - 1; i++){
		
		if(arr[i] == arr[i+1]){
			
			return true;
		}
	}

	return false;
}

void main(){
	
	int arr[] = {1,2,3,1};

	int arrSize = sizeof(arr) / sizeof(arr[0]);

	bool ret = containsDuplicates(arr, arrSize);

	if(ret == true){
	
		printf("Array Contains Duplicates\n");	
	}else{
		
		printf("Array Does not Contains Duplicates\n");	
	}
}
