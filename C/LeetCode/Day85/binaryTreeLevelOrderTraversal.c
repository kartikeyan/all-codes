
/*
 	102. Binary Tree Level Order Traversal

	Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).

	Input: root = [3,9,20,null,null,15,7]
	Output: [[3],[9,20],[15,7]]

	Input: root = [1]
	Output: [[1]]

	Input: root = []
	Output: []
	
	Constraints:

	    The number of nodes in the tree is in the range [0, 2000].
	    -1000 <= Node.val <= 1000
*/

//	Using Recursion

/*

#include <stdio.h>
#include <stdlib.h>

struct node {
	int data;
	struct node *left, *right;
};

//	Function prototypes 
void printCurrentLevel(struct node* root, int level);
int height(struct node* node);
struct node* newNode(int data);

void printLevelOrder(struct node* root)
{
	int h = height(root);
	int i;
	for (i = 1; i <= h; i++)
		printCurrentLevel(root, i);
}


void printCurrentLevel(struct node* root, int level)
{
	if (root == NULL)
		return;
	if (level == 1)
		printf("%d ", root->data);
	else if (level > 1) {
		printCurrentLevel(root->left, level - 1);
		printCurrentLevel(root->right, level - 1);
	}
}

int height(struct node* node)
{
	if (node == NULL)
		return 0;
	else {
		//	compute the height of each subtree
		int lheight = height(node->left);
		int rheight = height(node->right);

		//	use the larger one
		if (lheight > rheight)
			return (lheight + 1);
		else
			return (rheight + 1);
	}
}

struct node* newNode(int data)
{
	struct node* node
		= (struct node*)malloc(sizeof(struct node));
	node->data = data;
	node->left = NULL;
	node->right = NULL;

	return (node);
}

int main()
{
	struct node* root = newNode(1);
	root->left = newNode(2);
	root->right = newNode(3);
	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("Level Order traversal of binary tree is \n");
	printLevelOrder(root);

	printf("\n");

	return 0;
}

*/

//	2 - Using Queue

#include <stdio.h>
#include <stdlib.h>
#define MAX_Q_SIZE 500

struct node {
	int data;
	struct node* left;
	struct node* right;
};

struct node** createQueue(int*, int*);
void enQueue(struct node**, int*, struct node*);
struct node* deQueue(struct node**, int*);


void printLevelOrder(struct node* root)
{
	int rear, front;
	struct node** queue = createQueue(&front, &rear);
	struct node* temp_node = root;

	while (temp_node) {
		printf("%d ", temp_node->data);

		/*Enqueue left child */
		if (temp_node->left)
			enQueue(queue, &rear, temp_node->left);

		/*Enqueue right child */
		if (temp_node->right)
			enQueue(queue, &rear, temp_node->right);

		/*Dequeue node and make it temp_node*/
		temp_node = deQueue(queue, &front);
	}
}

struct node** createQueue(int* front, int* rear)
{
	struct node** queue = (struct node**)malloc(
		sizeof(struct node*) * MAX_Q_SIZE);

	*front = *rear = 0;
	return queue;
}

void enQueue(struct node** queue, int* rear,
			struct node* new_node)
{
	queue[*rear] = new_node;
	(*rear)++;
}

struct node* deQueue(struct node** queue, int* front)
{
	(*front)++;
	return queue[*front - 1];
}


struct node* newNode(int data)
{
	struct node* node
		= (struct node*)malloc(sizeof(struct node));
	node->data = data;
	node->left = NULL;
	node->right = NULL;

	return (node);
}

int main()
{
	struct node* root = newNode(1);
	root->left = newNode(2);
	root->right = newNode(3);
	root->left->left = newNode(4);
	root->left->right = newNode(5);

	printf("Level Order traversal of binary tree is \n");
	printLevelOrder(root);
	
	printf("\n");
	return 0;
}

