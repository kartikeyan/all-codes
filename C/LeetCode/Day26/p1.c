
/*	Prog : Find Minimum in Rotated Sorted Array 2
 *
 *	Suppose an array of length "n" sorted in ascending order in "rotated" between 1 and "n" times. For example, the array nums = [0,1,4,4,5,6,7] might become :
 *		[4,5,6,7,0,1,4] if it was rotated "4" times
 *
 *		[0,1,4,4,5,6,7] if it was rotated "7" times
 *
 *	
 *	Notice that "rotating" an array [a[0],a[1],a[2],...,a[n-1]] 1 time results in the array [a[n-1],a[0]].
 *
 *	Given the sorted rotated array "nums" that may contain "duplicates" return the minimum element of this array
 *
 *	You must decrease the overall operations steps as much as possible.
 *
 *	Input : nums = [1,3,5]
 *	Output : 1
 *
 *	Input : nums = [2,2,2,0,1]
 *	Ouput : 0
 *
 *	Constraits :
 *
 *		1 <= n <= 5000
 *
 *		-5000 <= nums[i] <= 5000
*/

#include<stdio.h>

void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

	int arr1[ele1], arr2[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int  itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

int findMin(int arr[], int size){
	
	mergeSort(arr, 0, size-1);

	return arr[0];
}

void main(){
	
	int arr[] = {2,2,2,0,1};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = findMin(arr, size);

	printf("Ret is : %d\n", ret);
}
