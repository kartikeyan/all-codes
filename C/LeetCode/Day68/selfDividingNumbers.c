
/*
 	728. Self Dividing Numbers

	A self-dividing number is a number that is divisible by every digit it contains.

    	For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.

	A self-dividing number is not allowed to contain the digit zero.

	Given two integers left and right, return a list of all the self-dividing numbers in the range [left, right].

	Input: left = 1, right = 22
	Output: [1,2,3,4,5,6,7,8,9,11,12,15,22]

	Input: left = 47, right = 85
	Output: [48,55,66,77]

	Constraints:

	    1 <= left <= right <= 104
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

bool isSelfDividing(int num){
		
	int temp = num;

	while(temp > 0){
			
		int digit = temp % 10;

		if(digit == 0 && num % digit != 0){
				
			return false;
		}

		temp /= 10;
	}

	return true;
}

void selfDividingNumber(int left, int right, int *newArr ,int *returnSize){

	*returnSize = 0;

	for(int i = left; i <= right; i++){
			
		if(isSelfDividing(i)){
				
			newArr[*returnSize] = i;
			(*returnSize)++;
		}
	}
}
	
void main(){
		
	int left = 1, right = 22;

	int *newArr = (int *)malloc(sizeof(int) * (right-left+1));

	int *returnSize;

	selfDividingNumber(left, right, newArr, returnSize);

	for(int i = 0; i < right-left+1; i++){
			
		printf("%d\n", newArr[i]);
	}
}
