
/*	
 	724. Find Pivot Index

	Given an array of integers nums, calculate the pivot index of this array.

	The pivot index is the index where the sum of all the numbers strictly to the left of the index is equal to the sum of all the numbers strictly to the index's right.

	If the index is on the left edge of the array, then the left sum is 0 because there are no elements to the left. This also applies to the right edge of the array.

	Return the leftmost pivot index. If no such index exists, return -1.

	Input: nums = [1,7,3,6,5,6]
	Output: 3
	Explanation:
		The pivot index is 3.
		Left sum = nums[0] + nums[1] + nums[2] = 1 + 7 + 3 = 11
		Right sum = nums[4] + nums[5] = 5 + 6 = 11

	Input: nums = [1,2,3]
	Output: -1
	Explanation:
		There is no index that satisfies the conditions in the problem statement.

	Input: nums = [2,1,-1]
	Output: 0
	Explanation:
		The pivot index is 0.
		Left sum = 0 (no elements to the left of index 0)
		Right sum = nums[1] + nums[2] = 1 + -1 = 0

	Constraints:

	    1 <= nums.length <= 104
	    -1000 <= nums[i] <= 1000
*/

#include<stdio.h>

int pivotIndex(int arr[], int size){
		
	int allSum = 0, temp = 0;

	for(int i = 0; i < size; i++){
			
		allSum = allSum + arr[i];
	}

	for(int j = 0; j < size; j++){
			
		allSum = allSum - arr[j];

		if(allSum == temp){
				
			return j;
		}

		temp = temp + arr[j];
	}

	return -1;
}


/*
 int pivotIndex(int arr[], int size){
		
	int allSum = 0, lSum = 0;

	for(int i = 0; i < size; i++){
			
		allSum = allSum + arr[i];
	}

	int rSum = allSum;

	for(int j = 0; j < size; j++){
			
		rSum = rSum - arr[j];

		if(lSum == rSum){
				
			return j;
		}

		lSum = lSum + arr[j];
	}

	return -1;
}
*/

/*	
int pivotIndex(int arr[], int size){
		
	int total = 0, left = 0;

	for(int i = 0; i < size; i++){
			
		total = total + arr[i];
	}

	for(int j = 0; j < size; j++){

		if(left == total - left - arr[j]){
				
			return j;
		}

		left = left + arr[j];
	}

	return -1;
}
*/
void main(){
		
	int arr[] = {1,7,3,6,5,6};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = pivotIndex(arr, size);

	printf("%d\n", ret);
}
	
