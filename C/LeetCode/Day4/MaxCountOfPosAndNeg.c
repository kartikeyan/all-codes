
/*	Program 1 : Maximum Count of Positive Integer and Negative Unteger
 *
 *	Given an array nums sorted in "non-decreasing" order, return the maximum between the
 *	number of positive integers and the number of negative integers
 *
 *	In other words, if the number of positive integers in nums is "pos" and the number of
 *	negative integer is "neg", then return the maximum of "pos" and "neg"
 *
 *	Input : nums = [-2,-1,-1,1,2,3]
 *	Output : 3
 *
 *	Input : nums = [-3,-2,-1,0,0,1,2]
 *	Ouput : 3
*/

#include<stdio.h>

int maximumCount(int arr[], int size){
	
	int pos = 0, neg = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] > 0){
			
			pos++;
		}
		if(arr[i] < 0){
			
			neg++;
		}
	}

	if(pos > neg){
		
		return pos;
	}else{
		return neg;
	}
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int ret = maximumCount(arr, size);
	printf("Count is : %d\n", ret);
}
