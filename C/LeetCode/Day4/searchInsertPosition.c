
/*	Program 2 : Search Insert Position
 *
 *	Given a sorted array of distinct integers and a target value, return the index
 *	if the target is found.If not, return the index where it would be it were inserted in order.
 *	
 *	You must write an algorithm with "0(log n)" runtime complexity
 *
 *	Input : nums = [1,3,5,6], target = 5
 *	Output : 2
 *
 *	Input : nums = [1,3,5,6], target = 2
 *	Ouput : 1
*/

#include<stdio.h>

int searchInsert(int arr[], int size, int search){
	
	int start = 0, end = size - 1, mid;
	
	if(search < arr[start]){
		
		return 0;
	}

	if(search > arr[end]){
		
		return size;
	}

	while(start <= end){
		
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] < search){
			
			start = mid + 1;
		}

		if(arr[mid] > search){
			
			end = mid - 1;
		}
	}

	return end + 1;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret = searchInsert(arr, size, search);
	printf("Pos is : %d\n", ret);
}
