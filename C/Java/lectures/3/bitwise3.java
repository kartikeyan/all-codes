
//	Bitwise Operator (&, |, ^, <<, >>, >>>, ~)

class Bitwise3{
	
	public static void main(String[] args){
		
		int x = 8, y = 10;

		System.out.println(x & y);	// 8
		System.out.println(x | y);	// 10
		System.out.println(x ^ y);	// 2
	}
}

