
//	Bitwise Operator (&, |, ^, <<, >>, >>>, ~)

class Bitwise2{
	
	public static void main(String[] args){
		
		int x = 9, y = 14;

		System.out.println(x & y);	// 8
		System.out.println(x | y);	// 15
	}
}

