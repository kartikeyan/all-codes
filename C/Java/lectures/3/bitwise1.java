
//	Bitwise Operator (&, |, ^, <<, >>, >>>, ~)

class Bitwise{
	
	public static void main(String[] args){
		
		int x = 5, y = 7;

		System.out.println(x & y);	// 5
		System.out.println(x | y);	// 7
	}
}

