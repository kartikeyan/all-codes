#include<stdio.h>
#include<pthread.h>

void *run(){
	
	printf("Child Thread\n");

	printf("Child = %ld\n", pthread_self());
}

void main(){
	
	pthread_t tid;

	printf("Main Thread : %ld\n", pthread_self());

	pthread_create(&tid, NULL, run, NULL);

	pthread_join(tid, NULL);

	printf("Main Thread\n");
}


