
/*	Prog : Creating Thread in Java
 *		
 *		1] "Using Thread Class"
 *		2] "Using Runnable Interface"
*/

//	1] Runnable Interface

class RunnableDemo implements Runnable{
	
	public void run(){
		
		System.out.println("Child Thread");
		System.out.println(Thread.currentThread().getName());		// Child Thread la by default name asto Thread - 0
	}

	public static void main(String[] args){
		
	
		//Thread Create
		RunnableDemo obj = new RunnableDemo();
		
		Thread t1 = new Thread(obj);

		//Thread Start
		t1.start();

		System.out.println("Main Thread");
		System.out.println(Thread.currentThread().getName());
	}
}
