/*	Program 1 :
 *
 *	fork() system call
*/	

#include<stdio.h>
#include<unistd.h>

void main(){

	int retval = fork();

	printf("Child Process ID %d\n", getpid());

	printf("Parent Process ID %d\n", getppid());

//	printf("%d\n", retval);
}
