/*	lec No - 15
 *
*/

#include<stdio.h>
#include<unistd.h>

int main(){

	int pid = fork();

	if(pid > 1){
	
		printf("Parent = %d\n", getpid());			// parent id
		printf("Child cha Ajoba = %d\n", getppid());		// bash id
	}else if(pid == 0){	
	
		printf("child = %d\n", getpid());			// child id
		printf("Child cha parent = %d\n", getppid());		// parent id
	}else{
	
		return -1;
	}
}

