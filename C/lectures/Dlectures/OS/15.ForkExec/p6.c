/*	lec No - 15
 *	
 *	Program 5 : Bash Code
*/

#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

int main(){

	int pid = fork();

	if(pid > 1){							//Parent
		
		wait(NULL);

		printf("Parent = %d\n", getpid());
		printf("Child cha Ajoba = %d\n", getppid());		

	}else if(pid == 0){						//Child
		
		printf("child = %d\n", getpid());
		printf("Child cha parent = %d\n", getppid());			
		
		execlp("/bin/cp", "dummy","shashi","Kartik", NULL);
		
	}else{
	
		return -1;
	}
}
