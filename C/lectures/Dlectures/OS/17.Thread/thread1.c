
/*	lect 17 : Thread
 * 		
 *	Program 1 : C madla Thread
*/

#include<stdio.h>
#include<pthread.h>

void fun(){

	printf("Start Fun\n");
	printf("%ld\n", pthread_self());
	printf("End Fun\n");
}

void main(){

	printf("Start Main\n");

	printf("%ld\n", pthread_self());

	fun();

	printf("End Main\n");
}
