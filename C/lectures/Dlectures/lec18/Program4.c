/*	WAP to print sum of even numbers
 *	between given range
*/

#include<stdio.h>

void main(){

	int num1, num2;
       	int sum = 0;

	printf("Enter Number1 :\n");
	scanf("%d", &num1);

	printf("Enter Number2 :\n");
	scanf("%d", &num2);

	for(int i = num1; i <= num2; i++){
	
		if(i % 2 == 0){
			sum = sum + i;
		}
	}
	printf("sum of even numbers is :%d\n", sum);
}
