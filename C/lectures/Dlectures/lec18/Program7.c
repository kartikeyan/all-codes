/*	WAP to print the count of numbers 
 *	divisible by 5 between given range
*/

#include<stdio.h>


void main(){

	int x,y, count = 0;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	for(int i = x; i <= y; i++){
		
		if(i % 5 == 0){
			count++;
		}
	}
	printf("Counts divisible by 5 are :%d\n", count);
}
