/*	Program 3 : WAP to print 1 to 10 using for loop 
 *		    initializing it with 0 only
*/

#include<stdio.h>

void main(){
	
	for(int i=0; i<10; i++){
		printf("%d\n", i+1);
	}
}
