/*	Program 4 :
 *
 *	Topic : Pointer Addition to Integer 
 *		
 *		Pointer addition to interger hoto
*/

#include<stdio.h>
void main(){

	int x = 10;

	int y = 20;
	
	int *ptr = &x;

	printf("%d\n", *ptr);
	
	printf("%d\n", *(ptr + 1));
}

/*	
 *	*(ptr + 1)
 *
 *	*(100 + 1 * sizeof(DTP))
 *
 *	*(100 + 1 * sizeof(int))
 *
 *	*(100 + 1 * 4)
 *
 *	*(104)
 *
 *	ValueAt(104)
 *
 *	20
*/	

