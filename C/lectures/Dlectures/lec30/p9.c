/*	Program 9 : 
 *
 *	Pointer cha addition fakt interger, character sobat hoto.
 *
*/

#include<stdio.h>
void main(){

	char ch1 = 'x';
	char ch2 = 'y';

	char *ptr = &ch1;

	printf("%p\n", ptr);
	printf("%c\n", *ptr);

	printf("%p\n", (ptr + 'A'));
	printf("%c\n", *(ptr + 'A'));
}

/*	Character internally ASCII value jato mhnje
 *	interger value. Tar pointer ani integer cha addition hoto.
 *	'A' cha ASCII value ahe 65. Jar pointer ani 'A' cha addition kela
 *	tar additon "PROCESS ADDRESS SPACE MADHE" yeu shakto mahnun 2nd printf
 *	line la "GARBAGE VALUE KIVA SEGEMENTATION FAULT" yeu shakto.
*/	
