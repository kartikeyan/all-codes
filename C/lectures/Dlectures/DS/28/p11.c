/*	PRogram 10 :
 *
 *	Topic : Recursion in Array
*/

//	Palindrome

#include<stdio.h>
#include<stdbool.h>

bool isPalindrome(char *arr, int start, int end){

	if(start >= end){
		
		return true;
	}

	if(arr[start] == arr[end]){
		
		return isPalindrome(arr, start+1,end-1);
	}else{
	
		return false;
	}
}

void main(){

	char *arr = "appa";

	int size = sizeof(arr) / sizeof(arr[0]);

	bool ret = isPalindrome(arr, size);

	if(ret == true){
	
		printf("String is Palindrome\n");
	}else{
	
		printf("String is not a Palindrome\n");
	}
}
