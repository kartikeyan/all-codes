/*	PRogram 2 :
 *
 *	Fibonacci Series
*/

#include<stdio.h>

void fib(int n){

	int a = 0;		// 0 and 1 ka ghetlet karan aplyala fibonacci kadtana magche 2 value preseve karava lagto
	int b = 1;		// mhnje fibonacci is sum of previous 2 terms tyamule magche 2 value lagto
	int c;

	for(int i = 2; i <= n; i++){
	
		c = a + b;
		a = b;
		b = c;
	}

	printf("%d\n", c);

}

void main(){

	int n;
	printf("Enter Number\n");
	scanf("%d", &n);

	fib(n);
}
