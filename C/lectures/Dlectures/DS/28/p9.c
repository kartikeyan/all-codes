/*	PRogram 9 :
 *
 *	Topic : Recursion in Array
*/

//	Palindrome

#include<stdio.h>
#include<stdbool.h>

bool isPalindrome(char *arr, int size){

	int start = 0;
	int end = size-1;

	while(start < end){
		
		if(arr[start] == arr[end]){
			
			start++;
			end--;
		}else{
		
			return false;
		}
	}

	return true;
}

void main(){

	char *arr = "appa";

	int size = sizeof(arr) / sizeof(arr[0]);

	bool ret = isPalindrome(arr, size);

	if(ret == true){
	
		printf("String is Palindrome\n");
	}else{
	
		printf("String is not a Palindrome\n");
	}
}
