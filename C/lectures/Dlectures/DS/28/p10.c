/*	PRogram 10 :
 *
 *	Topic : Recursion in Array
*/

//	Palindrome

#include<stdio.h>
#include<stdbool.h>

bool isPalindrome(char *arr, int start, int end){

	if(start >= end){
		
		return true;
	}

	if(arr[start] == arr[end] && isPalindrome(arr, start+1, end-1)){
		
		return true;
	}

	return false;
}

void main(){

	char arr[] = "appa";

	bool ret = isPalindrome(arr, 0, 3);

	if(ret == true){
	
		printf("String is Palindrome\n");
	}else{
	
		printf("String is not a Palindrome\n");
	}
}
