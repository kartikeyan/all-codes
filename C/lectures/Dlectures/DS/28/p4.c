/*	PRogram 4 :
 *
 *	Topic : Recursion in Array
*/

//	Sum of whole array
#include<stdio.h>

int sumArray(int *arr, int size){

	int sum = 0;

	for(int i = 0; i < size; i++){
	
		sum = sum + *(arr + i);
	}

	return sum;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}

	printf("Array Elements are\n");
	for(int i = 0; i < size; i++){
	
		printf("%d\n", arr[i]);
	}

	int ret = sumArray(arr, size);
	printf("Sum is : %d\n",ret); 
}
