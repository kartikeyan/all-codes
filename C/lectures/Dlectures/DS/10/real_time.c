/*	Program 3 :
 *
 *	Real Time Example of Singly LL
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct LED{

	char color[20];
	float current;
	int warr;

	struct LED *next;
}led;

led *head = NULL;

//0 - Function

led *createNode(){

	led *newNode = (led*)malloc(sizeof(led));

	getchar();
	printf("Enter Led Color :\n");

	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
	
		(*newNode).color[i] = ch;
		i++;
	}

	printf("Enter Current required to LED\n");
	scanf("%f", &newNode->current);

	printf("Enter Warranty of LED\n");
	scanf("%d", &newNode->warr);

	newNode->next = NULL;

	return newNode;
}

//1
void addNode(){

	led *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		led *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

//2
void addFirst(){

	led *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		newNode->next = head;
		head = newNode;
	}
}

//3
void addLast(){

	addNode();
}

//5
int countNodes(){

	led *temp = head;
	
	int count = 0;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	printf("COunt is :%d", count);

	return count;
}

//4
int addAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos >= count + 2){
	
		printf("INvalid position to insert\n");
		return -1;

	}else{
		if(pos == 1){
			addFirst();
		}
		else if(pos == count + 1){
			addLast();
		}else{
	
			led *temp = head;

			while(pos - 2){
		
				temp = temp->next;
				pos--;
			}

			led *newNode = createNode();

			newNode->next = temp->next;
			temp->next = newNode;
		}

		return 0;
	}
}

//6
int deleteFirst(){
	
	int count = countNodes();

	if(head == NULL){
	
		printf("Single LED attach nahi ahe\n");
		return -1;

	}else if(count == 1){
	
		free(head);
		head = NULL;
		printf("First LED is removed\n");
	}else{
	
		led *temp = head;

		head = head->next;		//1 st way
		//head = temp->next;		//2 nd way

		free(temp);
	}
}

//7

void deleteLast(){

	led *temp = head;

	int count = countNodes();

	if(count == 1){
	
		free(temp);
		head = NULL;
		printf("Last Led is removed\n");

	}else if(head->next != NULL){
	
		while(temp->next->next != NULL){
		
			temp = temp->next;
		}

		free(temp->next);
		temp->next = NULL;
	}else{
	
		printf("No led is present to delete\n");
	}
}

//8

int deleteAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos > count){
	
		printf("Invalid position to delete\n");
		return -1;

	}else{
	
		if(pos == 1){
			deleteFirst();

		}else if(pos == count){
			
			deleteLast();
		}else{
		
			led *temp = head;

			while(pos - 2){
				
				temp = temp->next;
				pos--;
			}

			led *temp1 = temp->next;
			temp->next = temp->next->next;

			free(temp1);
		}

		return 0;
	}
}

//9

void printLL(){

	led *temp = head;

	while(temp->next != NULL){
	
		printf("|%s : %f : %d|->", temp->color, temp->current, temp->warr);

		temp = temp->next;
	}

	printf("|%s : %f : %d|", temp->color, temp->current, temp->warr);
}

void main(){

	char choice;

	do{
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addLast\n");
		printf("4.addAtPos\n");
		printf("5.countNodes\n");
		printf("6.deleteFirst\n");
		printf("7.deleteLast\n");
		printf("8.deleteAtPos\n");
		printf("9.printLL\n");
		printf("10.NoofNodes\n");

		int ch;
		printf("\nEnter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:
				{
				int pos;
				printf("Enter Pos :\n");
				scanf("%d", &pos);

				addAtPos(pos);

				}
				break;
			case 5:
				countNodes();
				break;
			case 6:
				deleteFirst();
				break;
			case 7:
				deleteLast();
				break;
			case 8:
				{
				int pos;
				printf("Enter Pos to delete\n");
				scanf("%d", &pos);

				deleteAtPos(pos);
				}
				break;
			case 9:
				printLL();
				break;
			case 10:
				{
				int num;
				printf("Enter Nodes u directly want to create at once\n"); 
				scanf("%d", &num);

				for(int i = 1; i <= num; i++)
					addNode();
				}
				break;

			default:
				printf("Enter Valid Choice\n");
				break;
		
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);


	}while(choice == 'Y' || choice == 'y');
}
