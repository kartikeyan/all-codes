/*	PRogram 2:
 *
 *	Correct Template for Singly Linked List
 *
 *	Handled all Cases
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void addFirst(){

	demo *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
	}else{
	
		newNode->next = head;
		head = newNode;
	}
}

int countNode();

int addAtPos(int pos){
	
	int count = countNode();

	if(pos <= 0 || pos >= count + 2){
	
		printf("Invalid Position to Insert\n");
		return -1;
	}else{
	
		if(pos == count+1){
		
			addNode();
		}else if(pos == 1){
		
			addFirst();
		}else{
		
			demo *newNode = createNode();

			demo *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next = newNode;
		}
		return 0;
	}
}

int countNode(){

	demo *temp = head;

	int count = 0;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	printf("Count Of Node is :%d\n", count);

	return count;
}

int deleteFirst(){
	
	int count = countNode();

	if(head == NULL){
		
		printf("Nothing to delete\n");
		return -1;

	}else if(count == 1){
	
		free(head);
		head = NULL;
		printf("Last vala Data is Removed\n");
	}else{
		demo *temp = head;

		head = head->next;		//1 st way
		//head = temp->next;		//2 nd way

		free(temp);
	}
}

int deleteLast(){

	demo *temp = head;

	if(head->next == NULL){
	
		free(temp);
		head = NULL;
		printf("Last vala data is removed\n");

	}else if(head->next != NULL){
		while(temp->next->next != NULL){
		
			temp = temp->next;
		}
	
		free(temp->next);
		temp->next = NULL;
	}else{
	
		printf("Nothing to delete\n");
		return -1;
	}
}

int deleteAtPos(int pos){

	int count = countNode();

	if(pos <= 0 || pos > count){
	
		printf("Invalid Position to insert\n");
		return -1;
	}else{
	
		if(pos == 1){
		
			deleteFirst();
		}else if(pos == count){
			deleteLast();
		}else{
		
			demo *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			demo *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}
	}
}

void printLL(){

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Data is :%d|", temp->data);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.countNode\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.deleteAtPos\n");
		printf("8.printLL\n");

		int ch;
		printf("Enter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				{
				int pos;
				printf("Enter Node Position\n");
				scanf("%d", &pos);

				addAtPos(pos);

				}
				break;
			case 4:
				countNode();
				break;
			case 5:
				deleteFirst();
				break;
		
			case 6:
				deleteLast();
				break;
			case 7:
				{
				int pos;
				printf("Enter pos to delete\n");
				scanf("%d", &pos);

				deleteAtPos(pos);
				}
				break;
		
			case 8:
				printLL();
				break;		

			default:
				printf("Enter Correct Choice\n");
		}

		getchar();

		printf("Do u want to continue\n");
		scanf("%c", &choice);
	}while(choice == 'y' || choice == 'Y');
}
