/*	Program 4 :
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Movie{

	int count;
	char mName[20];
	float rating;

	struct Movie *next;
}Mov;

void fillData(Mov *ptr){

	printf("Enter the count : ");
	scanf("%d", &(ptr->count));

	printf("Enter the movie name : ");
	scanf("%s", &(ptr->mName));

	printf("Enter the rating : ");
	scanf("%f", &(ptr->rating));
}

void printData(Mov *ptr){

	printf("%d\n", ptr->count);
	printf("%s\n", ptr->mName);
	printf("%f\n", ptr->rating);
}

void main(){

	Mov *m1 = (Mov*) malloc (sizeof(Mov));

	Mov *m2 = (Mov*) malloc (sizeof(Mov));

	Mov *m3 = (Mov*) malloc (sizeof(Mov));

	for(int i = 1; i <= 3; i++){
	
		printf("Fill the data by m%d\n", i);
		fillData(m1 + i * sizeof(Mov));
		printf("\n");
	}
	
	for(int i = 1; i <= 3; i++){
	
		printf("Print the data by m%d\n", i);
		printData(m1 + i * sizeof(Mov));
		printf("\n");
	}
}
