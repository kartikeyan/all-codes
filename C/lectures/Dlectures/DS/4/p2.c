/*	PRogram 2 :
*/

#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct Employee{

	int empId;
	char empName[20];
	float sal;

	struct Employee *next;
}Emp;

void main(){

	Emp *emp1 = (Emp *) malloc (sizeof(Emp));

	Emp *emp2 = (Emp *) malloc (sizeof(Emp));

	Emp *emp3 = (Emp *) malloc (sizeof(Emp));

	emp1->empId = 1;
	strcpy(emp1->empName, "Kartik");
	emp1->sal = 60.4;

	emp1->next = emp2;

	emp2->empId = 2;
	strcpy(emp2->empName, "Onkar");
	emp2->sal = 45.56;

	emp2->next = emp3;

	emp3->empId = 3;
	strcpy(emp3->empName, "Shree");
	emp3->sal = 89.4;

	emp3->next = NULL;

	//Access
	printf("%d\n", emp1->empId);
	printf("%s\n", emp1->empName);
	printf("%f\n", emp1->sal);
	printf("%p\n", emp1->next);

	printf("%d\n", emp2->empId);
	printf("%s\n", emp2->empName);
	printf("%f\n", emp2->sal);
	printf("%p\n", emp2->next);

	printf("%d\n", emp3->empId);
	printf("%s\n", emp3->empName);
	printf("%f\n", emp3->sal);
	printf("%p\n", emp3->next);
}
