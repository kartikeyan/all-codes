#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Batsman{

	int jerNo;
	char bName[20];
	float avg;

	struct Batsman *next;
}Bat;

void printData(Bat *b1, int count){
	
	while(count--){
	
		printf("%d\n", b1->jerNo);
		printf("%s\n", b1->bName);
		printf("%f\n", b1->avg);

		b1 = b1 + sizeof(Bat);
	}
}

Bat *insertData(Bat *b1, int count){

	int i = 1;

	while(count--){
	
		printf("Enter Data of %d object\n", i);

		printf("Enter Batsaman jersey no\n");
		scanf("%d", &(b1->jerNo));

		printf("Enter Batsman name\n");
		scanf("%s", b1->bName);

		printf("Enter Batsman avg\n");
		scanf("%f", &(b1->avg));

		b1 = b1 + sizeof(Bat);

		i++;
	}
	return b1;
}

void main(){

	Bat *b1 = (Bat*)malloc(sizeof(Bat));

	printf("Enter the no.of object you want to create :\n");

	int objCount;
	scanf("%d", &objCount);
	printf("\n");

	insertData(b1, objCount);

	printData(b1, objCount);
}
