/*	Program 1 :
 *
 *	User Input
 *
 *	Applicable to only one node
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Movie{

	char mName[20];
	float imdb;
	struct Movie *next;
}Mov;

Mov *head = NULL;

void addNode(){

	Mov *newNode = (Mov*)malloc(sizeof(Mov));

	printf("Enter Movie name\n");
	fgets(newNode->mName, 15, stdin);

	printf("Enter Movie rating\n");
	scanf("%f", &newNode->imdb);

	newNode->next = NULL;

	head = newNode;					
}

void printLL(){

	Mov *temp = head;

	while(temp != NULL){
	
		printf("%s", temp->mName);
		printf("%f", temp->imdb);
		printf("\n");

		temp = temp->next;
	}
}

void main(){

	addNode();
	printLL();
}

/*	Ekach Node sathi code barobar ahe,
 *	karan apna navin node kela tar,
 *	addNode madhe head = newNode ahe tychamule
 *	head pahila node la connect hota pan dusra 
 *	node connect kartana head = newNode kela asel,
 *	tar pahila node cha connection head sobat jato 
 *	ani dusra node sobat head cha connection establish 
 *	hoto.
*/	
