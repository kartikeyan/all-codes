/*	Program 2 :
 *
 *	Topic : Implenting Stack Using Linked List
 *
 *	Proper Template
*/

#include<stdio.h>
#include<stdbool.h>
#include<stdlib.h>

struct Node{
	
	int data;
	struct Node *next;
};


int countNodes = 0;
int size = 0;
int flag = 0;

struct Node *head = NULL;


int eleCount(){

	int count = 0;

	struct Node *temp = head;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	return count;
}

bool isFull(){

	if(eleCount() == countNodes){
	
		return true;
	}else{
	
		return false;
	}
}

struct Node *createNode(){

	struct Node *newNode = (struct Node *)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int push(){

	if(isFull()){
	
		return -1;
	}else{
	
		addNode();

		return 0;
	}
}

bool isEmpty(){

	if(head == NULL){
	
		return true;
	}else{
	
		return false;
	}
}

int deleteLast(){

	if(isEmpty()){
	
		flag = 0;

		return -1;

	}else{ 

		int ret;
		
		if(eleCount() == 1){
	
			ret = head->data;
			free(head);
			head = NULL;

		}else{
	
			struct Node *top = head;

			while(top->next->next != NULL){
		
				top = top->next;
			}

			ret = top->next->data;

			free(top->next);
			top->next = NULL;
		}

		return ret;
	}
}

int pop(){

	if(isEmpty()){
		
		flag = 0;
		return -1;
	}else{
	
		int val = deleteLast();
		
		flag = 1;
		return val;
	}
}

int printLL(){

	int ret;

	if(head->next == NULL){
	
		ret = head->data;
	}else{
	
		struct Node *top = head;

		while(top->next != NULL){
			
			top = top->next;
		}

		ret = top->data;
	}

	return ret;
}

int peek(){

	if(isEmpty()){
	
		flag = 0;
		return -1;
	}else{
	
		int val = printLL();
		flag = 1;
		return val;
	}
}

void main(){

	printf("Enter Stack Count Nodes\n");
	scanf("%d", &countNodes);

	int stack[size];

	char choice;

	do{
		
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("4.isFull\n");
		printf("5.isEmpty\n");

		int ch;
		printf("Enter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				{
					int ret;
					ret = push(stack);

					if(ret == -1){
					
						printf("Stack OverFlow\n");
					}
				}
				break;
			case 2:
				{
					int ret;
					ret = pop(stack);

					if(flag == 0){
					
						printf("Stack UnderFlow\n");
					}else{
					
						printf("%d is Popped\n", ret);
					}
				}
				break;
			case 3:
				{
					int ret;
					ret = peek(stack);

					if(flag == 0){
					
						printf("Stack is Empty\n");
					}else{
					
						printf("%d is Peeked\n", ret);
					}
				}
				break;
			case 4:
				{
					bool ret;
					ret = isFull();

					if(ret == true){
					
						printf("Stack is Full\n");
					}else{
					
						printf("Stack is not Full\n");
					}
				}
				break;
			case 5:
				{
					bool ret;
					ret = isEmpty();

					if(ret == true){
					
						printf("Stack is Empty\n");
					}else{
					
						printf("Stack is not Empty\n");
					}
				}
				break;
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
