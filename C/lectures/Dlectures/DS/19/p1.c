/*	Program 1 :
 *
 *	Topic : Implenting Stack Using Array
 *
 *	Proper Template
*/

#include<stdio.h>
#include<stdbool.h>

int top = -1, size = 0, flag = 0;

bool isFull(){

	if(top == size - 1){
	
		return true;
	}else{
		
		return false;
	}
}

int push(int stack[]){

	if(isFull()){
		
		return -1;
	}else{
	
		top++;
		printf("Enter Data\n");
		scanf("%d", &stack[top]);

		return 0;
	}	
}

bool isEmpty(){

	if(top == -1){
		
		return true;
	}else{
	
		return false;
	}
}

int pop(int *stack){

	if(isEmpty()){
		
		flag = 0;

		return -1;
	}else{
	
		int val = stack[top];
		top--;
		flag = 1;

		return val;
	}	
}

int peek(int *stack){

	if(isEmpty()){
	
		flag = 0;
		return -1;
	}else{
	
		int val = stack[top];
		flag = 1;

		return val;
	}
}

void main(){

	printf("Enter Stack Size\n");
	scanf("%d", &size);

	int stack[size];

	char choice;

	do{
		
		printf("1.Push\n");
		printf("2.Pop\n");
		printf("3.Peek\n");
		printf("4.isFull\n");
		printf("5.isEmpty\n");

		int ch;
		printf("Enter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				{
					int ret;
					ret = push(stack);

					if(ret == -1){
					
						printf("Stack OverFlow\n");
					}
				}
				break;
			case 2:
				{
					int ret;
					ret = pop(stack);

					if(flag == 0){
					
						printf("Stack UnderFlow\n");
					}else{
					
						printf("%d is Popped\n", ret);
					}
				}
				break;
			case 3:
				{
					int ret;
					ret = peek(stack);

					if(flag == 0){
					
						printf("Stack is Empty\n");
					}else{
					
						printf("%d is Peeked\n", ret);
					}
				}
				break;
			case 4:
				{
					bool ret;
					ret = isFull();

					if(ret == true){
					
						printf("Stack is Full\n");
					}else{
					
						printf("Stack is not Full\n");
					}
				}
				break;
			case 5:
				{
					bool ret;
					ret = isEmpty();

					if(ret == true){
					
						printf("Stack is Empty\n");
					}else{
					
						printf("Stack is not Empty\n");
					}
				}
				break;
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);
	}while(choice == 'Y' || choice == 'y');
}
