/*	PRogram 6 :
 *
 *	Kiti Stack Push hotat heh nagnyasathi two ways ahet
 *		1] Global Variable Declare
 *		2] Static Storage Class cha vapar karun
*/

#include<stdio.h>

int x = 0;

void fun(){
	
//	static int x = 0;
	printf("%d\n", ++x);
	fun();
}

void main(){

	fun();
}

//	"5,23,742" ala nantar Segmentation Fault yeto

/*	PRogram 5 and 6 varun kalto ki mulat static ani global vegale ahet
 *
 *	Pan static ani global la jaga Data section madhe bhet-to
*/	
