/*	PRogram 15 :
 *
 *	Factorial Recursion
 *	
 *	// Correct Version
*/

#include<stdio.h>

int factorail(int num){

	static int fact = 1;

	fact = fact * num;

	if(num != 1){
	
		factorail(--num);
	}

	return fact;
}

void main(){

	int ans = factorail(5);

	printf("%d\n", ans);
}	


