/*	PRogram 16 :
 *
 *	Factorial Recursion
 *	
 *	// Correct Version
*/

#include<stdio.h>

int factorail(int num){
	
	if(num == 0){
		
		return 1;
	}

	int prevFact = factorail(num-1);
	return num * prevFact;
}

void main(){

	int ans = factorail(5);

	printf("%d\n", ans);
}	


