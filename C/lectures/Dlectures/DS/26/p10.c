/*	PRogram 10 :
 *
 *	Recursion Starts from Here
*/

#include<stdio.h>

int x = 5;

void fun(int x){

	printf("HEllo\n");
	
	if(x != 1){
		fun(--x);
	}

	printf("Bye\n");
}

void main(){
	fun(x);
}

/*	First Iteration la 
 *		a] x = 5
 *		b] 5 != 1 true ahe
 *		c] fun(4) la call gela
 *		d] pan printf line stack frame madhe ajun baki ahe,
 *		   toh paryant fun(4) la call gela
 *
 *	Second Iteration la
 *		a] x = 4
 *		b] 4 != 1 true ahe
 *		c] fun(3) la call gela
 *		d] pan printf line ajun baki ahe
 *
 *	Third Itearation la
 *		Same
 *
 *	Fourth Iteration la
 *		a] x = 2
 *		b] 2 != 1 true ahe
 *		c] fun(1) la call gela
 *		d] ajun printf line baki ahe
 *
 *	Fifth Iteration la
 *		a] x = 1
 *		b] 1 != 1 false honar
 *		c] fun(0) la call nahi janar
 *		d] printf("Bye") printf honar,
 *		   ani fifth iteration vala stack frame pop honar,
 *		   tyala kutun call ala hota tithe jail
 *
 *	Fourth Iteration la 
 *		a] printf line baki hota toh print honar,
 *		   ani kutun call ala hota tithe janar
 *
 *	Third Iteration
 *		a] Same
 *	Second Iteration
 *		a] Same
 *	First Iteration
 *		a] printf line baki hota toh print honar,
 *		   ani kutun call ala hota tithe janar
 *	main pan pop zala
 *
 *	"output" : Hello
 *		   Hello
 *		   Hello
 *		   Hello
 *		   Hello
 *		   Bye
 *		   Bye
 *		   Bye
 *		   Bye
 *		   Bye
*/		   
