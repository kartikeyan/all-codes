/*	PRogram 8 :
 *
 *	Printing Number from 10 to 0 Recursion
*/

#include<stdio.h>

void fun(int x){

	printf("%d\n", x);
	
	if(x != 1){
		fun(--x);
	}
}

void main(){
	fun(10);
}
