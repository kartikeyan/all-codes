/*	PRogram 5 :
 *
 *	Kiti Stack Push hotat heh nagnyasathi two ways ahet
 *		1] Global Variable Declare
 *		2] Static Storage Class cha vapar karun
*/

#include<stdio.h>

void fun(){
	
	static int x = 0;
	printf("%d\n", ++x);
	fun();
}

void main(){

	fun();
}

//	"5,23,801" ala nantar Segmentation Fault yeto
