
/*	Print All SubArrays
*/

#include<stdio.h>

void printSubArray(int arr[], int size){
	
	for(int i = 0; i < size; i++){

		for(int j = i; j < size; j++){
			
			for(int k = i; k <= j; k++){
				
				printf("%d ", arr[k]);
			}

			printf("\n");
		}
	}
}

void main(){
	
	int arr[] = {1,2,3,4,5,6};

	int size = sizeof(arr) / sizeof(arr[0]);

	printSubArray(arr, size);
}
