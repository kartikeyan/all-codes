
/*	Print Sum of Every Single SubArray
*/

#include<stdio.h>

void printSubArray(int arr[], int size){
	
	for(int i = 0; i < size; i++){

		for(int j = i; j < size; j++){

			int sum = 0;
			
			for(int k = i; k <= j; k++){
				
				sum = sum + arr[k];
				
			}

			printf("%d\n", sum);
		}
	}
}

void main(){
	
	int arr[] = {2,4,1,3};

	int size = sizeof(arr) / sizeof(arr[0]);

	printSubArray(arr, size);
}
