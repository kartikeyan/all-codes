
/*	Print Sum of Every Single SubArray using PrefixSum Technique
 *
 *	T.C = 0(N^2), S.C = 0(N)
 *
 *	Karan apan Ajun Ek array vapartoy array cha sum store karayla
*/

#include<stdio.h>

void prefixSumArray(int arr[], int size){
	
	int pSum[size];		// kiti size cha array ahe, tevdach navin array karaycha

	pSum[0] = arr[0];	// Assign kartoy karan apan preFix Sum madhe magcha element cha sum ani current element cha sum add kartoy
				// jar int i = 0 pasun start kela tar segementation fault yeto karan i = -1 array madhe invalid index ahe
	
	for(int i = 1; i < size; i++){
		
		pSum[i] = pSum[i - 1] + arr[i];			// sum insert kela ahe preSum array madhe now, preSum[] = {2,6,7,10} asa disel
	}

	for(int i = 0; i < size; i++){

		for(int j = i; j < size; j++){

			int sum = 0;
			
			if(i == 0){
				
				sum = pSum[j];
			}else{
				
				sum = pSum[j] + pSum[i-1];
			}
				
			printf("%d\n", sum);
		}
	}
}

void main(){
	
	int arr[] = {2,4,1,3};

	int size = sizeof(arr) / sizeof(arr[0]);

	prefixSumArray(arr, size);
}
