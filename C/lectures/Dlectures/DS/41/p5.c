
/*	Print Sum of Every Single SubArray with T.C O(N^2) and Without using extra space complexity
 *
 *	T.C = 0(N^2), S.C = 0(1)
*/

#include<stdio.h>

void prefixSumArray(int arr[], int size){

	for(int i = 0; i < size; i++){
		
		int sum = 0;

		for(int j = i; j < size; j++){

			sum = sum + arr[j];

			printf("%d\n", sum);
		}
	}
}

void main(){
	
	int arr[] = {2,4,1,3};

	int size = sizeof(arr) / sizeof(arr[0]);

	prefixSumArray(arr, size);
}
