/*	PRogram 2 : Pointer
 *
 *	This program is used to show that kutla 
 *	datatype cha address store kelay tya type
 *	cha pointer gyaycha.
*/

#include<stdio.h>

void main(){

	char carr[5] = {'A','B','C','D','E'};

	char *cptr = carr;

	int *iptr = carr;

	printf("%c\n", *cptr);		//A
	printf("%c\n", *iptr);		//A

	cptr++;
	iptr++;

	printf("%c\n", *cptr);		//B
	printf("%c\n", *iptr);		//E
}

/*	warning : initialization of "int *" from incompatible pointer type "char *"
*/	
