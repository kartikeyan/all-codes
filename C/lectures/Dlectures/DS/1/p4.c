/*	Program 4 : Malloc
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct OTT{

	char pName[10];
	int usersCount;
	float prize;
};

void main(){
	
	struct OTT *ptr1 = (struct OTT*) malloc (sizeof(struct OTT));

	strcpy(ptr1->pName, "Prime Video");
	ptr1->usersCount = 10000;
	ptr1->prize = 350.5;

	printf("%s\n", ptr1->pName);
	printf("%d\n", ptr1->usersCount);
	printf("%f\n", ptr1->prize);

	struct OTT *ptr2 = (struct OTT*) malloc (sizeof(struct OTT));

	strcpy(ptr2->pName, "Netflix");
	ptr2->usersCount = 50000;
	ptr2->prize = 505.5;

	printf("%s\n", ptr2->pName);
	printf("%d\n", ptr2->usersCount);
	printf("%f\n", ptr2->prize);
}
