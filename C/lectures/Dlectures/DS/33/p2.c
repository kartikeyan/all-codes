
/*	Prog : Factors Sort
 *	
 *	You are given an array A of N elements
 *	Sort the given array in increasing order of the number of distinct factors of each element
 *	i.e element having the least no. of factors should be first and no. of highest no. of factors should be
 *	last one.
 *	If 2 elements have same number of factors, then the number with less value should be first
 *	
	Input : A = [6,8,9]
*	Ouput : A = [9,6,8]
*/	

#include<stdio.h>
/*
int factors(int arr[], int size){
	
	int count = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] % i == 0){
			
			count++;
		}
	}

	return count;
}

void sort(int arr[] , int size){
	
	for(int i = 0; i < size - 1; i++){
		
		int minIndex = factors(arr, size);

		for(int j = i + 1; j < size; j++){
			
			if(arr[minIndex] > arr[j]){
				
				minIndex = j;
			}
		}

		int temp = arr[i];
		arr[i] = arr[minIndex];
		arr[minIndex] = temp;
	}
}
*/
void main(){
	
	int arr[] = {6,8,9};

	int size = sizeof(arr) / sizeof(arr[0]);
	
	sort(arr, size);

	printf("After\n");
	for(int i = 0; i < size ; i++){
		
		printf("%d\n", arr[i]);
	}
}
