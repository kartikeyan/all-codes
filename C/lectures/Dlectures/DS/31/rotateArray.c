
/*	Program : Find Pivot in an Array
 *
 *	arr[] = {7,8,9,1,2,3};
*/

#include<stdio.h>

int getPivot(int arr[], int size){
	
	int start = 0;
	int end = size - 1;
	int mid;

	while(start < end){
		
		mid = start + (end-start) / 2;

		if(arr[mid] >= arr[0]){
		
			start = mid + 1;
		}else{
		
			end = mid;
		}
	}

	return start; 		// end pan chalel
}

int binarySearch(int arr[] , int st, int en, int search){

	int start = st;
	int end = en , mid;

	while(start <= end){
	
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			return mid;
		}
		if(arr[mid] < search){
			
			start = mid + 1;
		}
		if(arr[mid] > search){
			
			end = mid - 1;
		}
	}

	return -1;
}

int findPosition(int arr[], int size, int search){

	int pivot = getPivot(arr, size);

	if(search >= arr[pivot] && search <= arr[size-1]){
		
		//BS on second half
		return binarySearch(arr, pivot, size-1, search);
	}else{
		
		//BS on first half
		return binarySearch(arr, 0, pivot - 1, search);
	}
}

void main(){
	
	int size;
	printf("Enter size;\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}
	
	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);
	
	int start = 0;
	int end = size;

//	int ret = getPivot(arr, size);
	int ret = binarySearch(arr, start, end, search);
	printf("Index is %d\n", ret);
}
