
#include<stdio.h>

long long int squareRoot(int num){

	int start = 0;
	int end = num;
	long long int mid, ans = -1;

	while(start <= end){
		
		mid = start + (end-start) / 2;

		long long int square = mid * mid;

		if(square == num){
		
			return mid;
		}
		if(square < num){
			
			ans = mid;
			start = mid + 1;
		}else{
			end = mid - 1;
		}
	}

	return ans;
}

void main(){
	
	int num = 26;

	int ret = squareRoot(num);
	printf("%d\n", ret);
}
