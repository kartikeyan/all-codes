
/*	PRogram 1 : 
 *	
 *	arr[] = {2,4,5,7,11,17,21};
 *
 *	search = 9,  ceiling => smallest num >= search = 11
*/

#include<stdio.h>

int ceilingLinear(int arr[], int size, int search){
	
	int num = -1;

	for(int i = 0; i < size; i++){
	
		if(arr[i] >= search){
			
			num = arr[i];
			break;
		}
	}

	return num;	
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret = ceilingLinear(arr, size, search);
	printf("Number is : %d\n", ret);
}
