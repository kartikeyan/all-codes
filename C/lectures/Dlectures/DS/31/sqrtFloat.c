
#include<stdio.h>

long long int squareRoot(int num){

	int start = 0;
	int end = num;
	long long int mid, ans = -1;

	while(start <= end){
		
		mid = start + (end-start) / 2;

		long long int square = mid * mid;

		if(square == num){
		
			return mid;
		}
		if(square < num){
			
			ans = mid;
			start = mid + 1;
		}else{
			end = mid - 1;
		}
	}

	return ans;
}

double morePrecision(int num, int precision , int tempSol){

	double factor = 1;
	double ans = tempSol;

	for(int i = 0; i < precision; i++){
		
		factor = factor / 10;

		for(double j = ans ; j*j < num; j = j + factor){
			
			ans = j;
		}
	}

	return ans;
}

void main(){
	
	int num = 29;
	
	int tempSol = squareRoot(num);

	int ret = morePrecision(num, 3, tempSol);
	printf("%d\n", ret);
}
