
/*	Program : Find Pivot in an Array
 *
 *	arr[] = {7,8,9,1,2,3};
*/

#include<stdio.h>

int getPivot(int arr[], int size){
	
	int start = 0;
	int end = size - 1;
	int mid;

	while(start < end){
		
		mid = start + (end-start) / 2;

		if(arr[mid] >= arr[0]){
		
			start = mid + 1;
		}else{
		
			end = mid;
		}
	}

	return start; 		// end pan chalel
}

void main(){
	
	int size;
	printf("Enter size;\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int ret = getPivot(arr, size);
	printf("Pivot Index is %d\n", ret);
}
