
/*	PRogram 5 : 
 *	
 *	arr[] = {2,3,3,4,4,4,4,5,6,8,11,18};
 *
 *	search = 4, o:p = 3
*/

#include<stdio.h>

int floorBinary(int arr[], int size, int search){
	
	int start = 0, end = size-1, store = -1, mid;

	while(start <= end){
	
		mid = start + (end - start) / 2;

		if(arr[mid] == search){
			
			store = mid;
			if(arr[mid - 1]  != search){
				
				return store;
			}

			end = mid - 1;

		}else if(arr[mid] > search){
			
			store = arr[mid];
			end = mid - 1;
		}else{
			
//			store = arr[mid];
			start = mid + 1;
		}
	}

	return store;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int search;
	printf("Enter Search Ele\n");
	scanf("%d", &search);

	int ret = floorBinary(arr, size, search);
	printf("Number is : %d\n", ret);
}
