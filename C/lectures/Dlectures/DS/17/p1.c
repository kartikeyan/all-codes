/*	PRogram 1 :
 *
 *	Topic : Stack
 *
 *	Stack is just a Perspective, i.e bagnaycha drushtikone vegala ahe
 *
*/

/*	Implementation of Stack Using Array
 *
 *	3 Operations : 1]push 2]pop 3]peek
*/

#include<stdio.h>

int top = -1;

int arr[5];

void push(int data){

	top++;
	arr[top] = data;
}

void main(){

	push(10);
	push(20);
	push(30);
	push(40);
	push(50);

	for(int i = top; i >= 0; i--){
	
		printf("|%d|", arr[i]);
	}

	printf("\n");
}
