/*	Program 2 :
 *
 *	Getting ready to include function to print the data
 *	for that this is prerequisite
*/

#include<stdio.h>

void fun(int *a){

	*a = *a + 10;

	printf("%d\n", *a);
}

void main(){

	int x = 10;

	fun(&x);

	printf("%d\n", x);
}


/*	1 - fun(x) asel tar parameter asel (datatype)
 *	2 - fun(&x) asel tar parameter asel (pointer) i.e (datatype pointer)
 *	3 - fun(*x) asel tar parameter asel (pointer) i.e (datatype pointer)
 *	4 - int x = 10;
 *	    int *ptr1 = &x;
 *	    int *ptr2 = ptr1;
 *
 *	    fun(ptr2) asel tar parameter asel (double pointer) i.e (datatype double pointer)
*/	
