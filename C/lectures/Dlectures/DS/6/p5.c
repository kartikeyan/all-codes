/*	Program 5 :
 *
 *	//Approach
 *
 *	//veritas qquestion
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Student{

	int id;
	float ht;
	struct Student *next;
}Stud;

Stud *addNodes(Stud *head){

	Stud *newNode = (Stud*)malloc(sizeof(Stud));

	newNode->id = 1;
	newNode->ht = 5.5;
	newNode->next = NULL;

	head = newNode;

	return head;
}

void printLL(Stud *head){
	
	Stud *temp = head;

	while(temp != NULL){

		printf("%d\n", temp->id);
		printf("%f\n", temp->ht);

		temp = temp->next;
	}
}

void main(){

	Stud *head = NULL;

	head = addNodes(head);
	printLL(head);
}
