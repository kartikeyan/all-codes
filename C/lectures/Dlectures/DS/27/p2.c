/*	
 *	Recursion : Problem Statement la sub-problem madhe divide karner
 *		    ani tya problem che task achieve karne yala Recursion
 *		    ase mhntat
 *
 *	Types of Recursion:
 *		1] Direct Recursion
 *		2] InDirect Recursion
 *		3] Tail Recursion
 *		4] Non-Tail Recursion
*/

/*	1] Direct Recursion : Ekada function la tyach function chya madhe direct call
 *		              karne mhnje Direct Recusrion

 	Pseudo Code	

	void fun(){

		fun();
	}
*/


/*	2] Indirect Recursion : Ekada function madhe dusrya function la call karne 
 *		                ani dusrya function madhun pahila function la parat yene
 *		                mhnje ithe loop tyayar zala yala mhntat Indirect Recursion
 *
 *	void fun1(){
 *
 *		fun2();
 *	}
 *
 *	void fun2(){
 *	
 *		fun1();
 *	}
*/
