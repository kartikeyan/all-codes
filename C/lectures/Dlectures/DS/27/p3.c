
/*	Tail Recursion
*/

#include<stdio.h>

int tailDemo(int x){

	if(x == 1){
		
		return 1;
	}

	return tailDemo(--x);		// ithe function chya last madhe parat function la call ahe kutle arithemtic calculation kiva printf statements nahit mahnun tail recursion
}

void main(){

	int ret = tailDemo(4);
	printf("%d\n", ret);
}
