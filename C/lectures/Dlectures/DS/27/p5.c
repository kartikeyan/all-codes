/*	PRogram 5 :
 *
 *	Best Way to write Recursion:
 *		1] Base Condition ( To Stop Recursion)
 *		2] Main Logic ( Divide into Sub-problem)
*/

// 	Factorial

//	1 - Normal Way
/*
#include<stdio.h>

void factorial(int x){

	int fact = 1;

	for(int i = 1; i <= x;i++){
	
		fact = fact * i;
	}

	printf("%d\n", fact);
}

void main(){

	factorial(5);
}
*/

//	2 - Bad way recursion
/*
#include<stdio.h>

int factorial(int n){
	
	static int fact = 1;

	if(n == 1){
	
		return fact;
	}

	fact = fact * n;
	return factorial(--n);
}

void main(){

	int ret = factorial(5);
	printf("%d\n", ret);
}
*/

//	3 - Best Way by recursion
/*
#include<stdio.h>

int factorial(int n){

	if(n <= 1){
	
		return 1;
	}

	return factorial(n-1) * n;
}

void main(){

	int ret = factorial(5);
	printf("%d\n", ret);
}
*/

//	4 - Apni Kaksha
/*
#include<stdio.h>

int factorial(int n){

	if(n <= 1){
	
		return 1;
	}
	
	int prevFact = factorial(n-1);
	return n * prevFact;
}

void main(){

	int ret = factorial(5);
	printf("%d\n", ret);
}
*/
