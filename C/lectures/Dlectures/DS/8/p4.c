/*	Program 3 :
 *
 *	5] addAtPos()
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Employee{

	char name[20];
	int id;

	struct Employee *next;
}Emp;

Emp *head = NULL;

Emp *createNode(){

	Emp *newNode = (Emp*)malloc(sizeof(Emp));
	
	getchar();					

	printf("Enter Employee Name\n");

	char ch;
	int i = 0;

	while((ch = getchar()) != '\n'){		
							
		(*newNode).name[i] = ch;		
		i++;					
	}						

	printf("Enter Employee Id\n");
	scanf("%d", &newNode->id);

	newNode->next = NULL;

	return newNode;				
}							

void addNode(){

	Emp *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{	
		Emp *temp = head;

		while(temp->next != NULL){
			
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

void CountNode(){

	Emp *temp = head;

	int count = 0;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	printf("Count is : %d\n", count);
}

void addFirst(){

	Emp *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
	}else{
		
		newNode->next = head;
		head = newNode;
	}
}

void addAtPos(int pos){

	Emp *newNode = createNode();

	Emp *temp = head;

	while(pos - 2){
	
		temp = temp->next;
		pos--;
	}

	newNode->next = temp->next;
	temp->next = newNode;
}

void printLL(){

	Emp *temp = head;

	while(temp != NULL){
	
		printf("|Employee Name is : %s ->", temp->name);
		printf("Employee id is : %d |", temp->id);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	int nodeCount;
	printf("Enter Node Count :\n");
	scanf("%d", &nodeCount);

	for(int i = 0; i < nodeCount; i++){
	
		addNode();
	}

	printLL();

	CountNode();

	addFirst();
	

	int pos;
	printf("Enter Position to Enter Node\n");
	scanf("%d", &pos);

	addAtPos(pos);

	printLL();
}
