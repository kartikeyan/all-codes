/*	PRogram 2 :
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct Project{
	
	char pName[20];
	int EmpCount;
	float profit;
};

void main(){

	struct Project *pptr = (struct Project*)malloc(sizeof(struct Project));

	strcpy(pptr -> pName, "ML");

	pptr->EmpCount = 70;

	pptr->profit = 15.5;

	printf("pName = %s\n", pptr->pName);
	printf("EmpCount = %d\n", (*pptr).EmpCount);
	printf("profit = %f\n", pptr->profit);
}

/*	malloc ni heap section var memory bhet-to
 *	pan heap varch ka karan ki user la
 *	kiti memory pahije aplyala mahit nasto, toh 
 *	aplyala runtime la kalnar ki kiti memory
 *	pahije tyamule apan heap var memory magto.
 *	Heap cha size vary hou shakto pan tyacha 
 *	pan ek limit ahe.
*/	
