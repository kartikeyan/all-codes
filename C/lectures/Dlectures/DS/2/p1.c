/*	PRogram 1 :
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct Company{
	
	char cName[20];
	int EmpCount;
	float revenue;
};

void main(){

	struct Company *cptr = (struct Company*)malloc(sizeof(struct Company));

	strcpy(cptr -> cName, "Veritas");

	cptr->EmpCount = 700;

	cptr->revenue = 150.5;

	printf("cName = %s\n", cptr->cName);
	printf("EmpCount = %d\n", (*cptr).EmpCount);
	printf("revenue = %f\n", cptr->revenue);
}

/*	malloc ni heap section var memory bhet-to
 *	pan heap varch ka karan ki user la
 *	kiti memory pahije aplyala mahit nasto, toh 
 *	aplyala runtime la kalnar ki kiti memory
 *	pahije tyamule apan heap var memory magto.
 *	Heap cha size vary hou shakto pan tyacha 
 *	pan ek limit ahe.
*/	
