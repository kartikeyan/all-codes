/*	PRogram 2 :
*/

#include<stdio.h>
#include<string.h>
#include<stdlib.h>

struct Cricket{
	
	char Tname[20];
	int teams;
	float prize;
};

void main(){

	struct Cricket *cptr = (struct Cricket*)malloc(sizeof(struct Cricket));

	strcpy(cptr -> Tname, "IPL");

	cptr->teams = 10;

	cptr->prize = 15.5;

	printf("TName = %s\n", cptr->Tname);
	printf("teams = %d\n", (*cptr).teams);
	printf("prize = %f\n", cptr->prize);
}

/*	malloc ni heap section var memory bhet-to
 *	pan heap varch ka karan ki user la
 *	kiti memory pahije aplyala mahit nasto, toh 
 *	aplyala runtime la kalnar ki kiti memory
 *	pahije tyamule apan heap var memory magto.
 *	Heap cha size vary hou shakto pan tyacha 
 *	pan ek limit ahe.
*/	
