
/*	Program 2 : Selection Sort
 *
 *	In Selection Sort, pratyak iteration la "minimum element" first la thevto.
 *
 *	In Insertion Sort apan "first index" var kaam karto i.e  first index madhe minimum value asto.
*/

#include<stdio.h>

void selectionSort(int arr[], int size){
	
	for(int i = 0; i < size - 1; i++){
		
		int minIndex = i;

		for(int j = i+1 ; j < size; j++){
			
			if(arr[minIndex] > arr[j]){
				
				minIndex = j;
			}
		}

		int temp = arr[i];
		arr[i] = arr[minIndex];
		arr[minIndex] = temp;
	}
}

void main(){
	
	int size;
	printf("Enter Array size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	printf("Array Elements\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}

	selectionSort(arr, size);

	printf("Array Elements after Sort\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
