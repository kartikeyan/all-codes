
/*	Program 1 : Bubble Sort
 *
 *	In Buuble Sort, pratyak iteration la "maximum element" last la thevto.
 *
 *	In Buuble Sort apan "last index" var kaam karto i.e  last index madhe maximum value aste.
 *
 *	If array Size if 6, then in 5 iteration we get sorted array.
 *
 *	I.E "n - 1" before we get sorted array.
*/

#include<stdio.h>

void bubbleSort(int arr[], int size){
	
	for(int i = 0; i < size; i++){
		
		for(int j = 0; j < size - i - 1; j++){
			
			if(arr[j] > arr[j+1]){
				
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
}

void main(){
	
	int size;
	printf("Enter Array size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	printf("Array Elements\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}

	bubbleSort(arr, size);

	printf("Array Elements after Sort\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
