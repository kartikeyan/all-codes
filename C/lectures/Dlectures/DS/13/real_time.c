/*
 *	real time 
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct StartUp{
	
	char sname[20];
	int emp;
	float rev;
	struct StartUp *next;
}st;

st *head = NULL;

st *createNode(){

	st *newNode = (st*)malloc(sizeof(st));

	printf("Enter StartUp Name\n");
	
	getchar();

	int ch;
	int i = 0;

	while((ch = getchar()) != '\n'){
	
		(*newNode).sname[i] = ch;
		i++;
	}

	printf("Enter StartUp Employees\n");
	scanf("%d", &newNode->emp);

	printf("Enter StartUp Revenue\n");
	scanf("%f", &newNode->rev);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	st *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
		newNode->next = head;
	}else{
	
		st *temp = head;

		while(temp->next != head){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->next = head;
	}
}

void addFirst(){

	st *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
		newNode->next = head;
	}else{
	
		st *temp = head;

		while(temp->next != head){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->next = head;
		head = newNode;
	}
}

void addLast(){

	addNode();
}

int countNodes(){

	st *temp = head;
	
	int count = 0;

	do{
		temp = temp->next;
		count++;

	}while(temp != head);

//	printf("Count is : %d\n", count);

	return count;
}


int addAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos >= count + 2){
	
		printf("Invalid pos to insert\n");
		return -1;
	}else{
	
		if(pos == 1){
			
			addFirst();
		}else if(pos == count + 1){
		
			addLast();
		}else{
		
			st *newNode = createNode();

			st *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			temp->next = newNode;
		}

		return 0;
	}
}


int deleteFirst(){

	int count = countNodes();

	if(head == NULL){
		
		printf("Nothing to delete\n");
		return -1;
	}else{ 
		if(count == 1){
	
			free(head);
			head = NULL;
		}else{

			st *temp = head;
			
			while(temp->next != head){
				
				temp = temp->next;
			}

			head = head->next;

			free(temp->next);

			temp->next = head;
		}
		return 0;
	}
}


int deleteLast(){

	int count = countNodes();

	if(head == NULL){
	
		printf("Nothing to delete\n");
		return -1;
	}else{
	
		if(count == 1){
		
			free(head);
			head == NULL;
		}else{
		
			st *temp = head;

			while(temp->next->next != head){
			
				temp = temp->next;
			}
			
			free(temp->next);
			temp->next = head;
		}

		return 0;
	}
}

int deleteAtPos(int pos){

	int count = countNodes();

	if(pos <= 0 || pos > count){
	
		printf("INvalid Pos\n");
		return -1;
	}else{
	
		if(pos == 1){
		
			deleteFirst();
		}else if(pos == count){
		
			deleteLast();
		}else{
		
			st *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			st *temp1 = temp->next;
			temp->next = temp->next->next;
			free(temp1);
		}

		return 0;
	}
}

int printLL(){
	
	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}

	st *temp = head;
	
	while(temp->next != head){
	
		printf("|%s|->", temp->sname);
		printf("|%d|->", temp->emp);
		printf("|%f|->", temp->rev);

		temp = temp->next;
	}

	printf("|%s|", temp->sname);
	printf("|%d|", temp->emp);
	printf("|%f|", temp->rev);
}

void main(){

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addlast\n");
		printf("4.addAtPos\n");
		printf("5.deleteFirst\n");
		printf("6.deleteLast\n");
		printf("7.countNodes\n");
		printf("8.deleteAtPos\n");
		printf("9.printLL\n");
		
		int ch;
		printf("Enter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				addLast();
				break;
			case 4:
				{
				int pos;
				printf("Enter Pos to insert\n");
				scanf("%d", &pos);

				addAtPos(pos);
				}

				break;
			case 5:
				deleteFirst();
				break;	
			case 6:
				deleteLast();
				break;
			case 7:
				countNodes();
				break;
			case 8:
				{
				int pos;
				printf("Enter Pos to insert\n");
				scanf("%d", &pos);

				deleteAtPos(pos);
				}
				break;
				
			case 9:
				printLL();
				break;
				
			default:
				printf("Enter Correct Pos to insert\n");
				
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);
	
	}while(choice == 'Y' || choice == 'y');
}
