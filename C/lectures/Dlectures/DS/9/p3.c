/*	Program 3 :
 *
 *	6] deleteFirst()
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	int data;
	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int countNode();

void addFirst(){

	demo *newNode = createNode();

	if(head == NULL){			//jar ek pan node present nasel tar head = newNode assign kar
		head = newNode;
	}else{
	
		newNode->next = head;		//jar ek pesksha jast node asel tar newNode cha next madhe head kuthe bagtoy
		head = newNode;			//tihte bag ani head = newNode assign kar
	}
}

void addAtPos(int pos){

	demo *newNode = createNode();
	
	demo *temp = head;	

	int size = countNode();

	if(pos > 1 && pos < size){
		while(pos - 2){
	
			temp = temp->next;
			pos--;
		}

	newNode->next = temp->next;
	temp->next = newNode;
	}else{
	
		printf("Enter Correct Position\n");
	}
}

int countNode(){

	demo *temp = head;

	int count = 0;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	printf("Count Of Node is :%d\n", count);

	return count;
}

void deleteFirst(){

	demo *temp = head;

	head = temp->next;   	//head = head->next;		

	free(temp);
}

void printLL(){

	demo *temp = head;

	while(temp != NULL){
	
		printf("|Data is : %d|", temp->data);

		temp = temp->next;
	}
	printf("\n");
}

void main(){

	char choice;

	do{
	
		printf("1.addNode\n");
		printf("2.addFirst\n");
		printf("3.addAtPos\n");
		printf("4.countNode\n");
		printf("5.deleteFirst\n");
		printf("6.printLL\n");

		int ch;
		printf("Enter Choice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1 :
				addNode();
				break;
			case 2:
				addFirst();
				break;
			case 3:
				{
				int pos;
				printf("Enter Node Pos :\n");
				scanf("%d", &pos);

				addAtPos(pos);

				}
				break;

			case 4 :
				countNode();
				break;
			case 5 :
				deleteFirst();
				break;
			case 6 :
				printLL();
				break;
			default:
				printf("Enter Correct Choice :\n");
		}

		getchar();

		printf("Do u want to Continue\n");
		scanf("%c", &choice);

	}while(choice == 'y' || choice == 'Y');
}
