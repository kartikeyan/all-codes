/*	
 *	PRogram 5 : 	Searching :-
 *
 *				1] Linear Search
 *				2] Sorted Search
 *					a] Binary Search
 *					b] InterPolation Search
*/

//	InterPolation Search is used only when array is uniform

#include<stdio.h>

int InterPolationSearch(int arr[], int size, int search){
	
	int start = 0;
	int end = size - 1; 

	while(start <= end && search >= arr[start] && search <= arr[end]){
	
		int mid = start + (( search - arr[start]) * (end - start)) / (arr[end] - arr[start]);

		if(arr[mid] == search){
		
			return mid;
		}

		if(arr[mid] < search){
			
			start = mid + 1;
		}

		if(arr[mid] > search){
		
			end = mid - 1;
		}
	}

	return -1;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];
	
	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int search;
	printf("Enter Search\n");
	scanf("%d", &search);

	int ret = InterPolationSearch(arr, size, search);
	printf("Index is : %d\n", ret);
}
