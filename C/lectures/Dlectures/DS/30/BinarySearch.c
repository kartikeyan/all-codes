/*	
 *	PRogram 4 : 	Searching :-
 *
 *				1] Linear Search
 *				2] Sorted Search
 *					a] Binary Search
 *					b] InterPolation Search
*/

#include<stdio.h>

int BinarySearch(int arr[], int size, int search){
	
	int start = 0;
	int end = size - 1, mid;

	while(start <= end){
	
		mid = (start + end) / 2;

		if(arr[mid] == search){
		
			return mid;
		}

		if(arr[mid] < search){
			
			start = mid + 1;
		}

		if(arr[mid] > search){
		
			end = mid - 1;
		}
	}

	return -1;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];
	
	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int search;
	printf("Enter Search\n");
	scanf("%d", &search);

	int ret = BinarySearch(arr, size, search);
	printf("Index is : %d\n", ret);
}
