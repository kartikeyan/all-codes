/*	
 *	PRogram 3 : 	Searching :-
 *
 *				1] Linear Search
 *				2] Sorted Search
 *					a] Binary Search
 *					b] InterPolation Search
*/

#include<stdio.h>

int linearSearch(int arr[], int size, int search){
	
	int lastOcc, secondLastOcc = -1;

	for(int i = 0; i < size; i++){
		
		if(arr[i] == search){
			
			secondLastOcc = lastOcc;
			lastOcc = i;
		}
	}

	return secondLastOcc;
}

void main(){

	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];
	
	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int search;
	printf("Enter Search\n");
	scanf("%d", &search);

	int ret = linearSearch(arr, size, search);
	printf("Index is : %d\n", ret);
}
