
/*	Program 1 : Sum of 2 Array
 *
 *	Input : nums1 = [8,5,3,7],  nums2 = [2,10,4,6]
 *
 *	Ouput : nums3 = [10,15,7,13]
*/

#include<stdio.h>


void main(){

	int arr1[4] = {8,5,3,7};

	int arr2[4] = {2,10,4,6};

	int arr3[4];
	
	int size = 4;

	for(int i = 0; i < 4; i++){
		
		arr3[i] = arr1[i] + arr2[i];
	}

	for(int i = 0; i < 4; i++){
		
		printf("%d\n", arr3[i]);
	}	
}
