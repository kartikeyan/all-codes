
/*	Program 1 : Hoare's Approach
 *
 *	First Index is Pivot.
 *
 *	Here we take into consideration of pivot, we dont skip pivot
*/

#include<stdio.h>

int partition(int arr[], int start, int end){
	
	int i = start - 1;
	int j = end + 1;

	int pivot = arr[start];

	while(1){
		
		// find leftmost element greater or equal to

		do{				// pivot madhe maximum element bhetla pahije
			i++;

		}while(arr[i] < pivot);


		// find rightmost element smaller than or equal to

		do{
			j--;			// pivot madhe minimum element bhetla pahije

		}while(arr[j] > pivot);


		if(i >= j){
			
			return j;
		}

		int temp = arr[i];
		arr[i] = arr[j];
		arr[j] = temp;
	}
}

void hoareSort(int arr[], int start, int end){
	
	if(start < end){
		
		int pivot = partition(arr, start, end);

		hoareSort(arr, start, pivot);
		hoareSort(arr, pivot+1, end);
	}
}

void main(){
	
	int arr[] = {4,2,7,9,5,3,1};

	int size = sizeof(arr) / sizeof(arr[0]);

	hoareSort(arr, 0 ,size-1);

	printf("Sorted Array\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
