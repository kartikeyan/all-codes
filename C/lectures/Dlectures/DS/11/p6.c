/*	PRogram 6 :
 *	
 *	8]deleteLast
*/

#include<stdio.h>
#include<stdlib.h>

typedef struct Demo{

	struct Demo *prev;

	int data;

	struct Demo *next;
}demo;

demo *head = NULL;

demo *createNode(){

	demo *newNode = (demo*)malloc(sizeof(demo));

	newNode->prev = NULL;

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	demo *newNode = createNode();

	if(head == NULL){
		head = newNode;
	}else{
	
		demo *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;

		newNode->prev = temp;
	}
}

void addFirst(){

	demo *newNode = createNode();

	if(head == NULL){
		
		head = newNode;
	}else{
	
		newNode->next = head;
		head->prev = newNode;
		head = newNode;
	}	
}

int countNodes();

int addAtPos(int pos){

	int count = countNodes();	

	if(pos <= 0 || pos >= count + 2){
	
		printf("Invalid Position to insert\n");
		return -1;
	}else{
	
		if(pos == 1){
			
			addFirst();
		}else if(pos == count + 1){
		
			addNode();
		}else{
		
			demo *newNode = createNode();

			demo *temp = head;

			while(pos - 2){
			
				temp = temp->next;
				pos--;
			}

			newNode->next = temp->next;
			newNode->prev = temp;
			temp->next->prev = newNode;
			temp->next = newNode;
		}
	}
}

void deleteFirst(){

	int count = countNodes();

	if(head == NULL){
	
		printf("Nothing to delete\n");

	}else if(count == 1){
	
		free(head);
		head = NULL;
	}else{
	
		head = head->next;
		free(head->prev);
		head->prev = NULL;
	}
}

void deleteLast(){

	int count = countNodes();

	if(head == NULL){
	
		printf("nothing to delete\n");

	}else if(count == 1){
	
		free(head);
		head = NULL;
	}else{
	
		demo *temp = head;

		while(temp->next->next != NULL){
		
			temp = temp->next;
		}

		free(temp->next);
		temp->next = NULL;
	}
}

int countNodes(){

	demo *temp = head;
	
	int count = 0;

	while(temp != NULL){
		
		count++;
		temp = temp->next;
	}

	printf("Count of Nodes is :%d\n", count);

	return count;
}

void printLL(){

	demo *temp = head;

	while(temp->next != NULL){
	
		printf("|%d|->", temp->data);

		temp = temp->next;
	}

	printf("|%d|\n", temp->data);
}

void main(){

	int nodeCount;
	printf("Enter Number of Nodes\n");
	scanf("%d", &nodeCount);

	for(int i = 0; i < nodeCount; i++){
	
		addNode();
	}

	printLL();

	countNodes();

	addFirst();

	printLL();

	int pos;
	printf("Enter Pos\n");
	scanf("%d", &pos);

	addAtPos(pos);

	printLL();

	deleteFirst();

	printLL();

	deleteLast();

	printLL();
}
