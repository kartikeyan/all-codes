#include<stdio.h>
#include<string.h>
#include<stdlib.h>

typedef struct Company{
	
	char Cname[20];
	int noCom;
	float rev;

	struct Company *next;
}Com;

void main(){

	Com obj1, obj2, obj3;

	Com *head = &obj1;

	strcpy(head->Cname, "C2w");
	head->noCom = 1;
	head->rev = 50.55;

	printf("%s\n", head->Cname);
	printf("%d\n", head->noCom);
	printf("%f\n", head->rev);

	head->next = &obj2;

	strcpy(head->next->Cname, "Biencaps");
	head->next->noCom = 2;
	head->next->rev = 87;

	printf("%s\n", head->next->Cname);
	printf("%d\n", head->next->noCom);
	printf("%f\n", head->next->rev);

	head->next->next = &obj3;

	strcpy(head->next->next->Cname, "Incubator");
	head->next->next->noCom = 3;
	head->next->next->rev = 10;

	printf("%s\n", head->next->next->Cname);
	printf("%d\n", head->next->next->noCom);
	printf("%f\n", head->next->next->rev);

	head->next->next->next = NULL;	
}
