/*	Program 1 :
 *
 *	Topic : Self Referential Pointer
 *
 *		Asa structure jo swatah sarkya structure cha 
 *		address store karto tyala mhantat Self Referential Structure
*/

#include<stdio.h>
#include<string.h>

typedef struct Employee{

	int empID;
	char empName[20];
	float sal;

	struct Employee *next;
}Emp;

void main(){
	
	Emp obj1, obj2, obj3;		// or struct Employee obj1, obj2, obj3;

	obj1.empID = 1;
	strcpy(obj1.empName, "Kanha");
	obj1.sal = 50.50;

	obj1.next = &obj2;

	obj2.empID = 2;
	strcpy(obj2.empName, "Rahul");
	obj2.sal = 60.60;

	obj2.next = &obj3;

	obj3.empID = 3;
	strcpy(obj3.empName, "Ashish");
	obj3.sal = 70.70;

	obj3.next = NULL;

	Emp *head = &obj1;	// or struct Employee = &obj1;

	printf("%d\n", head->empID);
	printf("%s\n", head->empName);
	printf("%f\n", head->sal);

	printf("%d\n", obj1.next->empID);
	printf("%s\n", obj1.next->empName);
	printf("%f\n", obj1.next->sal);

	printf("%d\n", obj2.next->empID);
	printf("%s\n", obj2.next->empName);
	printf("%f\n", obj2.next->sal);
}


/*	Why we are accessing data through pointers
 *	because apan pude malloc ni memory magtoy
 *	ani toh asel heap section madhe, heap
 *	section madhe naav deta yet nahi, heap madhe
 *	apan data access karu shakto only through
 *	pointers.
*/
