/*	PRogram 4 :
*/

#include<stdio.h>
#include<string.h>

struct Flat{
	
	int noOfPersons;
	char name[20];
	float rent;	

	struct Flat *next;
};

void main(){

	struct Flat obj1, obj2, obj3;

	struct Flat *head = &obj1;

	head->noOfPersons = 1;
	strcpy(head->name, "Kartik");
	head->rent = 2750.50;

	printf("%d\n", head->noOfPersons);
	printf("%s\n", head->name);
	printf("%f\n", head->rent);

	head->next = &obj2;

	head->next->noOfPersons = 2;
	strcpy(head->next->name, "Shree");
	head->next->rent = 1750.5;

	printf("%d\n", head->next->noOfPersons);
	printf("%s\n", head->next->name);
	printf("%f\n", head->next->rent);

	head->next->next = &obj3;

	head->next->next->noOfPersons = 3;
	strcpy(head->next->next->name, "Mayur");
	head->next->next->rent = 2350.5;

	printf("%d\n", head->next->next->noOfPersons);
	printf("%s\n", head->next->next->name);
	printf("%f\n", head->next->next->rent);

	head->next->next->next = NULL;
}
