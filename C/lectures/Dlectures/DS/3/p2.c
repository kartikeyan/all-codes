/*	Program 2 :
 *
 *	Topic : Self Referential Pointer
 *
 *		Asa structure jo swatah sarkya structure cha 
 *		address store karto tyala mhantat Self Referential Structure
*/

#include<stdio.h>
#include<string.h>

typedef struct Batsman{

	int jerNo;
	char name[20];
	float avg;

	struct Batsman *next;
}Bat;

void main(){
	
	Bat obj1, obj2, obj3;		// or struct Batsman obj1, obj2, obj3;

	Bat *head = &obj1;
	
	head->jerNo = 45;
	strcpy(head->name, "Rohit");
	head->avg = 55.5;

	printf("%d\n", head->jerNo);
	printf("%s\n", head->name);
	printf("%f\n", head->avg);
	
	head->next = &obj2;

	head->next->jerNo = 18;
	strcpy(head->next->name, "Virat");
	head->next->avg = 58.4;

	printf("%d\n", head->next->jerNo);
	printf("%s\n", head->next->name);
	printf("%f\n", head->next->avg);
	
	head->next->next = &obj3;

	head->next->next->jerNo = 10;
	strcpy(head->next->next->name, "MS DHONI");
	head->next->next->avg = 69.5;

	printf("%d\n", head->next->next->jerNo);
	printf("%s\n", head->next->next->name);
	printf("%f\n", head->next->next->avg);

	head->next->next->next = NULL;
}


/*	Why we are accessing data through pointers
 *	because apan pude malloc ni memory magtoy
 *	ani toh asel heap section madhe, heap
 *	section madhe naav deta yet nahi, heap madhe
 *	apan data access karu shakto only through
 *	pointers.
*/
