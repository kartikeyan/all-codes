/*	
 *	1 - array cha nav(call) kela tar pahila element cha address deto
*/

// 1

/*
#include<stdio.h>
void main(){

	int iarr[5] = {10,20,30,40,50};

	printf("%d\n", iarr);
	printf("%p\n", iarr);
}
*/

//2

/*
#include<stdio.h>
void main(){

	int iarr[5] = {10,20,30,40,50};

	int x = 10;
	char ch = 'A';

	printf("%d\n", x);
	printf("%c\n", ch);

	printf("%p\n", &x);
	printf("%p\n", &ch);

	printf("%p\n", iarr);
	printf("%p\n", &iarr);
}
*/

//3
#include<stdio.h>
void main(){
	
	char carr[4];

	carr[0] = 'A';
	carr[1] = 'B';
	carr[2] = 'C';
	carr[3] = 'D';

	printf("%c\n", carr[0]);
	printf("%c\n", carr[1]);
	printf("%c\n", carr[2]);	
	printf("%c\n", carr[3]);

}
