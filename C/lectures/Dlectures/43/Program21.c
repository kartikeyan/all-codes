/*	Program 2 : WAP TO Take one value from  user
 *		    in another function and print that value
 *		    in Main function
*/

#include<stdio.h>

int fun(){
	
	int a;
	printf("Enter value of a:\n");
	scanf("%d",&a);

	return a;
}

void main(){
	
	int a;
	a = fun();
	printf("%d\n", a);
}
