/*	Program 3 : WAP to take an array from the user in main function
 *		    and print that array in another function.take array 
 *		    size from the user.
 *
*/

#include<stdio.h>

int printArr(int *ptr, int size){

	for(int i = 0; i < size; i++){
	
		printf("%d\n", *(ptr + i));
	}
}

void main(){

	int size;
	printf("Enter size of an array :\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter array elements :\n");

	for(int i=0; i < size; i++){
		scanf("%d", &arr[i]);
	}
	printArr(arr, size);
}
