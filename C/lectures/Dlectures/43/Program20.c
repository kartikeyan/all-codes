/*	Program 1 : WAP to take a value from the user
 *		    in main function and print that value
 *		    in another function
*/ 

#include<stdio.h>

int fun(int x1, int y1){

	printf("%d\n",x1);
	printf("%d\n",y1);

}

void main(){
	
	int x,y;
	printf("Enter value of x,y :\n");
	scanf("%d %d", &x, &y);

	fun(x,y);
}
