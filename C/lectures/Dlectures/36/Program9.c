/*	Program 9:
*/

#include<stdio.h>
void main(){

	int arr1[] = {10,20,30};
	int arr2[] = {40,50,60};

	int (*ptr[])[3] = {&arr1,&arr2};

	printf("%d\n", *(*(ptr[0])));

	printf("%d\n", *(*(ptr[1])));
}
