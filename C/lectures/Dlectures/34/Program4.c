/*	Internally 2D - Array ha ya type madhe jato
 *
 * 	Formula = *[ *(arr + 1 * sizeof(whole 1D-Array i.e 1-row)) + 2 * sizeof(DTP)]
 *	
 *
 * 	eg - 
 *
 * 	arr[1][1] Internally ya type madhe jato
 *
 * 	= *[ *(arr + 1 * sizeof(whole 1D-Array i.e 1-row)) + 1 * sizeof(DTP)]
 *
 * 	= *[ *(100 + 1 * 12) + 1 * 4)
 *
 * 	= *[ *(112) + 4]
 *
 * 	= *[ 112 + 4 ]
 *
 * 	= *(116)
 *
 * 	= ValueAt(116)
 *
 *	arr[1][1] = 50
 *
 *
 *	arr[2][1] 
 *
 *	= *[ *(arr + 2 * sizeof(whole 1D array i.e 1-row)) + 1 * sizeof(DTP)]
 *
 *	= *[ *(100 + 2 * 12) + 1 * 4]
 *
 *	= *[ *(124) + 4]
 *
 *	= *[ 124 + 4]
 *
 *	= *(128)
 *
 *	= ValueAt(128)
 *
 *	= 8
*/
