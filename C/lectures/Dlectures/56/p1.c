/*	Program 1 :
 *
 *	Topic : Enum / Enumeration
*/

#include<stdio.h>

enum RoomMates{

	Harshal,
	Shreeyash,
	Mayur,
	Kartik
};

void main(){

	enum RoomMates obj;

	printf("%ld\n", sizeof(obj));	//4	
}
