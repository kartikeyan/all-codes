/*	Program 4 :
*/

#include<stdio.h>

enum RoomMates{

	Harshal,
	Shreeyash,
	Mayur,
	Kartik
};

void main(){

	enum RoomMates obj = {Core2Web, Biencaps, BoomPanda};

	printf("%d\n", Harshal);	//0
	printf("%d\n", Shreeyash);	//1
	printf("%d\n", Mayur);		//45
	printf("%d\n", Kartik);		//46
}	

/*	error : Core2Web, Biencaps, BoomPanda undeclared.
*/	
