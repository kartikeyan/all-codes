/*	Program 4 :
 *
 *	Topic : Internal of 2D Array
*/

#include<stdio.h>
void main(){

	int arr[3][3] = {10,20,30,40,50,60,70,80,90};

	printf("%p\n", &arr);	 // &arr mhnje whole array cha address

	printf("%p\n", arr);	// arr mhnje Sagla 1st row cha address

	printf("%p\n", arr[0]);	 // 1st row cha 1st element cha address

	printf("%p\n", &arr[0]);  // Complete 1st row cha address

	printf("%p\n", &arr[0][0]); //0th row cha 0th column cha address

	printf("%d\n", *(*(arr + 1) + 1)); //50

	printf("%d\n", *(*(arr + 2) + 2)); //90

}
