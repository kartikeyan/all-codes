/*	Program 2: WAP to access elements in 3D Array
 *
 *	Topic : 3D-Array
*/
#include<stdio.h>
void main(){
	
	int arr[2][2][3] = {{1,2,3,4,5,6}, {7,8,9,10,11,12}};

	printf("Array elements are :\n");

	for(int i = 0; i < 2; i++){
	
		for(int j = 0; j < 2; j++){
			
			printf("Plane is :%d\n", i);

			for(int k = 0; k < 3; k++){
			
				printf("%d ", arr[i][j][k]);
			}

			printf("\n");
		}
	}
}
