/*	Program 1 : 
 *
 *	Topic : Structure
 *
 *	when similar or different type of data stored in a same array
 *	known  as Structure.
 *
 *	Declaration : struct CricPlayer VK;
 *				^        ^
 *			    datatype     variable
*/


//	Jyana fix value ahe tyach datatype chi value consider keli jate.

#include<stdio.h>

struct CricPlayer{

	int jer_No;
	float avg;
	char grade;
};

void main(){

	struct CricPlayer Vk;

	printf("%ld\n", sizeof(Vk));	//12

	printf("%ld\n", sizeof(struct CricPlayer));	//12
}
