/*	PRogram 8 :
 *
 *	Topic : Structure Assignment / Assigning value to structure variable.
 *
 *	Structure madhe initialization naste fakt Assignment aste.
 *	karan structure la compile time la jaga bhetat nahi. jevha
 *	structure cha object banavto tevha jaga bhette.
*/

#include<stdio.h>

struct CricPlayer{

	char pName[20];
	int jerNo;
	float avg;
};

void main(){

	struct CricPlayer obj1 = {"Virat", 18, 50.33};	

	struct CricPlayer obj2 = {"MS Dhoni", 7, 45.25};

	printf("%s\n", obj1.pName);

	printf("%d\n", obj1.jerNo);

	printf("%f\n", obj1.avg);


	printf("%s\n", obj2.pName);

	printf("%d\n", obj2.jerNo);
	
	printf("%f\n", obj2.avg);
}
