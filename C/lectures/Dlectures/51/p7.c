/*	Program 7 :
 *
 *	#pragma pack : ha sangto ki structure madhe vayya janari jaga
 *		       pre-processing chya veles bagh ani jitki jaga
 *		       lagte titki de.
*/

#include<stdio.h>

#pragma pack(1)

struct Demo{

	int x;
	float y;
	double arr[5];
	char ch1;
};

void main(){

	struct Demo kar;

	printf("%ld\n", sizeof(kar));	//24

	printf("%ld\n", sizeof(struct Demo));	//24
}
