/*	Program 5 :
 *
 *	Topic : Array of Character Pointer.
 *
 *		Initialization is done in two ways :-
 *
 *		1]  char arr1[3][10] = {"Ashish", "Kanha", "Rahul"};
 *
 *		2]  char arr2[][10] = {
 *					{'A','s','h','i','s','h','\0'},
 *					{'K','a','n','h','a','\0'},
 *					{'R','a','h','u','l','\0'}
 *								  };
 *		3 naav ahet 10 size che.
 *
 *		In Array of character pointer data cannot be changed.
*/

#include<stdio.h>
#include<string.h>

void main(){

	char *arr1[3] = {"Ashish","Kanha","Ashish"};

	printf("%p\n", arr1[0]);	
	
	printf("%p\n", arr1[1]);
	
	printf("%p\n", arr1[2]);

	puts(arr1[0]);

	printf("%s\n", arr1[2]);

	strcpy(arr1[2] , "Rahul");

	puts(arr1[0]);

	printf("%s\n", arr1[2]);
}
