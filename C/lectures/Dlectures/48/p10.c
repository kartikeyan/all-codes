/*	Program 10 :
 *
 *	10 - strncat (numer of string to be concated)
 *
 *	There is no strncat library function
 *
 *	Using User defined function
*/

#include<stdio.h>

char *mystrncat(char *str1, char *str2, int num){

	while(num != 0 && *str2 != '\0'){
		
		if(num <= 0){
			
			break;
		}

		while(*str1 != '\0'){
		
			str1++;
		}

		while(*str2 != '\0'){
		
			*str1 = *str2;
			str1++;
			str2++;
		}
	}
}

void main(){

	
	char str1[20] = {'k','a','r','\0'};

	char* str2 = "tik";

	int num = 4;

	puts(str1);
	puts(str2);

	mystrncat(str1,str2,num);

	puts(str1);
}
