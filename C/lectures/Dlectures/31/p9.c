/*	Program 8 :
*/

#include<stdio.h>
void main(){
	
	char ch = 'A';

	int *iptr = &ch; //warning : initialization of "int *" from incompatible pointer type "char *"

	char *cptr = &ch;

	printf("%d\n", *iptr);

	printf("%c\n", *cptr);
}
