/*	Program 2 :
 *
 *	Topic : Substraction of Pointers
*/

#include<stdio.h>
void main(){

	int arr[] = {10,20,30,40,50};

	int *ptr1 = &(arr[0]);

	int *ptr2 = &(arr[3]);

	printf("%d\n", *ptr1);

	printf("%d\n", *ptr2);

	//Subtraction of Pointers
	
	printf("%d\n", ptr2 - ptr1);
						/* Ya donhi printf line la WARNING YETO karan donhi pointer cha 
						   substraction kelya var value yeto, toh value LONG INT chya
						   format madhe asto. 
						*/
	printf("%d\n", ptr1 - ptr2);

	printf("%ld\n", ptr2 - ptr1);  // correct format specifier for long int

	printf("%ld\n", ptr1 - ptr2);
}


/* Don pointer cha difference MEANINGFULL rahil
 * jeva doni pointer ekach array madhle elements la
 * point kartat
*/

/*	ptr2 - ptr1 = ptr2 - ptr1 / sizeof(DTP)
 *
 *	ptr1 - ptr2 = ptr1 - ptr2 / sizeof(DTP)
*/	
