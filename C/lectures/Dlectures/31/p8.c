/*	Program 8 :
*/

#include<stdio.h>
void main(){

	int x = 10;

	int *iptr = &x;

	char *cptr = &x; // warning : initialization of "char *" from incompatible pointer type "int *"

	printf("%d\n", *iptr);

	printf("%c\n", *cptr);
}
