/*	PRogram 4 :
 *
 *	Topic : Call By Address
 *
 *	In C language Call By Address is same as Call By Reference
*/

#include<stdio.h>

void swap(int* , int* );

void main(){

	int x = 10;
	int y = 20;
	
	printf("In main\n");
	printf("x = %d\n", x); //10
	printf("y = %d\n", y); //20

	swap(&x, &y);
	
	printf("In main after swap :\n");
	printf("x = %d\n", x); //20
	printf("y = %d\n", y); //10

}

void swap(int* x, int* y){
	
	printf("In Swap\n");	
	printf("x = %d\n", *x); //10
	printf("y = %d\n", *y); //20

	int temp;
	
	temp = *x;
	*x = *y;
	*y = temp;
	
	printf("In Swap after temp\n");
	printf("x = %d\n", *x); //20
	printf("y = %d\n", *y); //10
}
