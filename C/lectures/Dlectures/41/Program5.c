/*	PRogram 5 :
 *
 *	Topic : Call By Address
 *
 *	In C language Call By Address is same as Call By Reference
*/

#include<stdio.h>

void callByAddress(int*);

void main(){

	int x = 10;

	printf("%d\n", x); //10

	callByAddress(&x);
	
	printf("%d\n", x); //50

}

void callByAddress(int* ptr){

	printf("%p\n", ptr); //0x100
	printf("%d\n", *ptr); //10

	*ptr = 50;

	printf("%d\n", *ptr); //50
}
