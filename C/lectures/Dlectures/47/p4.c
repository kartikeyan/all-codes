/*	Program 4 :
 *	
 *	4 - strrev (string reverse)
 *	
 *	There is no strrev library function.
 *
 *	Using User defined Function
*/	

#include<stdio.h>

char *mystrrev(char *str){

	char *temp = str;

	while(*temp != '\0'){
	
		temp++;
	}

	temp--;

	char x;

	while(str < temp){
	
		x = *str;
		*str = *temp;
		*temp = x;

		str++;
		temp--;
	}
}

void main(){

	char str[50] = "Kartik Onkar";

	mystrrev(str);

	puts(str);
}
