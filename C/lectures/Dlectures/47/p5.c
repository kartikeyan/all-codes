/*	Program 5 :
 *
 *	5 - String Compare Ignorecase
 *
 *	There is no strcmpi Library Function
*/

#include<stdio.h>
#include<string.h>

void main(){

	char *str1 = "AShish";

	char *str2 = "ashish";

	int diff = strcmpi(str1,str2);

	if(diff == 0){
		
		printf("Strings are equal\n");
	}else{
		
		printf("Strings are not equal\n");
	}
}
