/*	Program 1 : 
 *
 *	User defined Function strcat
*/

#include<stdio.h>

char *mystrcat(char *str1, char *str2){

	while(*str1 != '\0'){
		
		str1++;
	}

	while(*str2 != '\0'){
		
		*str1 = *str2;
		str1++;
		str2++;
	}
}

void main(){

	char str1[15] = {'K','a','r','t','i','k','\0'};

	char *str2 = "Shah";

	puts(str1);
	puts(str2);

	mystrcat(str1, str2);

	puts(str1);
	puts(str2);
}
