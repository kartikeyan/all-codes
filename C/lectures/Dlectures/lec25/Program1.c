/*	Array : 
 *		1 - Array internally is Pointer
 *
 *		2 - Array is used because multiple elements of same datatype can be initialized only once.
 *		
 *		3 - By using Array size of memory is not decreased.
 *
 *	
 *	Array initialization :
 *
 *	1 - int iarr[5] = {12,34,56,87,79};
 *
 *	2 - int iarr[] = {10,20,340,54,9};
 *
 *		There are 5 values in the array that why array allocates 5 boxes to store elements.
 *		5 elements size would be 20 because int size is 4 bytes and 5 elements * 4 = 20
 *
 *	3 - char carr[4] = {'A', 'B', 'Y', 'L'};
 *
 *	4 - float farr[3] = {20.1, 49.5, 23.8};
 *
