/*	Program 12 :
 *
 *	Topic : Bit field
*/

#include<stdio.h>

struct Demo{

	int x:4;
	int y;
};

void main(){

	struct Demo obj = {16, 500};
	
	printf("%ld\n", sizeof(struct Demo));	//8
}

/*	warning : overflow in conversion from "int" to "signed char:1" changes value from "16" to "0"
 *		  
 *		  struct Demo obj = {16, 500};
 *		  		      ^
*/		  		      
