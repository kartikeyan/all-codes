/*	PRogram 10 :
 *
 *	Topic : Bit field
*/

#include<stdio.h>

struct Demo{

	int x:4;
	int y;
};

void main(){

	struct Demo obj = {15, 500};
	
	printf("%ld\n", sizeof(struct Demo));	//8
}
