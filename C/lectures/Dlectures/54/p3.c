/*	Program 3 : 
 *
 *	Important Problem
*/

#include<stdio.h>

struct Demo{

	int x;
	float y;
};

void main(){

	struct Demo obj = {10, 20.5f};

	int arr[] = {10,20,30,40,50};

	printf("%p\n", &arr[0]);	//0x200
	printf("%p\n", arr);		//0x200

	printf("%p\n", &obj.x);		//0x100
	printf("%p\n", &obj);		//0x100
}

