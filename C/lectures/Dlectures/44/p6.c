/*	Program 6 :
 *
 *	'\0' tells that strings ends here
*/

#include<stdio.h>

void main(){

	char carr[] = {'c','o','r','e','2','w','e','b','\0'};

	char *str = "core2web";		// constant string

	printf("%s\n", carr);	// core2web

	printf("%s\n", str);	// core2web

	carr[4] = '3';		

	printf("%s\n", carr);	// core3web
}

/*	character array madhe string change karu shakto 
 *	pan character pointer madhe change nahi karu shakat
 *	karan character pointer constant ahe.
*/	
