/*	Program 5 :
 *
 *	Topic : Character Array or String 
 *
 *	Initialization can be done in two ways :
 *
 *	1 - char *str = "Coreweb";
 *		            ^ is String Literal
 *	
 *	2 - char carr[] = {'C','o','r','e','2','w','e','b'};
*/

#include<stdio.h>

void main(){

	 char *str = "Coreweb";
 		           
 	 char carr[] = {'C','o','r','e','2','w','e','b'};

	 printf("%s\n", carr);

	 printf("%s\n", str);	
}
