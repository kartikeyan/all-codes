/*	Program 2 : Returning Array to a function (Wrong Concept)
*/

#include<stdio.h>

int *fun(){
	
	int arr[] = {10,20,30};

	return arr;	// warning : function returns address of local variables

}

void main(){

	int *ptr = fun();

	printf("%d\n", *ptr);	// segmentation fault core dumped

}
