/*	Program 7 :
*/

#include<stdio.h>

void main(){

	char carr[] = {'c','o','r','e','2','w','e','b','\0'};

	char *str = "core2web";		// constant string

	printf("%s\n", carr);	// core2web

	printf("%s\n", str);	// core2web

	*str = 'B'; 

	printf("%s\n", str);	// segmentation fault 
}

/*	character pointer madhe string change nahi karu shakat
 *	karan character pointer constant ahe.
 *
 *	ROdata madhe char pointer(string) jatat
 *	ani tey read only data asto.Data change
 *	nahi karu shakat
*/	
