/*	Program 1 :
*/

#include<stdio.h>

int *fun(int x, int y){

	printf("%d\n", x+y);
	
	int val;
	val = x+y;

	return &val;	// warning : function return address of local variables
		 	//	     return &val;
			//
			// output : segmentation fault
}

void main(){

	int *ptr = fun(10,20);

	printf("%p\n", ptr);	//nil

	printf("%d\n", *ptr);	//30
}


// local variables che address kadich return karaycha nahi.
