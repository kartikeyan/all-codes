/*	Program 4 :
 *
 *	Topic : Function Storage Class.
 *
 *	Function parameter la storage class deta yet nahi.
 *
 *	Function la default linkage extern asto.
*/

#include<stdio.h>

void fun(int x, int y){

	int a;
	register int b = 30;
	static int c = 40;
	extern int d;
}

void main(){

	fun(20,30);
}

/*	int a cha default  storage class auto ahe.
 *
 *	register la CPU madhe memory milto.
 *	Pan memory milnyachi probability kaam ahe.
 *	register chya variables cha address kadayacha 
 *	prayatna kelas tar error yeil.
 *
 *	local static variable ahe
 *
 *	extern variable ahe d.Kahi print kela nahi
 *	mule code compile hoto, nahitar jar variables 
 *	print kele aste tar error ala asta karan apan 
 *	d la value dilach nahi.
 *
 *	Ek function madhe ekach variables che static and extern 
 *	storage class chalat nahi, pan different variables 
 *	ghetla ani static and extern ekach function madhe chaltat.
 *
 *	int x, int y function che parameter ahet,
 *	parameter la storage class deta yet nahi.
 *
 *	Function la default linkage extern asto.
