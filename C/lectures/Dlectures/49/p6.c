/*	Program 6 :
 *
 *	register fakt local variable mahnun chalto.
*/

#include<stdio.h>

register int y = 10;

void main(){

	register int x = 10;

	printf("%d\n", x);

	printf("%d\n", y);

//	printf("%p\n", &y);

}

/*	error : register name not specified for 'y'
 *		
 *		register int y = 10;
*/		
