/*	Program 3 :
 *	
 *	Topic : Wild And Null Pointer
*/

#include<stdio.h>
void main(){

	int x = 10;

	//wild pointer
	int *iptr;

	printf("%p\n", iptr);  // nil

	printf("%d\n", *iptr); // Segmentation fault

	//null pointer
	int *iptr = 0;

	printf("%p\n", iptr); // nil

	printf("%d\n", *iptr); // segmentation fault
}


/*
 * 	Address of wild and null pointer is nil
*/ 	
