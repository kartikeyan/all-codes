/*	Program 1 :
 *
 *	Constant fakt compile time paryant maryadit ahe 
*/

#include<stdio.h>

void main(){

	const int x;

	scanf("%d", &x);

	printf("%d\n", x);
}

/*	warning : writing into constant object (argument 2)
 *		  scanf("%d", &x);
 *		  ^ 
*/		  
