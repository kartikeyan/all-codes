/*	Program 13 :
*/

#include<stdio.h>

void main(){

	char cName[] = {'V','i','r','a','t','\0'};	// Gets memory in stack frame

	char *fName1 = "Messi";		// Same address as for Fname3

	char *fName2 = "Neymar";i			

	char *fName3 = "Messi";		// Same address as for Fname1

	printf("%p\n", cName);

	printf("%p\n", fName1);

	printf("%p\n", fName2);

	printf("%p\n", fName3);
}


/* fname1, fname2, fname3 gets memory in ROdata frame.
*/ 
