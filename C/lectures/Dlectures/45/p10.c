/*	Program 10 :
 *
 *	Always add '\0' in char array because '\0' 
 *	indicates end of the string
 *
 *	In char pointer there is already '\0' in string
*/

#include<stdio.h>

void main(){

	char pName[] = {'r','o','h','i','t','S','\0'};

	char *name = "ViratK";

	for(int i = 0; i < 7 ; i++){
	
		printf("%c", pName[i]);
	}

	printf("\n");

	for(int i = 0; i < 7; i++){
	
		printf("%c", name[i]);
	}
}

/*	user kadun string gyaychi asel tar - array gyaycha
 *	string mahit asel tar - char Pointer gyaycha
*/	
