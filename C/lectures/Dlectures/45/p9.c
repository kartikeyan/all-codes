/*	Program 9 :
 *
 *	Using Function gets (get string) instead of scanf
 *
 *	gets is Dangerous function ahe
 *
 * 	puts can be used instead of printf
*/

#include<stdio.h>

void main(){

	char name[20];

	printf("Enter name :\n");

	gets(name);
	
	puts(name);

	printf("%s\n", name);
}

/*	warning : implicit declaration of function 'gets'; did you mean 'fgets'?
 *		  
 *		  gets(name);
 *		  ^
 *		  fgets
 * 	
 * 	warning : the 'gets' function is dangerous and should not be used
*/		  
