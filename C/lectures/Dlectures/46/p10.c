/*	Program 10 : 
 *
 *	3 - strcat (string concate);
 *		
 *	    Declaration : char *strcat(char *src, char *dest);
 *
 *	    Using library function
*/
#include<stdio.h>
#include<string.h>

void main(){

	char str1[20] = {'k','a','r','t','i','k','\0'};

	char *str2 = "Charkupalli";

	puts(str1);
	puts(str2);

	strcat(str1, str2);

	puts(str1);
	puts(str2);
}
