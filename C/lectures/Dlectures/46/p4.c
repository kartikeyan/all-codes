/*
 *	Topic : String Libraries Functions :-
 *
 *	1 - strlen = string length
 *
 *	2 - strcpy = string copy
 *
 *	3 - strcat = string concate
 *
 *	4 - strrev = string reverse
 *
 *	5 - strcmp = string compare
 *
 *	6 - strcmpi = string compare Ignorecase
 *
 *	7 - strlwr = string lowercase
 *
 *	8 - strupr = string uppercase
 *
 *	9 - strncpy = numbers of characater copy
 *
 *	10 - strncat = numbers of character concate
*/	
