/*	Program 3 :
 *
 *	Passing string to a function
*/

#include<stdio.h>

void printString(char str1[]);
				// Declaratiobs : That is type of function
				// 		  is going to come forwar in code
void stringPrint(char *str);   

void main(){

	char arr[20];

	printf("Enter your name :\n");
	gets(arr);

	printString(arr);

	stringPrint(arr);
}

void printString(char str1[]){

	puts(str1);
}

void stringPrint(char *str2){

	puts(str2);
}
