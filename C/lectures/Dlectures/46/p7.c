/*	Program 7 :
 *
 *	2 - strcpy (string copy)
 *
 *	    Declaration : char *mystrcpy(char *dest, const char *src);
 *
 *	    Using Library Function
*/

#include<stdio.h>
#include<string.h>

void main(){

	char *str1 = "Hardik Pandya";

	char str2[20];

	puts(str1);

	puts(str2); 		

	strcpy(str2, str1);	// strcpy(dest, src);

	puts(str1);

	puts(str2);
}

/*	for string copy, first you have to send
 *	string destination and then string source
 *	where u have to copy string
*/	
