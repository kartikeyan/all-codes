/*	PRogram 9 :
 *
 *	Topic : Function Types
 *		
 *		2 - Function with 1 input and output.
*/

#include<stdio.h>

void fun(int x){
	
	printf("%d\n", x);
}

void main(){
	
	fun(10);	//meaning becomes int x = 10.
}
