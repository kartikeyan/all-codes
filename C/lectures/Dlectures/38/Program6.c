/*	Program 6 :
 *
 *	Topic : Functions
 *
 *		Declaration(Prototype) :  	
 *
 *		return-value functionName(parameters)
 *
 *						^ - Parameters can be 0 or more
*/

#include<stdio.h>

int work(){
	
	printf("Working.......\n");
	
}

void main(){
	
	printf("Start Main\n");

	work();

	printf("End Main\n");
}

