/*	Program 6:WAP which takes a no. as input from user if no. is even
 *		  print that no. in reverse order upto 1 & if no. is odd
 *		  print that no. in reverse order in difference of 2
*/

#include<stdio.h>

void main(){

	int x;
	printf("Enter x:\n");
	scanf("%d", &x);

	if(x % 2 == 0){
		
		while(x > 0){
			printf("%d\n", x);
			x--;
		}
	}else if(x % 2 != 0){
		
		while(x > 0){
			printf("%d\n", x);
			x = x - 2;
		}
	}else{
		printf("Zero\n");
	}
}
