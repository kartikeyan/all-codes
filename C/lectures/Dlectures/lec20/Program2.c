/*	Program 2:WAP to take 2 users from input and see if they are
 *		  divisible by 4 and 5 and is they are divisible then 
 *		  print them as it is
*/

#include<stdio.h>

void main(){
	
	int xstart, yend;

	printf("Enter xstart, yend values :\n");
	scanf("%d %d", &xstart, &yend);

	while(xstart <= yend){
		
		if(xstart % 4 == 0 && xstart % 5 == 0){
			printf("Numbers are : %d\n", xstart);
		}
		xstart++;
	}
}
