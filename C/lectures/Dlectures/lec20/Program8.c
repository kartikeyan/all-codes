#include<stdio.h>
void main(){
	
	char lower;
	char upper;

	printf("Enter lower bound :\n");
	scanf(" %c", &lower);

	printf("Enter upper bound :\n");
	scanf(" %c", &upper);
	
	while(lower <= upper){
	
		if(lower == 'A' || lower == 'E' || lower == 'I' || lower == 'O' || lower == 'U'||
				lower == 'a'|| lower == 'e' || lower == 'i' || lower == 'o' || lower == 'u'){
				
			printf("%c is a vowel\n", lower);
		}else{
			printf("%c is a consonant\n", lower);
		}
		lower++;
	}
}
