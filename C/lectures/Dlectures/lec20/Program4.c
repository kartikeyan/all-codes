/*	Program 4 : WAP to print the number in reverse order after multiplying it by 5
 *
*/

#include<stdio.h>
void main(){

	int x;
	int rem;

	printf("Enter number :\n");
	scanf("%d", &x);

	while(x != 0){
	
		rem = x % 10;

		printf("Number is :%d\n", rem * 5);
		x = x / 10;
	}
}
