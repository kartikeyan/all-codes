/*	Program 5 : WAP to print the countdown of days 
 *		    to submit the assignment.
*/

#include<stdio.h>

void main(){

	int x;
	printf("Enter days :\n");
	scanf("%d", &x);

	while(x != 0){
		
		printf("%d days remaining\n", x);
		x--;
	}
	printf("%d due date\n", x);
}


/*	while(x != 0){
 *		
 *		if(x == 1){
 *			printf("% day remaining\n", x);
 *		}else{
 *			printf("%d days remaining\n", x);
 *		}
 *		x--;
 *	}
 *	printf("%d due date\n", x);
 *
 *	}
*/	
