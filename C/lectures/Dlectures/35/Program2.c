/*	Program 1 : 
 *
 *	Topic : ARRAY OF POINTERS
 *
 *	1 - Array of pointers stores address of array elements.
 *
 *	2 - Instead of Addressing Array of Pointers, call
 *	    ARRAY OF VOID POINTERS.
 *
 *	3 - Array of pointers cha size nehmi 8 bytes asto.
*/

#include<stdio.h>
void main(){

	int x = 10;
	int y = 20;
	int z = 30;

	int* arr[3] = {&x, &y, &z};

	printf("%ld\n", sizeof(arr)); //24

	printf("%p\n", arr[0]); //)0x100
	printf("%p\n", arr[1]); //0x104
	printf("%p\n", arr[2]); //0x108

	printf("%d\n", *arr[0]); //10
	printf("%d\n", *arr[1]); //20
	printf("%d\n", *arr[2]); //30
}
