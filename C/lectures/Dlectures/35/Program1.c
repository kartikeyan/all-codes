/*	Program 1 : 
 *
 *	Topic : ARRAY OF POINTERS
 *
 *	1 - Array of pointers stores address of array elements.
 *
 *	2 - Instead of Addressing Array of Pointers, call
 *	    ARRAY OF VOID POINTERS.
 *
 *	3 - Array of pointers cha size nehmi 8 bytes asto.
*/

#include<stdio.h>
void main(){

	int x = 10;
	int y = 20;
	int z = 30;

	int* arr[3] = {x, y, z};

	printf("%p\n", arr[0]);
	printf("%p\n", arr[1]);
	printf("%p\n", arr[2]);

	printf("%d\n", *arr[0]); //segmentation fault 
	printf("%d\n", *arr[1]);
	printf("%d\n", *arr[2]);


}

/*	warning : initialization of 'int *' from 'int' makes pointer from integer without a cast.
 *		  int* arr = {x,y,z};
 *		              ^
 *		  note : near initialization for 'arr[0]'
 *
 *	warning : same
 *		
 *		  note : near initialization for 'arr[1]'
 *
 *	warning :same
 *		 
 *		 note : near initialization for 'arr[2]'
*/		 
