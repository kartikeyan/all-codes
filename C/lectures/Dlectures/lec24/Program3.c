/*	Program 3 : Take no. of rows from the user
 *		    
 *		    1 A 2 B
 *		    1 A 2
 *		    1 A
 *		    1
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		int x = 1;
		char ch = 'A';

		for(int j = 1; j <= row-i; j++){
		
			if(j % 2 == 0){
				printf("%d ", x);
				x++;	
			}else{
				printf("%c ", ch);
				ch++;	
			}
		
		}

		printf("\n");
	}
}
