/*	Program 5 :
*/
#include<stdio.h>
void main(){

	int arr1[3][3] = {1,2,3,4,5,6,7,8,9};  /*  arr1  1 2 3
						   	 4 5 6
							 7 8 9
						*/

	int arr2[2][3] = {1,2,3,4};	/*  arr2 1 2 3
					    	 4 0 0
					*/

	int arr3[][3] = {1,2,3,4,5,6,7,8,9};  /* arr3 1 2 3
						      4 5 6
						      7 8 9
					      */

	int arr4[3][] = {1,2,3,4,5,6,7,8,9};  // error : array type has incomplete element type "int[]"
}
