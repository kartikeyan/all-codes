/*	Program 4:
 *
 *	Topic : Multidimensional Array
 *
 *	Multidimensional Array madhe column dena compulsory ahe
 *	nahitar error milto.
*/

#include<stdio.h>
void main(){

	int arr1[] = {1,2,3,4,5,6,7,8,9};

	int arr2[][] = {1,2,3,4,5,6,7,8,9}; 

}

/*	error : array element has incomplete element type "int[]"
 *		int arr2[][] = {1,2,..,9};
 *		    ^
 *		
 *	note : declaration of "arr2" as multidimensional array must have
 *	       bounds for all dimensions except the first
