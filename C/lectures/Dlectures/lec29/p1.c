/*		Topic : Pointer  
 *		
 *		1 - Definition : Pointer is a variable that can store address of any variable.
 *
 *		2 - Size of Pointer is 8 bytes
 *
 *		3 - "%p" format specifier is used to remove address of any variable or pointer
 *
 *		4 - "&" - Reference Pointer
 *		    
 *		    "*" - Dereference Pointer
 */


#include<stdio.h>

void main(){

	int x = 10;

	printf("%d\n", x);

	printf("%d\n", &x);

	printf("%p\n", &x);
}

/*	In above program "%d" format specifier gives half address of variable x 
 *	because "%d" is for interger data type and it has size 4 bytes.
 *
 *	"%p" format specifier is used to remove address of variable x.
 *	It gives full address i.e address is in hexa-decimal format.
