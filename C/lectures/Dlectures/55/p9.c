/*	PRogram 9 :
 *
 *	Topic : Enum
*/

#include<stdio.h>

struct Emp{

	int x : 4;
	float y;
	double z;
}obj2;

void main(){

	printf("%p\n", &obj2.x);	//0x100
	printf("%p\n", &obj2.y);	//0x104
	printf("%p\n", &obj2.z);	//0x108
}

//	error : cannot take address of bit-field 'x'
