/*	Program 4 : 
 *
 *	Topic : Union
 *
 *		Memory vachvaycha asel tar union gyaycha.
*/

#include<stdio.h>

struct Demo1{

	int x;
	float y;
	double z;
}obj1;

union Demo2{

	int x;
	float y;
	double z;
}obj2;

void main(){
	
	printf("%ld\n", sizeof(obj1));	//16

	printf("%ld\n", sizeof(obj2));	//8
}

/*	Union madhe largest element cha size bhetato
*/	
