/*	Program 1 : WAP to initialize a character array or string in a multiple way
 *		    and print(from the user and hardcoaded using for loop and get()).
*/

#include<stdio.h>

void main(){

	char arr1[15] = {'V','I','R','A','T','K','O','H','L','I','\0'};

	char *str1 = "ROHITSHARMA";

	char arr2[20];

	char *str2;

	gets(arr2);

	str2 = arr2;

	printf("Printing Using s\n");

	printf("%s\n", arr1);	
	printf("%s\n", str1);
	printf("%s\n", arr2);
	printf("%s\n", str2);

	printf("Printing Using puts() \n");

	puts(arr1);
	puts(str1);
	puts(arr2);
	puts(str2);

	printf("Printing Using Forloop\n");

	for(int i = 0; arr1[i] != '\0'; i++){
	
		printf("%c", arr1[i]);
	}
	printf("\n");

	for(int i = 0; str1[i] != '\0'; i++){
	
		printf("%c", str1[i]);
	}

	printf("\n");

}
