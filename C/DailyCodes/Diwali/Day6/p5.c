/*	PRogram 5 : WAP to get 2 strings from user and compare that with case 
 *		    ignore(write your on string compare function with ignore case).
*/

#include<stdio.h>

int mystrcmpi(char *str1, char *str2){

	int flag = 1;

	if(strlen(str1) == strlen(str2)){
	
		while(*str1 != '\0'){
			
			if(*str1 == *str2 || *str1 == (*str2) + 32 || *str1 == (*str2) - 32){
			
				str1++;
				str2++;
			}else{
			
				flag = 0;
				break;
			}
		}
	}else{
	
		flag = 0;
	}
	return flag;
}

void main(){

	char str1[20];
	gets(str1);

	char str2[20];
	gets(str2);

	int diff = mystrcmpi(str1, str2);

	if(diff == 1){
		
		printf("String is Same\n");
	}else{
	
		printf("Strings are not same\n");
	}
}
