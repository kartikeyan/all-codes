/*	Program 4 : Write a function for string reverse and 
 *		    check it reversed string and original string is same or not.
*/

#include<stdio.h>
#include<string.h>

char *mystrrev(char *str2, char *str1){

	int len = strlen(str1);

	str2 = str2 + len;
	*str2 = '\0';
	str2--;

	for(int i = 0; *str1 != '\0'; i++){
	
		*str2 = *str1;
		str1++;
		str2--;
	}
	return str2;
}

int mystrcmp(char *str1, char *str2){

	int flag = 1;

	if(strlen(str1) == strlen(str2)){
		
		while(*str1 != '\0'){
		
			if(*str1 != *str2){
				
				flag = 0;
				break;
			}
			str1++;
			str2++;
		}
	}else{
	
		flag = 0;
	}
	return flag;
}

void main(){

	char str1[20];

	gets(str1);

	char str2[20];

	printf("\nAfter String Reverse\n");
	mystrrev(str2, str1);

	printf("str2->");
	puts(str2);

	int diff = mystrcmp(str1, str2);

	printf("\nAfter String Reverse\n");

	if(diff == 1){
		
		printf("String is same\n");
	}else{
	
		printf("String is not same\n");
	}
}
