/*	Program 2 : WAP to use predefined length function and your own length function
 *		    for array.(get array from user).
*/

#include<stdio.h>
#include<string.h>

int mystrlen(char *str){

	int count = 0;

	while(*str != '\0'){
		
		count++;
		str++;
	}
	return count;
}

void main(){
	
	char arr[20];

	gets(arr);

	int len = strlen(arr);

	printf("Length using predefined function is %d\n", len);

	len = mystrlen(arr);

	printf("Length using own function is %d\n", len);
}
