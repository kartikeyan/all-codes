/*	Program 6 : WAP to take number from user in retval() function and
 *		    return the first and last digit of that number in main function and print.
*/

#include<stdio.h>

void retVal(int num, int *ptrfirst, int *ptrlast){

	int rem;
	*ptrlast = num % 10;

	while(num != 0){
	
		rem =  num % 10;
		num /= 10;
		*ptrfirst = rem;
	}
}

void main(){

	int num;

	printf("Enter number :\n");
	scanf("%d", &num);

	int first;
	int last;

	retVal(num, &first, &last);

	printf("The first & last digit of %d is %d & %d\n", num, first, last);
}
