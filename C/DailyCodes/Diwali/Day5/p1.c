/*	PRogram 1 : WAP to take size, integer array element from user in main function and
 *		    print all even element in evenArr() function(passing Array to function).
*/

#include<stdio.h>

void EvenArr(int arr[], int size){

	for(int i = 0; i < size; i++){
	
		if(arr[i] % 2 == 0){
			
			printf("%d\n", arr[i]);
		}
	}
}

void main(){

	int size;

	printf("Enter Array size :\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array Elements\n");
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}
	
	printf("Even element of given array is :\n");

	EvenArr(arr, size);
}
