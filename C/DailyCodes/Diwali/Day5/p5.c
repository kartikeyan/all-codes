/*	Program 5 : WAP to take a size, character array element from user in retArr() function
 *		    return array to the main function, print the all array element in main function.
*/		    

#include<stdio.h>

int[] retArr(){

	int size;

	printf("Enter Array Size: ");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");

	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}

	return arr[];
}

void main(){

	int arr[] = retArr();

	for(int i = 0; i < 5; i++){
	
		printf("%d\n", arr[i]);
	}
}

//  returning Array from function is not possible because multiple values will be return.
