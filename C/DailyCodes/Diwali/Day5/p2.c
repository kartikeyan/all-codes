/*	Program 2 : WAP to print the Multiplication of two number using 
 *		    Function Pointer(write a mult function and print the multiplied
 *		    value in main function).
*/

#include<stdio.h>

int mult(int a, int b){
	
	return a * b;
}

void main(){

	int a,b;

	printf("Enter value of a and b :\n");
	scanf("%d", &a);
	scanf("%d", &b);

	int (*fptr)(int,int);

	fptr = mult;

	int mult = fptr(a,b);

	printf("Multiplication of %d & %d is %d\n",a,b,mult);
}
