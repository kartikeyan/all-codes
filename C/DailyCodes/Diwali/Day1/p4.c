/*	Program 4 : WAP to compare two arrays by with length and elements given 
 *		    by user if array are equal then print "Equal" otherwise
 *		    print "Not Equal"
*/

#include<stdio.h>
void main(){

	int size1,size2;
	
	printf("Enter first array size:\n");
	scanf("%d", &size1);
	
	int arr1[size1];

	printf("Enter First array elements :\n");

	for(int i = 0; i < size1; i++){
	
		scanf("%d", &arr1[i]);
	}

	printf("Enter second array size:\n");
	scanf("%d", &size2);
	
	int arr2[size2];

	printf("Enter Second Array elements:\n");
	
	for(int i = 0; i < size2; i++){
	
		scanf("%d", &arr2[i]);
	}

	if(size1 == size2){
	
		int flag = 0;

		for(int i = 0; i < 5; i++){
		
			if(arr1[i] != arr2[i]){
			
				printf("Not Equal\n");

				flag = 0;
			}else{
				
				flag = 1;
			}
		}

		if(flag == 1){
		
			printf("Equal\n");
		}
	}else{
	
		printf("Not Equal:\n");
	}

}
