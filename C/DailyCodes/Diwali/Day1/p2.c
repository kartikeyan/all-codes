/*	Program 2 : WAP to create 2-D Array of size given from user and assign that 
 *		    array to another array of same size and print the seecond Array.
*/

#include<stdio.h>
void main(){

	int row,col;

	printf("Enter row in an Array:\n");
	scanf("%d", &row);

	printf("Enter col in an Array:\n");
	scanf("%d", &col);

	int arr1[row][col];
	int arr2[row][col];

	printf("Enter first Array Elements :\n");
	for(int i = 0; i < row; i++){
	
		for(int j = 0; j < col; j++){
		
			scanf("%d", &arr1[i][j]);
		}
	}

	for(int i = 0; i < row; i++){
	
		for(int j = 0; j < col; j++){
			
			arr2[i][j] = arr1[i][j];

		}
	}

	printf("Enter Second Array Elements :\n");

	for(int i = 0; i < row; i++){
	
		for(int j = 0; j < col; j++){
		
			printf("%d\t", arr2[i][j]);
		}

		printf("\n");
	}	
}
