/*	Program 5 : WAP to take 1-D array from the user and print the second
 *		    largest element of the array.
*/

#include<stdio.h>
void main(){

	int size;
	
	printf("Enter array size:\n");
	scanf("%d", &size);
	
	int arr[size];

	printf("Enter array elements :\n");
	for(int i = 0; i < size; i++){
		scanf("%d", &arr[i]);
	}
	
	int temp;

	for(int i = 0; i < size; i++){
	
		for(int j = i + 1; j < size; j++){
		
			if(arr[i] > arr[j]){
			
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}

	printf("%d\n", arr[size-2]);
}
