/*	Program 5 : Write a function which returns the reverse number of the given number.
*/

#include<stdio.h>

int revNUM(int num){

	int rev = 0;
	int rem;

	while(num != 0){
		
		rem = num % 10;
		rev = rev * 10 + rem;
		num = num / 10;
	}
	return rev;
}

void main(){

	int x;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	int reverse = revNUM(x);

	printf("Reverse Num is :%d\n", reverse);
}
