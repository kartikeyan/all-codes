/*	Program 4 : Write a function which checks the given number is prime number or not.
*/

#include<stdio.h>

int Prime(int num){

	for(int i = 2; i <= num/2; i++){
	
		if(num % i == 0){
			return 0;
		}
	}
	return 1;
}

void main(){

	int x;

	printf("ENter value of x :\n");
	scanf("%d", &x);

	int flag = Prime(x);

	if(flag == 1){
		
		printf("%d is prime number\n", x);
	}else{
	
		printf("%d is not a prime number\n",x);
	}
}
