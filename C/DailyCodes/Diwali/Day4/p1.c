/*	Program 1 : Write a function which returns the addition of two numbers and
 *		    print the answer in main function.
*/

#include<stdio.h>

int sum(int a,int b){

	return a + b;
}

void main(){

	int a,b;

	printf("Enter value of a :\n");
	scanf("%d", &a);
	printf("Enter value of b :\n");
	scanf("%d", &b);

	int ans = sum(a,b);

	printf("Sum is : %d\n", ans);
}
