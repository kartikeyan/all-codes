/*	Program 2 : WAP of swapping two numbers by using call by value.
*/

#include<stdio.h>

void swap(int x, int y){

	printf("Before Swapping :\n");

	printf("x is :%d\n", x);
	printf("y is :%d\n", y);
	
	//Using Third Variable temp.
	int temp;

	temp = x;
	x = y;
	y = temp;

	printf("After Swapping :\n");

	printf("x is :%d\n", x);
	printf("y is :%d\n", y);
}

void main(){

	int x,y;

	printf("Enter value of x :\n");
	scanf("%d", &x);

	printf("Enter value of y :\n");
	scanf("%d", &y);

	swap(x,y);
}

/*	We can also swap without using third variable.
 *	
 *	1 Method :
 *		
 *		1-step : x = x+y;
 *		2-step : y = x-y;
 *		3-step : x = x-y;
 *
 *		We can also use multipication and division.
 *
 *		1-step : x = x*y;
 *		2-step : y = x/y;
 *		3-step : x = x/y;
 *
 *
 *	2 Method :
 *		
 *		1-step : x = x ^ y;
 *		2-step : y = x ^ y;
 *		3-step : x = x ^ y;
 *		
 *
*/	
