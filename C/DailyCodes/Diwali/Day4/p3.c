/*	Program 3 : WAP of swapping two numbers by using call by address.
*/

#include<stdio.h>

void swap(int *x, int *y){
	
	//Using third variable
	int temp;

	temp = *x;
	*x = *y;
	*y = temp;
}

void main(){

	int x, y;

	printf("Enter value of x :\n");
	scanf("%d", &x);
	printf("Enter value of y :\n");
	scanf("%d", &y);
	
	printf("Before swapping :\n");
	printf("x is :%d\n", x);
	printf("y is :%d\n", y);
	
	swap(&x, &y);
	
	printf("After Swapping :\n");
	printf("x is :%d\n", x);
	printf("y is :%d\n", y);

}
