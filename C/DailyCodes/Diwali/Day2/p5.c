/*	Program 5 : Draw a neat diagram, ouput and arithmetic explanation for below code.
*/

#include<stdio.h>

void main(){

	int arr[] = {10,20,30,40,50};		//address = {100,104,108,112,116};

	int *ptr1 = &arr[1];
	int *ptr2 = &arr[2];

	printf("%p\n", ptr1);		//104
	printf("%p\n", ptr2);		//108

	ptr1++;		// ptr1 = 104 + 1 * 4 = 108
	--ptr1;		// ptr2 = 108 + (-1) * 4 = 104

	printf("%d\n", *ptr1);		//30
	printf("%d\n", *ptr2);		//20
	
	printf("%p\n", ptr1);		//108
	printf("%p\n", ptr2);		//104

	
	printf("%ld\n", (ptr1-ptr2)+2);		//(108-104/4) + 2 = 3
	printf("%p\n", ptr2+(ptr1-ptr2));	//104+(108-104/4) = 108
	printf("%d\n", *(ptr2+(ptr1-ptr2)));	//30

	printf("%p\n", (ptr1*2));
	printf("%d\n", *(ptr1*ptr2));
	printf("%p\n", (ptr1/2));
	//multiplication and division is not allowed
}
