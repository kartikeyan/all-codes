/*	Program 3 : Take an array of void pointer and store data of multiple 
 *		    datatype of element(int, char, float) and print the array.
*/

#include<stdio.h>

void main(){

	int age = 14;
	char name = 'k';
	float salary = 5.00f;

	void *vparr[] = {&age, &name, &salary};
	
	printf("Address of vparr :%p\n", vparr);

	printf("Address of age : %p\n", &age);
	printf("Address of name : %p\n", &name);
	printf("Address of salary : %p\n", &salary);
	
	printf("%d\n", *(int*)vparr[0]);
	printf("%c\n", *(char*)vparr[1]);
	printf("%f\n", *(float*)vparr[2]);

}	
