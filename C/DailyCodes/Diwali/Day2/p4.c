/*	Program 4 : WAP of every type of pointer and draw a suitable diagram of that,
 *		    (wild pointer, null pointer, dangling pointer, void pointer).
*/

#include<stdio.h>

int *dptr = 0;		// Dangling Pointer

void fun(){

	int x = 10;

	printf("In fun\n");
	printf("x = %d\n", x);
	printf("Address of x = %p\n", &x);

	dptr = &x;

	printf("Address stored dptr in fun :%p\n", dptr);	
	printf("Dereference of dptr in fun :%d\n", *dptr);
}

void main(){

	printf("In main\n");

	fun();
	printf(" *dptr after fun :%d\n", *dptr);

	int *wptr;
	printf("Address stored in wptr :%p\n", wptr);
//	printf("Dereferencing of wptr :%d\n", *wptr);		//segmentation fault(core dump)


	int *nptr = 0;
	printf("Address stored in nptr :%p\n", nptr);
//	printf("Dereferencing of nptr :%d\n", *nptr);		//segmentation fault(core dump)


}
