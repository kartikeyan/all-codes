/*	Program 1 : WAP and take one integer pointer, two integer pointer to an pointer and
 *		    store address in it, print that data by dereferencing all three Pointer.
*/

#include<stdio.h>
void main(){

	int x = 10;

	int *ptr1 = &x;
	int *ptr2 = ptr1;
	int **ptr3 = &ptr2;
	int ***ptr4 = &ptr3;

	printf("Address of x :%p\n", &x);
	printf("Address stored in ptr1 :%p\n",ptr1);
	printf("Address stored in ptr2 :%p\n",ptr2);
	printf("Address stored in ptr3 :%p\n",ptr3);
	printf("Address stored in ptr4 :%p\n",ptr4);

	printf("%d\n", x);
	printf("%d\n", *ptr1);
	printf("%d\n", *ptr2);
	printf("%d\n", **ptr3);
	printf("%d\n", ***ptr4);

}
