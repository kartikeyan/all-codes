/*	Program 2 : WAP which shows a concept of void pointer and access data
 *		    using void pointer(Don't repeat class example)
*/

#include<stdio.h>
void main(){

	int std = 10;
	char name = 'K';
	float salary = 30.50f;

	void *vptr1 = &std;
	void *vptr2 = &name;
	void *vptr3 = &salary;

	printf("Address stored in std :%p\n",&std);
	printf("Address stored in name :%p\n",&name);
	printf("Address stored in salary :%p\n",&salary);
	
	printf("\n");

	printf("Address stored in vptr1 :%p\n", vptr1);
	printf("Address stored in vptr2 :%p\n", vptr2);
	printf("Address stored in vptr3 :%p\n", vptr3);

	printf("%d\n", *(int*)vptr1);
	printf("%c\n", *(char*)vptr2);	
	printf("%f\n", *(float*)vptr3);
	
}
