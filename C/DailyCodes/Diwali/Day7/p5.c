/*	Program 5 : Write the user defined functions of mystrlwr and mystrupr.
*/

#include<stdio.h>

char *mystrlwr(char *str){

	while(*str != '\0'){
	
		if(*str >= 65 && *str <=90){
			
			*str = *str + 32;
		}
		str++;
	}
}

char *mystrupr(char *str){

	while(*str != '\0'){
	
		if(*str >= 97 && *str <= 122){
		
			*str = *str - 32;
		}

		str++;
	}
}

void main(){

	char name[10];
	printf("Enter name :\n");
	gets(name);

	mystrlwr(name);
	printf("Entered string in lowercase is ");
	puts(name);

	mystrupr(name);
	printf("Entered string in uppercase is ");
	puts(name);
}
