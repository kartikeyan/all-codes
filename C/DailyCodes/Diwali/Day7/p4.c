/*	Program 4 : WAP in which make array of character ppointer storing
 *		    addresses of string and print the strings.
*/

#include<stdio.h>

void main(){

	char *str1 = "COEP";
	char *str2 = "PICT";
	char *str3 = "VJTI";

	char *college[10] = {str1, str2, str3};

	for(int i = 0; i < 3; i++){
	
		puts(college[i]);
		printf("-> %p\n", &college[i]);
	}
}
