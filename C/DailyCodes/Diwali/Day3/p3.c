/*	Program 3 : WAP which shows the concept of Pointer to an array
*/

#include<stdio.h>

void main(){

	int arr[5] = {10,20,30,40,50};

	int (*ptr)[5] = &arr;

	printf("%d\n", **ptr);
}
