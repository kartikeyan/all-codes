/*	Program 6 : Take no. of rows from the user.
 *
 *		    - - - d
 *		    - - c c c
 *		    - b b b b b
 *		    a a a a a a a  
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	char ch = 96 + rows;

	for(int i = 1; i <= rows; i++){
	
		for(int space = rows; space > i; space--){
		
			printf("  ");
		}
		
		int num = 4;

		for(int j = 1; j <= 2 * i - 1; j++){
			
			printf("%c ", ch);
		}
		
		ch--;
		printf("\n");
	}
}
