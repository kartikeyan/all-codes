/*	Program 6 : Take no. of rows from the user.
 *
 *		    - - - d
 *		    - - C C C
 *		    - b b b b b
 *		    A A A A A A A   
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	char ch = 64 + rows;

	for(int i = 1; i <= rows; i++){
	
		for(int space = rows; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= 2 * i - 1; j++){
			
			if(i % 2 != 0){
				
				printf("%c ", ch+32);
			}else{
				printf("%c ", ch);
			}
		}
		ch--;
		printf("\n");
	}
}
