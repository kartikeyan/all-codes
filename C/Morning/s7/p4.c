/*	Program 4 : Take no. of rows from the user.
 *
 *		    - - - 4
 *		    - - 3 3 3
 *		    - 2 2 2 2 2
 *		    1 1 1 1 1 1 1  
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);
	
	int num = 4;

	for(int i = 1; i <= rows; i++){
	
		for(int space = rows; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= 2 * i - 1; j++){
			
			printf("%d ", num);
		}

		num--;
		printf("\n");
	}
}
