/*	Program 1 : Take no. of rows from the user
 *
 *		    1 2 3 4 5 6 7
 *		      1 2 3 4 5
 *		        1 2 3
 *		          1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows; i++){
	
		for(int space = 1; space < i; space++){
		
			printf("  ");
		}
		
		int num = 1;

		for(int j = 1; j <= rows * 2 - 2 * i + 1; j++){
		
			printf("%d ", num);
			num++;
		}

		printf("\n");
	}
}
