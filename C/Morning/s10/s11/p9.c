/*	Program 9 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  1
 *			4 2 4
 *		      9 6 3 6 9	
 *	          16 12 8 4 8 12 16
 *	              9 6 3 6 9
 *	                4 2 4
 *	                  1
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols, num1, num2;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		num1 = cols;
		num2 = cols;

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		for(int j = 1; j < cols * 2; j++){
			
			if(j < cols){
				
				printf("%d\t", num1 * num2);
				num2--;
			}else{
	
				printf("%d\t", num1 * num2);
				num2++;
			}
		}

		printf("\n");
	}
}
