/*	Program 10 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *	    	  * * * * * * *
 *		  * * *   * * *
 *		  * *       * *	
 *	          * 	      *
 *	          * *       * *
 *	          * * *   * * * 
 *	          * * * * * * *
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols, cols1, cols2;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			cols1 = rows - i + 1;
			sp = (i - 1);
			cols2 = rows - 2 + i; 
		}else{
		
			cols1 = i + 1 - rows;
			sp = rows - cols1;
			cols2 = rows * 2 - i + 2;
		}

		for(int j = 1; j <= cols1; j++){
		
			printf("*\t");
		}

		for(int k = 1; k <= cols2 * 2; k++){
			
			if(k <= sp * 2 - 1){
				
				printf("\t");
			}else{
	
				printf("*\t");
			}
		}

		printf("\n");
	}
}
