/*	Program 1 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  *
 *			* * *
 *		      * * * * *	
 *	            * * * * * * *
 *	              * * * * *
 *	                * * *
 *	                  *
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		for(int j = 1; j < cols * 2; j++){
		
			printf("*\t");
		}

		printf("\n");
	}
}
