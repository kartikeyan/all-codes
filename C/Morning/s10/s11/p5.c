/*	Program 5 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  4
 *			3 3 3
 *		      2 2 2 2 2	
 *	            1 1 1 1 1 1 1
 *	              2 2 2 2 2
 *	                3 3 3
 *	                  4
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols, num;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
			num = rows - i + 1;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
			num = i - rows + 1;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		for(int j = 1; j < cols * 2; j++){
			
			printf("%d\t", num);
		}

		printf("\n");
	}
}
