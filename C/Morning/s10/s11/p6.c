/*	Program 6 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  A
 *			b b b
 *		      C C C C C	
 *	            d d d d d d d
 *	              C C C C C
 *	                b b b
 *	                  A
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		for(int j = 1; j < cols * 2; j++){
			
			if(i % 2 == 1){
				printf("%c\t", 64+cols);
			}else{
				printf("%c\t", 96+cols);
			}
		}

		printf("\n");
	}
}
