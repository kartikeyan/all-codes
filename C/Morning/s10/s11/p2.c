/*	Program 2 : Take no. of rows from the user.
 *	            
 *	            rows = 4
 *
 *			  1
 *			2 2 2
 *		      3 3 3 3 3	
 *	            4 4 4 4 4 4 4
 *	              3 3 3 3 3
 *	                2 2 2
 *	                  1
 *	                  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int sp, cols;

	for(int i = 1; i < rows * 2; i++){
	
		if(i <= rows){
		
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
		}

		for(int space = 1; space <= sp; space++){
		
			printf("\t");
		}

		for(int j = 1; j < cols * 2; j++){
		
			printf("%d\t", cols);
		}

		printf("\n");
	}
}
