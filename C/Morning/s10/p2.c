/*	Program 2 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *		    	  
 *		    	  1
 *		        1 2
 *		      1 2 3
 *		    1 2 3 4
 *		      1 2 3
 *		        1 2
 *		          1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows : ");
	scanf("%d", &rows);
	
	int space, cols;

	for(int i = 1; i < 2 * rows ; i++){
	
		if(i <= rows){
			
			space = rows - i;
			cols = i;
		}else{
		
			space = i - rows;
			cols = 2 * rows - i;
		}

		for(int sp = 1; sp <= space; sp++){
		
			printf("\t");
		}
		
		int num = 1;

		for(int j = 1; j <= cols; j++){
		
			printf("%d\t", num);
			num++;
		}

		printf("\n");
	}
}
