/*	Program 4 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *		    	  
 *		    	  4
 *		        3 3
 *		      2 2 2
 *		    1 1 1 1
 *		      2 2 2
 *		        3 3
 *		          4
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter rows : ");
	scanf("%d", &rows);
	
	int sp, cols;

	for(int i = 1; i < 2 * rows ; i++){
	
		if(i <= rows){
			
			sp = rows - i;
			cols = i;
		}else{
		
			sp = i - rows;
			cols = 2 * rows - i;
		}

		for(int space = 1; space <= space; space++){
		
			printf("\t");
		}
		
		for(int j = 1; j <= cols; j++){
		
			printf("%d\t", rows:w-cols+1);
		}

		printf("\n");
	}
}
