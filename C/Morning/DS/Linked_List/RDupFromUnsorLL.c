/*	Program 1 
*/


//Approach 1

#include<stdio.h>
#include<stdlib.h>

typedef struct Node{

	int data;
	struct Node *next;
}node;

node *head = NULL;

node *createNode(){

	node *newNode = (node*)malloc(sizeof(node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){
	
	node *newNode = createNode();

	if(head == NULL){
	
		head = newNode;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int printLL(){

	if(head == NULL){
	
		printf("LL is empty\n");
		return -1;
	}else{
	
		node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|n", temp->data);
	}
}

node* unSorted(){

	node *curr = head;

	while(curr != NULL){
		
		node *temp = head;

		while(temp != NULL){
		
			if(curr->data == temp->data){
			
				 node *temp2 = curr->next->next;
				 free(curr->next);
				 curr->next = temp2;
			}else{
			
				temp = temp->next;		// T.C = O(n^2)
								// S.C = O(1)
			}
		}

		curr = curr->next;
	}

	return head;
}

void main(){

	int node;
	printf("Enter Node Count\n");
	scanf("%d", &node);

	for(int i = 1; i<=node; i++){
	
		addNode();
	}

	printLL();

	unSorted();

	printLL();
}

// Approach 2 =   1] Sort LL			
// 		  2] then Remove Diplicates
// 		  3] T.C = O(nlogn)
// 		     S.C = O(1)


