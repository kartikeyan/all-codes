/*	Program 1:
 *
 *
 *	Singly LinKed List
 *
 *	Love Babbar Approach - Appraoch 1
 *
 *	Iterative Solution
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){

		head = newNode;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

struct Node *inPlaceRev(){

	if(head == NULL){
		
		return NULL;
	}else{
	
		if(head->next == NULL){
		
			return head;
		}else{

			struct Node *prev = NULL;
			struct Node *curr = head;
			struct Node *forward = NULL;
				
			while(curr != NULL){			//Time Complexity = O(n)
								//Space Complecity = O(1)
				forward = curr->next;
				curr->next = prev;
				prev = curr;
				curr = forward;
			}

			head = prev;

			return head;
		}
	}
}

int printLL(){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}



void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	struct Node *newNode = inPlaceRev();

	printLL();
}


	
