/*	Program 4:
 *
 *
 *	Singly  Circular LinKed List
 *
 *	In Place Rev
 *
 *	"Same as SinglyLL only in place of head temp3 is there"
 *
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){

		head = newNode;
		newNode->next = head;
	}else{
	
		struct Node *temp = head;

		while(temp->next != head){
		
			temp = temp->next;
		}

		temp->next = newNode;
		newNode->next = head;
	}
}

struct Node *inPlaceRev(){

	if(head == NULL){
		
		return NULL;
	}else{
	
		if(head->next == NULL){
		
			return head;
		}else{

			struct Node *temp1 = NULL;	// Ithe 4 pointer cha garaj lagto head la pakdun,
							// temp3 bagnar head kade, temp2 asnar temp3->next,
							// temp1 bagnar temp3 kade.
			struct Node *temp2 = NULL;

			struct Node *temp3 = head;

			while(temp2 != head){		
			
				temp2 = temp3->next;
				temp3->next = temp1;
				temp1 = temp3;
				temp3 = temp2;
	
			}

			head->next = temp1;
			head = temp1;

			return head;
		}
	}
}

int printLL(){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != head){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}



void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	struct Node *newNode = inPlaceRev();

	printLL();
}


	
