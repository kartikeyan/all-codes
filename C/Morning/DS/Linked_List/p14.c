/*
 *	Find If Singly LinKed List is Palindrome
 *
 *	Approach - 2
 *
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){

		head = newNode;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

int countNodes(){

	int count = 0;

	struct Node *temp = head;

	while(temp != NULL){
	
		count++;
		temp = temp->next;
	}

	return count;
}

struct Node *midNode(){

	if(head == NULL){
	
		printf("LL is empty\n");
	}else{
	
		if(head->next == NULL){
		
			return head->data;
		}else{
		
			struct Node *fastptr = head;
			struct Node *slowptr = head;

			while(fastptr != NULL && fastptr->next != NULL){
			
				fastptr = fastptr->next->next;
				slowptr = slowptr->next;
			}

			return slowptr;
		}
	}	
}

struct Node *inPlaceRev(){

	if(head == NULL){
		
		return NULL;
	}else{
	
		if(head->next == NULL){
				
			return true;
		}else{
		
			struct Node *temp1 = head;
			struct Node *temp2 = head;

			while(head != NULL){
			
				temp2 = head->next;
				head->next = temp1;
				temp1 = head;
				head = temp2;
			}

			head = temp1;

			return head;
		}
	}
}

bool isPalindrome(){

	if(head == NULL){
		
		printf("LL is empty\n");
	}else{
	
		if(head->next == NULL){
		
			return true;
		}else{
			
			struct Node *newNode = midNode();

			inPlaceRev();

				



			return true;
		}
	}
}

int printLL(){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}

void main(){

	char choice;

	do{
		printf("1.addNode\n");
		printf("2.printLL\n");
		printf("3.isPalindrome\n");

		int ch;
		printf("Enter CHoice\n");
		scanf("%d", &ch);

		switch(ch){
		
			case 1:
				addNode();
				break;
			case 2:
				printLL();
				break;
			case 3:
				{
					bool val = isPalindrome();

					if(val == false){
				
						printf("LL is not Palindrome\n");
					}else{
				
						printf("LL is Palindrome\n");
					}
				}
				break;
			default:
				printf("Do u want to continue\n");
		}

		getchar();
		printf("\nDo u want to continue\n");
		scanf("%c", &choice);
	
	}while(choice == 'Y' || choice == 'y');
}


	
