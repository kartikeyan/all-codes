/*	Program 4:
 *
 *
 *	Singly LinKed List
 *
 *	In Place Rev - "Approach 2"	
 *
 * 	PreFer This Approach.
*/

#include<stdio.h>
#include<stdlib.h>

struct Node{

	int data;
	struct Node *next;
};

struct Node *head = NULL;

struct Node *createNode(){

	struct Node *newNode = (struct Node*)malloc(sizeof(struct Node));

	printf("Enter Data\n");
	scanf("%d", &newNode->data);

	newNode->next = NULL;

	return newNode;
}

void addNode(){

	struct Node *newNode = createNode();

	if(head == NULL){

		head = newNode;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			temp = temp->next;
		}

		temp->next = newNode;
	}
}

struct Node *inPlaceRev(){

	if(head == NULL){
		
		return NULL;
	}else{
	
		if(head->next == NULL){
		
			return head;
		}else{

			struct Node *temp1 = NULL;	// three Pointer head la pakdun garaj ahe, 
							// head bagnar pahila node kade, pahila
			struct Node *temp2 = NULL;	// temp1 poniter asnar NULL la, dusra temp2 asnar
							// head->next kade, don pointer cha garaj ahe,
			while(head != NULL){		// karan apan jevha head->next karto, tevha
							// head asnar 2 node kade, tar pahila node 
				temp2 = head->next;	// link jail mahnun temp1 head kade bagh mahntoy,
				head->next = temp1;	// ani temp2 madhe head->next thevtoy.
				temp1 = head;		
				head = temp2;
			}

			head = temp1;

			return head;
		}
	}
}

int printLL(){

	if(head == NULL){
		printf("Invalid\n");
		return -1;
	}else{
	
		struct Node *temp = head;

		while(temp->next != NULL){
		
			printf("|%d|->", temp->data);

			temp = temp->next;
		}

		printf("|%d|\n", temp->data);
	}
}



void main(){

	int nodeCount;
	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
	
		addNode();
	}

	printLL();

	struct Node *newNode = inPlaceRev();

	printLL();
}


	
