/*	PRogram 2 : Take no. of rows from the user
 *		    
 *		    rows = 3
 *
 *		    1 2 3 4 5
 *		      6 7 8
 *		        9
 *		        
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);
	
	int num = 1;

	for(int i = 1; i <= row; i++){
	
		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}

		for(int j = 1; j <= (row - i) * 2 + 1; j++){	 
		
			printf("%d\t", num);
			num++;
		}
		printf("\n");
	}
}

//	(row - i) * 2 + 1 == row * 2 - 2 * i + 1
