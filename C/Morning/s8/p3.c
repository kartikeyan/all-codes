/*	Program 3 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    1 2 3 4 5 6 7
 *		      1 2 3 4 5
 *		        1 2 3
 *		          1
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}

		int num = 1;

		for(int j = 1; j <= row * 2 - 2 * i + 1; j++){
		
			printf("%d\t", num);
			num++;
		}
		printf("\n");
	}
}
