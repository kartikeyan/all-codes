/*	Program 6 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    1 2 3 4 3 2 1
 *		      1 2 3 2 1
 *		        1 2 1
 *		          1
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);


	for(int i = 1; i <= row; i++){

		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}
		
		int num = 1;

		for(int j = 1; j <= (row - i) * 2 + 1; j++){
			
			if(j <= row - i){
		
				printf("%d\t", num);	
				num++;
			}else{
			
				printf("%d\t", num);
				num--;
			}
		}
		printf("\n");
	}
}
