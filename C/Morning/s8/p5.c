/*	Program 5 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    d d d d d d d
 *		      c c c c c
 *		        b b b
 *		          a
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);


	for(int i = 1; i <= row; i++){

		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}

		for(int j = 1; j <= (row - i) * 2 + 1; j++){
		
			printf("%c\t", 97+row-i);	
		}
		printf("\n");
	}
}
