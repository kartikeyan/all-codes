/*	Program 9 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    A0 B1 C2 D3 E4 F5 G6
 *		       H2 I3 J4 K5 L6
 *		          M4 N5 O6
 *		             P6
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);
	
	int num;
	int ch = 1;

	for(int i = 1; i <= row; i++){

		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}
		
		int num = (i - 1) * 2;

		for(int j = 1; j <= (row - i) * 2 + 1; j++){
			
			printf("%c%d\t", 64 + ch, num);
			ch++;
			num++;
		}
		printf("\n");
	}
}
