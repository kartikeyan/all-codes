/*	PRogram 1 : Take no. of rows from the user
 *		    
 *		    rows = 4
 *
 *		    * * * * * * *
 *		      * * * * *
 *		        * * *
 *		          *
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}

		for(int j = 1; j <= row * 2 - 2 * i + 1; j++){
		
			printf("*\t");
		}
		printf("\n");
	}
}
