/*	Program 12 : Take no. of rows from the user
 *		    
 *		    rows = 5
 *
 *		    1 3 5 7 7 5 3 2 1
 *		      9 7 5 3 5 7 9
 *		        3 5 7 5 3
 *		          7 5 7
 *		            5
 *
 *		    Hint Use an extra variable
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){

		for(int sp = 1; sp < i; sp++){
		
			printf("\t");
		}

		for(int j = 1; j <= (row - i) * 2 + 1; j++){
			
			
		}
		printf("\n");
	}
}
