/*	Program 6 :  WAP to find whether the given input character 
 *		     is an alphabet, a digit, or a special character
 *
 *		     Input : *
 *		     Output : * is a special character
*/

#include<stdio.h>

void main(){
	int start;
	printf("Enter start :\n");
	scanf("%d", &start);

	if(start == '*'){
		printf("* is a special character\n",start);
	}
	else if(start == 1 || start == 9){
		printf("%d is a digit", start);
	}
	else if(start == 'a' || start == 'z'){
		printf("%c is a alphabet", start);
	}
}
