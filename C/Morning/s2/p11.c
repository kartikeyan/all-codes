/*	Program 11 : Take no. of rows from the user
 *		    
 *		    D D D D
 *		    C C C C
 *		    B B B B
 *		    A A A A
 *		   
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	char ch = 64 + row;
	for(int i=1; i <= row; i++){
		for(int j = 1; j <= row; j++){
			printf("%c ", ch);
			
		}
		ch--;
		printf("\n");
	}

}
