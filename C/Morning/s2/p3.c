/*	Program 3 : Take no. of rows from the user
 *
 *	1
 *	1 2
 *	1 2 3
 *	1 2 3 4
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	for(int i=1; i <= row; i++){
		int x = 1;
		for(int j = 1; j <= i; j++){
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}

}
