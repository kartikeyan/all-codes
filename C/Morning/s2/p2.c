/*	Program 2 : Take no. of rows from the user
 *
 *	1
 *	2 2
 *	3 3 3
 *	4 4 4 4
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 1;
	for(int i=1; i <= row; i++){
		for(int j = 1; j <= i; j++){
			printf("%d ", x);
		}
		printf("\n");
		x++;
	}

}
