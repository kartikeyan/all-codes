/*	Program 7 : Take no. of rows from the user
 *		    
 *		    1  4  7
 *		    10 13 16
 *		    19 22 25
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 1;
	for(int i=1; i <= row; i++){
		for(int j = 1; j <= row; j++){
			printf("%d ", x);
			x+=3;
		}
		printf("\n");
	}

}
