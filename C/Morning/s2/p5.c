/*	Program 5 : WAP to print the odd numbers as it is
 *		    and cube of even numbers between a given
 *		    renge from the user.
 *		    
 *		    input : Enter start 10
 *		    	    Enter end 15
 *		    output : 1000 11 1728 13 2744 15	    
*/

#include<stdio.h>

void main(){
	int start,end;
	printf("Enter start and end :\n");
	scanf("%d %d", &start, &end);
	
	for(int i = start; i <= end; i++){
		
		if(i % 2 != 0){
			printf("%d ", i);
		}else{
			printf("%d ", i*i*i);
		}
	}
}
