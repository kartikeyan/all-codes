/*	Program 15 : Take no. of rows from the user
 *		    
 *		     1 2 3 4
 *		     a b c d 
 *		     # # # # 
 *		     5 6 7 8
 *		     e f g h 
 *		     # # # # 
 *		   
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 97;
	int y = 1;

	for(int i = 1; i <= row; i++){
		for(int j = 1; j <= 4; j++){
			
			if(i % 3 == 0){
				printf("# ");
			}
			else if(i % 3 == 2){
				printf("%c ", x);
				x++;
			}
			else if(i % 3 == 1){
				printf("%d ", y);
				y++;
			}
		}
		printf("\n");
	}
}

