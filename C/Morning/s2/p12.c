/*	Program 12 : Take no. of rows from the user
 *		    
 *		    9  64 7
 *		    36 5  16
 *		    3  4  2
 *		   
*/

#include<stdio.h>

void main(){
	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 9;
	for(int i=1; i <= row; i++){
		for(int j = 1; j <= row; j++){
			if(j % 2 == 0){
				printf("%d ", x * x);
			}
			else{
				printf("%d ", x);
			}
			x--;	
		}
		printf("\n");
	}

}
