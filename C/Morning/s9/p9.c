/*	Program 9 : Take no. of rows from the user
 *		    
 *		    row = 3
 *
 *		    1
 *		    3 2 1
 *		    5 4 3 2 1
 *		    3 2 1
 *		    1 
 *		     
 *		  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int num;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			num = i;
		}else{
		
			num = 2 * rows - i;
		}

		for(int j = (num-1) * 2 + 1; j >= 1; j--){
		
			printf("%d\t", j);
		}

		printf("\n");
	}
}
