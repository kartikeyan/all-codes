/*	Program 6 : Take no. of rows from the user
 *		    
 *		    row = 3
 *
 *		    1
 *		    1 2 3
 *		    1 2 3 4 5
 *		    1 2 3
 *		    1 
 *		     
 *		  
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int num;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			num = i;
		}else{
		
			num = 2 * rows - i;
		}

		for(int j = 1; j <= (num-1) * 2 + 1; j++){
		
			printf("%d\t", j);
		}

		printf("\n");
	}
}
