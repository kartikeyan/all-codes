/*	Program 10 : Take no. of rows from the user
 *		    
 *		    row = 4
 *
 *		    1
 *		    2 4
 *		    3 6 9
 *		    4 8 12 16
 *		    3 6 9
 *		    2 4
 *		    1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int num;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			num = i;
		}else{
		
			num = 2 * rows - i;
		}

		for(int j = 1; j <= num; j++){
		
			printf("%d\t", num*j);
		}

		printf("\n");
	}
}
