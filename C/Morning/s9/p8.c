/*	Program 8 : Take no. of rows from the user
 *		    
 *		    row = 4
 *
 *		    D
 *		    C D
 *		    B C D
 *		    A B C D
 *		    B C D
 *		    C D
 *		    D
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int num;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			num = i;
		}else{
		
			num = 2 * rows - i;
		}

		for(int j = 1; j <= num; j++){
		
			printf("%c\t", 64+rows-num+j);
		}

		printf("\n");
	}}
