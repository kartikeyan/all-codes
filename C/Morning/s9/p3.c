/*	Program 3 : Take no. of rows from the user
 *		    
 *		    row = 4
 *
 *		    1
 *		    2 1
 *		    3 2 1
 *		    4 3 2 1
 *		    3 2 1
 *		    2 1
 *		    1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int num;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			num = i;
		}else{
		
			num = 2 * rows - i;
		}

		for(int j = 1; j <= num; j++){
		
			printf("%d\t", num-j+1);
		}

		printf("\n");
	}
}
