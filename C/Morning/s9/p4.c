/*	Program 4 : Take no. of rows from the user
 *		    
 *		    row = 4
 *
 *		    4
 *		    3 3
 *		    2 2 2
 *		    1 1 1 1
 *		    2 2 2
 *		    3 3
 *		    4
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int num;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			num = i;
		}else{
		
			num = 2 * rows - i;
		}

		for(int j = 1; j <= num; j++){
		
			printf("%d\t", rows-num+1);
		}

		printf("\n");
	}
}
