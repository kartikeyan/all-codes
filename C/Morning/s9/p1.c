/*	Program 1 : Take no. of rows from the user
 *		    
 *		    row = 4
 *
 *		    1
 *		    1 2
 *		    1 2 3
 *		    1 2 3 4
 *		    1 2 3
 *		    1 2
 *		    1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int col;

	for(int i = 1; i <= 2 * rows - 1; i++){
		
		if(i < rows){
			
			col = i;
		}else{
		
			col = 2 * rows - i;
		}

		int num = 1;

		for(int j = 1; j <= col; j++){
		
			printf("%d\t", num);
			num++;
		}

		printf("\n");
	}
}
