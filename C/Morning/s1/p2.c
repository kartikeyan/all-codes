/*	Program 2 : WAP to print the number divisible by 3 and 7 between given range.
 *
 *	Input : Enter start 10
 *		Enter end 20
 *	Ouput : 
*/

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start and end :\n");
	scanf("%d %d", &start, &end);

	for(int i = start; i <= end; i++){
	
		if(i % 3 == 0 && i % 7 == 0){
			printf("%d ", i);
		}
	}
}
