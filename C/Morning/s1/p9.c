/*	Program 9 : Take no. of rows from the user
 *		    
 *		    A A A
 *		    B B B
 *		    C C C
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 65;

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
			
			printf("%c ", x);
		
		}
		printf("\n");
		x++;
	}
}
