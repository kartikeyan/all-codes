/*	Program 1 : WAP to print the square of odd number between given range
 *
 *	Input : Enter start 10
 *		Enter end 20
 *	Ouput : 121 169 225 289 361
*/

#include<stdio.h>
void main(){
	int start, end;
	printf("Enter start and end :\n");
	scanf("%d %d", &start, &end);

	for(int i = start; i <= end; i++){
	
		if(i % 2 != 0){
			printf("%d\t", i*i);
		}
	}
}
