/*	Program 11 : Take no. of rows from the user
 *		    
 *		    1 2 3 4
 *		    a b c d
 *		    5 6 7 8
 *		    e f g h
 *
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 1;
	int y = 97;

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
			
			if(i % 2 == 0){
				printf("%c ", y);
				y++;
			}else{
				printf("%d ", x);
				x++;
			}
		
		}
		printf("\n");
	}
}
