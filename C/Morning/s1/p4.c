/*	Program 4 : Take no. of rows from the user.
 *		    
 *		    1 2 3 4
 *		    5 6 7 8
 *		    9 10 11 12
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	int x = 1;

	for(int i = 1; i <= row; i++){
	
		for(int j = 1; j <= row; j++){
			
			printf("%d ", x);
			x++;
		}
		printf("\n");
	}
}
