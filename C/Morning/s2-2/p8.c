/*	Program 8 : Take no. of rows from the user.
 *		    
 *		    Rows = 4
 *
 *		    1
 *		    2 4 
 *		    3 6 9 
 *		    4 8 12 16
*/		    
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows:\n");
	scanf("%d", &row);
	
	int x = 1;

	for(int i = 1; i <= row; i++){

		for(int j = 1; j <= i; j++){
			
			printf("%d ", x);
		}
		x+i;
		printf("\n");
	}
}
