/*	Program 7 : Take no. of rows from the user.
 *		    
 *		    Rows = 3
 *
 *		    c
 *		    b b 
 *		    a a a
*/		    
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows:\n");
	scanf("%d", &row);
	
	char ch = 'c';

	for(int i = 1; i <= row; i++){

		for(int j = 1; j <= i; j++){
			
			printf("%c ", ch);
		}
		ch--;
		printf("\n");
	}
}
