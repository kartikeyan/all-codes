/*	Program 9 : Take no. of rows from the user.
 *		    
 *		    Rows = 4
 *
 *		    4
 *		    8  12 
 *		    16 20 24
 *		    28 32 36 40
 *		  
*/		    
#include<stdio.h>

void main(){
	int row;
	printf("Enter rows:\n");
	scanf("%d", &row);
	
	int x = row;

	for(int i = 1; i <= row; i++){

		for(int j = 1; j <= i; j++){
			
			printf("%d ", x);
			x+=row;
		}
		printf("\n");
	}
}
