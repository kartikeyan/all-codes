/*	Program 12 : Take no. of rows from the user
 *
 *	D C B A
 *	C B A
 *	B A
 *	A
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	char ch2 = 64 + row;

	for(int i = 1; i <= row; i++){
		
		char ch2 = ch2--;

		for(int j = row; j >= i; j--){		
		
			printf("%c ", ch2);	
			ch2--;
		}
		printf("\n");
	}
}
