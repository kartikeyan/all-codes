/*	Program 17 : WAP to print the factorial of each number 
 *		     between a given range.
 *
 *		     Input : start : 1
 *		     	     end : 5
 *		     Ouput : Factorial of 1 is 1
 *		     	     Factorial of 2 is 2
 *		     	     Factorial of 3 is 6
 *		     	     Factorial of 4 is 24
 *		     	     Factorial of 5 is 120
*/		     	     
#include<stdio.h>
void main(){

	int start,end;
	printf("Enter start and end :\n");
	scanf("%d %d", &start, &end);

	int Fact = 1;

	for(int i = start; i <= end; i++){
	
//		Fact = Fact * i;

		printf("Factorial of %d is :%d\n ", i , Fact*i);
	}
}
