/*	Program 1 : WAP to write the factorial of a given number.
 *	
 *
*/

#include<stdio.h>

void main(){
	
	int num;
	printf("Enter number :\n");
	scanf("%d", &num);

	int storeFact = 1;

	while(num >= 1){
		
		storeFact = storeFact * num;
		num--;
	}

	printf("%d\n", storeFact);
}

/*	int cha range kiti ahe tyachat basat asel tarch
 *	value return karel, ani int chya range baher jat asel
 *	tar value ramp up hoil.	
 *
 *	for eg - ithe 16 paryant factorial yetat, 
 *	pan 17 cha value ramp hoil, ani negative 
 *	madhe jato.
 *
 *	range of int = -2147483648 to 2147483634
 *
 *	renge of char = -128 to 127
 *
 *	range of long = -9223372036854775808 to 9223372036584775807.
 *
*/	
