/*	Program 13 : Take no. of rows from the user
 *
 *	4 3 2 1
 *	C B A
 *	2 1
 *	A
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	int x = 4;
	char ch = 'C';

	for(int i = 1; i <= row; i++){

		for(int j = row; j >= i; j--){		
			
			if(i % 2 != 0){	
				printf("%d ", x);
				x--;
			}
			else{
				printf("%c ", ch);
				ch--;
			}
		}
		printf("\n");
	}
}
