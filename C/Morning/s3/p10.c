/*	Program 10 : Take no. of rows from the user
 *
 *	d d d d
 *	c c c 
 *	b b 
 *	a
 */

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	char ch = 'd';

	for(int i = 1; i <= row; i++){

		for(int j = row; j >= i; j--){		
		
			printf("%c ", ch);
		} 
		ch--;
		printf("\n");
		
	}
}
