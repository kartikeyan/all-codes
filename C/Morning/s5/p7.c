/*	Program 7 : Take no. of rows from the user.
 *	
 *	Here For rows = 4
 *
 *	- - - 4
 *	- - 4 3
 *	- 4 3 2
 *	4 3 2 1
*/	

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}
		
		int num = row;

		for(int j = 1; j <= i; j++){
			
			printf("%d ", num);
			num--;
		}

		printf("\n");
	}
}
