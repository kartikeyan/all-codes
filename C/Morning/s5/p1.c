/*	 Floyds Triangle
 *	    
 *	    1
 *	    2 3
 *	    4 5 6
 *	    7 8 9 10
*/

/*	Space Patterns 
 *
 *	Program 1 : Take no. of rows from the user.
 *	
 *	For rows = 4;
 *
 *		   1		 - - - 1	
 *	- - -	+  2 3       =	 - - 2 3	      
 *	- -	   4 5 6	 - 4 5 6		
 *	-	   7 8 9 10	 7 8 9 10
*/		    

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter rows :\n");
	scanf("%d", &rows);

	int num = 1;

	for(int i = 1; i <= rows; i++){
	
		for(int space = rows; space > i; space--){
			
			printf("  ");
		}

		for(int j = 1; j <= i; j++){
			
			printf("%d ", num);
			num++;
		}

		printf("\n");
	}
}
