/*	Program 11 : Take no. of rows from the user.
 *	
 *	Here For rows = 5
 *	
 *    -	- - - 5       - - - -       5
 *    -	- - 5 6    =  - - -     +   5 6
 *    -	- 5 4 3       - -           5 4 3
 *    -	5 6 7 8	      -		    5 6 7 8
 *    5 4 3 2 1			    5 4 3 2 1
*/    

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}
		
		int num = row;

		for(int j = 1; j <= i; j++){
		
			if(i % 2 != 0){
				printf("%d ", num);
				num--;
			}else{
				printf("%d ", num);
				num++;
			}
			
		}
		printf("\n");
	}
}
