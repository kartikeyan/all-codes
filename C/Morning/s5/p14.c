/*	Program 14 : Take no. of rows from the user.
 *	
 *	Here For rows = 4
 *	
 *    	- - - 4 	 4	     - - -
 *    	- - 3 6      =   3 6 	+    - -
 *    	- 2 4 6		 2 4 6 	     -
 *    	1 2 3 4	         1 2 3 4
*/    

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);
	
	int x = row;

	for(int i = 1; i <= row; i++){
		
		int y = x;

		for(int space = row; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= i; j++){
			
			printf("%d ", y);
			y=y+x;
			
		}
		printf("\n");
		x--;
	}
}
