/*	Program 13 : Take no. of rows from the user.
 *	
 *	Here For rows = 4
 *	
 *    	- - - 1 	 1	     - - -
 *    	- - A b      =   A b 	+    - -
 *    	- 1 2 3		 1 2 3 	     -
 *    	A b C d	         A b C d
*/    

#include<stdio.h>
void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);
	
	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}
		
		int x = 1;

		char ch = 'A';

		for(int j = 1; j <= i; j++){
		
			if(i % 2 != 0){
				printf("%d ", x);
				x++;
			}else{
				if((i + j) % 2 != 0){
					printf("%c ", ch);
					ch+=2;
				}
				else{
					printf("%c ", ch+31);
				}
			}
			
		}
		printf("\n");
	}
}
