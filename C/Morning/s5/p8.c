/*	Program 8 : Take no. of rows from the user.
 *	
 *	Here For rows = 4
 *
 *	- - - d
 *	- - c c
 *	- b b b
 *	a a a a
*/	

#include<stdio.h>

void main(){

	int row;
	printf("Enter no. of rows:\n");
	scanf("%d", &row);

	char ch = 96 + row;

	for(int i = 1; i <= row; i++){
	
		for(int space = row; space > i; space--){
		
			printf("  ");
		}

		for(int j = 1; j <= i; j++){
			
			printf("%c ", ch);
		}
		ch--;
		printf("\n");
	}
}
