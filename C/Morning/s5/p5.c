/*	Program 5 : Take no. of rows from the user
 *
 *	for rows = 4
 *
 *	- - - 1
 *	- - 1 2 1
 *	- 1 2 3 2 1
 *	1 2 3 4 3 2 1
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter rows:\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
		
		for(int space = row; space > i; space--){
		
			printf("  ");
		}
		
		int num = 1;

		for(int j = 1; j <= 2 * i - 1; j++){
			
			if(j < i){
				
				printf("%d ", num);
				num++;
			}else{
				
				printf("%d ",num);
				num--;
			}	
		}

		printf("\n");
	}
}
