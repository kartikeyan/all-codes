/*	Program 3 : Take no. of rows from the user.
 *
 *	For rows = 4
 *
 *	1 2 3 4	       - 	      1 2 3 4
 *	- 1 2 3   =    - -	+     1 2 3
 *	- - 1 2	       - - -	      1 2
 *	- - - 1			      1
*/

#include<stdio.h>

void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 1; i <= row; i++){
		
		for(int space = 1; space < i; space++){
			
			printf("  ");
		}
		
		int num = 1;

		for(int j = row; j >= i; j--){
			
			printf("%d ", num);
			num++;
		} 

		printf("\n");

	}
}
