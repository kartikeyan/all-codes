/*	Program 3 : Take no. of rows from the user.
 *
 *		    Here rows = 3
 *
 *		   1 2 3 4  =   	     1 2 3 4
 *		     1 2 3	-	+    1 2 3
 *		       1 2	- - 	     1 2
 *		         1	- - -	     1
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		int x = 1;

		for(int j = rows; j >= i; j--){
		
			printf("%d ", x++);
		}

		printf("\n");
	}
}
