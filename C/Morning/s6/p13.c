/*	Program 13 : WAP to print the numbers whose factorial is even.
 *		     Take range from the user.
 *		     Input : Start 1
 *		     	     End 5
 *
 *		     Ouput : 2 3 4 5
*/

#include<stdio.h>

void main(){
	
	int start, end;
	printf("Enter start and end :\n");
	scanf("%d %d", &start, &end);

	int Fact = 1;

	for(int i = start; i <= end; i++){
	
		Fact = Fact * i;

		printf("%d Factorial is : %d\n", i, Fact);
	}

}
