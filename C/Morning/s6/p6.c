/*	Program 6 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		   1 2 3 4  =   	     1 2 3 4
 *		     2 3 4	-	+    2 3 4
 *		       3 4	- - 	     3 4
 *		         4	- - -	     4
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int x = 1;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	
		
		int x = i;

		for(int j = rows; j >= i; j--){
				
			printf("%d ", x);
			x++;

		}

		printf("\n");
	}
}
