/*	Program 4 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		   a B c D  =   	     a B c D
 *		     E f G	-	+    E f G
 *		       h I	- - 	     h I
 *		         J	- - -	     J
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	char ch1 = 'A';
	char ch2 = 'a';

	int c = 0;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = i; j <= rows; j++){
				
			if(j % 2 == 0){
				printf("%c ", ch1+c);
			}else{
				printf("%c ", ch2+c);
			}

			c++;

		}

		printf("\n");
	}
}
