/*	Program 2 : Take no. of rows from the user.
 *
 *		    Here rows = 3
 *
 *		   1 2 3 4
 *		     5 6 7
 *		       8 9
 *		         10
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int x = 1;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i; space++){

			printf("  ");
		}	
		for(int j = rows; j >= i; j--){
		
			printf("%d ", x++);
		}

		printf("\n");
	}
}
