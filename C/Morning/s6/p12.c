/*	Program 12 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		    A b C d        =   		     
 *		      e G i		- 	+    A b C d
 *		        K n	        - -  	     e G i
 *		          q		- - - 	     K n
 *		            	   		     q
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int x = 1;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
			
			
		}

		printf("\n");
	}
}
