/*	Program 7 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		   4 3 2 1  =   	     4 3 2 1
 *		     3 2 1	-	+    3 2 1
 *		       2 1	- - 	     2 1
 *		         1	- - -	     1
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int  x = 4;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
				
			printf("%d ", x);
			x--;

		}
		
		x = rows - i;

		printf("\n");
	}
}
