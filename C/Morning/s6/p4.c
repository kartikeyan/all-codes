/*	Program 4 : Take no. of rows from the user.
 *
 *		    Here rows = 4
 *
 *		   d d d d  =   	     d d d d
 *		     c c c	-	+    c c c
 *		       b b	- - 	     b b
 *		         a	- - -	     a
*/

#include<stdio.h>

void main(){
	
	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	char ch = 96 + rows;

	for(int i = 1; i <= rows; i++){
		
		for(int space = 1; space < i ; space++){

			printf("  ");
		}	

		for(int j = rows; j >= i; j--){
		
			printf("%c ", ch);
		}

		ch--;

		printf("\n");
	}
}
