
/*	PRogram 1 :  Count of Elements
 *
 *		     Given an array A of N integrs.
 *		     Count the number of number of elements that have alteast 1 element greater than itself
 *
 *		     Input : [3,1,2]
 *		     Ouput : 2
*/

#include<stdio.h>

int count(int arr[], int size){
	
/*	int min = arr[0];

	for(int i = 1; i < size; i++){
		
		if(arr[i] < min){
			
			min = arr[i];
		}
	}

	int count = 0;

	for(int i = 0; i < size; i++){
		
		if(arr[i] > min){
			
			count++;
		}
	}

	return count;
*/	
	int count = 0;
	int min = arr[0];

	for(int i = 0; i < size; i++){
		if(arr[i] < min){
			min = arr[i];
		}

		if(arr[i] > min){
			count++;
		}
	}

	return count;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int ret = count(arr, size);
	printf("Count is : %d\n", ret);
}
