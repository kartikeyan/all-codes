
/*	PRogram 2 :  Good Pair
 *
 *		     Given an array A and an integer B.
 *		     A pair(i,j) in the array is a good pair if i != j  and (A[i] + A[j] -- B)
 *		     Check if any good pair exist or not
 *		     Return 1 if good pair exists otherwise return 0
 *
 *		     Input : [1,2,3,4], B = 7
 *		     Ouput : 1
*/

#include<stdio.h>

int pair(int arr[], int size, int target){

	for(int i = 1; i < size; i++){

		for(int j = i + 1; j < size; j++){
			
			if(arr[i] + arr[j] == target){
			
				return 1;
			}
		}
	}
		
	return 0;
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int target;
	printf("Enter target\n");
	scanf("%d", &target);

	int ret = pair(arr, size, target);
	printf("%d\n", ret);
}
