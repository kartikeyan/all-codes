
/*	PRogram 4 :  Reverse the array
 *
 *		     You are given a constant array A
 *		     Your are required to return another array which is reversed form of the input array.
 *		     Return an integer array
 *
 *		     Input : [1,2,3,4]			   Input : [2,5,6]
 *		     Ouput : [4,3,2,1]			   Ouput : [6,5,2]
*/

#include<stdio.h>

int reverse(int arr[], int size){
	
	int start = 0, end = size - 1;

	while(start < end){
		
		int temp = arr[start];
		arr[start] = arr[end];
		arr[end] = temp;

		start++;
		end--;
	}
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	reverse(arr, size);
	
	printf("After Reverse Array elements are\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
