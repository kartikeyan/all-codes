
/*	PRogram 5 :  Array Rotation
 *
 *		     Given an integer array A of size N and an integer B, you have to return
 *		     the same array after rotating it B times towards the right.
 *		     Return the array A after rotating it B times to the right. 
 *
 *		     Input : [1,2,3,4], B = 2	           Input : [2,5,6], B = 1
 *		     Ouput : [3,4,1,2]			   Ouput : [6,2,5]
*/

#include<stdio.h>

int rotate(int arr[], int size, int k){
	
/*	for(int i = 0; i < k; i++){
		
		int last = arr[size];

		for(int j = size; j > 0; j--){
			
			arr[j] = arr[j - 1];
		}

		arr[0] = last;
	}
*/
		
/*	while(k--){
		
		int store = arr[size-1];

		for(int i = 1; i < size; i++){
			
			arr[i] = arr[i-1];
		}
		
		arr[0] = store;
	}
*/

	for(int i = 0; i < k; i++){
		
		int start = arr[0];

		for(int j = 1; j < size; j++){
			
			arr[j-1] = arr[j];
		}

		arr[size-1] = start;
	}
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}
	
	int k;
	printf("Enter K\n");
	scanf("%d", &k);

	rotate(arr, size, k);
	
	printf("After Rotating Array elements are\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
