
/*	PRogram 3 :  Reverse in a Range
 *
 *		     Given an array A of N integrs
 *		     Also given are two integers B and C
 *		     Reverse the array A in the given range[B,C]
 *		     Return the array A after reversing in the given range
 *
 *		     Input : [1,2,3,4], B = 2,  C = 3,     Input : [2,5,6], B = 0,  C = 2
 *		     Ouput : [1,2,4,3]			   Ouput : [6,5,2]
*/

#include<stdio.h>

int pair(int arr[], int size, int start, int end){
	
	while(start < end){
		
		int temp = arr[start];
		arr[start] = arr[end];
		arr[end] = temp;

		start++;
		end--;
	}
}

void main(){
	
	int size;
	printf("Enter Size\n");
	scanf("%d", &size);

	int arr[size];

	printf("Enter Array elements\n");
	for(int i = 0; i < size; i++){
		
		scanf("%d", &arr[i]);
	}

	int start, end;
	printf("Enter Start and end\n");
	scanf("%d %d", &start, &end);

	pair(arr, size, start, end);
	
	printf("After Reverse Array elements are\n");
	for(int i = 0; i < size; i++){
		
		printf("%d\n", arr[i]);
	}
}
