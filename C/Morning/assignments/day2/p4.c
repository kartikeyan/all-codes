
/*	Program 4 : Minor` Diagonal Sum
 *
*/

#include<stdio.h>

int minorDiagonal(int arr[][3], int size){
	
	int sum = 0;

	for(int i = 0; i < size; i++){

		for(int j = 0; j < size; j++){
			
			if(i+j == size-1){

				sum = sum + arr[i][j];
			}
		}
	}

	printf("Sum of minor diagonal is : %d\n", sum);
}

void main(){

	int size = 3;

	int arr[][3] = {{1,-2,-3},{-4,5,-6},{-7,-8,9}};

	minorDiagonal(arr, size);
}
