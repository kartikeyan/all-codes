
/*	Program 8 : Are matrices same
 *
*/

#include<stdio.h>

int same(int arr1[][2], int arr2[][2], int size){
	
	for(int i = 0; i < size; i++){

		for(int j = 0; j < size; j++){
			
			if(arr1[i][j] == arr2[i][j]){
				
				return 1;
			}
		}
	}

	return -1;
}
void main(){
	
	int size = 2;

	int arr1[][2] = {{1,2},{3,4}};

	int arr2[][2] = {{1,2},{3,4}};

	int ret = same(arr1, arr2, size);
	printf("%d\n", ret);
}
