
/*	Program 7 : Rotate Matrix
 *
 *	You are given a N * N 2 D matrix A representing an image.
 *
 *	Rotate the image by 90 degrees (clockwise).
 *
 *	You need to do this in place
 *
 *	Input : 1 2 		Explanation : After rotating the matrix by 90 degrees :
 *		3 4				- 1 goes to 2, 2 goes to 4
 *					        - 4 goes to 3, 3 goes to 1
 *	Ouput : 3 1
 *		4 2			
 *		
*/

#include<stdio.h>

void revMatrix(int arr[][2], int size){

	for(int i = 0; i < size; i++){
		
		int s = 0, e = size - 1, temp = 0;

		while(s < e){
			
			temp = arr[i][s];
			arr[i][s] = arr[i][e];
			arr[i][e] = temp;

			s++;
			e--;
		}
	}

	printf("Reversed Matrix is : \n");
	for(int i = 0; i < size; i++){
		
		for(int j = 0; j < size; j++){
			
			printf(" %d ", arr[i][j]);
		}

		printf("\n");
	}
}

void main(){

	int size = 2;

	int arr[][2] = {{1,2},{3,4}};

	revMatrix(arr,size);
}
