
/*	Program 1 : Row Sum
 *
 *		    You are given a 2D integer matrix A and return a 1D integer array containing row-wise sums
 *		    of the original matrix.
 *
 *		    Return an array containing column-wise sums of the original matrix.
 *
 *		    Input : [1,2,3,4]		Ouput : [10,26,18]
 *		    	    [5,6,7,8]
 *		    	    [9,2,3,4]
 *
*/

#include<stdio.h>

int rows,columns;

int *sum(int arr[rows][columns], int rows, int columns,int arr1[]){

	for(int i = 0; i < rows; i++){

		int sum = 0;

		for(int j = 0; j < columns; j++){

			sum = sum + arr[i][j];
		}

		arr1[i] = sum;
	}

	return arr1;
}
void main(){
	printf("Enter Rows and Columns\n");
	scanf("%d%d", &rows, &columns);
	
	int arr[rows][columns];

	printf("Enter Array Elements\n");
	for(int i = 0; i < rows; i++){
		
		for(int j = 0; j < columns; j++){
			
			scanf("%d", &arr[i][j]);
		}
	}

	printf("Array Element are\n");

	for(int i = 0; i < rows; i++){
		
		for(int j = 0; j < columns; j++){
			
			printf("%d\n", arr[i][j]);
		}
	}
	
	int arr1[columns];

	sum (arr,rows,columns,arr1);

	printf("sum =\n");

	for(int i=0 ; i < rows; i++){

		printf("%d\n",arr1[i]);
	}
}
