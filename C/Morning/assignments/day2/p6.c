
/*	Program 6 : Matrix Transpose
 *
 *	You are given a Matrix A, and you have to return another matrix which is the transpose of A.
 *
 *	You have to return the Transpose of this 2 D Matrix
 *
 *	Input : 1 2 3		Explanation : after converting rows to columns and columns to rows of
 *		4 5 6			      [[1,2,3],[4,5,6],[7,8,9]]
 *		7 8 9 			      we will get [[1,4,7],[2,5,8],[3,6,9]]
 *					      
 *	Ouput : 1 4 7		
 *		2 5 8			 
 *		3 6 9
 *		
*/

#include<stdio.h>

void transposeMatrix(int arr[3][3], int size){

	for(int i = 0; i < size; i++){
		
		int temp = 0;

		for(int j = i + 1; j < size; j++){
			
			temp = arr[j][i];
			arr[j][i] = arr[i][j];
			arr[i][j] = temp;
		}
	}
	
	printf("Transpose Matrix is \n");
	for(int i = 0; i < size; i++){
		
		for(int j = 0; j < size; j++){
			
			printf(" %d ", arr[i][j]);
		}

		printf("\n");
	}
}

void revMatrix(int arr[][3], int size){
		
	transposeMatrix(arr, size);

	for(int i = 0; i < size; i++){
		
		int s = 0, e = size - 1, temp = 0;

		while(s < e){
			
			temp = arr[i][s];
			arr[i][s] = arr[i][e];
			arr[i][e] = temp;

			s++;
			e--;
		}
	}

	printf("Reversed Matrix is : \n");
	for(int i = 0; i < size; i++){
		
		for(int j = 0; j < size; j++){
			
			printf(" %d ", arr[i][j]);
		}

		printf("\n");
	}
}

void main(){

	int size = 3;

	int arr[][3] = {{1,2,3},{5,6,7},{9,10,11}};

	revMatrix(arr,size);
}
