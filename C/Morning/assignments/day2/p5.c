
/*	Program 4 : Anti Diagonal
 *
 *	Given an N * N square matrix A, and return an array of it's anti-diagonals
 *
 *	Return a 2D integer array of size (2 * N - 1) * N, representing the 
 *	anti-diagonals of input array A
 *
 *	The vacant spaces in the grid should be assigned to 0.
 *
 *	Input : 1 2 3		Input : 1 2
 *		4 5 6			3 4
 *		7 8 9 	
 *				Output : 1 0
 *	Ouput : 1 0 0			 2 3
 *		2 4 0			 4 0
 *		3 5 7
 *		6 8 0
 *		9 0 0
*/

#include<stdio.h>

int antiDiagonal(int arr[][4], int size){
	
	int m = (2 * size) - 1;

	for(int i = 0; i < m; i++){

		for(int j = 0; j < size; j++){
			
			for(int k = 0; k < size; k++){

				if((j + k) == i){

					printf("%d ", arr[j][k]);
				}
			}

		}
		printf("\n");
	}
}

void main(){

	int size = 4;

	int arr[][4] = {{1,2,3,4},{5,6,7,8},{9,10,11,12}, {13,14,15,16}};

	antiDiagonal(arr, size);
}
