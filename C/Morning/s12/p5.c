/*	PRogram 5 : Take no. of rows from the user.
 *		    
 *		    rows = 4
 *
 *		    D c B a
 *		    a B c D
 *		    F e D c
 *		    b C d E
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows; i++){
		
		for(int j = 1; j <= rows; j++){
			
			if(i % 2 == 1){
				if(j % 2 == 1){
				
					printf("%c\t", 65 + rows + i - 1 - j);
				}else{
			
					printf("%c\t", 97 + rows + i - 1 - j);
				}
			}else{
			
				if(j % 2 == 1){
				
					printf("%c\t", 96 + j + (i/2) - 1);
				}else{
					
					printf("%c\t", 64 + j + (i/2) - 1);
				}
			}
		}

		printf("\n");
	}
}
