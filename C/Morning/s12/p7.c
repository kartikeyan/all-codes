/*	Program 7 : Take no. of rows from the user.
 *		    
 *		    rows = 3
 *
 * 			    1
 *			3 2 1
 *		    5 4 3 2 1
 *		        3 2 1
 *		            1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int num, sp;

	for(int i = 1; i <= rows * 2 - 1; i++){
		
		if(i <= rows){
		
			num = i;
			sp = rows - i;

		}else{
		
			num = rows * 2 - i;
			sp = i - rows;
		}

		for(int j = 1; j <= sp * 2; j++){
			
			printf("\t");
		}
	

		for(int k = (num-1) * 2 + 1; k >= 1; k--){
		
			printf("%d\t", k);
		}

		printf("\n");
	}
}
