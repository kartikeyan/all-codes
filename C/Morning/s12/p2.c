/*	PRogram 2 : Take no. of rows from the user.
 *		    
 *		    rows = 5
 *
 *		    1
 *		    1 2
 *		    2 3 4
 *		    4 5 6 7
 *		    7 8 9 10 11
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int cols = 1;
	int num = 1;
	int sp;
	
	sp = rows - 1;

	for(int i = 1; i <= rows; ){
	
		if(sp >= 1){
		
			printf("\t");
			sp--;
		}else if(cols <= i){
		
			if(cols % 2 == 1){
				
				printf("%d\t", num);
				num++;
			}else{
			
				printf("%c\t", 96+cols);
			}
			
			cols++;
		}else{
			
			printf("\n");
			i++;
			cols = 1;
			num = 1;
			sp = rows - i;
		}
	}
}
