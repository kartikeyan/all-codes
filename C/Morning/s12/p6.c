/*	Program 6 : Take no. of rows from the user.
 *		    
 *		    rows = 4
 *
 *		    D C B A
 *		      e f g
 *		        F E
 *		          g
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows; i++){
		
		for(int j = 1; j <= i; j++){
			
			printf("\t");
		}
	

		for(int k = 1; k <= rows - i + 1; k++){
		
			if(i % 2 == 1){
			
				printf("%c\t", 64 + rows + i - k);
			}else{
			
				printf("%c\t", 96 + rows + k + i - 2);
			}
		}

		printf("\n");
	}
}
