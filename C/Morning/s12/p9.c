/*	Program 9 : Take no. of rows from the user.
 *		    
 *		    rows = 4
 *
 * 		    g 6 e 4 c 2 a
 * 		      5 d 3 b 1
 * 		        c 2 a
 * 		          1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows; i++){
		
		for(int j = 1; j < i; j++){
		
			printf("\t");
		}

		for(int k = (rows - i) * 2 + 1; k >= 1; k--){
		
			if(k % 2 == 1){
			
				printf("%c\t", 96+k);
			}else{
			
				printf("%d\t", k);
			}
		}
		
		printf("\n");
	}
}
