/*	PRogram 4 : Take no. of rows from the user.
 *		    
 *		    rows = 5
 *
 *		    5 D 3 B 1
 *		    D 3 B 1
 *		    3 B 1
 *		    B 1
 *		    1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	for(int i = 1; i <= rows + 1; i++){
		
		for(int j = rows + 2 - i; j >= 1; j--){
		
			if(j % 2 == 1){
			
				printf("%d\t", j);
			}else{
			
				printf("%c\t", 64+j);
			}
		}

		printf("\n");
	}
}
