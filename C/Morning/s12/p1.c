/*	PRogram 1 : Take no. of rows from the user.
 *		    
 *		    rows = 5
 *
 *		    1
 *		    1 2
 *		    2 3 4
 *		    4 5 6 7
 *		    7 8 9 10 11
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);

	int cols = 1;
	int num = 1;

	for(int i = 1; i <= rows; ){
	
		if(cols <= i){
		
			printf("%d\t", num);
			num++;
			cols++;
		}else{
		
			printf("\n");
			cols = 1;
			num = num - 1;
			i++;
		}
	}
}
