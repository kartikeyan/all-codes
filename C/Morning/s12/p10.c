/*	Program 9 : Take no. of rows from the user.
 *		    
 *		    rows = 4
 *
 *			    1
 *			  4 2 4
 *		       9  6 3 6 9
 * 		    16 12 8 4 8 12 16
 * 		        9 6 3 6 9
 * 		          4 2 4
 * 		            1
*/

#include<stdio.h>

void main(){

	int rows;
	printf("Enter no. of rows :\n");
	scanf("%d", &rows);
	
	int num, temp = 0, sp, cols;

	for(int i = 1; i < rows * 2; i++){
		
		if(i < rows){
		
			sp = rows - i;
			cols = i;
			num = cols;
			temp = num;
		}else{
		
			sp = i - rows;
			cols = rows * 2 - i;
			num = cols;
			temp = num;
		}

		for(int j = 1; j < sp; j++){
		
			printf("\t");
		}

		for(int k = 1; k <= cols * 2 - 1; k++){
		
			if(k <= cols){
			
				printf("%d\t", num * (num-k+1));
		
			}else{
			
				printf("%d\t", num * temp);
				num++;
			}
		}
		
		printf("\n");
	}
}
