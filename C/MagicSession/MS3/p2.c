/*	Program 2 : WAP that accepts number from user separate digits from that number
 *		    and enter them in an array then sort the array in ascending array
 *
 *	Input : enter number : 942111423
 *
 *	Ouput : 1 | 1 | 1 | 2 | 2 | 3 | 4 | 4 | 9
*/

#include<stdio.h>

void main(){

	int size;
	printf("Enter size :\n");
	scanf("%d\n", &size);

	int arr[size];
	for(int i = 0; i < size; i++){
	
		scanf("%d", &arr[i]);
	}

	int temp;

	for(int i = 0; i < size; i++){
	
		for(int j = i+1; j < size; i++){
		
			if(arr[i] > arr[j]){
				
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}

	for(int i = 0; i < size; i++){
	
		printf("%d\n", arr[i]);
	}
}
