/*	Program 1 : Write the output of the following code.
*/

#include<stdio.h>

int fun(int);

void main(){

	int s;
 	s = fun(10);
	printf("%d\n", s);
}

int fun(int s){
	
	s > 20 ? return(9) : return(20);	
}


/*	error : expected expression before return.
 *		
 *		s > 20 ? return(9) : return(20);
 *			 ^
*/

/*	because a ternary operation is an expression
 *	and you can't use statements in expressions.
 *
 *	Correct way
 *
 * 	return s > 20 ? 9 : 20;	
*/
