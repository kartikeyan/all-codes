/*	PRogram 13 : WAP to print different datatype of elements using a void pointer.
 *		     int, char, float, double.
*/

#include<stdio.h>
void main(){

	int x = 10;
	char ch = 'A';
	float f = 20.35;
	double d = 11.221341;

	void *arr[4] = {&x, &ch, &f, &d};

	printf("%d\n", *(int *)(arr[0]));

	printf("%c\n", *(char *)(arr[1]));

	printf("%f\n", *(float *)(arr[2]));

	printf("%lf\n", *(double *)(arr[3]));

}
