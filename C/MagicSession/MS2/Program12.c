/*	Program 12 : WAP to print addition of both diaginal elements using a pointer
 *		     without repeating middle element.
*/

#include<stdio.h>
void main(){

	int row,col;
	printf("Enter row,col :\n");
	scanf("%d %d", &row, &col);

	int arr[row][col];

	for(int i = 0; i < row; i++){
	
		for(int j = 0; j < col; j++){
		
			scanf("%d", &arr[i][j]);
		}
	}
	
	int sum = 0;
	
	for(int i = 0; i < row; i++){
		for(int j = 0; j < col; j++){
		/*	if(i == j){
				sum = sum + arr[i][j];
			}
		*/	
			
			if((i + j) % 2 == 0){
				sum = sum + arr[i][j];
			}
		}
	}
	printf("%d", sum);
}
