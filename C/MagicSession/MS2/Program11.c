/*	Program 11 : WAP to print 2nd largest element in the array.
 *		     Print using Pointer.
*/

#include<stdio.h>
void main(){

	int size;
	printf("Enter Array size :\n");
	scanf("%d", &size);

	int arr[size];

	for(int i = 0; i < size; i++){
		scanf("%d", &arr[i]);
	}
	
	int temp;

	for(int i = 0; i < size; i++){
		
		for(int j = i+1; j < size; j++){
			
			if(arr[i] > arr[j]){
				
				temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}

	printf("%d\n", *(arr + (size - 2)));
}
