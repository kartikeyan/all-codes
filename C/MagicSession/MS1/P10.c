/*	Program 10 : Take no. of rows from the user
 *		     
 *		     D D D D
 *		       C C C
 *		         B B
 *		           A
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);
	int ch=64+row;

	for(int i = 0; i < row; i++){
	
		for(int sp = 0; sp < i; sp++){
			
			printf("- ");
		}

		for(int k = 0; k < row-i; k++){
		
			printf("%c ", ch);
			
		}
		ch--;

		printf("\n");
	}
}
