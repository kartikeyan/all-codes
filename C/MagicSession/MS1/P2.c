/*	Program 2 : WAP to count digits in given no.
*/

#include<stdio.h>
void main(){

	int x;
	printf("Enter number :\n");
	scanf("%d", &x);

	int count = 0;

	while(x > 0){
		
		x = x / 10;
		count++;
	}

	printf("digit count is :%d\n", count);
}
