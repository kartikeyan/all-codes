/*	Program 6 : Take no. of rows from the user
 *		    
 *		    D e F g
 *		    e D c B
 *		    F g H i
 *		    g F e D
*/

#include<stdio.h>
void main(){

	int row;
	printf("Enter rows :\n");
	scanf("%d", &row);

	for(int i = 0;  i<row; i++){

		for(int j = 0; j < row; j++){
		
			if(i%2 == 0){
				if((i+j)%2==0) {
					printf("%c\t",(64+row)+j);
				}else{
					printf("%c\t",(96+row)+j);
				
				}
				
			}else {
				if((i+j)%2!=0) {
					printf("%c\t",(96+row+i)-j);
				}else {
					printf("%c\t",(64+row+i)-j);
					
					
				}		
			}
		}
			
			printf("\n");
			
	}
}
