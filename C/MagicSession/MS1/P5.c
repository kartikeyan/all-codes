/*	Program 5 : Take no. of rows from the user
 *		    
 *		    2  3  5
 *		    7  11 13
 *		    17 19 23
*/

#include<stdio.h>
void main(){

	int row;
	int n = 2;
	printf("Enter rows :\n");
	scanf("%d", &row);
	
	for (int i = 0; i<row; i++) {
	
		for (int j = 0; j<row;) {
		
			int count = 0;

			for (int k = 1; k<n; k++) {
			
				if(n%k==0) {
					count++;
				}
			}
			if(count==1) {
				printf("%d\t",n);
				j++;
			}
		
			n++;
		}
		printf("\n");
			
	}	
}
	
