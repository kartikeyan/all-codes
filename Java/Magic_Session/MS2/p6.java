
/*	
 *	WAP and take two characters if these characters are equal then print them as it is but,
 *	if they are unequla then print their difference.
 *	(Note : consider position differences NOt ASCIIs)
 *
 *	Input : a p
 *	Ouput : the difference bet a and p is 15
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter char1");

		char ch1 = br.readLine().charAt(0);
		
		System.out.println("Enter char2");

		char ch2 = br.readLine().charAt(0);

		if(ch1 == ch2){
			
			System.out.println("THey are equal");
		}else{
			
			if(ch1 > ch2){
				
				System.out.println(ch1 - ch2);
			}else{
				System.out.println(ch2 - ch1);
			}
		}
	}
}
