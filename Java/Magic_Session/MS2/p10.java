
/*	
 *	WAP to print a series of prime no. from entered range.
 *
 *	Input : start = 10, end = 100
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");

		int start = Integer.parseInt(br.readLine());	

		System.out.println("Enter End");

		int end = Integer.parseInt(br.readLine());	
		
		for(int i = start; i <= end; i++){
			
			int count = 0;

			for(int j = 1; j <= i; j++){
				
				if(i % j == 0){
					
					count++;
				}
			}

			if(count == 2){
				
				System.out.println(i);
			}
		}
	}
}
