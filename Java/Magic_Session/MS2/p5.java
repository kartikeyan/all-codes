
/*	
 *	0
 *	1 1
 *	2 3 5
 *	8 13 21 34
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Rows");

		int row = Integer.parseInt(br.readLine());
		
		System.out.println("Enter Prev");
		int prev = Integer.parseInt(br.readLine());
		
		System.out.println("Enter Curr");
		int curr = Integer.parseInt(br.readLine());
	
		int next;

		for(int i = 1; i <= row; i++){

			for(int j = 1; j <= i; j++){
				
				System.out.print(prev + " ");
				next = prev + curr;
				prev = curr;
				curr = next;
			}

			System.out.println();
		}
	}
}
