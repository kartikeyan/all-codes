
/*	
 *	WAP to take number as input and print addition of factorials of each digit from that number.
 *
 *	Input : 1234
 *	Ouput : 33
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Num");

		int n = Integer.parseInt(br.readLine());	
		
		int sum = 0;

		while(n != 0){
			
			int rem = n % 10;

			int prod = 1;

			for(int i = 1; i <= rem; i++){
				
				prod = prod * i;
			}

			sum = sum + prod;

			n = n / 10;
		}

		System.out.println(sum);
	}
}
