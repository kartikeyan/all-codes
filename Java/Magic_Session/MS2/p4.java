
/*	
 *	WAp to print all even numbers in reverse order and odd numbers in the standard way.
 *	Both separately within a range.Take the start and end from the user.
 *
 *	Input : Enter Start number = 2
 *		Enter End number = 9
 *
 *	Ouput : 
 *		8 6 4 2
 *		3 5 7 9
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");

		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End");

		int end = Integer.parseInt(br.readLine());
		
		System.out.println("Even Reverse numbers");

		for(int i = end; i >= start; i--){
			
			if(i % 2 == 0){
				
				System.out.print(i + " ");
			}
		}
		
		System.out.println();

		System.out.println("Odd numbers");

		for(int i = start; i <= end; i++){
				
			if(i % 2 == 1){
				
				System.out.print(i + " ");
			}
		}

		System.out.println();
	}
}
