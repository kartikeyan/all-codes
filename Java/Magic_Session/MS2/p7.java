
/*	
 *	row = 5				row = 4
 *
 *	O				10
 *	14 13				I  H
 *	L  K  J				7  6  5 
 *	9  8  7  6			D  C  B  A
 *	E  D  C  B  A
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Rows");

		int row = Integer.parseInt(br.readLine());
		
		int n  = row * (row + 1) / 2;
		int ch = 64 + n;
		
		if(row % 2 == 0){

			for(int i = 1; i <= row; i++){

				for(int j = 1; j <= i; j++){
					
					if(i % 2 == 1){
					
						System.out.print(n + " ");
					}else{
						System.out.print((char)ch + " ");
					}

					n--;
					ch--;
				}

				System.out.println();
			}
		}else{
			
			for(int i = 1; i <= row; i++){

				for(int j = 1; j <= i; j++){
					
					if(i % 2 == 0){
					
						System.out.print(n + " ");
					}else{
						System.out.print((char)ch + " ");
					}

					n--;
					ch--;
				}

				System.out.println();
			}
		}
	}
}
