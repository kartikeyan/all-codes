
class assign{
	
	public static void main(String[] args){
		
		int i = 1, N = 10, sum = 0, prod = 1;

		while(i <= N){

			if(i % 2 == 0){
				
				sum = sum + i;
			}else{
				
				prod = prod * i;
			}

			i++;
		}

		System.out.println(sum);
		System.out.println(prod);
	}
}
