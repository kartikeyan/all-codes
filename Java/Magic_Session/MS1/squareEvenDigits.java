
class assign{
	
	public static void main(String[] args){
		
		int i = 342,  rem;

		while(i != 0){
			
			rem = i % 10;

			if(rem % 2 == 0){
				
				System.out.println(rem * rem);
			}

			i = i / 10;
		}
	}
}
