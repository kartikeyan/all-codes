
class assign{
	
	public static void main(String[] args){
		
		int i = 75757, count = 0, rem;

		while(i != 0){
			
			rem = i % 10;

			if(rem % 2 == 1){
				count++;
			}
			i = i / 10;
		}

		System.out.println(count);
	}
}
