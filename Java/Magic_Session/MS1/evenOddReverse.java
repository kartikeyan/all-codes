
/*	If No is even print that no. in reverse order or if 
 *	no. is odd print that no. in reverse order by diff.
 *	of 2
 *
 *	i/p : 6			i/p : 7
 *	o/p : 6 5 4 3 2 1	o/p : 7 5 3 1
*/	

class assign{
	
	public static void main(String[] args){
		
		int i = 7;
	
		if(i % 2 == 0){
			
			while(i != 0){
				
				System.out.println(i);
				i--;
			}
		}else{
			while(i > 0){
				System.out.println(i);
				i -= 2;
			}
		}
	}
}
