
class Core2Web{

	public static void main(String args[]){
		
		int arr[5] = {1,2,3,4,5};
		
		for(int va : arr)
			System.out.println(va);
	}
}

/*	
 *	While instantiating arrays in Java the size should
 *	not be mentioned it generates a syntax error	
