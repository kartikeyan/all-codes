
class Core2Web{

	public static void main(String args[]){
		
		int arr[];

		arr = new int[4];

		arr[2] = 420;
		
		for(int va : arr)
			System.out.println(va);
	}
}

/*	
 *	When an array is declared all the elements are initialized
 *	to zero. Only the element at index 2 is reinitialized to 420.
 *	Array elements are then displayed using for each loop.
*/


