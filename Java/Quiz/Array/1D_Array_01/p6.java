
class Demo{
	
	public static void main(String[] args){
		
		int []var1 = new int[2];

		var1[0] = 0;
		var1[1] = 1;

		System.out.println(var1[0] + " " + var1[1] + " " + var1.length + var1[2]);
		
	}
}

/*	
 	Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 2 out of bounds for length 2
	at Demo.main(p6.java:11)
*/
