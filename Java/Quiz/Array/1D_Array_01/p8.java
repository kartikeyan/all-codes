
class Demo{
	
	public static void main(String[] args){
		
		int []arr;
		arr = {1,2,3,4};

		for(int var2 = 0; var2 < arr.length; var2++){

			System.out.println(arr[var2]);
		}
	}
}

/*	
 	An Array cannot be initialized as shown because the compiler will raise error.This way the array can be initialized but both declaration and assignment should be done on the same line.
 
 	p8.java:7: error: illegal start of expression
		arr = {1,2,3,4};
		      ^
	p8.java:7: error: not a statement
		arr = {1,2,3,4};
		       ^
	p8.java:7: error: ';' expected
		arr = {1,2,3,4};
		        ^
	3 errors

*/
