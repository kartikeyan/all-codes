
class Demo{
	
	public static void main(String[] args){
		
		int var1[] = new int[];

		var1[0] = 0;
		var1[1] = 1;

		System.out.println(var1[0] + " " + var1[1] + " " + var1.length);

	}
}

/*	During creation the array needs dimension or initialization, both are missing in this case which leads to compile time error
*/

/*	
 	p3.java:6: error: array dimension missing
		int var1[] = new int[];
		                      ^
	1 error
*/
