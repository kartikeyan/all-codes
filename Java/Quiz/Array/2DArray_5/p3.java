
class Core2Web{
	
	public static void main(String[] s){
		
		int arrInt[][] = {{1,2},{3,4},{4,4}};

		System.out.println(arrInt[1][2]);
	}
}

/*	
 	Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 2 out of bounds for length 2
	at Core2Web.main(p3.java:8)
	
	The column index '2' in [1][2] is illegal, only [1][0] and [1][1]
	are accessible in row index 1. [1][2] suggests that there is a 
	3rd element
*/	
