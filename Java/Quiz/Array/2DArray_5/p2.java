
class Core2Web{
	
	public static void main(String[] s){
		
		int arrInt[][];

		arrInt = new int[2][2];

		arrInt[0] = {0,0};
		arrInt[1] = {1,1};

		System.out.println(arrInt[1][1]);
	}
}

//	Invalid way to initialize an array

/*	
 	p2.java:10: error: illegal start of expression
		arrInt[0] = {0,0};
		            ^
	p2.java:10: error: not a statement
		arrInt[0] = {0,0};
		             ^
	p2.java:10: error: ';' expected
		arrInt[0] = {0,0};
		              ^
	p2.java:11: error: illegal start of expression
		arrInt[1] = {1,1};
		            ^
	p2.java:11: error: not a statement
		arrInt[1] = {1,1};
		             ^
	p2.java:11: error: ';' expected
		arrInt[1] = {1,1};
		              ^
	6 errors
*/	
