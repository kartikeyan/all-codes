
class Core2web{
	
	public static void main(String[] s){
		
		short[][][] arr = new short[][][] {{{},{},{}}, {{},{},{}}};

		System.out.println(arr.length);

		System.out.println(arr[0].length);

		System.out.println(arr[0][0].length);
	}
}

/*
 	arr.length refers to number of 2D arrays in the array i.e 2

	arr[0].length refers to number of rows in 0th 2D array i.e 3

	arr[0][0].length refers to no. of elements in 0th row of 0th 2d array i.e 0
*/
