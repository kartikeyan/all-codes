
class Core2Web{
	
	public static void main(String[] args){

		System.out.println(args[0]);
	}
}

//	input :> java Core2Web &

/*
 
 	katikeyan@kartikeyan:~/javacodes/Quiz/Array/CommandLineArgument_3$ Exception in thread "main" java.lang.ArrayIndexOutOfBoundsException: Index 0 out of bounds for length 0
	at Core2Web.main(p5.java:6)

*/

/*
 	Special characters like & have a meaning in the shell(terminal)

	When & is given at the command line, the meaning of & in the shell is, to run the command or task in the background And terminal doesnot wait for the task to finish.

	So "&" becomes meaningful for the shell and never reaches String[] args(array).

	And we try to access a value at the index, where nothing is stored.

	Thus we get ArrayIndexOutOfBoundsException
*/	
