
class Core2Web{
	
	public static void main(String[] s){

		String s1 = new String("Core2Web");

		String s2 = new String("Core2Web");
		
		if(s1 == s2){
			System.out.println("True 1 ");
		}
		if(s1.equals(s2)){
			System.out.println("True 2 ");
		}
		if(s1.hashCode() == s2.hashCode()){
			System.out.println("True 3 ");
		}
		
		System.out.println(s1.hashCode());

		System.out.println(s2.hashCode());

		System.out.println(System.identityHashCode(s1));

		System.out.println(System.identityHashCode(s2));
	}
}

/*	
	True 2 
	True 3 
*/

/*
 	Java Integer hashCode() Method

	The hashCode() method is a Java Integer class method which returns the hash code for the given inputs. There are two different types of Java hashCode() method which can be differentiated depending on its parameter.

	These are:

	    1]Java Integer hashCode() Method
	    2]Java Integer hashCode(int value) Method

	hashCode() Method

	The hashCode() is a method of Java Integer Class which determines the hash code for a given Integer. It overrides hashCode in class Object. By default, this method returns a random integer that is unique for each instance.
hashCode(int value) Method

	The hashCode(int value) is an inbuilt Java Integer Class method which determines a hash code for a given int value. This method is compatible with Integer.hashCode().
	
	Syntax:

	Following is the declaration of hashCode() method:

	   1] public int hashCode()
	   2] public static int hashCode(int value)


	Parameter:

	DataType 	Parameter 	Description
	int 	         value 	      It is an int value which determines the hash code.

	Returns:
	Method 			Returns
	hashCode() 		It returns a hash code value for this object, equal to the primitive int value represented by this Integer object.
	hashCode(int value) 	It returns a hash code value for an int value specified in the method argument.

	Exceptions:

	InputMismatchException, NumberFormatException

	Compatibility Version:

	hashCode()
		Java 1.2 and above
	hashCode(int value)
		Java 1.8 and above
*/
