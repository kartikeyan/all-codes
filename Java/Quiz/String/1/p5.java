
class Core2Web{
	
	public static void main(String[] s){

		String str1 = new String("java");

		String str2 = "java";

		System.out.println(str1 == str2);
	}
}

/*	
 	false

	string literal goes to SCP and string created by new creates new object
	on heap, == operator checks address not the value at address
*/

