
class Core2Web{
	
	public static void main(String[] s){

		String var1 = "This is a String";
		String var2 = "This is a String";

		if(System.identityHashCode(var1) == System.identityHashCode(var2)){

			System.out.println("Equal");
		}else{
			System.out.println("Not Equal");
		}
	}
}

/*	
 	Both Strings have same identity HashCode since
	marked to same object in String Constant Pool
*/

