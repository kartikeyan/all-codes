
class Core2Web{
	
	public static void main(String[] s){

		String str1 = "This is a String";

		String str2 = "a String";

		String str3 = str1 + str2;
	
		if(System.identityHashCode(str1) == System.identityHashCode(str3)){

			System.out.println("Equal");
		}else{		
			System.out.println("Not Equal");
		}
	}
}

/*	
 	Not Equal

	Operations on string creates new object
*/

