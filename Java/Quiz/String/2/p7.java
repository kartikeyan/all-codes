
class Core2Web{

	public static void main(String[] s){
		
		StringBuffer var1 = new StringBuffer("shashi");

		StringBuffer var2 = new StringBuffer("shashi");

		System.out.println(var1.equals(var2));

		var1.setLength(3);

		System.out.println(var1);
		
	}
}	

//	false
//	sha

/*
 	equals() method of stringBuffer is not over-riddent as of string, and it compares
	actual objects and the var1 and var2 are different objects therefore equals() returns false.

	setLength() limits the length of the StringBuffer
*/	
