class Core2Web{

	public static void main(String[] s){
		
		String var1 = new String("Shashi");

		String var2 = var1.reverse();
			
		System.out.println(var1);
		
		System.out.println(var2);
	}
}

/*
 	p2.java:7: error: cannot find symbol
		String var2 = var1.reverse();
		                  ^
	  symbol:   method reverse()
	  location: variable var1 of type String
	1 error
*/

//	String class does not have reverse() method
