class Core2Web{

	public static void main(String[] s){
		
		StringBuffer var1 = new StringBuffer("Shashi");

		StringBuffer var2 = new StringBuffer("Shashi");
			
		if(var2.equals(var1)){

			System.out.println("Both are equal");
		}else{

			System.out.println("Both are not equal");
		}
	}
}

/*	Here though char sequence is same in both but they are objects of different class,
 *	therefore not equal. To check char sequence contentEquals() method is used
*/		
