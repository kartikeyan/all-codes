
class Core2Web{

	public static void main(String[] s){
		
		StringBuffer sb = new StringBuffer();

		for(int i = 0; i < 18; i++){
			
			sb.append(i);
		}

		System.out.println(sb.length());

		System.out.println(sb.capacity());
	}
}	

//	26
//	34

/*
 	StringBuffer initial capacity = 16, when more than 16 characters are to be appended,
	capacity = (capacity + 1) * 2 ==> (16 + 1) * 2 == 34

	StringBuffer is mutable therefore append() doesn't return new object so it need to be caught but it adds
	in the same object and therefore change is reflected. And here are some 2 digit nos therefore they have 2 characters
	in them and therefore length = 26
*/	
