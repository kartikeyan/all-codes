
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str = new StringBuffer(400);

		str.append(400);

		str.setLength(str.length());

		System.out.println(str.capacity());
	}
}

/*
 	400
*/

/*
 	StringBuffer object can be initialized using Integer parameter which defines capacity.
	In StringBuffer append method also has a single integer parameter which will be converted
	to String using value of method of String.
	Change in length of Stringbuffer will not change the capacity.
*/
