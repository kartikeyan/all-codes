
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str = new StringBuffer(new String(new char[] {'D','B'}));

		str.append("MySql");

		System.out.println(str);
	}
}

/*
 	DBMySql
*/

/*
 	StringBuffer object can alse be made by using temporary object of character array(new char[]{}).

	In above case StringBuffer object will contain "DB" and appending "MySql" to 
	it gives the answer
*/
