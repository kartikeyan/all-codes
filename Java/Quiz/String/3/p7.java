
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str = new StringBuffer(400);

		str.append("Java_Developement_Kit");

		str.trimToSize();
		str.setLength(10);

		System.out.println(str.capacity());

		System.out.println(str.length());
	}
}

/*
 	20
	10
*/

/*
 	trimToSize method trims the capacity of the StringBuffer upto the length of String stored in it.
	setLength method trims the content in the StringBuffer upto the Integer parameter, but Change
	in length gives no change in capacity.
*/
