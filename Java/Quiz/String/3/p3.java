
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str = new StringBuffer("James Gosling");

		System.out.println(str.replace('J','G'));
	}
}

/*
 	p3.java:8: error: method replace in class StringBuffer cannot be applied to given types;
		System.out.println(str.replace('J','G'));
		                      ^
  	required: int,int,String
  	found:    char,char
  	reason: actual and formal argument lists differ in length
	1 error
*/

//	Storing StringBuffer in String gives compile time error as incompatible types
