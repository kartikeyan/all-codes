
class Core2Web{
	
	public static void main(String[] args){
		
		String sbvar1 = "Core2Web";

		StringBuffer sbvar2 = new StringBuffer("Core2Web");

		if(sbvar1.equals(sbvar2)){

			System.out.println("Equal");
		}else{

			System.out.println("Not Equal");
		}
	}
}

/*
 	Not Equal
*/

/*
 	equals method checks the hashCode of the two objects, but if the two objects are of
	different class then their hashCode will also be different.
*/
