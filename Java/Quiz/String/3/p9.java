
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer sbvar1 = new StringBuffer("Core2Web");

		long lvar1 = System.identityHashCode(sbvar1);

		sbvar1.append("technologies");

		long lvar2 = System.identityHashCode(sbvar1);

		if(lvar1 == lvar2){

			System.out.println("True");
		}else{

			System.out.println("false");
		}
	}
}

/*
 	True
*/

/*
 	StringBuffer is mutable which means we can do changes in single StringBuffer object and
	these changes will not result in change in identityHashCode.

	So System.identityHashCode will remain same before and after the change in single StringBuffer 
	object
*/
