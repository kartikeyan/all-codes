
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str1 = new StringBuffer("DEMO");

		StringBuffer str2 = new StringBuffer();

		str2 = str2.append("DEMO");

		System.out.println(str1.capacity() == str2.capacity());
	}
}

/*
 	false
*/

/*
 	str1 initialization is done with the string "Demo" results in capacity 
	of 20(default 16 + String length 4).

	But str2 is initilized without giving any parameter so it's capacity will be 16,
	now append method will be called on runtime and the appended string DEMO will not
	change the capacity of the str2.

	Hence 20 == 16, returns false
*/
