
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer var1 = "Core2Web";

		System.out.println(var1.intern());
	}
}

/*
 	20
	10
*/

/*
 	StringBuffer cannot be initialized by directly passing String value as 
	like String Class
*/
