
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str = new StringBuffer("AndroidOne");

		str.delete(7,10);

		str.insert(str.length(), true);

		System.out.println(str);
	}
}

/*
 	Androidtrue
*/

/*
 	delete method deletes the content in the StringBuffer object within the range.
	In above code "One" will be deleted so the length becomes 7. Now, inserting String
	at the last + 1 th index results the answer
*/	
