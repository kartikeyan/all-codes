
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuffer str = new StringBuffer();

		str.ensureCapacity(10);

		System.out.println(str.capacity());
	}
}

/*
 	16
*/

/*
 	ensureCapacity method checks the capacity is less than the given integer parameter.
	If yes then change the capacity otherwise the capacity is not changed
*/
