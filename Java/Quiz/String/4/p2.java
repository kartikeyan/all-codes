
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder strBuilder = new StringBuilder("String");

		StringBuilder strBuffer = new StringBuilder(strBuilder);

		String str = new String(strBuffer);
	
		System.out.println(str.replace('t','p'));

		System.out.println(str);
	}
}

/*	Spring
	String
*/

/*
 	Every Change in String gives new Object which should be stored in the given object otherwise
	it will discarded and the String object on which the method is called remains unchanged
*/
