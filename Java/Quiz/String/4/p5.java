
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder str = new StringBuilder();

		str.append("Java_Virtual_Machine");

		str.setCharAt(str.charAt(14), 'z');

		System.out.println(str);
	}
}

/*	
 	Exception in thread "main" java.lang.StringIndexOutOfBoundsException: Index 97 out of bounds for length 20
	at java.base/jdk.internal.util.Preconditions$1.apply(Preconditions.java:55)
	at java.base/jdk.internal.util.Preconditions$1.apply(Preconditions.java:52)
	at java.base/jdk.internal.util.Preconditions$4.apply(Preconditions.java:213)
	at java.base/jdk.internal.util.Preconditions$4.apply(Preconditions.java:210)
	at java.base/jdk.internal.util.Preconditions.outOfBounds(Preconditions.java:98)
	at java.base/jdk.internal.util.Preconditions.outOfBoundsCheckIndex(Preconditions.java:106)
	at java.base/jdk.internal.util.Preconditions.checkIndex(Preconditions.java:302)
	at java.base/java.lang.String.checkIndex(String.java:4570)
	at java.base/java.lang.AbstractStringBuilder.setCharAt(AbstractStringBuilder.java:536)
	at java.base/java.lang.StringBuilder.setCharAt(StringBuilder.java:91)
	at Core2Web.main(p5.java:10)
*/

/*
 	charAt method replaces its places with the returned character which is "a".
	So the method call changes to str.setChar('a','z').
	But parameter required to method setCharAt is int,char.
	So the 'a' replaces with its ASCII value i.e 97,
	but index 97 is index out of range in StringBuilder object
*/
