
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder str1 = new StringBuilder("Shashi");

		str1.insert(6,"t");

		System.out.println(str1);
		
	}
}

/*
 	Shashit
*/

/*
 	As the string "Shashi" contains 6 characters i.e index from 0 to 5 so insert method
	inserts string "t" at the end of the given string "Shashi"
*/
