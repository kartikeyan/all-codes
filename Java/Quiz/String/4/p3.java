
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder str = new StringBuilder("Builder");

		str.insert(5, new String("0"));

		str.reverse();

		System.out.println(str.reverse());
	}
}

/*	
 	Build0er
*/

/*
 	Unlike string, the change made in Stringbuilder object will not gives the new object but results
	change in existing object.Reversing an object even times gives the same unchanged object
*/
