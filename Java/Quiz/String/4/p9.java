
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder str1 = new StringBuilder("Shashi");

		str1.replace(0, 1, "P");

		System.out.println(str1);
		
	}
}

/*
 	Phashi
*/

/*
 	replace (int start, int end, String str) method replaces the string in StringBuilder object
	within the range start to end and with the string str.In above scenario "P" will replace
	the string "S" in String "Shashi"
*/
