
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder str1 = new StringBuilder("Hey");

		StringBuilder str2 = new StringBuilder("Hey");

		if(str1.equals(str2)){

			System.out.println("Equal");
		}else{
			System.out.println("Not Equal");
		}
	}
}

/*
 	Not Equal
*/

/*
 	equals method uses hashCode(not identityHashCode) to check the object.

	In case of hashCode is same if content of two string is same, but this is not true
	in case of StringBuilder.
*/
