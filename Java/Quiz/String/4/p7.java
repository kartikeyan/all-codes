
class Core2Web{
	
	public static void main(String[] args){
		
		StringBuilder str1 = new StringBuilder(10);

		System.out.println(str1);
		
	}
}

/*
 	No error, No output
*/

/*
 	new StringBuilder(10), creates a null StringBuilder object having capacity 10.
	Though its capacity is 10, there is nothing to print in it and it is also
	syntactically correct.
*/
