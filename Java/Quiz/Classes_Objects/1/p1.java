public class Core2Web{
	
	int var1 = 10;

	public static void main(String[] args){
		
		Core2Web c2w = new Core2Web();

		System.out.println(++c2w.var1);
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ vim p1.java 
	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ javac p1.java 
	p1.java:2: error: class Core2Web is public, should be declared in a file named Core2Web.java
	public class Core2Web{
	       ^
	1 error
*/

/*
 	When we write a class "public" then the .java file containing the class should have a name
	as Classname.java

	So here instead of P1.java, the filename must be Core2Web.java
*/
