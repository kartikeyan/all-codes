
public class Core2Web_2{
	
	int var1 = 10;

	public static void main(String[] args){
		
		Core2Web c2w = new Core2Web();

		System.out.println(++c2w.var1);
	}
}

class Main{
	
	public static void main(String[] args){
		
		System.out.println("In Main");

		Core2Web c2w = new Core2Web();

		System.out.println(++c2w.var1);
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ javac Core2Web.java
	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ java Main 
	In Main
	11
*/

/*
 	Here both classes have main() method, therefore both classes are executable, so after
	java whichever class is given, code from that particular class's main only gets executed
*/
