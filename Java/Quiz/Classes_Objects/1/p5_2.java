
class Main{

	public static void main(String[] args){
		
		Core2Web c2w = new Core2Web();

		System.out.println(c2w.var1);
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ javac p5_1.java 
	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ javac p5_2.java 
	katikeyan@kartikeyan:~/javacodes/Quiz/Classes_Objects/1$ java Main 
	10
*/

/*
 	There is no compile time error in any of these files as the Main class accessing Core2Web class
	Which has accessibility as default and default things can be accessed in the same directory
*/	
