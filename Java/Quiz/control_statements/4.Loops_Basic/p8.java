
class assign{
	
	public static void main(String[] args){
		
		int val = 10;

		while(val > 0){
			
			System.out.println(val);
			val--;	
			if(val < 5)
				break;
		}
	}
}

/*	o:p - 10
 *	       9
 *	       8
 *	       7
 *	       6
 *	       5
*/		
