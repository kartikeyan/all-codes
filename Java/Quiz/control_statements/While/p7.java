
class Core2web{
	
	public static void main(String[] s){
		
		int var = 5;

		System.out.println("Inside main");

		while(var > 5);{

			System.out.println("Inside while");
		}
	}
}

/*	
 *	o:p = Inside main and Inside while
 *	
 *	Expla : Here while is an empty statement, due to semicolon(;) given after the while statement.
 *		So the block after that is normal block with no association with "while"
*/		
