
class Core2web{
	
	public static void main(String[] s){
		
		int var = 5;

		System.out.println("Inside main");

		while(var > 3);{

			System.out.println("Inside while");
		}
	}
}

/*	
 *	o:p = Inside main and (code goes into infinite loop with no output)
 *	
 *	Expla : Here while is an empty statement, but the condition given with it is true so this unexpexted behaviour happens
*/		
