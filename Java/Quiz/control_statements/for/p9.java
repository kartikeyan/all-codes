
class assign{
	
	public static void main(String[] args){
		
		System.out.println("Before for loop");

		for(int i, j; i < 3; i++){
			
			System.out.println("Inside for loop");
		}
		

		System.out.println("After for loop");
	}
}

/*	
 *	error : variable might not have been initialized
 *
 *	Here both loop variables i and j are not initialized, but
 *	we are only accessing the value of i.So the error is only for i
*/		
