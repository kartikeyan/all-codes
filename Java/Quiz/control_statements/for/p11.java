
class assign{
	
	public static void main(String[] args){
		
		int var1 = 5;

		for(int i = 1; i <= var1; i++)
			
			int x = var1 / i;
		
	}
}

/*	
 *	error : variable declaration not allowed here 
 *
 *	As we know, if curly braces are not given then there is noly one statement
 *	is considered in the loop, so if we declare a varible at that line then it
 *	is no use as it cannot be used
*/		
