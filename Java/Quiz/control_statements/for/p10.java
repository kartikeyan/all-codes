
class assign{
	
	public static void main(String[] args){

		for(int var1 = 10, float var2 = 40.25f; var1 < 15; var1++, var2++){
			
			if(var2 % 2 == 0){
				System.out.println(var1);
			}
		}
	}
}

/*	
 *	error 
 *
 *	In a loop, we cannot initialize variables of different data types.
 *
 *	It gives multiple errors for syntax
*/		
