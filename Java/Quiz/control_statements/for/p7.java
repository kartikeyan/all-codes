
class assign{
	
	public static void main(String[] args){

		for(int val = 10; System.out.println(val); val++){
			
			if(val == 12)
				System.out.println("After for loop");
		}
	}
}

/*	
 *	error : incompatible types : void cannot converted to boolean
 *
 *	In the for loop, the conditional statement must be a boolean 
 *	value.
*/		
