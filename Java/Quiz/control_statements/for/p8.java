
class assign{
	
	public static void main(String[] args){
		
		System.out.println("Before for loop");

		for(int i = 0, j = 0; i < 1; j++){
			
			System.out.println("Inside for loop");
		}

		System.out.println("After for loop");
	}
}

/*	
 *	Infinite loop
 *
 *	As we are not changing the value of i, the loop
 *	condition will never become false resulting
 *	in an infinite loop
*/		
