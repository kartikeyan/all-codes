
class assign{
	
	public static void main(String[] args){
		
		System.out.println("Before for loop");

		for(int i = 1, j = 1; j < 3; i++,j++){
			
			System.out.println("Inside for");	
		}

		System.out.println("After for loop");
	}
}

/*	
 *	o / p : Before for loop
 *		Inside for
 *		Inside for
 *		After for loop
*/		
