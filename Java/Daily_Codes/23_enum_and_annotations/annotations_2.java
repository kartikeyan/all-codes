
/*
 	Annotations are used to provide supplemental information about a program. 

	    Annotations start with ‘@’.
	    Annotations do not change the action of a compiled program.
	    Annotations help to associate metadata (information) to the program elements i.e. instance variables, constructors, methods, classes, etc.
	    Annotations are not pure comments as they can change the way a program is treated by the compiler. See below code for example.
	    Annotations basically are used to provide additional information, so could be an alternative to XML and Java marker interfaces.

	package : java.lang.annotation.Annotation
*/

class Parent{

	void m1(){
		
		System.out.println("Parent-m1");
	}
}

class Child extends Parent{
	
	@Override
	void m1(int x){
		
		System.out.println("Child-m1");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent p = new Child();

		p.m1();
	}
}

/*
 	annotations_2.java:24: error: method does not override or implement a method from a supertype
	@Override
	^
	1 error
*/
