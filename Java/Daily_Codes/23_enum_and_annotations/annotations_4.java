
/*
 	Annotations are used to provide supplemental information about a program. 

	    Annotations start with ‘@’.
	    Annotations do not change the action of a compiled program.
	    Annotations help to associate metadata (information) to the program elements i.e. instance variables, constructors, methods, classes, etc.
	    Annotations are not pure comments as they can change the way a program is treated by the compiler. See below code for example.
	    Annotations basically are used to provide additional information, so could be an alternative to XML and Java marker interfaces.

	package : java.lang.annotation.Annotation
*/

class Parent{
	
	@Deprecated
	void m1(){		// in dependency classes if m1() is used then gives a warning that u are using deprecated method
		
		System.out.println("Parent-m1");
	}
}

class Child extends Parent{
		
	void m1(int x){
		
		System.out.println("Child-m1");
	}
}

class Client{
	
	@SuppressWarnings("Hello")
	public static void main(String[] args){
		
		Parent p = new Child();

		p.m1();
	}
}

/*
 	Note: annotations_3.java uses or overrides a deprecated API.
	Note: Recompile with -Xlint:deprecation for details.
*/
