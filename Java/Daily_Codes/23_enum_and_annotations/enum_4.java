/*
 	Compiled from "Enum.java"
	public abstract class java.lang.Enum<E extends java.lang.Enum<E>> implements java.lang.constant.Constable, java.lang.Comparable<E>, java.io.Serializable {
	  public final java.lang.String name();
	  public final int ordinal();
	  protected java.lang.Enum(java.lang.String, int);
	  public java.lang.String toString();
	  public final boolean equals(java.lang.Object);
	  public final int hashCode();
	  protected final java.lang.Object clone() throws java.lang.CloneNotSupportedException;
	  public final int compareTo(E);
	  public final java.lang.Class<E> getDeclaringClass();
	  public final java.util.Optional<java.lang.Enum$EnumDesc<E>> describeConstable();
	  public static <T extends java.lang.Enum<T>> T valueOf(java.lang.Class<T>, java.lang.String);
	  protected final void finalize();
	  public int compareTo(java.lang.Object);
	}
*/

/*
 	1] String data integer value ne represent karto

	2] enum is also a class
*/

enum LineUp{
	
	Rohit,Gill,Virat,KL,Ishan,Hardik;

	public static void main(String[] args){
		
		LineUp player = LineUp.Virat;

		switch(player){
			
			case Rohit:
				System.out.println("Sajdeh");
				break;
			case Gill:
				System.out.println("Sara");
				break;
			case Virat:
				System.out.println("Sharma");
				break;
			case KL:
				System.out.println("Shetty");
				break;
			case Ishan:
				System.out.println("Hundia");
				break;
			case Hardik:
				System.out.println("Natasa");
				break;
			default:
				System.out.println("Afridi");
		}
	}
}

//	Sharma
