/*
 	Compiled from "Enum.java"
	public abstract class java.lang.Enum<E extends java.lang.Enum<E>> implements java.lang.constant.Constable, java.lang.Comparable<E>, java.io.Serializable {
	  public final java.lang.String name();
	  public final int ordinal();
	  protected java.lang.Enum(java.lang.String, int);
	  public java.lang.String toString();
	  public final boolean equals(java.lang.Object);
	  public final int hashCode();
	  protected final java.lang.Object clone() throws java.lang.CloneNotSupportedException;
	  public final int compareTo(E);
	  public final java.lang.Class<E> getDeclaringClass();
	  public final java.util.Optional<java.lang.Enum$EnumDesc<E>> describeConstable();
	  public static <T extends java.lang.Enum<T>> T valueOf(java.lang.Class<T>, java.lang.String);
	  protected final void finalize();
	  public int compareTo(java.lang.Object);
	}
*/

/*
 	1] String data integer value ne represent karto

	2] enum is also a class
*/

enum ProgLang{
	
	C,CPP,Java,Python;	// these all are objects
}

/*
 	Findings from bytecode :-

	1] enum is converted into class

	2] C, CPP, Java, Python are objects

	3] These objects are kept in ProgLang[] array using values() method

	4] ProgLang parent class is abstract class Enum

	5] In LProgLang L stands for class

	6] These objects are stored as String
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/23_enum_and_annotations$ javap -c ProgLang.class 
	Compiled from "enum_3.java"
	final class ProgLang extends java.lang.Enum<ProgLang> {
	  public static final ProgLang C;
	
	  public static final ProgLang CPP;

	  public static final ProgLang Java;

	  public static final ProgLang Python;

	  public static ProgLang[] values();
	    Code:
	       0: getstatic     #1                  // Field $VALUES:[LProgLang;
	       3: invokevirtual #2                  // Method "[LProgLang;".clone:()Ljava/lang/Object;
	       6: checkcast     #3                  // class "[LProgLang;"
	       9: areturn

	  public static ProgLang valueOf(java.lang.String);
	    Code:
	       0: ldc           #4                  // class ProgLang
	       2: aload_0
	       3: invokestatic  #5                  // Method java/lang/Enum.valueOf:(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;
	       6: checkcast     #4                  // class ProgLang
	       9: areturn

	  static {};
	    Code:
	       0: new           #4                  // class ProgLang
	       3: dup
	       4: ldc           #7                  // String C
	       6: iconst_0
	       7: invokespecial #8                  // Method "<init>":(Ljava/lang/String;I)V
	      10: putstatic     #9                  // Field C:LProgLang;
	      13: new           #4                  // class ProgLang
	      16: dup
	      17: ldc           #10                 // String CPP
	      19: iconst_1
	      20: invokespecial #8                  // Method "<init>":(Ljava/lang/String;I)V
	      23: putstatic     #11                 // Field CPP:LProgLang;
	      26: new           #4                  // class ProgLang
	      29: dup
	      30: ldc           #12                 // String Java
	      32: iconst_2
	      33: invokespecial #8                  // Method "<init>":(Ljava/lang/String;I)V
	      36: putstatic     #13                 // Field Java:LProgLang;
	      39: new           #4                  // class ProgLang
	      42: dup
	      43: ldc           #14                 // String Python
	      45: iconst_3
	      46: invokespecial #8                  // Method "<init>":(Ljava/lang/String;I)V
	      49: putstatic     #15                 // Field Python:LProgLang;
	      52: iconst_4
	      53: anewarray     #4                  // class ProgLang
	      56: dup
	      57: iconst_0
	      58: getstatic     #9                  // Field C:LProgLang;
	      61: aastore
	      62: dup
	      63: iconst_1
	      64: getstatic     #11                 // Field CPP:LProgLang;
	      67: aastore
	      68: dup
	      69: iconst_2
	      70: getstatic     #13                 // Field Java:LProgLang;
	      73: aastore
	      74: dup
	      75: iconst_3
	      76: getstatic     #15                 // Field Python:LProgLang;
	      79: aastore
	      80: putstatic     #1                  // Field $VALUES:[LProgLang;
	      83: return
*/

