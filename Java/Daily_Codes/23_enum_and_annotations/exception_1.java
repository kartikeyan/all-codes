
class Demo extends Exception{
	
	Demo(String str){
		
		super(str);
	}
}

class Parent{
	
	Parent(){
		System.out.println("In Parent Constructor");
	}
	void m1(){
		System.out.println("In Parent-m1");

		throw new Demo("Exception thrown");
	}
}

class Child extends Parent{
	
	Child(){
		System.out.println("Child Constructor");
	}

	void m1(){
		System.out.println("In Child m1");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent p = new Child();

		p.m1();
	}
}

/*
 	exception_1.java:18: error: unreported exception Demo; must be caught or declared to be thrown
		throw new Demo("Exception thrown");
		^
	1 error
*/
