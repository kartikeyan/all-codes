
class Player{
	
	private int jerNo = 0;

	private String name = null;

	Player(int jNo, String pName){		// Player(Player this, int jNo, String pName)
						
					// Company cha protocol asa ahe ki, Player constructor che parameter ani 
					// instance variable che naav same asayla pahije.
		jerNo = jNo;
		name = pName;
	}

	void info(){			// info(Player this)
		
		System.out.println(jerNo + " = " + name);
	}
}

class Client{
	
	public static void main(String[] s){
		
		Player obj1 = new Player(18, "Virat");		// Player(obj1, 18, "Virat")
		obj1.info();					// info(obj1)

		Player obj2 = new Player(7, "MSD");		// Player(obj2, 7, "MSD")
		obj2.info();					// info(obj2)

		Player obj3 = new Player(45, "Rohit");		// Player(obj3, 45, "Rohit")
		obj3.info();					// info(obj3)
	}
}

/*
 	18 = Virat
	7 = MSD
	45 = Rohit
*/
