
//	Using Setter-Getter Method to change private Instance variables data

class Player{
	
	private int jerNo = 0;

	private String name = null;

/*	Player(int jerNo, String name){		// Company cha protocol asa ahe ki, Player constructor che parameter ani
						// instance variable che naav same asayla pahije.
		this.jerNo = jerNo;	
		this.name = name;			
	}
*/
	void info(int jerNo, String name){	// info(Player this, int jerNo, String name)
		
		this.jerNo = jerNo;
		this.name = name;

		System.out.println(jerNo + " = " + name);
	}
}

class Client{
	
	public static void main(String[] s){
		
		Player obj1 = new Player();		// Player(obj1)
		obj1.info(18, "Virat");			// info(obj1, 18, "Virat")

		Player obj2 = new Player();
		obj2.info(7, "MSD");

		Player obj3 = new Player();
		obj3.info(45, "Rohit");
	}
}

/*
 	18 = Virat
	7 = MSD
	45 = Rohit
*/
