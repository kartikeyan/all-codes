
class Demo{

	int x = 10;

	Demo(){
		
		System.out.println("In No-Arg Const");
	}

	Demo(int x){
		
		this();

		System.out.println("In Para Const");
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo(50);
	}
}

/*
 	In No-Arg Const
	In Para Const
*/

/*
 	Compiled from "this_2.java"
class Demo {
  int x;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #7                  // Field x:I
      10: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      13: ldc           #19                 // String In No-Arg Const
      15: invokevirtual #21                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      18: return

  Demo(int);
    Code:
       0: aload_0
       1: invokespecial #27                 // Method "<init>":()V
       4: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
       7: ldc           #28                 // String In Para Const
       9: invokevirtual #21                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      12: return

  public static void main(java.lang.String[]);
    Code:
       0: new           #8                  // class Demo
       3: dup
       4: bipush        50
       6: invokespecial #30                 // Method "<init>":(I)V
       9: astore_1
      10: return
}

*/
