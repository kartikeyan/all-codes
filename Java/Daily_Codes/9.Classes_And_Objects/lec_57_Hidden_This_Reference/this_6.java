
class Demo{

	int x = 10;
	
	Demo(){
		
		System.out.println("In No-Args Const");
	}

	Demo(int x){

		super();

		this();
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo(50);
	}
}

/*
 	this_6.java:15: error: call to this must be first statement in constructor
		this();
		    ^
	1 error
*/

