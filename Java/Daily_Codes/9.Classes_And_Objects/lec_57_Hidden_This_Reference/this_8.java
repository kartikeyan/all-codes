
class Demo{

	int x = 10;
	
	Demo(){
		
		super();
		System.out.println("In No-Args Const");
	}

	Demo(int x){

		this();
		System.out.println("In Para Const");
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo(10);
	}
}

/*
 	In No-Args Const
	In Para Const
*/

