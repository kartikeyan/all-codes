
class Demo{

	int x = 10;
	
	Demo(){
		
		System.out.println("In No-Args Const");
	}

	Demo(int x){
		
		this();

		super();
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo(50);
	}
}

/*
 	this_5.java:15: error: call to super must be first statement in constructor
		super();
		     ^
	1 error
*/

