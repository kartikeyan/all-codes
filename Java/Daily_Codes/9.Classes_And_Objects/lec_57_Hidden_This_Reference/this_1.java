
class Demo{
	
	int x = 10;

	Demo(){					// Demo(Demo this);
		
		System.out.println("In No-Args Constructor");
	}

	Demo(int x){				// Demo(Demo this, int x);
		
		System.out.println("In Para Constructor");
	}
	
	public static void main(String[] s){
	
		Demo obj1 = new Demo();		// Demo(obj1);

		Demo obj2 = new Demo(50);	// Demo(obj2);
	}
}

/*
 	In No-Args Constructor
	In Para Constructor
*/

/*	Q] Ithe x kuthe initialize hoil?
 *	A] 	
 *		1] Bytecode bagitla tar doganmadhe initialize hoil.
 *		2] Runtime la jyacha object banel tyachyat initialize hoil.
 *		3] obj1 constructor Demo() madhe x initialize hoil.
 *		4] obj2 constructor Demo(int x) madhe x initialize hoil.
*/	

/*
 	Compiled from "this_1.java"

class Demo {
  int x;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #7                  // Field x:I
      10: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      13: ldc           #19                 // String In No-Args Constructor
      15: invokevirtual #21                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      18: return

  Demo(int);
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #7                  // Field x:I
      10: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      13: ldc           #27                 // String In Para Constructor
      15: invokevirtual #21                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      18: return

  public static void main(java.lang.String[]);
    Code:
       0: new           #8                  // class Demo
       3: dup
       4: invokespecial #29                 // Method "<init>":()V
       7: astore_1
       8: new           #8                  // class Demo
      11: dup
      12: bipush        50
      14: invokespecial #30                 // Method "<init>":(I)V
      17: astore_2
      18: return
}
*/
