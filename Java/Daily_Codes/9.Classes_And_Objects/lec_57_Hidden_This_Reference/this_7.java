
class Demo{

	int x = 10;
	
	Demo(){
		
		this(10);
		System.out.println("In No-Args Const");
	}

	Demo(int x){

		super();
		System.out.println("In Para Const");
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo();
	}
}

/*
 	In Para Const
	In No-Args Const
*/

