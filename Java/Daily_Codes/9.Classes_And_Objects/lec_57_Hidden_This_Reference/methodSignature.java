
/*	Method Signature madhe method cha naav ani tyache parameters che naav astat
 *
 *	Method Signature in java is defined as the structure of a method that is designed by the programmer. 
 *	Method Signature is the combination of a method's name and its parameter list. 
 *	A class cannot have two methods with the same signature. 
 *	If we declare two methods with the same signature, compilation error is thrown.
 *	Method signature does not include the return type of a method.
*/

class Demo{
	
	int x = 10;

	Demo(){
		
		System.out.println("In Const. 1");
		System.out.println("X is : " + x);
	}

	Demo(){
		
		System.out.println("In Const. 1");
		System.out.println("X is : " + x);
	}
}

/*
 	methodSignature.java:12: error: constructor Demo() is already defined in class Demo
	Demo(){
	^
	1 error
*/
