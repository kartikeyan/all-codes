
class Demo{

	int x = 10;

	Demo(){
		
		System.out.println("In No-Arg Const");
	}

	Demo(int x){

		System.out.println("In Para Const");

		this();
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo(50);
	}
}

/*
 	this_3.java:15: error: call to this must be first statement in constructor
		this();
		    ^
	1 error
*/

