
class Demo{

	int x = 10;

	Demo(){
		
		this(70);
	}

	Demo(int x){

		this();
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo(50);
	}
}

/*
 	this_4.java:8: error: recursive constructor invocation
		this(70);
		^
	1 error
*/

