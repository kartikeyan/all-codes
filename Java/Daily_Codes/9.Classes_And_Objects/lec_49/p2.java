
class Demo{

	int x = 10;
	int y = 20;

	void display(){
		
		System.out.println(x);	// 10
		System.out.println(y);	// 20
	}
	
	public static void main(String[] s){
		
		Demo obj = new Demo();

		obj.display();
	}
}
