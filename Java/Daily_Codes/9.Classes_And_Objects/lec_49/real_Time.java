
class ONDC{

	float funding = 48.5f;

	void directRivals(){
			
		System.out.println("Rival 1 : Swiggy");
		System.out.println("Rival 2 : Zomato");
	}
	
	void backedBy(){
		
		System.out.println("Backed by : Government");
	}

	public static void main(String[] s){
		
		ONDC obj = new ONDC();
		
		System.out.println("Total Funding taken : " + obj.funding + " Cr");

		obj.backedBy();

		obj.directRivals();
	}
}


