
//	This cha parameter asto Class name

class Demo{
	
	Demo(Demo xyz){
		
		System.out.println("Para Demo");
	}

	Demo(int x){
		
		System.out.println("Args");
	}

	Demo(){
		
		System.out.println("No Args");
	}

	public static void main(String[] s){
		
		Demo obj1 = new Demo();		

		Demo obj2 = new Demo(10);	

		Demo obj3 = new Demo(obj1);	
	}
}

/*
 	No Args
	Args
	Para Demo
*/
