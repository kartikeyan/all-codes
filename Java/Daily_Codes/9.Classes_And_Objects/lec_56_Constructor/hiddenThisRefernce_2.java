
//	This cha parameter asto class name

class Demo{
		
	int x = 10;

	Demo(){							// Demo (Demo this)
			
		System.out.println("In Constructor");
		
		System.out.println("This is " + this);

		System.out.println("This.x is " + this.x);

	}

	void fun(){						// fun (Demo this)
			
		System.out.println("x is " + x);

		System.out.println("This for fun is " + this);

		System.out.println("This.x for fun is " + this.x);
	}
	
	public static void main(String[] args){
		
		Demo obj = new Demo();				// Demo(obj)

		System.out.println(obj);

		obj.fun();					// fun(obj)
	}
}

/*
 	In Constructor
	This is Demo@372f7a8d
	This.x is 10
	Demo@372f7a8d
	x is 10
	This for fun is Demo@372f7a8d
	This.x for fun is 10
*/
