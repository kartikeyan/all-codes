
//	This cha parameter asto class name

class Demo{
		
	int x = 10;

	Demo(){							// Demo (Demo this)
			
		System.out.println("In Constructor");
	}

	void fun(){						// fun (Demo this)
			
		System.out.println("x is " + x);
	}
	
	public static void main(String[] args){
		
		Demo obj = new Demo();				// Demo(obj)

		System.out.println(obj);

		obj.fun();					// fun(obj)
	}
}

/*
 	In Constructor
	Demo@6ff3c5b5
	x is 10
*/
