
class Employee{
	
	int empId = 1;				// Instance variable pratyak object sathi vegala asto
	String empName = "harshal";		

	void empInfo(){
		
		System.out.println("EmpId is = " + empId);

		System.out.println("EmpName is = " + empName);
	}
}

class MainDemo{
	
	public static void main(String[] s){
		
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.empInfo();
		emp2.empInfo();

		System.out.println("After Change");

		emp2.empId = 2;
		emp2.empName = "Shree";

		emp1.empInfo();
		emp2.empInfo();
	}
}

/*
 	EmpId is = 1
	EmpName is = harshal
	EmpId is = 1
	EmpName is = harshal
	After Change
	EmpId is = 1
	EmpName is = harshal
	EmpId is = 2
	EmpName is = Shree
*/
