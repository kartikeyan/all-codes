
class Employee{
	
	int empId = 1;
	String empName = "harshal";

	void empInfo(){
		
		System.out.println("EmpId is = " + empId);

		System.out.println("EmpName is = " + empName);
	}
}

class MainDemo{
	
	public static void main(String[] s){
		
		Employee emp = new Employee();

		emp.empInfo();

		System.out.println(emp.empId);

		System.out.println(emp.empName);

	}
}

/*
 	EmpId is = 1
	EmpName is = harshal
	1
	harshal
*/
