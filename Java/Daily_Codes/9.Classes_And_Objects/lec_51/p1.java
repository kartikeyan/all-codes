
class Demo{
	
	int x = 10;
	private int y = 20;

	void fun(){
		
		System.out.println(x);

		System.out.println(y);
	}
}

class MainDemo{
	
	public static void main(String[] s){

		Demo obj = new Demo();

		obj.fun();

		System.out.println(obj.x);

		System.out.println(obj.y);

		System.out.println(x);

		System.out.println(y);

	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/5$ javac p1.java
	p1.java:25: error: y has private access in Demo
		System.out.println(obj.y);
		                      ^
	p1.java:27: error: cannot find symbol
		System.out.println(x);
		                   ^
	  symbol:   variable x
	  location: class MainDemo
	p1.java:29: error: cannot find symbol
		System.out.println(y);
		                   ^
	  symbol:   variable y
	  location: class MainDemo
	3 errors
*/
