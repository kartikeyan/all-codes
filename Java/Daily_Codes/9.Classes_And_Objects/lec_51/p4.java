
class Employee{
	
	int empId = 1;				// Instance variable pratyak object sathi vegala asto
	String empName = "harshal";		
	
	static int y = 5;			// static variable saglyan sathic common asto

	void empInfo(){
		
		System.out.println("EmpId is = " + empId);

		System.out.println("EmpName is = " + empName);

		System.out.println("y is = " + y);
	}
}

class MainDemo{
	
	public static void main(String[] s){
		
		Employee emp1 = new Employee();
		Employee emp2 = new Employee();

		emp1.empInfo();
		emp2.empInfo();

		System.out.println("After Change");

		emp2.empId = 2;
		emp2.empName = "Shree";
		emp2.y = 5000;

		emp1.empInfo();
		emp2.empInfo();
	}
}

/*
 	EmpId is = 1
	EmpName is = harshal
	y is = 5
	EmpId is = 1
	EmpName is = harshal
	y is = 5
	After Change
	EmpId is = 1
	EmpName is = harshal
	y is = 5000
	EmpId is = 2
	EmpName is = Shree
	y is = 5000
*/
