
class Git{
	
	String str1 = "Before Push";

	static String str2 = "Files are in Local Machines";

	void display(){
		
		System.out.println(str1);

		System.out.println(str2);
	}
}

class Main{
	
	public static void main(String[] s){
		
		Git obj1 = new Git();
		Git obj2 = new Git();

		obj1.display();
		obj2.display();
		
		System.out.println("After Commit");

		obj1.str1 = "After Push";
		obj2.str2 = "Files are in Globally present";

		obj1.display();
		obj2.display();
	}
}

