
/*
 	1] Constructor
		
		- Constructor alay Instance variable initialize karayla 

		- Constructor Special Methods Astat

		- Constructor Ani Class cha naav same asto

		- Saglya Class madhe Constructor asto

		- Instance Variable la jaga Constructor madhe bhetto
	
	1] In Java, a constructor is a block of codes similar to the method. It is called when an instance of the class is created. At the time of calling constructor, memory for the object is allocated in the memory. 
	
	2] It is a special type of method which is used to initialize the object.

	3] Every time an object is created using the new() keyword, at least one constructor is called. 

	4] It calls a default constructor if there is no constructor available in the class. In such case, Java compiler provides a default constructor by default.

	Rules for creating Java constructor

	There are two rules defined for the constructor.

	1] Constructor name must be the same as its class name
    	2] A Constructor must have no explicit return type
    	3] A Java constructor cannot be abstract, static, final, and synchronized

*/

class Sol{

	int x = 10;		// instance variable

	static void fun(){
			
		int y = 20;	// local variable
	}

	public static void main(String[] s){
			
		int z = 30;	// local variable
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/1$ javap -c Sol.class 

	Compiled from "p1.java"
	class Sol {
	  int x;

	  Sol();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: aload_0
	       5: bipush        10
	       7: putfield      #2                  // Field x:I
	      10: return

	  static void fun();
	    Code:
	       0: bipush        20
	       2: istore_0
	       3: return

	  public static void main(java.lang.String[]);
	    Code:
	       0: bipush        30
	       2: istore_1
	       3: return
	}

*/

/*
 	invokespecial : Parent class chya constructor la call kar

	init : mhnje Constructor

*/
