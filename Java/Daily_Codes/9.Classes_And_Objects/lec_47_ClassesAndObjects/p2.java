
class Sol{
		
	int x = 10;

	static void fun(){
			
		int y = 20;

		System.out.println(y);

		System.out.println(x);
	}

	public static void main(String[] s){
		
		int z = 30;

		System.out.println(z);

		fun();
	}
}

/*
 	p2.java:12: error: non-static variable x cannot be referenced from a static context
		System.out.println(x);
		                   ^
	1 error
*/

/*
 	Constructor la jaga navin object banavla nantar bhetto, non-static cha object banavla nahi tar error yeto

	The reference to the non static variable is stored in constructor on the heap and the constructor in created ONLY WHEN OBJECT IS CREATED . If there is no object creation then it will lead to no constructor creation and there will be no reference to the non static variable, and this variable cannot be accessed from prgram,hence non static variable cannot be referenced from static context 

*/
