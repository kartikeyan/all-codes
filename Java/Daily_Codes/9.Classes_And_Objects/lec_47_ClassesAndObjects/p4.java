
//	Q] Static Block la jaga kute bhetto ?

//	A] Static Block

class Demo{

	int z = 30;

	static int a = 40;	// main method() chya agodhar yeto

	static void fun(){
		
		int y = 20;
	}
	
	public static void main(String[] s){
		
		int x = 10;
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/1$ javap -c Demo.class 
Compiled from "p4.java"
class Demo {
  int z;

  static int a;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        30
       7: putfield      #2                  // Field z:I
      10: return

  static void fun();
    Code:
       0: bipush        20
       2: istore_0
       3: return

  public static void main(java.lang.String[]);
    Code:
       0: bipush        10
       2: istore_1
       3: return

  static {};
    Code:
       0: bipush        40
       2: putstatic     #3                  // Field a:I
       5: return
}

*/
