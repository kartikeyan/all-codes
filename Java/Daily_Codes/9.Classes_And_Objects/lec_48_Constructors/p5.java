
class constDemo{

	constDemo(){
		
		System.out.println("In Constructor");
	}

	void fun(){
		
		constDemo obj = new constDemo();	// In constructor
		
		System.out.println(obj);
	}

	public static void main(String[] s){
		
		constDemo obj1 = new constDemo();	// In constructor
		
		System.out.println(obj1);
	
		constDemo obj2 = new constDemo();	// In constructor

		System.out.println(obj2);
	
		obj1.fun();

		System.out.println(obj);
	}
}

/*
 	In Constructor
	constDemo@d716361
	In Constructor
	constDemo@6ff3c5b5
	In Constructor
	constDemo@3764951d
*/


/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/2$ javap -c constDemo.class 
Compiled from "p5.java"
class constDemo {
  constDemo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       7: ldc           #3                  // String In Constructor
       9: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      12: return

  void fun();
    Code:
       0: new           #5                  // class constDemo
       3: dup
       4: invokespecial #6                  // Method "<init>":()V
       7: astore_1
       8: return

  public static void main(java.lang.String[]);
    Code:
       0: new           #5                  // class constDemo
       3: dup
       4: invokespecial #6                  // Method "<init>":()V
       7: astore_1
       8: new           #5                  // class constDemo
      11: dup
      12: invokespecial #6                  // Method "<init>":()V
      15: astore_2
      16: aload_1
      17: invokevirtual #7                  // Method fun:()V
      20: return
}

*/
