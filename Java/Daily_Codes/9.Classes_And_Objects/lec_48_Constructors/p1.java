
class Core2Web{
	
	int x = 10;

	static int y = 20;

	void fun(){
		
		int z = 30;
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/2$ javap -c Core2Web.class 
Compiled from "p1.java"
class Core2Web {
  int x;

  static int y;

  Core2Web();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field x:I
      10: return

  void fun();
    Code:
       0: bipush        30
       2: istore_1
       3: return

  static {};
    Code:
       0: bipush        20
       2: putstatic     #3                  // Field y:I
       5: return
}

*/
