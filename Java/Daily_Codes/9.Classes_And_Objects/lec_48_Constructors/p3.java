
class Core2Web{
	
	Core2Web(){
		
		System.out.println("In C2W");		
	}
	
	int x = 10;

	public static void main(String[] s){
			
		System.out.println("In main");		// In main
	}

	int y = 20;
}

/*
 	
class Core2Web {
  int x;

  int y;

  Core2Web();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field x:I
      10: aload_0
      11: bipush        20
      13: putfield      #3                  // Field y:I
      16: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
      19: ldc           #5                  // String In C2W
      21: invokevirtual #6                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      24: return

  public static void main(java.lang.String[]);
    Code:
       0: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: ldc           #7                  // String In main
       5: invokevirtual #6                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
       8: return
}


*/
