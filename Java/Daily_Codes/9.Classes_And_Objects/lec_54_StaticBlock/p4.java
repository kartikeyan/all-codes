
class Demo{
	
	private int x = 10;

	private static int y = 20;

	static void fun1(){

		System.out.println(y);		// 20
	}

	void fun2(){
		
		System.out.println(x);

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo obj = new Demo();

		System.out.println(obj.x);

		System.out.println(obj.y);
	}
}

/*
 	p4.java:27: error: x has private access in Demo
		System.out.println(obj.x);
		                      ^
	p4.java:29: error: y has private access in Demo
		System.out.println(obj.y);
		                      ^
	2 errors
*/
