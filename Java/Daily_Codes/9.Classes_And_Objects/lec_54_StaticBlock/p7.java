
class Demo{
	
	int x = 10;

	static int y = 20;

	static{
		
		System.out.println("Static Block 1");
	}
	
	static void fun(){
		
		System.out.println("In Fun");
	}

	public static void main(String[] args){
		
		System.out.println("MAin Method");

		Demo obj = new Demo();

		System.out.println(obj.x);
	}

	static{
		
		System.out.println("Static Block 2");

		System.out.println("y is : " + y);

		fun();
	}
}

/*
 	Static Block 1
	Static Block 2
	y is : 20
	In Fun
	MAin Method
	10

*/

/*
 	Compiled from "p7.java"
	class Demo {
	  int x;

	  static int y;

	  Demo();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: aload_0
	       5: bipush        10
	       7: putfield      #2                  // Field x:I
	      10: return

	  static void fun();
	    Code:
	       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #4                  // String In Fun
	       5: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return

	  public static void main(java.lang.String[]);
	    Code:
	       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #6                  // String MAin Method
	       5: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: new           #7                  // class Demo
	      11: dup
	      12: invokespecial #8                  // Method "<init>":()V
	      15: astore_1
	      16: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	      19: aload_1
	      20: getfield      #2                  // Field x:I
	      23: invokevirtual #9                  // Method java/io/PrintStream.println:(I)V
	      26: return

	  static {};
	    Code:
	       0: bipush        20
	       2: putstatic     #10                 // Field y:I
	       5: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       8: ldc           #11                 // String Static Block 1
	      10: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	      13: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	      16: ldc           #12                 // String Static Block 2
	      18: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	      21: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	      24: getstatic     #10                 // Field y:I
	      27: invokedynamic #13,  0             // InvokeDynamic #0:makeConcatWithConstants:(I)Ljava/lang/String;
	      32: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	      35: invokestatic  #14                 // Method fun:()V
	      38: return
	}

*/
