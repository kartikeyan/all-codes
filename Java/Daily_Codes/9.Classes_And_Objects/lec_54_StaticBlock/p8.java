
class Demo{
	
	int x = 10;

	static int y = 20;

	static{
		
		System.out.println("Static Block 1");

		System.out.println(x);
	}

	public static void main(String[] args){
		
		System.out.println("MAin Method");

		Demo obj = new Demo();

		System.out.println(obj.x);
	}

	static{
		
		System.out.println("Static Block 2");

		System.out.println("y is : " + y);
	}
}

/*
 	p8.java:12: error: non-static variable x cannot be referenced from a static context
		System.out.println(x);
		                   ^
	1 error
*/
