
class Demo{
	
	int x = 10;

	static int y = 20;

	static void fun1(){

		System.out.println(y);		
	}

	void fun2(){
		
		System.out.println(x);

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo.fun1();

		Demo.fun2();
	}
}

/*
 	p2.java:27: error: non-static method fun2() cannot be referenced from a static context
		Demo.fun2();
		    ^
	1 error
*/
