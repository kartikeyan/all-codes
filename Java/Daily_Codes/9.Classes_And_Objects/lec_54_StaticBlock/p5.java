
//	Static Block main chya agodhar initialize hotat

//	Static Variables tevhach initialize hotat jevha Static Block cha Stackpush kela hotat

class Demo{
	
	static{
		
		System.out.println("Static Block");
	}

	public static void main(String[] args){
		
		System.out.println("Main Method");
	}
}

/*
 	Static Block
	Main Method
*/

/*
 	Compiled from "p5.java"
	class Demo {
	  Demo();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: return

	  public static void main(java.lang.String[]);
	    Code:
	       0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #3                  // String Main Method
	       5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return

	  static {};
	    Code:
	       0: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #5                  // String Static Block
	       5: invokevirtual #4                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return
	}

*/
