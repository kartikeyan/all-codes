
class Demo{
	
	int x = 10;

	static int y = 20;

	static{

		Demo obj = new Demo();
		
		System.out.println("x is : " + obj.x);
		
		System.out.println("Static Block 1");
	}

	public static void main(String[] args){
		
		System.out.println("MAin Method");

		Demo obj = new Demo();

		System.out.println(obj.x);
	}

	static{
		
		System.out.println("Static Block 2");

		System.out.println("y is : " + y);
	}
}

/*
 	x is : 10
	Static Block 1
	Static Block 2
	y is : 20
	MAin Method
	10
*/
