
class Demo{
	
	int x = 10;

	static int y = 20;

	static void fun1(){

		System.out.println(y);		// 20
	}

	void fun2(){
		
		System.out.println(x);

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo.fun1();
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/lec_54_StaticBlock$ javap -c Demo.class 
Compiled from "p1.java"
class Demo {
  int x;

  static int y;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field x:I
      10: return

  static void fun1();
    Code:
       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: getstatic     #4                  // Field y:I
       6: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
       9: return

  void fun2();
    Code:
       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: aload_0
       4: getfield      #2                  // Field x:I
       7: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      10: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      13: getstatic     #4                  // Field y:I
      16: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      19: return

  static {};
    Code:
       0: bipush        20
       2: putstatic     #4                  // Field y:I
       5: return
}

*/
