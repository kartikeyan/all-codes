
class Demo{
	
	int x = 10;

	static int y = 20;

	static void fun1(){

		System.out.println(y);		// 20
	}

	static void fun2(){
		
		System.out.println(x);	

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo.fun1();

		Demo.fun2();
	}
}

/*
 	p3.java:15: error: non-static variable x cannot be referenced from a static context
		System.out.println(x);
		                   ^
	1 error
*/
