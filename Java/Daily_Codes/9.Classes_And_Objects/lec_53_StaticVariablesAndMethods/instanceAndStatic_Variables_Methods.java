
/*
 	1] Object chya navane static and non-static pan access hotat
*/

class StaticDemo{
	
	int x = 10;

	static int y = 20;

	void fun1(){
		
		System.out.println(x);

		System.out.println(y);
	}

	static void fun2(){
		
		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] s){
		
		StaticDemo obj = new StaticDemo();

		obj.fun1();
		obj.fun2();

		System.out.println(obj.x);
		System.out.println(obj.y);
	}
}

/*
 	10
	20
	20
	10
	20
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/lec_53$ javap -c StaticDemo.class 
Compiled from "instanceAndStatic_Variables_Methods.java"
class StaticDemo {
  int x;

  static int y;

  StaticDemo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field x:I
      10: return

  void fun1();
    Code:
       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: aload_0
       4: getfield      #2                  // Field x:I
       7: invokevirtual #4                  // Method java/io/PrintStream.println:(I)V
      10: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      13: getstatic     #5                  // Field y:I
      16: invokevirtual #4                  // Method java/io/PrintStream.println:(I)V
      19: return

  static void fun2();
    Code:
       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: getstatic     #5                  // Field y:I
       6: invokevirtual #4                  // Method java/io/PrintStream.println:(I)V
       9: return

  static {};
    Code:
       0: bipush        20
       2: putstatic     #5                  // Field y:I
       5: return
}

*/
