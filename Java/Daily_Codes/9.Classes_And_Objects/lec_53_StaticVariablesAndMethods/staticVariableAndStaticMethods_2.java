
class StaticDemo{
	
	static int x = 10;
	static int y = 20;

	static void display(){
		
		System.out.println(x);

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] s){
		
		System.out.println(StaticDemo.x);
		System.out.println(StaticDemo.y);

		StaticDemo.display();
	}
}

/*
 *	1] Static Variable cha direct samband asto Method Area Sobat.
 *	2] Static Variable Class chya navane access karta yeto
*/	
