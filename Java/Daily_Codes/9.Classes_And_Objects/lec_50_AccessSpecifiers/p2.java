
class Core2Web{
	
	int numCourses = 8;
	private String favCourse = "Cpp";

	void display(){
		
		System.out.println(numCourses);
		System.out.println(favCourse);
	}
}

class Student{
	
	public static void main(String[] s){
		
		Core2Web obj = new Core2Web();

		System.out.println(obj.numCourses);

		System.out.println(obj.favCourse);
	}
}

/*
 	p2.java:22: error: favCourse has private access in Core2Web
		System.out.println(obj.favCourse);
		                      ^
	1 error
*/
