
class Core2Web{
	
	int courses = 3;		// bydefault access specifiers default ahe
	String favCourse = "OS";	// same

	void display(){
		
		System.out.println(courses);		
		System.out.println(favCourse);
	}
}

class Users{

	public static void main(String[] s){
		
		Core2Web obj = new Core2Web();

		obj.display();

		System.out.println(obj.courses);		// mahnun access hot ahe
		System.out.println(obj.favCourse);		// same
	}
}
