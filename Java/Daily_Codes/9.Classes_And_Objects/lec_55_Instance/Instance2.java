
class Demo{
	
	int x = 10;

	static int y = 20;

	Demo(){
		
		System.out.println("In Constructor");
	}

	static{
		System.out.println("In Static Block 1");
	}

	{
		System.out.println("In Instance Block 1");
	}

	public static void main(String[] s){

		System.out.println("In Main");

		Demo obj = new Demo();
	}

	static{
		System.out.println("In Static Block 2");
	}

	{
		System.out.println("In Instance Block 2");
	}
}

/*
 	In Static Block 1
	In Static Block 2
	In Main
	In Instance Block 1
	In Instance Block 2
	In Constructor
*/

/*
 	Compiled from "Instance2.java"
class Demo {
  int x;

  static int y;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field x:I
      10: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      13: ldc           #4                  // String In Instance Block 1
      15: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      18: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      21: ldc           #6                  // String In Instance Block 2
      23: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      26: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      29: ldc           #7                  // String In Constructor
      31: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      34: return

  public static void main(java.lang.String[]);
    Code:
       0: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       3: ldc           #8                  // String In Main
       5: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
       8: new           #9                  // class Demo
      11: dup
      12: invokespecial #10                 // Method "<init>":()V
      15: astore_1
      16: return

  static {};
    Code:
       0: bipush        20
       2: putstatic     #11                 // Field y:I
       5: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
       8: ldc           #12                 // String In Static Block 1
      10: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      13: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      16: ldc           #13                 // String In Static Block 2
      18: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      21: return
}

*/
