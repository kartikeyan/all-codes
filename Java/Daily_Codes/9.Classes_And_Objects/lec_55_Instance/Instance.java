
/*
 	Instance
		
		1] Instance Variable

		2] Instance Block

		3] Instance Method

			a] Constructor (Special Method)

			b] Non-static Methods
*/

//	Instance Block Constructor madhe asto

class Demo{
	
	int x = 10;

	Demo(){
		
		System.out.println("Constructor");
	}

	{
		System.out.println("Instance Block 1");		// Instance Block
	}

	public static void main(String[] args){
		
		Demo obj = new Demo();

		System.out.println("Main");
	}

	{
		System.out.println("Instance Block 2");
	}
}

/*
 	Instance Block 1
	Instance Block 2
	Constructor
	Main
*/

/*
 	Compiled from "Instance.java"
class Demo {
  int x;

  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: aload_0
       5: bipush        10
       7: putfield      #2                  // Field x:I
      10: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      13: ldc           #4                  // String Instance Block 1
      15: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      18: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      21: ldc           #6                  // String Instance Block 2
      23: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      26: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      29: ldc           #7                  // String Constructor
      31: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      34: return

  public static void main(java.lang.String[]);
    Code:
       0: new           #8                  // class Demo
       3: dup
       4: invokespecial #9                  // Method "<init>":()V
       7: astore_1
       8: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
      11: ldc           #10                 // String Main
      13: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      16: return
}

*/
