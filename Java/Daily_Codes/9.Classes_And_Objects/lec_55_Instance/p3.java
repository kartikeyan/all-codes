
/*
        static variable konta pan static block, static method, non-static method madhe pan chalat nahi karan

        Java Supports Global Static Variables that is why it is known as Class Variables

        Mhnje Static Variable saglyat adhi declare hotat tyamule tyala chalat nahi

        Java Sequnce :

                1] Static Varibale              5] Instance Block

                2] Static Block                 6] Constructor

                3] Static Methods               7] Instance Method

                4] Instance Variable

*/

class Demo{
	
	static int x = 10;

	void fun(){
		
		static int y = 20;
	}
}

/*
 	p3.java:8: error: illegal start of expression
		static int y = 20;
		^
	p3.java:10: error: class, interface, or enum expected
	}
	^
	2 errors
*/
