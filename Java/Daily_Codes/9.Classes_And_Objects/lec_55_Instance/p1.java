
class Demo{
	
	static{

		System.out.println("Static Block 1");
	}

	public static void main(String[] args){
		
		System.out.println("In Demo Main");
	}
}

class Client{
	
	static{	

		System.out.println("Static Block 2");
	}

	public static void main(String[] args){
		
		System.out.println("In Client Main");
	}

	static{

		System.out.println("Static Block 3");	
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/lec_55_Instance$ java Client 
	Static Block 2
	Static Block 3
	In Client Main
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/9.Classes_And_Objects/lec_55_Instance$ java Demo 
	Static Block 1
	In Demo Main
*/
