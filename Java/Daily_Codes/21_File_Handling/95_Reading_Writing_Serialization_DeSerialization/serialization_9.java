
//	In Serialization we store objects in file
//
//	Important Class are : 1] FileOutputStream   2] ObjectOutputStream

import java.io.*;

class Player implements Serializable{
	
	int jerNo;

	String pName;

	Player(int jerNo, String pName){
		
		this.jerNo = jerNo;

		this.pName = pName;
	}
}

class SerilzationDemo{
	
	public static void main(String[] args) throws IOException{
		
		Player obj1 = new Player(18, "Virat");

		Player obj2 = new Player(45, "Rohit");

		FileOutputStream fos = new FileOutputStream("PlayerData.txt");

		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(obj1);
		oos.writeObject(obj2);

		oos.close();
		fos.close();
	}
}

/*
 	In PlayerData.txt we have 2 objects
*/
