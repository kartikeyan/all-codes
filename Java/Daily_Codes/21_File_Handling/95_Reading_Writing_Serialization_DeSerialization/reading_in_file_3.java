
/*
 	Compiled from "FileReader.java"

	public class java.io.FileReader extends java.io.InputStreamReader {
	  public java.io.FileReader(java.lang.String) throws java.io.FileNotFoundException;
	  public java.io.FileReader(java.io.File) throws java.io.FileNotFoundException;
	  public java.io.FileReader(java.io.FileDescriptor);
	  public java.io.FileReader(java.lang.String, java.nio.charset.Charset) throws java.io.IOException;
	  public java.io.FileReader(java.io.File, java.nio.charset.Charset) throws java.io.IOException;
	}
*/

import java.io.*;

class FileReaderDemo{
	
	public static void main(String[] args){

		FileReader fr = new FileReader("Incubator.txt");

		int data = fr.read();

		while(data == -1){
			
			System.out.println((char)data);

			data = fr.read();
		}
		
		fr.close();
	}
}

/*
 	reading_in_file_3.java:20: error: unreported exception FileNotFoundException; must be caught or declared to be thrown
		FileReader fr = new FileReader("Incubator.txt");
		                ^
	reading_in_file_3.java:22: error: unreported exception IOException; must be caught or declared to be thrown
		int data = fr.read();
		                  ^
	reading_in_file_3.java:28: error: unreported exception IOException; must be caught or declared to be thrown
			data = fr.read();
			              ^
	reading_in_file_3.java:31: error: unreported exception IOException; must be caught or declared to be thrown
			fr.close();
		        ^
	4 errors
*/
