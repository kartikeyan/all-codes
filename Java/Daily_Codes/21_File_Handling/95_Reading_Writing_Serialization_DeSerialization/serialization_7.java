
import java.io.*;

class Player{
	
	int jerNo;

	String pName;

	Player(int jerNo, String pName){
		
		this.jerNo = jerNo;

		this.pName = pName;
	}
}

class SerilzationDemo{
	
	public static void main(String[] args){
		
		Player obj1 = new Player(18, "Virat");

		Player obj2 = new Player(45, "Rohit");

		FileOutputStream fos = new FileOutputStream("PlayerData.txt");

		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(obj1);
		oos.writeObject(obj2);

		oos.close();
		fos.close();
	}
}

/*
 	serialization_7.java:26: error: unreported exception FileNotFoundException; must be caught or declared to be thrown
		FileOutputStream fos = new FileOutputStream("PlayerData.txt");
		                       ^
	serialization_7.java:28: error: unreported exception IOException; must be caught or declared to be thrown
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		                         ^
	serialization_7.java:30: error: unreported exception IOException; must be caught or declared to be thrown
		oos.writeObject(obj1);
		               ^
	serialization_7.java:31: error: unreported exception IOException; must be caught or declared to be thrown
		oos.writeObject(obj2);
		               ^
	serialization_7.java:33: error: unreported exception IOException; must be caught or declared to be thrown
		oos.close();
		         ^
	serialization_7.java:34: error: unreported exception IOException; must be caught or declared to be thrown
		fos.close();
		         ^
	6 errors
*/
