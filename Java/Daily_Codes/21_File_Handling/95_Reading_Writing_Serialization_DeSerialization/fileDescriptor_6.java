
/*
 	katikeyan@kartikeyan:~/javacodes/Codes/21_File_Handling/95_Reading_Writing_Serialization_DeSerialization$ javap java.io.FileDescriptor
	Compiled from "FileDescriptor.java"
	public final class java.io.FileDescriptor {
	  public static final java.io.FileDescriptor in;
	  public static final java.io.FileDescriptor out;
	  public static final java.io.FileDescriptor err;
	  public java.io.FileDescriptor();
	  public boolean valid();
	  public native void sync() throws java.io.SyncFailedException;
	  synchronized void set(int);
	  void setHandle(long);
	  synchronized void registerCleanup(jdk.internal.ref.PhantomCleanable<java.io.FileDescriptor>);
	  synchronized void unregisterCleanup();
	  synchronized void close() throws java.io.IOException;
	  synchronized void attach(java.io.Closeable);
	  synchronized void closeAll(java.io.Closeable) throws java.io.IOException;
	  static {};
	}
*/

import java.io.*;

class FileDesDemo{
	
	public static void main(String[] args){
		
		FileDescriptor fd = new FileDescriptor();

		System.out.println(fd);
	}
}

//	java.io.FileDescriptor@d716361
