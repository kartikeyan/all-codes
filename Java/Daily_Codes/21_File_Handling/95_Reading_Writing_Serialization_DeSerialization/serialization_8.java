
import java.io.*;

class Player{
	
	int jerNo;

	String pName;

	Player(int jerNo, String pName){
		
		this.jerNo = jerNo;

		this.pName = pName;
	}
}

class SerilzationDemo{
	
	public static void main(String[] args) throws IOException{
		
		Player obj1 = new Player(18, "Virat");

		Player obj2 = new Player(45, "Rohit");

		FileOutputStream fos = new FileOutputStream("PlayerData.txt");

		ObjectOutputStream oos = new ObjectOutputStream(fos);

		oos.writeObject(obj1);
		oos.writeObject(obj2);

		oos.close();
		fos.close();
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/21_File_Handling/95_Reading_Writing_Serialization_DeSerialization$ java SerilzationDemo 
	Exception in thread "main" java.io.NotSerializableException: Player
		at java.base/java.io.ObjectOutputStream.writeObject0(ObjectOutputStream.java:1175)
		at java.base/java.io.ObjectOutputStream.writeObject(ObjectOutputStream.java:345)
		at SerilzationDemo.main(serialization_8.java:30)
*/
