
//	In DeSerialization we remove objects from file and print it
//
//	Important Class are : 1] FileInputStream   2] ObjectInputStream

import java.io.*;

class DeSerilzationDemo{
	
	public static void main(String[] args) throws IOException{

		FileInputStream fis = new FileInputStream("PlayerData.txt");

		ObjectInputStream ois = new ObjectInputStream(fis);
	
		Player obj1 = (Player)ois.readObject();
		Player obj2 = (Player)ois.readObject();

		ois.close();
		fis.close();

		System.out.println("JerNo = " + obj1.jerNo);

		System.out.println("NAme is = " + obj1.pName);

		System.out.println("JerNo = " + obj2.jerNo);

		System.out.println("NAme is = " + obj2.pName);
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/21_File_Handling/95_Reading_Writing_Serialization_DeSerialization$ javac deserialization_10.java 
	deserialization_10.java:16: error: unreported exception ClassNotFoundException; must be caught or declared to be thrown
		Player obj1 = (Player)ois.readObject();
		                                    ^
	deserialization_10.java:17: error: unreported exception ClassNotFoundException; must be caught or declared to be thrown
		Player obj2 = (Player)ois.readObject();
		                                    ^
	2 errors
*/
