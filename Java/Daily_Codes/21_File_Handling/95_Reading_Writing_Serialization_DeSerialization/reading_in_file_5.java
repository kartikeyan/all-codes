
/*
 	Compiled from "FileReader.java"

	public class java.io.FileReader extends java.io.InputStreamReader {
	  public java.io.FileReader(java.lang.String) throws java.io.FileNotFoundException;
	  public java.io.FileReader(java.io.File) throws java.io.FileNotFoundException;
	  public java.io.FileReader(java.io.FileDescriptor);
	  public java.io.FileReader(java.lang.String, java.nio.charset.Charset) throws java.io.IOException;
	  public java.io.FileReader(java.io.File, java.nio.charset.Charset) throws java.io.IOException;
	}
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/21_File_Handling/95_Reading_Writing_Serialization_DeSerialization$ javap java.io.FileInputStream
	Compiled from "FileInputStream.java"
	public class java.io.FileInputStream extends java.io.InputStream {
	  public java.io.FileInputStream(java.lang.String) throws java.io.FileNotFoundException;
	  public java.io.FileInputStream(java.io.File) throws java.io.FileNotFoundException;
	  public java.io.FileInputStream(java.io.FileDescriptor);
	  public int read() throws java.io.IOException;
	  public int read(byte[]) throws java.io.IOException;
	  public int read(byte[], int, int) throws java.io.IOException;
	  public byte[] readAllBytes() throws java.io.IOException;
	  public byte[] readNBytes(int) throws java.io.IOException;
	  public long transferTo(java.io.OutputStream) throws java.io.IOException;
	  public long skip(long) throws java.io.IOException;
	  public int available() throws java.io.IOException;
	  public void close() throws java.io.IOException;
	  public final java.io.FileDescriptor getFD() throws java.io.IOException;
	  public java.nio.channels.FileChannel getChannel();
	  static {};
	}
*/

import java.io.*;

class FileReaderDemo{
	
	public static void main(String[] args) throws IOException{
		
		FileInputStream fis = new FileInputStream("Incubator.txt");

		FileDescriptor fd = fis.getFD();

		FileReader fr = new FileReader(fd);	// 3rd type of const

		int data = fr.read();

		while(data != -1){
			
			System.out.print((char)data);

			data = fr.read();
		}
		
		fr.close();
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/21_File_Handling/95_Reading_Writing_Serialization_DeSerialization$ java FileReaderDemo 
	FlutterFrontEndBackEnd
	FlutterFrontEndBackEnd
*/
