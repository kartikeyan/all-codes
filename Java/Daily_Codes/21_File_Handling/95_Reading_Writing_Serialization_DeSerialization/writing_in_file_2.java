
/*
 	Compiled from "FileWriter.java"
	public class java.io.FileWriter extends java.io.OutputStreamWriter {
	  public java.io.FileWriter(java.lang.String) throws java.io.IOException;
	  public java.io.FileWriter(java.lang.String, boolean) throws java.io.IOException;
	  public java.io.FileWriter(java.io.File) throws java.io.IOException;
	  public java.io.FileWriter(java.io.File, boolean) throws java.io.IOException;
	  public java.io.FileWriter(java.io.FileDescriptor);
	  public java.io.FileWriter(java.lang.String, java.nio.charset.Charset) throws java.io.IOException;
	  public java.io.FileWriter(java.lang.String, java.nio.charset.Charset, boolean) throws java.io.IOException;
	  public java.io.FileWriter(java.io.File, java.nio.charset.Charset) throws java.io.IOException;
	  public java.io.FileWriter(java.io.File, java.nio.charset.Charset, boolean) throws java.io.IOException;
	}
*/

import java.io.*;

class FileWriteDemo{
	
	public static void main(String[] args) throws IOException{
		
		File f = new File("Incubator.txt");

		FileWriter fw = new FileWriter(f, true);	// 2nd type of constructor
								// if true then append in file
								// by default it is false

		fw.write("Flutter");

		fw.write("FrontEnd");

		fw.write("BackEnd");
		
		fw.close();
	}
}

//	FlutterFrontEndBackEnd
//	FlutterFrontEndBackEnd 
