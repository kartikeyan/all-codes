
//	In DeSerialization we remove objects from file and print it
//
//	Important Class are : 1] FileInputStream   2] ObjectInputStream

import java.io.*;

class DeSerilzationDemo{
	
	public static void main(String[] args) throws IOException, ClassNotFoundException{

		FileInputStream fis = new FileInputStream("PlayerData.txt");

		ObjectInputStream ois = new ObjectInputStream(fis);
	
		Player obj1 = (Player)ois.readObject();
		Player obj2 = (Player)ois.readObject();

		ois.close();
		fis.close();

		System.out.println("JerNo = " + obj1.jerNo);

		System.out.println("NAme is = " + obj1.pName);

		System.out.println("JerNo = " + obj2.jerNo);

		System.out.println("NAme is = " + obj2.pName);
	}
}

/*
 	JerNo = 18
	NAme is = Virat
	JerNo = 45
	NAme is = Rohit
*/
