
/*
 	Compiled from "FileReader.java"

	public class java.io.FileReader extends java.io.InputStreamReader {
	  public java.io.FileReader(java.lang.String) throws java.io.FileNotFoundException;
	  public java.io.FileReader(java.io.File) throws java.io.FileNotFoundException;
	  public java.io.FileReader(java.io.FileDescriptor);
	  public java.io.FileReader(java.lang.String, java.nio.charset.Charset) throws java.io.IOException;
	  public java.io.FileReader(java.io.File, java.nio.charset.Charset) throws java.io.IOException;
	}
*/

import java.io.*;

class FileReaderDemo{
	
	public static void main(String[] args) throws IOException{

		FileReader fr = new FileReader("Incubator.txt");	// 1st type of const

		int data = fr.read();

		while(data != -1){
			
			System.out.print((char)data);

			data = fr.read();
		}
		
		fr.close();
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/21_File_Handling/95_Reading_Writing_Serialization_DeSerialization$ java FileReaderDemo 
	FlutterFrontEndBackEnd
	FlutterFrontEndBackEnd
*/
