
import java.io.*;

class FileDemo{
	
	public static void main(String[] args) throws IOException{
		
		File obj = new File("/home/katikeyan/javacodes/Codes/21_File_Handling");
		System.out.println(obj.exists());	// true

		String[] files = obj.list();

		for(String s : files){
			
			System.out.println(s);
		}

		int cFile = 0, cFolder = 0;

		for(String s : files){
			
			File f = new File(s);

			if(f.isFile()){
				cFile++;
			}
			if(f.isDirectory()){
				cFolder++;
			}
		}

		System.out.println("File Count is " + cFile);
		System.out.println("Directory Count is " + cFolder);
	}
}

/*
 	true
	howToCreateFile.java
	listMethodInFile.java
	FileDemo.class
	kartik
	Shree.txt
	UnixFileSystem.txt
	p1.java
	howToCreateDirectory.java
	p2.java
	kartik.txt
	methods.java
	File.txt
	File Count is 11
	Directory Count is 1
*/
