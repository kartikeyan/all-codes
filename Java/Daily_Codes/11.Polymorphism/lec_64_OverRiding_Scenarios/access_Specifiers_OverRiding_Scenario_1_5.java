
/*
 	Parent class cha access child class madhe kami hau naye, nahitar error
*/	

class Parent{
	
	protected void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	Parent class madla method protected ahe, toh child class madhe default zalay, tyamule tyacha access
	kami zalay karan protected dusrya folder chya child class madhe disu shakto, pan default matra eka folder purta
	access hoto, tyamule ithe tyacha acess kami zala mahnun error yenar.
*/

/*
 	access_Specifiers_OverRiding_Scenario_1_5.java:16: error: fun() in Child cannot override fun() in Parent
	void fun(){
	     ^
  	attempting to assign weaker access privileges; was protected
	1 error
*/
