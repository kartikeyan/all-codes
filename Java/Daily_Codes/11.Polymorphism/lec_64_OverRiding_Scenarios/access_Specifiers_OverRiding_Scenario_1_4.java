
/*
 	Parent class cha access child class madhe kami hau naye, nahitar error
*/	

class Parent{
	
	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	private void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	Parent class madla method default ahe, toh child class madhe private zalay, tyamule tyacha access
	kami zalay karan private tya class purta disu shakto, pan default matra eka folder purta
	access hoto, tyamule ithe tyacha acess kami zala mahnun error yenar.
*/

/*
 	access_Specifiers_OverRiding_Scenario_1_4.java:16: error: fun() in Child cannot override fun() in Parent
	private void fun(){
	             ^
  	attempting to assign weaker access privileges; was package
	1 error
*/
