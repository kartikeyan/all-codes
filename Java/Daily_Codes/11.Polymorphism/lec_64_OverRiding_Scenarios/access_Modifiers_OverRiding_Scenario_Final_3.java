
class Parent{
	
	final void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	final void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	access_Modifiers_OverRiding_Scenario_Final_3.java:12: error: fun() in Child cannot override fun() in Parent
	final void fun(){
	           ^
  	overridden method is final
	1 error
*/
