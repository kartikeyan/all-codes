
class Parent{
	
	final void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	access_Modifiers_OverRiding_Scenario_Final.java:12: error: fun() in Child cannot override fun() in Parent
	void fun(){
	     ^
  	overridden method is final
	1 error
*/
