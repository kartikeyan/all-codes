
//	Static OverRiding madhe chalat nahi

class Parent{
	
	static void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	access_Modifiers_OverRiding_Scenario_Static_1.java:12: error: fun() in Child cannot override fun() in Parent
	void fun(){
	     ^
  	overridden method is static
	1 error
*/
