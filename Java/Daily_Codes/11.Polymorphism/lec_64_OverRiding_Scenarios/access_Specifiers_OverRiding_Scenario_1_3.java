
/*
 	Parent class cha access child class madhe kami hau naye, nahitar error
*/	

class Parent{
	
	private void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	Parent class madla method private ahe, toh child class madhe default zalay, tyamule tyacha access
	vadlay karan private tya class purta disu shakto, pan default matra eka folder purta
	access hoto, tyamule ithe tyacha acess vadla mahnun error nahi yenar
*/

