
/*
 	Method Hiding :

		1] If a subclass defines a static method with the same signature as a static method in the superclass, then the method in the subclass hides the one in the superclass. 

		2] This mechanism happens because the static method is resolved at the compile time. Static method bind during the compile time using the type of reference not a type of object.

*/

class Parent{
	
	static void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	static void fun(){
		
		System.out.println("Child fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Parent();
		obj1.fun();

		Child obj2 = new Child();
		obj2.fun();

		Parent obj3 = new Child();	
		obj3.fun();
	}
}

/*
 	Parent fun
	Child fun
	Parent fun
*/

/*
 	Difference Between Method Overriding and Method Hiding in Java

    		1] In method overriding both the method parent class and child class are non-static.
    		2] In method Hiding both the method parent class and child class are static.
    		3] In method Overriding method resolution is done on the basis of the Object type.
    		4] In method Hiding method resolution is done on the basis of reference type.
    		5] The version of the overridden instance method that gets invoked is the one in the subclass.
    		6] The version of the hidden static method that gets invoked depends on whether it is invoked from the superclass or the subclass. 

*/
