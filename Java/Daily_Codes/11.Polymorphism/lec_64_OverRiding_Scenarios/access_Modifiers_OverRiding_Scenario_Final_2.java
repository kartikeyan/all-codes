
class Parent{
	
	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	final void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	No error
*/
