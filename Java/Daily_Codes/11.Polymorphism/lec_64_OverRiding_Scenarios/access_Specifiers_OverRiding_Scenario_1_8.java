
/*
 	Parent class cha access child class madhe kami hau naye, nahitar error
*/	

class Parent{
	
	protected void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	public void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	Parent class madla method protected ahe, toh child class madhe public zalay, tyamule tyacha access
	vadlay karan protected dusrya folder chya child class madhe disu shakto, pan public dusrya folder madhe
	access hoto, tyamule ithe tyacha acess vadlay mahnun error nahi yenar.
*/

/*
 	No error
*/
