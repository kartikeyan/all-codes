
/*
 	Parent class cha access child class madhe kami hau naye, nahitar error
*/	

class Parent{
	
	public void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	protected void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	Parent class madla method public ahe, toh child class madhe protected zalay, tyamule tyacha access
	kami zalay karan protected dusrya folder chya child class madhe disu shakto, pan public dusrya folder madhe
	access hoto, tyamule ithe tyacha acess kami zala mahnun error yenar.
*/

/*
 	access_Specifiers_OverRiding_Scenario_1_7.java:16: error: fun() in Child cannot override fun() in Parent
	protected void fun(){
	               ^
  	attempting to assign weaker access privileges; was public
	1 error
*/
