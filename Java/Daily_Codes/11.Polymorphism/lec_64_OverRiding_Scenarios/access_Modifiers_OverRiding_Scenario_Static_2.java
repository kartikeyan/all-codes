
//	Static OverRiding madhe chalat nahi

class Parent{
	
	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	static void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	access_Modifiers_OverRiding_Scenario_Static_2.java:14: error: fun() in Child cannot override fun() in Parent
	static void fun(){
	            ^
  	overriding method is static
	1 error
*/
