
/*
 	Parent class cha access child class madhe kami hau naye, nahitar error
*/	

class Parent{
	
	public void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

/*
 	Parent class madla method public ahe, toh child class madhe default zalay, tyamule tyacha access
	kami zala karan public dusrya folder madhe pan disu shakto, pan default matra eka folder purta
	access hoto, tyamule ithe tyacha acess kami zala mahnun error
*/

/*
 	access_Specifiers_OverRiding_Scenario_1.java:16: error: fun() in Child cannot override fun() in Parent
	void fun(){
	     ^
	attempting to assign weaker access privileges; was public
	1 error
*/
