/*
 	1] OverRiding madhe Primitive datatype cha returntype same pahije Parent-Child Class madhe, nahitar error
*/

class Parent{
	
	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	int fun(){
		
		System.out.println("Child fun");
		return 10;
	}
}

class Client{
	
	public static void main(String[] s){
		
		Parent obj = new Child();

		obj.fun();	// Child fun
	}
}

/*
 	overRiding_scenario_2_2.java:15: error: fun() in Child cannot override fun() in Parent
	int fun(){
	    ^
  	return type int is not compatible with void
	1 error
*/
