/*
 	1] OverRiding madhe Co-Variant Returntype chalto fakt Classes sathi ani tyat Parent-Class Relationship pahije
*/

class Parent{
	
	String fun(){
		
		System.out.println("String");
		return "shashi";
	}
}

class Child extends Parent{
	
	Object fun(){
		
		System.out.println("object");
		return new Object();
	}
}

class Client{
	
	public static void main(String[] s){
		
		Parent obj = new Child();

		obj.fun();	
	}
}

/*
 	overRiding_scenario_3_2.java:16: error: fun() in Child cannot override fun() in Parent
	Object fun(){
	       ^
  	return type Object is not compatible with String
	1 error
*/
