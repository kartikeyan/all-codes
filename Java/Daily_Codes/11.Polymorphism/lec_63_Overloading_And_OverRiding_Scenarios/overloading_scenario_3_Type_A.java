
class Demo{
	
	void fun(String str){
		
		System.out.println("String");
	}

	void fun(StringBuffer str){
		
		System.out.println("StringBuffer");
	}
}

class Client{
	
	public static void main(String[] s){
		
		String str1 = null;
		StringBuffer str2 = null;

		Demo obj = new Demo();

		obj.fun("C2W");
		obj.fun(new StringBuffer("C2W"));

		obj.fun(null);
	}
}

/*
 	overloading_scenario_3_2.java:27: error: reference to fun is ambiguous
		obj.fun(null);
		   ^
  	both method fun(String) in Demo and method fun(StringBuffer) in Demo match
	1 error
*/
