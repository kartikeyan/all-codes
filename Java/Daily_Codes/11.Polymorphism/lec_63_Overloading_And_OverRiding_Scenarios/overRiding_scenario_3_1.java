/*
 	1] OverRiding madhe Co-Variant Returntype chalto fakt Classes sathi ani tyat Parent-Class Relationship pahije
*/

class Parent{
	
	Object fun(){
		
		System.out.println("Object");
		return new Object();
	}
}

class Child extends Parent{
	
	String fun(){
		
		System.out.println("String");
		return "kartik";
	}
}

class Client{
	
	public static void main(String[] s){
		
		Parent obj = new Child();

		obj.fun();	// String	
	}
}
