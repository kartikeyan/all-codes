
class Demo{
	
	void fun(int x){
		System.out.println("Int");
	}

	void fun(float x){
		System.out.println("Float");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Demo obj = new Demo();
		obj.fun(10.5);
	}
}

/*
 	overloading_scenario_1.java:18: error: no suitable method found for fun(double)
		obj.fun(10.5);
		   ^
    	method Demo.fun(int) is not applicable
      	(argument mismatch; possible lossy conversion from double to int)
    	method Demo.fun(float) is not applicable
      	(argument mismatch; possible lossy conversion from double to float)
	1 error
*/
