
class Match{
	
	void matchInfo(){
		
		System.out.println("Test/ODI/T20");
	}
}

class IPLMatch extends Match{
	
	void matchInfo(){
		
		System.out.println("T20");
	}
}

class TestMatch extends Match{
	
	void matchInfo(){
		
		System.out.println("Test");
	}
}

class ODIMatch extends Match{
	
	void matchInfo(){
		
		System.out.println("ODI");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Match type1 = new Match();
		type1.matchInfo();

		Match type2 = new IPLMatch();
		type2.matchInfo();

		Match type3 = new TestMatch();
		type3.matchInfo();

		Match type4 = new ODIMatch();
		type4.matchInfo();
	}
}

/*
 	Test/ODI/T20
	T20
	Test
	ODI
*/
