class Demo{
	
	void fun(Object obj){
		
		System.out.println("Object");
	}

	void fun(String str){
		
		System.out.println("String");
	}
}

class Client{
	
	public static void main(String[] s){
		
		String str1 = null;
		StringBuffer str2 = null;

		Demo obj = new Demo();

		obj.fun("C2W");
		obj.fun(new StringBuffer("C2W"));

		obj.fun(null);		// ithe dogankade jau shakto, mahnun object cha child mhnje String madhe jail
	}
}

/*
 	String
	Object
	String
*/
