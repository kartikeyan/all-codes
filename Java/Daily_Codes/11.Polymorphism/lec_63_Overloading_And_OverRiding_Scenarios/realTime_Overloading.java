
class WTC{
	
	void matchInfo(String team1, String team2){
		
		System.out.println(team1 + " vs " + team2);
	}

	void matchInfo(String team1, String team2, String venue){
		
		System.out.println(team1 + " vs " + team2 + " at " + venue);
	}
}

class Client{
	
	public static void main(String[] s){

		WTC wt2023 = new WTC();

		wt2023.matchInfo("India", "Australia");
		wt2023.matchInfo("India", "Australia", "Oval");
	}
}

/*
 	India vs Australia
	India vs Australia at Oval
*/
