/*
 	1] OverRiding madhe Primitive datatype cha returntype same pahije Parent-Child Class madhe, nahitar error
*/

class Parent{
	
	int fun(){
		
		System.out.println("Parent fun");
		return 20;
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Parent obj = new Child();

		obj.fun();	// Child fun
	}
}

/*
 	overRiding_scenario_2_1.java:16: error: fun() in Child cannot override fun() in Parent
	void fun(){
	     ^
  	return type void is not compatible with int
	1 error
*/
