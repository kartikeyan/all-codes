
class Demo{
	
	void fun(int x, float y){
		
		System.out.println("Int-float");
	}

	void fun(float x, int y){
		
		System.out.println("float-int");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Demo obj = new Demo();

		obj.fun(10,10);
	}
}

/*
 	overloading_scenario_2.java:21: error: reference to fun is ambiguous
		obj.fun(10,10);
		   ^
  	both method fun(int,float) in Demo and method fun(float,int) in Demo match
	1 error
*/

