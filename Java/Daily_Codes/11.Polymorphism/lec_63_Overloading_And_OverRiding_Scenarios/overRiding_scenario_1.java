
/*
 	1] OverRiding madhe Primitive datatype cha returntype same pahije Parent-Child Class madhe, nahitar error
*/

class Parent{
	
	int fun(){
		
		System.out.println("Parent fun");
		return 20;
	}
}

class Child extends Parent{
	
	int fun(){
		
		System.out.println("Child fun");
		return 10;
	}
}

class Client{
	
	public static void main(String[] s){
		
		Parent obj = new Child();

		obj.fun();	// Child fun
	}
}
