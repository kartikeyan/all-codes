/*
 	1] OverRiding madhe Primitive datatype cha returntype same pahije Parent-Child Class madhe, nahitar error
*/

class Parent{
	
	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	void fun(){
		
		System.out.println("Child fun");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Parent obj = new Child();

		obj.fun();	// Child fun
	}
}
