
//	Overloading madhe function cha returntype matter karat nahi

class Demo{
	
	void fun(int x){	// fun(int)
		
		System.out.println(x);
	}

	void fun(float x){	// fun(int)
		
		System.out.println(x);
	}

	void fun(Demo obj){
		
		System.out.println(obj);

		System.out.println("In Demo Para");
	}

	public static void main(String[] s){
		
		Demo obj = new Demo();

		obj.fun(10);

		obj.fun(10.5f);

		Demo obj1 = new Demo();
		obj1.fun(obj);
	}
}

/*
 	10
	10.5
	Demo@1dbd16a6
	In Demo Para
*/
