
//	Overloading madhe function cha returntype matter karat nahi

class Demo{
	
	int fun(int x){	// fun(int)
		
		System.out.println(x);
	}

	float fun(int x){	// fun(int)
		
		System.out.println(x);
	}
}

/*
 	p2.java:9: error: method fun(int) is already defined in class Demo
	float fun(int x){	// fun(int)
	      ^
	1 error

*/
