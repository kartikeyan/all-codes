class Parent{
	
	Parent(){
		
		System.out.println("Parent Const");
	}

	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("Child Const");
	}

	void gun(){
		
		System.out.println("Child gun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj1 = new Child();
		obj1.fun();
		obj1.gun();

		Parent obj2 = new Parent();
		obj2.fun();
		obj2.gun();
	}
}

/*
 	Method Table for Parent

		1] Parent()
		2] fun()

	Method Table for Child
		
		1] Child()
		2] gun()
		3] fun()
*/

/*
 	overRiding_2.java:37: error: cannot find symbol
		obj2.gun();
		    ^
	symbol:   method gun()
	location: variable obj2 of type Parent
	1 error
*/
