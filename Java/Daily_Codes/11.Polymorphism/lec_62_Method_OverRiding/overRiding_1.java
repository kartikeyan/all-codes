
class Parent{
	
	Parent(){
		
		System.out.println("Parent Const");
	}

	void property(){
		
		System.out.println("Home, Gold, etc");
	}

	void marry(){
		
		System.out.println("Anushka Desai");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("Child Const");
	}

	void marry(){
		
		System.out.println("Siddhi Kore");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();

		obj.property();

		obj.marry();
	}
}

/*
 	Parent Const
	Child Const
	Home, Gold, etc
	Siddhi Kore
*/
