class Parent{
	
	Parent(){
		
		System.out.println("Parent Const");
	}

	void fun(int x){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("Child Const");
	}

	void fun(){
		
		System.out.println("Child fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj = new Child();		// Parent obj1 ===> Reference ===> Complie time 
							
							// new Child();  ===> Object ===> Runtime
		
		obj.fun();
	}
}

/*
	compiler obj.fun() bhetat nahi mahnun error yeto
/*
 	overRiding_6.java:35: error: method fun in class Parent cannot be applied to given types;
		obj.fun();
		   ^
	required: int
	found:    no arguments
	reason: actual and formal argument lists differ in length
	1 error
*/

