class Parent{
	
	Parent(){
		
		System.out.println("Parent Const");
	}

	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("Child Const");
	}

	void fun(int x){
		
		System.out.println("Child fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj = new Child();		// Parent obj1 ===> Reference ===> Complie time 
							
							// new Child();  ===> Object ===> Runtime
		
		obj.fun();
	}
}

/*
 	Method table for Parent
		1] Parent()
		2] fun()

	Method table for Child
		1] Child()
		2] fun(int)
		3] fun()
*/

/*
 	Parent Const
	Child Const
	Parent fun
*/

