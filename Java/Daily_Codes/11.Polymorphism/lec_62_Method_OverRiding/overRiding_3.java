class Parent{
	
	Parent(){
		
		System.out.println("Parent Const");
	}

	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("Child Const");
	}

	void gun(){
		
		System.out.println("Child gun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Child();		// Parent obj1 ===> Reference ===> Complie time 
							
							// new Child();  ===> Object ===> Runtime
		Child obj2 = new Parent();		
	}
}

/*
 	overRiding_3.java:34: error: incompatible types: Parent cannot be converted to Child
		Child obj2 = new Parent();		
		             ^
	1 error
*/
