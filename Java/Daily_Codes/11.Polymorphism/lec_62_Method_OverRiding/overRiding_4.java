class Parent{
	
	Parent(){
		
		System.out.println("Parent Const");
	}

	void fun(){
		
		System.out.println("Parent fun");
	}
}

class Child extends Parent{
	
	Child(){
		
		System.out.println("Child Const");
	}

	void fun(){
		
		System.out.println("Child fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj = new Child();		// Parent obj1 ===> Reference ===> Complie time 
							
							// new Child();  ===> Object ===> Runtime
		
		obj.fun();
	}
}

/*
 	Parent Const
	Child Const
	Child fun
*/

/*
 	Compile time la compiler la vat-toh ki ouput "Parent fun" yeil, Pan runtime la output yeto "Child fun"
*/

/*
 	Parent obj = new Child();
	
	1] Compile time la compiler fakt Parent obj bagto, ani tohnew Child() la bagat nahi, tyala farak padat nahi ki right side la Child() cha object ahe ki Parent() ahe.
	
	2] Tyamule toh parent cha object execute karto mahun, compiler la output vat-toh ki "Parent fun".

	3] Runtime la JVM execute karto new Child() mhnje child cha object banto ani ouput yeto "Child fun".

*/
