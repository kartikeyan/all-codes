
/*	Pattern - 
 *	
 *		1 2 3
 *		A B C
 *		4 5 6
 *		D E F
*/	

class pattern{
	
	public static void main(String[] args){

		int ch = 65, x = 1;

		for(int i = 1; i <= 4; i++){

			for(int j = 1; j <= 3; j++){
				
				if(i % 2 == 0){	
					System.out.print((char) ch + " ");
					ch++;
				}else{
					System.out.print(x++ + " ");
				}
			}

			System.out.println();
		}
	}
}

