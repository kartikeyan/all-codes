
/*	Pattern
 *
 *		1
 *		4  9
 *		16 25 36
 *		49 64 81 100
*/

/*	T.C = 0(N^2)	
*/	

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = 1;

		for(int i = 1; i <= n; i++){

			for(int j = 1; j <= i; j++){
				
				System.out.print(x*x + " ");
				x++;	
			}

			System.out.println();
		}
	}
}
