
/*	Pattern
 *
 *		1
 *		2 c
 *		4 e 6
 *		7 h 9 j
*/

/*	T.C = 0(N^2)	
*/	

class assign{
	
	public static void main(String[] args){
		
		int n = 4;

		int ch = 97, x = 1;

		for(int i = 1; i <= n; i++){

			for(int j = 1; j <= i ; j++){
				
				if(j % 2 == 0){
					System.out.print((char)ch + " ");
				}else{
					System.out.print(x +" ");
				}
				x++;
				ch++;
			}

			System.out.println();
		}
	}
}
