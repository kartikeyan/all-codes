
/*	Pattern
 *
 *		1
 *		2 3
 *		4 5 6
 *		7 8 9 10
 *		11 12 13 14 15
*/

/*	T.C = 0(N^2)	
 *
 *	    = 0(N * (N + 1) / 2)
 *
 *	    = N^2 + N / 2
 *
 *	    = N^2 / 2 + N / 2
 *
 *	    = N^2
 *
 *	    = 1/2 * N^2
 *
 *	T.C = 0(N^2)
*/	

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = 1;

		for(int i = 1; i <= n; i++){
			
			for(int j = 1; j <= i; j++){
				
				System.out.print(x + " ");	
				x++;
			}

			System.out.println();
		}
	}
}
