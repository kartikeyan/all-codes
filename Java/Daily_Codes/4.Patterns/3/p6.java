
/*	Pattern
 *
 *		10
 *		9  8
 *		7  6  5
 *		4  3  2  1
*/

/*	T.C = 0(N^2)	
 *
 *	    = 0(N * (N + 1) / 2)
 *
 *	    = N^2 + N / 2
 *
 *	    = N^2 / 2 + N / 2
 *
 *	    = N^2
 *
 *	    = 1/2 * N^2
 *
 *	T.C = 0(N^2)
*/	

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = n * (n + 1) / 2;

		for(int i = 1; i <= n; i++){

			for(int j = 1; j <= i; j++){
				
				System.out.print(x + " ");	
				x--;
			}

			System.out.println();
		}
	}
}
