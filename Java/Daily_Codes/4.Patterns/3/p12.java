
/*	Pattern
 *
 *		A 1 B 2
 *		C 3 D 
 *		E 4
 *		F
*/

/*	T.C = 0(N^2)	
*/	

class assign{
	
	public static void main(String[] args){
		
		int n = 4;

		int x = 1, ch = 65;

		for(int i = 1; i <= n; i++){

			for(int j = 1; j <= n - i + 1; j++){
				
				if(j % 2 == 0){
					System.out.print(x++ + " ");
				}else{
					System.out.print((char)ch +" ");
					ch++;
				}
			}

			System.out.println();
		}
	}
}
