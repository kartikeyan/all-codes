
/*	Pattern - 
 *	
 *		A C E
 *		G I K
 *		M O Q
*/	

class pattern{
	
	public static void main(String[] args){

		int ch = 65;

		for(int i = 1; i <= 3; i++){

			for(int j = 1; j <= 3; j++){
			
				System.out.print((char) ch + " ");
				ch = ch+2;
			}

			System.out.println();
		}
	}
}

