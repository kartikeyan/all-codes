
/*
 	Compiled from "URI.java"
	public final class java.net.URI implements java.lang.Comparable<java.net.URI>, java.io.Serializable {
	  static final long serialVersionUID;
	  static final boolean $assertionsDisabled;
	  public java.net.URI(java.lang.String) throws java.net.URISyntaxException;
	  public java.net.URI(java.lang.String, java.lang.String, java.lang.String, int, java.lang.String, java.lang.String, java.lang.String) throws java.net.URISyntaxException;
	  public java.net.URI(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String) throws java.net.URISyntaxException;
	  public java.net.URI(java.lang.String, java.lang.String, java.lang.String, java.lang.String) throws java.net.URISyntaxException;
	  public java.net.URI(java.lang.String, java.lang.String, java.lang.String) throws java.net.URISyntaxException;
	  java.net.URI(java.lang.String, java.lang.String);
	  public static java.net.URI create(java.lang.String);
	  public java.net.URI parseServerAuthority() throws java.net.URISyntaxException;
	  public java.net.URI normalize();
	  public java.net.URI resolve(java.net.URI);
	  public java.net.URI resolve(java.lang.String);
	  public java.net.URI relativize(java.net.URI);
	  public java.net.URL toURL() throws java.net.MalformedURLException;
	  public java.lang.String getScheme();
	  public boolean isAbsolute();
	  public boolean isOpaque();
	  public java.lang.String getRawSchemeSpecificPart();
	  public java.lang.String getSchemeSpecificPart();
	  public java.lang.String getRawAuthority();
	  public java.lang.String getAuthority();
	  public java.lang.String getRawUserInfo();
	  public java.lang.String getUserInfo();
	  public java.lang.String getHost();
	  public int getPort();
	  public java.lang.String getRawPath();
	  public java.lang.String getPath();
	  public java.lang.String getRawQuery();
	  public java.lang.String getQuery();
	  public java.lang.String getRawFragment();
	  public java.lang.String getFragment();
	  public boolean equals(java.lang.Object);
	  public int hashCode();
	  public int compareTo(java.net.URI);
	  public java.lang.String toString();
	  public java.lang.String toASCIIString();
	  public int compareTo(java.lang.Object);
	  static {};
	}
*/

import java.awt.*;
import java.net.*;
import java.io.*;

class OpenWebsite{

        public static void main(String[] args) throws URISyntaxException, IOException{

                String url = "https://www.core2web.in";

                URI obj = new URI(url);

		Desktop desk = Desktop.getDesktop();

		desk.browse(obj);
        }
}

