
/*
 	Compiled from "InetAddress.java"
	public class java.net.InetAddress implements java.io.Serializable {
	  static final int IPv4;
	  static final int IPv6;
	  final transient java.net.InetAddress$InetAddressHolder holder;
	  static final java.net.InetAddressImpl impl;
	  static final java.net.spi.InetAddressResolver$LookupPolicy PLATFORM_LOOKUP_POLICY;
	  java.net.InetAddress$InetAddressHolder holder();
	  static boolean systemAddressesOrder(int);
	  static boolean ipv4AddressesFirst(int);
	  static boolean ipv6AddressesFirst(int);
	  java.net.InetAddress();
	  public boolean isMulticastAddress();
	  public boolean isAnyLocalAddress();
	  public boolean isLoopbackAddress();
	  public boolean isLinkLocalAddress();
	  public boolean isSiteLocalAddress();
	  public boolean isMCGlobal();
	  public boolean isMCNodeLocal();
	  public boolean isMCLinkLocal();
	  public boolean isMCSiteLocal();
	  public boolean isMCOrgLocal();
	  public boolean isReachable(int) throws java.io.IOException;
	  public boolean isReachable(java.net.NetworkInterface, int, int) throws java.io.IOException;
	  public java.lang.String getHostName();
	  java.lang.String getHostName(boolean);
	  public java.lang.String getCanonicalHostName();
	  public byte[] getAddress();
	  public java.lang.String getHostAddress();
	  public int hashCode();
	  public boolean equals(java.lang.Object);
	  public java.lang.String toString();
	  public static java.net.InetAddress getByAddress(java.lang.String, byte[]) throws java.net.UnknownHostException;
	  public static java.net.InetAddress getByName(java.lang.String) throws java.net.UnknownHostException;
  	public static java.net.InetAddress[] getAllByName(java.lang.String) throws java.net.UnknownHostException;
	  public static java.net.InetAddress getLoopbackAddress();
	  static java.net.InetAddress[] getAllByName0(java.lang.String, boolean) throws java.net.UnknownHostException;
	  static java.net.InetAddress[] getAddressesFromNameService(java.lang.String) throws java.net.UnknownHostException;
	  public static java.net.InetAddress getByAddress(byte[]) throws java.net.UnknownHostException;
	  public static java.net.InetAddress getLocalHost() throws java.net.UnknownHostException;
	  static java.net.InetAddress anyLocalAddress();
	  static {};
	}
*/

import java.net.*;
import java.io.*;

class IPAddress{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter Site");

		String site = br.readLine();

		InetAddress ip = InetAddress.getByName(site);	// java.net.UnknownHostException;

		System.out.println("IP Address is " + ip);
	}
}
