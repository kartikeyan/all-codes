
/*
 	Compiled from "URLConnection.java"
	public abstract class java.net.URLConnection {
	  protected java.net.URL url;
	  protected boolean doInput;
	  protected boolean doOutput;
	  protected boolean allowUserInteraction;
	  protected boolean useCaches;
	  protected long ifModifiedSince;
	  protected boolean connected;
	  static final boolean $assertionsDisabled;
	  public static java.net.FileNameMap getFileNameMap();
	  public static void setFileNameMap(java.net.FileNameMap);
	  public abstract void connect() throws java.io.IOException;
	  public void setConnectTimeout(int);
	  public int getConnectTimeout();
	  public void setReadTimeout(int);
	  public int getReadTimeout();

	  protected java.net.URLConnection(java.net.URL);	// constructor

	  public java.net.URL getURL();
	  public int getContentLength();
	  public long getContentLengthLong();
	  public java.lang.String getContentType();
	  public java.lang.String getContentEncoding();
	  public long getExpiration();
	  public long getDate();
	  public long getLastModified();
	  public java.lang.String getHeaderField(java.lang.String);
	  public java.util.Map<java.lang.String, java.util.List<java.lang.String>> getHeaderFields();
	  public int getHeaderFieldInt(java.lang.String, int);
	  public long getHeaderFieldLong(java.lang.String, long);
	  public long getHeaderFieldDate(java.lang.String, long);
	  public java.lang.String getHeaderFieldKey(int);
	  public java.lang.String getHeaderField(int);
	  public java.lang.Object getContent() throws java.io.IOException;
	  public java.lang.Object getContent(java.lang.Class<?>[]) throws java.io.IOException;
	  public java.security.Permission getPermission() throws java.io.IOException;
	  public java.io.InputStream getInputStream() throws java.io.IOException
	  public java.io.OutputStream getOutputStream() throws java.io.IOException;
	  public java.lang.String toString();
	  public void setDoInput(boolean);
	  public boolean getDoInput();
	  public void setDoOutput(boolean);
	  public boolean getDoOutput();
	  public void setAllowUserInteraction(boolean);
	  public boolean getAllowUserInteraction();
	  public static void setDefaultAllowUserInteraction(boolean);
	  public static boolean getDefaultAllowUserInteraction();
	  public void setUseCaches(boolean);
	  public boolean getUseCaches();
	  public void setIfModifiedSince(long);
	  public long getIfModifiedSince();
	  public boolean getDefaultUseCaches();
	  public void setDefaultUseCaches(boolean);
	  public static void setDefaultUseCaches(java.lang.String, boolean);
	  public static boolean getDefaultUseCaches(java.lang.String);
	  public void setRequestProperty(java.lang.String, java.lang.String);
	  public void addRequestProperty(java.lang.String, java.lang.String);
	  public java.lang.String getRequestProperty(java.lang.String);
	  public java.util.Map<java.lang.String, java.util.List<java.lang.String>> getRequestProperties();
	  public static void setDefaultRequestProperty(java.lang.String, java.lang.String);
	  public static java.lang.String getDefaultRequestProperty(java.lang.String);
	  public static synchronized void setContentHandlerFactory(java.net.ContentHandlerFactory);
	  public static java.lang.String guessContentTypeFromName(java.lang.String);
	  public static java.lang.String guessContentTypeFromStream(java.io.InputStream) throws java.io.IOException;
	  static {};
	}
*/

import java.net.*;
import java.io.*;
import java.util.*;	// for Date

class URLConnectionDemo{
	
	public static void main(String[] args) throws IOException{
		
		URL obj = new URL("https://www.core2web.in");	// URLConnection construtcor is protected that is why we are using openConnection method of URL which returns URLConnection

		URLConnection conn = obj.openConnection();

		System.out.println("Last Modified " + new Date(conn.getLastModified()));
	}
}

//	Last Modified Mon Sep 11 18:52:24 IST 2023
