
class assign{
	
	public static void main(String[] args){
		
		int x = 10;

		{	
			int x = 20;
			System.out.println(x);
		}

		{
			int x = 30;
			System.out.println(x);
		}

		System.out.println(x);
		
	}
}

/* 	error : variable x is already defined in main method 
*/  	
