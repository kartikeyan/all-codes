
/*
 	1] Client.class         
	2] 'Outer$Inner.class'   
	3] Outer.class
*/

class Outer{
	
	int x = 10;

	static int y = 20;

	class Inner{
		
		int a = 30;

		static int b = 40;
	}
}

class Client{
	
	public static void main(String[] s){
		
		System.out.println(Outer.y);

		Outer obj = new Outer();

		System.out.println(obj.x);

		Outer.Inner obj1 = obj.new Inner();

		System.out.println(obj1.a);

		System.out.println(obj1.b);
	}
}

/*
 	20
	10
	30
	40
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/15_Inner_Classes/72_Inner_And_Annoymous_Inner_Class$ javap -c Outer\$Inner.class 
	Compiled from "static_Variable_In_InnerClass_1.java"
	class Outer$Inner {
	  int a;
	
	  static int b;
	
	  final Outer this$0;

	  Outer$Inner(Outer);
	    Code:
	       0: aload_0
	       1: aload_1
	       2: putfield      #1                  // Field this$0:LOuter;
	       5: aload_0
	       6: invokespecial #7                  // Method java/lang/Object."<init>":()V
	       9: aload_0
	      10: bipush        30
	      12: putfield      #13                 // Field a:I
	      15: return

	  static {};
	    Code:
	       0: bipush        40
	       2: putstatic     #17                 // Field b:I
	       5: return
	}
*/
