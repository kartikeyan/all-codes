
/*
 	'Client$1.class'
	 Client.class
	 Demo.class
*/

class Demo{
	
	void marry(){
		
		System.out.println("Kriti Sanon");
	}

	void fun(){
		
		System.out.println("In fun");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Demo obj = new Demo(){		// internally Demo obj = new Client$1() mahnun jato
			
			void marry(){
				
				System.out.println("Karishma Mehta");
			}
			
			void fun(){
				
				System.out.println("In fun");
			}
		}.fun();
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/15_Inner_Classes/72_Inner_And_Annoymous_Inner_Class$ javac annonymous_Inner_Class_3.java 
	annonymous_Inner_Class_3.java:36: error: incompatible types: void cannot be converted to Demo
		}.fun();
		     ^
	1 error
*/
