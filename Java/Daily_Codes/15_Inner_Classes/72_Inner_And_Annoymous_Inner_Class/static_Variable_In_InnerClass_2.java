
//	static variable allowed in Inner Class from 17 Version

/*
 	1] Client.class        
	2] 'Outer$Inner.class'
	3] Outer.class
*/

class Outer{
	
	int x = 10;

	static int y = 20;

	class Inner{
		
		int a = 30;

		static int b = 40;	// internal final static int b = 40;
	}
}

class Client{
	
	public static void main(String[] args){
		
		System.out.println(Outer.y);	// 20

		Outer obj1 = new Outer();

		System.out.println(obj1.x);	// 10

		System.out.println(obj1.new Inner().b);	// 40
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/15_Inner_Classes/72_Inner_And_Annoymous_Inner_Class$ javap Outer\$Inner.class 
	Compiled from "static_Variable_In_InnerClass_2.java"
	class Outer$Inner {
	  int a;
	  static int b;
	  final Outer this$0;
	  Outer$Inner(Outer);
	  static {};
	}
*/
