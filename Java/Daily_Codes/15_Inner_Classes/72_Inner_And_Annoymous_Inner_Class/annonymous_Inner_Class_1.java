
/*
 	'Client$1.class'
	 Client.class
	 Demo.class
*/

class Demo{
	
	void marry(){
		
		System.out.println("Kriti Sanon");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Demo obj = new Demo(){		// internally Demo obj = new Client$1() mahnun jato
			
			void marry(){
				
				System.out.println("Karishma Mehta");
			}

		};

		obj.marry();	// Karishma Mehta
	}
}

/*
 	Annonymous Inner Class cha object ekdach(1) vaparla jau shakto karan ki, ithe apan ekda object create karat nahiye tar class create kartoy
*/

//	Client$1 cha parent Demo ahe

/*
 
 	katikeyan@kartikeyan:~/javacodes/Codes/15_Inner_Classes/72_Inner_And_Annoymous_Inner_Class$ javap -c Client\$1.class 
	Compiled from "annonymous_Inner_Class_1.java"
	class Client$1 extends Demo {
	  Client$1();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method Demo."<init>":()V
	       4: return

	  void marry();
	    Code:
	       0: getstatic     #7                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #13                 // String Karishma Mehta
	       5: invokevirtual #15                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return
	}

*/
