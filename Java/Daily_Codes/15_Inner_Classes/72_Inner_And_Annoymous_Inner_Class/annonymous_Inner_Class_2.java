
/*
 	'Client$1.class'
	 Client.class
	 Demo.class
*/

class Demo{
	
	void marry(){
		
		System.out.println("Kriti Sanon");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Demo obj = new Demo(){		// internally Demo obj = new Client$1() mahnun jato
			
			void marry(){
				
				System.out.println("Karishma Mehta");
			}
			
			void fun(){
				
				System.out.println("In fun");
			}
		};

		obj.fun();
	}
}

/*
 	annonymous_Inner_Class_2.java:33: error: cannot find symbol
		obj.fun();
		   ^
  symbol:   method fun()
  location: variable obj of type Demo
1 error

*/

//	fun() cha method parent class madhe pan pahije
