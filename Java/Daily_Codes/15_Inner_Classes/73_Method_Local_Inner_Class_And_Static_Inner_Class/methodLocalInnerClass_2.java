/*
	1] Client.class                   
	2] 'Outer$1Inner1.class'
	3] 'Outer$1Inner1$1Inner2.class'   
	4] Outer.class
*/

class Outer{
	
	void m1(){
		
		System.out.println("In m1-Outer");

		class Inner1{
			
			void m1(){
				
				System.out.println("In m1-Inner1");

				class Inner2{
					
					void m1(){
						
						System.out.println("In m1-Inner2");
					}
				}

				Inner2 obj = new Inner2();
				obj.m1();
			}
		}

		Inner1 obj = new Inner1();
		obj.m1();
	}

	void m2(){
		
		System.out.println("In m2-Outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj = new Outer();

		obj.m1();
	}
}

/*
 	In m1-Outer
	In m1-Inner1
	In m1-Inner2
*/
