
class Outer{
	
	Object m1(){
		
		System.out.println("In m1-outer");

		class Inner{
			
			void m1(){
				
				System.out.println("In m1-inner");
			}
		}

		return new Inner();
	}
}

class Client{
	
	public static void main(String[] s){
		
		Outer obj = new Outer();

		obj.obj.m1();
	}
}

/*
 	methodLocalInnerClass_3.java:26: error: cannot find symbol
		obj.obj.m1();
		   ^
  	symbol:   variable obj
  	location: variable obj of type Outer
	1 error
*/
