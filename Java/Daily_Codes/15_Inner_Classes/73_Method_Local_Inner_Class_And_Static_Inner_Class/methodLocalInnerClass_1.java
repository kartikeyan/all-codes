/*
	1] Client.class 
	2] 'Outer$1Inner.class'   
	3] Outer.class
*/

class Outer{
	
	void m1(){
		
		System.out.println("In m1-Outer");

		class Inner{
			
			void m1(){
				
				System.out.println("In m1-Inner");
			}
		}

		Inner obj = new Inner();
		obj.m1();
	}

	void m2(){
		
		System.out.println("In m2-Outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj = new Outer();

		obj.m1();
	}
}

/*
 	In m1-Outer
	In m1-Inner
*/
