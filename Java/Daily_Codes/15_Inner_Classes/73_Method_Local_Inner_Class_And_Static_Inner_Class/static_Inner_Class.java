/*
	1] Client.class 
	2] 'Outer$Inner.class' 
	3] Outer.class
*/

class Outer{
	
	static class Inner{
		
		void m1(){
			
			System.out.println("In m1-Inner");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer.Inner();

		obj.m1();
	}
}

/*
 	In m1-Inner
*/
