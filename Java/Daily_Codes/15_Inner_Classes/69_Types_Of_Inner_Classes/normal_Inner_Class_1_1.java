
class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("Inner-m1");
		}
	}

	void m2(){
		
		System.out.println("Outer-m2");
	}

	public static void main(String[] args){
		
		Inner obj = new Inner();
		obj.m1();
	}
}

/*
 	normal_Inner_Class_1.java:19: error: non-static variable this cannot be referenced from a static context
		Inner obj = new Inner();
		            ^
	1 error
*/
