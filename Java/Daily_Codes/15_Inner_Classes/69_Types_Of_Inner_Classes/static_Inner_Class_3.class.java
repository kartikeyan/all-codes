
/*
 	Class Names :
		
		'Outer$Inner.class'
		 Outer.class
		 Client.class
*/

class Outer{
	
	void m1(){
		
		System.out.println("In m1-outer");
	}

	static class Inner{
		
		void m1(){
			
			System.out.println("In m1-Inner");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer.Inner obj = new Outer.Inner();
		obj.m1();
	}
}

//	In m1-Inner
