
/*	
 	Class Names :-

	 	'Outer$1Inner.class'
		 Outer.class
*/

class Outer{
	
	void m1(){
		
		System.out.println("In m1-Outer");
	
		class Inner{
		
			void m1(){
			
				System.out.println("In m1-Inner");
			}
		}
		
		Inner obj = new Inner();	// object should be created here only
		obj.m1();	
	}

	void m2(){
		
		System.out.println("In m2-Outer");
	}

	public static void main(String[] args){
		
		Outer obj = new Outer();
		obj.m1();
		obj.m2();
	}
}

/*
 	In m1-Outer
	In m1-Inner
	In m2-Outer
*/
