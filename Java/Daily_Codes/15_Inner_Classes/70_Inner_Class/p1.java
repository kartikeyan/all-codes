
/*
 	 1] Client.class  
	 2] 'Outer$Inner.class'   
	 3] Outer.class
*/

class Outer{
	
	class Inner{
		
		void fun1(){
			
			System.out.println("Fun1-Inner");
		}
	}

	void fun1(){
		
		System.out.println("Fun1-Outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj1 = new Outer();
		obj1.fun1();

		Outer.Inner obj2 = obj1.new Inner();
		obj2.fun1();
	}
}

/*
 	Fun1-Outer
	Fun1-Inner
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/15_Inner_Classes/70_Inner_Class$ javap -c Outer\$Inner.class 
	Compiled from "p1.java"
	class Outer$Inner {
	  final Outer this$0;
	
	  Outer$Inner(Outer);
	    Code:
	       0: aload_0
	       1: aload_1
	       2: putfield      #1                  // Field this$0:LOuter;
	       5: aload_0
	       6: invokespecial #7                  // Method java/lang/Object."<init>":()V
	       9: return

	  void fun1();
	    Code:
	       0: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #19                 // String Fun1-Inner
	       5: invokevirtual #21                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return
	}
*/

/*
 	Outer.Inner obj = new Outer().new Inner();

		here new Outer() ==> Outer$Inner(obj, new Outer());

	Constructor will be ==> Outer$Inner(Outer$Inner this, Outer this$0);
*/	
