
//	Inner Class madhun Outer class che sagle variable, methods access hotat

class Outer{
	
	int x = 10;

	static int y = 20;

	class Inner{
		
		void fun2(){
			
			System.out.println("fun2-Inner");

			System.out.println(x);

			System.out.println(y);

			fun1();
		}
	}

	void fun1(){
		
		System.out.println("In fun1-Outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj1 = new Outer();

		Outer.Inner obj2 = obj1.new Inner();
		obj2.fun2();
	}
}

/*
 	fun2-Inner
	10
	20
	In fun1-Outer
*/
