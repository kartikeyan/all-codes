
/*
 	Type 1: Nested Inner Class 

		a] It can access any private instance variable of the outer class. 
		b] Like any other instance variable, we can have access modifier private, protected, public, and default modifier. Like class, an interface can also be nested and can have access specifiers. 

*/

class Outer{
	
	class Inner{
	
	}
}
