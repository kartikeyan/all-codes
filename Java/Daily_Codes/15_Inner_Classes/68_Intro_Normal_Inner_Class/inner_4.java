
class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("In m1-inner");
		}
	}

	void m2(){
		
		System.out.println("In m2-outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj1 = new Outer();
		obj1.m2();

		Inner obj2 = new Inner();
	}
}

/*
 	inner_4.java:25: error: cannot find symbol
		Inner obj2 = new Inner();
		^
  	symbol:   class Inner
  	location: class Client
	inner_4.java:25: error: cannot find symbol
		Inner obj2 = new Inner();
		                 ^
  	symbol:   class Inner
  	location: class Client
	2 errors
*/
