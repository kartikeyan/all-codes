
class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("In m1-inner");
		}
	}

	void m2(){
		
		System.out.println("In m2-outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj1 = new Outer();
		obj1.m2();

		Outer.Inner obj2 = new Outer().new Inner();
		obj2.m1();

		Outer.Inner obj3 = obj1.new Inner();
		obj3.m1();
	}
}

/*
 	In m2-outer
	In m1-inner
	In m1-inner
*/
