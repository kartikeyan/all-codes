
class Outer{
	
	class Inner{
		
		void m1(){
			
			System.out.println("In m1-inner");
		}
	}

	void m2(){
		
		System.out.println("In m2-outer");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Outer obj1 = new Outer();

		obj1.m1();
	}
}

/*
 	inner_3.java:24: error: cannot find symbol
		obj1.m1();
		    ^
  	symbol:   method m1()
	location: variable obj1 of type Outer
	1 error
*/
