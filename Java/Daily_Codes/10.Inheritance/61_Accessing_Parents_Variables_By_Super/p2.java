
/*

 	Q] Jar 2 classes ahet, ani doni classes madhe same variables ahet, tar parent chya variables access karaycha asel tar "super" ni access karat yeto.

*/

class Parent{
	
	int x = 10;

	static int y = 20;

	Parent(){
		
		System.out.println("Parent");
	}
}

class Child extends Parent{
	
	int x = 100;

	static int y = 200;

	Child(){
		
		System.out.println("Child");
	}

	void access(){
		
		System.out.println(super.x);

		System.out.println(super.y);

		System.out.println(x);

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();

		obj.access();
	}
}

/*
 	Parent
	Child
	10
	20
	100
	200
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/10.Inheritance_Polymorphism/61_Accessing_Parents_Variables_By_Super$ javap -c Child.class 
Compiled from "p2.java"
class Child extends Parent {
  int x;

  static int y;

  Child();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method Parent."<init>":()V
       4: aload_0
       5: bipush        100
       7: putfield      #7                  // Field x:I
      10: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      13: ldc           #19                 // String Child
      15: invokevirtual #20                 // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      18: return

  void access();
    Code:
       0: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
       3: aload_0
       4: getfield      #26                 // Field Parent.x:I
       7: invokevirtual #27                 // Method java/io/PrintStream.println:(I)V
      10: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      13: getstatic     #30                 // Field Parent.y:I
      16: invokevirtual #27                 // Method java/io/PrintStream.println:(I)V
      19: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      22: aload_0
      23: getfield      #7                  // Field x:I
      26: invokevirtual #27                 // Method java/io/PrintStream.println:(I)V
      29: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
      32: getstatic     #33                 // Field y:I
      35: invokevirtual #27                 // Method java/io/PrintStream.println:(I)V
      38: return

  static {};
    Code:
       0: sipush        200
       3: putstatic     #33                 // Field y:I
       6: return
}

*/
