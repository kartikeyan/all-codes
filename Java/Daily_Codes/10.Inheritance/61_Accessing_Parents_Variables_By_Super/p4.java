
/*

 	Q] Jar 2 classes ahet, ani doni classes madhe same variables ahet, tar parent chya variables access karaycha asel tar "super" ni access karat yeto.

*/

class Parent{
	
	int x = 10;

	static int y = 20;

	Parent(){
		
		System.out.println("Parent");
	}

	void info(){
		
		System.out.println("In info");
	}
}

class Child extends Parent{
	
	int x = 100;

	static int y = 200;

	Child(){
		
		System.out.println("Child");
	}

	void access(){
		
		System.out.println(this);

//		System.out.println(super);

		System.out.println(super.x);

		System.out.println(super.y);

		System.out.println(y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();

		obj.access();
	}
}

/*
 	Parent
	Child
	Child@251a69d7
	10
	20
	200
*/
