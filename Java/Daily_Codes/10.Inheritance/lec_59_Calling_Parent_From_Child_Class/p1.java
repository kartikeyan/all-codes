
class Parent{
	
	Parent(){			// Parent(Parent this)		== 	(ithe "this" child class cha ahe)
		
		System.out.println("IN Parent Const");
	}

	void parentProperty(){				
		
		System.out.println("Flat, Car, Gold");
	}
}

class Child extends Parent{
	
	Child(){			// Child(Child this)

		System.out.println("In Child Const");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();	// Child(obj)

		obj.parentProperty();		// parentProperty(obj)
	}
}

/*
 	IN Parent Const
	In Child Const
	Flat, Car, Gold
*/
