
/*
 	Inheritance madhe ekach object banto irrespective of parent-class relationship

	And the proof is "this reference and obj " we are printing are same that is "child reference" is passed to parent class

	1] Please note that during inheritance only the object of the subclass is created, not the superclass. 

*/

class Parent{
	
	Parent(){			// Parent(Parent this)		== 	(ithe "this" child class cha ahe)
			

		System.out.println(this);
		System.out.println("IN Parent Const");
	}
}

class Child extends Parent{
	
	Child(){			// Child(Child this)

		System.out.println(this);
		
		System.out.println("In Child Const");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Parent obj1 = new Parent();	// Parent(obj1)

		Child obj2 = new Child();	// Child(obj2)
						
		Parent obj3 = new Child();	// Parent(obj3)			// child cha object ani parent cha reference chalto

		System.out.println(obj1);

		System.out.println(obj2);

		System.out.println(obj3);
	}
}

//	here addresses are different because we have created 2 objects

/*
	Parent@7ad041f3		== Parent this
	IN Parent Const
	Child@7344699f		== Child this
	IN Parent Const
	Child@7344699f		== child this
	In Child Const
	Child@6b95977		== child this
	IN Parent Const
	Child@6b95977		== child this
	In Child Const
	Parent@7ad041f3		== parent this
	Child@7344699f		== child this
	Child@6b95977		== child this
*/

/*
 	1] In inheritance, subclass acquires super class properties. 
	2] An important point to note is, when a subclass object is created, a separate object of a superclass object will not be created. 
	3] Only a subclass object is created that has superclass variables. 
	4] This situation is different from a normal assumption that a constructor call means an object of the class is created, so we can’t blindly say that whenever a class constructor is executed, the object of that class is created or not.
	5] Note: It is mandatory that when an object is created, the constructor is for sure called but it is not mandatory when a constructor is called object creation is mandatory. 
*/
