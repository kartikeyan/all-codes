class PTC{
	
	String founder1 = "Samuel Geisberg";
	String founder2 = "Mike Payne";

	String CEO = "James Heppelmann";

	PTC(){
		
		System.out.println("Company PTC");
	}
}

class Product extends PTC{
	
	String product1 = "Onshape";
	String product2 = "Creo";
	String product3 = "ThingWorx";
	String product4 = "WindChill";

	Product(){
		
		System.out.println("founder1 is " + founder1);
		System.out.println("founder2 is " + founder2);
		System.out.println("CEO is " + CEO);

		System.out.println("Product Name is " + product1);
		System.out.println("Product Name is " + product2);
		System.out.println("Product Name is " + product3);
		System.out.println("Product Name is " + product4);
	}
}

class Client{
	
	public static void main(String[] args){
		
		PTC obj1 = new Product();
	}
}

