class Parent{
	
	int x = 10;

	Parent(){
		
		System.out.println("In Parent Const");
	}

	void access(){
		
		System.out.println("In Parent Instance");
	}
}

class Child extends Parent{
	
	int y = 20;

	Child(){
		
		System.out.println("In Child Constructor");
		System.out.println("x is " + x);
		System.out.println("y is " + y);
	}
}

class Client{
	
	public static void main(String[] args){
		
		System.out.println("In Main");

		Child obj = new Child();

		obj.access();
	}
}

/*
 	In Main.
	In Parent Const.
	In Child Constructor.
	x is 10.
	y is 20.
	In Parent Instance.
*/
