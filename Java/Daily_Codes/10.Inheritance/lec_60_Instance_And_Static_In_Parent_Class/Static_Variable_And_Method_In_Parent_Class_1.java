/*
 	1] Ekach Class madle Static block merge hotat.
	2] Vegale Class madle static block merge hot nahi.
*/	

class Parent{
	
	static{
		System.out.println("In Parent Static Block");
	}
}

class Child extends Parent{
	
	static{
		System.out.println("In Child Static Block");
	}
}

class Client{
	
	public static void main(String[] args){
		
		System.out.println("In Main");
		Child obj = new Child();
	}
}

/*
 	In Main
	In Parent Static Block
	In Child Static Block
*/
