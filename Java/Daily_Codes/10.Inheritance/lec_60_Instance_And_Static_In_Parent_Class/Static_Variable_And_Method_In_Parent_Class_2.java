/*
 	1] Ekach Class madle Static block merge hotat.
	2] Vegale Class madle static block merge hot nahi.


	1] Pratyak Special Structure chya varthi tyacha parent class cha special structure asto
*/	

class Parent{
	
	static int x = 10;

	static{
		System.out.println("In Parent Static Block");
	}

	static void access(){
		System.out.println("x is " + x);
	}
}

class Child extends Parent{
	
	static{
		System.out.println("In Child Static Block");
		System.out.println("x is " + x);
		access();
	}
}

class Client{
	
	public static void main(String[] args){
		
		System.out.println("In Main");
		Child obj = new Child();
	}
}

/*
 	In Main.
	In Parent Static Block.
	In Child Static Block.
	x is 10.
	x is 10.
*/
