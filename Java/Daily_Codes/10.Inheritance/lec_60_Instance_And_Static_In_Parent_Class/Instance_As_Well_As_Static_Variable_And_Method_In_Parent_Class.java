/*
 	1] Ekach Class madle Static block merge hotat.
	2] Vegale Class madle static block merge hot nahi.


	1] Pratyak Special Structure chya varthi tyacha parent class cha special structure asto
*/	

class Parent{
	
	int x = 10;	
	static int y = 10;

	static{
		System.out.println("In Parent Static Block");
	}

	Parent(){
		System.out.println("In Constructor");
	}

	void methodOne(){
		System.out.println("x is " + x);
		System.out.println("y is " + y);
	}

	static void methodTwo(){
		System.out.println("y is " + y);
	}
}

class Child extends Parent{
	
	static{
		System.out.println("In Child Static Block");
	}

	Child(){
		System.out.println("In Child Constructor");
	}
}

class Client{
	
	public static void main(String[] args){
		
		System.out.println("In Main");
		Child obj = new Child();
		obj.methodOne();
		obj.methodTwo();
	}
}

/*
 	In Main
	In Parent Static Block
	In Child Static Block
	In Constructor
	In Child Constructor
	x is 10
	y is 10
	y is 10
*/
