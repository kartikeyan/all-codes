import java.io.*;

class cricDemo{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Player Name");

		String name = br.readLine();

		System.out.println("Enter jersey number");

		int jerNo = Integer.parseInt(br.readLine());

		System.out.println("Enter Average");

		float avg = Float.parseFloat(br.readLine());

		System.out.println(name);

		System.out.println(jerNo);

		System.out.println(avg);
	}
}

/*	
 	bufferReader1.java:11: error: unreported exception IOException; must be caught or declared to be thrown
		String batsman = br.readLine();
		                            ^
	bufferReader1.java:15: error: unreported exception IOException; must be caught or declared to be thrown
		String bowler = br.readLine();
		                           ^
	2 errors

*/

/*	IOException error is only shown when the imported package is already imported, but if the package is not imported
 *	it will only give error to the imported class name
*/	

