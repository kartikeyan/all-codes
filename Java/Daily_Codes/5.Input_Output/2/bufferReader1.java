import java.io.*;

class cricDemo{
	
	public static void main(String[] args){
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Batsman Name");

		String batsman = br.readLine();

		System.out.println("Enter Bowler Name");

		String bowler = br.readLine();

		System.out.println("Batsman Name :" + batsman);

		System.out.println("Batsman Name :" + bowler);
	}
}

/*	
 	bufferReader1.java:11: error: unreported exception IOException; must be caught or declared to be thrown
		String batsman = br.readLine();
		                            ^
	bufferReader1.java:15: error: unreported exception IOException; must be caught or declared to be thrown
		String bowler = br.readLine();
		                           ^
	2 errors

*/

/*	IOException error is only shown when the imported package is already imported, but if the package is not imported
 *	it will only give error to the imported class name
*/	

