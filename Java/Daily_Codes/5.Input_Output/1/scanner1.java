
/*	Java User Input (Scanner)
 *
 *	Java User Input
 *
 *	The Scanner class is used to get user input, and it is found in the java.util package.
 *
 	Scanner Class Methods
 
 *	Method 		Description
 	
	nextBoolean() 	Reads a boolean value from the user
	nextByte|() 	Reads a byte value from the user
	nextDouble() 	Reads a double value from the user
	nextFloat() 	Reads a float value from the user
	nextInt() 	Reads a int value from the user
	nextLine() 	Reads a String value from the user
	nextLong() 	Reads a long value from the user
	nextShort() 	Reads a short value from the user
*/

import java.util.Scanner;

class ScannerDemo{
	
	public static void main(String[] args){
		
		Scanner obj = new Scanner(System.in);

		String name = obj.next();

		System.out.println("UserName " + name);
	}
}
