import java.io.*;

class IODemo{
	
	public static void main(String[] args){
		
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));

		//BufferedReader br2 = new BufferReader(new InputStreamReader(System.in));

		String str1 = br1.readLine();

		System.out.println("String1 = " + str1);
	}
}

/*
 *	bufferReader2.java:11: error: unreported exception IOException; must be caught or declared to be thrown
		String str1 = br1.readLine();
				 	  ^
	1 error
 *
*/
