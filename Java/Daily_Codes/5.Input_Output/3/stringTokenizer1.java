

/*	StringTokenizer Class in Java
 *
 *	1] StringTokenizer class in Java is used to break a string into tokens.
 *	2] A StringTokenizer object internally maintains a current position within the string to be tokenized.
 *	3] Some operations advance this current position past the characters processed.
 *	4] A token is returned by taking a substring of the string that was used to create the StringTokenizer object.
 *	5] The String Tokenizer class allows an application to break strings into tokens
 *	6] It implements the Enumeration interface. This class is used for parsing data.
 *	7] o use String Tokenizer class we have to specify an input string and a string that contains delimiters.
 *	8] Delimiters are the characters that separate tokens. Each character in the delimiter string is considered a valid delimiter. Default delimiters are whitespaces, new line, space, and tab. 
 *
*/

/*	Methods Of StringTokenizer Class

	Method			Action Performed

	countTokens()		Returns the total number of tokens present
	hasMoreToken()		Tests if tokens are present for the StringTokenizer’s string
	nextElement()		Returns an Object rather than String
	hasMoreElements()	Returns the same value as hasMoreToken
	nextToken()		Returns the next token from the given StringTokenizer.

*/


//	StringTokenizer Class - Utility Package madhe ahe

import java.io.*;
import java.util.*;

class StringDemo{

	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Society, Wing, Flat No");

		String info = br.readLine();

		System.out.println(info);

		StringTokenizer obj = new StringTokenizer(info, " ");	// string name, delimitizer

		String token1 = obj.nextToken();

		String token2 = obj.nextToken();

		String token3 = obj.nextToken();

		System.out.println("Society : " + token1);

		System.out.println("Wing : " + token2);

		System.out.println("Flat No : " + token3);
	}
}

