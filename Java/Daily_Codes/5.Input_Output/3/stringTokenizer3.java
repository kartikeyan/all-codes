
import java.util.*;

class StringDemo{
	
	public static void main(String[] s){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Player Info");

		String st = sc.nextLine();

		StringTokenizer str = new StringTokenizer(st, " ");

		System.out.println(str.countTokens());

		while(str.hasMoreTokens()){
			
			System.out.println(str.nextToken());
		}
	}
}

