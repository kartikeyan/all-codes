import java.io.*;

class IODemo{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
		
		InputStreamReader isr = new InputStreamReader(System.in);

//		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		String str1 = br1.readLine();

		System.out.println("String1 = " + str1);

		br1.close();

		char ch = (char)isr.read();

		System.out.println("char : " + ch);
	}
}

/*	
	kartik
	String1 = kartik
	Exception in thread "main" java.io.IOException: Stream closed
		at java.base/java.io.BufferedInputStream.getBufIfOpen(BufferedInputStream.java:176)
		at java.base/java.io.BufferedInputStream.read(BufferedInputStream.java:342)
		at java.base/sun.nio.cs.StreamDecoder.readBytes(StreamDecoder.java:284)
		at java.base/sun.nio.cs.StreamDecoder.implRead(StreamDecoder.java:326)
		at java.base/sun.nio.cs.StreamDecoder.read(StreamDecoder.java:178)
		at java.base/sun.nio.cs.StreamDecoder.read0(StreamDecoder.java:127)
		at java.base/sun.nio.cs.StreamDecoder.read(StreamDecoder.java:112)
		at java.base/java.io.InputStreamReader.read(InputStreamReader.java:164)
		at IODemo.main(bufferReader6.java:19)

*/

//	ekda eka object ne close kela tar purna keyboard through System.in cha pipe close hoto

