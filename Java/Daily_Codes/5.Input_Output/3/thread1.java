
import java.io.*;
/*
class ThreadDemo extends Thread {
	
	public void run() throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the name");

		String name = br.readLine();

		System.out.println(name);

		System.out.println("Child Thread");

		br.close();

	}

	public static void main  (String[] s) throws IOException{
		
		System.out.println("Hello");

		ThreadDemo obj = new ThreadDemo();

		obj.start();

		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter second name");
		String name2=br1.readLine();
		System.out.println(name2);
		
		System.out.println("Main Thread");
	}
}
*/

class RunnableDemo implements Runnable{
	
	public void run() throws IOException{
		
		System.out.println("Child Thread");

		BufferedReader br2 = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter second name");

		String name2=br2.readLine();
		
		System.out.println(name2);
	}

	public static void main(String[] s) throws IOException{
		
		RunnableDemo obj = new RunnableDemo();

		Thread t1 = new Thread(obj);

		BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter second name");

		String name2=br1.readLine();

		System.out.println(name2);

		t1.start();

		br1.close();

		System.out.println("In Main Thread");
	}
}
