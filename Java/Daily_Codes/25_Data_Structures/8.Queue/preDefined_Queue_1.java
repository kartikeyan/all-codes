
/*	PRogram 1 :
 *
 *	Topic : Queue
 *
 *	Types :1] Linear Queue
 *	       2] Circular Queue
 *	       3] Priority Queue
 *	       4] Deque
 *
 *	Operations : 1] Enqueue
 *		     2] Dequeue
 *		     3] Front
 *		     4] PrintQueue
 *
 *	
 *	Compiled from "Queue.java"
	public interface java.util.Queue<E> extends java.util.Collection<E> {
	  public abstract boolean add(E);
	  public abstract boolean offer(E);
	  public abstract E remove();
	  public abstract E poll();
	  public abstract E element();
	  public abstract E peek();
	}
*/

import java.util.*;

class QueueDemo{
	
	/*
	public static void main(String[] args){

		Queue q = new Queue();
	}

	
	 	preDefined_Queue_1.java:34: error: Queue is abstract; cannot be instantiated
		Queue q = new Queue();
		          ^
		1 error
	*/

	public static void main(String[] args){
		
		Queue<Integer> q = new LinkedList<>();

		q.offer(10);

		q.offer(20);

		q.offer(30);

		System.out.println(q);

		System.out.println(q.peek());

		System.out.println(q);

		System.out.println(q.poll());

		System.out.println(q);
	}
}

/*
 	[10, 20, 30]
	10
	[10, 20, 30]
	10
	[20, 30]
*/
