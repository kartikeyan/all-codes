
import java.util.*;

class DequeArray{
	
	int maxSize;
	int front;
	int rear;
	int queueArr[];

	DequeArray(int size){
		
		this.maxSize = size;

		this.queueArr = new int[size];

		this.front = -1;

		this.rear = -1;
	}
	
	void enqueueRear(int data){		// pushBack = old
		
	 	if((rear == maxSize - 1 && front == 0) || ((rear+1) % maxSize == front)){
			System.out.println("Queue is Full");
			return;
		}else if(front == -1){
			
			front = rear = 0;

		}else if(rear == maxSize - 1 && front != 0){
				
			rear = 0;
		}else{

			rear++;
		}

		queueArr[rear] = data;
	}

	void enqueueFront(int data){		// pushFront = new
		
	 	if((rear == maxSize - 1 && front == 0) || ((rear+1) % maxSize == front)){
			System.out.println("Queue is Full");
			return;
		}else if(front == -1){
			
			front = rear = 0;

		}else if(front == 0){
				
			front = maxSize - 1;
		}else{

			front = front - 1;
		}

		queueArr[front] = data;
	}

	int dequeueFront(){		// popFront = old
		
		if(front == -1){
			
			System.out.println("Queue is Empty");
			return -1;
		}else{
			
			int val = queueArr[front];

			if(front == rear){
				
				front = rear = -1;

			}else if(front == maxSize - 1){
				
				front = 0;
			}else{
				
				front++;
			}

			return val;
		}
	}

	int dequeueRear(){		// popBack = new
		
		if(front == -1){
			
			System.out.println("Queue is Empty");
			return -1;
		}else{
			
			int val = queueArr[rear];

			if(front == rear){
				
				front = rear = -1;

			}else if(rear == 0){
				
				rear = maxSize - 1;
			}else{
				
				rear = rear - 1;
			}

			return val;
		}
	}

	int Ffront(){

		if(isEmpty()){
			
			System.out.println("Queue is Empty");
			return -1;
		}else
			return queueArr[front];
	}

	int Rrear(){
		
		if(isEmpty()){
			
			System.out.println("Queue is Empty");
			return -1;
		}else

			return queueArr[rear];
	}

	boolean isEmpty(){
		
		if(front == -1)
			return true;
		else
			return false;
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Queue Size");
		int size = sc.nextInt();

		DequeArray obj = new DequeArray(size);
	
		char choice;

		do{
			System.out.println("1.EnqueueFront");
			System.out.println("2.EnqueueRear");
			System.out.println("3.DequeueFront");
			System.out.println("4.DequeueRear");
			System.out.println("5.isEmpty");
			System.out.println("6.Ffront");
			System.out.println("7.Rrear");

			System.out.println("Enter Choice");
			int ch = sc.nextInt();

			switch(ch){
				
				case 1:
					{
						System.out.println("Enter Data to Add");
						int data = sc.nextInt();

						obj.enqueueFront(data);
					}
					break;
				case 2:
					{
						System.out.println("Enter Data to Add");
						int data = sc.nextInt();

						obj.enqueueRear(data);
					}
					break;
				case 3:
					{
						int ret = obj.dequeueFront();
	
						if(ret != -1){
						
							System.out.println("Dequeue data is " + ret);
						}
					}
					break;
				case 4:
					{
						int ret = obj.dequeueRear();
	
						if(ret != -1){
						
							System.out.println("Dequeue data is " + ret);
						}
					}
					break;
				case 5:
					{
						boolean ret = obj.isEmpty();
	
						if(ret)
							System.out.println("Queue is Empty");
						else
							System.out.println("Queue is not Empty");
					}
					break;
				case 6:
					{
						int ret = obj.Ffront();

						if(ret != -1){
						
							System.out.println("Front data is " + ret);
						}
					}
					break;
				case 7:
					{
						int ret = obj.Rrear();
	
						if(ret != -1){
							
							System.out.println("Rear data is " + ret);
						}
					}
					break;
				default:
					System.out.println("Wrong Input");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);
		}while(choice == 'Y' || choice == 'y');
	}
}
