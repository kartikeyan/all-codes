import java.util.*;

class Node{
	
	int data;

	Node next = null;

	Node(int data){
		
		this.data = data;
	}
}

class CircularQueueLinkedList{

	Node front;

	Node rear;

	CircularQueueLinkedList(){

		this.rear = null;

		this.front = null;
	}
	
	void enqueue(int data){

		Node newNode = new Node(data);
		
		if(rear == null && front == null){
				
			front = rear = newNode;

			rear.next = newNode;
		}else{
				
			rear.next = newNode;

			rear = newNode;

			newNode.next = front;
		}
	}

	boolean isEmpty(){
		
		if(front == null && rear == null)
			return true;
		else
			return false;
	}

	int dequeue(){
		
		if(isEmpty()){
			
			System.out.println("Queue is Underflow");
			return -1;
		}else{
			int val;
				
			if(front == rear){

				val = front.data;

				front = null;

				rear = null;
			}else{

				val = front.data;

				front = front.next;

				rear.next = front;
			}

			return val;
		}
	}

	int Ffront(){

		if(front == null){
			
			System.out.println("Queue is Empty");
			return -1;
		}else
			return front.data;
	}

	int Rrear(){
		
		if(rear == null){
			
			System.out.println("Queue is Empty");
			return -1;
		}else

			return rear.data;
	}

	void printQueue(){
		
		if(rear == null && front == null){
			
			System.out.println("Queue is Empty");
			return;
		}else if(rear.next == rear){
			
			System.out.println("[" + rear.data + "]");
		}
		else{
			Node temp = front;

			System.out.print("[");
			while(temp.next != front){
				
				System.out.print(temp.data + " ");

				temp = temp.next;
			}

			System.out.print(temp.data);
			System.out.println("]");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		CircularQueueLinkedList obj = new CircularQueueLinkedList();

		char choice;

		do{
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("3.Empty");
			System.out.println("4.Ffront");
			System.out.println("5.Rear");
			System.out.println("6.PrintQueue");

			System.out.println("Enter Choice");
			int ch = sc.nextInt();

			switch(ch){
				
				case 1:
					{
						System.out.println("Enter Data to Add");
						int data = sc.nextInt();

						obj.enqueue(data);
					}
					break;
				case 2:
					{
						int ret = obj.dequeue();
	
						if(ret != -1){
						
							System.out.println("Dequeue data is " + ret);
						}
					}
					break;
				case 3:
					{
						boolean ret = obj.isEmpty();
	
						if(ret)
							System.out.println("Queue is Empty");
						else
							System.out.println("Queue is not Empty");
					}
					break;
				case 4:
					{
						int ret = obj.Ffront();

						if(ret != -1){
						
							System.out.println("Front data is " + ret);
						}
					}
					break;
				case 5:
					{
						int ret = obj.Rrear();
	
						if(ret != -1){
							
							System.out.println("Rear data is " + ret);
						}
					}
					break;
				case 6:
					obj.printQueue();
					break;

				default:
					System.out.println("Wrong Input");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);
		}while(choice == 'Y' || choice == 'y');
	}
}
