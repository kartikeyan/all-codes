import java.util.*;

class Node{
	
	int data;

	Node next = null;

	Node(int data){
		
		this.data = data;
	}
}

class QueueLinkedList{

	Node head = null;

	Node rear;

	QueueLinkedList(){

		this.rear = null;
	}
	
	void enqueue(int data){

		Node newNode = new Node(data);
		
		if(head == null){
				
			head = newNode;
		}else{
				
			Node temp = head;

			while(temp.next != null){
					
				temp = temp.next;
			}

			temp.next = newNode;
		}
		
		rear = newNode;
	}

	boolean isEmpty(){
		
		if(head == null)
			return true;
		else
			return false;
	}

	int dequeue(){

		int val;
		
		if(isEmpty()){
			
			System.out.println("Queue is Underflow");
			return -1;
		}else{
			
			if(head.next == null){
				
				val = head.data;

				head = null;

				rear = null;
			}else{
							
				val = head.data;

				head = head.next;
			}
		}

		return val;
	}

	int Ffront(){

		if(rear == null || head == null){
			
			System.out.println("Queue is Empty");
			return -1;
		}else
			return head.data;
	}

	int Rrear(){
		
		if(rear == null){
			
			System.out.println("Queue is Empty");
			return -1;
		}else

			return rear.data;
	}

	void printQueue(){
		
		if(rear == null){
			
			System.out.println("Queue is Empty");
			return;
		}else{
			Node temp = head;
			System.out.print("[");
			while(temp != null){
				
				System.out.print(temp.data + " ");

				temp = temp.next;
			}
			System.out.println("]");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		QueueLinkedList obj = new QueueLinkedList();

		char choice;

		do{
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("3.Empty");
			System.out.println("4.Ffront");
			System.out.println("5.Rear");
			System.out.println("6.PrintQueue");

			System.out.println("Enter Choice");
			int ch = sc.nextInt();

			switch(ch){
				
				case 1:
					{
						System.out.println("Enter Data to Add");
						int data = sc.nextInt();

						obj.enqueue(data);
					}
					break;
				case 2:
					{
						int ret = obj.dequeue();
	
						if(ret != -1){
						
							System.out.println("Dequeue data is " + ret);
						}
					}
					break;
				case 3:
					{
						boolean ret = obj.isEmpty();
	
						if(ret)
							System.out.println("Queue is Empty");
						else
							System.out.println("Queue is not Empty");
					}
					break;
				case 4:
					{
						int ret = obj.Ffront();

						if(ret != -1){
						
							System.out.println("Front data is " + ret);
						}
					}
					break;
				case 5:
					{
						int ret = obj.Rrear();
	
						if(ret != -1){
							
							System.out.println("Rear data is " + ret);
						}
					}
					break;
				case 6:
					obj.printQueue();
					break;

				default:
					System.out.println("Wrong Input");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);
		}while(choice == 'Y' || choice == 'y');
	}
}
