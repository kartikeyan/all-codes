
import java.util.*;

class QueueArray{
	
	int maxSize;
	int front;
	int rear;
	int queueArr[];

	QueueArray(int size){
		
		this.maxSize = size;

		this.queueArr = new int[size];

		this.front = -1;

		this.rear = -1;
	}
	
	void enqueue(int data){
		
		if(rear == maxSize - 1){
			
			System.out.println("Queue Overflow");
			return;
		}
			
		if(front == -1){
				
			front = rear = 0;
		}else{
			rear++;
		}

		queueArr[rear] = data;
	}

	int dequeue(){
		
		if(rear == -1){
			
			System.out.println("Queue is Underflow");
			return -1;
		}else{
			
			int val = queueArr[front];

			front++;

			if(front > rear){
				
				front = rear = -1;
			}

			return val;
		}
	}

	int Ffront(){

		if(front == -1){
			
			System.out.println("Queue is Empty");
			return -1;
		}else
			return queueArr[front];
	}

	int Rrear(){
		
		if(rear == -1){
			
			System.out.println("Queue is Empty");
			return -1;
		}else

			return queueArr[rear];
	}

	boolean isEmpty(){
		
		if(front == -1)
			return true;
		else
			return false;
	}

	void printQueue(){
		
		if(front == -1){
			
			System.out.println("Queue is Empty");
			return;
		}else{
			System.out.print("[");
			for(int i = front; i <= rear; i++){
				
				System.out.print(queueArr[i] + " ");
			}
			System.out.println("]");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Queue Size");
		int size = sc.nextInt();

		QueueArray obj = new QueueArray(size);
	

		char choice;

		do{
			System.out.println("1.Enqueue");
			System.out.println("2.Dequeue");
			System.out.println("3.Empty");
			System.out.println("4.Ffront");
			System.out.println("5.Rear");
			System.out.println("6.PrintQueue");

			System.out.println("Enter Choice");
			int ch = sc.nextInt();

			switch(ch){
				
				case 1:
					{
						System.out.println("Enter Data to Add");
						int data = sc.nextInt();

						obj.enqueue(data);
					}
					break;
				case 2:
					{
						int ret = obj.dequeue();
	
						if(ret != -1){
						
							System.out.println("Dequeue data is " + ret);
						}
					}
					break;
				case 3:
					{
						boolean ret = obj.isEmpty();
	
						if(ret)
							System.out.println("Queue is Empty");
						else
							System.out.println("Queue is not Empty");
					}
					break;
				case 4:
					{
						int ret = obj.Ffront();

						if(ret != -1){
						
							System.out.println("Front data is " + ret);
						}
					}
					break;
				case 5:
					{
						int ret = obj.Rrear();
	
						if(ret != -1){
							
							System.out.println("Rear data is " + ret);
						}
					}
					break;
				case 6:
					obj.printQueue();
					break;

				default:
					System.out.println("Wrong Input");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);
		}while(choice == 'Y' || choice == 'y');
	}
}
