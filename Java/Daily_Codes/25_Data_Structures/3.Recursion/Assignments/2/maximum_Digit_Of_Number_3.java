
//	WAP to print maximum Digit in a given number

class Sol{

	static int max = Integer.MIN_VALUE;

	static void maxDigit(int n){
/*		
		int max = Integer.MIN_VALUE;

		while(n > 0){
			
			int rem = n % 10;

			if(rem > max){
				
				max = rem;
			}

			n = n / 10;
		}

		return max;
*/
		if(n / 10 == 0){
			
			return ;
		}		

		if(n % 10 > max){
			
			max = n % 10;
		}

		maxDigit(n / 10);
	}
	
	public static void main(String[] args){
		
		int n = 123;

		maxDigit(n);

		System.out.println("Maximum Digit is " + max);
	}
}
