
/*
 	For a number to be a Strong Number, the sum of Factorial of each digit of the number must be equal to the number itself.
*/

class Sol{
/*
	static int factorial(int n){
		
		int fact = 1;

		for(int i = 2; i <= n; i++){
			
			fact = fact * i;
		}

		return fact;
	}

	static boolean strong(int n){
		
		int sum = 0, temp = n;

		while(n != 0){
			
			int rem = n % 10;

			sum = sum + factorial(rem);

			n = n / 10;
		}

		if(sum == temp){
			
			return true;
		}else
			return false;
	}
*/

	static int factorial(int n){
		
		if(n == 0){
			
			return 1;
		}

		return n * factorial(n-1);
	}
/*
	static boolean strong(int n){
		
		int sum = 0, temp = n;

		while(n != 0){
			
			int rem = n % 10;

			sum = sum + factorial(rem);

			n = n / 10;
		}
		
		return sum == temp;
	}

	public static void main(String[] args){
		
		int n = 145;

		boolean ret = strong(n);

		if(ret == true){
			
			System.out.println("Strong Num");
		}else
			System.out.println("Not Strong");
	}
*/
	static boolean strong(int n, int temp, int sum){
		
		if(n == 0){
			
			return temp == sum;
		}else{
			
			int digit = n % 10;

			int fact = factorial(digit);

			return strong(n / 10, temp, sum + fact);
		}
	}

	public static void main(String[] args){
		
		boolean ret = strong(145, 145, 0);

		if(ret == true){
			
			System.out.println("Strong Number");
		}else
			System.out.println("Not Strong Number");
	}
}
