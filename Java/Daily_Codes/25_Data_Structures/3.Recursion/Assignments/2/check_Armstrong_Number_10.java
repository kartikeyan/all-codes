
class Sol{
/*
	static boolean armstrong(int N){

		int sum = 0, count = 0, temp = N, rem;
		
		while(N != 0){
			
			count++;
			N = N / 10;
		}
		
		System.out.println(count);

		N = temp;

		while(N != 0){
			
			rem = N % 10;

			int mult = 1;

			for(int i = 1; i <= count; i++){
				
				mult = mult * rem;
			}

			System.out.println(mult);

			sum = sum + mult;

			N = N / 10;
		}

		return sum == temp;
	}
*/
	static int calculate(int n, int sum){
		
		if(n == 0){
			
			return sum;
		}

		int digit = n % 10;

		return calculate(n / 10, sum + (int) Math.pow(digit, (int) Math.log10(n) + 1));
	}

	static boolean armstrong(int n){
		
		int sum = calculate(n, 0);

		return sum == n;
	}

	public static void main(String[] args){
		
		int n = 6;

		boolean ret = armstrong(n);

		if(ret == true)
			System.out.println("Armstrong Number");
		else
			System.out.println("Not Armstrong Num");
	}
}
