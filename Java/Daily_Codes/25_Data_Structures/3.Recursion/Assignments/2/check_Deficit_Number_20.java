
//	A Deficit number is a positive integer where the sum
//	of its proper divisors is less than number itself

class Sol{

	static boolean deficiet(int n, int divisor, int sum){
		
		if(n <= 1){
			
			return false;
		}

		if(divisor > Math.sqrt(n)){
			
			return sum < n;
		}

		if(n % divisor == 0){
			
			sum += divisor;

			if(divisor != n / divisor){
				
				sum += n / divisor;
			}
		}

		return deficiet(n, divisor+1, sum);
	}

	public static void main(String[] args){
		
		int n = 6;

		if(deficiet(n,1,1)){
			
			System.out.println(n + " is deficiet number");
		}else
			System.out.println(n + " is not deficiet number");
	}
/*	
	static boolean deficiet(int n){
		
		if(n <= 1){
			
			return false;
		}

		int sum = 1;

		for(int i = 2; i <= Math.sqrt(n); i++){
			
			if(n % i == 0){
				
				sum += i;
			}

			if(i != n / i){
				
				sum += n / i;
			}
		}

		return sum < n;
	}
	
	public static void main(String[] args){
		
		int n = 6;

		if(deficiet(n)){
			
			System.out.println(n + " is deficiet number");
		}else
			System.out.println(n + " is not deficiet number");
	}
*/	
}
