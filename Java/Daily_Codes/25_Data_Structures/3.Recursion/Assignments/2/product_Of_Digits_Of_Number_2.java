
//	WAP to print product of digits of given number

class Sol{

	static int product(int n){
/*		
		int prod = 1;

		while(n != 0){
			
			prod = prod * n % 10;

			n = n / 10;
		}

		return prod;
*/
		if(n < 10){
			
			return n;
		}

		int lastDigit = n % 10;

		int remainingDigits = product(n / 10);

		return lastDigit * remainingDigits;
	}
	
	public static void main(String[] args){
		
		int n = 123;
		
		int ret = product(n);

		System.out.println("Product of Digits is " + ret);
	}
}
