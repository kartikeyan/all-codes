
//	WAP to check if given number is Magic Number or Not.
//	(A magic number is a number in which eventual sum of 
//	 digits is equal to 1)

class Sol{

//	static boolean magic(int n){
		// Approach - 1
	/*	
		int sum = 0;

		while(n != 0){
			
			int rem = n % 10;

			sum = sum + rem;

			n = n / 10;
		}
		
		int s1 = 0;

		while(sum != 0){
			
			s1 = s1 + sum % 10;

			sum = sum / 10;
		}
		
		return s1 == 1;
	*/
		// Appraoch - 2

/*		
		int sum = 0;

		while(n > 0 || sum > 9){
			
			if(n == 0){
				
				n = sum;
				sum = 0;
			}

			sum = sum + n % 10;

			n = n / 10;
		}

		return sum == 1;
*/
		//	Appraoch 3
		
		// return (n % 9 == 1);

	static boolean magic(int n){
			
		if(n <= 9){
				
			return n == 1;
		}else{
			int sum = calculateSum(n);
				
			return magic(sum);
		}
	}

	static int calculateSum(int n){
			
		if(n == 0){
			
			return 0;
		}else{
				
			return n % 10 + calculateSum(n / 10);
		}
	}
	
	public static void main(String[] args){
		
		int n = 50113;

		boolean ret = magic(n);

		if(ret == true)
			System.out.println("MAgic Number");
		else
			System.out.println("Not Magic Number");
	}
}
