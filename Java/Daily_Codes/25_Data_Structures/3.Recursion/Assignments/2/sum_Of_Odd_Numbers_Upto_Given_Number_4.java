//      WAP to print sum of odd numbers up to given number

class Sol{
	
	static int sum = 0;

	static void sumOdd(int n){
		
		if(n == 0){
			
			return;
		}

		if(n % 2 != 0){

			sum += n;
		}

		sumOdd(n - 1);
	}	

	public static void main(String[] args){
		
		sumOdd(5);

		System.out.println(sum);
	}

/*	static int sumOdd(int n){
		
		int sum = 0;

		for(int i = 1; i <= n; i++){
			
			if(i % 2 == 1){
				
				sum = sum + i;
			}
		}

		return sum;
*/
/*	
		if(n == 1){
			
			return 1;
		}

		if(n % 2 == 1){
			
			return n + sumOdd(n-1);
		}else
			return sumOdd(n-1);
	}
	
	public static void main(String[] args){
		
		int n = 4;

		int ret = sumOdd(n);

		System.out.println("Sum of odd numbers is " + ret);
	}
*/
}
