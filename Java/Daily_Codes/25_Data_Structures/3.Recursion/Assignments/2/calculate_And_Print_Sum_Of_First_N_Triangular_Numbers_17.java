
/*
 	WAP to calculate and print the sum of first N triangular numbers.
*/

class Sol{

	static int sumTri(int n){
		
		if(n == 0){
			
			return 0;
		}

		int temp = n;

		int tri = temp * (temp + 1) / 2;

		return tri + sumTri(n - 1);
	}
/*	
	static int sumTri(int n){
		
		int sum = 0;

		for(int i = 1; i <= n; i++){
			
			int tri = i * (i + 1) / 2;

			sum += tri;
		}

		return sum;
	}
*/
	public static void main(String[] args){
		
		int n = 5;

		int re = sumTri(n);

		System.out.println("Sum of first " + n + " triangular numbers is : " + re);
	}
}
