
//	WAP to determine whether given positive integer is sad or not

import java.util.*;

class Sol{

	static int sumOfSquares(int n, int sum){
		
		if(n == 0){
			
			return sum;
		}

		int digit = n % 10;

		return sumOfSquares(n / 10, sum + digit * digit);
	}

	static boolean sad(int n, Set<Integer> seen){
		
		if(n == 1){
			
			return true;
		}

		if(!seen.add(n)){
			
			return false;
		}

		int sum = sumOfSquares(n, 0);

		return sad(sum, seen);
	}
	
	public static void main(String[] args){
		
		int n = 45;

		if(sad(n, new HashSet<>())){
			
			System.out.println(n + " is sad number");
		}else
			System.out.println(n + " is not sad number");
	}

/*	
	static boolean sad(int n){
	
		Set<Integer> sawNum = new HashSet<>();

		while(n != 1 && sawNum.add(n)){
			
			int sum = 0;

			while(n > 0){
				
				int temp = n % 10;

				sum += temp * temp;

				n /= 10;
			}

			n = sum;
		}

		return n == 1;
	}

	public static void main(String[] args){
		
		int n = 45;

		if(sad(n)){
			
			System.out.println(n + " is sad number");
		}else
			System.out.println(n + " is not sad number");
	}
*/
}
