
//	An Abundant number is the sum of all its proper
//	divisors, denoted by sum(n), is greater than the
//	number's value

class Sol{

	static boolean abundant(int n){
/*		
		int sum = 0;

		for(int i = 1; i < n; i++){
			
			if(n % i == 0){
				
				sum = sum + i;
			}
		}

		return sum > n;
*/
		if(isAbundant(n, n - 1, 0)){
			
			return true;
		}else
			return false;
	}

	static boolean isAbundant(int n, int divisor, int sum){
		
		if(n <= 1){
			
			return false;
		}

		sum = sumOfDivisors(n, divisor, sum);

		return sum > n;
	}

	static int sumOfDivisors(int n, int divisor, int sum){
		
		if(divisor == 1){
			
			return sum;
		}

		if(n % divisor == 0){
			
			sum = sum + divisor;
		}

		return sumOfDivisors(n, divisor - 1, sum);
	}
	
	public static void main(String[] args){
		
		int n = 6;

		boolean ret = abundant(n);

		if(ret == true)
			System.out.println("Abundant Number");
		else
			System.out.println("Not Abundant Num");
	}
}
