
//     An Happy Number is number which eventually reaches
//     1 when replaced by the sum of square of each digit

import java.util.*;

class Sol{

	static boolean happy(int n, Set<Integer> sawNum){
		
		if(n == 1){
			
			return true;
		}

		if(!sawNum.add(n)){
			
			return false;
		}

		int sum = squareSum(n);

		return happy(sum, sawNum);
	}

	static int squareSum(int n){
		
		if(n == 0){
			
			return 0;
		}else{
			
			return (n % 10) * (n % 10) + squareSum(n / 10);
		}
	}

	public static void main(String[] args){
		
		int n = 19;

		if(happy(n, new HashSet<>())){
			
			System.out.println(n + " is happy number");
		}else{
			
			System.out.println(n + " is not happy number");
		}
	}
/*
	static int sumOfSquares(int n){
		
		int sum = 0;

		while(n > 0){
			
			int digit = n % 10;

			sum = sum + (digit * digit);

			n = n / 10;
		}

		return sum;
	}

	static boolean happy(int n){
	
		// Iterative
/*		
		int slow = n, fast = n;

		do{
			
			slow = sumOfSquares(slow);

			fast = sumOfSquares(sumOfSquares(fast));
		}while(fast != slow);

		return slow == 1;
*/
/*	
		if(n == 1){
			
			return true;
		}

		if(n == 4){
			
			return false;
		}

		return happy(sumOfSquares(n));
*/
//	}
	/*	
	public static void main(String[] args){
		
		int n = 19;

		boolean ret = happy(n);

		if(ret == true)
			System.out.println("Strong Number");
		else
			System.out.println("Not Strong Num");
	}
	*/
}
