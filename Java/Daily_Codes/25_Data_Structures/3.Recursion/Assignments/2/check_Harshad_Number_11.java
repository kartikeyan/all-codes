
//	A Harshad number is a number that is divisble by the 
//	sum of its digit

class Sol{

	static boolean harshad(int n){
/*		
		int sum = 0;

		while(n != 0){
			
			sum = sum + n % 10;

			n = n / 10;
		}

		return(n % sum == 0);
*/	
		int sum = sumOfDigits(n);

		return (n % sum) == 0;
	}

	static int sumOfDigits(int n){

		if(n == 0){
			
			return 0;
		}

		return n % 10 + sumOfDigits(n / 10);
	}
	
	public static void main(String[] args){
		
		int n = 3;

		boolean ret = harshad(n);

		if(ret == true)
			System.out.println("Harshad Number");
		else
			System.out.println("Not Harshad Num");
	}
}
