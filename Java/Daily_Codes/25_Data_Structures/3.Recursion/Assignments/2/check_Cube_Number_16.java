
//	A cube number is defined as a number that is the 
//	cube of an integer

class Sol{
	

	static boolean cubeNumber(int n, int x){
		
		int cube = x * x * x;

		if(cube == n){
			
			return true;
		}else if(cube > n){
			
			return false;
		}else
			return cubeNumber(n, x + 1);
	}

	public static void main(String[] args){
		
		int n = 125;

		if(cubeNumber(n, 1)){
			
			System.out.println(n + " is cube number");
		}else
			System.out.println(n + " is not cube number");
	}

/*	
	static boolean cube(int n){
		
		int x = 1;

		while(x * x * x <= n){
			
			if(x * x * x == n){
				
				return true;
			}

			x++;
		}

		return false;
	}

	public static void main(String[] args){
		
		int n = 125;

		if(cube(n)){
			
			System.out.println(n + " is cube number");
		}else
			System.out.println(n + " is not cube number");
	}

	*/
}
