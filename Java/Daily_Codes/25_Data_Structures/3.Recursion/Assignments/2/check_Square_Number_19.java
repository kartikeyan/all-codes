
class Sol{

	static boolean square(int n, int current){
		
		if(n < 0){
			
			return false;
		}

		if(n == 0){
			
			return true;
		}

		if(n < current){
			
			return false;
		}

		return square(n - current, current + 2);
	}
	
	public static void main(String[] args){
		
		int n = 25;

		if(square(n,1)){
			
			System.out.println(n + " is sqare number");
		}else
			System.out.println(n + " is not square number");
	}

/*
	static boolean square(int n){
		
		if(n < 0){
			
			return false;
		}

		int sqrt = (int) Math.sqrt(n);

		return sqrt * sqrt == n;
	}

	public static void main(String[] args){
		
		int n = 25;

		if(square(n)){
			
			System.out.println(n + " is sqare number");
		}else
			System.out.println(n + " is not square number");
	}
*/
}
