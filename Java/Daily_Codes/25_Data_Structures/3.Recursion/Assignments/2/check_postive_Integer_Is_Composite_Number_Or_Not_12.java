
//	Determine whether a given positive integer is a 
//	compostive number or not

class Sol{

/*	
	static boolean composite(int n){
		
		int c = 0;

		for(int i = 1; i <= n; i++){
			
			if(n % i == 0){
				
				c++;
			}
		}

		if(c > 2){
			
			return true;
		}else
			return false;

	}
*/
	static boolean composite(int n){
		
		if(isComposite(n, 2)){

			return true;
		}else
			return false;
	}

	static boolean isComposite(int n, int divisor){
		
		if(n <= 1){
			
			return false;
		}

		if(divisor >= n){
			
			return false;
		}

		if(n % divisor == 0){
			
			return true;
		}

		return isComposite(n, divisor + 1);
	}

	public static void main(String[] args){
		
		int n = 6;

		boolean ret = composite(n);

		if(ret == true)
			System.out.println("Composite Number");
		else
			System.out.println("Not Composite Num");
	}
}
