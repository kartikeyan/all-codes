
//	A perfect number is positive integer that is equal to 
//	sum of its proper divisiors, excluding itself

class Sol{

	static boolean perfect(int n, int divisor, int sum){
		
		if(divisor > n / 2){
			
			return sum == n;
		}

		if(n % divisor == 0){
			
			sum += divisor;
		}

		return perfect(n, divisor + 1, sum);
	}

	public static void main(String[] args){

		int n = 6;
		
		if(perfect(n, 1, 0)){
			
			System.out.println(n + " is perfect number");
		}else
			System.out.println(n + " is not perfect number");
	}

/*	
	static int divisorSum(int n, int divisor){
		
		if(divisor == 1){
			
			return 1;
		}

		if(n % divisor == 0){
			
			return divisor + divisorSum(n, divisor-1);
		}

		return divisorSum(n, divisor-1);
	}

	static boolean perfect(int n){
		
		// Iterative
/*		
		int temp = n, sum = 0;

		for(int i = 1; i < n; i++){
			
			if(n % i == 0){
				
				sum = sum + i;
			}
		}

		return temp == sum;
*/
		// Recursive
/*	
		int temp = divisorSum(n , n-1);

		return temp == n;		
	}
*/	
	/*
	public static void main(String[] args){
		
		int n = 6;

		boolean ret = perfect(n);

		if(ret == true)
			System.out.println("Perfect Number");
		else
			System.out.println("Not Perfect Num");
	}
	*/
}
