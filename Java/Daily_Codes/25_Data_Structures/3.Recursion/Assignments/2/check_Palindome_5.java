

class Sol{

	static int reverse(int n, int rev){
		
		if(n == 0){
			
			return rev;
		}

		rev = rev * 10 + n % 10;

		return reverse(n/10,rev); 
	}

	public static void main(String[] args){
		
		int n = 121;

		int rev = 0;

		int ret = reverse(n,rev);

		if(ret == n){

			System.out.println("Palindrome");
		}else
			System.out.println("Not Palindrome");
	}
}
