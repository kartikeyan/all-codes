
//	A Duck number is a number which doesn't start with 
//	zero but has at least one digit as zero


class Sol{

	static boolean duck(int n){
		
		if(n == 0){
			
			return false;
		}

		int last = n % 10;

		if(last == 0){
			
			return true;
		}

		return duck(n / 10);
	}

	public static void main(String[] args){
		
		int n = 100;

		if(duck(n)){
			
			System.out.println(n + " is duck number");
		}else
			System.out.println(n + " is not duck number");
	}

/*	
	static boolean duck(String s){

		int zero = 0;

		for(int i = 0; i < s.length(); i++){
			
			char ch = s.charAt(i);

			if(ch == '0'){
				
				zero++;
			}
		}	

		char firstDigit = s.charAt(0);

		if(firstDigit != '0' && zero > 0){
			
			return true;
		}else{

			return false;
		}
	}

	public static void main(String[] args){
		
		String n = "60";

		boolean ret = duck(n);

		if(ret == true)
			System.out.println("Duck Number");
		else
			System.out.println("Not Duck Num");
	}
*/
}

