
//	An automorphic number is a number whose square ends
//	with the same digits as the number itself

class Sol{

	static boolean automorphic(int n){
/*		
		int square = n * n;

		while(n != 0){
			
			if(n % 10 != square % 10){
				
				return false;
			}

			square /= 10;
			n /= 10;
		}

		return true;
*/
		if(isAutomorphic(n, n * n)){
			
			return true;
		}else
			return false;
	}

	static boolean isAutomorphic(int n, int square){
		
		if(n == 0){
			
			return true;
		}

		int digit = n % 10;
		int lastDigit = square % 10;

		if(digit != lastDigit){
			
			return false;
		}

		return isAutomorphic(n / 10, square / 10);
	}

        public static void main(String[] args){

                int n = 6;

                boolean ret = automorphic(n);

                if(ret == true)
                        System.out.println("Automorphic Number");
                else
                        System.out.println("Not Automorphic Num");
        }
}

