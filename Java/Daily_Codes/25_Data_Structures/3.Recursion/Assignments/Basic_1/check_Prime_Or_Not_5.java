
//	WAP to check whether number is prime or not

/*
 	int c = 0;

	for(int i = 1; i <= n; i++){
		
		if(n % i == 0){
			
			c++;
		}

		if(c > 2){
			
			System.out.println("Not a Prime");
		}
	}

	if(c == 2){
		
		System.out.println("Prime");
	}
*/

class Sol{

	static boolean prime(int n, int i){
		
		if(n == 2){
			
			return true;
		}		

		if(n < 2){
			
			return false;
		}

		if(n % i == 0){
			
			return false;
		}

		if(i * i > n){
			
			return true;
		}

		return prime(n, i+1);
	}
	
	public static void main(String[] args){
		
		int n = 10;

		int i = 2;

		boolean ret = prime(n,i);
		
		if(ret == true)
			System.out.println("Prime");
		else
			System.out.println("Not a Prime No");
	}
}
