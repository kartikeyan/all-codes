
//	WAP to calculate sum of digits of given positive integer

/*
 	int sum = 0;

	while(n != ){
		
		sum = n % 10;

		n = n / 10;
	}
		
	System.out.println(sum);
	
*/

class Sol{

	static int digitSum(int n){

		if(n == 0){
			
			return 0;
		}

		return n % 10 + digitSum(n / 10);
	}
	
	public static void main(String[] args){
		
		int n = 158;

		int ret = digitSum(n);

		System.out.println("Sum of digits are " + ret);
	}
}
