
//	WAP to display the first 10 natural numbers in reverse order

class Sol{

	static void print(int n){
		
		if(n == 0)

			return;
		
		System.out.println(n);

		print(n-1);
	}
	
	public static void main(String[] args){
		
		int n = 10;

		print(n);
	}
}
