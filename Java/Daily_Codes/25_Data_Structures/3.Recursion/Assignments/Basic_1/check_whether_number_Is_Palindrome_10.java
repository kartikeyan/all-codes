
//	WAP to check whether given number is palindrome or not

/*
	int n = 121, temp = n, rem, rev = 0;

	while(n != 0){
			
		rem = n % 10;
		rev = rem + rev * 10;
		n = n / 10;
	}

	System.out.println(rev);

	if(temp == rev){
		
		System.out.println("Palindrome");
	}else{
		
		System.out.println("Not Palindrome");
	}
*/

class Sol{

	static int reverse(int n, int rev){
		
		if(n == 0){
			
			return rev;
		}

		rev = rev * 10 + n % 10;

		return reverse(n/10, rev);
	}

	static void palidrome(int n){

		int rev = 0;

		int ret = reverse(n, rev); 

		if(n == ret){
			
			System.out.println("Palindrome number");
		}else
			System.out.println("Not Palindrome number");
	}
	
	public static void main(String[] args){
		
		int n = 141;

		palidrome(n);
	}
}
