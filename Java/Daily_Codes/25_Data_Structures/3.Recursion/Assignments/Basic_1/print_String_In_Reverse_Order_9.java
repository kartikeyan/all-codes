
//	WAP to print string in reverse order

class Sol{
	
	static void reverse(String str, int index){
		
		if(index < str.length()){
			
			reverse(str, index+1);

			System.out.println(str.charAt(index));
		}
	}

	public static void main(String[] args){
		
		String str = "Kartik";

		reverse(str,0);
	}
/*
	static String reverse(String str){

		if(str == null || str.length() <= 1){
			
			return str;
		}else{
			char first = str.charAt(0);

			String remaining = str.substring(1);

			return reverse(remaining) + first;
		}
	}

	public static void main(String[] args){
		
		String str = "kartik";

		String ret = reverse(str);

		System.out.println("Reverse String is " + ret);
	}
*/
}

//	Reverse String is kitrak
