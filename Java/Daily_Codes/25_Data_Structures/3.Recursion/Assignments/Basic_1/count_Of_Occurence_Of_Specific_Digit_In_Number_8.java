
//	count_Of_Occurence_Of_Specific_Digit_In_Number

class Sol{

	static int countOcc(int n, int x){
		
		if(n == 0){
			
			return 0;
		}

		if(n % 10 == x){
			
			return 1 + countOcc(n / 10, x);
		}

		return countOcc(n / 10, x);
	}
	
	public static void main(String[] args){
	
		int n = 1442244;

		int x = 1;

		int ret = countOcc(n, x);

		System.out.println("Occurences of " + x + " is " + ret);
	}
}
