
//	WAP to print count of digits in number


class Sol{

	static int digits(int n){
		
		if(n == 0){
			
			return 1;
		}		

		return digits(n/10) + 1;
	}
	
	public static void main(String[] args){
		
		int n = 0;

		int ret = digits(n);

		System.out.println("Length of digits is " + ret);
	}
}
