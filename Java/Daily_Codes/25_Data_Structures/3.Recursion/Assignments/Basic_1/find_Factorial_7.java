
//	WAP to calculate factorial of number

/*
 	int prod = 1;

	for(int i = 2; i <= n; i++){
		
		prod = prod * i;
	}
		
	System.out.println(prod);
*/

class Sol{

	static int factorial(int n){

		if(n == 0){
			
			return 1;
		}

		return factorial(n - 1) * n;
	}
	
	public static void main(String[] args){
		
		int n = 5;

		int ret = factorial(n);

		System.out.println("Factorial Of Num is " + ret);
	}
}
