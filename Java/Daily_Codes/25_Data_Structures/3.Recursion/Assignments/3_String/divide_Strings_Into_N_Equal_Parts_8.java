
/*
 	Input : str = aaaabbbbcccc

		n = 3

	Output : aaaa
		 bbbb
		 cccc
*/

class Sol{
/*	
	public static void main(String[] args){
		
		String str = "aaaabbbbcccc";

		int n = 4;

		int size = str.length() / n;

		String[] strArr = new String[size];

		for(int i = 0; i < size; i++){
			
			strArr[i] = str.substring(n * i, n * (i + 1));
		}

		for(int i = 0; i < size; i++){
			
			System.out.println(strArr[i]);
		}
	}
*/

	static String[] nEqual(String str, int n, String[] strArr, int size){
		
		int temp = (strArr.length) - size;

		if(temp == strArr.length){
			
			return strArr;
		}

		strArr[temp] = str.substring(n * temp, n * (temp + 1));

		return nEqual(str, n, strArr, size-1);
	}

	public static void main(String[] args){
		
		String str = "aaaabbbbccccdddd";

		int n = 4;

		int size = str.length() / n;

		String[] strArr = new String[size];

		String[] strArr1 = nEqual(str, n, strArr, size);

		for(int i = 0; i < size; i++){
			
			System.out.println(strArr1[i]);
		}
	}
}
