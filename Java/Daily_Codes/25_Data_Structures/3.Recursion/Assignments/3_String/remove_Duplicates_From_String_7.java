
/*
 	Input : str = HappyNewYear

	Output : HapyNewYr
*/

class Sol{
/*
	static String removeDup(String str){
		
		if(str.isEmpty()){
			
			return str;
		}

		char firstChar = str.charAt(0);

		String remaining = removeDup(str.substring(1).replaceAll(String.valueOf(firstChar), ""));

		return firstChar + remaining;
	}
	
	public static void main(String[] args){
	
		String str = "HappyNewYear";

		String ret = removeDup(str);

		System.out.println("Original String is " + str);

		System.out.println("String after removing duplicates " + ret);
	}
*/

	static String removeDup(String str){
		
		if(str.isEmpty()){
			
			return str;
		}

		char firstChar = str.charAt(0);

		String remaining = removeDup(str.substring(1).replaceAll(String.valueOf(firstChar), ""));

		return firstChar + remaining;
	}
	
	public static void main(String[] args){
	
		String str = "HappyNewYear";

		String ret = removeDup(str);

		System.out.println("Original String is " + str);

		System.out.println("String after removing duplicates " + ret);
	}
}
