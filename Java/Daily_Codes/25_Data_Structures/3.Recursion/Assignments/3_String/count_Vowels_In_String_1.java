
import java.util.*;

class Sol{
/*
	static int countVowel(String str){
		
		int count = 0;

		for(int i = 0; i < str.length(); i++){
			
			char ch = str.charAt(i);

			if(ch == 'a' || ch == 'e' ||ch == 'i' ||ch == 'o' ||ch == 'u' ||ch == 'A' ||ch == 'E' ||ch == 'I' ||ch == 'O' ||ch == 'U'){
				count++;
			}
		}

		return count;
	}
*/
	static int countVowel(String str){
		
		if(str.isEmpty()){
			
			return 0;
		}else{
			
			int count;

			char ch = str.charAt(0);

			if(ch == 'a' || ch == 'e' ||ch == 'i' ||ch == 'o' ||ch == 'u' ||ch == 'A' ||ch == 'E' ||ch == 'I' ||ch == 'O' ||ch == 'U'){
				count = 1;
			}else{
				count = 0;
			}

			return count + countVowel(str.substring(1));
		}
	}

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String");
		String str = sc.nextLine();

		int count = countVowel(str);

		System.out.println("Count of Vowels in String " + count);
	}
}
