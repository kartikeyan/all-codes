
/*
 	Given two strings str1 and str2, return true if str2 is an anagram of str1, and return false

	Input : str1 = "listen",  str2 = "silent"

	Output : true
*/

class Sol{
/*	
	static boolean isAnagram(String str1, String str2){
		
		if(str1.length() != str2.length()){
			
			return false;
		}

		int []count = new int[256];

		for(int i = 0; i < str1.length(); i++){
			
			count[str1.charAt(i)]++;

			count[str2.charAt(i)]--;
		}

		for(int val : count){
			
			if(val != 0){
				
				return false;
			}
		}

		return true;
	}
*/
	static boolean isAnagram(String str1, String str2){
		
		if(str1.isEmpty() && str2.isEmpty()){
			
			return true;
		}

		if(str1.length() != str2.length()){
			
			return false;
		}

		int index = str2.indexOf(str1.charAt(0));

		if(index != -1){
			
			String temp1 = str1.substring(1);

			String temp2 = str2.substring(0, index) + str2.substring(index + 1);

			return isAnagram(temp1, temp2);
		}else{
			
			return false;
		}
	}

	public static void main(String[] args){
		
		String str1 = "listen";
		String str2 = "silent";

		boolean check = isAnagram(str1, str2);

		System.out.println("Str1 : " + str1);
		System.out.println("Str2 : " + str2);

		if(check){
			
			System.out.println("The Strings are anagrams");
		}else
			System.out.println("Strings are not anagrams");
	}
}
