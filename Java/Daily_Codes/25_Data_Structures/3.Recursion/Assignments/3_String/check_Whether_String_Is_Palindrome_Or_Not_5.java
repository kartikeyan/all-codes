
class Sol{
/*
	static boolean checkPalindrome(String str){
		
		int start = 0, end = str.length() - 1;

		while(start < end){
			
			if(str.charAt(start) != str.charAt(end)){
				
				return false;
			}

			start++;
			end--;
		}
	
		return true;
	}
	
	public static void main(String[] args){
		
		String str = "radar";

		boolean ret = checkPalindrome(str);

		if(ret){
			
			System.out.println(str + " is a palindrome");
		}else
			System.out.println(str + " is not a palindrome");
	}
*/
	static boolean checkPalindrome(String str, int start, int end){

                if(start >= end){
			
			return true;
		}

		if(str.charAt(start) != str.charAt(end)){
			
			return false;
		}

		return checkPalindrome(str, start + 1, end - 1);
        }

        public static void main(String[] args){

                String str = "radar";

		System.out.println("Input String " + str);

                boolean ret = checkPalindrome(str, 0, str.length()-1);

                if(ret){

                        System.out.println(str + " is a palindrome");
                }else
                        System.out.println(str + " is not a palindrome");
        }
}
