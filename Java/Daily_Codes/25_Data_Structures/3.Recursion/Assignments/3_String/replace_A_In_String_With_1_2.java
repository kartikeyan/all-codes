import java.util.*;

class Sol{
/*
	static String replaceAWith1(String str){
		
		return str.replaceAll("a", "1");
	}
*/
	static String replaceAWith1(String str, int index){
		
		if(index == str.length()){
			
			return "";
		}

		char ch = str.charAt(index);

		String str1;

		if(ch == 'a'){
			
			str1 = "1";
		}else{
			
			str1 = String.valueOf(ch);
		}
		
		return str1 + replaceAWith1(str, index + 1);
	}	

	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);
	
		System.out.println("Enter String");
		String str = sc.nextLine();

		String ret = replaceAWith1(str, 0);

		System.out.println(ret);
	}
}
