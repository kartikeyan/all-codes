import java.util.*;

class Sol{
/*
        static char[] lowerCaseArr(char arr[]){
                
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Character.toLowerCase(arr[i]);
		}

		return arr;
        }

        public static void main(String[] args){

		char arr[] = {'A','B','C','D','E'};

		System.out.println("UpperCase String is " + new String(arr));

                char ret[] = lowerCaseArr(arr);

                System.out.println("LowerCase String is " + new String(ret));
        }
*/
	
        static void lowerCaseArr(char arr[], int index){

                if(index < arr.length){
			
			arr[index] = Character.toLowerCase(arr[index]);

			lowerCaseArr(arr, index + 1);
		}
        }

        public static void main(String[] args){

		char arr[] = {'A','B','C','D','E'};

		System.out.println("UpperCase String is " + new String(arr));

                lowerCaseArr(arr, 0);

                System.out.println("LowerCase String is " + new String(arr));
	}
}

