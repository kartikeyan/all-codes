
/*
	Input : S = "AB"

	Output : "AB", "BA"
*/

class Sol{
/*
	static void permutations(String rem1, String current){
		
		if(rem1.length() == 0){
			
			System.out.println(current);
		}else{
			
			for(int i = 0; i < rem1.length(); i++){
				
				char ch = rem1.charAt(i);

				String rem = rem1.substring(0, i) + rem1.substring(i + 1);

				permutations(rem, current + ch);
			}
		}
	}
*/	
	static void permutations(String rem, String current){
		
		if(rem.isEmpty()){
			
			System.out.println(current);
		}else{
			
			char ch = rem.charAt(0);

			String rem1 = rem.substring(1);

			permutations(rem1, current + ch);
			
			if(rem.length() > 1){
				
				rem1 = rem.substring(1) + rem.charAt(0);

				permutations(rem1, current);
			}
		}
	}

	public static void main(String[] args){
		
		String str = "ABC";

		System.out.println("Permutations of " + str);

		permutations(str, " ");
	}
}
