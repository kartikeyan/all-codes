
class Sol{
/*
        static boolean compare(String str1, String str2){

		if(str1.length() != str2.length()){
			
			return false;
		}

		for(int i = 0; i < str1.length(); i++){
			
			if(str1.charAt(i) != str2.charAt(i)){
				
				return false;
			}
		}

		return true;
        }
        
        public static void main(String[] args){
                
                String str1 = "Radar";
		String str2 = "radar";

                boolean ret = compare(str1, str2);

                if(ret){
                        
                        System.out.println("Both Strings are Equal");
                }else
                        System.out.println("Both Strings are not Equal");
        }
*/
	
        static boolean compareStrings(String str1, String str2, int index){

                if(str1.length() != str2.length() || index == str1.length()){
			
			return str1.equals(str2);
		}

		if(str1.charAt(index) != str2.charAt(index)){
			
			return false;
		}

		return compareStrings(str1, str2, index+1);
        }

        public static void main(String[] args){

                String str1 = "radar";
		String str2 = "radar";

		int index = 0;

                boolean ret = compareStrings(str1, str2,0);

                if(ret){

                        System.out.println("Both Strings are equal");
                }else
                        System.out.println("The Strings are not equal");
        }

}

