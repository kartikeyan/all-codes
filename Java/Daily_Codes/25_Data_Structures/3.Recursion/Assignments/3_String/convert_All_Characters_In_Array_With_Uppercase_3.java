
import java.util.*;

class Sol{
/*
        static char[] upperCaseArr(char arr[]){
                
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Character.toUpperCase(arr[i]);
		}

		return arr;
        }

        public static void main(String[] args){

		char arr[] = {'a','b','c','d','e'};

		System.out.println("LowerCase String is " + new String(arr));

                char ret[] = upperCaseArr(arr);

                System.out.println("UpperCase String is " + new String(ret));
        }
*/
        static void upperCaseArr(char arr[], int index){

                if(index < arr.length){
			
			arr[index] = Character.toUpperCase(arr[index]);

			upperCaseArr(arr, index + 1);
		}
        }

        public static void main(String[] args){

		char arr[] = {'a','b','c','d','e'};

		System.out.println("LowerCase String is " + new String(arr));

                upperCaseArr(arr, 0);

                System.out.println("UpperCase String is " + new String(arr));
        }
}

