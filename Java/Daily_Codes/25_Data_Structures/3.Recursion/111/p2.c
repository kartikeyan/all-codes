
#include<stdio.h>

void fun(int n){
		
	if(n == 0){
			
		return;
	}

	fun(--n);

	printf("%d\n", n);
}
	
void main(){
		
	printf("Start Main\n");

	fun(2);

	printf("End Main\n");
}

/*
 	Start Main
	0
	1
	End Main
*/
