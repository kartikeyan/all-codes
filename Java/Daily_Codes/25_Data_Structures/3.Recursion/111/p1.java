
class Recursion{

	void fun(int n){
		
		if(n == 0){
			
			return;
		}

		fun(--n);

		System.out.println(n);
	}
	
	public static void main(String[] args){
		
		System.out.println("Start Main");

		Recursion obj = new Recursion();

		obj.fun(2);

		System.out.println("End Main");
	}
}

/*
 	Start Main
	0
	1
	End Main
*/
