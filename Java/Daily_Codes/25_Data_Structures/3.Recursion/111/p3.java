
class Recursion{

	int fun(int n){
		
		if(n == 0){
			
			return 1;
		}

		return 5 + fun(--n);
	}
	
	public static void main(String[] args){
		
		System.out.println("Start Main");

		Recursion obj = new Recursion();

		int ret = obj.fun(2);

		System.out.println(ret);

		System.out.println("End Main");
	}
}

/*
 	Start Main
	11
	End Main
*/
