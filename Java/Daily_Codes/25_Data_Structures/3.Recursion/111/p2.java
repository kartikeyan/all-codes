
class Recursion{

	void fun(int n){
		
		if(n == 0){
			
			return 1;
		}

		n = 5 + fun(--n);

		System.out.println(n);
	}
	
	public static void main(String[] args){
		
		System.out.println("Start Main");

		Recursion obj = new Recursion();

		obj.fun(2);

		System.out.println("End Main");
	}
}

/*
 	p2.java:8: error: incompatible types: unexpected return value
			return 1;
			       ^
	p2.java:11: error: 'void' type not allowed here
			n = 5 + fun(--n);
			           ^
	2 errors
*/
