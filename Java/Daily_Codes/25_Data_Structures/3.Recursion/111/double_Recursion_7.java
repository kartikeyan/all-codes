
class DoubleRec{

	static void fun(int n){
		
		if(n <= 1){
			
			return;
		}

		fun(n-2);
		
		System.out.println(n);

		fun(n-1);
	}
	
	public static void main(String[] args){
		
		fun(5);
	}
}

/*
 	3
	2
	5
	2
	4
	3
	2
*/
