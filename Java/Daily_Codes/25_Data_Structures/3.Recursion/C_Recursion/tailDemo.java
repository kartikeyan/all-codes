
//	tail recursion demo

class recDemo{

	static int tailDemo(int X){
		
		if(X == 1){
			
			return 1;
		}

		return tailDemo(--X);
	}
	
	public static void main(String[] args){
		
		int ans = tailDemo(4);

		System.out.println(ans);
	}
}
