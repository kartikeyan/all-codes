
// 	Non-Tail Demo

class recDemo{
	
	static int demo(int X){

		if(X == 1){
			
			return 1;
		}

		return 3 + demo(--X);
	}

	public static void main(String[] args){
		
		int ans = demo(4);

		System.out.println(ans);
	}
}
