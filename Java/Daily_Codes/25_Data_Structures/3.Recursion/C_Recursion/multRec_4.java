
class recDemo{

	static int fun(int N){
		
		if(N <= 1){
			
			return 1;
		}

		return N + fun(N - 1) + fun(N - 2);
	}

	public static void main(String[] args){

		int ret = fun(4);

		System.out.println(ret);
	}
}


