/*	Program 9 :
 *
 *	Recursion Starts from here
*/

class recDemo{

	static int n = 5;

	static void fun(int n){
		
		System.out.println("Hello");

		if(n != 1){
			
			fun(--n);
		}
			
		System.out.println("Bye");
	}

	public static void main(String[] args){
		
		fun(n);
	}
}

