/*	PRogram 8 :
 *
 *	Printing Number from 1 to 10 with Recursion
*/

class recDemo{

	static void fun(int n){
		
		System.out.println(n);
		
		if(n != 10){
			
			fun(++n);
		}
	}

	public static void main(String[] args){
		
		fun(1);
	}
}
