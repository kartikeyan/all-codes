
class recDemo{

	static int fun(int N){
		
		if(N <= 1){
			
			return 1;
		}

		return fun(N-1) + fun(N-2);
	}

	public static void main(String[] args){

		int sum = fun(3);

		System.out.println(sum);
	}
}

