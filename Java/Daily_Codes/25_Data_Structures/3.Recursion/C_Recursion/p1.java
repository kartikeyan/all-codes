
/*	Program 1:
 *
 *	Q] What is recursion ?
 *	A] Problem Statement la sub-problem madhe divide karne
 *	   ani tya problem che task achieve karne mhnje "Recursion"
 *
 *	eg :- First u should tell Binary Search,
 *	      Second Merge Sort, Quick Sort,
 *	      and Atlast Trees, Graph
*/

class recDemo{
	
	static void fun(){
		
		System.out.println("In fun");
		fun();
	}

	public static void main(String[] s){
		
		System.out.println("IN MAin");
		fun();
	}
}

//	error : Exception in thread "main" java.lang.StackOverflowError

