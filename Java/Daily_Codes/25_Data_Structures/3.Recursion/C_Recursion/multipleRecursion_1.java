
class recDemo{

	static void fun(int N){
		
		if(N <= 1){
			
			return 1;
		}

		int x = fun(N-1);
		int y = fun(N-2);

		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String[] args){

		fun(4);
	}
}

/*	multipleRecursion_1.java:8: error: incompatible types: unexpected return value
			return 1;
			       ^
	multipleRecursion_1.java:11: error: incompatible types: void cannot be converted to int
		int x = fun(N-1);
		           ^
	multipleRecursion_1.java:12: error: incompatible types: void cannot be converted to int
		int y = fun(N-2);
		           ^
	3 errors
*/
