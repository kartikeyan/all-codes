
class recDemo{

	static int fun(int N){
		
		if(N <= 1){
			
			return 1;
		}

		int x = fun(N-1);
		int y = fun(N-2);

		System.out.println(x);

		System.out.println(y);

		return 1;
	}

	public static void main(String[] args){

		fun(3);
	}
}


