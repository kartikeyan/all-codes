/*	PRogram 7 :
 *
 *	Printing Number from 10 to 0 with Recursion
*/

class recDemo{

	static void fun(int n){
		
		System.out.println(n);
		
		if(n != 1){
			
			fun(--n);
		}
	}

	public static void main(String[] args){
		
		fun(10);
	}
}
