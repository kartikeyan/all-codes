
/*	Power(n , p)
 *
 *	n^p = n * n * n ....... * p times
 *
 *	n^p = n * (p-1) times
*/

class recDemo{

	static int pow(int n, int p){
		
		if(p == 0){
			
			return 1;
		}

		int prevPower = pow(n , p-1);

		return n * prevPower;
	}
	
	public static void main(String[] args){
		
		int n = 2, p = 5;

		int ret = pow(n, p);

		System.out.println(ret);
	}
}

