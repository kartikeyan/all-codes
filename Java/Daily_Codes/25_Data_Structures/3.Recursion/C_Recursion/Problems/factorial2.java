/*	Program 11 : factorial - 2
*/

class recDemo{

	static int factorial(int n){

		if(n == 0){
			
			return 1;	// because factorial of 0 is 1
		}

		int prevFact = factorial(n - 1);
		return n * prevFact;
	}

	public static void main(String[] args){
		
		int ans = factorial(5);

		System.out.println(ans);
	}
}

