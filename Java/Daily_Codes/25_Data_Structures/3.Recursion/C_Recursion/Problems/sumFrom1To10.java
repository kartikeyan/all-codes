
class recDemo{
	
	static int sum = 1;

	static int sum1to10(int n){
		
		if(n != 1){
			
			sum = sum + n;

			sum1to10(--n);
		}

		return sum;
	}
	
	public static void main(String[] s){
		
		int ans = sum1to10(10);

		System.out.println(ans);
	}
}
