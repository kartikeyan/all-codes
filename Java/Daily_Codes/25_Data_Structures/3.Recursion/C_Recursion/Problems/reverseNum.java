
class recDemo{

/*	static int reverse(int n){
		
		int rev = 0, rem;

		while(n != 0){
			
			rem = n % 10;
			rev = rev * 10 + rem;
			n = n / 10;
		}

		return rev;
	}
*/
	
	static int reverse(int n, int rev){
		
		if(n == 0){
			
			return rev;
		}

		int rem = n % 10;
		rev = rev * 10 + rem;

		return reverse(n = n / 10, rev);
	}

	public static void main(String[] args){
		
		int n = 123, rev = 0;

		int ret = reverse(n, rev);

		System.out.println(ret);
	}
}
