
class recDemo{

	static int revNum(int num, int rev){
		
		if(num == 0){
			
			return rev;
		}

		int rem = num % 10;
		rev = rev * 10 + rem;

		return revNum(num = num / 10, rev);
	}
	
	public static void main(String[] s){
		
		int num = 564;

		int rev = 0;

		int ret = revNum(num, rev);

		System.out.println(ret);
	}
}
