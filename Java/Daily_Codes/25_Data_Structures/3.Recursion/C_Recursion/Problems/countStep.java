
/*	Program 2 : CountStep LeetCode problem
 *
 *	if N = 14 then,
 *
 *		if N is even divide N by 2,
 *		then if N is odd subtract by 1,
 *		.
 *		.
 *		.
 *		Repeat steps until we get 0
*/

class recDemo{

/*	static int countStep(int n){
		
		if(n == 0){
			
			return 0;
		}

		int step = 0;

		while(n != 0){
			
			if(n % 2 == 0){
				
				n = n / 2;
			}else{
				n = n - 1;
			}

			step++;
		}

		return step;
	}
*/

	static int countStep(int n, int count){
		
		if(n == 0){
			
			return count;
		}

		if(n % 2 == 0){

			return countStep(n = n / 2, ++count);
		}

		if(n % 2 != 0){

			return countStep(n = n - 1, ++count);
		}

		return count;
	}
	
	public static void main(String[] s){
		
		int n = 14;

		int count = 0;

		int ret = countStep(n, count);

		System.out.println(ret);
	}
}
