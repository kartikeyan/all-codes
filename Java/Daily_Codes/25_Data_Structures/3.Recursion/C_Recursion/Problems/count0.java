
//	Prog : Count Number in 0's in number

class recDemo{

/*	static int countNum(int n){
		
		if(n == 0){
			
			return 0;
		}

		int count = 0;

		while(n != 0){
			
			if(n % 10 == 0){
				
				count++;
			}

			n = n / 10;
		}

		return count;
	}
*/	
	
/*	static int countNum(int n, int count){
		
		if(n == 0){
			
			return count;
		}

		if(n % 10 == 0){
			
			count++;
		}

		return countNum(n / 10, count);
	}
*/
	static int countNum(int n, int count){
		
		if(n == 0 && count == 0){
			
			return count;
		}

		if(n % 10 == 0){
			
			return 1 + countNum(n / 10, count);
		}

		return countNum(n / 10, count);
	}

		
	public static void main(String[] s){
		
		int n = 3012;

		int count = 0;

		int ret = countNum(n, count);

		System.out.println(ret);
	}
}
