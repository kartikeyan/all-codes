
class recDemo{

	static boolean isPalindrome(char arr[], int start, int end){
		
		if(start >= end){
			return true;
		}

		if(arr[start] == arr[end]){
			
			return isPalindrome(arr, start+1, end-1);

		}else{

			return false;
		}
	}
	
	public static void main(String[] s){

		char arr[] = {'a','p','p','a'};

		boolean ret = isPalindrome(arr, 0, 3);

		if(ret == true){
			
			System.out.println("Array is Palindrome");
		}else{
			System.out.println("Array is not a Palindrome");
		}
	}
}
