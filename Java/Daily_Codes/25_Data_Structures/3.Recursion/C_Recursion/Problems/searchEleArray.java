
class recDemo{
	
	static boolean searchArr(char arr[], char search, int size){
		
		if(size == 0){
			
			return false;
		}

		if(arr[size] == search){
			
			return true;
		}

		return searchArr(arr, search, size-1);
	}

	public static void main(String[] s){
		
		char arr[] = {'k','a','r','t','i','k'};

		char search = 't';

		int size = arr.length-1;

		boolean ret = searchArr(arr, search, size);

		if(ret){
			
			System.out.println("Search Element Found");
		}else{
			System.out.println("Search Element not found");
		}
	}
}
