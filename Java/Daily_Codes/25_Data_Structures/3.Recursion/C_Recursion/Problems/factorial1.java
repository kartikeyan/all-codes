/*	Program 11 : factorial - 1
*/

class recDemo{

	static int fact = 1;

	static int factorial(int n){
	
		fact = fact * n;

		if(n != 1){
			
			factorial(--n);
		}

		return fact;
	}

	public static void main(String[] args){
		
		int ans = factorial(5);

		System.out.println(ans);
	}
}

