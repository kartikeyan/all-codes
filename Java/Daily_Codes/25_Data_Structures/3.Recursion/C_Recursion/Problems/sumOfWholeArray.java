
class recDemo{

	static int sumArray(int arr[], int size){

		if(size == 1){					//  if(size == 0){
								//	return 0;
								//  }
			
			return arr[size-1];
		}

		return sumArray(arr, size-1) + arr[size-1];
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5};

		int size = arr.length;

		int ret = sumArray(arr, size);

		System.out.println(ret);
	}
}
