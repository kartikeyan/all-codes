
class recDemo{
	
	static int sumN(int n){
		
		if(n <= 1){
			
			return 1;
		}

		return sumN(n-1) + n;
	}

	public static void main(String[] s){
		
		int ret = sumN(5);

		System.out.println(ret);
	}
}
