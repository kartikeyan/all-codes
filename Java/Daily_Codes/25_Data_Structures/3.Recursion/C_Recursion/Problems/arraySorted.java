
class recDemo{

	static boolean sorted(int arr[],int size, int index){

		if(index == size-1){
			
			return true;
		}

		return arr[index] < arr[index+1] && sorted(arr, size, index+1);
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		int index = 0;

		int size = arr.length;

		boolean ret = sorted(arr,size, index);

		if(ret){
			
			System.out.println("Array is Sorted");
		}else{
			System.out.println("Array is not Sorted");
		}
	}
}
