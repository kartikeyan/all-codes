
/*	Program :
 *
 *	Given an array {1,5,4,3}
 *
 *		if no. of count of even number is greater than 2 than return true else false;
*/

class recDemo{
/*
	static boolean arrCount(int arr[]){
		
		int count = 0;

		int len = arr.length;

		for(int i = 0; i < len; i++){
			
			if(arr[i] % 2 == 0){
				
				count++;
			}

			if(count >= 2){
				
				return true;
			}
		}

		return false;
	}
*/

	static boolean arrCount(int arr[], int size, int count){

		if(count >= 2){
			
			return true;
		}

		if(size == 0){
			
			return false;
		}

		if(arr[size-1] % 2 == 0){
			
			count++;
		}

		return arrCount(arr, size-1, count);
	}

	public static void main(String[] args){
		
		int arr[] = {1,5,4,3};

		int size = arr.length;

		int count = 0;

		boolean ret = arrCount(arr, size, count);

		if(ret){
			
			System.out.println("Array is even");
		}else{
			System.out.println("Array is odd");
		}
	}
}
