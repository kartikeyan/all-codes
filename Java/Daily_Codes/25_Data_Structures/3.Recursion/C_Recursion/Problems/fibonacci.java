
class recDemo{

	
	static int fib(int n){
		
		int a = 0;
		int b = 1, c = 0;

		while(n != 0){
			
			c = a + b;
			a = b;
			b = c;

			n--;
		}

		return c;
	}


/*	static int fib(int n){
		
		if(n == 0 || n == 1){
			
			return n;
		}

		return fib(n-1) + fib(n-2);
	}
*/	
	public static void main(String[] s){
		
		int ret = fib(5);

		System.out.println(ret);
	}
}
