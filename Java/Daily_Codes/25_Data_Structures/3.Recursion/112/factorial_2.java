
class Sol{

	static int factorial(int n){
		
		if(n == 0){
			
			return 1;
		}

		return factorial(n - 1) * n;
	}
	
	public static void main(String[] args){
		
		int n = 5;

		int ret = factorial(n);

		System.out.println("Factorial Num is " + ret);
	}
}
