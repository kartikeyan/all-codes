
class Sol{

	static int fibonacci(int n){
		
		if(n == 0){
			
			return 0;
		}

		if(n == 1){
			
			return 0;
		}

		if(n == 2){
			
			return 1;
		}

		return fibonacci(n - 1) + fibonacci(n - 2);
	}
	
	public static void main(String[] args){
		
		int n = 8;

		int ret = fibonacci(n);

		System.out.println("Fibo Num is " + ret);
	}
}
