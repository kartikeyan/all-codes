
class Sol{

	static String reverse(String str){
		
		if(str == null || str.length() <= 1){
			
			return str;
		}

		return reverse(str.substring(1)) + str.charAt(0);
	}
	
	public static void main(String[] args){
		
		String str = "Charkupalli";

		String ret = reverse(str);

		System.out.println("Reversed String is " + ret);
	}
}

//	Reversed String is illapukrahC
