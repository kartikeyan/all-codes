
import java.util.*;

class Demo{

	static int sumN(int num){
		
		int s = 0;

		for(int i = 1; i <= num; i++){	// No of iteration is N.
			
			s = s + i;
		}

		return s;
	}

	static int sumOptimal(int num){
		
		int s = num * (num + 1) / 2;	// No of iteration is = 1.

		return s;
	}
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		int x = sc.nextInt();

		int ret = sumOptimal(x);

		System.out.println("Sum is " + ret);
	}
}
