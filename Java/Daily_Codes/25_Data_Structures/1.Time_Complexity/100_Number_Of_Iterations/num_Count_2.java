import java.util.*;

class Demo{

	static int countN(int a, int b){
		
		int c = 0;

		for(int i = a; i <= b; i++){	// No of iterations is N.
			
			c++;
		}

		return c;
	}

	static int countNOptimal(int a, int b){
		
		return b - a + 1;	// No of iterations is 1
	}
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		int a = sc.nextInt();
		int b = sc.nextInt();

		int ret = countNOptimal(a, b);

		System.out.println("Count of number is " + ret);
	}
}
