

        for(int i = 1; i <= N; i = i + 2){

                System.out.println(i);
        }


/*      1] N    2] (N + 1) / 2      3] N / 2        4] N ^ 2
 *
 *      if N = 5,
 *
 *      	i = 1, 1 <= 5; print(1); 3;
 *      	i = 3, 3 <= 5, print(3); 5;
 *      	i = 5, 5 <= 5, print(5); 7;
 *      	i = 7, 7 <= 5 == false;		==> count is 3
 *
 *      if N = 4,
 *      	i = 2, 1 <= 4, print(1); 3;
 *              i = 3, 3 <= 4, print(3); 5;
 *              i = 4, 5 <= 4 == false;         ==> count is 2
 *
 *      from this it derives as (N + 1) / 2;
*/      
