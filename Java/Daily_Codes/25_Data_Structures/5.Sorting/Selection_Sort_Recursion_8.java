
class Sol{
/*
	static int[] selectionSort(int arr[]){

		int itr = 0;

		for(int i = 0; i < arr.length; i++){
			
			int minIndex = i;

			boolean flag = false;

			for(int j = i + 1; j < arr.length; j++){
				
				itr++;

				if(arr[minIndex] > arr[j]){
					
					minIndex = j;
					flag = true;
				}
			}

			if(flag == false){
				
				break;
			}

			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		}
		
		System.out.println("No of Iterations are " + itr);
		return arr;
	}
*/
	
	static int itr = 0;

	static void selectionSort(int arr[], int start){
		
		if(start == arr.length-1){
			
			return;
		}

		itr++;
		
		int minIndex = start;

		for(int j = start+1; j < arr.length; j++){
			
			if(arr[minIndex] > arr[j]){
				
				minIndex = j;
			}
		}

		int temp = arr[start];
		arr[start] = arr[minIndex];
		arr[minIndex] = temp;

		selectionSort(arr, start+1);
	}

	public static void main(String[] args){
		
		int arr[] = {9,1,8,2,7,3,6,4,5};

		selectionSort(arr, 0);

		for(int i = 0; i < arr.length; i++){

			System.out.print(arr[i] + " ");
		}
		
		System.out.println();
	
		System.out.println("No of Iterations are " + itr);
	}
}

/*
 	1 2 3 4 5 6 7 8 9 
	No of Iterations are 8
*/
