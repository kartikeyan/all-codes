
class Sol{

	static int[] insertionSort(int arr[]){

		int itr = 0;
		
		for(int i = 1; i < arr.length; i++){
			
			int j = i - 1;

			int ele = arr[i];

//			while(arr[j] > ele && j >= 0)	// This should not be the case because it gives arrayIndexOutOfBoundError
			
			while(j >= 0 && arr[j] > ele){

				itr++;
				
				arr[j+1] = arr[j];

				j--;
			}

			arr[j+1] = ele;
		}

		System.out.println("No. of Iterations are " + itr);

		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {9,1,8,2,7,3,6,4,5};

		int ret[] = insertionSort(arr);

		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}

/*
 	No. of Iterations are 20
	1 2 3 4 5 6 7 8 9 
*/
