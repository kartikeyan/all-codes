
class Sol{

	static int[] bubbleSort(int arr[]){
		
		int n = arr.length, itr = 0;

		for(int i = 0; i < n; i++){
			
			boolean flag = false;

			for(int j = 0; j < n - i - 1; j++){
				
				itr++;

				if(arr[j] > arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;

					flag = true;
				}
			}

			if(flag == false){
				
				break;
			}
		}
		
		System.out.println("No of Iterations is " + itr);
		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6,7};

		int ret[] = bubbleSort(arr);

		for(int i = 0; i < arr.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}

/*
 	No of Iterations is 6
	1 2 3 4 5 6 7 
*/
