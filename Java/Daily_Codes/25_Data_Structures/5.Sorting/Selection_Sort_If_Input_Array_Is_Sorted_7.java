
class Sol{

	static int[] selectionSort(int arr[]){

		int itr = 0;

		for(int i = 0; i < arr.length; i++){
			
			int minIndex = i;

			boolean flag = false;

			for(int j = i + 1; j < arr.length; j++){
				
				itr++;

				if(arr[minIndex] > arr[j]){
					
					minIndex = j;
					flag = true;
				}
			}

			if(flag == false){
				
				break;
			}

			int temp = arr[i];
			arr[i] = arr[minIndex];
			arr[minIndex] = temp;
		}
		
		System.out.println("No of Iterations are " + itr);
		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6,7,8,9};

		int ret[] = selectionSort(arr);

		for(int i = 0; i < ret.length; i++){

			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}

/*
 	No of Iterations are 8
	1 2 3 4 5 6 7 8 9 
*/
