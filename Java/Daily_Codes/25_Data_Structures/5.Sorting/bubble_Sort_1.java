
class Sol{
/*
	static int[] bubbleSort(int arr[]){
		
		int itr = 0, n = arr.length;

		for(int i = 0; i < n; i++){
			
			for(int j = 0; j < n - 1; j++){
				
				itr++;

				if(arr[j] > arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		
		System.out.println("No. of iterations are : " + itr);
		return arr;
	}
*/
	/*
	 	No. of iterations are : 72
		1 2 3 4 5 6 7 8 9 
	*/
/*
	static int[] bubbleSort(int arr[]){
		
		int itr = 0, n = arr.length;

		for(int i = 0; i < n; i++){
			
			for(int j = 0; j < n - i - 1; j++){
				
				itr++;

				if(arr[j] > arr[j+1]){
				
					int temp = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = temp;
				}
			}
		}
		
		System.out.println("No. of iterations are : " + itr);
		return arr;
	}
*/
	/*
	 	No. of iterations are : 36
		1 2 3 4 5 6 7 8 9 
	*/	

	public static void main(String[] args){
		
		int arr[] = {9,1,4,2,3,8,5,6,7};

		int ret[] = bubbleSort(arr);

		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
