
import java.util.*;

class Sol{

	static ArrayList sort(int arr[],int k){
		
		ArrayList al = new ArrayList();

		int n = arr.length;

		for(int i = 0; i < n; i++){

			if(arr[i] == k){
				
				al.add(i);
			}
		}

		Collections.sort(al);

		return al;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,5,2,3};

		int k = 2;

		ArrayList al = sort(arr,k);

		for(Object i : al)
			System.out.println(i);

	}
}
