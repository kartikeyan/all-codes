
class Sol{

	static void bubbleSort(int arr[]){
		
		int n = arr.length;

		for(int i = 0; i < n; i++){

			for(int j = 0; j < n - i - 1; j++){
				
				if(arr[j] > arr[j+1]){
					
					int t = arr[j];
					arr[j] = arr[j+1];
					arr[j+1] = t;
				}
			}
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {9,1,8,2,7,3,6,4,5};

		bubbleSort(arr);

		for(int i : arr){
			
			System.out.print(i + " ");
		}

		System.out.println();
	}
}
