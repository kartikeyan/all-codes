
class Sol{

	static boolean sort(int arr[]){
		
		int n = arr.length;

		int c = 0;

		for(int i = 1; i < n; i++){
			
			if(arr[i - 1] > arr[i]){
				
				c++;
			}
		}

		if(arr[n - 1] > arr[0]){
			
			c++;
		}

		return c <= 1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {3,4,5,1,2};

		boolean ret = sort(arr);

		if(ret == true){
			
			System.out.println("Sorted");
		}else
			System.out.println("Not Sorted");
	}
}
