
class Sol{

	static int sort(int arr[]){
		
		int n = arr.length;

		for(int i = 0; i < n-1; i++){

			if(arr[i] > arr[i+1]){
				
				return 0;
			}	
		}

		return 1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {/*9,1,8,2,7,3,6,*/4,5};

		int ret = sort(arr);

		if(ret == 1){
			
			System.out.println("Sorted");
		}else
			System.out.println("Not Sorted");
	}
}
