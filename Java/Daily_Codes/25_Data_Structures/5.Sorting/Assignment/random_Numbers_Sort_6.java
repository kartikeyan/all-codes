

class Sol{

	static void merge(int arr[], int start, int mid, int end){

		int ele1 = mid - start + 1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i = 0; i < ele1; i++){

			arr1[i] = arr[start + i];
		}

		for(int j = 0; j < ele2; j++){

			arr2[j] = arr[mid + 1 + j];
		}

		int itr1 = 0, itr2 = 0, itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){

			if(arr1[itr1] < arr2[itr2]){

				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
				arr[itr3] = arr2[itr2];
				itr2++;
			}

			itr3++;
		}

		while(itr1 < ele1){

			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){

			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	static void mergeSort(int arr[], int start, int end){

		if(start < end){

			int mid = start + (end - start) / 2;

			mergeSort(arr, start, mid);
			mergeSort(arr, mid+1, end);

			merge(arr, start, mid, end);
		}
	}

	public static void main(String[] args){

		int arr[] = {9,1,8,2,7,3,6,4,5};

		mergeSort(arr, 0, arr.length - 1);		// T.C = O(N log N)
								// S.C = O(N)

		for(int i = 0; i < arr.length; i++){

			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}
}

/*
 	1 2 3 4 5 6 7 8 9
*/
