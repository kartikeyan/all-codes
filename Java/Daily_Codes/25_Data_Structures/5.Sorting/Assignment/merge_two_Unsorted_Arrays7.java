
import java.util.*;

class Sol{
	
	
	static void sort(int arr1[], int arr2[]){
		
		int n = arr1.length, m = arr2.length;

		int arr3[] = new int[n + m];

		int i = 0, j = 0, k = 0;

		while(i < n && j < m){
			
			if(arr1[i] < arr2[j]){
				
				arr3[k] = arr1[i];
				i++;
			}else{
				
				arr3[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < n){
			
			arr3[k] = arr1[i];
			i++;
			k++;
		}
		
		while(j < m){
			
			arr3[k] = arr2[j];
			j++;
			k++;
		}

		for(int x = 0; x < arr3.length; x++){
			
			System.out.print(arr3[x] + " ");
		}

		System.out.println();
	}
	
	public static void main(String[] args){
		
		int arr1[] = {1,3,5,7};

		int arr2[] = {0,2,4,6,8};

		sort(arr1, arr2);
	}
}
