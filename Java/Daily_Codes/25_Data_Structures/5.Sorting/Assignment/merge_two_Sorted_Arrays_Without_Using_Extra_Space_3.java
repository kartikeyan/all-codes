
import java.util.*;

class Sol{
	
	/*
	static void sort(int arr1[], int arr2[]){
		
		int n = arr1.length, m = arr2.length;

		int arr3[] = new int[n + m];

		int i = 0, j = 0, k = 0;

		while(i < n && j < m){
			
			if(arr1[i] < arr2[j]){
				
				arr3[k] = arr1[i];
				i++;
			}else{
				
				arr3[k] = arr2[j];
				j++;
			}
			k++;
		}

		while(i < n){
			
			arr3[k] = arr1[i];
			i++;
			k++;
		}
		
		while(j < m){
			
			arr3[k] = arr2[j];
			j++;
			k++;
		}

		for(int x = 0; x < arr3.length; x++){
			
			if(x < n){		// T.C = O(n + m) + O(n + m)
				
				arr1[x] = arr3[x];
			}else{
				arr2[x - n] = arr3[x];
			}
		}
	}
	*/

	static void sort(int arr1[], int arr2[]){
		
		int left = arr1.length - 1;	// left stands at max element of arr1
		int right = 0;	// right stands at min element of arr2

		while(left >= 0 && right < arr2.length){
			
			if(arr1[left] > arr2[right]){
				
				int t = arr1[left];
				arr1[left] = arr2[right];
				arr2[right] = t;

				left--;		// left is going to 0 index of arr1

				right++;	// right is going to arr2.length - 1 index
			}else{
				
				break;		// already sorted
			}
		}

		Arrays.sort(arr1);
		Arrays.sort(arr2);	// T.C = O(n + m) + O(log n + m)
	}
	
	public static void main(String[] args){
		
		int arr1[] = {1,3,5,7};

		int arr2[] = {0,2,4,6,8};

		sort(arr1, arr2);

		for(int i : arr1){
			
			System.out.print(i + " ");
		}
		
		System.out.println();
	
		System.out.println("Array 2");
		for(int i : arr2){
			
			System.out.print(i + " ");
		}

		System.out.println();
	}
}
