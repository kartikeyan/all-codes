
//	Internal implementation of toString() method in java

/*
 	public static String toString(int[] v)
3023:   {
3024:     if (v == null)
3025:       return "null";
3026:     StringBuilder b = new StringBuilder("[");
3027:     for (int i = 0; i < v.length; ++i)
3028:       {
3029:     if (i > 0)
3030:       b.append(", ");
3031:     b.append(v[i]);
3032:       }
3033:     b.append("]");
3034:     return b.toString();
3035:   }

*/

import java.util.*;

class Sol{

	static void swap(int arr[], int x, int y){
		
		int t = arr[x];
		arr[x] = arr[y];
		arr[y] = t;
	}

	static int partition(int arr[], int start, int end){
		
		int pivot = arr[end];

		int itr = start - 1;

		for(int j = start; j < end; j++){
			
			if(arr[j] < pivot){
				
				itr++;

				swap(arr, itr, j);
			}
		}

		swap(arr, itr+1, end);		// swap pivot element

		return itr + 1;
	}

	static void quickSort(int arr[], int start, int end){
		
		if(start < end){
			
			int pivot = partition(arr, start, end);

			quickSort(arr, start, pivot - 1);

			quickSort(arr, pivot+1, end);
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {9,1,8,2,7,3,6,4,5};

		System.out.println(Arrays.toString(arr));

		quickSort(arr, 0, arr.length - 1);

		System.out.println(Arrays.toString(arr));
	}
}

/*
 	1] Best Case = O(N log N)
	2] Average Case = O(N log N)
	3] Worst Case = O(N^2)
*/
