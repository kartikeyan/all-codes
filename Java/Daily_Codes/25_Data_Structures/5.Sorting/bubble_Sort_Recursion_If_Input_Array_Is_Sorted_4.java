
class Sol{

	static int itr = 0;

	static void bubbleSort(int arr[], int n){

		if(n == 1){
			
			return;
		}
		
		itr++;

		boolean flag = false;

		for(int j = 0; j < n-1; j++){
			
			if(arr[j] > arr[j+1]){
			
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;

				flag = true;
			}
		}

		if(flag == false){
			
			return;
		}

		bubbleSort(arr, n-1);
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6,7,8};

		int n = arr.length;

		bubbleSort(arr, n);

		for(int i = 0; i < arr.length; i++){
			
			System.out.print(arr[i] + " ");
		}
		
		System.out.println();

		System.out.println("No of Iterations are " + itr);
	}
}

/*	1 2 3 4 5 6 7 8 9
	No of Iterations are 1
*/
