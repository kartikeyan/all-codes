
/*
 	katikeyan@kartikeyan:~/javacodes/Codes/25_Data_Structures/7.Stack$ javap java.util.Stack
	Compiled from "Stack.java"
	public class java.util.Stack<E> extends java.util.Vector<E> {
	  public java.util.Stack();
	  public E push(E);
	  public synchronized E pop();
	  public synchronized E peek();
	  public boolean empty();
	  public synchronized int search(java.lang.Object);
	}
*/

import java.util.*;

class StackDemo{

	public static void main(String[] args){
		
		Stack s = new Stack();

		s.push(10);

		s.push(20);

		s.push(30);

		s.push(40);

		System.out.println(s);

		System.out.println(s.pop());

		System.out.println(s);

		System.out.println(s.isEmpty());

		System.out.println(s.peek());

		System.out.println(s.size());
	
		s.pop();

		s.pop();

		s.pop();

		System.out.println(s.pop());
	}
}

/*
 	[10, 20, 30, 40]
	40
	[10, 20, 30]
	false
	30
	3
	Exception in thread "main" java.util.EmptyStackException
		at java.base/java.util.Stack.peek(Stack.java:102)
		at java.base/java.util.Stack.pop(Stack.java:84)
		at StackDemo.main(preDefined_Stack_1.java:49)
*/
