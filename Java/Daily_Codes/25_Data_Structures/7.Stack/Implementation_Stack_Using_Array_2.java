
//	Stack Implementation Using Array

import java.util.*;

class Stack{

	int maxSize;

	int stackArr[];

	int top = -1;

	Stack(int size){
		
		this.stackArr = new int[size];

		this.maxSize = size;
	}

	boolean isFull(){
		
		if(top == maxSize - 1)
			return true;
		else
			return false;
	}

	void push(int data){

		if(isFull()){
			
			System.out.println("Stack Overflow");
			return;
		}else{
		
			top++;

			stackArr[top] = data;
		}
	}

	boolean isEmpty(){
		
		if(top == -1)
			
			return true;
		else
			return false;
	}

	int pop(){
		
		if(isEmpty()){
			
			System.out.println("Stack Underflow");

			return -1;
		}else{

			int data = stackArr[top];

			top--;

			return data;
		}
	}

	int peek(){
		
		if(isEmpty()){
			
			System.out.println("Stack Underflow");

			return -1;
		}else{

			int data = stackArr[top];

			return data;
		}
	}

	int size(){
		
		return top;
	}

	void printStack(){
		
		if(isEmpty()){
			
			System.out.println("Stack is Empty");
			return;
		}else{
			
			System.out.print("[");
			for(int i = top; i >= 0; i--){
				
				System.out.print(stackArr[i] + " "); 
			}
			System.out.println("]");
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Array Size");
		int size = sc.nextInt();

		Stack s = new Stack(size);
		
		char ch;

		do{
		
			System.out.println("1.Push");
			System.out.println("2.Pop");
			System.out.println("3.Peek");
			System.out.println("4.isEmpty");
			System.out.println("5.IsFull");
			System.out.println("6.Size");
			System.out.println("7.PrintStack");

			System.out.println("Enter Your Choice");
			int choice = sc.nextInt();

			switch(choice){
				
				case 1:
					{
						System.out.println("Enter Element for Stack");

						int data = sc.nextInt();
						s.push(data);
					}
					break;

				case 2:
					{
						int ret = s.pop();

						if(ret != -1)
							System.out.println("Popped data is " + ret);
					}
					break;
				case 3:
					{
						int ret = s.peek();

						if(ret != -1)
							System.out.println("Peeked data is " + ret);
					}
					break;
				case 4:
					{
						boolean ret = s.isEmpty();
						if(ret == true)
							System.out.println("Stack is FullEmpty");
						else
							System.out.println("Stack is not Empty");

					}
					break;
				case 5:
					{	
						boolean ret = s.isFull();
						if(ret == true)
							System.out.println("Stack is Full");
						else
							System.out.println("Stack is not Full");
					}
					break;
				case 6:
					{
						int sz = s.size();
						System.out.println("Stack Size is " + (sz+1));
					}
					break;
				case 7:
					{
						s.printStack();
					}
					break;
				default:
					System.out.println("Wrong Choice");
			}

			System.out.println("Do u want to conitnue");

			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}
