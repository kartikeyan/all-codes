
/*
 	int arr[] = {4,2,1,5,3}

	o.p : 2 1 -1 3 -1
*/

class Sol{

	static void smallerEle(int arr[]){
		
		for(int i = 0; i < arr.length - 1; i++){
			
			if(arr[i] > arr[i+1]){
				
				System.out.print(arr[i+1] + " ");
			}else{
				System.out.print("-1" + " ");
			}
		}

		System.out.print("-1");
		System.out.println();
	}
	
	public static void main(String[] args){
		
		int arr[] = {4,2,1,5,3};

		smallerEle(arr);
	}
}
