
/*
 	Given a mathematical equation using numbers/variables and +, -, *, /. Print the equation in reverse.

	Examples:  

	Input : 20 - 3 + 5 * 2
	Output : 2 * 5 + 3 - 20

	Input : 25 + 3 - 2 * 11
	Output : 11 * 2 - 3 + 25
	
	Input : a + b * c - d / e
	Output : e / d - c * b + a
*/

import java.util.*;

class Sol{

	static String reverse(String str){
		
		Stack<Character> s = new Stack<>();

		int i = str.length() - 1;

		StringBuilder sb = new StringBuilder("");

		while(i >= 0){
			
			char ch = str.charAt(i);

			if(ch == '+' || ch == '-' || ch == '*' || ch == '/'){
				
				while(!s.empty()){
					
					char c = s.peek();

					sb.append(c);

					s.pop();
				}

				sb.append(ch);
			}else{
				s.push(ch);
			}

			i--;
		}

		while(!s.empty()){
			
			char c = s.peek();

			sb.append(c);

			s.pop();
		}

		return sb.toString();
	}
	
	public static void main(String[] args){
		
		String s = "20-3+5*2";

		String ret = reverse(s);

		System.out.println(ret);
	}
}
