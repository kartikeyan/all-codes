
import java.util.*;

class ReverseString{
	
	String revString(String str){
		
		Stack<Character> s = new Stack<>();

		for(int i = 0; i < str.length(); i++){

			s.push(str.charAt(i));
		}

		char stackArr[] = new char[str.length()];

		int i = 0;

		while(!s.empty()){
			
			stackArr[i] = s.pop();

			i++;
		}

		return new String(stackArr);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter String");
		String str = sc.next();

		ReverseString obj = new ReverseString();
		String rev = obj.revString(str);

		System.out.println(rev);
	}
}
