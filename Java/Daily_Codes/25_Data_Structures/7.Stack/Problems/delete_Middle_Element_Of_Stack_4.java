
/*
 	Middle ele = floor((size of stack + 1) / 2) (1-based indexing) 
	from bottom of stack

	Stack = {10,20,30,40,50}
	o.p = {10,20,40,50}

	Stack = {10,20,30,40}
	o.p = {10,30,40}
*/

import java.util.*;

class Sol{

	static void deleteMiddle(Stack s){
		
		int n = s.size();

		System.out.println(n);

		Stack newStack = new Stack();

		int count = 0;

		// put first n / 2 elements of s in newStack

		while(count < n / 2){
			
			newStack.push(s.pop());		// top ele of stack gets poped
			
			count++;
		}

		s.pop();	// delete middle ele

		while(!newStack.empty()){	// put all n/2 ele of newStack in s
			
			s.push(newStack.pop());

		}

		System.out.println(s);
	}
	
	public static void main(String[] args){
		
		Stack s = new Stack();

		s.push(10);

		s.push(20);

		s.push(30);

		s.push(40);

//		s.push(50);

		deleteMiddle(s);
	}
}
