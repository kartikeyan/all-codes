
//	Stack Implementation Using Linked List

import java.util.*;

class Node{
	
	int data;

	Node next = null;

	Node(int data){
		
		this.data = data;
	}
}

class Stack{

	Node head = null;
	
	int maxSize;

	int top = 0;

	Stack(int size){
		
		this.maxSize = size;
	}

	void push(int data){
		
		if(top == maxSize){

			System.out.println("Stack Overflow");
			return;
		}else{

			Node newNode = new Node(data);
			
			if(head == null){
				
				head = newNode;
			}else{
				
				newNode.next = head;
				head = newNode;
			}

			top++;
		}
	}

	int pop(){

		int ret;
		
		if(head == null){
			
			System.out.println("Stack Underflow");

			return -1;
		}else{	

			if(head.next == null){
				
				ret = head.data;

				head = null;
			}else{
				
				ret = head.data;

				head = head.next;
			}
		}

		top--;

		return ret;
	}

	int peek(){
		
		if(head == null && top == 0){
			
			System.out.println("Stack Underflow");
			return -1;
		}else
			return head.data;
		
	}

	void printStack(){
	
		if(head == null){
			
			System.out.println("Stack is Empty");
			return;
		}else{
			
			Node temp = head;

			System.out.print("[");
			while(temp != null){
				
				System.out.print(temp.data + " ");

				temp = temp.next;
			}

			System.out.println("]");
		}
	}
}

class Client{
	
	public static void main(String[] args){

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Size of stack");
		int size = sc.nextInt();

		Stack s = new Stack(size);
		
		char ch;

		do{
		
			System.out.println("1.Push");
			System.out.println("2.Pop");
			System.out.println("3.Peek");
			System.out.println("4.PrintStack");

			System.out.println("Enter Your Choice");
			int choice = sc.nextInt();

			switch(choice){
				
				case 1:
					{
						System.out.println("Enter Element for Stack");

						int data = sc.nextInt();
						s.push(data);
					}
					break;

				case 2:
					{
						int ret = s.pop();

						if(ret != -1)
							System.out.println("Popped data is " + ret);
					}
					break;
				case 3:
					{
						int ret = s.peek();

						if(ret != -1)
							System.out.println("Peeked data is " + ret);
					}
					break;
				case 4:
					{
						s.printStack();
					}
					break;
				default:
					System.out.println("Wrong Choice");
			}

			System.out.println("Do u want to conitnue");

			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}
