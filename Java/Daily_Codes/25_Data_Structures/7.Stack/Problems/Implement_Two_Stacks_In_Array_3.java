
import java.util.*;

class TwoStacks{

	int maxSize;

	int stackArr[];

	int top1, top2;

	TwoStacks(int size){
		
		this.maxSize = size;

		this.stackArr = new int[size];

		this.top1 = -1;

		this.top2 = size;
	}
	
	void push1(int data){
		
		if(top2 - top1 > 1){		// space is there in stack
			
			top1++;
			stackArr[top1] = data;
		}else{
			
			System.out.println("Stack OverFlow");
		}
	}

	void push2(int data){
		
		if(top2 - top1 > 1){		// space is there in stack
			
			top2--;
			stackArr[top2] = data;
		}else{
			
			System.out.println("Stack OverFlow");
		}
	}

	int pop1(){
		
		if(top1 == -1){
		
			System.out.println("Stack1 Underflow");
			return -1;
		}else{
			
			int val = stackArr[top1];

			top1--;

			return val;
		}
	}

	int pop2(){
		
		if(top2 == maxSize){
		
			System.out.println("Stack2 Underflow");
			return -1;
		}else{
			
			int val = stackArr[top2];

			top2++;

			return val;
		}
	}

	int peek1(){
	
		if(top1 == -1){
		
			System.out.println("Stack1 Underflow");
			return -1;
		}else{
			
			int val = stackArr[top1];

			top1--;

			return val;
		}
	}

	int peek2(){
	
		if(top2 == maxSize){
		
			System.out.println("Stack2 Underflow");
			return -1;
		}else{
			
			int val = stackArr[top2];

			return val;
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Arrary Size");
		int size = sc.nextInt();

		TwoStacks obj = new TwoStacks(size);

		char ch;

		do{
		
			System.out.println("1.Push1");
			System.out.println("2.Push2");
			System.out.println("3.Pop1");
			System.out.println("4.Pop2");
			System.out.println("5.Peek1");
			System.out.println("6.Peek2");

			System.out.println("Enter Your Choice");
			int choice = sc.nextInt();

			switch(choice){

				case 1:
					{
						System.out.println("Enter ELement for Stack1");
						int data = sc.nextInt();

						obj.push1(data);
					}
					break;
				case 2:
					{
						System.out.println("Enter ELement for Stack2");
						int data = sc.nextInt();

						obj.push2(data);
					}
					break;
				case 3:
					{
						int ret = obj.pop1();

						if(ret != -1){
							
							System.out.println("Popped data " + ret);
						}
					}
					break;
				case 4:
					{
						int ret = obj.pop2();

						if(ret != -1){
							
							System.out.println("Popped data " + ret);
						}
					}
					break;
				case 5:
					{
						int ret = obj.peek1();

						if(ret != -1){
							
							System.out.println("Peeked1 is " + ret);
						}
					}
					break;
				case 6:
					{
						int ret = obj.peek2();

						if(ret != -1){
							
							System.out.println("Peeked2 is " + ret);
						}
					}
					break;
				default:
					System.out.println("Enter Correct Choice");
			}

			System.out.println("Do u want to contiune");
			ch = sc.next().charAt(0);

		}while(ch == 'Y' || ch == 'y');
	}
}
