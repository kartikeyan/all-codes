
import java.util.*;

class Sol{

	static void design(int arr[], Stack<Integer> s){
		
		for(int i = 0; i < arr.length; i++){
			
			s.push(arr[i]);
		}

		while(!s.empty()){
			
			System.out.println(s.pop());
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5};

		Stack<Integer> s = new Stack<>();

		design(arr,s);
	}
}
