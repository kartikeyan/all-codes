
import java.util.*;

class Sol{

	static void sort(Stack<Integer> s){
		
		int arr[] = new int[s.size()];

		for(int i = 0; i < arr.length; i++){
			
			arr[i] = s.pop();
		}

		for(int i = 0; i < arr.length - 1; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] > arr[j]){
					
					int t = arr[i];
					arr[i] = arr[j];
					arr[j] = t;
				}
			}
		}

		for(int i = 0; i < arr.length; i++){
			
			s.push(arr[i]);
		}
	}

	public static void main(String[] args){
		
		Stack<Integer> s = new Stack<>();

		s.push(3);

		s.push(-5);

		s.push(-1);

		s.push(-2);

		s.push(-3);

		System.out.println(s);

		sort(s);

		System.out.println(s);
	}	
}
