
/*
	Given a stack S and an integer N, the task is to insert N at the bottom of the stack.

Examples:

    Input: N = 7
    S = 1 <- (Top)
          2
         3
         4
         5
    Output: 1 2 3 4 5 7

    Input: N = 17
    S = 1 <- (Top)
         12
         34
         47
         15
    Output: 1 12 34 47 15 17

*/

import java.util.*;

class Sol{

	static void insert(Stack<Integer> s, int n){
		
		Stack<Integer> temp = new Stack<>();

		while(!s.empty()){
			
			temp.push(s.pop());
		}

		s.push(n);

		while(!temp.empty()){
			
			s.push(temp.pop());
		}
	}
	
	public static void main(String[] agrs){
		
		Stack<Integer> s = new Stack<>();

		s.push(6);

		s.push(5);

		s.push(4);

		s.push(3);

		s.push(2);

		s.push(1);

		System.out.println(s);

		int n = 7;

		insert(s,n);

		System.out.println(s);
	}
}
