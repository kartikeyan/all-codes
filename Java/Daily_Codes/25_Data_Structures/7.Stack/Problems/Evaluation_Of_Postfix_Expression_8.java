
/*
 	Given a postfix expression, the task is to evaluate the postfix expression.

    	Postfix expression: The expression of the form “a b operator” (ab+) i.e., when a pair of operands is followed by an operator.

	Examples:

	    Input: str = “2 3 1 * + 9 -“
	    Output: -4
	    Explanation: If the expression is converted into an infix expression, it will be 2 + (3 * 1) – 9 = 5 – 9 = -4.

	    Input: str = “100 200 + 2 / 5 * 7 +”
	    Output: 757
*/

/*
 	Follow the steps mentioned below to evaluate postfix expression using stack:

	1] Create a stack to store operands (or values).

    	2] Scan the given expression from left to right and do the following for every scanned element.

	3] If the element is a number, push it into the stack.

        4] If the element is an operator, pop operands for the operator from the stack. Evaluate the operator and push the result back to the stack.

    	5] When the expression is ended, the number in the stack is the final answer.

*/

import java.util.*;

/*
 
class Sol{

	static int evaluate(String str){
		
		Stack<Integer> s = new Stack<>();

		for(int i = 0; i < str.length(); i++){
			
			char c = str.charAt(i);

			if(Character.isDigit(c)){
				
				s.push(c - '0');
			}else{
				
				int n1 = s.pop();
				int n2 = s.pop();

				switch(c){
					
					case '+':
						s.push(n2 + n1);
						break;
					case '-':
						s.push(n2 - n1);
						break;
					case '*':
						s.push(n2 * n1);
						break;
					case '/':
						s.push(n2 / n1);
						break;
				}
			}
		}

		return s.pop();
	}
	
	public static void main(String[] args){
		
		String s = "231*+9-";

		int ret = evaluate(s);

		System.out.println(ret);
	}
}

*/

/*
	150. Evaluate Reverse Polish Notation

	You are given an array of strings tokens that represents an arithmetic expression in a Reverse Polish Notation.

	Evaluate the expression. Return an integer that represents the value of the expression.

	Note that:

	    The valid operators are '+', '-', '*', and '/'.
	    Each operand may be an integer or another expression.
	    The division between two integers always truncates toward zero.
	    There will not be any division by zero.
	    The input represents a valid arithmetic expression in a reverse polish notation.
	    The answer and all the intermediate calculations can be represented in a 32-bit integer.

	Example 1:

	Input: tokens = ["2","1","+","3","*"]
	Output: 9
	Explanation: ((2 + 1) * 3) = 9

	Example 2:

	Input: tokens = ["4","13","5","/","+"]
	Output: 6
	Explanation: (4 + (13 / 5)) = 6

	Example 3:

	Input: tokens = ["10","6","9","3","+","-11","*","/","*","17","+","5","+"]
	Output: 22
	Explanation: ((10 * (6 / ((9 + 3) * -11))) + 17) + 5
		= ((10 * (6 / (12 * -11))) + 17) + 5
		= ((10 * (6 / -132)) + 17) + 5
		= ((10 * 0) + 17) + 5
		= (0 + 17) + 5
		= 17 + 5
		= 22

	Constraints:

	    1 <= tokens.length <= 104
	    tokens[i] is either an operator: "+", "-", "*", or "/", or an integer in the range [-200, 200].

*/

class Sol{

	static int evaluate(String[] str){
		
		Stack<Integer> s = new Stack<>();

		for(String x : str){
		
			if(x.equals("+")){
				
				int n1 = s.pop();
				int n2 = s.pop();

				int ret = n2 + n1;

				s.push(ret);
			}	
			else if(x.equals("-")){
				
				int n1 = s.pop();
				int n2 = s.pop();

				int ret = n2 - n1;

				s.push(ret);

			}else if(x.equals("*")){
				
				int n1 = s.pop();
				int n2 = s.pop();

				int ret = n2 * n1;

				s.push(ret);

			}else if(x.equals("/")){
				
				int n1 = s.pop();
				int n2 = s.pop();

				int ret = n2 / n1;

				s.push(ret);
			}else{
				// If the token is not an operator, it's an operand, so push it onto the stack
				s.push(Integer.parseInt(x));
				
				//parseInt is used to convert string into integer
			}
		}

		return s.pop();
	}	

	public static void main(String[] args){
		
		String[] s = {"2","1","+","3","*"};

		int ret = evaluate(s);

		System.out.println(ret);
	}
}
