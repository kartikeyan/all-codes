
import java.util.*;

/*
class SpecialStack extends Stack<Integer>{

	Stack<Integer> newStack = new Stack<>();

	void push(int x){
		
		if(newStack.size() == 0){
			
			super.push(x);

			newStack.push(x);
		}else{
			super.push(x);

			int y = newStack.pop();

			newStack.push(y);

			if(x < y)
				newStack.push(x);
			else
				newStack.push(y);
		}
	}

	public Integer pop(){
		
		int x = super.pop();

		newStack.pop();

		return x;
	}

	int getMin(){
		
		int x = newStack.pop();

		newStack.push(x);

		return x;
	}
	
	public static void main(String[] args){
		
		SpecialStack s = new SpecialStack();

		s.push(10);

		s.push(20);

		s.push(30);

		System.out.println(s.getMin());

		s.push(1);

		System.out.println(s.getMin());
	}
} 

*/

class SpecialStack {

	int min = Integer.MAX_VALUE;

	Stack<Integer> stack = new Stack<>();

	// only push the old minimum value when the current
        // minimum value changes after pushing the new value x
	void push(int x){
		
		if(x <= min){
			
			stack.push(min);

			min = x;
		}

		stack.push(x);
	}

	// if pop operation could result in the changing of the current minimum value,
        // pop twice and change the current minimum value to the last minimum value.
	int pop(){
		
		if(stack.pop() == min){
			
			min = stack.pop();

		}

		return min;
		
	}

	int top(){

		return stack.peek();
	}

	int getMin(){
		
		return min;
	}
	
	public static void main(String[] args){
		
		SpecialStack s = new SpecialStack();

		s.push(10);

		s.push(20);

		s.push(30);

		System.out.println(s.getMin());

		s.push(1);

		System.out.println(s.getMin());
	}
} 

