
/*
	20. Valid Parentheses
	
	Given a string s containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.

	An input string is valid if:

	    Open brackets must be closed by the same type of brackets.
	    Open brackets must be closed in the correct order.
	    Every close bracket has a corresponding open bracket of the same type.

 
	Example 1:

	Input: s = "()"
	Output: true

	Example 2:

	Input: s = "()[]{}"
	Output: true

	Example 3:

	Input: s = "(]"
	Output: false

 
	Constraints:

	    1 <= s.length <= 104
	    s consists of parentheses only '()[]{}'.
*/


import java.util.*;

class Validate{
	
	boolean valParenthesis(String str){
		
		Stack<Character> s = new Stack<>();

		for(int i = 0; i < str.length(); i++){
			
			char ch = str.charAt(i);

			if(ch == '{' || ch == '[' || ch == '('){
				
				s.push(ch);
			}else{
				
				if(!s.empty()){
					
					char x = s.peek();

					if((x == '[' && ch == ']') || (x == '{' && ch == '}') || (x == '(' && ch == ')')){
						
						s.pop();
					}else{
						
						System.out.println("Not matched");		
						return false;	// if not matched, return false
					}
					
				}else{		// if closed brackets are encountered first, then stack will be empty, and in else condtion's, if condition becomes false and comes in else condition and returns false
					System.out.println("Only closed brackets are encountered");
					return false;
				}
			}
		}

		if(s.empty())
			return true;
		else
			System.out.println("Only Opening brackets are encoutered");
			return false;
	}
}

class Client{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		Validate obj = new Validate();

		System.out.println("Enter Expression");

		String str = sc.next();

		boolean ret = obj.valParenthesis(str);	

		if(ret == true)
			System.out.println("Balanced Parenthesis");
		else
			System.out.println("Not Valid");
	}
}
