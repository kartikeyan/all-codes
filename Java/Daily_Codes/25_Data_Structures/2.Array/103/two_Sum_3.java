
/*
 	Two Sum
*/

import java.util.*;

class Sol{
/*
	static int twoSum(int arr[], int k){

                int c = 0, itr = 0;

                for(int i = 0; i < arr.length; i++){

                        for(int j = 0; j < arr.length; j++){
				
				itr++;

                        	if(i != j && (arr[i] + arr[j] == k)){

                                	c++;
	                        }
			}
                }
		
		System.out.println("Itr is " + itr);
                return c;
        }
*/

/*
 	Itr is 100	|| T.C = O(N^2)
	Count is 6	||
*/
/*	
	static int twoSum(int arr[], int k){

                int c = 0, itr = 0;

                for(int i = 0; i < arr.length; i++){

                        for(int j = i+1; j < arr.length; j++){

                                itr++;

                                if(i != j && (arr[i] + arr[j] == k)){

                                        c++;
                                }
                        }
                }

                System.out.println("Itr is " + itr);
                return c * 2;
        }
*/
/*
        Itr is 45       || T.C = O(N^2)
        Count is 6      ||
*/
	static int twoSum(int arr[], int k){
                
                int c = 0;

		Arrays.sort(arr);

		int s = 0, e = arr.length - 1;

		while(s < e){		// T.C = O(N * log N) + O(N / 2)
					//     = O(N)

			int sum = arr[s] + arr[e];

			if(sum == k){

				c++;

				s++;

				e--;

			}else if(sum < k){
				
				s++;
			}else if(sum > k){
				
				e--;
			}
		}

                return c * 2;
        }	

	public static void main(String[] args){

		int arr[] = {3,5,2,1,-3,7,8,15,6,13};

		int k = 10;

		int ret = twoSum(arr, k);

		System.out.println("Count is " + ret);
	}
}
