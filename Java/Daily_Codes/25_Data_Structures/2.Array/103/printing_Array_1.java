
class Sol{
	
	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5};	// protocol

		for(int i = 0; i < arr.length; i++){
			
			System.out.print(arr[i] + " ");
		}

		System.out.println();	// 1 2 3 4 5 
	}
}
