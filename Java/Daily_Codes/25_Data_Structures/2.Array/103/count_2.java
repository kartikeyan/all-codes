
/*
 	WAP to count no. of elements having atleast 1 element greater than itself

	i.p : arr[] = {2,5,1,4,8,0,8,1,3,8};
	o.p : 7
*/

import java.util.*;

class Sol{
/*
	static int countEle(int arr[]){
		
		int c = 0;

		int N = arr.length;

		for(int i = 0; i < N; i++){			// T.C = O(N^2)
			
			for(int j = 0; j < N; j++){
				
				if(arr[i] < arr[j]){
					
					c++;
					break;
				}
			}
		}

		return c;
	}
*/
/*	
	static int countEle(int arr[]){

                int c = 1;

                int N = arr.length;

		Arrays.sort(arr);

                for(int i = 0; i < N-1; i++){                     // T.C = O(N * log N) + O(N)
								  // 	 = O(N)

                        if(arr[i] < arr[i+1]){
				
				c++;
			}
                }

                return c;
        }
*/
	static int countEle(int arr[]){

                int c = 0;

                int N = arr.length;

                int maxEle = Integer.MIN_VALUE;
		for(int i = 0; i < N; i++){
			
			if(arr[i] > maxEle){
				
				maxEle = arr[i];
			}
		}

                for(int i = 0; i < N; i++){                     // T.C =  O(N)
                                                               
                        if(arr[i] == maxEle){
                                
                                c++;
                        }
                }

                return N - c;
        }

	public static void main(String[] args){
		
		int arr[] = new int[]{2,5,1,4,8,0,8,1,3,8};	// protocol
		
		int ret = countEle(arr);

		System.out.println("Count Ele is " + ret);
	}
}
