
class Sol{

	static int secondMax(int arr[]){
		
		int secMax = Integer.MIN_VALUE, max = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] > max){
				
				secMax = max;
				max = arr[i];
			}
		}

		System.out.println("MAx is " + max);
		System.out.println("Sec MAx is " + secMax);
		return secMax;
	}
	
	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5};

		int ret = secondMax(arr);

		System.out.println("Second Max Ele is " + ret);
	}
}
