
class Sol{

	static int[] reverseArray(int arr[]){
		
		int s = 0, e = arr.length - 1;

		while(s < e){
			
			int t = arr[s];
			arr[s] = arr[e];
			arr[e] = t;

			s++;
			e--;
		}

		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5};

		int ret[] = reverseArray(arr);

		for(int i = 0; i < ret.length; i++){
		
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
