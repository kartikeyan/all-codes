
/*
 	Maximum SubArray Sum

	1] USing T.C = O(N^2) 

	2] USing Prefix Sum - T.C = O(N^2) and S.C = O(1)

	3] Using Carry Forward - T.C = O(N^2) and S.C = O(1)

	4] Kadanes Algo - T.C = O(N) and S.C = O(1)
*/

/*
 	arr[] = {-2,1,-3,4,-1,2,1,-5,4}

	o.p = 4 i.e subarray [4,-1,2,1]
*/

class Sol{

	static int maximumSubArraySum(int arr[]){
	
		// 1 - Approach 
	/*		
		int maxSum = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i; j < arr.length; j++){
				
				int sum = 0;

				for(int k = i; k <= j; k++){
					
					sum += arr[k];
				}

				if(sum > maxSum){
					
					maxSum = sum;
				}
			}
		}

		return maxSum;
	*/
		
		// 2 - Approach
	/*
		int pre[] = new int[arr.length];
		pre[0] = arr[0];

		for(int i = 1; i < arr.length; i++){
			
			pre[i] = pre[i - 1] + arr[i];
		}

		int maxSum = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			int sum = 0;

			for(int j = i; j < arr.length; j++){
				
				if(i == 0){
					
					sum = pre[j];
				}else
					sum = pre[j] - pre[i - 1];	// prefixSum(start, end) = prefixSum(end) - prefixSum(start - 1)

									// prefixSum(i, j) = prefixSum(j) - prefixSum(i - 1)

				if(sum > maxSum){
					
					maxSum = sum;
				}
			}
		}

		return maxSum;
	*/
		// 3 - Approach

	/*	
		int maxSum = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			int sum = 0;

			for(int j = i; j < arr.length; j++){
				
				sum += arr[j];

				if(sum > maxSum){
					
					maxSum = sum;
				}
			}
		}

		return maxSum;
	*/
		// 4 - Approach
	/*
		int maxSum = Integer.MIN_VALUE;

		int sum = 0;

		for(int i = 0; i < arr.length; i++){
			
			sum += arr[i];

			if(sum < 0){
				
				sum = 0;
			}

			if(sum > maxSum){
				
				maxSum = sum;
			}
		}

		return maxSum;

	*/
		// 5 - Approach - LeetCode approach
	
		int maxSum = Integer.MIN_VALUE;

		int sum = 0;

		for(int i = 0; i < arr.length; i++){
			
			if(sum > 0){
				
				sum = sum + arr[i];
			}else
				sum = arr[i];

			if(sum > maxSum){
				
				maxSum = sum;
			}
		}

		return maxSum;
	}
	
	public static void main(String[] args){
		
		int arr[] = {-2,1,-3,4,-1,2,1,-5,4};

		int ret = maximumSubArraySum(arr);

		System.out.println("Maximum Subarray sum is " + ret);
	}
}
