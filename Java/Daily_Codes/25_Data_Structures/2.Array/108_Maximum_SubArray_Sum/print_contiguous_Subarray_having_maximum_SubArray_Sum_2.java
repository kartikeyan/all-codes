
class Sol{
	
	public static void main(String[] args){

		int arr[] = {-2,1,-3,4,-1,2,1,-5,4};

		int maxSum = Integer.MIN_VALUE;

		int sum = 0;

		int startIndex = 0, endIndex = 0;

		// 1 - My Approach
/*
		for(int i = 0; i < arr.length; i++){
			
			sum = sum + arr[i];

			if(sum < 0){

				sum = 0;

				startIndex = i + 1;
			}

			if(sum > maxSum){

				maxSum = sum;

				endIndex = i;
			}
		}
*/
		// 2 - Precise

/*
		for(int i = 0; i < arr.length; i++){
			
			sum = sum + arr[i];

			if(sum < 0){

				sum = 0;

				startIndex = i + 1;
			}

			if(sum > maxSum){

				maxSum = sum;
				
				startIndex = startIndex;

				endIndex = i;
			}
		}
*/
		// 3 - More Precise
			
		int x = -1;

		for(int i = 0; i < arr.length; i++){

			if(sum == 0){
				
				x = i;
			}
			
			sum = sum + arr[i];

			if(sum < 0){

				sum = 0;
			}

			if(sum > maxSum){

				maxSum = sum;

				startIndex = x;

				endIndex = i;
			}
		}

		System.out.println(maxSum);

		for(int i = startIndex; i <= endIndex; i++){
			
			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}
}
