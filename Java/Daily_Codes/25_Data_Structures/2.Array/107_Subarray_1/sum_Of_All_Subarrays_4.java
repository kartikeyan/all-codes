
/*
 	1] Print Sum of All Subarrays using T.C = O(N^3)

	2] Using Prefix Sum Technique T.C = O(N^2) and S.C = O(N)

	3] Using Carry Forward Techique using T.C = O(N^2) and S.C = O(1)
*/

class Sol{
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3};
	
		// 1 - Brute Force - O(N^3)
	/*	
		for(int i = 0; i < arr.length; i++){
			
			for(int j = i; j < arr.length; j++){
				
				int sum = 0;

				for(int k = i; k <= j; k++){
					
					sum += arr[k];
				}

				System.out.println(sum);
			}
		}
	*/

		// 2 - USing Prefix Sum Technique - T.C = O(N^2) and S.C = O(N)
	
	/*	
		int pre[] = new int[arr.length];

		pre[0] = arr[0];

		for(int i = 1; i < arr.length; i++){
			
			pre[i] = pre[i - 1] + arr[i];
		}

		int sum = 0;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i; j < arr.length; j++){
				
				if(i == 0){
					
					sum = pre[j];
				}else
					sum = pre[j] - pre[i - 1];	// prefixSum(start, end) = prefixSum(end) - prefixSum(start - 1);
									// prefixSum(i, j) = prefixSum(j) - prefixSum(i - 1);

				System.out.println(sum);
			}
		}

	*/
		// 3 - Using Carry Forward - T.C = O(N^2) && S.C = O(1)

		for(int i = 0; i < arr.length; i++){

			int sum = 0;
			
			for(int j = i; j < arr.length; j++){
				
				sum += arr[j];

				System.out.println(sum);
			}
		}
	}
}
