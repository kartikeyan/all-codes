
class Sol{

	static int totalSum(int arr[]){
		
		int total = 0;

		for(int i = 0; i < arr.length; i++){
			
			int sum = 0;

			for(int j = i; j < arr.length; j++){
				
				sum += arr[j];

				total += sum;
			}
		}

		return total;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3};

		int ret = totalSum(arr);

		System.out.println("Total Sum of all subarrays " + ret);
	}
}
