class Sol{

	static int[] prefixSum(int arr[]){
		
		for(int i = 1; i < arr.length; i++){
			
			arr[i] = arr[i-1] + arr[i];
		}

		return arr;	// T.C = O(N)
				// S.C = O(1)
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6,7};

		int ret[] = prefixSum(arr);

		System.out.println("Prefix Array is");
		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
