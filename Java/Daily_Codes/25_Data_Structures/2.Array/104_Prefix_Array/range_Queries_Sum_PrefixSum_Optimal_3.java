
/*
 	Given array of size N and Q number of Queries contains two parameters (s,e).
	For all queries print sum of all elements from index s to index e.

	arr[] = {-3, 6, 2, 4, 5, 2, 8, -9, 3, 1};

	N = 10

	Q = 3

	Queries		s	e	sum

	  1		1	3	12
	  2		2	7	12
	  3		1	1	6
*/

import java.util.*;
import java.io.*;

class UnderOverFlowException extends Exception{
	
	UnderOverFlowException(String msg){
		
		super(msg);
	}
}

class UnderFlowException extends Exception{

        UnderFlowException(String msg){

                super(msg);
        }
}

class Sol{

	static void sumQueries(int arr[], int q) throws Exception{
			
		Scanner sc = new Scanner(System.in);	

		int pSum[] = new int[arr.length];
		pSum[0] = arr[0];

		for(int i = 1; i < arr.length; i++){
			
			pSum[i] = pSum[i - 1] + arr[i];
		}

		System.out.println("Prefix Array is");
		for(int i = 0; i < pSum.length; i++){
			
			System.out.print(pSum[i] + " ");
		}
		System.out.println();

		int sum = 0;

		for(int i = 1; i <= q; i++){
			
			System.out.println("Enter Start");
                	int s = sc.nextInt();

        	        System.out.println("Enter End");
	                int e = sc.nextInt();

			if(s < 0 || e >= arr.length){

        	                throw new UnderOverFlowException("Enter Valid Number");
	       		}
			
			if(s == 0){
				
				sum = pSum[e];
			}else{

				sum = pSum[e] - pSum[s - 1];
			}

			System.out.println(sum);	// T.C = O(N) + O(Q)
							//     = O(max(N,Q))
							     
							// S.C = O(N)
		}
	}
	
	public static void main(String[] args) throws Exception{

		Scanner sc = new Scanner(System.in);

		int arr[] = {-3, 6, 2, 4, 5, 2, 8, -9, 3, 1};

		System.out.println("Enter Queries");
                int q = sc.nextInt();
		
		if(q <= 0){
			
			throw new UnderFlowException("Enter Num greater than 0");
		}
		
		sumQueries(arr, q);
	}
}
