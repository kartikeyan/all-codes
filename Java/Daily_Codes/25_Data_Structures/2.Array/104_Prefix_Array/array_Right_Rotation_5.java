import java.io.*;

class OverFlowException extends Exception{
	
	OverFlowException(String msg){
		
		super(msg);
	}
}

class Sol{
/*
	static int[] arrayRotation(int arr[], int k) throws Exception{

		int n = arr.length;
		
		k = k % n;

		if(k > n){
			
			throw new OverFlowException("Enter Valid Number");
		}

		int temp[] = new int[k];

		for(int i = n - k; i < n; i++){
			
			temp[i - n + k] = arr[i];
		}

		for(int i = n - k - 1; i >= 0; i--){
			
			arr[i + k] = arr[i];
		}

		for(int i = 0; i < k; i++){
			
			arr[i] = temp[i];
		}

		return arr;		// T.C = O(N)
					// S.C = O(k)
	}
*/	
	static void reverse(int arr[], int s, int e){
		
		while(s <= e){
			
			int t = arr[s];
			arr[s] = arr[e];
			arr[e] = t;

			s++;
			e--;
		}
	}

	static int[] arrayRotation(int arr[], int k){
			
		int n = arr.length;

		// reverse first n-k elements
		reverse(arr, 0, n - k - 1);

		// reverse last k elements
		reverse(arr, n - k, n - 1);

		//reverse whole array
		reverse(arr, 0, n - 1);

		return arr;
	}

	public static void main(String[] args) throws Exception{
		
		int arr[] = {1,2,3,4};

		int k = 2;

		int ret[] = arrayRotation(arr, k);

		System.out.println("After Array Rotation is");
		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
