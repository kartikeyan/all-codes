
import java.util.*;
import java.io.*;

class UnderOverFlowException extends Exception{
	
	UnderOverFlowException(String msg){
		
		super(msg);
	}
}

class Sol{

	static int sumRange(int arr[], int s, int e){
		
		int sum = 0;

		for(int i = s; i <= e; i++){
			
			sum += arr[i];
		}

		return sum;
	}
	
	public static void main(String[] args) throws Exception{

		Scanner sc = new Scanner(System.in);

		int arr[] = {2,5,3,11,7,9};
		
		System.out.println("Enter Start");
		int s = sc.nextInt();
		System.out.println("Enter End");
		int e = sc.nextInt();
		
		if(s < 0 || e >= arr.length){
			
			throw new UnderOverFlowException("Not a Valid Number");
		}

		int ret = sumRange(arr, s, e);

		System.out.println("Sum Range is " + ret);
	}
}
