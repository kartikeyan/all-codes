
//	Print Matrix Col-wise

class Sol{
	
	public static void main(String[] agrs){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9},{10,11,12}};
		
		System.out.println(arr.length);

		for(int i = 0; i < arr[0].length; i++){
			
			for(int j = 0; j < arr.length; j++){
				
				System.out.print(arr[j][i] + " ");
			}

			System.out.println();
		}
	}
}

/*
 	1 4 7 10 
	2 5 8 11 
	3 6 9 12 
*/
