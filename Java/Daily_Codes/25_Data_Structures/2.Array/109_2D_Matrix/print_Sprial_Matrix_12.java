
//	Valid only for n * n matrix ====== i.e square matrix

class Sol{
	
	public static void main(String[] args){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

		int n = arr.length;

		int i = 0, j = 0;
	
		while(n > 1){

			for(int x = 0; x < n - 1; x++){
			
				System.out.println(arr[i][j]);
				j++;
			}

			for(int x = 0; x < n - 1; x++){
			
				System.out.println(arr[i][j]);
				i++;
			}

			for(int x = 0; x < n - 1; x++){
			
				System.out.println(arr[i][j]);
				j--;
			}

			for(int x = 0; x < n - 1; x++){		// T.C = O(N)
			
				System.out.println(arr[i][j]);
				i--;
			}	

			n = n - 2;

			i++;

			j++;
		}

		if(n == 1){		// if n is odd then only 1 element is remaining that is why this condition is necessary
			
			System.out.println(arr[i][j]);
		}
	}
}

/*
 	1
	2
	3
	6
	9
	8
	7
	4
	5
*/
