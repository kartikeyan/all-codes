
/*	Print Right Diagonal of Square Matrix N * N
 
	arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

	output : 1 5 9
*/

class Sol{
	
	public static void main(String[] agrs){
		
		int arr[][] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
	
	/*	
		for(int i = 0; i < arr.length; i++){

			for(int j = 0; j < arr.length; j++){
				
				if((i + j) == (arr.length-1)){		// T.C = O(N^2)

					System.out.println(arr[i][j]);
				}
			}
		}
	*/
	
	/*	
		int j = arr.length - 1;

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i][j]);		// T.C = O(N)
			j--;
		}
	*/
	
		
		int i = 0, j = arr.length - 1;

		while(i < arr.length && j >= 0){	// T.C = O(N)
			
			System.out.println(arr[i][j]);
			i++;
			j--;
		}
	}
}

/*
	4
	7
	10
	13
*/
