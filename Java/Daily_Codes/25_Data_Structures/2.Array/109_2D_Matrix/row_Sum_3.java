
//	Print Row-Wise Sum

class Sol{
	
	public static void main(String[] agrs){
		
		int arr[][] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		
		for(int i = 0; i < arr.length; i++){
			
			int sum = 0;

			for(int j = 0; j < arr[0].length; j++){
				
				sum = sum + arr[i][j];
			}

			System.out.println(sum);
		}
	}
}

/*
 	10
	26
	42
	58
*/
