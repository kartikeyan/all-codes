
//	Print Col-Wise Sum

class Sol{
	
	public static void main(String[] agrs){
		
		int arr[][] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
		
		for(int i = 0; i < arr[0].length; i++){
			
			int sum = 0;

			for(int j = 0; j < arr.length; j++){
				
				System.out.print(arr[j][i] + " ");			
				sum = sum + arr[j][i];
			}
			
			System.out.println();
			System.out.println(sum);
		}
	}
}

/*
	1 5 9 13 
	28
	2 6 10 14 
	32
	3 7 11 15 
	36
	4 8 12 16 
	40
*/
