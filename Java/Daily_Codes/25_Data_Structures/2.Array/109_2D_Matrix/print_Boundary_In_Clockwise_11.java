/*
class Sol{
	
	public static void main(String[] args){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

		int n = 3, m = 3;
	
		for(int i = 0; i < n; i++){
			
			for(int j = 0; j < m; j++){
				
				if(i == 0 || j == 0 || i == m-1 || j == n-1){
					
					System.out.println(arr[i][j]);
				}
			}
		}		// T.C = O(N^2)
	}
}
*/
/*
 	1
	2
	3
	4
	6
	7
	8
	9
*/

class Sol{
	
	public static void main(String[] args){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

		int n = arr.length;

		int i = 0, j = 0;

		for(int x = 0; x < n - 1; x++){
			
			System.out.println(arr[i][j]);
			j++;
		}

		for(int x = 0; x < n - 1; x++){
			
			System.out.println(arr[i][j]);
			i++;
		}

		for(int x = 0; x < n - 1; x++){
			
			System.out.println(arr[i][j]);
			j--;
		}

		for(int x = 0; x < n - 1; x++){		// T.C = O(N)
			
			System.out.println(arr[i][j]);
			i--;
		}	
	}
}

/*
 	1
	2
	3
	6
	9
	8
	7
	4
*/
