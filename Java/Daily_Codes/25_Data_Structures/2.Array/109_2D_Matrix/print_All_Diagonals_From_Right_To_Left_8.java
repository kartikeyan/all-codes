
/*
 	Print All Diagonals from right to left

	Note - row = 0

	arr[][] = {{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18},{19,20,21,22,23,24}};

	o.p : 6 11 16 21
	      5 10 15 20
	      4 9 14 19
	      3 8 13
	      2 7
	      1
*/

class Sol{

	public static void main(String[] args){
		
		int arr[][] = {{1,2,3,4,5,6},{7,8,9,10,11,12},{13,14,15,16,17,18},{19,20,21,22,23,24}};

		int N = 4, M = 6;

		for(int j = M - 1; j >= 0; j--){
			
			int i = 0, x = j;

			while(i < N && x >= 0){
				
				System.out.print(arr[i][x] + " ");

				i++;

				x--;
			}

			System.out.println();
		}
	}
}
