
/*	Print Left Diagonal of Square Matrix N * N
 
	arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

	output : 1 5 9
*/

class Sol{
	
	public static void main(String[] agrs){
		
		int arr[][] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
	/*
		for(int i = 0; i < arr.length; i++){

			for(int j = 0; j < arr[0].length; j++){
				
				if(i == j){	// T.C = O(N^2)
					
					System.out.print(arr[i][j]);
				}
			}

			System.out.println();
		}
	*/	
	
	/*	
		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i][i]);		// T.C = O(N)
		}
	*/
		int i = 0, j = 0;

		while(i < arr.length && j < arr.length){
			
			System.out.println(arr[i][j]);
			i++;
			j++;
		}
	}
}

/*
	1
	6
	11
	16
*/
