
/*
 	arr[] = {-7, 1, 5, 2, -4, 3, 0}

	o.p : 3 index
*/

class Sol{

	// 1 - Brute Force
/*
	static int equillibrium(int arr[]){
		
		for(int i = 0; i < arr.length; i++){
			
			int lSum = 0, rSum = 0;

			for(int j = 0; j < i; j++){
				
				lSum += arr[j];
			}			// T.C = O(N^2)
						// S.C = O(1)

			for(int j = i + 1; j < arr.length; j++){
				
				rSum += arr[j];
			}

			if(lSum == rSum){
				
				return i;
			}
		}

		return -1;
	}
*/	
	// 2 - Optimized App - 1
/*
	static int equillibrium(int arr[]){

		int n = arr.length;
		
		int pre[] = new int[n];
		int suff[] = new int[n];

		pre[0] = arr[0];
		suff[n-1] = suff[n-1];

		for(int i = 1; i < n; i++){
			
			pre[i] = pre[i-1] + arr[i];
		}

		for(int i = n-2; i >= 0; i--){

                        suff[i] = suff[i+1] + arr[i];
                }					// T.C = O(N)
							// S.C = O(N)

		for(int i = 0; i < n; i++){

                        if(pre[i] == suff[i]){
				
				return i;
			}
                }

		return -1;
	}
*/	
	// 3 - Optimized way - 2
	static int equillibrium(int arr[]){
		
		int tSum = 0, temp = 0;

		for(int i = 0; i < arr.length; i++){
			
			tSum += arr[i];
		}

		for(int i = 0; i < arr.length; i++){

                        tSum -= arr[i];	// tSum is now right sum for index i

			if(temp == tSum){
				
				return i;
			}

			temp += arr[i];
                }

		return -1;
	}

	public static void main(String[] args){

		int arr[] = {-7, 1, 5, 2, -4, 3, 0};

		int ret = equillibrium(arr);

		System.out.println("Equillibrium Index of an array is " + ret);
	}
}
