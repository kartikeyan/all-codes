
class Sol{
	
	// 1 - brute Force
/*
	static int subArray(int arr[]){
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		
		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] > max){
				
				max = arr[i];
			}

			if(arr[i] < min){
				
				min = arr[i];
			}
		}
		
		System.out.println(min);
		System.out.println(max);

		int minStep = Integer.MAX_VALUE;
		int len = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == min){
				
				for(int j = i + 1; j < arr.length; j++){
					
					if(arr[j] == max){
						
						len = j - i + 1;

						if(len < minStep){
							
							minStep = len;
						}
					}
				}

			}
			else if(arr[i] == max){

                                for(int j = i + 1; j < arr.length; j++){

                                        if(arr[j] == min){

                                                len = j - i + 1;

						System.out.println("Max Len is " + len);

                                                if(len < minStep){

                                                        minStep = len;
                                                }
                                        }
                                }
                        }
		}

		return minStep;
	}
*/

/*	
	static int subArray(int arr[]){

                int min = Integer.MAX_VALUE;
                int max = Integer.MIN_VALUE;

		int minIndex = -1, maxIndex = -1;

                for(int i = 0; i < arr.length; i++){

                        if(arr[i] > max){

                                max = arr[i];
				maxIndex = i;
                        }

                        if(arr[i] < min){

                                min = arr[i];
				minIndex = i;
                        }
                }

		if(minIndex == -1 || maxIndex == -1){
			
			return -1;
		}
		
		System.out.println(maxIndex);
		System.out.println(minIndex);

		int start = Math.min(minIndex, maxIndex);
		int end = Math.max(minIndex, maxIndex);

		for(int i = start + 1; i < end; i++){
			
			if(arr[i] == min || arr[i] == max){
				
				return end - start + 1;
			}
		}

		return end - start + 2;
	}
*/

	public static void main(String[] args){
		
		int arr[] = {1,2,3,1,3,4,6,4,6,3};

		int ret = subArray(arr);

		System.out.println("Ret is " + ret);
	}
}
