/*
 	Given an Array of size N.

	Build an array leftmax of size N.

	LeftMax of i contains the maximum for the index 0 to the index i.

	arr[] = {-3,6,4,5,2,8,-9,3,1};

	N = 10

	leftMax = {-3,6,6,6,6,6,8,8,8,8};
*/

class Sol{
/*
	static int[] leftMax(int arr[]){
		
		int n = arr.length;

		int newArr[] = new int[n];

		for(int i = 0; i < n; i++){
			
			int max = Integer.MIN_VALUE;

			for(int j = 0; j <= i; j++){
				
				if(arr[j] > max){
					
					max = arr[j];
				}
			}

			newArr[i] = max;		// T.C = O(N^2)
							// S.C = O(1)
		}

		return newArr;
	}
*/
	static int[] leftMax(int arr[]){

                int n = arr.length;

                int leftMax[] = new int[n];

		leftMax[0] = arr[0];

                for(int i = 1; i < n; i++){
							// leftMax[i] = Math.max(LeftMax[i-1], arr[i]);
			if(leftMax[i-1] > arr[i]){
			
				leftMax[i] = leftMax[i-1];
			}else{
				
				leftMax[i] = arr[i];	// T.C = O(N)
							// S.C = O(1)
			}	         
                }

                return leftMax;
        }

	
	public static void main(String[] args){
		
		int arr[] = new int[]{-3,6,4,5,2,8,-9,3,1};

		int ret[] = leftMax(arr);

		System.out.println("Left Max array is");
		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
