/*
 	Given an Array of size N.

	Build an array Rightmax of size N.

	arr[] = {-3,6,4,5,2,8,-9,3,1};

	N = 10

	leftMax = {8,8,8,8,8,8,8,3,3,1};
*/

class Sol{

	static int[] RightMax(int arr[]){

                int n = arr.length;

                int rightMax[] = new int[n];

		rightMax[n-1] = arr[n-1];

                for(int i = n-2; i >= 0; i--){
							// RightMax[i] = Math.max(RightMax[i+1], arr[i]);
			if(rightMax[i+1] > arr[i]){
			
				rightMax[i] = rightMax[i+1];
			}else{
				
				rightMax[i] = arr[i];	// T.C = O(N)
							// S.C = O(1)
			}	         
                }

                return rightMax;
        }
	
	public static void main(String[] args){
		
		int arr[] = new int[]{-3,6,4,5,2,8,-9,3,1};

		int ret[] = RightMax(arr);

		System.out.println("Left Max array is");
		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
