
/*
 	Given an character array (lowercase)

	Return the count of pair(i, j) such that

		a] i <= j
		b] arr[i] = 'a'
		   arr[j] = 'g'

	arr[] = {a, b, e, g, a, b};

	o.p = 3;
*/

class Sol{
/*
	static int countPair(char arr[]){
		
		int c = 0, n = arr.length, itr = 0;

		for(int i = 0; i < n; i++){
				
			for(int j = i + 1; j < n; j++){
				
				itr++;

				if(i <= j && (arr[i] == 'a' && arr[j] == 'g')){
					c++;
				}
			}	
		}		// T.C = O(N^2)
				// S.C = O(N)
		
		System.out.println(itr);	// 15
		return c;			// 3
	}
*/	

/*
	static int countPair(char arr[]){

                int c = 0, n = arr.length, itr = 0;

                for(int i = 0; i < n; i++){
			
			itr++;

			if(arr[i] == 'a'){

	                        for(int j = i + 1; j < n; j++){
				

                                	if(i <= j && (arr[i] == 'a' && arr[j] == 'g')){
                                        	c++;
	                                }
        	                }
			}
                }               // T.C = O(N^2)
                                // S.C = O(N)
		
		System.out.println(itr);	// 6
                return c;			// 3
        }
*/	
	static int countPair(char arr[]){

                int n = arr.length, itr = 0;

		int aCount = 0, validCount = 0;

                for(int i = 0; i < n; i++){

                        itr++;

                        if(arr[i] == 'a'){
				
				aCount++;
                        }			// T.C = O(N)
						// S.C = O(1)

			else if(arr[i] == 'g'){
				
				validCount = validCount + aCount;
			}
                }               

                System.out.println(itr);	          // 6
                return validCount;                       // 3
        }	

	public static void main(String[] args){
		
		char arr[] = new char[]{'a','b','e','g','a','g'};

		int ret = countPair(arr);

		System.out.println("Count is " + ret);
	}
}
