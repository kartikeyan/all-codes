import java.util.*;

class Sol{

	static ArrayList unionOf2(int arr1[], int arr2[]){
		
		HashSet hs = new HashSet();

		ArrayList al = new ArrayList();

		for(int i = 0; i < arr1.length; i++){
			
			hs.add(arr1[i]);
		}

		for(int i = 0; i < arr2.length; i++){
			
			hs.add(arr2[i]);
		}

		for(Object itr : hs){
			
			al.add(itr);
		}

		return al;
	}

	public static void main(String[] args){
	
		int arr1[] = {1,2,3,4,5};

		int arr2[] = {1,2,3};

		ArrayList al = unionOf2(arr1,arr2);

		System.out.println("Union of 2 sorted arrays are");
		for(int i = 0; i < al.size(); i++){
			
			System.out.println(al.get(i));
		}
	}
}
