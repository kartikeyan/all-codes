import java.util.*;

class Sol{
/*
	static int firstRepeatingEle(int arr[], int n){
		
		for(int i = 0; i < n; i++){
			
			for(int j = i + 1; j < n; j++){
				
				if(arr[i] == arr[j]){
					
					return i+1;
				}
			}
		}

		return -1;
	}
*/

	static int firstRepeatingEle(int arr[], int n){
		
		HashMap hp = new HashMap();

		for(int i = 0; i < arr.length; i++){

			int num = arr[i];
			
			if(hp.containsKey(num)){
				
				return (int)hp.get(num);
			}

			hp.put(num, i);
		}

		return -1;
	}

	public static void main(String[] args){
	
		int arr[] = {1,5,3,4,3,5,6};

		int n = arr.length;

		int ret = firstRepeatingEle(arr, n);

		System.out.println("First Repeating Ele in Array is " + ret);
	}
}
