
import java.util.*;

class Sol{

	static int[] rearrange(int arr[], int n){
	/*
		int pos = 0, neg = 1;

		int newArray[] = new int[arr.length];

		for(int i = 0; i < n; i++){
			
			if(arr[i] > 0){
				
				newArray[pos] = arr[i];
				pos += 2;
			}else{
				newArray[neg] = arr[i];
				neg += 2;
			}
		}
		
		return newArray;
	*/
		ArrayList ps = new ArrayList();
		ArrayList ne = new ArrayList();

		for(int i : arr){
			
			if(i > 0){
				
				ps.add(i);
			}else
				ne.add(i);
		}

		for(int i = 0; i < n; i++){
			
			if(i % 2 == 0){
				
				arr[i] = (int)ps.get(i);
			}else
				arr[i] = (int)ne.get(i);
		}

		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {3,6,12,1,5,8};

		int n = arr.length;

		int newArray[] = rearrange(arr, n);
		
		System.out.println("After Rearranging");
		for(int i = 0; i < n; i++){
			
			System.out.print(newArray[i] + " ");
		}

		System.out.println();
	}
}

