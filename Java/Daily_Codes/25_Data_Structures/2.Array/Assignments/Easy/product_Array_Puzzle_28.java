import java.util.*;

class Sol{

	static int[] productPuzzle(int arr[]){

		int n = arr.length;

	        int countZeroes = 0;
        	int indexZero = -1;
		int productWithoutZero = 1;
        	
//		If count of zeroes is greater than 1 so the array will be empty
        	for(int i = 0 ; i < n ; i++) {

			if(nums[i] == 0) {

				countZeroes++;
	                	indexZero = i;
		        }else{
			    
				productWithoutZero *= nums[i];
	            	}
	        }
        
        	int[] output = new int[n];

//		If the count of zeroes is 0 then we need to just divide the product of array with every element
	        if(countZeroes == 0) {

			for(int i = 0;i < n;i++) {

                		output[i] = productWithoutZero / nums[i];
	            	}
//		Lastly if the count of zeroes if 1, then we need to find the index of zero and product of array without zero and then just place the product at index of zero and we are done
	        }else if(countZeroes == 1) {

	            output[indexZero] = productWithoutZero;
        	}

	        return output;
	}

	public static void main(String[] args){

		int arr[] = {16,17,4,3,5,2};

		int[] ret = productPuzzle(arr);

		for(int i = 0; i < ret.length; i++){

			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
