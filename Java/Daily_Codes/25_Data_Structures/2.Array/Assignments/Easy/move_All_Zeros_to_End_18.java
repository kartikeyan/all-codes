
import java.util.*;

class Sol{

	static void move(int arr[], int n){
/*	
		ArrayList al = new ArrayList();

		for(int i = 0; i < n; i++){
			
			if(arr[i] != 0){
				
				al.add(arr[i]);
			}
		}

		for(int i = 0; i < al.size(); i++){
			
			arr[i] = (int)al.get(i);
		}

		for(int i = al.size(); i < n; i++){
			
			arr[i] = 0;
		}
*/

		// optimized

		int j = -1;
		
//		First, using a loop, we will place the pointer j. If we don’t find any 0, we will not perform the following steps. 
		for(int i = 0; i < n; i++){
			
			if(arr[i] == 0){
				
				j = i;
				break;
			}
		}

		if(j == -1){
			
			return;
		}

//		After that, we will point i to index j+1 and start moving the pointer using a loop.
		for(int i = j + 1; j < n; j++){
			
		//	If a[i] != 0 i.e. a[i] is a non-zero element: We will swap a[i] and a[j]. Now, the current j is pointing to the non-zero element a[i]. So, we will shift the pointer j by 1 so that it can again point to the first zero.	
			if(arr[i] != 0){
				
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;

				j++;
			}
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {3,5,0,4,0};

		int n = arr.length;

		move(arr, n);
		
		System.out.println("After Moving Are");
		for(int i = 0; i < n; i++){
			
			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}
}

