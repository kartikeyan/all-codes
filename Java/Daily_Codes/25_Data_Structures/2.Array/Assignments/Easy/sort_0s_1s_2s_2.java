
class Sol{

	static void sortArr(int arr[], int n){

		int arr0 = 0, arr1 = 0, arr2 = 0;

		for(int i = 0; i < n; i++){
			
			if(arr[i] == 0){
				
				arr0++;
			}
			if(arr[i] == 1){
				
				arr1++;
			}

			if(arr[i] == 2){
				
				arr2++;
			}
		}

		for(int i = 0; i < arr0; i++){
			
			arr[i] = 0;
		}
		
		for(int i = arr0; i < arr0 + arr1; i++){
			
			arr[i] = 1;
		}
		
		for(int i = arr0 + arr1; i < n; i++){
			
			arr[i] = 2;
		}

	}
	
	public static void main(String[] args){
		
		int arr[] = {0,2,1,2,0};

		int n = arr.length;

		sortArr(arr, n);

		System.out.println("After Sorting");

		for(int i = 0; i < n; i++){
			
			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}
}
