class Sol{
	
//	static int peakElement(int arr[], int n){

	/*
	 	In peakElement we are going to check these following conditions:

		1. there is only one element.

		2. the first element is a peak.

		3. the last element is a peak.

		4.if is the first and last is not a peek then found it by simply using if else.

		5 last if peak is not found return -1.
	*/
		/*
		if(n == 1){
			
			return 0; // there is only one element, which is peak
		}

		if(arr[0] >= arr[1]){
			
			return 0; // the first element is a peak
		}

		if(arr[n-1] >= arr[n-2]){
			
			return n-1; // the last element is a peak
		}

		for(int i = 1; i < n - 1; i++){
			
			if(arr[i] >= arr[i-1] && arr[i] >= arr[i+1]){
				
				return i; // found a peak
			}
		}

		return -1; // if no peak, return -1.
	}
	*/
	static int peakElement(int arr[], int n){
		
		int s = 0, e = n - 1;

		while(s < e){
			
			int mid = (s + e) / 2;

			if(arr[mid] < arr[mid+1]){
				
				s = mid + 1;
			}else
				e = mid;
		}

		return e;
	}
		
	public static void main(String[] args){
		
		int arr[] = {3,4,2};

		int n = arr.length;

		int ret = peakElement(arr, n);

		System.out.println("Peak Element is " + ret);
	}
}
