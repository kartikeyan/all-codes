
class Sol{

	static int missingNum(int arr[], int n){

		int totalSum = n * (n+1) / 2;

		System.out.println("TotalSum is " + totalSum);

		int arrSum = 0;

		for(int i = 0; i < n-1; i++){
			
			arrSum = arrSum + arr[i];
		}


		System.out.println("Array Sum is " + arrSum);

		return totalSum - arrSum;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,4,5,6};

		int n = arr.length;

		int ret = missingNum(arr, n);

		System.out.println("Missing Number is " + ret);
	}
}
