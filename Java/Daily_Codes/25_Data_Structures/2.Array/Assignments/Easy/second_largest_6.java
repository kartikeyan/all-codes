
class Sol{

	static int secondNum(int arr[], int n){

		int max = arr[0];

		for(int i = 1; i < n; i++){
			
			if(arr[i] > max){
				
				max = arr[i];
			}
		}

		System.out.println("Max Element is " + max);
		
		int sec = arr[0];

		for(int i = 1; i < n; i++){
			
			if(arr[i] > sec && arr[i] != max){
				
				sec = arr[i];
			}
		}

		return sec;
	}
	
	public static void main(String[] args){
		
		int arr[] = {12,35,1,10,34,1};

		int n = arr.length;

		int ret = secondNum(arr, n);

		System.out.println("Second Largest Number is " + ret);
	}
}
