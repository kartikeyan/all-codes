
class Sol{

	static boolean sorted(int arr[], int n){

		for(int i = 0; i < n-1; i++){
			
			if(arr[i] > arr[i+1]){
				
				return false;
			}
		}

		return true;
	}
	
	public static void main(String[] args){
		
		int arr[] = {10,11,12};

		int n = arr.length;

		boolean ret = sorted(arr, n);
		
		if(ret == true){
			System.out.println("Array is sorted");
		}else
			System.out.println("Array is not sorted");
	}
}
