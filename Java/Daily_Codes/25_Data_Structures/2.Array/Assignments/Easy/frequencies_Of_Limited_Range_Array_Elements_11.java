import java.util.*;

class Sol{

	static void frequenciesOfArrEle(int arr[], int n){
		
		HashMap<Integer, Integer> mp = new HashMap<>();

		for(int i = 0; i < n; i++){
			
			if(mp.containsKey(arr[i])){
				
				mp.put(arr[i], mp.get(arr[i]) + 1);
			}else{
			
				mp.put(arr[i], 1);
			}
		}

		for(Map.Entry<Integer, Integer> entry : mp.entrySet()){
			
			System.out.println(entry.getKey() + " " + entry.getValue());
		}
	}

	public static void main(String[] args){
	
		int arr[] = {2,3,2,3,5};

		int n = arr.length;

		frequenciesOfArrEle(arr, n);
	}
}
