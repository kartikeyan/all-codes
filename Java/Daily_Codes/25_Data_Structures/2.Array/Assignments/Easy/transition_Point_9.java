
class Sol{

	static int transitionPoint(int arr[], int n){
		
		for(int i = 0; i < n - 1; i++){
			
			if(arr[i] != arr[i+1]){
				
				return i+1;
			}
		}
		return -1;
	}
	
	public static void main(String[] args){
	
		int arr[] = {0,0,0,1,1};

		int n = arr.length;

		int ret = transitionPoint(arr, n);

		System.out.println("Transition Point in Array is " + ret);
	}
}
