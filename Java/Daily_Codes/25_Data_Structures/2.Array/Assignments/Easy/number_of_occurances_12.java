
class Sol{

	static int occurances(int arr[], int n, int num){
		
		int c = 0;

		for(int i = 0; i < n; i++){
			
			if(arr[i] == num){
				
				c++;
			}
		}

		return c;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,1,2,2,2,2,3};

		int n = arr.length;

		int num = 2;

		int ret = occurances(arr, n, num);
		
		System.out.println("Number of occurances of " + num + " are " + ret);
	}
}

//	for optimized use binary search
