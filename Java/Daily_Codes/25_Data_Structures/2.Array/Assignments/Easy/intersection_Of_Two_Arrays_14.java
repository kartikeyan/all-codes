import java.util.*;

class Sol{
/*
	static int commonEleCount(int arr1[], int arr2[]){
		
		int c = 0;

		for(int i = 0; i < arr1.length; i++){
			
			for(int j = 0; j < arr2.length; j++){
				
				if(arr1[i] == arr2[j]){
					
					c++;
				}
			}
		}

		return c;
	}
*/
	static int commonEleCount(int arr1[], int arr2[]){
		
		HashSet s1 = new HashSet();
		HashSet s2 = new HashSet();

		for(int num : arr1){
			
			s1.add(num);
		}

		for(int num : arr2){
			
			s2.add(num);
		}

		s1.retainAll(s2);// Find the intersection by retaining only common elements in set1
		
		Iterator itr = s1.iterator();

		while(itr.hasNext()){
			
			System.out.println(itr.next());
		}

		return s1.size();
	}

	public static void main(String[] args){
	
		int arr1[] = {1,2,3,4,5,6};

		int arr2[] = {3,4,5,6,7};

		int ret = commonEleCount(arr1,arr2);

		System.out.println("Count of common elements in 2 arrays are  "+ ret);
	}
}
