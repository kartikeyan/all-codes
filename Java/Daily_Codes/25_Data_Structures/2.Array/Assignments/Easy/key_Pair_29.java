import java.util.*;

class Sol{

	static boolean keyPair(int arr[], int x){

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] + arr[j] == x){
					
					return true;
				}
			}
		}

		return false;
	}

	public static void main(String[] args){

		int arr[] = {1,4,45,6,10,8};
		
		int x = 16;

		boolean ret = keyPair(arr, x);
		
		if(ret == true){
			System.out.println("Key Pair Exists");
		}else
			System.out.println("Key Pair Not Exists");
	}
}
