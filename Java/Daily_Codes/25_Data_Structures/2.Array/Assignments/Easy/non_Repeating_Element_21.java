import java.util.*;

class Sol{

	static int nonRepeating(int arr[]){
		
		Map<Integer, Integer> m = new HashMap<>();

		for(int i = 0; i < arr.length; i++){
			
			if(m.containsKey(arr[i])){
				
				m.put(arr[i], m.get(arr[i]) + 1);
			}else
				m.put(arr[i], 1);
		}

		for(int i = 0; i < arr.length; i++){
			
			if(m.get(arr[i]) == 1){
				
				return arr[i];
			}
		}

		return -1;
	}

	public static void main(String[] args){
	
		int arr[] = {-1,2,-1,3,2};

		int ret = nonRepeating(arr);

		System.out.println("Non Repeating Ele is " + ret);
	}
}
