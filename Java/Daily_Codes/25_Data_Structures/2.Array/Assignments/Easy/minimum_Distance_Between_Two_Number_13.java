import java.util.*;

class Sol{

	static int minimumDistance(int arr[], int x, int y, int n){
		
		int idx1 = -1, idx2 = -1, min_dist = Integer.MAX_VALUE;
		
		for(int i = 0; i < n; i++){
			
			if(arr[i] == x){
				
				idx1 = i;
			}else if(arr[i] == y){
				
				idx2 = i;
			}

			if(idx1 != -1 && idx2 != -1){
				
				min_dist = Math.min(min_dist, Math.abs(idx1 - idx2));
			}
		}	

		if(idx1 == -1 || idx2 == -1){
			
			return -1;
		}else
			return min_dist;
	}

	public static void main(String[] args){
	
		int arr[] = {1,2,3,2};

		int x = 1, y = 2;

		int n = arr.length;

		int ret = minimumDistance(arr, x, y, n);

		System.out.println("Minimum Distance between 2 elements are " + ret);
	}
}
