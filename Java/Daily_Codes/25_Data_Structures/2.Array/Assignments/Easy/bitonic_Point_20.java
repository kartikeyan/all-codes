import java.util.*;

class Sol{

	static int bitonicPoint(int arr[]){
		
		int low = 1, high = arr.length - 2;

		while(low <= high){
			
			int mid = low + (high - low) / 2;

			if(arr[mid] > arr[mid - 1] && arr[mid] > arr[mid+1]){
				
				return arr[mid];
			}else if(arr[mid] < arr[mid+1]){
				
				low = mid + 1;
			}else
				high = mid - 1;
		}

		return -1;
	}

	public static void main(String[] args){
	
		int arr[] = {1,15,25,45,42,21,17,12,11};

		int ret = bitonicPoint(arr);

		System.out.println("Bitonic Point is " + ret);
	}
}
