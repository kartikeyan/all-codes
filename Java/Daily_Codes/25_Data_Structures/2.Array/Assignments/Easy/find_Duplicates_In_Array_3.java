
import java.util.*;

class Sol{

	static List dupArr(int arr[], int n){
		
//		Step-1 -> Initializes an empty list called list to store the duplicate values.

//		Step-2 -> Also create an empty set called set to keep track of unique values encountered in the input array.
			
		List ll = new ArrayList();

		Set s = new HashSet();

//		Step-3 -> Loop through each element i in the nums array.
		for(int i : arr){
			
//		Step-4 -> If the set already contains the element i, it means i is a duplicate, so it's added to the list. Otherwise, if i is not in the set, it's added to the set to keep track of it.
			if(s.contains(i)){
				
				ll.add(i);
			}else
				s.add(i);
		}
		
//		Step-5 -> Finally, the method returns the list containing the duplicates found in the nums array.
		return ll;
	}
	
	public static void main(String[] args){
		
		int arr[] = {2,3,1,2,3};

		int n = arr.length;

		List ret = dupArr(arr, n);

		for(int i = 0; i < ret.size(); i++){
			
			System.out.print(ret.get(i) + " ");
		}

		System.out.println();
	}
}
