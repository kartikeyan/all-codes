class Sol{
	
	static int maximumIndex(int arr[], int n){
		
		int max = Integer.MIN_VALUE;
		int k = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){

			for(int j = i; j < arr.length; j++){

				if(arr[i] <= arr[j]){
					
					k = j - i;

					if(k > max){
						
						max = k;
					}
				}
			}
		}

		return max;
	}

	public static void main(String[] args){
		
		int arr[] = {34,8,10,3,2,80,30,33,1};

		int n = 9;

		int ret = maximumIndex(arr, n);

		System.out.println("Maximum Index is " + ret);
	}
}
