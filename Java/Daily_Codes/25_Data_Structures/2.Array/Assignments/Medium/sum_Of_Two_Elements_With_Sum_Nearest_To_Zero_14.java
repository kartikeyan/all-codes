import java.util.*;

class Sol{

	static int sumNearestToZero(int arr[]){

		int l = 0, r = arr.length - 1, sum , min_sum = Integer.MAX_VALUE;

		int min_l = l, min_r = r;

		if(arr.length < 2){
			
			return -1;
		}

		Arrays.sort(arr);

		while(l < r){
			
			sum = arr[l] + arr[r];

			if(Math.abs(sum) < Math.abs(min_sum)){
				
				min_sum = sum;
				min_l = l;
				min_r = r;
			}

			if(sum < 0){
				
				l++;
			}else
				r--;
		}

		return arr[min_l] + arr[min_r];
    	}

	public static void main(String[] args){
		
		int arr[] = {-8,-66,-60};

		int ret = sumNearestToZero(arr);
		
		System.out.println("Ans is " + ret);
	}
}
