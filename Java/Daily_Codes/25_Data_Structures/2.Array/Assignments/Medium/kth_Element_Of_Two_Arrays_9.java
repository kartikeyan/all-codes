import java.util.*;

class Sol{
	
	static int kthEle(int arr1[], int arr2[], int k){
		
		ArrayList al = new ArrayList();

		for(int i = 0; i < arr1.length; i++){

			al.add(arr1[i]);
		}

		for(int i = 0; i < arr2.length; i++){

			al.add(arr2[i]);
		}

		Collections.sort(al);

		return (int)al.get(k);
	}

	public static void main(String[] args){
		
		int arr1[] = {2,3,6,7,9};

		int arr2[] = {1,4,8,10};

		int k = 5;

		int ret = kthEle(arr1, arr2, k-1);

		System.out.println("Kth Element is " + ret);
	}
}
