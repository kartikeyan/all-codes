class Sol{
	
	static void rotate2DArray(int arr[][], int n){
		
		// 1 - Find Transpose
		for(int i = 0; i < n; i++){
			
			for(int j = i + 1; j < n; j++){
				
				int temp = arr[i][j];

				arr[i][j] = arr[j][i];

				arr[j][i] = temp;
			}
		}

		// 2 - Reverse every column
		int idx = n-1;

		for(int i = 0; i < n; i++){
		
			for(int j = 0; j < n / 2; j++){
				
				int temp = arr[j][i];

				arr[j][i] = arr[idx][i];

				arr[idx][i] = temp;

				idx--;
			}	

			idx = n - 1;
		}

		for(int i = 0; i < n; i++){
			
			for(int j = 0; j < n; j++){

				System.out.print(arr[i][j] + " ");
			}

			System.out.println();
		}
	}

	public static void main(String[] args){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

		int n = 3;

		rotate2DArray(arr, n);
	}
}
