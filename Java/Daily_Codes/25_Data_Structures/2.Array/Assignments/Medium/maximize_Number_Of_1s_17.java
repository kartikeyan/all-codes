class Sol{
	
	static int maxOnes(int arr[], int n, int k){
		
		int res = 0, zeros, ones;

		for(int i = 0; i < n; i++){
			
			zeros = 0;
			ones = 0;

			int j = i;

			for(    ;j < n; j++){
				
				if(arr[j] == 0){
					
					zeros++;
				}else
					ones++;

				if(zeros > k){
					
					break;
				}
			}

			res = Math.max(res, j - i);
		}

		return res;
	}

	public static void main(String[] args){
		
		int arr[] = {1,0,0,1,1,0,1,0,1,1,1};

		int n = 11, k = 2;

		int ret = maxOnes(arr, n, k);

		System.out.println("Maximize number of 1s are " + ret);
	}
}
