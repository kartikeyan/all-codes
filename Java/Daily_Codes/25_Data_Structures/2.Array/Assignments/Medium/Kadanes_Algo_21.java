class Sol{
/*	
	static void maximumSubarray(int arr[], int n){
		
		int max = Integer.MIN_VALUE;

		for(int i = 0; i < n; i++){
			
			int s = 0;

			for(int j = i; j < n; j++){
				
				s = s + arr[j];

				if(s > max){
					
					max = s;

					System.out.print(arr[j] + " "); 
				}
			}
		}
		
		System.out.println();
		System.out.println("Max Subarray sum is " + max);
	}
*/
	static void maximumSubarray(int arr[], int n){
		
		int max = Integer.MIN_VALUE;
		
		int s = 0;

		for(int i = 0; i < n; i++){
	
			s = s + arr[i];

			if(s > max){
				
				max = s;

				System.out.print(arr[i] + " ");
			}

			if(s < 0){
				
				s = 0;
			}
			
		}
		
		System.out.println();
		System.out.println("Max Subarray sum is " + max);
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,3,-2,5};

		int n = 5;

		maximumSubarray(arr, n);
	}
}
