import java.util.*;

class Sol{
	
	static int noOfTriangles(int arr[]){

		int n = arr.length;
		
		int c = 0;

		for(int i = 0; i < n; i++){
			
			for(int j = i + 1; j < n; j++){
				
				for(int k = j + 1; k < n; k++){
					
					if((arr[i] + arr[j] > arr[k]) && (arr[i] + arr[k] > arr[j]) && (arr[j] + arr[k] > arr[i])){
						
						c++;
					}
				}
			}
		}

		return c;
	}

	public static void main(String[] args){
		
		int arr[] = {6,4,9,7,8};

		int ret = noOfTriangles(arr);

		System.out.println("Number of Triangles are " + ret);
	}
}
