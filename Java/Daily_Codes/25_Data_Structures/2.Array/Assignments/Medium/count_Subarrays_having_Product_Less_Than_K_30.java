class Sol{
	
	static void countSubArrays(int arr[], int n, int k){
		
		int c = 0;

		for(int i = 0; i < n; i++){
			
			int prod = 1;

			for(int j = i; j < n; j++){
				
				prod = prod * arr[j];

				if(prod < k){
					
					c++;

					System.out.print(arr[j] + " "); 
				}
			}
		}
		
		System.out.println();
		System.out.println("Count of Subarray having product less than K is " + c);
	}


	/*
	static void maximumSubarray(int arr[], int n){
		
		int max = Integer.MIN_VALUE;
		
		int s = 0;

		for(int i = 0; i < n; i++){
	
			s = s + arr[i];

			if(s > max){
				
				max = s;

				System.out.print(arr[i] + " ");
			}

			if(s < 0){
				
				s = 0;
			}
			
		}
		
		System.out.println();
		System.out.println("Max Subarray sum is " + max);
	}
	*/

	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		int n = 4, k = 10;

		countSubArrays(arr, n, k);
	}
}
