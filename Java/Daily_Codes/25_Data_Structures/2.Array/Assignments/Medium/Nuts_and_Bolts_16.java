import java.util.*;

class Sol{
/*	
	static void nutsAndBolts(char nuts[], char bolts[]){

		for(int i = 0; i < nuts.length; i++){
			
			char nut = nuts[i];

			for(int j = 0; j < bolts.length; j++){
				
				if(nut == bolts[j]){
					
					swap(bolts, i, j);
					break;
				}
			}
		}

		System.out.println("Matched Nuts and Bolts are : ");
		System.out.println(Arrays.toString(nuts));
		System.out.println(Arrays.toString(bolts));
    	}
*/
	static void nutsAndBolts(char nuts[], char bolts[]){
		
		HashMap<Character, Integer> mp = new HashMap<Character, Integer>();
		for(int i = 0; i < nuts.length; i++){
			
			mp.put(nuts[i], i);
		}

		for(int i = 0; i < bolts.length; i++){
			
			char bolt = bolts[i];

			if(mp.containsKey(bolt)){
				
				nuts[i] = bolts[i];
			}else{
				
				System.out.println("In bolt " + bolt + " no nut is present");
			}
		}

		System.out.println("(HashMap) Matched Nuts and Bolts are : ");
		System.out.println(Arrays.toString(nuts));
		System.out.println(Arrays.toString(bolts));
    	}
		
	static void swap(char bolts[], int i, int j){
		
		char temp = bolts[i];

		bolts[i] = bolts[j];

		bolts[j] = temp;
	}

	public static void main(String[] args){
		
		char nuts[] = {'@', '%', '$', '#', '^'};

		char bolts[] = {'%', '@', '#', '$', '^'};

		nutsAndBolts(nuts, bolts);
	}
}
