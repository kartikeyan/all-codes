import java.util.*;

class Sol{
/*	
	static boolean tripletsWithZeroSum(int arr[]) {

        	int n = arr.length;

		for(int i = 0; i < n - 2; i++){
			
			for(int j = i + 1; j < n - 1; j++){
				
				for(int k = j + 1; k < n; k++){
					
					if(arr[i] + arr[j] + arr[k] == 0){
						
						return true;
					}
				}
			}
		}

		return false;
    	}
*/
	static boolean tripletsWithZeroSum(int arr[]) {

        	int n = arr.length;

		for(int i = 0; i < n - 1; i++){
			
			HashSet s = new HashSet();

			for(int j = i + 1; j < n; j++){
				
				int x = -(arr[i] + arr[j]);
					
				if(s.contains(x)){
						
					return true;
				}else
					s.add(arr[j]);
			}
		}

		return false;
    	}

	public static void main(String[] args){
		
		int arr[] = {0,-1,2,-3,1};

		boolean ret = tripletsWithZeroSum(arr);
		
		if(ret == true){
			System.out.println("Exists");
		}else
			System.out.println("Not Exists");
	}
}
