import java.util.*;

class Sol{
	
	static List spiral(int arr[][]){
		
		List al = new ArrayList();

		int rows = arr.length;
		int cols = arr[0].length;

		int top = 0, left = 0, bottom = rows - 1, right = cols - 1;

		while(top <= bottom && left <= right){
			
			// moving left to right
			for(int i = left; i <= right; i++){
				
				al.add(arr[top][i]);
			}

			top++;

			// moving top to bottom
			for(int i = top; i <= bottom; i++){
				
				al.add(arr[i][right]);
			}

			right--;

			// movinf right to left
			if(top <= bottom){
				
				for(int i = right; i >= left; i--){
					
					al.add(arr[bottom][i]);
				}

				bottom--;
			}

			// moving left to right
			if(left <= right){
				
				for(int i = bottom; i >= top; i--){
					
					al.add(arr[i][left]);
				}

				left++;
			}
		}

		return al;
	}

	public static void main(String[] args){
		
		int arr[][] = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};

		List al = spiral(arr);

		for(int i = 0; i < al.size(); i++){
			
			System.out.print(al.get(i) + " ");
		}

		System.out.println();
	}
}
