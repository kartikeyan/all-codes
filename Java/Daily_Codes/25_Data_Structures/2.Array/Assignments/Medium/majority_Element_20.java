import java.util.*;

class Sol{
/*
	static int majorityEle(int arr[]){

		int n = arr.length;

		for(int i = 0; i < n; i++){
			
			int c = 0;

			for(int j = 0; j < n; j++){
				
				if(arr[i] == arr[j]){
					
					c++;
				}
			}

			if(c > (n / 2)){
				
				return arr[i];
			}
		}

		return -1;
    	}
*/
	static int majorityEle(int arr[]){

		int n = arr.length;

		int cnt = 0, ele = 0;

		for(int i = 0; i < n; i++){

			if(cnt == 0){
				
				cnt = 1;
				
				ele = arr[i];

			}else if(ele == arr[i]){
				
				cnt++;
			}else
				cnt--;
		}

		int cnt1 = 0;

		for(int i = 0; i < n; i++){
			
			if(arr[i] == ele){
				
				cnt1++;
			}
		}

		if(cnt1 > (n / 2)){
			
			return ele;
		}

		return -1;
    	}

	public static void main(String[] args){
		
		int arr[] = {3,1,3,3,2};

		int ret = majorityEle(arr);
		
		System.out.println("Majority Ele is " + ret);
	}
}
