
class Sol {
/*
	static int[] findMissingRepeatingNumbers(int[] a, int n) {

	        int repeating = -1, missing = -1;

        	//Find the repeating and missing number:
		for (int i = 1; i <= n; i++) {

            		//Count the occurrences:
	            	int cnt = 0;

            		for (int j = 0; j < n; j++) {

                		if (a[j] == i) cnt++;
            		}

            		if (cnt == 2) repeating = i;

            		else if (cnt == 0) missing = i;

            		if (repeating != -1 && missing != -1)
		                break;
        	}

		int[] ans = {repeating, missing};
		return ans;
    	}
*/

	static int[] findMissingRepeatingNumbers(int arr[], int n){
		
		int newArr[] = new int[n + 1];

		for(int i = 0; i < n; i++){
			
			newArr[arr[i]]++;
		}

		int repeat = -1, missing = -1;

		for(int i = 1; i <= n; i++){
			
			if(newArr[i] == 2){
				
				repeat = i;
			}else if(newArr[i] == 0){
				
				missing = i;
			}

			if(repeat != -1 && missing != -1){
				
				break;
			}
		}

		int ans[] = {repeat,missing};

		return ans;
	}

	public static void main(String[] args) {

        	int[] a = {1,3,3};
		
		int n = 3;

        	int[] ans = findMissingRepeatingNumbers(a, n);

        	System.out.println("The repeating and missing numbers are: {"
                           + ans[0] + ", " + ans[1] + "}");
    	}
}

