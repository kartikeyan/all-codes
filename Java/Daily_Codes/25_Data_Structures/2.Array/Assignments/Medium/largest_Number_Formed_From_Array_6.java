import java.util.*;

class Sol{
	
	static void largestNum(Vector<String> arr){
		
		Collections.sort(arr, new Comparator<String>()
		{
			public int compare(String X, String Y){
				
				String XY = X + Y;

				String YX = Y + X;

				return XY.compareTo(YX) > 0 ? -1 : 1;
			}
		});

		Iterator it = arr.iterator();

		while(it.hasNext()){
			
			System.out.print(it.next());
		}

		System.out.println();
	}

	public static void main(String[] args){
		
		Vector<String> vc = new Vector<>();

		vc.add("54");
		vc.add("546");
		vc.add("548");
		vc.add("60");

		largestNum(vc);
	}
}
