
import java.util.*;

class Sol{
	
	// USing sorting
	static void kThLargest(int arr[], int n, int k){
		
		// using merge sort, quick sort, etc
		Arrays.sort(arr);
		

		// here we have to print in reverse, simple task is to reverse the array
		for(int i = n - k; i < n; i++){
			
			System.out.print(arr[i] + " ");
		}

		System.out.println();
	}

	public static void main(String[] args){
		
		int arr[] = {1,23,12,9,30,2,50};

		int n = 7;

		int k = 3;

		kThLargest(arr, n, k);
	}
}
