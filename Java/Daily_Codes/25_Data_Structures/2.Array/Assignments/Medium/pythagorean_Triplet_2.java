import java.util.*;

class Sol{

/*	
	static boolean pythagoreanTriplet(int arr[], int n){

		for(int i = 0; i < arr.length; i++){

			for(int j = i+1; j < arr.length; j++){

				for(int k = j+1; k < arr.length; k++){
					
					int x = arr[i] * arr[i], y = arr[j] * arr[j], z = arr[k] * arr[k];
					if(x == y + z || y == x + z || z == x + y){
						return true;
					}
				}
			}
		}

		return false;
	}
*/
	
	static boolean pythagoreanTriplet(int arr[], int n){
		
		// 1 - square array elements

		for(int i = 0; i < arr.length; i++){
			
			arr[i] = arr[i] * arr[i];
		}

		// 2 - sort array elements

		Arrays.sort(arr);

		// 3

		for(int i = n - 1; i >= 2; i--){
			
			int l = 0, r = i - 1;

			while(l < r){
				
				if(arr[l] + arr[r] == arr[i]){
					
					return true;
				}

				if(arr[l] + arr[r] < arr[i]){
					
					l++;
				}else
					r--;
			}
		}

		return false;
	}

	public static void main(String[] args){
		
		int arr[] = {3,2,4,6,5};

		int n = 5;

		boolean ret = pythagoreanTriplet(arr, n);
		
		if(ret == true){
			System.out.println("Exists");
		}else
			System.out.println("Not Exists");
	}
}
