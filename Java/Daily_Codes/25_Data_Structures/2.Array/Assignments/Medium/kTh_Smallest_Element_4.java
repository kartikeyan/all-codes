
import java.util.*;

class Sol{
	
	// USing sorting
/*
	static int kThSmallest(int arr[], int n, int k){
		
		// using merge sort, quick sort, etc
		Arrays.sort(arr);

		return arr[k-1];
	}
*/
	// USing Set data structure
	static int kThSmallest(int arr[], int n, int k){
		
		k--;

		Set s = new TreeSet();

		for(int i = 0; i < n; i++){
			
			s.add(arr[i]);
		}

		Iterator itr = s.iterator();

		while(k > 0){
			
			itr.next();

			k--;
		}

		return (int)itr.next();
	}

	public static void main(String[] args){
		
		int arr[] = {7,10,4,3,20,15};

		int n = 6;

		int k = 3;

		int ret = kThSmallest(arr, n, k);

		System.out.println("kTh Smallest Element is " + ret);
	}
}
