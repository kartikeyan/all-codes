import java.util.*;

class Sol{

	static boolean jumpGame(int arr[]){
		
		// Leetcode

		//it shows at max what index can I reach.	
	        //initially I can only reach index 0, hence reach = 0
		int reach = 0;

		for(int i = 0; i < arr.length; i++){
		//at every index I'll check if my reach was atleast able to 
		//reach that particular index.		

			//reach >= idx -> great, carry on. Otherwise,
			if(i > reach){
				
				return false;
			}
			
		//now as you can reach this index, it's time to update your reach
		//as at every index, you're getting a new jump length.
			reach = Math.max(reach, i + arr[i]);
		}

		//this means that you reached till the end of the array.
		return true;
    	}

	public static void main(String[] args){
		
		int arr[] = {1,2,0,3,0,0};

		boolean ret = jumpGame(arr);
		
		if(ret == true){
			System.out.println("Can reach");
		}else
			System.out.println("Cannot reach");
	}
}
