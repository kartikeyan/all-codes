import java.util.*;

class Sol{
	
	static int[] maximumOfAll(int[] a, int k){

        	int n = a.length;

		int[] maxOfSubarrays = new int[n - k + 1];

	        int idx = 0;

		for(int i = 0; i <= n-k; i++){

			int maxElement = Integer.MIN_VALUE;

	            	for(int j = i; j < i+k; j++){

	                	if(maxElement < a[j]){

	                    		maxElement = a[j];
        	        	}
            	    	}	

	            	maxOfSubarrays[idx++] = maxElement;
        	}

        	return maxOfSubarrays;
    	}

	public static void main(String[] args){
		
		int arr[] = {1,2,3,1,4,5,2,3,6};

		int k = 3;

		int ret[] = maximumOfAll(arr, k);
		
		System.out.println("Maximum of all subarrays are");
		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
