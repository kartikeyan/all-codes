
class Sol{
	
	static int zeroSum(int arr[], int n){
		
		int c = 0;

		for(int i = 0; i < arr.length; i++){
			
			int s = 0;

			for(int j = i; j < arr.length; j++){

				s = s + arr[j];

				if(s == 0){
					
					c++;
				}
			}
		}

		return c;
	}

	public static void main(String[] args){
		
		int arr[] = {0,0,5,5,0,0};

		int n = 6;

		int ret = zeroSum(arr, n);

		System.out.println("Zero Sum Subarrays are " + ret);
	}
}
