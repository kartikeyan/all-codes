import java.util.*;

class Sol{
	
	static int indexOfAll1s(int arr[][]) {

        	int n = arr.length;

	        int rowIndex = 0, maxCount = 0;

		for(int i = 0; i < arr.length; i++) {
		
			int count = 0;

	            	for(int j = 0; j < arr[i].length; j++) {

	                	if(arr[i][j] == 1) {

	                    		count++;	
        	        	}
            	    	}	

	            	if(count > maxCount){
				
				maxCount = count;
				rowIndex = i;
			}
        	}

        	return rowIndex;
    	}

	public static void main(String[] args){
		
		int arr[][] = {{0,0,0,1},{0,1,1,1},{1,1,1,1},{0,0,0,0}};

		int ret = indexOfAll1s(arr);
		
		System.out.println("Index of row with maximum 1s is " + ret);
	}
}
