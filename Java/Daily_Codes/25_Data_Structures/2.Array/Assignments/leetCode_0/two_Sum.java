
import java.util.*;

class Sol{
	/*
	static int[] twoSum(int arr[], int k){
		
		int newArr[] = new int[2];

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] + arr[j] == k){
					
					newArr[0] = i;
					newArr[1] = j;

					break;
				}
			}
		}

		return newArr;		// T.C = O(N^2)
					// S.C = O(1)
	}
	*/

	static int[] twoSum(int arr[], int k){
		
		Arrays.sort(arr);

		int s = 0, e = arr.length - 1;

		int newArr[] = new int[2];	

		while(s < e){	
			
			int sum = arr[s] + arr[e];	

			if(sum == k){		// T.C = O(N log N) + O(N) = O(N
						// S.C = O(1)
				
				newArr[0] = s;
				newArr[1] = e;

				break;
			}else if(sum < k){
				
				s++;
			}else
				e--;
		}

		return newArr;
	}
	public static void main(String[] args){
		
		int arr[] = new int[]{2,7,11,15};
		
		int k = 9;

		int ret[] = twoSum(arr, k);
		
		for(int i = 0; i < ret.length; i++){
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}
