
import java.util.*;

class Sol{
	
	static int insertPos(int []arr, int k){
		
		int start = 0, end = arr.length - 1;

		if(k > arr[end-1]){
			
			return end;
		}

		while(start <= end){
			
			int mid = start + (end - start) / 2;

			if(arr[mid] == k){
				
				return mid;
			}

			if(arr[mid] < k){
				
				start = mid + 1;
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}
		}

		return end+1;		// T.C = O(log N)
					// S.C = O(1)
	}
	
	public static void main(String[] args){
		
		int arr[] = new int[]{1,3,5,6};

		int target = 2;

		int ret = insertPos(arr, target);
		
		System.out.println("Inserted Index is " + ret);
	}
}
