
import java.util.*;

class Sol{
	
	static int reverse(int n){
		
		int rev = 0;

		while(n != 0){
			
			if(rev > Integer.MAX_VALUE / 10 || rev < Integer.MIN_VALUE / 10){
				
				return 0;
			}

			rev = rev * 10 + n % 10;

			n = n / 10;
		}

		return rev;		// T.C = O(N)
					// S.C = O(1)
	}
	
	public static void main(String[] args){
		
		int n = 123;

		int ret = reverse(n);
		
		System.out.println("Reverse Integer is " + ret);
	}
}
