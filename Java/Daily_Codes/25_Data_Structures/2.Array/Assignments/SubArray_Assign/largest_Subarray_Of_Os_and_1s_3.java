
/*
 	Largest Subarray with an equal 0's and 1's

	Given an array of 0s and 1s.Find the length subarray with equal number of 0s and 1s

	arr[] = {0,1,0,1}
	o.p : 4
	Expla : The array from index [0...3] contains equal numbers of 0s and 1s.Thus maximum length of subarray having equal number of 0s and 1s is 4.

	arr[] = {0,0,1,0,0}
	o.p : 2

	T.C = O(N)
	S.C = O(N)
*/

class Sol{
	
	public static void main(String[] args){
		
		int arr[] = {0,1,0,1};

		int sum = 0, maxLen = -1;

		for(int i = 0; i < arr.length - 1; i++){
			
			sum = (arr[i] == 0) ? -1 : 1;

			for(int j = i+1; j < arr.length; j++){
				
				if(arr[i] == 0){
					
					sum += -1;
				}else
					sum += 1;

				if(sum == 0 && maxLen < j - i + 1){
					
					maxLen = j - i + 1;
				}
			}
		}

		System.out.println("Longest Subarrays is " + maxLen);
	}
}
