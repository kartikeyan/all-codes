/*
 	Equal Left and Right Subarray Sum

	arr[] = {1,3,5,2,2}
	o.p : 3
*/

class Sol{

	static int equalLeftRight(int arr[]){
		
		int totalSum = 0;

		for(int i = 0; i < arr.length; i++){
			
			totalSum += arr[i];
		}

		int temp = 0;

		for(int i = 0; i < arr.length; i++){
			
			totalSum -= arr[i];

			if(totalSum == temp){
				
				return i+1;
			}

			temp += arr[i];
		}

		return -1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,3,5,2,2};

		int ret = equalLeftRight(arr);

		System.out.println("Index having equal left and right sum is " + ret);
	}
}
