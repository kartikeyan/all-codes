
/*
 	Maximum of all subarrays of size k.

	arr[] = {1,2,3,1,4,5,2,3,6}, k = 3

	o.p : 3 3 4 5 5 5 6

		1st contiguous subarray = {1,2,3} ==> max = 3
		1st contiguous subarray = {2,3,1} ==> max = 3
		1st contiguous subarray = {3,1,4} ==> max = 4
		1st contiguous subarray = {1,4,5} ==> max = 5
		1st contiguous subarray = {4,5,2} ==> max = 5
		1st contiguous subarray = {5,2,3} ==> max = 5
		1st contiguous subarray = {2,3,6} ==> max = 6
*/


