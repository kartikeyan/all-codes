
/*
 	SubArray with given sum

	Given an unsorted array of size N that contains only positive integers, find a contiguous sub-array that adds to a
	given number S and return the left and right index(1-based indexing) of that subarray

	In case of multiple subarrays, return the subarray indexes which come first on moving from left to right.

	Note : You have to return an ArrayList consisting of two elements left and right.
	       In case no such subarrays exists, return an array consisting of element -1

	arr[] = {1,2,3,7,5}, S = 12

	Output : 2 4

	Explanation : The sum of elements from 2nd position to 4th position is 12


	arr[] = {1,2,3,4,5,6,7,8,9,10}, S = 15
	Output : 1 5

	Expected T.C = O(N)
	Expected S.C = O(N)
*/

class Sol{
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6,7,8,9,10};

		int k = 15;
	
		for(int i = 0; i < arr.length; i++){
			
			int sum = 0;

			for(int j = i; j < arr.length; j++){
				
				sum += arr[j];

				if(sum == k){
					
					System.out.println("Subarray found between indicex " + ++i + " and " + ++j);
					
				}
			}

				break;
		}
	}
}
