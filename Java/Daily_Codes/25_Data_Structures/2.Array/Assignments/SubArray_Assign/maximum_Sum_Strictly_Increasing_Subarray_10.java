
/*
 	Find the maximum sum of strictly increasing subarray

	arr[] = {1,2,3,2,5,1,7}

	o.p : 8

	Explanation :
		
		Some Strictly incresing sunarrays are :-

		{1,2,3}  sum = 6,
		{2, 5}  sum = 7,
		{1,7} sum = 8.
		maximum sum = 8
*/


