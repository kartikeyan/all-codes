
/*
 	Smallest Subarray with sum greater than a given value

	arr[] = {1, 4, 45, 6, 0, 19}

	x = 51

	o.p : 3
*/

class Sol{

	static int smallestSub(int arr[], int x){

		int n = arr.length;
		
		int currSum = 0, minLen = n + 1;

		int start = 0, end = 0;

		while(end < n){
			
			while(currSum <= x && end < n){
				
				currSum += arr[end++];
			}

			while(currSum > x && start < n){

				if(end - start < minLen){
					
					minLen = end - start;
				}

				currSum -= arr[start++];
			}
		}

		return minLen;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1, 4, 45, 6, 0, 19};

		int x = 51;

		int ret = smallestSub(arr, x);

		System.out.println("Smallest Subarray with given sum " + ret);
	}
}
