
/*
 	Subarray with 0 sum

	Given an array of positive and negative numbers.Find if there is a subarray with 0 sum.

	arr[] = {4,2,-3,1,6}
	o.p : Yes
	Expla : 2, -3, 1 is the subarray with sum 0.

	arr[] = {4,2,0,1,6}
	o.p : Yes

	T.C = O(N)
	S.C = O(N)
*/

import java.util.*;

class Sol{

	static boolean subArrayExists(int arr[]){
	
	/*	
		for(int i = 0; i < arr.length; i++){
			
			int sum = arr[i];

			if(sum == 0){
			
				return true;
			}

			for(int j = i + 1; j < arr.length; j++){
				
				sum += arr[j];

				if(sum == 0){
					
					return true;
				}
			}
		}

		return false;
	*/
		HashMap<Integer, Boolean> hp = new HashMap<>();

		int sum = 0;

		for(int i = 0; i < arr.length; i++){
			
			sum += arr[i];

			if(hp.containsKey(sum) || sum == 0){
				
				return true;
			}else
				hp.put(sum, false);
		}

		return false;
	}
	
	public static void main(String[] args){
		
		int arr[] = {0,1,0,1};

		boolean ret = subArrayExists(arr);

		if(ret == true){
			
			System.out.println("0 sum subarray exists");
		}else
			System.out.println("Not exists");
	}
}
