
/*
 	152. Maximum Product Subarray


	Given an integer array nums, find a
subarray that has the largest product, and return the product.

	The test cases are generated so that the answer will fit in a 32-bit integer.

	Example 1:

	Input: nums = [2,3,-2,4]
	Output: 6
	Explanation: [2,3] has the largest product 6.

	Example 2:

	Input: nums = [-2,0,-1]
	Output: 0
	Explanation: The result cannot be 2, because [-2,-1] is not a subarray.

	Constraints:

	    1 <= nums.length <= 2 * 104
	    -10 <= nums[i] <= 10
	    The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

*/

class Sol{
	
	// Brute force - 1
/*	
	static int maximumProduct(int arr[]){
		
		int maxProd = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				int prod = 1;

				for(int k  = i; k <= j; k++){
					
					prod = prod * arr[k];
				}

				maxProd = Math.max(prod, maxProd);
			}
		}

		return maxProd;
	}
*/
	
	// Brute force - 2
/*	
	static int maximumProduct(int arr[]){
		
		int maxProd = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){

			int prod = arr[i];
			
			for(int j = i + 1; j < arr.length; j++){
				
				maxProd = Math.max(maxProd, prod);

				prod = prod * arr[j];
			}

			maxProd = Math.max(maxProd, prod);
		}

		return maxProd;
	}
*/
	static int maximumProduct(int arr[]){
		
		int maxProd = Integer.MIN_VALUE;

		int pre = 1, suff = 1;

		int n = arr.length;

		for(int i = 0; i < arr.length; i++){

			if(pre == 0){
				
				pre = 1;
			}
			if(suff == 0){
				
				suff = 1;
			}

			pre *= arr[i];

			suff *= arr[n - i - 1];

			maxProd = Math.max(maxProd, Math.max(pre, suff));
		}

		return maxProd;
	}

	public static void main(String[] args){
		
		int arr[] = {6, -3, -10, 0, 2};

		int ret = maximumProduct(arr);

		System.out.println("Maximum Product Subarray is " + ret);
	}
}
