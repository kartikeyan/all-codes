class Sol{
/*
	static int[] product(int arr[]){
		
		int prod = 1;

		for(int i = 1; i < arr.length; i++){
			
			prod *= arr[i];
		}
		
		System.out.println(prod);

		for(int i = 0; i < arr.length; i++){		// T.C = O(N)
								// S.C = O(1)
			arr[i] = prod / arr[i] ;		
		}

		return arr;
	}
*/
/*	
	static int[] product(int arr[]){
		
		int n = arr.length;

		int pre[] = new int[n];

		int suff[] = new int[n];	

		pre[0] = 1;
		suff[n - 1] = 1;

		for(int i = 1; i < arr.length; i++){

			pre[i] = pre[i - 1] * arr[i - 1];
		}
		
		System.out.println("Prefix Array is");
		for(int i = 0; i < pre.length; i++){
			
			System.out.print(pre[i] + " ");
		}
		System.out.println();

		for(int i = n - 2; i >= 0; i--) {

            		suff[i] = suff[i + 1] * arr[i + 1];
		}

		System.out.println("Suffix Array is");
                for(int i = 0; i < suff.length; i++){

                        System.out.print(suff[i] + " ");
                }
                System.out.println();

	        for(int i = 0; i < n; i++) {		// T.C = O(N)
							// S.C = O(N)

			arr[i] = pre[i] * suff[i];
	        }

		return arr;
	}
*/
	static int[] product(int arr[]){
		
		int n = arr.length;

		int countZeroes = 0;
		int indexZero = -1;
		int productWithoutZero = 1;

		for(int i = 0 ; i < n ; i++) {

			if(arr[i] == 0) {

				countZeroes++;
				indexZero = i;
			}
			else {
				productWithoutZero *= arr[i];
			}
		}

		int[] output = new int[n];

		if(countZeroes == 0) {

			for(int i = 0 ; i < n ; i++) {

				output[i] = productWithoutZero / arr[i];
			}
		}
		else if(countZeroes == 1) {

			output[indexZero] = productWithoutZero;
		}

		return output;
	}

	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5};

		int ret[] = product(arr);

		System.out.println("Product Array IS");
		for(int x : ret){
			
			System.out.print(x + " ");
		}
		System.out.println();
	}	
}

