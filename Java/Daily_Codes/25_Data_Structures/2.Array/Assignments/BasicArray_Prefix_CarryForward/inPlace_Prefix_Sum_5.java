
class Sol{

	static int[] inPlace(int arr[]){

		for(int i = 1; i < arr.length; i++){
			
			arr[i] = arr[i - 1] + arr[i];
		}

		return arr;
	}

	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5};

		int ret[] = inPlace(arr);

		System.out.println("Prefix Array IS");
		for(int x : ret){
			
			System.out.print(x + " ");
		}
		System.out.println();
	}	
}
