
/*
 	In One Second, you can increase the value of one element by 1.
	Find the minimum time in seconds to make all elements of array equal.

	arr[] = {2,4,1,3,2};
	o.p : 8
*/

class Sol{
/*
	static int minMoves(int arr[]){
		
		int totalSum = 0;
		for(int i = 0; i < arr.length; i++){
			
			totalSum += arr[i];
		}

		int min = Integer.MAX_VALUE;
		for(int i = 0; i < arr.length; i++){
			if(arr[i] < min){
				
				min = arr[i];
			}
		}

		return totalSum - (arr.length * min);
	}
*/
	static int minMoves(int arr[]){

                int min = Integer.MAX_VALUE;
                for(int i = 0; i < arr.length; i++){
                        if(arr[i] < min){

                                min = arr[i];
                        }
                }
		
		/*
		 	Take the minimum number from the array and then substarct that minimum number from all the elements in the array, the final answer is the sum of the difference between the minimum number and the integer values in the given array.
		*/

		int c = 0;

		for(int i = 0; i < arr.length; i++){
			
			c = c + arr[i] - min;
		}

                return c;
        }

	public static void main(String[] args){
		
		int arr[] = new int[]{2,4,1,3,2};

		int ret = minMoves(arr);

		System.out.println(ret);
	}	
}
