
/*
 	Given an integer array A of length N

 	Given an 2D integer array with dimension M * 2, where each row denotes [L, R] query.

	For each query, you have to find the sum of all elements from L to R indices in A.

	More formally, find A[L] + A[L+1] + A[L+2] + ...... + A[R-1] + A[R] for each query.

	arr[] = {1,2,3,4,5};
	o.p : [10,5]
	expla : The sum of all elements of A[0.....3] = 1 + 2 + 3 + 4 = 10
		The sum of all elements of A[1...2] = 2 + 3 = 5

	arr[] = {{0,3},{1,2}}
	o.p : [2,4]
	expla : The sum of all elements of A[0....0] = 2 = 2
                The sum of all elements of A[1...2] = 2 + 2 = 4
*/

class Sol{

	static int minMax(int arr[]){
		
		
	}

	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5,6};

		int ret = minMax(arr);

		System.out.println(ret);
	}	
}
