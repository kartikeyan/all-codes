
class Sol{

	static int minMax(int arr[]){
		
		int min = Integer.MAX_VALUE, max = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] > max){
				
				max = arr[i];
			}

			if(arr[i] < min){
				
				min = arr[i];
			}
		}

		return max + min;
	}

	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,3,4,5,6};

		int ret = minMax(arr);

		System.out.println(ret);
	}	
}
