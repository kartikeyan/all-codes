
import java.util.*;

class Sol{
/*
	static List leadersInArray(int arr[]){
		
		List al = new ArrayList();

		for(int i = 0; i < arr.length; i++){
			
			int flag = 1;

			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[j] > arr[i]){
					
					flag = 0;
					break;
				}
			}				// T.C = O(N)
							// S.C = O(N)

			if(flag == 1){
				
				al.add(arr[i]);
			}
		}

		return al;
	}
*/
	static List leadersInArray(int arr[]){
		
		List al = new ArrayList();

		int n = arr.length;

		int max = arr[n-1];

		al.add(max);	// last ele is always leader

		for(int i = n - 2; i >= 0; i--){
			
			if(arr[i] > max){
				
				al.add(arr[i]);

				max = arr[i];
			}
		}

		return al;
	}

	public static void main(String[] args){
		
		int arr[] = new int[]{16,17,4,3,5,2};

		List al = leadersInArray(arr);	// [17,2,5]

		System.out.println("Leaders Are");
		for(int i = 0; i < al.size(); i++){
			
			System.out.print(al.get(i) + " ");
		}
		System.out.println();
	}	
}
