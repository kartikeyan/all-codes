
class Sol{

	static int linearSearch(int arr[], int k){
		
		int c = 0;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == k){
				
				c++;
			}
		}

		return c;
	}

	public static void main(String[] args){
		
		int arr[] = new int[]{1,2,2};

		int k = 2;

		int ret = linearSearch(arr, k);

		System.out.println(ret);
	}	
}
