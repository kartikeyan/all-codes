
import java.util.*;

class Sol{

	static void duplicates(int arr[]){

		HashMap<Integer,Boolean> al = new HashMap<>();

		for(int i = 0; i < arr.length; i++){
			
			if(!al.containsKey(arr[i])){

				al.put(arr[i],true);
			}
		}
		
		Set<Map.Entry<Integer, Boolean>> data = al.entrySet();

		Iterator itr = data.iterator();

		while(itr.hasNext()){
			
			System.out.println(itr.next());
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,1,4,2};

		duplicates(arr);
	}
}

