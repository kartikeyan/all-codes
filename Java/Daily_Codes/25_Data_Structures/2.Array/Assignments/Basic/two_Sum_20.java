
class Sol{

	static boolean twoSum(int arr[], int x){

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] + arr[j] == x){
					
					return true;
				}
			}
		}
		return false;
	}
	
	public static void main(String[] args){
		
		int arr[] = {0,-1,2,-3,1};

		int x = -2;

		boolean ret = twoSum(arr,x);

		if(ret == true){
			
			System.out.println("TWo Sum Exists");
		}else
			System.out.println("Two Sum not exists");
	}
}

