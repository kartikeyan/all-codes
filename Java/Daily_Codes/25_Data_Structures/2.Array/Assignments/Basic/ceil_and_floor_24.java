
class Sol{

	static void ceilFloor(int arr[], int x){

		int ceil = Integer.MIN_VALUE, floor = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
				
			if(arr[i] <= x){
					
				floor = arr[i];
			}

			if(arr[i] >= x){
				
				ceil = arr[i];
				break;
			}
		}
		
		System.out.println("Floor is " + floor);

		System.out.println("Ceil is " + ceil);

	}
	
	public static void main(String[] args){
		
		int arr[] = {5,6,8,9,6,5,5,6};

		int x = 7;

		ceilFloor(arr, x);
	}
}


//	Optimized Sol is using binary Search
