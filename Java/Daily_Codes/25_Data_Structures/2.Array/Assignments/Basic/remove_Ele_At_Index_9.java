
import java.util.*;

class Sol{

	static int[] remove(int arr[], int idx){
	/*
		int newArr[] = new int[arr.length];

		for(int i = 0; i < newArr.length; i++){
			
			if(i != idx){
				newArr[i] = arr[i];	
			}
		}
		return newArr;
	*/

		ArrayList al = new ArrayList();

		for(int i = 0; i < arr.length;i++){
			
			al.add(arr[i]);
		}

		al.remove(idx);

		int[] newArray = new int[al.size()];

		for(int i = 0; i < al.size(); i++){
			
			newArray[i] = (int)al.get(i);
		}

		return newArray;
	}
	
	public static void main(String[] args){
		
		int arr[] = {4,5,9,8,1};
		
		int idx = 3;

		int[] ret = remove(arr, idx);
		
		System.out.println("Returned Array is");
		for(int i = 0; i < ret.length; i++){
			System.out.print(ret[i] + " ");
		}
		System.out.println();
	}
}

