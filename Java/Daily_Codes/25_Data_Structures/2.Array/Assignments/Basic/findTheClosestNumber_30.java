
import java.util.*;

class Sol{

	static int closest(int arr[], int k){

		int c =  Integer.MIN_VALUE;

		for(int i = 0; i < arr.length-1; i++){
			
			if(arr[i] < k){
			
				c = arr[i];
			}
			
			if((arr[i+1] - k) == (k - arr[i])){
				
				return arr[i+1];	
			}
		}

		return c;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,5,6,8,9};

		int k = 4;

		int ret = closest(arr,k);

		System.out.println("Closest num is " + ret);
	}
}

