
import java.util.*;

class Sol{

	static void printLargest(Vector<String> arr){

		Collections.sort(arr, new Comparator<String>(){
			
			@Override 
			public int compare(String X, String Y){

				String XY = X + Y;

				String YX = Y + X;

				return XY.compareTo(YX) > 0 ? -1 : 1;
			}
		});

		Iterator it = arr.iterator();

		while (it.hasNext())
			System.out.print(it.next());

		System.out.println();
	}
	
	public static void main(String[] args){

		Vector<String> arr = new Vector<>();

		arr.add("9");
		arr.add("0");
		arr.add("1");
		arr.add("3");
		arr.add("0");

		printLargest(arr);	//	93100
	}
}

