import java.util.*;

class Sol{

	static int[] pendulum(int arr[]){

		Arrays.sort(arr);

		int temp = arr[0];

		int mid = (arr.length - 1) / 2;

		int k = 1;

		int newArr[] = new int[arr.length];

		for(int i = 1; i <= mid; i++){
			
			newArr[mid + i] = arr[k++];

			newArr[mid - i] = arr[k++];
		}

		newArr[mid] = temp;

		return newArr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {3,4,5,2,1};

		int ret[] = pendulum(arr);
		
		for(int i = 0; i < ret.length; i++){

			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}

