
import java.util.*;

class Sol{

	static ArrayList leaders(int arr[]){

		ArrayList v = new ArrayList();

		for(int i = 0; i < arr.length; i++){
			
			boolean leader = true;

			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[j] > arr[i]){
					
					leader = false;
					break;
				}
			}

			if(leader){
					
				v.add(arr[i]);
			}
		}

		return v;
	}
	
	public static void main(String[] args){
		
		int arr[] = {16,17,4,3,5,2};

		ArrayList al = leaders(arr);

		for(int i = 0; i < al.size(); i++){
			
			System.out.print(al.get(i) + " ");
		}

		System.out.println();
	}
}

