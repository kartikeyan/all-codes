
class Sol{

	static void minMax(int arr[]){
		
		int max = Integer.MIN_VALUE;

		int min = Integer.MAX_VALUE;

		for(int i = 0; i < arr.length-1; i++){
			
			if(arr[i+1] > max){
				
				max = arr[i+1];
			}

			if(arr[i] < min){
				min = arr[i];
			}
		}

		System.out.println("Max is " + max);

		System.out.println("Min is " + min);

	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		minMax(arr);
	}
}

