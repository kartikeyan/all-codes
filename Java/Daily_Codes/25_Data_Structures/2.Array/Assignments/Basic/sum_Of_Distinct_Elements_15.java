import java.util.*;

class Sol{

	static int sumDistinct(int arr[]){

		int s = 0;

		Set<Integer> hs = new HashSet<Integer>();

		for(int i = 0; i < arr.length; i++){
			
			if(hs.contains(arr[i]) == false){
				
				s += arr[i];

				hs.add(arr[i]);
			}
		}

		return s;
	}

	public static void main(String[] args){
		
		int arr[] = {5,5,5,5,5};

		int ret = sumDistinct(arr);

		System.out.println("Sum Of Distince Elements is " + ret);
	}
}
