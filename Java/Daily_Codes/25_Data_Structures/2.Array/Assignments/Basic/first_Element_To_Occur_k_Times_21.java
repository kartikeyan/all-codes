
import java.util.*;

class Sol{
/*
	static int firstEleK(int arr[], int k){

		for(int i = 0; i < arr.length; i++){
			
			int c = 0;

			for(int j = 0; j < arr.length; j++){
				
				if(arr[i] == arr[j])
					c++;
			}

			if(c == k){
				
				return arr[i];
			}
		}

		return -1;
	}
*/
	static int firstEleK(int arr[], int k){
	
		HashMap<Integer, Integer> hp = new HashMap<>();
	
		int n = arr.length;

		for(int i = 0; i < n; i++){
			
			int a = 0;

			if(hp.get(arr[i]) != null){
				
				a = hp.get(arr[i]);
			}

			hp.put(arr[i], a + 1);
		}

		for(int i = 0; i < arr.length; i++){
			
			if(hp.get(arr[i]) == k){
				
				return arr[i];
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		int arr[] = {1, 7, 4, 3, 4, 8, 7};

		int k = 2;

		int ret = firstEleK(arr, k);

		System.out.println("First Ele to occur K times " + ret);
	}
}
