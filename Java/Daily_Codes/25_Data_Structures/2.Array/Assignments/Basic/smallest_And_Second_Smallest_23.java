
class Sol{

	static void minTwoMin(int arr[]){

		int min = Integer.MAX_VALUE, min2 = Integer.MAX_VALUE;

		for(int i = 0; i < arr.length; i++){
				
			if(arr[i] < min){
					
				min = arr[i];
			}
		}

		for(int i = 0; i < arr.length; i++){
				
			if(arr[i] < min2 && arr[i] != min){
					
				min2 = arr[i];
			}
		}
		
		System.out.println("Smallest is " + min);

		System.out.println("Second Smallest is " + min2);

	}
	
	public static void main(String[] args){
		
		int arr[] = {2,4,3,5,6};

		minTwoMin(arr);
	}
}

