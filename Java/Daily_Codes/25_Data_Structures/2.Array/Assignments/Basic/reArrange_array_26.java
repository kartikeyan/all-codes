import java.util.*;
class Sol{

	static int[] rearrange(int arr[]){
	/*
		int posIndex = 0, negIndex = 1;

		int newArray[] = new int[arr.length];

		for(int i = 0; i < arr.length; i++){
				
			if(arr[i] > 0){
				
				newArray[posIndex] = arr[i];
				posIndex += 2;
			}else{
				newArray[negIndex] = arr[i];
				negIndex += 2;
			}
			
		}

		return newArray;
	*/
		ArrayList ps = new ArrayList();
		ArrayList ne = new ArrayList();

		for(int i : arr){
			
			if(i > 0){
				ps.add(i);
			}else
				ne.add(i);
		}

		for(int i = 0; i < arr.length; i++){
			
			if(i % 2 == 0){
				
				arr[i] = (int)ps.get(i/2);
			}else
				arr[i] = (int)ne.get(i/2);
		}

		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {-1,2,-3,4,-5,6};

		int ret[] = rearrange(arr);
		
		System.out.println("After REarranging");

		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
