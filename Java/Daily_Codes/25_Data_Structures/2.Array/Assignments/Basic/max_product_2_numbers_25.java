
class Sol{

	static int maxProd(int arr[]){
		
		int max = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] * arr[j] > max){
					
					max = arr[i] * arr[j];
				}
			}
		}

		return max;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,4,3,6,7,0};

		int ret = maxProd(arr);

		System.out.println("Max Product is " + ret);
	}
}

