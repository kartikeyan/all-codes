import java.util.*;

class Sol{

	static int[] evenOcc(int arr[]){
		
		HashMap<Integer, Integer> mp = new HashMap<>();

		for(int i = 0; i < arr.length; i++){
			
			if(mp.containsKey(arr[i])){
				
				mp.put(arr[i], mp.get(arr[i]) + 1);
			}else
				mp.put(arr[i], 1);
		}

		ArrayList<Integer> list = new ArrayList<>();

		for(Integer i : mp.keySet()){
			
			int ans = mp.get(i);

			if(ans % 2 == 0){
				
				list.add(i);
			}
		}

		if(list.isEmpty()){
			
			int a[] = new int[1];

			a[0] = -1;

			return a;
		}

		int newArr[] = new int[list.size()];

		for(int i = 0; i < list.size(); i++){
			
			newArr[i] = list.get(i);
		}

		Arrays.sort(newArr);

		return newArr;
	}

	public static void main(String[] args){
		
		int arr[] = {9, 12, 23, 10, 12, 12, 15, 23, 14, 12, 15};

		int ret[] = evenOcc(arr);
	
		for(int i = 0; i < ret.length; i++){

			System.out.println(ret[i]);
		}
	}
}
