
class Sol{

	static int replace(int n){
		
		String str = Integer.toString(n);

		StringBuilder s = new StringBuilder();

		char arr[] = str.toCharArray();

		for(char i = 0; i < arr.length; i++){
			
			if(arr[i] == '0'){
				
				s.append('5');
			}else
				s.append(arr[i]);
		}

		return Integer.parseInt(s.toString());
	}
	
	public static void main(String[] args){
		
		int n = 1004;

		int ret = replace(n);

		System.out.println("Num is " + ret);
	}
}

