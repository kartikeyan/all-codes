
class Sol{

	static int pairsSum(int arr1[], int arr2[], int sum){
		
		int c = 0;

		for(int i = 0; i < arr1.length; i++){
			
			for(int j = 0; j < arr2.length; j++){
				
				if(arr1[i] + arr2[j] == sum){
					
					c++;
				}
			}
		}

		return c;
	}
	
	public static void main(String[] args){
		
		int arr1[] = {1,3,5,7};

		int arr2[] = {2,3,5,8};

		int sum = 10;

		int ret = pairsSum(arr1,arr2,sum);

		System.out.println("Count of pairs equal are " + ret);
	}
}
