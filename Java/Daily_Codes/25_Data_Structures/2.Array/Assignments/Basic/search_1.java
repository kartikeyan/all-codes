
class Sol{

	static int searchArr(int arr[], int search){
		
		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == search){
				
				return i;
			}
		}

		return -1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		int search = 3;

		int ret = searchArr(arr, search);

		System.out.println("Search Ele found at index " + ret);
	}
}

//	for optimized Use Binary Search
