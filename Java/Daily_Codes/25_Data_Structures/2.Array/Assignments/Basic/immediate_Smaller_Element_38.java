
import java.util.*;

class Sol{

	static int[] immediateSmaller(int arr[]){

		for(int i = 0; i < arr.length-1; i++){

			if(arr[i] > arr[i+1]){
				
				arr[i] = arr[i+1];
			}else
				arr[i] = -1;
		}	

		arr[arr.length - 1] = -1;

		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {4,2,1,5,3};

		int ret[]  = immediateSmaller(arr);

		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}

