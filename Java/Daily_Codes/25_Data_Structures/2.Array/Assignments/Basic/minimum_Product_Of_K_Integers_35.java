import java.util.*;

class Sol{

	static int minProd(int arr[], int k){

		int prod = 1;

		Arrays.sort(arr);

		for(int i = 0; i < k; i++){
			
			prod = prod * arr[i];
		}

		return prod;
	}
	
	public static void main(String[] args){
		
		int arr[] = {9, 10, 8};

		int k = 2;

		int ret = minProd(arr, k);

		System.out.println("Min Prod of k integers is " + ret);
	}
}

