
import java.util.*;

class Sol{

	static char winning(int arr1[], int arr2[]){
	
		int t1 = 0, t2 = 0;

		for(int i = 0; i < arr1.length; i++){
			
			if(arr1[i] > arr2[i]){
				
				t1 = arr1[i] - arr2[i];
			}else
				t2 = arr2[i] - arr1[i];
		}

		if(t1 > t2){
			
			return 'A';
		}else
			return 'B';
	}
	
	public static void main(String[] args){
		
		int arr1[] = {2,2};

		int arr2[] = {5,5};

		char ret = winning(arr1, arr2);

		System.out.println("Country Winning is " + ret);
	}
}

