import java.util.*;

class Sol{

	static int[] moveAll(int arr[]){

		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			// if both are negative increment start	
			if(arr[start] < 0 && arr[end] < 0){
				
				start++;
			} 
			// is start is negative and end is positive then replace
			else if(arr[start] > 0 && arr[end] < 0){
				
				int temp = arr[start];

				arr[start] = arr[end];

				arr[end] = temp;

				start++;
				end--;
			}

			// if both is positive then decrement end
			else if(arr[start] > 0 && arr[end] > 0){
				
				end--;
			}
			else{	
				start++;
				end--;
			}
		}

		return arr;
	}
	
	public static void main(String[] args){
		
		int arr[] = {-12, 11, -13, -5, 6, -7, 5, -3, -6};

		int ret[]  = moveAll(arr);

		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}

