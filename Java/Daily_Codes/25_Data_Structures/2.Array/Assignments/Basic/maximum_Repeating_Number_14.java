import java.util.*;

class Sol{

	static int maximumRepeating(int arr[], int k){

		int maxCount = 0;

		for(int i = 0; i < arr.length; i++){
			
			int count = 1;

			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[j] == arr[i]){

					count++;

					if(count > maxCount){
						
						maxCount = arr[j];
					}
				}
			}
		}

		return maxCount;
	}

	public static void main(String[] args){
		
		int arr[] = {2,2,1,2};

		int k = 3;

		int ret = maximumRepeating(arr, k);

		System.out.println("Maximum Repeating Number " + ret);
	}
}
