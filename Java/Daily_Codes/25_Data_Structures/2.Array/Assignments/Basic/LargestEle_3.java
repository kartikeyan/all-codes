
class Sol{

	static int largest(int arr[]){
		
		int max = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length-1; i++){
			
			if(arr[i+1] > max){
				
				max = arr[i+1];
			}
		}
		return max;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		int ret = largest(arr);

		System.out.println("Largest Ele is " + ret);
	}
}
