
class Sol{

	static int prodMinMax(int arr1[], int arr2[]){
		
		int max = Integer.MIN_VALUE;

		int min = Integer.MAX_VALUE;

		for(int i = 0; i < arr1.length; i++){
			
			if(arr1[i] > max){
				
				max = arr1[i];
			}
		}

		for(int i = 0; i < arr2.length; i++){

                        if(arr2[i] < min){

                                min = arr2[i];
                        }
                }
		
		System.out.println(min);

		System.out.println(max);


		return max * min;
	}
	
	public static void main(String[] args){
		
		int arr1[] = {5,7,9,3,6,2};

		int arr2[] = {1,2,6,-1,0,9};

		int ret = prodMinMax(arr1, arr2);

		System.out.println("Product of 2 arrays is " + ret);
	}
}

