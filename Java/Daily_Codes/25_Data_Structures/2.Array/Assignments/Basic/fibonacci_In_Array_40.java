class Sol{

	static boolean isPerfect(int num){

		int n = (int)Math.sqrt(num);

		return (n * n == num);
	}

	static int fiboArr(int arr[]){
		
		int c = 0;

		for(int i = 0; i < arr.length; i++){
			
			if(isPerfect(5 * arr[i] * arr[i] + 4) || isPerfect(5 * arr[i] * arr[i] - 4)){
				System.out.println(arr[i] + " ");

				c++;
			}
		}

		if(c == 0){
			
			System.out.println("None Present");
		}
		
		return c;
	}
	
	public static void main(String[] args){
		
		int arr[] = {4, 2, 8, 5, 20, 1, 40, 13, 23};

		int ret = fiboArr(arr);

		System.out.println("Count is " + ret);
	}
}

