import java.util.*;

class Sol{

	static int maxOddSum(int arr[]){

		boolean isOdd = false;

		int sum = 0;

		int minimumOdd = Integer.MAX_VALUE;

		for(int num : arr){
			
			if(num > 0){

				sum += num;
			}

			if(num % 2 != 0){
				
				isOdd = true;

				if(minimumOdd > Math.abs(num)){
					
					minimumOdd = Math.abs(num);
				}
			}
		}

		if(sum % 2 == 0){
			
			sum = sum - minimumOdd;
		}

		return isOdd == false ? -1 : sum;
	}

	public static void main(String[] args){
		
		int arr[] = {4, -3, 3, -5};

		int ret = maxOddSum(arr);

		System.out.println("Max Odd Sum is " + ret);
	}
}
