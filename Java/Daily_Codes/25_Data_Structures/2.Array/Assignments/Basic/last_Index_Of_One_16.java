
import java.util.*;

class Sol{

	static int firstInd(String s){

		if(s.length() == 1 && s.charAt(0) == '1'){
			
			return 0;
		}

		for(int i = s.length()-1; i >= 0; i--){
			
			if(s.charAt(i) == '1'){
				
				return i;
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		String s = "00001";

		int ret = firstInd(s);

		System.out.println("First Index is " + ret);
	}
}
