
import java.util.*;

class Sol{

	static int countEle(int arr[], int n1, int n2){

		int c = 0, start = 0, end = 0;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == n1){
			
				start = i;
			}
			
			if(arr[i] == n2){
				
				end = i;
			}
		}

		for(int i = start+1; i < end; i++){
			
			c++;
		}

		return c;
	}
	
	public static void main(String[] args){
		
		int arr[] = {4,2,1,10,6};

		int n1 = 4, n2 = 6;

		int ret = countEle(arr,n1,n2);

		System.out.println("Count of number elements between 2 given elements are " + ret);
	}
}

