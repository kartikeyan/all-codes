import java.util.*;

class Sol{

	static int closestNumber(int arr[], int target){
		
		int s = 0, e = arr.length - 1;

		if(target < arr[s])
			return arr[s];

		if(target > arr[e])
			return arr[e];

		while(s <= e){
			
			int m = (s + e) / 2;

			if(arr[m] == target){
				
				return arr[m];
			}

			else if(arr[m] < target){
				
				s = m + 1;
			}

			else
				e = m - 1;
		}

		// if return true return floor
		if(Math.abs(arr[e] - target) < Math.abs(arr[s] - target)){
			
			return arr[e];
		}

		// otherwise return ceil
		return arr[s];
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,3,6,7};

		int k = 4;

		int ret = closestNumber(arr, k);

		System.out.println("Closest Number is " + ret);
	}
}

