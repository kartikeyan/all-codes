import java.util.*;

class Sol{

	static int multiplyLeftAndRight(int arr[]){

		int mid = arr.length / 2;

		int left = 0;
		for(int i = 0; i < mid; i++){
			
			left += arr[i];
		}

		int right = 0;
		for(int i = mid; i < arr.length; i++){
			
			right += arr[i];
		}

		return left * right;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		int ret = multiplyLeftAndRight(arr);

		System.out.println("Prod is " + ret);
	}
}

