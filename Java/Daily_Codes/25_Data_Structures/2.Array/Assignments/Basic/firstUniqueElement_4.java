
import java.util.*;

class Sol{

	static int unique(int arr[], int k){
		
		HashMap hp = new HashMap();

		for(int i = 0; i < arr.length; i++){
			
			if(hp.containsKey(arr[i])){
				
				hp.put(arr[i], (int)hp.getValue() + 1);
			}else
				hp.put(arr[i],1);
		}
		
		Set<Map.Entry> ss = hp.entrySet();

		Iterator<Map.Entry> itr = ss.iterator();
		
		while(itr.hasNext()){
			
			if((int)itr.next().getValue() % k != 0){
				
				return (int)itr.next().getKey();
			}
		}
/*
		for(Map.Entry entry : hp.entrySet()){
			if((int)entry.getValue() % k != 0){
				
				return (int)entry.getKey();
			}
		}
*/
		return -1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {6,2,5,2,2,6,6};

		int k = 3;

		int ret = unique(arr, k);

		System.out.println("Unique Ele is " + ret);
	}
}
