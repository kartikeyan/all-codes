
class Sol{

	static int index(String s){
		
		char arr[] = s.toCharArray();

		for(int i = arr.length-1; i > 0; i--){
			
			if(arr[i] == '1'){
				
				return i;
			}
		}

		return -1;
	}
	
	public static void main(String[] args){
		
		String s = "00001";

		int ret = index(s);

		System.out.println("Last Index of 1 in given string is " + ret);
	}
}
