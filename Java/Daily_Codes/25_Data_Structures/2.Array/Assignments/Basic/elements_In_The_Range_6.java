import java.util.*;

class Sol{

	static boolean eleInRange(int arr[], int a, int b){
		
		if(a > b){
			
			return false;
		}

		for(int i = a; i <= b; i++){
			
			boolean found = false;

			for(int j = 0; j < arr.length; j++){
				
				if(arr[j] == i){
					
					found = true;
					break;
				}
			}

			if(found == false){
				
				return false;
			}
		}

		return true;
	}
	
	public static void main(String[] args){

		int arr[] = {1,4,5,2,7,8,3};

		int a = 2, b = 5;

		if(eleInRange(arr, a, b)){
			
			System.out.println("Yes");
		}else
			System.out.println("No");
	}
}

