
class Sol{

	static int firstOcc(int arr[], int x){

		int start = 0, end = arr.length - 1, ans = -1;

		while(start <= end){
		
			int mid = (start + end) / 2;

			if(arr[mid] == x){
				
				ans = mid;

				end = mid - 1;
			}else if(arr[mid] < x){
				start = mid + 1;
			}else{
				end = mid - 1;
			}
		}

		return ans;
	}
	
	static int LastOcc(int arr[], int x){

		int start = 0;
		int end = arr.length - 1;

		int last = -1;

		while(start <= end){
		
			int mid = (start + end) / 2;

			if(arr[mid] == x){
				
				last = mid;

				start = mid + 1;

			}else if(arr[mid] < x){
				start = mid + 1;
			}else{
				end = mid - 1;
			}
		}

		return last;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,3,3,4};

		int x = 3;

		int ret1 = firstOcc(arr, x);
		System.out.println("First Occ of "+ x + " is at "+ ret1);
		
		int ret2 = LastOcc(arr, x);
		System.out.println("Last Occ of "+ x + " is at "+ ret2);
	
	}
}

