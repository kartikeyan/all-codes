import java.util.*;

class Sol{

	static int prodOfMaxMin(int arr1[], int arr2[]){

		int max = Integer.MIN_VALUE;

		for(int i = 0; i < arr1.length; i++){
			
			if(arr1[i] > max){
				
				max = arr1[i];
			}
		}

		int min = Integer.MAX_VALUE;

		for(int i = 0; i < arr2.length; i++){
			
			if(arr2[i] < min){
				
				min = arr2[i];
			}
		}

		return max * min;
	}

	public static void main(String[] args){
		
		int arr1[] = {5,7,9,3,6,2};

		int arr2[] = {1,2,6,-1,0,9};

		int ret = prodOfMaxMin(arr1, arr2);

		System.out.println("Prod of Max and Min " + ret);
	}
}
