
class Sol{

	static void subarray(int arr[], int sum){

		for(int i = 0; i < arr.length; i++){
			
			int s = arr[i];

			for(int j = i+1; j < arr.length; j++){

				s += arr[j];

				if(s == sum){
					
					System.out.println("Sum found between indices " + i + " " + j);
					return;
				}
			}
		}

		System.out.println("No Subarray found");
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,4,20,3,10,5};
		
		int sum = 33;

		subarray(arr, sum);
	}
}
