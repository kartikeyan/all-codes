
import java.util.*;

class Sol{
/*
	static int exceptionallyOdd(int arr[]){

		for(int i = 0; i < arr.length; i++){
			
			int c = 0;

			for(int j = 0; j < arr.length; j++){
				
				if(arr[i] == arr[j])
					c++;
			}

			if(c % 2 != 0){
				
				return arr[i];
			}
		}

		return -1;
	}
*/
	static int exceptionallyOdd(int arr[]){
	
		HashMap<Integer, Integer> hp = new HashMap<>();
	
		int n = arr.length;

		for(int i = 0; i < n; i++){
			
			if(hp.containsKey(arr[i])){
				
				int val = hp.get(arr[i]);

				hp.put(arr[i], val + 1);
			}else
				hp.put(arr[i], 1);
		}

		for(Integer i : hp.keySet()){
			
			if(hp.get(i) % 2 != 0){
				
				return i;
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,3,2,3,1,3};

		int ret = exceptionallyOdd(arr);

		System.out.println("Exceptionally Odd is " + ret);
	}
}
