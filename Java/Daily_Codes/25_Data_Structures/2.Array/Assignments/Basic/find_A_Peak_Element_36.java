import java.util.*;

class Sol{

	static ArrayList peakEle(int arr[]){

		ArrayList al = new ArrayList();

		for(int i = 1; i < arr.length - 1; i++){
			
			if(arr[i-1] < arr[i] && arr[i] > arr[i+1]){
				
				al.add(arr[i]);
			}
		}

		return al;
	}
	
	public static void main(String[] args){
		
		int arr[] = {10, 20, 15, 2, 23, 90, 67};

		ArrayList al  = peakEle(arr);

		for(int i = 0; i < al.size(); i++){
			
			System.out.print(al.get(i) + " ");
		}

		System.out.println();
	}
}

