
/*
 	Input : arr[] = {-3,4,-2,5,-3,-2,8,2,1,4}

		k = 4

	output : Count is 7
*/

class Sol{
	
	static int countSubK(int arr[], int k){	
		
		// Prefered Approach 
		int left = 0, right = k - 1;

		int N = arr.length, count = 0;

		while(right < N){
			
			count++;

			left++;

			right++;
		}

		return count;
		
		// Approach - 2
//		return arr.length - k + 1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {-3,4,-2,5,-3,-2,8,2,1,4};

		int k = 4;

		int ret = countSubK(arr, k);

		System.out.println("Count of Subarrays whose length k is " + ret);
	}
}

//	Count of Subarrays whose length k is 7
