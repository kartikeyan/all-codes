
/*
 	Input : arr[] = {-3,4,-2,5,-3,-2,8,2,1,4}

		k = 4

	output : 
		 [-3,4,-2,5] == > sum is 4

		 [4,-2,5,-3] == > sum is 4

		 [-2,5,-3,-2] == > sum is -2

		 [5,-3,-2,8] == > sum is 8

		 [-3,-2,8,2] == > sum is 5

		 [-2,8,2,1] == > sum is 9

		 [8,2,1,4] == > sum is 15


		 ==> Maximum Subarray sum is 15
*/

class Sol{
	
	//	Appraoch 1
/*	
	static int sumSubK(int arr[], int k){	
		
		int left = 0, right = k - 1;

		int N = arr.length;

		int maxSum = Integer.MIN_VALUE;

		while(right < N){			// T.C = O(N * K)
			
			int sum = 0;

			for(int i = left; i <= right; i++){
				
				sum = sum + arr[i];
			}

			if(sum > maxSum){
				
				maxSum = sum;
			}

			left++;

			right++;
		}

		return maxSum;
	}

	//	Approach - 2 - Prefix Sum
*/

/*	
	static int sumSubK(int arr[], int k){	

		int pSum[] = new int[arr.length];

		pSum[0] = arr[0];

		for(int i = 1; i < arr.length; i++){
			
			pSum[i] = pSum[i - 1] + arr[i];
		}
		
		int left = 0, right = k - 1;

		int N = arr.length;

		int maxSum = Integer.MIN_VALUE;

		while(right < N){			// T.C = O(N)
							// S.C = O(N)
			
			int sum = 0;

			if(left == 0){
				
				sum = pSum[right];
			}else
				sum = pSum[right] - pSum[left - 1];	//  pSum(start,end) = pSum(end) - pSum(start - 1);

			if(sum > maxSum){
				
				maxSum = sum;
			}

			left++;

			right++;
		}

		return maxSum;
	}
*/
	static int sumSubK(int arr[], int k){	
		
		int left = 0, right = k - 1, sum = 0;

		for(int i = left; i <= right; i++){
			
			sum += arr[i];
		}

		int N = arr.length;

		int maxSum = Integer.MIN_VALUE;

		left = 1;
		right = k;

		while(right < N){			// T.C = O(N)
							// S.C = O(1)
			
			sum = sum - arr[left - 1] + arr[right];

			if(sum > maxSum){
				
				maxSum = sum;
			}

			left++;

			right++;
		}

		return maxSum;
	}

	public static void main(String[] args){
		
		int arr[] = {-3,4,-2,5,-3,-2,8,2,1,4};

		int k = 4;

		int sum = sumSubK(arr, k);

		System.out.println("Maximum subarray sum of length " +  k + " is " + sum);
	}
}

/*
	Maximum subarray sum of length 4 is 15
*/
