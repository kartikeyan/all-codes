
/*
 	Input : arr[] = {-3,4,-2,5,-3,-2,8,2,1,4}

		k = 4

	output : 
		 [-3,4,-2,5] == > sum is 4

		 [4,-2,5,-3] == > sum is 4

		 [-2,5,-3,-2] == > sum is -2

		 [5,-3,-2,8] == > sum is 8

		 [-3,-2,8,2] == > sum is 5

		 [-2,8,2,1] == > sum is 9

		 [8,2,1,4] == > sum is 15
*/

class Sol{
	
	static void sumSubK(int arr[], int k){	
		
		int left = 0, right = k - 1;

		int N = arr.length;

		while(right < N){
			
			int sum = 0;

			for(int i = left; i <= right; i++){
				
				sum = sum + arr[i];
			}

			System.out.println(sum);

			left++;

			right++;
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {-3,4,-2,5,-3,-2,8,2,1,4};

		int k = 4;

		sumSubK(arr, k);
	}
}

/*
 	4
	4
	-2
	8
	5
	9
	15
*/
