class Recusive{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				return mid;
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				start = mid + 1;
			}
		}

		return -1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {2,5,9,10,12,15,19};

		int search = 15;

		int ret = binarySearch(arr, search);

		if(ret == -1){
			
			System.out.println("Element Not Found");
		}else
			System.out.println("Element Found At Index " + ret);
	}
}
