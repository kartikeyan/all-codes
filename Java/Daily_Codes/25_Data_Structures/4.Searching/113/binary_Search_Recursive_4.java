
class Recursive{

	static int binaryRecursive(int arr[], int start, int end, int search){
		
		if(start > end){
			
			return -1;
		}

		int mid = (start + end) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] > search){
			
			return binaryRecursive(arr, start, mid - 1, search);
		}

		return binaryRecursive(arr, mid + 1, end, search);
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6,7,8,9};

		int search = 9;

		int start = 0, end = arr.length - 1;

		int ret = binaryRecursive(arr, start , end, search);

		if(ret == -1){
			
			System.out.println("Element Not found");
		}else
			System.out.println("Element Found At Index " + ret);
	}
}
