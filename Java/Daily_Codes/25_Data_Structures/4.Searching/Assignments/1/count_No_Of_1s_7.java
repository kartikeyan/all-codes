
/*
	arr[] = {0,0,0,0,1,1}

	output : 2
*/

class Sol{

/*
 	Brute force:

	1] A brute force approach is to traverse through the array, and while traversing if 1 is encountered for the first time in the array that means all the elements to the right-hand side of this first occurrence of 1 will be 1’s only, as the array is sorted.

    	2] Traverse through array

    	3] Find the index where you are getting the first occurrence of 1 then return the answer as Length of the array – index of the first occurrence of 1 
*/
	static int linearSearch(int arr[]){
		
		int count = 0;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == 1){
				
				count = arr.length - i;

				break;
			}
		}

		return count;			// T.C = O(N)
	}

/*
 	1] Find the first occurrence index of 1 and all the elements on the right side of this index will be 1 only.

	2] Since the array is sorted we can find the first occurrence of 1 using the Binary Search algorithm

	3] If the element at the middle index is 0 then make start index = middle index +1 

    	4] Else if the element at middle index is 1 then update answer as the middle index ( because the middle index element may be the first occurrence of 1) and make the end index = middle index – 1 

    	5] Return ans
*/

	static int binarySearch(int arr[]){
		
		int start = 0, end = arr.length - 1, ans = 0;

		while(start < end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == 0){
				
				start = mid + 1;

			}else if(arr[mid] == 1){
				
				ans = arr.length - mid;

				end = mid - 1;
			}
		}

		return ans;
	}

	public static void main(String[] args){
		
		int arr[] = {0,0,0,1,1};

		int ret = linearSearch(arr);	

		if(ret == -1)
			System.out.println("Ele not found");
		else
			System.out.println("Total Number of 1s are  " + ret);
	}
}
