
/*
	arr[] = {1,3,20,4,1,0}

	output : 20

	Expla : 20 is greater than its neighbours 3 and 4
*/

class Sol{

	static int binarySearch(int arr[]){
		
		int start = 0, end = arr.length - 1;

		while(start < end){
			
			int mid = (start + end) / 2;

			if(arr[mid] < arr[mid+1]){
				
				start = mid + 1;
			}else{
				end = mid;
			}
		}

		return end;
	}

	public static void main(String[] args){
		
		int arr[] = {0,0,0,1,1};

		int ret = binarySearch(arr);	

		if(ret == -1)
			System.out.println("Ele not found");
		else
			System.out.println("Peak Element Is  " + ret);
	}
}
