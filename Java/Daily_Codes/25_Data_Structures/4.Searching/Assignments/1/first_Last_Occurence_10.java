
class Sol{
	
	static int[] binarySearch(int arr[], int key){
		
		int start = 0, end = arr.length - 1, mid = -1;
		int ans = -1;

		int ret[] = new int[2];

		while(start <= end){
			
			mid = start + (end - start) / 2;

			if(arr[mid] == key){
			
				ans = mid;
				end = mid - 1;
			}
			if(arr[mid] < key){
			
				start = mid + 1;
			}

			if(arr[mid] > key){
			
				end = mid - 1;
			}
		}

		ret[0] = ans;

		start = 0;
		end = arr.length - 1;
		mid = -1;
		ans = -1;

		while(start <= end){

			mid = start + (end - start) / 2;

			if(arr[mid] == key){

				ans = mid;
				start = mid + 1;
			}
			if(arr[mid] < key){

				start = mid + 1;
			}

			if(arr[mid] > key){

				end = mid - 1;
			}
		}

		ret[1] = ans;

		return ret;
	}

	public static void main(String[] args){
		
		int arr[] = {5,7,7,8,8,10};

		int k = 8;

		int ret[] = binarySearch(arr,k);

		for(int i = 0; i < ret.length; i++){
			
			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
