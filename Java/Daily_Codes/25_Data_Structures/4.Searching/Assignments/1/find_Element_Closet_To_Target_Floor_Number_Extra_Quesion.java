/*
	arr[] = {1,2,4,7,9}, k = 6

	output : 4

	Expla : 4 is the closet element to 6 in the array
*/

class Sol{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1, ans = -1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				ans = mid;

				start = mid + 1;
			}

			if(arr[mid] > k){
			
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				ans = arr[mid];	
				start = mid + 1;
			}
		}

		return ans;
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,4,7,9};

		int k = 6;

		int ret = binarySearch(arr, k);

		if(ret == -1)
			System.out.println("Ele not found");
		else
			System.out.println("Ele closet to search ele is " + ret);
	}
}
