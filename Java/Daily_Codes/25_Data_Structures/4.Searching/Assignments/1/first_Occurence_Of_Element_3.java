
/*
	arr[] = {2,4,4,4,6,7,8}, k = 4

	output : 1

	Expla : 4 appears at index 1 in the array
*/

class Sol{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1, ans = -1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				ans = mid;

				end = mid - 1;
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				start = mid + 1;
			}
		}

		return ans;
	}

	public static void main(String[] args){
		
		int arr[] = {2,4,4,4,6,7,8};

		int k = 4;

		int ret = binarySearch(arr, k);

		if(ret == -1)
			System.out.println("Ele not found");
		else
			System.out.println("Ele found at index  " + ret);
	}
}
