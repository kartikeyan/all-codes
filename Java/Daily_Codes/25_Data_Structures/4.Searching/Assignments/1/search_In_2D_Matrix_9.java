
class Sol{
/*
	static boolean binarySearch(int arr[][], int k){
		
		for(int i = 0; i < arr.length; i++){
			
			for(int j = 0; j < arr[i].length; j++){
				
				if(arr[i][j] == k){
					
					return true;
				}
			}
		}

		return false;
	}
*/
	static boolean binarySearch(int arr[][], int k){
		
		int rows = arr.length;

		int cols = arr[0].length;

		int start = 0, end = rows * cols - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			int x = mid / cols;

			int y = mid % cols;

			int guess = arr[x][y];

			if(guess == k){
				
				return true;
			}

			else if(guess < k){
				
				start = mid + 1;
			}

			else{
				
				end = mid - 1;
			}
		}

		return false;
	}

	public static void main(String[] args){
		
		int arr[][] = {{1,4,7,11},{2,5,8,12},{3,6,9,16},{10,13,14,17}};
		int k = 5;

		boolean ret = binarySearch(arr, k);

		if(ret == true)
			System.out.println("Ele Found");
		else
			System.out.println("Not found");
	}
}
