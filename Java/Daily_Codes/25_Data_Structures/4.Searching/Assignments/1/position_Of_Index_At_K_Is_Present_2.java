
/*
 	Given a sorted array of size N and an integer N, find
	the positive(0-based indexing) at which K is present
	in the array using binary Search

	arr[] = {1,2,3,4,5}, k = 4

	output : 3

	Expla : 4 appears at index 3
*/

class Sol{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				return mid;
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				start = mid + 1;
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5};

		int k = 4;

		int ret = binarySearch(arr, k);

		if(ret == -1)
			System.out.println("Ele not found");
		else
			System.out.println("Ele found at index  " + ret);
	}
}
