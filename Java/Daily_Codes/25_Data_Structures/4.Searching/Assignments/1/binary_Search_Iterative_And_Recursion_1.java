

class Sol{

/*
	static int binarySearch(int arr[], int search){
		
		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == search){
				
				return mid;
			}

			if(arr[mid] > search){
				
				end = mid - 1;
			}

			if(arr[mid] < search){
				
				start = mid + 1;
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		int arr[] = {1,3,6,87,91};

		int k = 6;

		int ret = binarySearch(arr, k);

		if(ret == -1)
			System.out.println("Element Not found");
		else
			System.out.println("Element found at Index " + ret);
	}
	*/
	
	static int binarySearch(int arr[], int start, int end, int search){
		if(start > end){
			
			return -1;
		}

		int mid = (start + end) / 2;

		if(arr[mid] == search){
			
			return mid;
		}

		if(arr[mid] > search){
			
			return binarySearch(arr, start, mid - 1, search);
		}

		return binarySearch(arr, mid+1, end, search);
	}

	public static void main(String[] args){
		
		int arr[] = {1,3,6,87,91};

		int k = 6;

		int start = 0, end = arr.length - 1;

		int ret = binarySearch(arr, start, end, k);

		if(ret == -1)
			System.out.println("Element Not found");
		else
			System.out.println("Element found at Index " + ret);
	}
}
