
/*
	arr[] = {1,3,5,6}, k = 2

	output : 1

	Expla : As inserting 2 at index 1 would maintain the sorted order.
*/

class Sol{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				return mid;
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				start = mid + 1;
			}
		}

		return end+1;
	}

	public static void main(String[] args){
		
		int arr[] = {1,3,5,6};

		int k = 2;

		int ret = binarySearch(arr, k);

		System.out.println("Ele Inserted at Index  " + ret);
	}
}
