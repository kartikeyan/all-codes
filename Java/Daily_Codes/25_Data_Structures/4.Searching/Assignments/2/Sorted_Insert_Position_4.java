
/*
	35. Search Insert Position

	Given a sorted array of distinct integers and a target value, return the index if the target is found. If not, return the index where it would be if it were inserted in order.

	You must write an algorithm with O(log n) runtime complexity.

	Example 1:

	Input: nums = [1,3,5,6], target = 5
	Output: 2

	Example 2:

	Input: nums = [1,3,5,6], target = 2
	Output: 1

	Example 3:

	Input: nums = [1,3,5,6], target = 7
	Output: 4

	Constraints:

	    1 <= nums.length <= 104
	    -104 <= nums[i] <= 104
*/

class Sol{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				return mid;
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				start = mid + 1;
			}
		}	

		return end + 1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,3,5,6};

		int k = 2;

		int ret = binarySearch(arr, k);

		System.out.println(ret);
	}
}
