
/*
 	34. Find First and Last Position of Element in Sorted Array

	Given an array of integers nums sorted in non-decreasing order, find the starting and ending position of a given target value.

	If target is not found in the array, return [-1, -1].

	Example 1:

	Input: nums = [5,7,7,8,8,10], target = 8
	Output: [3,4]

	Example 2:

	Input: nums = [5,7,7,8,8,10], target = 6
	Output: [-1,-1]

	Example 3:

	Input: nums = [], target = 0
	Output: [-1,-1]

	Constraints:

	    0 <= nums.length <= 105
	    -109 <= nums[i] <= 109
	    nums is a non-decreasing array.
	    -109 <= target <= 109
*/


class Sol{

	static int[] binarySearch(int arr[], int key){

		int start = 0, end = arr.length - 1, mid = -1;
		int ans = -1;

		int ret[] = new int[2];

		while(start <= end){

			mid = start + (end - start) / 2;

			if(arr[mid] == key){

				ans = mid;
				end = mid - 1;
			}
			if(arr[mid] < key){

				start = mid + 1;
			}

			if(arr[mid] > key){

				end = mid - 1;
			}
		}

		ret[0] = ans;

		start = 0;
		end = arr.length - 1;
		mid = -1;
		ans = -1;

		while(start <= end){

			mid = start + (end - start) / 2;

			if(arr[mid] == key){

				ans = mid;
				start = mid + 1;
			}
			if(arr[mid] < key){

				start = mid + 1;
			}

			if(arr[mid] > key){

				end = mid - 1;
			}
		}

		ret[1] = ans;

		return ret;
	}

	public static void main(String[] args){

		int arr[] = {5,7,7,8,8,10};

		int k = 8;

		int ret[] = binarySearch(arr,k);

		for(int i = 0; i < ret.length; i++){

			System.out.print(ret[i] + " ");
		}

		System.out.println();
	}
}
