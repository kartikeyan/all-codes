
/*
 	Find The closet number.

	Given an array of sorted integers. We need to find the closest value to the given number. Array may contain duplicate values and negative numbers.

	arr[] = {1,3,6,7}, k = 4
	o.p : 3
	Expla : We have an array [1,3,6,7] and target is 4.
		If we look at the absolute difference of target
		with every element of an array we will get
		[|1-4||3-4||6-4||7-4|]	==> [3,1,2,3]
		So the closet number is 3.

	arr[] = {1,2,3,5,6,8,9}, k = 4
	o.p : 5
*/

class Sol{

	static int closetNumber(int arr[], int target){
		/*
		int min = Integer.MAX_VALUE;

		for(int i = 0; i < arr.length; i++){	// T.C = O(N)
			
			if(Math.abs(arr[i] - target) < target){
				
				min = Math.abs(arr[i] - target);
			}
		}

		return min;

		*/

		
		int n = arr.length;

		if(target <= arr[0]){
			
			return arr[0];
		}

		if(target >= arr[n-1]){
			
			return arr[n-1];
		}

		int start = 0, end = arr.length, mid = 0;

		while(start < end){
			
			mid = (start + end) / 2;

			if(arr[mid] == target){
				
				return arr[mid];
			}

			if(target < arr[mid]){
				
				if(target > 0 && target > arr[mid-1]){
					
					return getClosetNumber(arr[mid-1], arr[mid], target);
				}

				end = mid;
			}else{
				
				if(mid < n - 1 && target < arr[mid + 1]){
					
					return getClosetNumber(arr[mid], arr[mid+1], target);
				}

				start = mid + 1;
			}
		}

		return arr[mid];
	}

	static int getClosetNumber(int x, int y, int target){
		
		if(target - x >= y - target){
			
			return y;
		}else
			return x;
	}


	public static void main(String[] args){
		
		int arr[] = {1,3,6,7}, k = 4;

		int ret = closetNumber(arr, k);

		System.out.println("Closet Number is " + ret);
	}
}
