
/*
 	Given a sorted array with DISTICT elements

	Find the floor of no. K in the array.

	floor ==> greatest element <= K

	arr : [2.3,6,9,10,11,14,18]

	output :

		K = 5, floor = 3
		K = 4, floor = 3
		K = 6, floor = 6
*/

class Sol{

	static int binarySearch(int arr[], int k){
		
		int start = 0, end = arr.length - 1, ans = -1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == k){
				
				return arr[mid];
			}

			if(arr[mid] > k){
				
				end = mid - 1;
			}

			if(arr[mid] < k){
				
				ans = arr[mid];

				start = mid + 1;
			}
		}	

		return ans;
	}
	
	public static void main(String[] args){
		
		int arr[] = {2,3,6,9,10,11,14,18};

		int k = 6;

		int ret = binarySearch(arr, k);

		System.out.println(ret);
	}
}
