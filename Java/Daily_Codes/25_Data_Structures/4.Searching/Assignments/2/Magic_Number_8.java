
/*
 	Find Magic Number

	1] There are N number of people in the magic show, seated
       	according to their ages in ascending order.

	2] A magical number is that seat number where the person has the same age as that of the given seat number.

	Note: If there are multiple numbers in the array, return any one valid value.

	arr[] : {-10,-1,0,3,10,11,30,50,100,150}
	o.p : 3
*/

class Sol{

	static int magicNumber(int arr[]){
		
		int start = 0, end = arr.length - 1;

		while(start <= end){
			
			int mid = (start + end) / 2;

			if(arr[mid] == mid){
				
				return mid;
			}

			if(arr[mid] < mid){
				
				start = mid + 1;
			}else
				end = mid - 1;
		}

		return -1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {-10,-1,0,3,10,11,30,50,100,150};

		int ret = magicNumber(arr);

		System.out.println("Magic Number is " + ret);
	}
}
