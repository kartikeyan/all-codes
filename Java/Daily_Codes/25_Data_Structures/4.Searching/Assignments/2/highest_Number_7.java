
/*
 	Find the Highest Number

	Given an array in such a way that the elements stored in array are in increasing
	order initially and then after reaching to a peak element, the element stored 
	are in decreasing order.Find the Highest element.

	Note : arr[i] != arr[i+1]

	arr[] : {1,2,3,4,5,6,5,4,3,2,1}
	o.p : 6
	Expla : Highest Number is 6
	
	arr[] : {1,2,3,4,5}
	o.p : 5
*/

class Sol{

	static int highestNumber(int arr[], int start, int end){

		if(start == end){
			
			return arr[end];
		}

		int mid = (start + end) / 2;

		if(arr[mid] > arr[mid-1] && arr[mid] >= arr[mid+1]){
				
			return arr[mid];
		}

		if(arr[mid] < arr[mid+1]){
				
			return highestNumber(arr, mid+1, end);
		}else
			return highestNumber(arr, start, mid-1);
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5};

		int ret = highestNumber(arr, 0, arr.length-1);

		System.out.println("Highest Number " + ret);
	}
}
