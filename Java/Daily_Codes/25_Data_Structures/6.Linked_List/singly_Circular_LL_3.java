import java.util.*;

class Node{
	
	int data;
	Node next = null;

	Node(int data){
		
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;

	void addFirst(int data){
		
		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
			newNode.next = head;
		}else{
			Node temp = head;

			while(temp.next != head){
				
				temp = temp.next;
			}
	
			temp.next = newNode;			
			newNode.next = head;
			head = newNode;
		}
	}

	void addLast(int data){

		Node newNode = new Node(data);
		
		if(head == null){
			
			head = newNode;
			newNode.next = head;
		}else{

			Node temp = head;
			
			while(temp.next != head){
				
				temp = temp.next;
			}

			temp.next = newNode;
			newNode.next = head;
		}
	}

	void addAtPos(int data, int pos){
		
		if(pos <= 0 || pos >= countNodes() + 2){
			
			System.out.println("Invalid Index");
			return;
		}else{
			
			if(pos == 1){
				
				addFirst(data);
			}else if(pos == countNodes() + 1){
				
				addLast(data);
			}else{
				
				Node newNode = new Node(data);

				Node temp = head;

				while(pos - 2 != 0){
					
					temp = temp.next;

					pos--;
				}

				newNode.next = temp.next;
				temp.next = newNode;
			}
		}
	}

	int countNodes(){
		
		Node temp = head;

		int c = 0;

		while(temp != null){
			
			c++;
			temp = temp.next;
		}

		return c;
	}

	void deleteFirst(){
		
		if(head == null){
			
			System.out.println("Nothing to delete");

		}else if(head.next == head){
				
			head = null;
		}else{

			Node temp = head;
			
			// traverse till last node in SLL
			while(temp.next != head){

				head = head.next;
			}
			
			// assign last nodes next to 2nd node in SLL
			temp.next = head.next;

			//move head to next
			head = head.next;
		}
	}

	void deleteLast(){
		
		if(head == null){
			
			System.out.println("Nothing to delete");

		}else if(head.next == head){
				
			head = null;
		}else{
			
			Node temp = head;

/*			Node prev = null;
			

			// traverse to last node
			while(temp.next != head){
				
				// store previous link node as we need to change its next val
				prev = temp;	
				temp = temp.next;
			}
			
			prev.next = head;
*/
			while(temp.next.next != null){
				
				temp = temp.next;
			}

			temp.next = head;
		}
	}

	void deleteAtPos(int pos){
		
		if(pos <= 0 || pos >= countNodes() + 1){
			
			System.out.println("Invalid Index");
			return;
		}else{
			
			if(pos == 1){
				
				deleteFirst();
			}else if(pos == countNodes()){
				
				deleteLast();
			}else{

				Node temp = head;

				Node prev = null;

				while(pos - 2 != 0){
					
					prev = temp;

					temp = temp.next;

					pos--;
				}
				
				// change previous nodes next node to nth nodes next node
				prev.next = temp.next;
			}
		}
	}

	void printLL(){
		
		if(head == null){
			
			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp.next != head){
				
				System.out.println(temp.data + " ");
				temp = temp.next;
			}

			System.out.println(temp.data);
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		LinkedList ll = new LinkedList();

		char choice;

		do{
		
			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.countNodes");
			System.out.println("5.deleteFirst");
			System.out.println("6.deleteLast");
			System.out.println("7.deleteAtPos");
			System.out.println("8.printLL");

			System.out.println("Enter Choice");
			
			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();		
	
			switch(ch){
		
				case 1:	
					{
						System.out.println("Enter data");
						int data = sc.nextInt();
						ll.addFirst(data);							      }
					break;			
				case 2:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();
						
						ll.addLast(data);							      }
					break;
				case 3:	
					{
					int pos,data;
					System.out.println("Enter Node Position");
					pos = sc.nextInt();

					System.out.println("Enter Node Data");

					data = sc.nextInt();
	
					ll.addAtPos(data,pos);	
					}
					break;
				case 4:
					{
					int count = ll.countNodes();
					System.out.println("Count of Node is " + count);
					}
					break;
				case 5:
					ll.deleteFirst();
					break;
				case 6:
					ll.deleteLast();
					break;
				case 7:
					{
					int pos;
					System.out.println("Enter pos to delete");
					pos = sc.nextInt();

					ll.deleteAtPos(pos);
	
					}
					break;
				case 8:	
					ll.printLL();			
					break;	
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
