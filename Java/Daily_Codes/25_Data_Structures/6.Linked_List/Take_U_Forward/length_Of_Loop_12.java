
/*
 	

    Floyd’s Cycle detection algorithm terminates when fast and slow pointers meet at a common point. It is also known that this common point is one of the loop nodes. Store the address of this common point in a pointer variable ptr. Then initialize a counter with 1 and start from the common point and keeps on visiting the next node and increasing the counter till the common pointer is reached again. At that point, the value of the counter will be equal to the length of the loop.

*/

import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	// 1 - Naive Approach
/*
	int lengthOfLoop(Node head){
		
		Map<Node,Integer> mp = new HashMap<>();

		Node temp = head;

		int timer = 1;				// T.C = O(N)
							// S.C = O(N)

		while(temp != null){
			
			if(mp.containsKey(temp)){
				
				int loopLen = timer - mp.get(temp);

				return loopLen;
			}

			mp.put(temp, timer);

			temp = temp.next;

			timer++;
		}

		return 0;	// there is no loop
	}
*/
	// 2 - Optimal

	int countNodes(Node slow, Node fast){

		fast = fast.next;

		int c = 1;

		while(slow != fast){

			c++;
			fast = fast.next;
		}

		return c;
	}

	int lengthOfLoop(Node head){
		
		Node slow = head, fast = head;			// T.C = O(N)
								// S.C = O(1)

		while(fast != null && fast.next != null){
			
			slow = slow.next;

			fast = fast.next.next;

			if(slow == fast){
				
				System.out.println("Loop");				
				return countNodes(slow,fast);
			}
		}

		return 0;
	}

	void printLL(Node head){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		Node head = new Node(1);

		Node second = new Node(2);

		Node third = new Node(3);

		Node fourth = new Node(4);

		Node fifth = new Node(5);

		head.next = second;
		second.next = third;
		third.next = fourth;
		fourth.next = fifth;

		fifth.next = second;

		int ret = ll.lengthOfLoop(head);

		if(ret == 0){
			
			System.out.println("There is no loop");
		}else{
			System.out.println("Length of loop is " + ret);
		}
	}
}
