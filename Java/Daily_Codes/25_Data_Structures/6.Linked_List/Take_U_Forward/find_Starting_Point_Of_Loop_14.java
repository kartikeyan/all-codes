
/*
 	

    Floyd’s Cycle detection algorithm terminates when fast and slow pointers meet at a common point. It is also known that this common point is one of the loop nodes. Store the address of this common point in a pointer variable ptr. Then initialize a counter with 1 and start from the common point and keeps on visiting the next node and increasing the counter till the common pointer is reached again. At that point, the value of the counter will be equal to the length of the loop.

*/

import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	// 1 - Naive Approach
/*
	Node startingOfLoop(Node head){
		
		Map<Node,Integer> mp = new HashMap<>();

		Node temp = head;

							// T.C = O(N)
							// S.C = O(N)
		while(temp != null){
			
			if(mp.containsKey(temp)){

				return temp;
			}

			mp.put(temp, 1);

			temp = temp.next;
		}

		return null;	// there is no loop
	}
*/
	// 2 - Optimal

	Node startingOfLoop(Node head){
		
		Node slow = head, fast = head;			// T.C = O(N)
								// S.C = O(1)

		while(fast != null && fast.next != null){
			
			slow = slow.next;

			fast = fast.next.next;

			if(slow == fast){
				
				slow = head;

				while(slow != fast){

					slow = slow.next;
					fast = fast.next;
				}

				return slow;
			}
		}

		return null;
	}

	void printLL(Node head){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		Node head = new Node(1);

		Node second = new Node(2);

		Node third = new Node(3);

		Node fourth = new Node(4);

		Node fifth = new Node(5);

		head.next = second;
		second.next = third;
		third.next = fourth;
		fourth.next = fifth;

		fifth.next = second;

		Node ret = ll.startingOfLoop(head);

		if(ret == null){
			
			System.out.println("There is no loop");
		}else{
			System.out.println("Loops starting point is " + ret.data);
		}
	}
}
