import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){
			
			head = newNode;
		}else{
			
			Node temp = head;

			while(temp.next != null){
				
				temp = temp.next;
			}

			temp.next = newNode;
		}	
	}
/*
	Node remove(int n){
	
		if(head == null){
			System.out.println("LL is empty");
			return null;
		}
		
		int count = 0;			// T.C = O(len(N))
		Node temp = head;
		
		while(temp != null){
			
			count++;

			temp = temp.next;
		}

		if(count == n){
			
			Node newNode = head.next;

			return newNode;
		}

		int res = count - n;

		temp = head;

		while(temp != null){
			
			res--;

			if(res == 0){
				
				break;
			}

			temp = temp.next;
		}

		Node delNode = temp.next;
		temp.next = temp.next.next;

		return head;
	}
*/
	Node remove(int k){
		
		Node fast = head, slow = head;

		for(int i = 0; i < k; i++){
			
			fast = fast.next;
		}

		if(fast == null){
			
			return head.next;		// T.C = O(len(N))
		}

		while(fast.next != null){
			
			fast = fast.next;
			slow = slow.next;
		}

		Node delNode = slow.next;
		slow.next = slow.next.next;

		return head;
	}

	void printLL(Node head){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(10);
		ll.addLast(20);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(50);

		ll.printLL(ll.head);

		Node ret = ll.remove(4);

		ll.printLL(ret);
	}
}
