
import java.util.*;

class Node{
	
	int data;
	Node next = null;
	Node prev = null;

	Node(int data){
		
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);
		
		if(head == null){
			
			head = newNode;
		}else{

			Node temp = head;
			
			while(temp.next != null){
				
				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	// 1 - Naive Approach

	/*
	void pairs(Node head, int k){
		
		Node temp1 = head;			// T.C = O(N*N)
							// S.C = O(1)

		while(temp1 != null){
			
			Node temp2 = temp1.next;

			while(temp2 != null && temp1.data + temp2.data <= k){
				
				if(temp1.data + temp2.data == k){
					
					System.out.println(temp1.data + " : " + temp2.data);
				}

				temp2 = temp2.next;
			}

			temp1 = temp1.next;
		}
	}
	*/

	// 2 - Optimal
	
	void pairs(Node head, int k){
		
		Node left = head, temp = head;

		while(temp.next != null){
			
			temp = temp.next;
		}

		Node right = temp;			// T.C = O(2N)
							// S.C = O(1)

		while(left.data < right.data){
			
			if(left.data + right.data == k){
				
				System.out.println(left.data + " : " + right.data);
				left = left.next;
				right = right.prev;
			}else if(left.data + right.data < k){
				
				left = left.next;
			}else{
				
				right = right.prev;
			}
		}
	}
	void printLL(Node head){
		
		if(head == null){
			
			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){
				
				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		LinkedList ll = new LinkedList();

		ll.addLast(1);

		ll.addLast(2);

		ll.addLast(3);

		ll.addLast(4);

		ll.addLast(5);

		int k = 4;
		
		ll.printLL(ll.head);

		ll.pairs(ll.head, k);
	}
}
