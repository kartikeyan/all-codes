
//	The task is to left shift LL by k nodes, where k is positive no. smaller than or equal to length of LL

import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	void rotateLeft(int k){
	
		if(k == 0){
			System.out.println("No need to modify");
			return;
		}

		Node current = head;

		int count = 1;

		// curr will point to kth or null after this loop
		while(count < k && current != null){
			
			current = current.next;
			count++;
		}

		if(current == null){
			System.out.println("K is greater than or equal to countNodes");
			return;
		}
		
		// kThNode points to 40
		Node kThNode = current;

		// curr will point to last node after this loop
		while(current.next != null)
			current = current.next;

		// next of 60 is now changes to 10
		current.next = head;

		// head now points to 50
		head = kThNode.next;

		// change next of kth node will null
		kThNode.next = null;
	}

	Node findKthNode(Node temp, int k){
		
		int c = 1;

		while(temp != null){
			
			if(c == k){
				
				return temp;
			}

			c++;

			temp = temp.next;
		}

		return temp;
	}

	Node rotateRight(int k){

		if(head == null || k == 0){
			
			return head;				// T.C = O(2N)
								// S.C = O(1)
		}
		
		Node tail = head;

		int len = 1;

		while(tail.next != null){
			
			tail = tail.next;
			len++;
		}
		
		if(k % len == 0){
			
			return head;
		}else{
			k = k % len;
		}

		tail.next = head;

		Node newLastNode = findKthNode(head, len - k);

		head = newLastNode.next;

		newLastNode.next = null;

		return head;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(10);
		ll.addLast(20);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(50);

		ll.printLL();

	//	ll.rotateLeft(2);
	
		ll.rotateRight(2);

		ll.printLL();
	}
}

/*
 	Left - shift

   	10 20 30 40 50   
	30 40 50 10 20  for k = 2
*/

/*
	Right - shift
 	 
   	10 20 30 40 50 
	40 50 10 20 30  for k = 2
*/

