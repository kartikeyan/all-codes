
import java.util.*;

class Node{
	
	int data;
	Node next = null;
	Node prev = null;

	Node(int data){
		
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);
		
		if(head == null){
			
			head = newNode;
		}else{

			Node temp = head;
			
			while(temp.next != null){
				
				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	Node remove(Node head, int k){
		
		Node temp = head;			// T.C = O(N)
							// S.C = O(1)

		while(temp != null){
			
			if(temp.data == k){
				
				if(temp == head){
					
					head = head.next;
				}

				Node nextNode = temp.next;
				Node prevNode = temp.prev;

				if(nextNode != null){
					
					nextNode.prev = prevNode;
				}

				if(prevNode != null){
					
					prevNode.next = nextNode;
				}

				temp = nextNode;
			}else
				temp = temp.next;
		}

		return head;
	}

	void printLL(Node head){
		
		if(head == null){
			
			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){
				
				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		LinkedList ll = new LinkedList();

		ll.addLast(1);

		ll.addLast(2);

		ll.addLast(1);

		ll.addLast(4);

		ll.addLast(1);

		int k = 1;
		
		System.out.println("Before Removal");
		ll.printLL(ll.head);

		Node ret = ll.remove(ll.head, k);

		System.out.println("After Removal");
		ll.printLL(ret);
	}
}
