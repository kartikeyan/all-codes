

import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	Node intersection(Node head1, Node head2){
		
		Node ptr1 = head1, ptr2 = head2;

		if(ptr1 == null || ptr2 == null){
			System.out.println("In NULL");
			return null;
		}

		while(ptr1 != ptr2){

			if(ptr1 == null){
			
				ptr1 = head2;
			}else{
				
				ptr1 = ptr1.next;
			}

			if(ptr2 == null){
				
				ptr2 = head1;
			}else{
				ptr2 = ptr2.next;
			}
		}

		return ptr1;
	}

	void printLL(Node head){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll1 = new LinkedList();

		ll1.addLast(3);
		ll1.addLast(6);
		ll1.addLast(9);
		ll1.addLast(15);
		ll1.addLast(30);

		LinkedList ll2 = new LinkedList();

		ll2.addLast(10);
		ll2.addLast(15);
		ll2.addLast(30);

		ll1.printLL(ll1.head);
		ll1.printLL(ll2.head);


		Node inter = ll1.intersection(ll1.head,ll2.head);

		if(inter == null)
			System.out.println("NO Intersection");
		else
			System.out.println("Intersection Point " + inter.data);
	}
}
