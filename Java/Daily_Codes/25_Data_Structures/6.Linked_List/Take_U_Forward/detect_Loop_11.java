

import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	// 1 - Naive Approach
	boolean detect(Node head){
		
		HashSet<Node> ht = new HashSet<>();
	
		while(head != null){				// T.C = O(N)
								// S.C = O(N)
			
			if(ht.contains(head)){

				return true;
			}

			ht.add(head);

			head = head.next;
		}

		return false;
	}
/*
	boolean detect(Node head){
		
		Node slow = head, fast = head;			// T.C = O(N)
								// S.C = O(1)

		while(fast != null && fast.next != null){
			
			slow = slow.next;
			fast = fast.next.next;

			if(slow == fast){
				
				return true;
			}
		}

		return false;
	}
*/
	void printLL(Node head){

		if(head == null){ 

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		Node head = new Node(1);

		Node second = new Node(2);

		Node third = new Node(3);

		Node fourth = new Node(4);

		Node fifth = new Node(5);

		head.next = second;
		second.next = third;
		third.next = fourth;
		fourth.next = fifth;

		fifth.next = second;

		boolean ret = ll.detect(head);

		if(ret == false){
			
			System.out.println("There is no loop");
		}else{
			System.out.println("There is loop");
		}
	}
}
