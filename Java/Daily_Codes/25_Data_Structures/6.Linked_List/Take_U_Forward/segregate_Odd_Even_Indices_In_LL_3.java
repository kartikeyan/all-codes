
import java.util.*;

class Node{
	
	int data;
	Node next = null;
	Node prev = null;

	Node(int data){
		
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);
		
		if(head == null){
			
			head = newNode;
		}else{

			Node temp = head;
			
			while(temp.next != null){
				
				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	// 1 - Naive Approach
/*	
	void segregate(){

		if(head == null || head.next == null){
			
			System.out.println("LL is empty");
			return;
		}
		
		List<Integer> al = new ArrayList<>();

		Node temp = head;

		while(temp != null && temp.next != null){
			
			al.add(temp.data);

			temp = temp.next.next;
		}

		if(temp != null){	// if odd length LL is there, this condition ensures that last node gets included
			
			al.add(temp.data);
		}

		temp = head.next;
		
		while(temp != null && temp.next != null){
			
			al.add(temp.data);

			temp = temp.next.next;
		}
		
		if(temp != null){       // if even length LL is there, this condition ensures that last node gets included

                        al.add(temp.data);
                }


		int i = 0;			// T.C = O(2N)
						// S.C = O(N)

		temp = head;

		while(temp != null){
			
			temp.data = al.get(i);
			
			i++;

			temp = temp.next;
		}
	}
*/
	void segregate(){

		if(head == null || head.next == null){
			
			System.out.println("LL is Empty");
			return;
		}
						// T.C = O(N)
						// S.C = O(1)
		Node odd = head;	

		Node even = head.next;

		Node evenHead = head.next;		

		while(even != null && even.next != null){
			
			odd.next = odd.next.next;

			even.next = even.next.next;

			odd = odd.next;
			even = even.next;
		}

		odd.next = evenHead;
	}
		
	void printLL(){
		
		if(head == null){
			
			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){
				
				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		LinkedList ll = new LinkedList();

		ll.addLast(1);

		ll.addLast(2);

		ll.addLast(3);

		ll.addLast(4);

		ll.addLast(5);

		ll.addLast(6);

		System.out.println("Before Segregation");
		ll.printLL();

		ll.segregate();

		System.out.println("After Segregation");
		ll.printLL();
	}
}
