import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	Node reverse(Node head){
		
		Node curr = head, prev = null, next = null;

		while(curr != null){
			
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}

		return prev;
	}
	
	// 1 - Naive
/*	
	Node add1(){
		
		head = reverse(head);

		Node temp = head;

		int carry = 1;			// T.C = O(3N)
						// S.C = O(1)

		while(temp != null){	
			
			temp.data = temp.data + 1;

			if(temp.data < 10){
				
				carry = 0;
				break;
			}else{
				
				temp.data = 0;

				carry = 1;
			}

			temp = temp.next;
		}

		if(carry == 1){
			
			Node newNode = new Node(1);

			head = reverse(head);

			newNode.next = head;

			return newNode;
		}

		head = reverse(head);

		return head;
	}
*/		
	int addHelper(Node temp){
		
		if(temp == null){
			
			return 1;
		}

		int carry = addHelper(temp.next);

		temp.data = temp.data + carry;

		if(temp.data < 10){
			
			return 0;
		}

		temp.data = 0;

		return 1;
	}

	// 2 - Without reversing ===========> recursion is only way
	Node add1(Node head){
		
		int carry = addHelper(head);

		if(carry == 1){				// T.C = O(N)
							// S.C = O(N)
			
			Node newNode = new Node(1);

			newNode.next = head;

			return newNode;
		}	

		return head;
	}
	
	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(1);

		ll.addLast(2);

		ll.addLast(3);

		ll.printLL();
		
		// 1
//		Node ret = ll.add1();
		
		// 2
		Node ret = ll.add1(ll.head);

		System.out.println("After Adding 1");
		ll.printLL();
	}
}
