import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	// 1 - Naive Approach
/*
	void sort(){
		
		int z = 0, o = 0, t = 0;

		Node temp = head;		// T.C = O(2N)
						// S.C = O(1)

		while(temp != null){
			
			if(temp.data == 0)
				z++;
			else if(temp.data == 1)
				o++;
			else
				t++;

			temp = temp.next;
		}

		temp = head;

		while(temp != null){
			
			if(z != 0){
				
				temp.data = 0;

				z--;
			}else if(o != 0){
				
				temp.data = 1;

				o--;
			}else{
				
				temp.data = 2;

				t--;
			}

			temp = temp.next;
		}
	}
*/
	// 2 - Optimal Solution
	Node sort(){
		
		if(head == null || head.next == null){

			return head;
		}
	
		Node zeroHead = new Node(-1);			// T.C = O(N)
								// S.C = O(1)

		Node oneHead = new Node(-1);

		Node twoHead = new Node(-1);

		Node zero = zeroHead, one = oneHead, two = twoHead;

		Node temp = head;

		while(temp != null){
			
			if(temp.data ==  0){
				
				zero.next = temp;

				zero = zero.next;
			}else if(temp.data == 1){
				
				one.next = temp;

				one = one.next;;
			}else{
				
				two.next = temp;

				two = two.next;
			}

			temp = temp.next;
		}
		
		
		zero.next = (oneHead.next != null) ? oneHead.next : twoHead.next;

		one.next = twoHead.next;
		
		two.next = null;

		Node newHead = zeroHead.next;

		return newHead;
	}

	void printLL(Node newNode){

		if(newNode == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = newNode;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char choice;

		do{
			System.out.println("1.addLast");
			System.out.println("2.Sort");
			System.out.println("3.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      		}	
					break;
				case 2:
					{
						Node newNode = ll.sort();

						ll.printLL(newNode);
					}
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
