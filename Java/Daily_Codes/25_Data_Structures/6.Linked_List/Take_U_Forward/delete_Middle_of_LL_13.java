import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;


	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){
			
			head = newNode;
		}else{
			
			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	int countNodes(){
		
		Node temp = head;

		int c = 0;

		while(temp != null){
			
			c++;
			temp = temp.next;
		}

		return c;
	}

	int middleLLNaiveApproach(){

		if(head == null){
			
			return -1;
		}
		
		int nodeCount = countNodes();

		Node temp = head;

		int c = 0;

		while(c < (nodeCount / 2)){
			
			temp = temp.next;

			c++;
		}

		return temp.data;
	}

	int middleLLTwoPointers(){
		
		if(head == null){
			
			return -1;
		}

		Node slow = head;
		Node fast = head.next;

		while(fast != null){
			
			fast = fast.next;

			if(fast != null){
				
				fast = fast.next;
			}

			slow = slow.next;
		}

		return slow.data;
	}

	void deleteMiddle(){
		
		if(head == null || head.next == null){
			
			head = null;
			System.out.println("LL is empty");
		}

		Node slow = head;
		Node prev = null;
		Node fast = head;

		while(fast != null && fast.next != null){
			
			fast = fast.next.next;
			prev = slow;
			slow = slow.next;
		}

		prev.next = slow.next;
		slow.next = null;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char choice;

		do{
			System.out.println("1.addLast");
			System.out.println("2.MiddleLL Naive Approach");
			System.out.println("3.MiddleLL Two Pointers");
			System.out.println("4.Delete Middle");
			System.out.println("5.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      
					}
					break;
				case 2:
					{
						int ret = ll.middleLLNaiveApproach();
						if(ret == -1)
							System.out.println("LL is empty");
						else
							System.out.println("Middle data is " + ret);
					}
					break;
				case 3:
					{
						int ret = ll.middleLLTwoPointers();
						if(ret == -1)
							System.out.println("LL is empty");
						else
							System.out.println("Middle data is " + ret);
					}
					break;
				case 4:
					{
						ll.deleteMiddle();
					}
					break;
				case 5:
					ll.printLL();
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
