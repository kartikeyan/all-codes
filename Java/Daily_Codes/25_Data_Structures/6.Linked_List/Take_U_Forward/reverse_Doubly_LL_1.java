
import java.util.*;

class Node{
	
	int data;
	Node next = null;
	Node prev = null;

	Node(int data){
		
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);
		
		if(head == null){
			
			head = newNode;
		}else{

			Node temp = head;
			
			while(temp.next != null){
				
				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}
	
	// 1 - Naive Approach
/*	
	void reverse(){
		
		Stack<Integer> s = new Stack<>();	// T.C = O(N)
							// S.C = O(N)

		Node temp = head;

		while(temp != null){		// Step - 1
			
			s.push(temp.data);

			temp = temp.next;
		}

		// Step - 2
		temp = head;

		while(temp != null){
			
			temp.data = s.peek();

			s.pop();

			temp = temp.next;		
		}
	}
*/
	// 2 - Optimal
	void reverse(){
		
		if(head == null || head.next == null){
			
			System.out.println("LL is empty");
			return;
		}

		Node prev = null;
		Node curr = head;		// T.C = O(N)
						// S.C = O(1)

		while(curr != null){
			
			prev = curr.prev;

			curr.prev = curr.next;

			curr.next = prev;

			curr = curr.prev;
		}

		head = prev.prev;

	}
	
	void printLL(){
		
		if(head == null){
			
			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){
				
				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{
	
	public static void main(String[] args){
		
		LinkedList ll = new LinkedList();

		ll.addLast(1);

		ll.addLast(2);

		ll.addLast(3);

		ll.addLast(4);

		ll.addLast(5);

		System.out.println("Before Reverse");
		ll.printLL();

		ll.reverse();

		System.out.println("After Reverse");
		ll.printLL();
	}
}
