import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	int countNodes(){
		
		Node temp = head;

		int c = 0;

		while(temp != null){
			
			c++;

			temp = temp.next;
		}

		return c;
	}
	
	// 1 - Naive Appraoch
/*
	boolean isPalindrome(){
		
		if(head == null){		// T.C = O(N)
						// S.C = O(N)
			
			return false;
		}else{
			
			if(head.next == null){
				
				return true;
			}else{
				
				int count = countNodes();

				int arr[] = new int[count];

				Node temp = head;

				int i = 0;

				while(temp != null){
					
					arr[i] = temp.data;

					i++;

					temp = temp.next;
				}

				int start = 0, end = count - 1;

				while(start < end){
					
					if(arr[start] != arr[end]){
						
						return false;
					}

					start++;
					end--;
				}

				return true;
			}
		}
	}
*/
	Node reverse(Node head){

		Node curr = head, prev = null, next = null;

		while(curr != null){
			
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}

		return prev;
	}

	boolean isPalindrome(){
		
		if(head == null || head.next == null){
			
			return true;
		}

		Node slow = head, fast = head;

		while(fast.next != null && fast.next.next != null){
			
			slow = slow.next;
			fast = fast.next.next;
		}

		slow.next = reverse(slow.next);

		slow = slow.next;

		Node dummy = head;

		while(slow != null){
			
			if(slow.data != dummy.data){
				return false;
			}

			dummy = dummy.next;
			slow = slow.next;
		}

		return true;
	}


	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(1);

		ll.addLast(2);

		ll.addLast(3);

		ll.addLast(3);

		ll.addLast(2);

		ll.addLast(1);

		boolean ret = ll.isPalindrome();

		if(ret == true)
			System.out.println("LL is Palindrome");
		else
			System.out.println("LL is not Palindrome");
	}
}
