import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	Node sortedMerge(Node first, Node second){

		if(first == null)
			return second;
		if(second == null)
			return first ;

		if(first.data < second.data){
			
			first.next = sortedMerge(first.next, second);
			first.next.prev = first;
			first.prev = null;
			
			return first;
		}else{
			second.next = sortedMerge(first, second.next);
			second.next.prev = second;
			second.prev = null;

			return second;
		}
	}

	Node mergeSort(Node node){
		
		if(node == null || node.next == null){
			
			return node;
		}

		Node second = getMiddle(node);

		node = mergeSort(node);

		second = mergeSort(second);

		return sortedMerge(node, second);
	}

	Node getMiddle(Node h){
		
		if(h == null)
			return h;

		Node slow  = h, fast = h;

		while(fast.next != null && fast.next.next != null){
			
			slow = slow.next;
			fast = fast.next.next;
		}

		Node temp = slow.next;
		slow.next = null;
		return temp;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(50);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(10);
		ll.addLast(20);

		ll.head = ll.mergeSort(ll.head);

		ll.printLL();
	}
}
