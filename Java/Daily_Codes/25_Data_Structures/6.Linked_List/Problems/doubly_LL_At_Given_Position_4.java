
import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
	}

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	void addAtPos(int data, int pos){

		if(pos <= 0 || pos >= countNodes() + 2){

			System.out.println("Invalid Index");
			return;
		}else{

			if(pos == 1){

				addFirst(data);
			}else if(pos == countNodes() + 1){

				addLast(data);
			}else{

				Node newNode = new Node(data);

				Node temp = head;

				while(pos - 2 != 0){

					temp = temp.next;

					pos--;
				}

				newNode.next = temp.next;
				newNode.prev = temp;
				temp.next.prev = newNode;
				temp.next = newNode;
			}
		}
	}

	int countNodes(){

		Node temp = head;

		int c = 0;

		while(temp != null){

			c++;
			temp = temp.next;
		}

		return c;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char choice;

		do{

			System.out.println("1.addFirst");
			System.out.println("2.addLast");
			System.out.println("3.addAtPos");
			System.out.println("4.countNodes");
			System.out.println("5.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();
						ll.addFirst(data);							      }
					break;
				case 2:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      }
					break;
				case 3:
					{
					int pos,data;
					System.out.println("Enter Node Position");
					pos = sc.nextInt();

					System.out.println("Enter Node Data");

					data = sc.nextInt();

					ll.addAtPos(data,pos);
					}
					break;
				case 4:
					{
					int count = ll.countNodes();
					System.out.println("Count of Node is " + count);
					}
					break;
				case 5:
					ll.printLL();
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
