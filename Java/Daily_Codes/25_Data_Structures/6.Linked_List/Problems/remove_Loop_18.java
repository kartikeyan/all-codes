

import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	void remove(){
		
		Node slow = head, fast = head, start = head;

		while(fast != null && fast.next != null){
			
			slow = slow.next;
			fast = fast.next.next;

			if(slow == fast){
				
				while(start != slow){
					
					start = start.next;
					slow = slow.next;
				}

				while(fast.next != start){
					
					fast = fast.next;
				}

				fast.next = null;
				return;
			}
		}
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(10);
		ll.addLast(20);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(50);
		ll.addLast(60);

		ll.printLL();

		ll.remove();
	}
}
