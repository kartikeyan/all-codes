import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	int countNodes(){
		
		Node temp = head;

		int c = 0;

		while(temp != null){
			
			c++;

			temp = temp.next;
		}

		return c;
	}

	boolean isPalindrome(){
		
		if(head == null){
			
			return false;
		}else{
			
			if(head.next == null){
				
				return true;
			}else{
				
				int count = countNodes();

				int arr[] = new int[count];

				Node temp = head;

				int i = 0;

				while(temp != null){
					
					arr[i] = temp.data;

					i++;

					temp = temp.next;
				}

				int start = 0, end = count - 1;

				while(start < end){
					
					if(arr[start] != arr[end]){
						
						return false;
					}

					start++;
					end--;
				}

				return true;
			}
		}
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char choice;

		do{
			System.out.println("1.addLast");
			System.out.println("2.Check Palindrome");
			System.out.println("3.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      	      }	
					break;
				case 2:
					{
						boolean ret = ll.isPalindrome();

						if(ret == true)
							System.out.println("LL is Palindrome");
						else
							System.out.println("LL is not Palindrome");
					}
					break;
				case 3:
					ll.printLL();
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
