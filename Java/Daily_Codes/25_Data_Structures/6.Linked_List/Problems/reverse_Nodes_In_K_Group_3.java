/*
 	25. Reverse Nodes in k-Group

	Given the head of a linked list, reverse the nodes of the list k at a time, and return the modified list.

	k is a positive integer and is less than or equal to the length of the linked list. If the number of nodes is not a multiple of k then left-out nodes, in the end, should remain as it is.

	You may not alter the values in the list's nodes, only nodes themselves may be changed.

	Input: head = [1,2,3,4,5], k = 2
	Output: [2,1,4,3,5]

	Input: head = [1,2,3,4,5], k = 3
	Output: [3,2,1,4,5]

	Constraints:

	    The number of nodes in the list is n.
	    1 <= k <= n <= 5000
	    0 <= Node.val <= 1000
*/

import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	int countNodes(){
		
		Node temp = head;

		int c = 0;

		while(temp != null){
			
			c++;
			temp = temp.next;
		}

		return c;
	}

	Node reverseLLIterative(Node newHead){
		
		if(newHead == null){
			
			System.out.println("LL is empty");
		}

		if(newHead.next == null){
			
			System.out.println("Only 1 node is prresent");
		}

		Node prev = null;
		Node curr = newHead;
		Node next = null;

		while(curr != null){
			
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}

		newHead = prev;

		return newHead;
	}

	void reverseLLRecursive(Node prev, Node curr){
		
		if(curr == null){
			
			head = prev;
			return;
		}else{
			
			Node next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;
		}

		reverseLLRecursive(prev, curr);
	}

	Node getKthNode(Node temp, int k){
		
		k--;

		while(temp != null && k > 0){
			
			k--;

			temp = temp.next;
		}

		return temp;
	}

	void reverseKIterative(int k){
		
		Node temp = head;

		Node prevLast = null;

		while(temp != null){
			
			Node kThNode = getKthNode(temp, k);

			if(kThNode == null){
				
				if(prevLast == null){
					
					prevLast.next = temp;
					break;
				}
			}

			Node nextNode = kThNode.next;

			kThNode.next = null;

			reverseLLIterative(temp);

			if(temp == head){
				
				head = kThNode;
			}else{
				prevLast.next = kThNode;
			}

			prevLast = temp;

			temp = nextNode;
		}
	}

	int length(Node head){
		
		int c = 0;

		Node temp = head;

		while(temp != null){
			
			c++;

			temp = temp.next;
		}

		return c;
	}

	Node reverseKRecursive(Node head, int k){
		
		if(head == null){
			
			return null;
		}

		int len = length(head);

		System.out.println("len is "+len);

		if(len < k){

			return head;
		}


		
		Node curr = head;
		Node prev = null, next = null;

		int count = 0;

		while(curr != null && count < k){
			
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;

			count++;
		}

		if(next != null){
			
			head.next = reverseKRecursive(next, k);
		}

		return prev;
		

	/*	
		if(head == null){
			
			return null;
		}

		Node curr = head, prev = null, next = null;

		int count = 0;

		while(curr != null && count < k){
			
			next = curr.next;
			curr.next = prev;
			prev = curr;
			curr = next;

			count++;
		}

		if(next != null){
			
			head.next = reverseKRecursive(next, k);
		}

		return prev;
	*/
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char choice;

		do{

			System.out.println("1.addLast");
			System.out.println("2.ReverseLL Recursive");
			System.out.println("3.Reverse Nodes in K Group Iterative");
			System.out.println("4.Reverse Nodes in K Group Recursive");
			System.out.println("5.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      
					}
					break;
				case 2:
					{
						Node prev = null;
						Node curr = ll.head;

						ll.reverseLLRecursive(prev, curr);
					}
					break;
				case 3:
					{
						System.out.println("Enter K");
						int k = sc.nextInt();

						ll.reverseKIterative(k);
					}
					break;
				case 4:
					{
						System.out.println("Enter K");
						int k = sc.nextInt();

						ll.reverseKRecursive(ll.head, k);
					}
					break;
				case 5:
					ll.printLL();
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
