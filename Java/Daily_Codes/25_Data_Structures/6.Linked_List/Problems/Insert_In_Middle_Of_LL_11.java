import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			newNode.next = head;
			head = newNode;
		}
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}

	void addAtMid(int data){
		
		if(head == null){

			head = new Node(data);
		}else{
			
			Node newNode = new Node(data);

			Node slow = head;

			Node fast = head.next;

			while(fast != null && fast.next != null){
				
				slow = slow.next;

				fast = fast.next.next;
			}

			newNode.next = slow.next;

			slow.next = newNode;
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addFirst(10);

		ll.addFirst(20);

		ll.addFirst(40);

		ll.addFirst(50);

		ll.printLL();

		ll.addAtMid(30);

		System.out.println("After Inserting in Middle");
		ll.printLL();
	}
}
