import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			newNode.next = head;
			head = newNode;
		}
	}

	boolean isIdentical(LinkedList l1, LinkedList l2){
		
		Node h1 = l1.head, h2 = l2.head;

		while(h1 != null && h2 != null){
			
			if(h1.data != h2.data){
				
				return false;
			}

			h1 = h1.next;
			h2 = h2.next;
		}

		return (h1 == null && h2 == null);
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll1 = new LinkedList();
		LinkedList ll2 = new LinkedList();

		ll1.addFirst(1);

		ll1.addFirst(1);

		ll1.addFirst(1);


		ll2.addFirst(1);

		ll2.addFirst(1);

		ll2.addFirst(1);
		
		if(ll1.isIdentical(ll1,ll2) == true){
			
			System.out.println("Identical");
		}else
			System.out.println("Not");
	}
}
