
//	The task is to left shift LL by k nodes, where k is positive no. smaller than or equal to length of LL

import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	Node sortedMerge(Node first, Node second){

		if(first == null)
			return second;
		if(second == null)
			return first ;

		if(first.data < second.data){
			
			first.next = sortedMerge(first.next, second);
			first.next.prev = first;
			first.prev = null;
			
			return first;
		}else{
			second.next = sortedMerge(first, second.next);
			second.next.prev = second;
			second.prev = null;

			return second;
		}
	}

	Node mergeSort(Node node){
		
		if(node == null || node.next == null){
			
			return node;
		}

		Node second = getMiddle(node);

		node = mergeSort(node);

		second = mergeSort(second);

		return sortedMerge(node, second);
	}

	Node getMiddle(Node h){
		
		if(h == null)
			return h;

		Node slow  = h, fast = h;

		while(fast.next != null && fast.next.next != null){
			
			slow = slow.next;
			fast = fast.next.next;
		}

		Node temp = slow.next;
		slow.next = null;
		return temp;
	}
	
	void rotate(int k){
	
		if(k == 0){
			System.out.println("No need to modify");
			return;
		}

		Node current = head;

		int count = 1;

		// curr will point to kth or null after this loop
		while(count < k && current != null){
			
			current = current.next;
			count++;
		}

		if(current == null){
			System.out.println("K is greater than or equal to countNodes");
			return;
		}
		
		// kThNode points to 40
		Node kThNode = current;

		// curr will point to last node after this loop
		while(current.next != null)
			current = current.next;

		// next of 60 is now changes to 10
		current.next = head;

		// head now points to 50
		head = kThNode.next;

		// change next of kth node will null
		kThNode.next = null;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(10);
		ll.addLast(20);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(50);
		ll.addLast(60);

		ll.printLL();

		ll.rotate(4);

		ll.printLL();
	}
}
