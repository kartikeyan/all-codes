import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	void removeDup(){
		
		if(head == null){
			
			System.out.println("LL is empty");
			return;
		}else{
			
			Node ptr1 = head, ptr2 = null;

			while(ptr1 != null && ptr1.next != null){

				ptr2 = ptr1;
				
				while(ptr2.next != null){
					
					if(ptr1.data == ptr2.next.data){
						
						ptr2.next = ptr2.next.next;
					}else
						ptr2 = ptr2.next;
				}

				ptr1 = ptr1.next;
			}
		}
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		char choice;

		do{
			System.out.println("1.addLast");
			System.out.println("2.RemoveDuplicates");
			System.out.println("3.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      	      }	
					break;
				case 2:
					ll.removeDup();
					break;
				case 3:
					ll.printLL();
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
