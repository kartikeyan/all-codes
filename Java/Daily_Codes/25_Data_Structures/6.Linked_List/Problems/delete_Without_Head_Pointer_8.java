import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addFirst(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			newNode.next = head;
			head = newNode;
		}
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}

	void delete(Node pos){
		
		if(pos == null)
			return;		// LL is empty

		else if(pos.next == null){

			System.out.println("Only 1 node is present");
			return;
		}else{
			pos.data = pos.next.data;

			pos.next = pos.next.next;
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addFirst(20);

		ll.addFirst(4);

		ll.addFirst(15);

		ll.addFirst(35);

		ll.printLL();
	
		System.out.println("After Deletion");

		Node del = ll.head.next;

		ll.delete(del);

		ll.printLL();
	}
}
