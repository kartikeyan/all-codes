

import java.util.*;

class Node{

	int data;
	Node next = null;
	Node prev = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;

			newNode.prev = temp;
		}
	}

	Node merge(Node h1, Node h2){
		
		if(h1 == null)
			return h2;
		if(h2 == null)
			return h1;

		if(h1.data < h2.data){
			
			h1.next = merge(h1.next,h2);
			return h1;
		}else{
			h2.next = merge(h1, h2.next);
			return h2;
		}		
	}

	void printLL(Node head){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.print(temp.data + " ");
				temp = temp.next;
			}

			System.out.println();
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll1 = new LinkedList();

		ll1.addLast(10);
		ll1.addLast(20);
		ll1.addLast(30);

		LinkedList ll2 = new LinkedList();

		ll2.addLast(40);
		ll2.addLast(50);
		ll2.addLast(60);

		Node merged = ll1.merge(ll1.head,ll2.head);

		ll1.printLL(merged);
	}
}
