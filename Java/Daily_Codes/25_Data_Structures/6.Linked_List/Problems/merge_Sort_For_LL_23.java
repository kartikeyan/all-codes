import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	// Approach - 1
/*
	Node sortedMerge(Node a, Node b){
		
		Node result = null;

		if(a == null)
			return b;
		if(b == null)
			return a;

		if(a.data <= b.data){
			
			result = a;
			result.next = sortedMerge(a.next, b);
		}else{
			result = b;
			result.next = sortedMerge(a, b.next);
		}

		return result;
	}

	Node mergeSort(Node h){
		
		if(h == null || h.next == null){
			
			return h;
		}

		Node middle = getMiddle(h);
		Node nextOfMiddle = middle.next;

		middle.next = null;

		Node left = mergeSort(h);

		Node right = mergeSort(nextOfMiddle);

		Node sortedList = sortedMerge(left, right);

		return sortedList;
	}
*/
	
	Node merge(Node head1, Node head2){
		
		Node merged = new Node(-1);

		Node temp = merged;

		while(head1 != null && head2 != null){
			
			if(head1.data < head2.data){
				
				temp.next = head1;

				head1 = head1.next;
			}else{
				
				temp.next = head2;

				head2 = head2.next;
			}

			temp = temp.next;
		}

		while(head1 != null){
			
			temp.next = head1;

			head1 = head1.next;

			temp = temp.next;
		}

		while(head2 != null){
			
			temp.next = head2;

			head2 = head2.next;

			temp = temp.next;
		}
		
		return merged.next;
	}

	Node mergeSort(Node head){
		
		if(head.next == null || head == null){
			
			return head;
		}

		Node mid = getMiddle(head);

		Node nextOfMiddle = mid.next;

		mid.next = null;

		Node newHead1 = mergeSort(head);

		Node newHead2 = mergeSort(nextOfMiddle);

		Node finalHead = merge(newHead1, newHead2);

		return finalHead;
	}

	Node getMiddle(Node h){
		
		if(h == null)
			return h;

		Node slow  = h, fast = h;

		while(fast.next != null && fast.next.next != null){
			
			slow = slow.next;
			fast = fast.next.next;
		}

		return slow;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		ll.addLast(50);
		ll.addLast(30);
		ll.addLast(40);
		ll.addLast(10);
		ll.addLast(20);

		ll.head = ll.mergeSort(ll.head);

		ll.printLL();
	}
}
