
import java.util.*;

class Node{
	
	int data;

	Node next = null;

	Node(int data){
		
		this.data = data;
	}
}

class LinkedList{
	
	Node head = null;

	void addLast(int data){
		
		Node newNode = new Node(data);

		if(head == null){
			
			head = newNode;
		}else{
			
			Node temp = head;

			while(temp.next != null){
				
				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	Node addition(Node head1, Node head2){
		
		Node temp1 = head1, temp2 = head2;

		Node dummyNode = new Node(-1);

		Node curr = dummyNode;

		int carry = 0;

		while(temp1 != null || temp2 != null){
			
			int sum = carry;

			if(temp1 != null){
				
				sum += temp1.data;
			}

			if(temp2 != null){
				
				sum += temp2.data;
			}

			Node newNode = new Node(sum % 10);

			carry = sum / 10;

			curr.next = newNode;

			curr = curr.next;

			if(temp1 != null){
				
				temp1 = temp1.next;
			}

			if(temp2 != null){
				
				temp2 = temp2.next;
			}
		}

		if(carry != 0){
			
			Node newNode = new Node(carry);

			curr.next = newNode;
		}

		return dummyNode.next;
	}
}

class Client{

	public static void main(String[] args){
	
	LinkedList ll1 = new LinkedList();

	LinkedList ll2 = new LinkedList();

	ll1.addLast(2);
	ll1.addLast(4);
	ll1.addLast(3);

	ll2.addLast(5);
	ll2.addLast(6);
	ll2.addLast(4);

	Node newNode = ll1.addition(ll1.head, ll2.head);

	while(newNode != null){
		
		System.out.println(newNode.data);
		newNode = newNode.next;
	}
	}
}
