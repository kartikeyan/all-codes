
/*
 	

    Floyd’s Cycle detection algorithm terminates when fast and slow pointers meet at a common point. It is also known that this common point is one of the loop nodes. Store the address of this common point in a pointer variable ptr. Then initialize a counter with 1 and start from the common point and keeps on visiting the next node and increasing the counter till the common pointer is reached again. At that point, the value of the counter will be equal to the length of the loop.

*/

import java.util.*;

class Node{

	int data;
	Node next = null;

	Node(int data){

		this.data = data;
	}
}

class LinkedList{

	Node head = null;

	void addLast(int data){

		Node newNode = new Node(data);

		if(head == null){

			head = newNode;
		}else{

			Node temp = head;

			while(temp.next != null){

				temp = temp.next;
			}

			temp.next = newNode;
		}
	}

	int countNodes(Node x){

		Node temp = x;

		int c = 1;

		while(temp.next != null){

			c++;
			temp = temp.next;
		}

		return c;
	}

	int lengthOfLoop(){
		
		Node slow = head, fast = head;

		while(slow != null && fast != null && fast.next == null){
			
			slow = slow.next;

			fast = fast.next.next;

			if(slow == fast){
				
				return countNodes(slow);
			}
		}

		return 0;
	}

	void printLL(){

		if(head == null){

			System.out.println("LL is empty");
			return;
		}else{
			Node temp = head;

			while(temp != null){

				System.out.println(temp.data + " ");
				temp = temp.next;
			}
		}
	}
}

class Client{

	public static void main(String[] args){

		LinkedList ll = new LinkedList();

		Node head = newNode(1);

		char choice;

		do{

			System.out.println("1.addLast");
			System.out.println("2.Length of Loop");
			System.out.println("3.printLL");

			System.out.println("Enter Choice");

			Scanner sc = new Scanner(System.in);
			int ch = sc.nextInt();

			switch(ch){

				case 1:
					{
						System.out.println("Enter data");
						int data = sc.nextInt();

						ll.addLast(data);							      		}
					break;
				case 2:
					{
						int c = ll.lengthOfLoop();
						if(c == 0){

							System.out.println("There is no loop");
						}else
							System.out.println("length is " + c);
					}
					break;
				case 3:
					ll.printLL();
					break;
				default:
					System.out.println("Enter Correct Choice\n");
			}

			System.out.println("Do u want to continue");
			choice = sc.next().charAt(0);

		}while(choice == 'y' || choice == 'Y');
	}
}
