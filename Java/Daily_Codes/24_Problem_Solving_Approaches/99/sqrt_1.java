
import java.util.*;

class Sol{

	static int sqrt(int n){
		
		int ret = -1, itr = 0;

		for(int i = 0; i <= n; i++){	// T.C = O(sqrt(n))
			
			if(i * i <= n){
				
				ret = i;
			}

		}

		System.out.println("Itr is " + ret);
		return ret;
	}
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number");
		int x = sc.nextInt();

		int ret = sqrt(x);

		System.out.println("Sqaure Root of Num is " + ret);
	}
}

/*
 	Enter Number
	1000
	Itr is 31
	Sqaure Root of Num is 31
*/
