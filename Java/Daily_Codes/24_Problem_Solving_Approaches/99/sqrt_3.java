
import java.util.*;

class Sol{

	static int sqrt(int n){		
		
		int start = 1, end = n, ans = -1;
		
		int itr = 0;

		while(start <= end){
			
			itr++;

			int mid = (start + end) / 2;

			int sqr = mid * mid;		// T.C = O(log n)

			if(sqr == n){
				
				System.out.println("Itr is " + itr);	
				return mid;
			}

			if(sqr > n){
				
				end = mid - 1;
			}

			if(sqr < n){
				
				ans = mid;

				start = mid + 1;
			}
		}

		System.out.println("Itr is " + itr);	
				
		return ans;
	}
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number");
		int x = sc.nextInt();

		int ret = sqrt(x);

		System.out.println("Sqaure Root of Num is " + ret);
	}
}

/*
 	Enter Number
	1000
	Itr is 9
	Sqaure Root of Num is 31
*/
