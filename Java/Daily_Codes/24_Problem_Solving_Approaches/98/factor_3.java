import java.util.*;

class Sol{

	static int factor(int n){
		
		int c = 0, itr = 0;

		for(int i = 1; i <= Math.sqrt(n); i++){		// T.C = O(log n).
			
			itr++;

			if(n % i == 0){

				if(i == n / i){
					
					c++;
				}else
					c += 2;
			}
		}
		
		System.out.println("itr is " + itr);
		return c;
	}
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number");
		int x = sc.nextInt();

		int ret = factor(x);

		System.out.println("Factor Count is " + ret);
	}
}

/*
 	Enter Number
	15
	itr is 3
	Factor Count is 4
*/
