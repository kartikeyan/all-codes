import java.util.*;

class Sol{

	static int factor(int n){
		
		int c = 0, itr = 0;

		for(int i = 1; i <= n/2; i++){		// T.C = O(n/2).
			
			itr++;

			if(n % i == 0){

				c++;
			}
		}
		
		System.out.println("itr is " + itr);
		return c+1;
	}
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Number");
		int x = sc.nextInt();

		int ret = factor(x);

		System.out.println("Factor Count is " + ret);
	}
}

/*
 	Enter Number
	15
	itr is 7
	Factor Count is 4
*/
