interface Demo1{

	static void fun(){
		System.out.println("In Fun_Demo_1");
	}
}

interface Demo2{

	static void fun(){
		System.out.println("In Fun_Demo_2");
	}
}

class DemoChild implements Demo1, Demo2{		// static method DemoChild madhe alela nahiye
	
}

class Client{
	
	public static void main(String[] args){
		
		DemoChild obj = new DemoChild();

		obj.fun();
	}
}

/*
 	Interface_Static_2.java:26: error: cannot find symbol
		obj.fun();
		   ^
 	symbol:   method fun()
  	location: variable obj of type DemoChild
	1 error
*/
