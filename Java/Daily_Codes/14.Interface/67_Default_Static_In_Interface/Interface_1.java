
//	ithe static and default "Keywords ahet"

interface Demo{
	
	static void fun(){
		
		System.out.println("Fun_Demo");
	}

	default void gun(){
		
		System.out.println("Gun_Demo");
	}
}

/*
 	Compiled from "Interface_1.java"
	interface Demo {
	  public static void fun();
	    Code:
	       0: getstatic     #1                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #7                  // String Fun_Demo
	       5: invokevirtual #9                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return

	  public default void gun();
	    Code:
	       0: getstatic     #1                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: ldc           #15                 // String Gun_Demo
	       5: invokevirtual #9                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	       8: return
	}
*/
