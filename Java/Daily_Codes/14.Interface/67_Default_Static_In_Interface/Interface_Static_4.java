interface Demo1{

	static void fun(){

		System.out.println("In Fun_Demo_1");
	}
}

interface Demo2{

	static void fun(){

		System.out.println("In Fun_Demo_2");
	}
}

class DemoChild implements Demo1, Demo2{
	
	void fun(){
		
		System.out.println("In fun_Child");

		Demo1.fun();

		Demo2.fun();
	}	
}

class Client{
	
	public static void main(String[] args){
		
		Demo1 obj = new DemoChild();	// interface madhe static method asel tar, tyala fakt
						// inteface class cha reference mhnje (navane) access 
						// karta yeto.

		obj.fun();
	}
}

/*
	Interface_Static_4.java:33: error: illegal static interface method call
		obj.fun();
		       ^
  	the receiver expression should be replaced with the type qualifier 'Demo1'
	1 error
*/
