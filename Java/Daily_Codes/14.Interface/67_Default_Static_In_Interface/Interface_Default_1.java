
/*

	1] Inteface madhe method default asel tart, toh child class madhe overRide hoto

	2] Default tevhach vapraycha jevha method overRide karaycha asel tar.
*/

interface Demo{
	
	void gun();

	default void fun(){
		
		System.out.println("In fun_Demo");
	}
}

class DemoChild implements Demo{
	
	public void gun(){
			
		System.out.println("In gun_DemoChild");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo obj = new DemoChild();
		obj.gun();
		obj.fun();
	}
}

/*
 	In gun_DemoChild
	In fun_Demo
*/
