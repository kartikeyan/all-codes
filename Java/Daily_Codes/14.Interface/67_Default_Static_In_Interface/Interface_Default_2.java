
/*
	1] Inteface madhe method default asel tart, toh child class madhe overRide hoto

	2] Default tevhach vapraycha jevha method overRide karaycha asel tar.
*/

interface Demo1{

	default void fun(){
		
		System.out.println("In fun_Demo1");
	}
}

interface Demo2{
	
	default void fun(){
			
		System.out.println("In fun_Demo2");
	}
}

class DemoChild implements Demo1, Demo2{
	
	public void fun(){
	
		System.out.println("In fun_DemoChild");	
	}
}

class Client{
	
	public static void main(String[] args){
		
		DemoChild obj1 = new DemoChild();
		obj1.fun();
		
		Demo1 obj2 = new DemoChild();
		obj2.fun();
		
		Demo2 obj3 = new DemoChild();
		obj3.fun();
	}
}

/*
 	In fun_DemoChild
	In fun_DemoChild
	In fun_DemoChild
*/
