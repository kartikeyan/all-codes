/*
 	1] Jar class madhe method static asel tar, overRide hot nahi.

	2] Jar inteface madhe method static asel tar, overRide hoto.
*/

interface Demo{

	static void fun(){
		
		System.out.println("In fun_Demo");
	}
}

class DemoChild implements Demo{		// interface madhe method static asel tar, child class
						// madhe method overRide hot nahi
	
	
}

class Client{
	
	public static void main(String[] args){
		
		DemoChild obj = new DemoChild();
		obj.fun();
	}
}

/*
 	Interface_Static_1.java:27: error: cannot find symbol
		obj.fun();
		   ^
  	symbol:   method fun()
  	location: variable obj of type DemoChild
	1 error
*/
