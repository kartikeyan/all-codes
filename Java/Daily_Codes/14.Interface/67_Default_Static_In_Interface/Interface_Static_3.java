interface Demo1{

	static void fun(){
		System.out.println("In Fun_Demo_1");
	}
}

interface Demo2{

	static void fun(){
		System.out.println("In Fun_Demo_2");
	}
}

class DemoChild implements Demo1, Demo2{
	
	void fun(){
		
		System.out.println("In fun_Child");

		Demo1.fun();

		Demo2.fun();
	}	
}

class Client{
	
	public static void main(String[] args){
		
		DemoChild obj = new DemoChild();

		obj.fun();
	}
}

/*
	In fun_Child
	In Fun_Demo_1
	In Fun_Demo_2
*/
