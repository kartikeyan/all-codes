
/*
 	Interface je variable astat tyana jaga Stack Frame madhe bhetto
*/

interface Demo{
	
	int x = 10;

	void fun();
}

class DemoChild implements Demo{
	
	public void fun(){
		
		System.out.println(x);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo obj = new DemoChild();

		obj.fun();	// 10
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/14.Interface/68_Variables_In_Interface$ javap -c Demo.class 
	Compiled from "p2.java"
	interface Demo {
	  public static final int x;

	  public abstract void fun();
	}
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/14.Interface/68_Variables_In_Interface$ javap -c DemoChild.class 
	Compiled from "p2.java"
	class DemoChild implements Demo {
	  DemoChild();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: return

	  public void fun();
	    Code:
	       0: getstatic     #7                  // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: bipush        10
	       5: invokevirtual #15                 // Method java/io/PrintStream.println:(I)V
	       8: return
	}
*/
