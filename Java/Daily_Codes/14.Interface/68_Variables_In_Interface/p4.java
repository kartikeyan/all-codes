
interface A{
	
	int x = 10;
}

interface B{
	
	int x = 20;
}

class Child implements A, B{
	
	int x = 30;

	void fun(){
		
		System.out.println(A.x);
		System.out.println(B.x);
		System.out.println(x);
	}
}

class Client{
	
	public static void main(String[] args){
		
		Child obj = new Child();

		obj.fun();
	}
}

/*
 	10
	20
	30
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/14.Interface/68_Variables_In_Interface$ javap -c Child.class 
	Compiled from "p4.java"
	class Child implements A,B {
	  int x;

	  Child();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: aload_0
	       5: bipush        30
	       7: putfield      #7                  // Field x:I
	      10: return

	  void fun();
	    Code:
	       0: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
	       3: bipush        10
	       5: invokevirtual #21                 // Method java/io/PrintStream.println:(I)V
	       8: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
	      11: bipush        20
	      13: invokevirtual #21                 // Method java/io/PrintStream.println:(I)V
	      16: getstatic     #13                 // Field java/lang/System.out:Ljava/io/PrintStream;
	      19: aload_0
	      20: getfield      #7                  // Field x:I
	      23: invokevirtual #21                 // Method java/io/PrintStream.println:(I)V
	      26: return
	}
*/
