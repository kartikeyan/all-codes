
interface Demo{
	
	int x = 10;

	void fun();
}

class Client{
	
	static int y = 20;
}

/*
 	s_In_Interface$ javap -c Client.class 
	Compiled from "p1.java"
	class Client {
	  static int y;

	  Client();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: return

	  static {};
	    Code:
	       0: bipush        20
	       2: putstatic     #7                  // Field y:I
	       5: return
	}
*/

/*
 	s_In_Interface$ javap -c Demo.class 
	Compiled from "p1.java"
	interface Demo {
	  public static final int x;

	  public abstract void fun();
	}
*/
