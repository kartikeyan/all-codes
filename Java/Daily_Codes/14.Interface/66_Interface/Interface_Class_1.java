
/*
 	interface class madle method bydeafult "public abstract" astat tyala child class madle method compulsory "public" pahije
*/

interface Demo{
	
	void fun();
	void gun();
}

class DemoChild implements Demo{
	
	void fun(){
		
		System.out.println("In fun");
	}

	void gun(){
		
		System.out.println("In gun");
	}
}

/*
 	p1.java:15: error: gun() in DemoChild cannot implement gun() in Demo
	void gun(){
	     ^
  	attempting to assign weaker access privileges; was public
	p1.java:10: error: fun() in DemoChild cannot implement fun() in Demo
		void fun(){
		     ^
	attempting to assign weaker access privileges; was public
	2 errors
*/
