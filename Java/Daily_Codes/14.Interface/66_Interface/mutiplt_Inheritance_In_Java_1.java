
interface Demo1{
	
	void fun();
}

interface Demo2{
	
	void fun();
}

class DemoChild implements Demo1, Demo2{
	
	public void fun(){
		
		System.out.println("In Fun");
	}
}

class Client{
	
	public static void main(String[] args){
		
		Demo1 obj = new DemoChild();

		obj.fun();
	}
}

/*
 	inteface support karan karto ki, Demo1 madla fun() method la body nahiye, tasach Demo2 madla method pan, DemoChild implements Demo1, Demo2 , aplyala tasach compiler la mahit nasto ki kutla interface class madla method override zala mahit nasto, tyamule ithe ambiguous cha error yet nahi

*/
