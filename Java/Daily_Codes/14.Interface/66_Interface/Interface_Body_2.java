
/*	Interface madhe body deta yeto after Java 1.8 version,
 *	karan ki Jevha 3 classes ahet, ani 3 classes madhe same
 *	method tevha, redundancy kami karnyasathi alay.
 *
 *	Should Compulsory use "static or default modifiers".
*/

interface Demo1{ 

	static void fun(){
		
		System.out.println("Demo1-fun");
	}
}

interface Demo2{ 

	static void fun(){
		
		System.out.println("Demo2-fun");
	}
}

interface Demo3 extends Demo1, Demo2{
	
}

class DemoChild implements Demo3{
	
	public static void main(String[] args){
		
		Demo1 obj = new DemoChild();

		obj.fun();
	}
}

/*

 	Interface_Body_2.java:35: error: illegal static interface method call
		obj.fun();
		       ^
  	the receiver expression should be replaced with the type qualifier 'Demo1'
	1 error

*/
