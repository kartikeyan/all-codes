
//	Interface class cha object banvu shakat nahi

interface Demo{
	
	void fun();
	void gun();
}

class DemoChild implements Demo{
	
	public void fun(){
		
		System.out.println("In fun");
	}

	public void gun(){
		
		System.out.println("In gun");
	}
}

class Client{
	
	public static void main(String[] s){
		
		Demo obj = new Demo();

		obj.fun();
		obj.gun();
	}
}

/*
 	p2.java:25: error: Demo is abstract; cannot be instantiated
		Demo obj = new Demo();
		           ^
	1 error
*/
