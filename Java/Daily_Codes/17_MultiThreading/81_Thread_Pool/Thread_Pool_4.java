
import java.util.concurrent.*;

class MyThread implements Runnable{
	
	int num;

	MyThread(int num){
		
		this.num = num;
	}

	public void run(){
		
		System.out.println(Thread.currentThread() + "Start thread " + num);

		dailyTask();

		System.out.println(Thread.currentThread() + "End thread " + num);
	}

	void dailyTask(){
		
		try{
			Thread.sleep(8000);
		}catch(InterruptedException ie){
			
		}
	}
}

class ThreadPoolDemo{
	
	public static void main(String[] args){
		
		ExecutorService ser = Executors.newSingleThreadExecutor();

		for(int i = 1; i <= 6; i++){
			
			MyThread obj = new MyThread(i);
			ser.execute(obj);
		}

		ser.shutdown();
	}
}

/*
 	Thread[pool-1-thread-1,5,main]Start thread 1
	Thread[pool-1-thread-1,5,main]End thread 1
	Thread[pool-1-thread-1,5,main]Start thread 2
	Thread[pool-1-thread-1,5,main]End thread 2
	Thread[pool-1-thread-1,5,main]Start thread 3
	Thread[pool-1-thread-1,5,main]End thread 3
	Thread[pool-1-thread-1,5,main]Start thread 4
	Thread[pool-1-thread-1,5,main]End thread 4
	Thread[pool-1-thread-1,5,main]Start thread 5
	Thread[pool-1-thread-1,5,main]End thread 5
	Thread[pool-1-thread-1,5,main]Start thread 6
	Thread[pool-1-thread-1,5,main]End thread 6
	katikeyan@kartikeyan:~/javacodes/Codes/17_MultiThreading/81_Thread_Pool$ 
*/


