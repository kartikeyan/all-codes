
class MyThread extends Thread{
	
	public void run(){
		
		Thread t = Thread.currentThread();

		System.out.println(t.getPriority());
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		Thread t = Thread.currentThread();

		System.out.println(t.getPriority());	// 5 -default priority

		t.setPriority(7);

		System.out.println(t);

		MyThread obj1 = new MyThread();
		obj1.start();

		t.setPriority(9);

		MyThread obj2 = new MyThread();
		obj2.start();

		System.out.println(t.getPriority());	// 7	
	}
}

/*
 	5
	Thread[main,7,main]
	5
	7
*/
