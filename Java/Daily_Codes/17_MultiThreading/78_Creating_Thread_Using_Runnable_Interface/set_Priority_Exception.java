class MyThread extends Thread{
	
	public void run(){
		
		Thread t = Thread.currentThread();

		System.out.println(t.getPriority());
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		Thread t = Thread.currentThread();

		System.out.println(t.getPriority());	// 5 -default priority

		t.setPriority(7);

		MyThread obj1 = new MyThread();
		obj1.start();

		t.setPriority(11);

		MyThread obj2 = new MyThread();
		obj2.start();

		System.out.println(t.getPriority());	// 7	
	}
}

/*
 	5
	Exception in thread "main" 7
		java.lang.IllegalArgumentException
		at java.base/java.lang.Thread.setPriority(Thread.java:1138)
		at ThreadDemo.main(set_Priority_Exception.java:25)
*/
