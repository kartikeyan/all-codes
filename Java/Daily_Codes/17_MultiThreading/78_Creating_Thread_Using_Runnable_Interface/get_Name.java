
class Demo extends MyThread{
	
	public void run(){
		
		System.out.println(Thread.currentThread().getName());
	}
}

class MyThread extends Thread{
	
	public void run(){
		
		System.out.println(Thread.currentThread().getName());

		Demo obj = new Demo();

		obj.start();
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
			
		System.out.println(Thread.currentThread().getName());

		MyThread obj = new MyThread();
		obj.start();
	}
}

/*
 	main
	Thread-0
	Thread-1
*/
