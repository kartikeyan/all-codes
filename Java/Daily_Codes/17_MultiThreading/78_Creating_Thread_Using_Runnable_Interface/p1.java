
class MyThread extends Thread{

        public void run(){

		System.out.println(Thread.currentThread().getName());
//              System.out.println(Thread.currentThread().getPriority());
		
		Thread t = Thread.currentThread();
		System.out.println(t.getPriority());
        }
}

class Demo{

        public static void main(String args[]){

                Thread t = new Thread();
		
		System.out.println(t.getName());

                System.out.println(t.getPriority());

                t.setPriority(9);

                MyThread obj1 = new MyThread();
                obj1.start();

//              t.setPriority(7);

                MyThread obj2 = new MyThread();
                obj2.start();

                System.out.println(t.getPriority());
        }
}
