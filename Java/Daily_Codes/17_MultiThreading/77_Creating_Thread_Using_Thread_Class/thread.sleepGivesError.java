
class MyThread extends Thread{
	
	public void run(){
		
		for(int i = 0; i < 10; i++){
			
			System.out.println("In run");
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args){
		
		MyThread obj = new MyThread();
		obj.start();

		for(int i = 0; i < 10; i++){
			
			System.out.println("In main");

			Thread.sleep(1000);
		}
	}
}

/*
 	thread.sleepGivesError.java:24: error: unreported exception InterruptedException; must be caught or declared to be thrown
			Thread.sleep(1000);
			            ^
	1 error
*/
