
/*
 	Ithe compulsory try_catch vaprava lagel karan thread class madhe run()
	method exception throw karat nahi ani apn ithe override karun method 
	change kartoy mahnun throw vaparlyavar error yeto
*/

class MyThread extends Thread{
	
	public void run(){
		
		for(int i = 0; i < 10; i++){
			
			System.out.println("In Run");
			
			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){
				

			}
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args) throws InterruptedException{
		
		MyThread obj = new MyThread();
		obj.start();

		for(int i = 0; i < 10; i++){
			
			System.out.println("In main");

			Thread.sleep(1000);
		}
	}
}

/*
 	In main
	In Run
	In main
	In Run
	In Run
	In main
	In main
	In Run
	In Run
	In main
	In main
	In Run
	In main
	In Run
	In main
	In Run
	In main
	In Run
	In main
	In Run
*/
