
/*
 	Ithe compulsory try_catch vaprava lagel karan thread class madhe run()
	method exception throw karat nahi ani apn ithe override karun method 
	change kartoy mahnun throw vaparlyavar error yeto
*/

class MyThread extends Thread{
	
	public void run() throws InterruptedException{
		
		for(int i = 0; i < 10; i++){
			
			System.out.println("In Run");

			Thread.sleep(1000);
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args) throws InterruptedException{
		
		MyThread obj = new MyThread();
		obj.start();

		for(int i = 0; i < 10; i++){
			
			System.out.println("In main");

			Thread.sleep(1000);
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/17_MultiThreading/77_Creating_Thread_Using_Thread_Class$ javac In_Run_Method_Using_throws_Gives_Error_2.java

	In_Run_Method_Using_throws_Gives_Error_2.java:4: error: run() in MyThread cannot implement run() in Runnable

	public void run() throws InterruptedException{
	            ^
  	overridden method does not throw InterruptedException

	1 error
*/
