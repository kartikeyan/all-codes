
class MyThread extends Thread{
	
	public void run(){
		
		for(int i = 0; i < 10; i++){
			
			System.out.println("In Run");

			Thread.sleep(1000);
		}
	}
}

class ThreadDemo{
	
	public static void main(String[] args) throws InterruptedException{
		
		MyThread obj = new MyThread();
		obj.start();

		for(int i = 0; i < 10; i++){
			
			System.out.println("In main");

			Thread.sleep(1000);
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/17_MultiThreading/77_Creating_Thread_Using_Thread_Class$ javac In_Run_Method_Using_throws_Gives_Error.java
	In_Run_Method_Using_throws_Gives_Error.java:10: error: unreported exception InterruptedException; must be caught or declared to be thrown
			Thread.sleep(1000);
			            ^
	1 error
*/
