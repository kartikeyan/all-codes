
class MyThread extends Thread{
	
	MyThread(ThreadGroup tg, String str){
		
		super(tg, str);
	}

	public void run(){
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo{
	
	public static void main(String[] args){
		
		ThreadGroup pthreadGP = new ThreadGroup("C2W");

		MyThread obj1 = new MyThread(pthreadGP, "C");

		MyThread obj2 = new MyThread(pthreadGP, "CPP");

		MyThread obj3 = new MyThread(pthreadGP, "Java");
		obj1.start();
		obj2.start();
		obj3.start();
	}
}

/*
 	Thread[C,5,C2W]
	Thread[Java,5,C2W]
	Thread[CPP,5,C2W]
*/
