
class MyThread extends Thread{
	
	MyThread(ThreadGroup tg, String str){
		
		super(tg, str);
	}

	public void run(){
		
		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{
	
	public static void main(String[] args){
		
		ThreadGroup pthreadGP = new ThreadGroup("India");

		MyThread obj1 = new MyThread(pthreadGP, "Goa");

		MyThread obj2 = new MyThread(pthreadGP, "Maha");

		MyThread obj3 = new MyThread(pthreadGP, "Assam");
		obj1.start();
		obj2.start();
		obj3.start();
		
		ThreadGroup cthreadGP = new ThreadGroup(pthreadGP, "Pakistan");

		MyThread obj4 = new MyThread(cthreadGP, "Karachi");

                MyThread obj5 = new MyThread(cthreadGP, "Lahore");

                MyThread obj6 = new MyThread(cthreadGP, "Punjab");
                obj4.start();
                obj5.start();
                obj6.start();

		cthreadGP.interrupt();

		System.out.println(pthreadGP.activeCount());
		System.out.println(pthreadGP.activeGroupCount());
	}
}

/*
 	Sample answer may change

 	Thread[Goa,5,India]
	Thread[Punjab,5,Pakistan]
	6
	Thread[Lahore,5,Pakistan]
	Thread[Karachi,5,Pakistan]
	Thread[Maha,5,India]
	Thread[Assam,5,India]
	java.lang.InterruptedException: sleep interrupted
	java.lang.InterruptedException: sleep interrupted
	java.lang.InterruptedException: sleep interrupted
	1
*/
