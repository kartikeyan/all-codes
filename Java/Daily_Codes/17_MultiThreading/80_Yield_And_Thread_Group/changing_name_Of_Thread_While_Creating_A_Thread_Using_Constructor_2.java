
class MyThread extends Thread{

	MyThread(String str){
		
		super(str);
	}

	MyThread(){
		
	}
	
	public void run(){
		
		System.out.println(Thread.currentThread().getName());
	}
}

class ThreadGroupDemo{
	
	public static void main(String[] args){
		
		MyThread obj1 = new MyThread("Kartik");

		obj1.start();

		MyThread obj2 = new MyThread("Onkar");

                obj2.start();

		MyThread obj3 = new MyThread();

                obj3.start();
	}
}

/*
 	Kartik
	Thread-0
	Onkar
*/
