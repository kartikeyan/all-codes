
/*
 	Suitable Constructor

 	public java.lang.Thread(java.lang.ThreadGroup, java.lang.Runnable, java.lang.String);

*/


class MyThread implements Runnable{
	
	public void run(){

		System.out.println(Thread.currentThread());

		try{
			Thread.sleep(3000);
		}catch(InterruptedException ie){

			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{
	
	public static void main(String[] args){
		
		ThreadGroup pThreadGP = new ThreadGroup("India");
		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1 = new Thread(pThreadGP, obj1, "GOA");
		Thread t2 = new Thread(pThreadGP, obj2, "MAha");
		t1.start();
		t2.start();

		ThreadGroup cThreadGP = new ThreadGroup("Pakistan");
		MyThread obj3 = new MyThread();
		MyThread obj4 = new MyThread();

		Thread t3 = new Thread(cThreadGP, obj3, "Karachi");
		Thread t4 = new Thread(cThreadGP, obj4, "Lahore");
		t3.start();
		t4.start();

		cThreadGP.interrupt();
		System.out.println(pThreadGP.activeCount());
		System.out.println(pThreadGP.activeGroupCount());
	}
}

/*
 	Thread[GOA,5,India]
	Thread[Lahore,5,Pakistan]
	java.lang.InterruptedException: sleep interrupted
	2
	Thread[Karachi,5,Pakistan]
	Thread[MAha,5,India]
	java.lang.InterruptedException: sleep interrupted
	0
*/
