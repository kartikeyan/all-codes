
class MyThread extends Thread{
	
	MyThread(ThreadGroup tg, String str){
		
		super(tg, str);
	}

	public void run(){
		
		System.out.println(Thread.currentThread());
	}
}

class ThreadGroupDemo{
	
	public static void main(String[] args){
		
		ThreadGroup pthreadGP = new ThreadGroup("C2W");

		MyThread obj1 = new MyThread(pthreadGP, "C");

		MyThread obj2 = new MyThread(pthreadGP, "CPP");

		MyThread obj3 = new MyThread(pthreadGP, "Java");
		obj1.start();
		obj2.start();
		obj3.start();
		
		ThreadGroup cthreadGP = new ThreadGroup(pthreadGP, "Incubator");

		MyThread obj4 = new MyThread(cthreadGP, "Flutter");

                MyThread obj5 = new MyThread(cthreadGP, "React JS");

                MyThread obj6 = new MyThread(cthreadGP, "Spring Boot");
                obj4.start();
                obj5.start();
                obj6.start();

	}
}

/*
 	Sample answer may change

 	Thread[C,5,C2W]
	Thread[React JS,5,Incubator]
	Thread[Flutter,5,Incubator]
	Thread[Java,5,C2W]
	Thread[CPP,5,C2W]
	Thread[Spring Boot,5,Incubator]
*/
