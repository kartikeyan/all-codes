
/*
 	1] object

		2] Throwable

			3] Exception

				4] Runtime

					5] IllegalArgumentException

						6] NumberFormatException
*/

/*

import java.io.*;

class Demo{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();
		System.out.println(str);

		System.out.println("Enter Int");
		int data = Integer.parseInt(br.readLine());
		System.out.println(data);
	}
}

*/

/*
 	Enter String
	kartik
	kartik
	Enter Int
	charkupalli
	Exception in thread "main" java.lang.NumberFormatException: For input string: "charkupalli"
		at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:67)
		at java.base/java.lang.Integer.parseInt(Integer.java:668)
		at java.base/java.lang.Integer.parseInt(Integer.java:786)
		at Demo.main(NumberFormatException_try_catch_2.java:15)
*/


import java.io.*;

class Demo{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();
		System.out.println(str);

		System.out.println("Enter Int");

		int data = 0;
		
		try{
		
			data = Integer.parseInt(br.readLine());

		}catch(NumberFormatException obj){
			
			System.out.println("Please Enter Integer Data");

			data = Integer.parseInt(br.readLine());
		}

		System.out.println(data);
	
		System.out.println("End Main");
	}
}

/*
 	Enter String
	kartik
	kartik
	Enter Int
	charkupalli
	Please Enter Integer Data
	10
	10
	End Main
*/
