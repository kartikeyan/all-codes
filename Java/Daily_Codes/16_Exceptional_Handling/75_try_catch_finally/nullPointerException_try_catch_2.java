
/*
 	1] object

		2] Throwable

			3] Exception

				4] Runtime

						5] NumberFormatException
*/

/*

class Demo{

	void m1(){
		
		System.out.println("In m1");
	}

	void m2(){
		
		System.out.println("In m2");
	}
	
	public static void main(String[] args){

		System.out.println("Start Main");
		
		Demo obj = new Demo();

		obj.m1();

		obj = null;

		obj.m2();

		System.out.println("End Main");
	}
}

*/

/*
 	Start Main
	In m1
	Exception in thread "main" java.lang.NullPointerException: Cannot invoke "Demo.m2()" because "<local1>" is null
		at Demo.main(nullPointerException_try_catch_2.java:24)
*/

class Demo{

	void m1(){
		
		System.out.println("In m1");
	}

	void m2(){
		
		System.out.println("In m2");
	}
	
	public static void main(String[] args){

		System.out.println("Start Main");
		
		Demo obj = new Demo();

		obj.m1();

		obj = null;
		
		try{
			obj.m2();
		}catch(NullPointerException obj1){
			
			System.out.println("Exception Occured");
		}

		System.out.println("End Main");
	}
}

/*
 	Start Main
	In m1
	Exception Occured
	End Main
*/
