
/*
 	Child Exception nantar Parent Exception lihayla
	pahije nahitar compile time error yeto
*/

import java.io.*;

class Demo{
	
	public static void main(String[] args)throws IOException{
		
		System.out.println("Start Main");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();
		System.out.println(str);

		System.out.println("Enter Int");

		int data = 0;
		try{
			
			data = Integer.parseInt(br.readLine());
		}catch(NumberFormatException obj){

			System.out.println("Exception Occured");
			
		}catch(IllegalArgumentException obj1){
			
		}
		
		System.out.println(data);

		System.out.println("End Main");
	}
}

/*
 	Start Main
	Enter String
	kartik
	kartik
	Enter Int
	char
	Exception Occured
	0
	End Main
*/
