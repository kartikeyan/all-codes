
import java.io.*;

class Demo{
	
	public static void main(String[] args){
		
		for(int i = 0; i < 10; i++){
			
			System.out.println("In loop");

			try{
				Thread.sleep(5000);

			}catch(IOException obj){
			
			}
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/75_try_catch_finally$ javac thread_1.java
	thread_1.java:15: error: exception IOException is never thrown in body of corresponding try statement
			}catch(IOException obj){
			 ^
	thread_1.java:13: error: unreported exception InterruptedException; must be caught or declared to be thrown
				Thread.sleep(5000);
				            ^
	2 errors
*/
