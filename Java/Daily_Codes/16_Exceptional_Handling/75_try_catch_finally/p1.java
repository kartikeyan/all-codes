
import java.io.*;

class Input{
	
	public static void main(String[] args) throws IOException{

		System.out.println("Start Main");
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String 1");
		
		String str = br.readLine();
		
		System.out.println(str);

		br.close();
		
		System.out.println("Enter String 2");
		
		String str2 = br.readLine();

		System.out.println(str2);		

		System.out.println("End Main");
	}
}

/*
 	Start Main
	Enter String 1
	kartik
	kartik
	Enter String 2
	Exception in thread "main" java.io.IOException: Stream closed
		at java.base/java.io.BufferedReader.ensureOpen(BufferedReader.java:123)
		at java.base/java.io.BufferedReader.readLine(BufferedReader.java:321)
		at java.base/java.io.BufferedReader.readLine(BufferedReader.java:396)
		at Input.main(p1.java:22)
*/
