
/*
 	Child Exception nantar Parent Exception lihayla
	pahije nahitar compile time error yeto
*/

import java.io.*;

class Demo{
	
	public static void main(String[] args)throws IOException{
		
		System.out.println("Start Main");

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter String");
		String str = br.readLine();
		System.out.println(str);

		System.out.println("Enter Int");
		int data = 0;
		try{
			
			data = Integer.parseInt(br.readLine());
		}catch(IllegalArgumentException obj){
			
		}catch(NumberFormatException obj1){
			
		}

		System.out.println("End Main");
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/75_try_catch_finally$ javac trial_Error_try_catch_1.java
	trial_Error_try_catch_1.java:28: error: exception NumberFormatException has already been caught
		}catch(NumberFormatException obj1){
		 ^
	1 error
*/
