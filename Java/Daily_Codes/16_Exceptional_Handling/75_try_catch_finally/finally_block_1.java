
/*
 
 	finally: It is executed after the catch block. We use it to put some common code (to be executed irrespective of whether an exception has occurred or not ) when there are multiple catch blocks. 

	1] System.exit(0).

	2] System crash jala tar ya doni veles finally block execute hot nahi, baki saglya condition la execute hoto irrespective of whether exception or occured or not

	Finally is used to close database connection, sever connection, etc

*/

class Demo{

	void m1(){
		

	}

	void m2(){
		
		
	}
	
	public static void main(String[] args){
		
		Demo obj = new Demo();

		obj.m1();

		obj = null;

		try{
			
			obj.m2();
		}catch(NullPointerException obj5){
			
			System.out.println("Exception Occ");
		}finally{
			System.out.println("Connection Closed");
		}

		System.out.println("End Main");
	}
}

/*
 	Exception Occ
	Connection Closed
	End Main
*/
