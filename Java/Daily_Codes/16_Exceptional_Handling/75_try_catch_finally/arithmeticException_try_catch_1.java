
/*
 	1] Object
		
		2] Throwable

			3] Exception

				4] Runtime

					5] Arithmetic Exception
*/

/*

class Demo{
	
	public static void main(String[] args){
		
		System.out.println(10/0);
	}
}

*/

/*
 	Exception in thread "main" java.lang.ArithmeticException: / by zero
	at Demo.main(arithmeticException_try_catch_1.java:6)
*/

class Demo{
	
	public static void main(String[] args){
		
		System.out.println("Start Main");

		try{
			
			System.out.println(10/0);	// Risky Code

		}catch(ArithmeticException obj){
			
			System.out.println("Exception Occured");	// Handling Code
		}

		System.out.println("End Main");
	}
}

/*
 	Start Main
	Exception Occured
	End Main
*/
