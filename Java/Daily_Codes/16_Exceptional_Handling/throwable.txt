

    A] Object

	a] Throwable(Class)
	   
		public class Throwable extends Object implements Serializable

		1] The Throwable class is the superclass of all errors and exceptions in the Java language. 
		2] Only objects that are instances of this class (or one of its subclasses) are thrown by the Java Virtual Machine or can be thrown by the Java throw statement. 
		3] Similarly, only this class or one of its subclasses can be the argument type in a catch clause. 	
		4] For the purposes of compile-time checking of exceptions, Throwable and any subclass of Throwable that is not also a subclass of either RuntimeException or Error are regarded as checked exceptions. 					
		
		Method

		a] getMessage()
		b] getSuppressed()
		c] getStackTrace()
		d] fillInStackTrace()
		e] getLocalizedMessage()
		f] initCause()
		g] getCause()
		h] printStackTrace()

		Types of Throwable Class 

			1] Error (Class)
				
				public class Error extends Throwable

				An Error is a subclass of Throwable that indicates serious problems that a reasonable application should not try to catch. Most such errors are abnormal conditions. The ThreadDeath error, though a "normal" condition, is also a subclass of Error because most applications should not try to catch it.

				A method is not required to declare in its throws clause any subclasses of Error that might be thrown during the execution of the method but not caught, since these errors are abnormal conditions that should never occur. That is, Error and its subclasses are regarded as unchecked exceptions for the purposes of compile-time checking of exceptions.

				a] OutOfMemory
				b] VM error

			2] Exception (Class)

				Types of Exception 

					a] Checked (Compile Time) 	
					b] Unchecked (Runtime)

