
import java.util.*;

class DataOverFlowException{
	
	DataOverFlowException(String msg){
		
		
	}
}

class DataUnderFlowException{

        DataUnderFlowException(String msg){


        }
}

class ArrayDemo{
	
	public static void main(String[] args){
		
		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Integer value");

		System.out.println("Note : 0 < ele < 100");

		for(int i = 0; i < arr.length; i++){
			
			int data = sc.nextInt();

			if(data < 0){
				
				throw new DataUnderFlowException("Less than 0 ");
			}

			if(data > 100){
				
				throw new DataOverFlowException("Greater than 100");
			}
			
			arr[i] = data;
		}

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i]);
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ javac user_Defined_Exceptions_1.java
user_Defined_Exceptions_1.java:38: error: incompatible types: DataUnderFlowException cannot be converted to Throwable
				throw new DataUnderFlowException("Less than 0 ");
				^
	user_Defined_Exceptions_1.java:43: error: incompatible types: DataOverFlowException cannot be converted to Throwable
				throw new DataOverFlowException("Greater than 100");
				^
	2 errors
*/
