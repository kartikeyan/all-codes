
import java.util.Scanner;

class Demo{
	
	public static void main(String[] args){
		
		Scanner sc = new Scanner(System.in);

		int x = sc.nextInt();

		try{
			
			if(x == 0){
				
				throw new ArithmeticException("Divide by zero");
			}

			System.out.println(10 / x);

		}catch(ArithmeticException ae){
			
			System.out.print("Exception in thread " + Thread.currentThread().getName() + " ");


//			System.out.println(ae.getMessage());
			
			ae.printStackTrace();
		}
	}
}

/*
 	
   	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ java Demo 
	0
	Exception in thread main java.lang.ArithmeticException: Divide by zero
		at Demo.main(using_throw_Keyword_for_arithmeticException.java:16)
	
*/

