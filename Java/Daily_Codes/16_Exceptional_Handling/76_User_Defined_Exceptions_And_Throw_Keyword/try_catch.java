
//	fakt try_catch madhe exception table asto

//	https://www.infoworld.com/article/2076868/how-the-java-virtual-machine-handles-exceptions.html

//	https://stackoverflow.com/questions/6386917/strange-exception-table-entry-produced-by-suns-javac

class Sol{
	
	public static void main(String[] s){
		
		int x = 0;

		try{
			
			x = 10 / 0;
		}catch(ArithmeticException obj){
			
			System.out.println("Exception occured");
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ javap -c Sol.class 
	Compiled from "try_catch.java"
	class Sol {
	  Sol();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: return

	  public static void main(java.lang.String[]);
	    Code:
	       0: iconst_0
	       1: istore_1
	       2: bipush        10
	       4: iconst_0
	       5: idiv
	       6: istore_1
	       7: goto          19
	      10: astore_2
	      11: getstatic     #3                  // Field java/lang/System.out:Ljava/io/PrintStream;
	      14: ldc           #4                  // String Exception occured
	      16: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
	      19: return
	    Exception table:
	       from    to  target type
	           2     7    10   Class java/lang/ArithmeticException
	}
*/
