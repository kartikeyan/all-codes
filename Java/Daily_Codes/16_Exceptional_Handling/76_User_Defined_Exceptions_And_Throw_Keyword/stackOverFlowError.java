
class Demo{

	static void fun(int x){
		
		System.out.println(x);

		fun(++x);
	}
	
	public static void main(String[] args){
		
		fun(10);
	}
}

/*
 	19230
	Exception in thread "main" java.lang.StackOverflowError
		at java.base/java.io.PrintStream.write(PrintStream.java:568)
		at java.base/sun.nio.cs.StreamEncoder.writeBytes(StreamEncoder.java:234)
		at java.base/sun.nio.cs.StreamEncoder.implFlushBuffer(StreamEncoder.java:313)
		at java.base/sun.nio.cs.StreamEncoder.flushBuffer(StreamEncoder.java:111)
		at java.base/java.io.OutputStreamWriter.flushBuffer(OutputStreamWriter.java:178)
		at java.base/java.io.PrintStream.writeln(PrintStream.java:723)
		at java.base/java.io.PrintStream.println(PrintStream.java:938)
		at Demo.fun(stackOverFlowError.java:6)
		at Demo.fun(stackOverFlowError.java:8)
*/
