
/*
 	extends Exception pahije kutla tari i.e checked or unchecked
*/

import java.util.*;

class DataOverFlowException extends RuntimeException{
	
	DataOverFlowException(String msg){
		
		super(msg);		
	}
}

class DataUnderFlowException extends RuntimeException{

        DataUnderFlowException(String msg){

		super(msg);
        }
}

class ArrayDemo{
	
	public static void main(String[] args){
		
		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Integer value");

		System.out.println("Note : 0 < ele < 100");

		for(int i = 0; i < arr.length; i++){
			
			int data = sc.nextInt();

			if(data < 0){
				
				throw new DataUnderFlowException("Less than 0 ");
			}

			if(data > 100){
				
				throw new DataOverFlowException("Greater than 100");
			}
			
			arr[i] = data;
		}

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i]);
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ java ArrayDemo 
	Enter Integer value
	Note : 0 < ele < 100
	-10
	Exception in thread "main" DataUnderFlowException: Less than 0 
		at ArrayDemo.main(user_Defined_Exceptions_3.java:38)
*/
