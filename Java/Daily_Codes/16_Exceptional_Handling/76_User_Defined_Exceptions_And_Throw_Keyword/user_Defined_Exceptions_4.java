
/*
 	extends Exception pahije kutla tari i.e checked or unchecked
*/

import java.util.*;

import java.io.*;

class cgpaUnderFlowException extends IOException{
	
	cgpaUnderFlowException(String msg){
		
		super(msg);		
	}
}

class ArrayDemo{
	
	public static void main(String[] args){
		
		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Integer value");

		System.out.println("Note : 8 < ele < 10");

		for(int i = 0; i < arr.length; i++){
			
			int data = sc.nextInt();

			if(data < 8){
				
				throw new cgpaUnderFlowException("Not Eligible for company");
			}
			
			arr[i] = data;
		}

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i]);
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ javac user_Defined_Exceptions_4.java
user_Defined_Exceptions_4.java:36: error: unreported exception cgpaUnderFlowException; must be caught or declared to be thrown
				throw new cgpaUnderFlowException("Not Eligible for company");
				^
	1 error

*/
