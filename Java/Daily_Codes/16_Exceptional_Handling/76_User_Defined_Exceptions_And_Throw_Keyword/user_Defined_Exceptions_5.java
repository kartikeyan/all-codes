
/*
 	extends Exception pahije kutla tari i.e checked or unchecked
*/

import java.util.*;

import java.io.*;

class cgpaUnderFlowException extends IOException{
	
	cgpaUnderFlowException(String msg){
		
		super(msg);		
	}
}

class ArrayDemo{
	
	public static void main(String[] args) throws IOException{
		
		int arr[] = new int[5];

		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Integer value");

		System.out.println("Note : 8 < ele < 10");

		for(int i = 0; i < arr.length; i++){
			
			int data = sc.nextInt();

			if(data < 8){
				
				throw new cgpaUnderFlowException("Not Eligible for company");
			}
			
			arr[i] = data;
		}

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i]);
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ java ArrayDemo 
	Enter Integer value
	Note : 8 < ele < 10
	4
	Exception in thread "main" cgpaUnderFlowException: Not Eligible for company
		at ArrayDemo.main(user_Defined_Exceptions_5.java:36)

*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/16_Exceptional_Handling/76_User_Defined_Exceptions_And_Throw_Keyword$ javap -c ArrayDemo.class 
Compiled from "user_Defined_Exceptions_5.java"
class ArrayDemo {
  ArrayDemo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]) throws java.io.IOException;
    Code:
       0: iconst_5
       1: newarray       int
       3: astore_1
       4: new           #2                  // class java/util/Scanner
       7: dup
       8: getstatic     #3                  // Field java/lang/System.in:Ljava/io/InputStream;
      11: invokespecial #4                  // Method java/util/Scanner."<init>":(Ljava/io/InputStream;)V
      14: astore_2
      15: getstatic     #5                  // Field java/lang/System.out:Ljava/io/PrintStream;
      18: ldc           #6                  // String Enter Integer value
      20: invokevirtual #7                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      23: getstatic     #5                  // Field java/lang/System.out:Ljava/io/PrintStream;
      26: ldc           #8                  // String Note : 8 < ele < 10
      28: invokevirtual #7                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      31: iconst_0
      32: istore_3
      33: iload_3
      34: aload_1
      35: arraylength
      36: if_icmpge     73
      39: aload_2
      40: invokevirtual #9                  // Method java/util/Scanner.nextInt:()I
      43: istore        4
      45: iload         4
      47: bipush        8
      49: if_icmpge     62
      52: new           #10                 // class cgpaUnderFlowException
      55: dup
      56: ldc           #11                 // String Not Eligible for company
      58: invokespecial #12                 // Method cgpaUnderFlowException."<init>":(Ljava/lang/String;)V
      61: athrow
      62: aload_1
      63: iload_3
      64: iload         4
      66: iastore
      67: iinc          3, 1
      70: goto          33
      73: iconst_0
      74: istore_3
      75: iload_3
      76: aload_1
      77: arraylength
      78: if_icmpge     96
      81: getstatic     #5                  // Field java/lang/System.out:Ljava/io/PrintStream;
      84: aload_1
      85: iload_3
      86: iaload
      87: invokevirtual #13                 // Method java/io/PrintStream.println:(I)V
      90: iinc          3, 1
      93: goto          75
      96: return
}

*/
