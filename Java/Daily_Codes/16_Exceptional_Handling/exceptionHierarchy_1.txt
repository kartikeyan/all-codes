

	1] Exception 
	
		public class Exception extends Throwable

		a] The class Exception and its subclasses are a form of Throwable that indicates conditions that a reasonable application might want to catch.
		b] The class Exception and any subclasses that are not also subclasses of RuntimeException are checked exceptions. 

		c] Checked exceptions need to be declared in a method or constructor's throws clause if they can be thrown by the execution of the method or constructor and propagate outside the method or constructor boundary.

		d] Types of Exception

			A] Compile Time 

				a] IOException
				b] FileNotFoundException
				c] InterruptedException

			B] RunTimeException (class)

				public class RuntimeException extends Exception

				RuntimeException is the superclass of those exceptions that can be thrown during the normal operation of the Java Virtual Machine.

				RuntimeException and its subclasses are unchecked exceptions. Unchecked exceptions do not need to be declared in a method or constructor's throws clause if they can be thrown by the execution of the method or constructor and propagate outside the method or constructor boundary.
	
				a] Arithmetic 
				b] NullPointer
				c] IndexOutOfBound
					
					1] Array
					2] String
