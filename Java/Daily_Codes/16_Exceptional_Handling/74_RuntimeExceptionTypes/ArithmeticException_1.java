
class Demo{

	void m1(){
		
		System.out.println("m1");

		System.out.println(10/0);

		m2();
	}

	void m2(){
		
		System.out.println("m2");
	}

	public static void main(String[] args){	
		
		System.out.println("Start Main");

		Demo obj = new Demo();

		obj.m1();
		
		System.out.println("End Main");
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/Exceptional_Handling/74_RuntimeExceptionTypes$ java Demo 

	Start Main
	m1
	Exception in thread "main" java.lang.ArithmeticException: / by zero
		at Demo.m1(ArithmeticException_1.java:8)
		at Demo.main(ArithmeticException_1.java:24)
*/
