
class Demo{

	void m1(){
		
		System.out.println("M1");
	}
	
	void m2(){
		
		System.out.println("M2");
	}
	
	public static void main(String[] args){
		
		System.out.println("Start Main");

		Demo obj = new Demo();

		obj.m1();

		obj = null;

		obj.m2();

		System.out.println("End Main");
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/Exceptional_Handling/74_RuntimeExceptionTypes$ java Demo 

	Start Main
	M1
	Exception in thread "main" java.lang.NullPointerException: Cannot invoke "Demo.m2()" because "<local1>" is null
		at Demo.main(NullPointerException_2.java:24)
*/
