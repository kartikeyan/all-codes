
import java.io.*;

class Demo{

	void getData() throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int data = Integer.parseInt(br.readLine());

		System.out.println(data);
	}
	
	public static void main(String[] args){
		
		System.out.println("Start Main");

		Demo obj = new Demo();

		obj.getData();
		
		System.out.println("End Main");
	}
}

/*
 	Jar getData() method madhe throws IOException nahi kela tar error yeto

	NumberFormatException_3.java:10: error: unreported exception IOException; must be caught or declared to be thrown
		int data = Integer.parseInt(br.readLine());
		                                       ^
	1 error

	Jar getData() method madhe throws IOException kela tar kutun call 
	kela tar tithe pan throws Kiva try_Catch vaprayla pahije

	NumberFormatException_3.java:21: error: unreported exception IOException; must be caught or declared to be thrown
		obj.getData();
		           ^
	1 error
*/

/*
 	Throws cha asa disadvantage ahe ki jar 100 methods ahet, ani jar
	99 method madhe exception alay, ani call asa kelay ki 99 method la
	call 98 madhun, 98 la 97 madhun, 97 la 96 madhun .........2 from 1,
	tar 99 method madhun exception throw kelay tar 98, 97, 96, ....., 1
	saglya method madhe throw karava lagel exception

	Eka Method madhun jar throw kela tar tyala call kutun jalay tya
        method la pan throw karava lagel
*/

/*
 	Mahnun try_catch cha vapar karto

	eka method madhe try_catch lihila tar tya exception cha handling
	jalay 
*/


