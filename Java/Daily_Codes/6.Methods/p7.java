
/*	Methods in Java
 *
 *		1] Static
 *		2] Non-Static / Instance
*/		

class methodDemo{
	
	int x = 10;
	static int y = 20;

	void fun(){
		
		System.out.println("In fun");
	}

	void gun(){
		
		System.out.println("In gun");
	}

	public static void main(String[] s){
				
		methodDemo obj = new methodDemo();

		System.out.println(obj.x);

		obj.fun();

		obj.gun();
	}	
}

/*	non-static method/variable la access static method karaycha asel tar
 *	object banvun access karta yeil
*/	
