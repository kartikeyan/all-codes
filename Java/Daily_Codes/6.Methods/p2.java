
/*	Methods in Java
 *
 *		1] Static
 *		2] Non-Static / Instance
*/		

class methodDemo{
	
	public static void main(String[] s){
				
		fun();		
	}

	void fun(){
		
		System.out.println("In fun");
	}
}

/*	p2.java:12: error: non-static method fun() cannot be referenced from a static context
		fun();		
		^
	1 error
*/

//	static method madhun fakt static function() la call jato
