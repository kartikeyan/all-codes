
import java.util.Scanner;

class methodDemo{
	
	static void add(int a, int b){
		
		int ans = a + b;
		System.out.println("Addition is " + ans);
	}

	public static void main(String[] s){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Values");

		int a = sc.nextInt();
		int b = sc.nextInt();

		add(a, b);
	}
}
