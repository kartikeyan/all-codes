
/*	Methods in Java
 *
 *		1] Static
 *		2] Non-Static / Instance
*/		

class methodDemo{
	
	int x = 10;
	static int y = 20;

	public static void main(String[] s){
				
		System.out.println(x);
		System.out.println(y);
	}	
}

/*	p5.java:15: error: non-static variable x cannot be referenced from a static context
		System.out.println(x);
		                   ^
	1 error
*/

