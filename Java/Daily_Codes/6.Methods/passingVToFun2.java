
import java.util.Scanner;

class methodDemo{
	
	static void add(int a, int b){
		
		int ans = a + b;
		System.out.println("Addition is " + ans);
	}

	void sub(int a, int b){
		
		System.out.println("Subtract is " + (a - b));
	}

	void mult(int a, int b){
		
		System.out.println("Mult is " + a * b);
	}

	void div(int a, int b){
		
		System.out.println("Div is " + a / b);
	}

	public static void main(String[] s){
		
		Scanner sc = new Scanner(System.in);

		System.out.println("Enter Values");

		int a = sc.nextInt();
		int b = sc.nextInt();
		
		methodDemo obj = new methodDemo();

		add(a, b);
		obj.sub(a,b);
		obj.mult(a,b);
		obj.div(a,b);
	}
}
