
class methodDemo{

	void fun(float x){
		
		System.out.println(x);
	}
	
	public static void main(String[] args){

		methodDemo obj = new methodDemo();

		obj.fun(10);

		obj.fun(10.5f);

		obj.fun('A');
	}
}

/*
 	10.0
	10.5
	65.0
*/

/*	Java Strictly check karto
 *		1] Data loss
 *		2] Boundary Condition
*/
