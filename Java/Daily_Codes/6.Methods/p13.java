
class methodDemo{

	void fun(int x){
		
		System.out.println(x);
	}
	
	public static void main(String[] args){

		methodDemo obj = new methodDemo();

		obj.fun(10);

		obj.fun(10.5f);
	}
}

/*
 	p13.java:15: error: incompatible types: possible lossy conversion from float to int
		obj.fun(10.5f);
		        ^
	Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
	1 error

*/

/*	Java Strictly check karto
 *		1] Data loss
 *		2] Boundary Condition
