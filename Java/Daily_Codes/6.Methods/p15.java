
class methodDemo{

	void fun(float x){
		
		System.out.println(x);
	}
	
	public static void main(String[] args){

		methodDemo obj = new methodDemo();

		obj.fun(true);
	}
}

/*
 	p15.java:13: error: incompatible types: boolean cannot be converted to float
		obj.fun(true);
		        ^
	Note: Some messages have been simplified; recompile with -Xdiags:verbose to get full output
	1 error
*/
