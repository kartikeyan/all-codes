
/*	Methods in Java
 *
 *		1] Static
 *		2] Non-Static / Instance
*/		

class methodDemo{
	
	public static void main(String[] s){
				
		fun();
		gun();	
	}

	static void fun(){
		
		System.out.println("In fun");
	}

	void gun(){
		
		System.out.println("In gun");
	}
}

/*	p4.java:13: error: non-static method gun() cannot be referenced from a static context
		gun();	
		^
	1 error
*/


//	static method madhun fakt static function() la call jato
