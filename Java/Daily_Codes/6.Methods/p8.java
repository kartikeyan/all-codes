
/*	Methods in Java
 *
 *		1] Static
 *		2] Non-Static / Instance
*/		

class methodDemo{
	
	int x = 10;
	static int y = 20;

	void fun(){
		
		System.out.println(x);
		System.out.println(y);
	}

	public static void main(String[] s){
				
		methodDemo obj = new methodDemo();

		obj.fun();
	}	
}

/*	non-static method garib ahe, toh static ani non-static variable la 
 *	access karu shakto
*/	
