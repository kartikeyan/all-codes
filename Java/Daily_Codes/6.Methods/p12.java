
class methodDemo{

	void fun(int x){
		
		System.out.println(x);
	}
	
	public static void main(String[] args){
		
		System.out.println("In main");

		methodDemo obj = new methodDemo();

		obj.fun();

		System.out.println("End Main");
	}
}

/*
 	p12.java:15: error: method fun in class methodDemo cannot be applied to given types;
		obj.fun();
		   ^
	  required: int
	  found: no arguments
	  reason: actual and formal argument lists differ in length
	1 error

*/

