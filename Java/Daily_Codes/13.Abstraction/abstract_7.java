
/*
 	Abstract Class cha object banvu shakat nahi.
*/

abstract class Baba{
	
	void getProperty(){
		
		System.out.println("Bunglow, Car");
	}
}

class Shashi extends Baba{
	
	void marry(){
		
		System.out.println("Kirti Sanon");
	}
}

class Demo{
	
	public static void main(String[] args){
		
		Shashi s = new Shashi();

		s.getProperty();
		s.marry();
	}
}

/*
 	Bunglow, Car
	Kirti Sanon
*/
