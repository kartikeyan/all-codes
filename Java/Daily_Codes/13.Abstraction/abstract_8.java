
/*
 	Abstract Class cha reference ani child cha object chalto.
*/

abstract class Baba{
	
	void getProperty(){
		
		System.out.println("Bunglow, Car");
	}
}

class Shashi extends Baba{
	
	void marry(){
		
		System.out.println("Kirti Sanon");
	}
}

class Demo{
	
	public static void main(String[] args){

		Baba b = new Shashi();

		b.getProperty();
		b.marry();
	}
}

/*
 	Bunglow, Car
*/
