
/*
 	1] Abstract Class :- 

		1] Java abstract class is a class that can not be initiated by itself, it needs to be subclassed by another class to use its properties. 
		2] An abstract class is declared using the “abstract” keyword in its class definition.

	
	2] In Java, the following some important observations about abstract classes are as follows:

		a] An instance of an abstract class can not be created.

    		b] Constructors are allowed.

    		c] We can have an abstract class without any abstract method.

    		d] There can be a final method in abstract class but any abstract method in class(abstract class) can not be declared as final  or in simpler terms final method can not be abstract itself as it will yield an error: “Illegal combination of modifiers: abstract and final”

   	 	e] We can define static methods in an abstract class

    		f] We can use the abstract keyword for declaring top-level classes (Outer class) as well as inner classes as abstract

    		g] If a class contains at least one abstract method then compulsory should declare a class as abstract 

    		h] If the Child class is unable to provide implementation to all abstract methods of the Parent class then we should declare that Child class as abstract so that the next level Child class should provide implementation to the remaining abstract method

*/

/*
 	Abstarct Class :-

		1] Abstract Class is a incomplete class.

		2] 0 to 100% abstraction can be achieved.

		3] Jar aplyala mahit ahe, ki parent class madla method "override" honar ahe, mag pan parent class madle method "abstract" banvu shakto, ani implement Child class madhe karu sahkto

		4] Jar ekada class abstract banvat asal, tar tya class cha "object" banat nahi.

		5] We can create "Reference for an abstract class".

		6] Abstract class la "Cosntructor asto".

		7] Types :-

			a] Concrete Method / Complete Method

			b] Abstract Method

*/

//	1 - Concrete Method / Complete Method

	void method(){
		
	}

//	2 - Abstract Method
	
	abstract method();
