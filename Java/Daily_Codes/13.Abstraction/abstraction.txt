

	A] Abstraction in Java

		1] Abstraction in Java refers to hiding the implementation details of a code and exposing only the necessary information to the user. 
	
		2] It provides the ability to simplify complex systems by ignoring irrelevant details and reducing complexity

		3] Java provides many in-built abstractions and few tools to create our own.

	B] Abstraction in Java can be achieved using the following tools it provides :

    		1] Abstract classes
    		2] Interfaces


		1] Abstract Class

			a] In Java, an abstract class is a class that cannot be instantiated on its own and is meant to be subclassed. 
			b] It provides a common base for subclasses to inherit method and field definitions, but allows for subclasses to provide their own implementations.

		2] Interface


			a] Interfaces are probably the most powerful tools to achieve abstraction in java.

			b] Since it’s just like an abstract class, it cannot be instantiated. 

			c] And since it actually doesn’t provide an implementation and it cannot be instantiated it can’t have a constructor either. 


