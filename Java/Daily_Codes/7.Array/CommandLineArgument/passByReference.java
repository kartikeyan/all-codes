
class Sol{
	
	static void fun(int arr[]){
		
		System.out.println(arr);

		for(int x : arr){
			System.out.println(x);
		}

		for(int i = 0; i < arr.length; i++){
			arr[i] = arr[i] + 10;
		}
	}

	public static void main(String[] s){
		
		int arr[] = {1,2,3,4};

		fun(arr);

		System.out.println(arr);

		for(int x : arr){
			System.out.println(x);
		}
	}
}
