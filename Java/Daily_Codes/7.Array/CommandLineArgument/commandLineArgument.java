
class Sol{
	
	public static void main(String ...args){
		
		for(int i = 0; i < args.length; i++){
			
			System.out.println(args[i]);
		}
	}
}


/*
 	... (three dots) are called ellipsis or varargs 
 	They let you pass any number of objects of a specific type
 
	Ellipsis "..."

	The separator ... (ellipsis) is used only for varargs:

	int someMethod(int i, String s, int... k){  

		//k is an array with elements k[0], k[1], ...}

*/	
