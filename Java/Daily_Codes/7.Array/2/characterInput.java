
import java.io.*;

class Demo{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());
		System.out.println("Array Size is " + size);
		
		char arr[] = new char[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < size; i++){
			
			arr[i] = br.readLine().charAt(0);
		}

		System.out.println("Array Elements Are");
		for(int i = 0; i < size; i++){
			
			System.out.println(arr[i]);
		}
	}
}

//	Interger sodun ka pan input dila tar NumberFormatException yeto

/*
 	Enter Array Size
	a
Exception in thread "main" java.lang.NumberFormatException: For input string: "a"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.base/java.lang.Integer.parseInt(Integer.java:652)
	at java.base/java.lang.Integer.parseInt(Integer.java:770)
	at Demo.main(inputFromUser_BufferedReader.java:11)

katikeyan@katikeyan:~/javacodes/Codes/7.Array/2$ java Demo 
Enter Array Size
1.1
Exception in thread "main" java.lang.NumberFormatException: For input string: "1.1"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.base/java.lang.Integer.parseInt(Integer.java:652)
	at java.base/java.lang.Integer.parseInt(Integer.java:770)
	at Demo.main(inputFromUser_BufferedReader.java:11)

katikeyan@katikeyan:~/javacodes/Codes/7.Array/2$ java Demo 
Enter Array Size
5
Array Size is 5
Enter Array Elements
1
2
a
Exception in thread "main" java.lang.NumberFormatException: For input string: "a"
	at java.base/java.lang.NumberFormatException.forInputString(NumberFormatException.java:65)
	at java.base/java.lang.Integer.parseInt(Integer.java:652)
	at java.base/java.lang.Integer.parseInt(Integer.java:770)
	at Demo.main(inputFromUser_BufferedReader.java:19)
*/
 
