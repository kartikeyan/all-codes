
class Demo{
	
	public static void main(String[] args){
		
		//1
		int arr[] = new int[]{1,2,3,4};
		int size1 = arr.length;

		for(int i = 0; i < size1; i++){
			
			System.out.println(arr[i]);
		}

		//2
		char arr2[] = {'A','B','C'};
		int size2 = arr2.length;

		for(int i = 0; i < size2; i++){
			
			System.out.println(arr2[i]);
		}

		//3
		float arr3[] = new float[]{1.1f,1.1f,1.1f};
		int size3 = arr3.length;

		for(int i = 0; i < size3; i++){
			
			System.out.println(arr3[i]);
		}

		//4
		boolean arr4[] = new boolean[]{true,false};
		int size4 = arr4.length;

		for(int i = 0; i < size4; i++){
			
			System.out.println(arr4[i]);
		}

	}
}

/*
 	declaration1.java:20: error: array creation with both dimension expression and initialization is illegal
		int arr4[] = new int[4]{2,2,2,2};
		                       ^
	1 error
*/
