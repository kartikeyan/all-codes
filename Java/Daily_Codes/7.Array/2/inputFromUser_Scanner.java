
import java.util.*;

class Demo{
	
	public static void main(String[] s){
		
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Enter Array Size");
		int  size = sc.nextInt();		
		System.out.println("Array Size is : " + size);

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < size; i++){
			
			arr[i] = sc.nextInt();
		}

		System.out.println("Array Elements are");
		for(int i = 0; i < size; i++){
			
			System.out.println(arr[i]);	
		}

	}
}

//	If input is other than int

/*
 	Enter Array Size
	a
	Exception in thread "main" java.util.InputMismatchException
	at java.base/java.util.Scanner.throwFor(Scanner.java:939)
	at java.base/java.util.Scanner.next(Scanner.java:1594)
	at java.base/java.util.Scanner.nextInt(Scanner.java:2258)
	at java.base/java.util.Scanner.nextInt(Scanner.java:2212)
	at Demo.main(inputFromUser_Scanner.java:11)
*/
