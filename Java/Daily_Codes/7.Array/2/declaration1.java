
class Demo{
	
	public static void main(String[] args){
		
		//1
		int arr[] = new int[4];

		arr[0] = 10;
		arr[1] = 20;
		arr[2] = 30;

		//2
		int arr2[] = {50,60,70};

		//3
		int arr3[] = new int[]{1,1,1,1};

		// Error
		int arr4[] = new int[4]{2,2,2,2};	// size kiva initialiser list due shakto fakt
	}
}

/*
 	declaration1.java:20: error: array creation with both dimension expression and initialization is illegal
		int arr4[] = new int[4]{2,2,2,2};
		                       ^
	1 error
*/
