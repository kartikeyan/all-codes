
class Sol{
	
	public static void main(String[] s){
		
		int arr[][] = {{1,2,3},{4,5},{6}};

		for(int i = 0; i < arr.length; i++){	// arr.length returns the NUMBER OF ROWS in the 2D array
			
			for(int j = 0; j < arr[i].length; j++){	// arr[i].length returns the number of elements of 1D Array
				
				System.out.print(arr[i][j] + " ");
			}

			System.out.println();
		}
	}
}
