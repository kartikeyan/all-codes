
class Demo{

	public static void main(String[] s){
		
		int arr1[] = {1,2,3,4,5};		// ha array pan internally new array la call karto
		int arr2[] = new int[]{1,2,3,4,5};

		for(int i = 0; i < 5; i++){
			
			System.out.println(arr1[i]);
		}

		for(int i = 0; i < 5; i++){
			
			System.out.println(arr2[i]);
		}
	}
}

/*	
 *	javap -c Demo.class
 *
 	katikeyan@katikeyan:~/javacodes/Codes/7.Array/1$ javap -c Demo.class
	Compiled from "p8.java"
	class Demo {
	  Demo();
	    Code:
	       0: aload_0
	       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
	       4: return

	  public static void main(java.lang.String[]);
	    Code:
	       0: iconst_5
	       1: newarray       int
	       3: dup
	       4: iconst_0
	       5: iconst_1
	       6: iastore
	       7: dup
	       8: iconst_1
	       9: iconst_2
	      10: iastore
	      11: dup
	      12: iconst_2
     	      13: iconst_3
	      14: iastore
	      15: dup
	      16: iconst_3
	      17: iconst_4
	      18: iastore
	      19: dup
	      20: iconst_4
	      21: iconst_5
	      22: iastore
	      23: astore_1
	      24: iconst_5
	      25: newarray       int
	      27: dup
	      28: iconst_0
	      29: iconst_1
	      30: iastore
	      31: dup
	      32: iconst_1
	      33: iconst_2
	      34: iastore
	      35: dup
	      36: iconst_2
  	      37: iconst_3
	      38: iastore
	      39: dup
	      40: iconst_3
	      41: iconst_4
	      42: iastore
	      43: dup
	      44: iconst_4
	      45: iconst_5
	      46: iastore
	      47: astore_2
	      48: iconst_0
	      49: istore_3
	      50: iload_3
	      51: iconst_5
	      52: if_icmpge     70
	      55: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
	      58: aload_1
	      59: iload_3
	      60: iaload
	      61: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
	      64: iinc          3, 1
     	      67: goto          50
	      70: iconst_0
	      71: istore_3
	      72: iload_3
	      73: iconst_5
	      74: if_icmpge     92
	      77: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
	      80: aload_2
	      81: iload_3
	      82: iaload
	      83: invokevirtual #3                  // Method java/io/PrintStream.println:(I)V
	      86: iinc          3, 1
	      89: goto          72
	      92: return
}


*/


