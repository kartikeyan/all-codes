
class Demo{
	
	int a = 10;				// global variable

	public static void main(String[] s){
		
		int b = 20;			// local variable

		System.out.println(a);

		System.out.println(b);
	}
}

/*
 	p1.java:10: error: non-static variable a cannot be referenced from a static context
		System.out.println(a);
		                   ^
	1 error
*/
