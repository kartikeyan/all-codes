
import java.io.*;

class Demo{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		int sum = 0;
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < size; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			sum += arr[i];
		}

		System.out.println("Sum is " + sum);
	}
}
