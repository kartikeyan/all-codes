
/*
 	Question] Jar don(2) arrays ahet, array madle elements pan same ahet, tar tyana heap section madhe
		  same object miltil ka navin milel ?

	Ans ] Dongana(2) navin object miltin, pan array madhle elements che addresses same astil

	We can check using identityHashCode

	It is only valid for Integers and Characters

	It is only valid if it is in range of -128 to 127
*/	  

class Demo{
	
	public static void main(String[] s){
		
		int arr[] = {1,2,128,129};

		int arr1[] = {1,2,128,129};

		System.out.println(arr);
		System.out.println(arr1);

		System.out.println(System.identityHashCode(arr[0]));

		System.out.println(System.identityHashCode(arr1[0]));

		System.out.println(System.identityHashCode(arr[1]));

		System.out.println(System.identityHashCode(arr1[1]));

		System.out.println(System.identityHashCode(arr[2]));

		System.out.println(System.identityHashCode(arr1[2]));

		System.out.println(System.identityHashCode(arr[3]));

		System.out.println(System.identityHashCode(arr1[3]));
	}
}

//	IdentityHashCode will be different for arrays, it will be only be equal for array elements if they are same and in range -128 to 127

/*
	[I@d716361
	[I@6ff3c5b5

	217146947
	217146947

	1174888669
	1174888669

	929338653
	1259475182
			Different because there are not in range of -128 to 127
	1300109446
	1020371697
*/


