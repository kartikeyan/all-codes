
/*
 	Question] Jar don(2) arrays ahet, array madle elements pan same ahet, tar tyana heap section madhe
		  same object miltil ka navin milel ?

	Ans ] Dongana(2) navin object miltin, pan array madhle elements che addresses same astil

	We can check using identityHashCode
*/	  

class Demo{
	
	public static void main(String[] s){
		
		int arr[] = {1,2,3,4};

		int arr1[] = {1,2,3,4};

		System.out.println(arr);
		System.out.println(arr1);

		System.out.println(System.identityHashCode(arr));

		System.out.println(System.identityHashCode(arr1));
	}
}

//	IdentityHashCode will be different for arrays, it will be only be equal for array elements if they are same and in range -128 to 127

/*
	[I@d716361
	[I@6ff3c5b5
	225534817
	1878246837
*/


