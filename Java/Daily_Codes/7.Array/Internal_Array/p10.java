
/*
 	Question] Jar don(2) arrays ahet, array madle elements pan same ahet, tar tyana heap section madhe
		  same object miltil ka navin milel ?

	Ans ] Dongana(2) navin object miltin, pan array madhle elements che addresses same astil

	We can check using identityHashCode

	It is only valid for Integers and Characters

	It is only valid if it is in range of -128 to 127
*/	  

class Demo{
	
	public static void main(String[] s){
		
		char arr[] = {'a','b','c','d'};

		char arr1[] = {'a','b','c','d','e'};

		System.out.println(arr);

		System.out.println(arr1);
	}
}

//	IdentityHashCode will be different for arrays, it will be only be equal for array elements if they are same and in range -128 to 127

/*
	abcd
	abcde

	225534817
	225534817

	1878246837
	1878246837

	929338653
	929338653

	1259475182
	1259475182
*/


