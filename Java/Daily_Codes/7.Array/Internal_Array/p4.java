
class Demo{
	
	public static void main(String[] s){
		
		double arr[] = {1.1,2.2,3.3,4.4};

		System.out.println(arr);
	}
}

//	[D@d716361   == [ denotes class, D denotes Double

/*	

Compiled from "p4.java"
class Demo {
  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_4
       1: newarray       double
       3: dup
       4: iconst_0
       5: ldc2_w        #2                  // double 1.1d
       8: dastore
       9: dup
      10: iconst_1
      11: ldc2_w        #4                  // double 2.2d
      14: dastore
      15: dup
      16: iconst_2
      17: ldc2_w        #6                  // double 3.3d
      20: dastore
      21: dup
      22: iconst_3
      23: ldc2_w        #8                  // double 4.4d
      26: dastore
      27: astore_1
      28: getstatic     #10                 // Field java/lang/System.out:Ljava/io/PrintStream;
      31: aload_1
      32: invokevirtual #11                 // Method java/io/PrintStream.println:(Ljava/lang/Object;)V
      35: return
}


*/
