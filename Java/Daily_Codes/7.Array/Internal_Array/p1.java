
class Demo{
	
	public static void main(String[] s){
		
		int arr[] = {1,2,3,4};

		System.out.println(arr);
	}
}

//	[I@d716361   == [ denotes class, I denotes Integer

/*	

Compiled from "p1.java"
class Demo {
  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_4
       1: newarray       int
       3: dup
       4: iconst_0
       5: iconst_1
       6: iastore
       7: dup
       8: iconst_1
       9: iconst_2
      10: iastore
      11: dup
      12: iconst_2
      13: iconst_3
      14: iastore
      15: dup
      16: iconst_3
      17: iconst_4
      18: iastore
      19: astore_1
      20: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      23: aload_1
      24: invokevirtual #3                  // Method java/io/PrintStream.println:(Ljava/lang/Object;)V
      27: return
}

*/
