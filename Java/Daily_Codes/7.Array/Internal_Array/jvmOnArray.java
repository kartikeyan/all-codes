
class Demo{

	void fun(int arr[]){
		
		arr[1] = 70;
		arr[2] = 80;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5};

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(System.identityHashCode(arr[i]));
		}

		Demo obj = new Demo();

		obj.fun(arr);

		for(int i : arr){
			
			System.out.print(i + " ");
		}

		for(int i = 0; i < arr.length; i++){
			
			System.out.println(System.identityHashCode(arr[i]));
		}

		int x = 70;
		int y = 80;


		System.out.println(System.identityHashCode(x));
		
		System.out.println(System.identityHashCode(y));
	}
}
