
class Demo{
	
	public static void main(String[] s){
		
		float arr[] = {1.1f,2.2f,3.3f,4.4f};

		System.out.println(arr);
	}
}

//	[F@d716361   == [ denotes class, F denotes Float

/*	

Compiled from "p2.java"
class Demo {
  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_4
       1: newarray       float
       3: dup
       4: iconst_0
       5: ldc           #2                  // float 1.1f
       7: fastore
       8: dup
       9: iconst_1
      10: ldc           #3                  // float 2.2f
      12: fastore
      13: dup
      14: iconst_2
      15: ldc           #4                  // float 3.3f
      17: fastore
      18: dup
      19: iconst_3
      20: ldc           #5                  // float 4.4f
      22: fastore
      23: astore_1
      24: getstatic     #6                  // Field java/lang/System.out:Ljava/io/PrintStream;
      27: aload_1
      28: invokevirtual #7                  // Method java/io/PrintStream.println:(Ljava/lang/Object;)V
      31: return
}

*/
