
/*
 	Question] Jar don(2) arrays ahet, array madle elements pan same ahet, tar tyana heap section madhe
		  same object miltil ka navin milel ?

	Ans ] Dongana(2) navin object miltin, pan array madhle elements che addresses same astil

	We can check using identityHashCode

	It is only valid for Integers and Characters

	It is only valid if it is in range of -128 to 127
*/	  

class Demo{
	
	public static void main(String[] s){
		
		double arr[] = {1.1, 2.2, 3.3, 4.4};

		double arr1[] = {1.1, 2.2, 3.3, 4.4};


		System.out.println(arr);
		System.out.println(arr1);

		System.out.println(System.identityHashCode(arr[0]));

		System.out.println(System.identityHashCode(arr1[0]));

		System.out.println(System.identityHashCode(arr[1]));

		System.out.println(System.identityHashCode(arr1[1]));

		System.out.println(System.identityHashCode(arr[2]));

		System.out.println(System.identityHashCode(arr1[2]));

		System.out.println(System.identityHashCode(arr[3]));

		System.out.println(System.identityHashCode(arr1[3]));
	}
}

//	IdentityHashCode will be different for arrays, it will be only be equal for array elements if they are same and in range -128 to 127

/*
 	[D@d716361
	[D@6ff3c5b5

	929338653
	1259475182

	1300109446
	1020371697

	789451787
	1950409828

	1229416514
	2016447921
*/


