
class Demo{
	
	public static void main(String[] s){
		
		boolean arr[] = {true,false};

		System.out.println(arr);
	}
}

//	[Z@d716361

//	[Z@d716361   == [ denotes class, Z denotes Boolean

/*	

Compiled from "p3.java"
class Demo {
  Demo();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_2
       1: newarray       boolean
       3: dup
       4: iconst_0
       5: iconst_1
       6: bastore
       7: dup
       8: iconst_1
       9: iconst_0
      10: bastore
      11: astore_1
      12: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      15: aload_1
      16: invokevirtual #3                  // Method java/io/PrintStream.println:(Ljava/lang/Object;)V
      19: return
}


*/
