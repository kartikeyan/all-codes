
class Sol{
	
	public static void main(String[] s){
		
		int arr1[][] = {{},{},{}}; 		// complete declaration and internally it goes as arr1[3][0]

		int arr2[][] = new int[2][];		// There is scope that u can change the columns in the array

		System.out.println(arr1.length);	// 3

		System.out.println(arr1[0].length);	// 0

		System.out.println(arr2.length);	// 2

		System.out.println(arr2[0].length);	// Null Pointer Exception
	}
}

/*
   	3
	0
	2
	Exception in thread "main" java.lang.NullPointerException
		at Sol.main(nullPointerException.java:16)
*/
