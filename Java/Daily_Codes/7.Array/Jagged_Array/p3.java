
/*
 	
   	A jagged array is an array of arrays such that member arrays can be of different sizes, i.e., we can create a 2-D array but with a variable number of columns in each row. These types of arrays are also known as Jagged arrays. 

	

	Declaration and Initialization of Jagged array :

Syntax: data_type array_name[][] = new data_type[n][];  //n: no. of rows
             array_name[] = new data_type[n1] //n1= no. of columns in row-1
             array_name[] = new data_type[n2] //n2= no. of columns in row-2
             array_name[] = new data_type[n3] //n3= no. of columns in row-3
                                   .
                                   .
                                   .
             array_name[] = new data_type[nk]  //nk=no. of columns in row-n



Alternative, ways to Initialize a Jagged array :

                    int arr_name[][] = new int[][]  {
                                  new int[] {10, 20, 30 ,40},
                                  new int[] {50, 60, 70, 80, 90, 100},
                                  new int[] {110, 120}
                                      };

                              OR

                    int[][] arr_name = {
                          new int[] {10, 20, 30 ,40},
                          new int[] {50, 60, 70, 80, 90, 100},
                          new int[] {110, 120}
                              };

                              OR

                    int[][] arr_name = {
                           {10, 20, 30 ,40},
                           {50, 60, 70, 80, 90, 100},
                           {110, 120}
                              };

Following are Java programs to demonstrate the above concept.

*/

import java.io.*;

class Sol{
	
	public static void main(String[] s) throws IOException{

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		// Initialization has 2 ways == Type 2
		int arr[][] = new int[3][];
		
		// first row has 4 columns
		arr[0] = new int[4];

		// second row has 3 columns
		arr[1] = new int[3];

		// third row has 4 columns
		arr[2] = new int[4];

		
		for(int i = 0; i < arr.length; i++){
			
			for(int j = 0; j < arr[i].length; j++){
				
				arr[i][j] = Integer.parseInt(br.readLine());
			}
		}

		for(int[] x : arr){
			
			for(int y : x){
				
				System.out.print(y + " ");
			}

			System.out.println();
		}
	}
}

