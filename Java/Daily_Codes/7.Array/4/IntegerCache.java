
/*
 	 Integer Cache

		1] This feature was introduced in Java 5 in order to improve memory management. 
		
		2] It ranges from -128 to 127

		3] Integer Cache works only on autoboxing which means Conversion from a primitive type to an object reference is called autoboxing.

*/

class IntegerCache{
	
	public static void main(String[] s){
		
		int x = 10;
		int y = 10;

		Integer z = 10;

		System.out.println(System.identityHashCode(x));

		System.out.println(System.identityHashCode(y));

		System.out.println(System.identityHashCode(z));
	}
}

/*
 	1304217686
	1304217686
	1304217686
*/

/*	
 	Integer wrapper class cha source code
 
 	

	public final class Integer extends Number implements Comparable<Integer>
		{
		
			public static Integer valueOf(int val)
 	   		{
      				if (val < MIN_CACHE || val > MAX_CACHE)
	 			       return new Integer(val);
			 	       synchronized (intCache)

			 	if (intCache[val - MIN_CACHE] == null){
        				intCache[val - MIN_CACHE] = new Integer(val);
      					return intCache[val - MIN_CACHE];
        			}
   			}
		}
*/
