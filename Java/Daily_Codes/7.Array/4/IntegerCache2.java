
/*
 	Integer Cache


	Java Integer Cache Implementation:

		In Java 5, a new feature was introduced to save the memory and improve performance for Integer type objects handling. Integer objects are cached internally and reused via the same referenced objects.

	    	This is applicable for Integer values in the range between –128 to +127.
	    	This Integer caching works only on auto-boxing. Integer objects will not be cached when they are built using the constructor.

	Autoboxing :  

		This is equal to using the valueOf() as follows: 

		Integer a=10; // this is autoboxing
		Integer b==new Integer(40); // under the hood

	IntegerCache Class:

		IntegerCache is a private, static, and inner class of Java. As Java maintains standards and this is also nicely documented and gives complete information. Below given is the internal implementation for IntegerCache. Now, you will see the internal implementation logic for IntegerCache in java.

		In the below-given program var0,var1,var2,var3, and high for the range to check the values for IntegerCache, and by checking you can also see the range values for IntegerCache such that class is for a cache of values between -128 and 127. 

		It will help you when you will check any object reference values. And if the value will be in range declaration then the object reference for both values will be the same and if it is not in range declaration then the object reference will be different. So, if you will compare values even if it is same and it might be chance and you have taken out of range values then, in that case, both values even if it will same but will return false means it’s not equal because for both values object reference will be different.

*/

class IntegerCache{
	
	public static void main(String[] s){
		
		int x = 10;
		int y = 10;

		Integer z = 10;

		System.out.println(System.identityHashCode(x));

		System.out.println(System.identityHashCode(y));

		System.out.println(System.identityHashCode(z));

		if(x == y && y == z){
			
			System.out.println("x == y == z");
		}else{
			System.out.println("not Equal");
		}
		
	// Initializing variable a and b.
        // Java keeps a integer cache of integer
        // instances in range of -128 to 127.
        // Integer Cache works only on autoboxing.
        // Conversion from primitive type to object
        // reference is called autoboxing.
 
        // In range declaration (-128 to 127) then
        // object reference will be same.

		Integer a = 9;
		Integer b = 9;

		if(a == b){
			System.out.println("a == b");	
		}else{
			System.out.println("Not Equal");
		}



		// Not in range declaration (-128 to 127) then
	        // then object reference will not be same in this
	        // case.
        	Integer p = 396;
	        Integer q = 396;

        	// Then it will be false because both value of
	        // a and b will point to the different
	        // memory location.
        	if (p == q) {
	        	System.out.println("p == q");
        	}
        	else{
        	  	System.out.println("p != q");
	        }
	}
}

/*
 	1304217686
	1304217686
	1304217686
*/

/*	
 	Integer wrapper class cha source code
 
 	

	public final class Integer extends Number implements Comparable<Integer>{

	  	public static final int MIN_VALUE = 0x80000000;

		public static final int MAX_VALUE = 0x7fffffff;
		
			public static Integer valueOf(int val)
 	   		{
      				if (val < MIN_CACHE || val > MAX_CACHE)
	 			       return new Integer(val);
			 	       synchronized (intCache)

			 	if (intCache[val - MIN_CACHE] == null){
        				intCache[val - MIN_CACHE] = new Integer(val);
      					return intCache[val - MIN_CACHE];
        			}
   			}
		}
*/
