
/*
 	katikeyan@kartikeyan:~/javacodes/Codes/20_Lambda_Expressions$ javap java.lang.Iterable
	Compiled from "Iterable.java"
	public interface java.lang.Iterable<T> {
	  public abstract java.util.Iterator<T> iterator();
	  public default void forEach(java.util.function.Consumer<? super T>);
	  public default java.util.Spliterator<T> spliterator();
	}
*/

import java.util.*;

class forEachDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(30);

		al.add(20);

		al.add(40);
		
		al.forEach(
			
			(data) -> System.out.println(data)
		);
	}	
}

/*
 	10
	30
	20
	40
*/
