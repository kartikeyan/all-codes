
/*
 	 1] Year2023.class

	 2] Core2Web.class                 
	 
	 Take a look, here only 2 classes are created unlike annonymous inner class where 3 classes are created. Creation of class is heavy operation that is why lambda expression is used.
*/

interface Core2Web{
	
	String lang();
}

class Year2023{
	
	public static void main(String[] args){
		
		Core2Web c2w = () -> {
			
			return "C/CPP/Java/OS";	
		};

		System.out.println(c2w.lang());
	}
}

//	C/CPP/Java/OS

/*
 	() is argument list and " -> " are Arrow token "{}" is Body of lambda expression
*/
