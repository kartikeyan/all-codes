
/*
 	 1] Year2023.class

	 2] Core2Web.class                 
	 
	 3] 'Year2023$1.class'
*/

interface Core2Web{
	
	void lang();
}

class Year2023{
	
	public static void main(String[] args){
		
		Core2Web c2w = new Core2Web(){	// internally Core2Web c2w = new Year2023$1() mahnun jato
						// apan ithe interface cha object banvat nahiye
			public void lang(){
				
				System.out.println("C/CPP/Java/OS");	
			}
		};

		c2w.lang();
	}
}

//	C/CPP/Java/OS
