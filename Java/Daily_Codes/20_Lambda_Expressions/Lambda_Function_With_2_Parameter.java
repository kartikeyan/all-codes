
/*
 	2. Lambda Expression with Single parameter

	(p) -> System.out.println("One parameter: " + p);

	It is not mandatory to use parentheses if the type of that variable can be inferred from the context

*/

interface Core2Web{
	
	String lang(String numCourse);
}

class Year2023{
	
	public static void main(String[] args){
			
		Core2Web c2w = (xyz) -> "Bootcamp/Java/OS" + ":" + xyz;	 // Dataype nahi dila tari pan chalel, compiler samjun deto

		System.out.println(c2w.lang("Python"));
	}
}

//	Bootcamp/Java/OS:Python
