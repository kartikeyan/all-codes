
//	Jar User Defined Class ahe, tar tyachyat unique data nasto karan object unique kasto

import java.util.*;

class CricPlayer{

	int jerNo = 0;

	String pName = null;

	CricPlayer(int jerNo, String pName){
		
		this.jerNo = jerNo;
		this.pName = pName;
	}

	public String toString(){
		
		return jerNo + " : " + pName;
	}
}

class HashSetDemo{
	
	public static void main(String[] args){
		
		LinkedHashSet hs = new LinkedHashSet();

		hs.add(new CricPlayer(18, "Virat"));
		hs.add(new CricPlayer(7, "MS"));
		hs.add(new CricPlayer(45, "Rohit"));
		hs.add(new CricPlayer(7, "MS"));

		System.out.println(hs);
	}
}

//	[18 : Virat, 7 : MS, 45 : Rohit, 7 : MS]
