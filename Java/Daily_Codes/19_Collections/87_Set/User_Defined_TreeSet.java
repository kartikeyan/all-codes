/*
 	Compiled from "Comparable.java"
	public interface java.lang.Comparable<T> {
	  public abstract int compareTo(T);
	}
*/

import java.util.*;

class MyClass implements Comparable<MyClass>{
	
	String str = null;

	MyClass(String str){
		
		this.str = str;
	}

	public int compareTo(MyClass obj){
		
//		return obj.str.compareTo(this.str);	//  [Rahul, Kanha, Badhe, Ashish]

		return this.str.compareTo(obj.str);	// [Ashish, Badhe, Kanha, Rahul]



	}

	public String toString(){
		
		return str;
	}
}

class TreeSetDemo{
	
	public static void main(String[] args){
		
		TreeSet ts = new TreeSet();

		ts.add(new MyClass("Kanha"));
		ts.add(new MyClass("Ashish"));
		ts.add(new MyClass("Rahul"));
		ts.add(new MyClass("Badhe"));

		System.out.println(ts);
	}
}
