
	1] PriorityBlockingQueue ==> Concurrency

	Unlike the PriorityQueue, the PriorityBlockingQueue is thread-safe.

	All of the PriorityBlockingQueue’s operations are guaranteed to be side-effect-free when it comes to race conditions or other concurrency issues. This makes the PriorityBlockingQueue the perfect data structure for production, Multithreaded Java application.

  	2] PriorityBlockingQueue - Unbound Capacity and Blocking

	The PriorityBlockingQueue class is an implementation of the BlockingQueue interface in Java. 

	In a typical producer-consumer problem in a multithreaded environment, the PriorityBlockingQueue will block the producer from adding more elements to it if the queue is full.  However, since the PriorityBlockingQueue is an unbound queue, meaning that it does not have a maximum capacity, the producers will never be blocked from adding more elements to the queue. 

	On the other hand, when the PriorityBlockingQueue becomes empty, consumer threads will be blocked until additional items are added to the queue.

	  3] PriorityBlockingQueue - Memory Efficiency and Performance

	The PriorityBlockingQueue is backed by an array-based binary tree heap - which is extremely memory efficient. This provides our application with high performance and reduces the memory overhead associated with reference-based data structures.

	However despite its array-based implementation, as we mentioned earlier, the PriorityBlockingQueue is an unbounded collection and it will grow dynamically as the elements get added to it.

	The default initial capacity for PriorityBlockingQueue is 11, but it can easily be changed by using the appropriate constructor.

  	4] PriorityBlockingQueue - Ordering Of Elements

	By default, the PriorityBlockingQueue is following the standard min-heap properties which mean the head of the PriorityBlockingQueue will have the smallest element based on natural ordering.

	Objects in the PriorityBlockingQueue must be comparable for natural ordering, or the  ClassCastException will be thrown.

	For additional flexibility when it comes to the ordering of elements,  a Comparator can be used.
