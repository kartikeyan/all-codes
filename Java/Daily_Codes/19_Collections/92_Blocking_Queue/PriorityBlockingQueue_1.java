
/*
 	Compiled from "PriorityBlockingQueue.java"
public class java.util.concurrent.PriorityBlockingQueue<E> extends java.util.AbstractQueue<E> implements java.util.concurrent.BlockingQueue<E>, java.io.Serializable {
	  public java.util.concurrent.PriorityBlockingQueue();
	  public java.util.concurrent.PriorityBlockingQueue(int);
	  public java.util.concurrent.PriorityBlockingQueue(int, java.util.Comparator<? super E>);
	  public java.util.concurrent.PriorityBlockingQueue(java.util.Collection<? extends E>);
	  public boolean add(E);
	  public boolean offer(E);
	  public void put(E);
	  public boolean offer(E, long, java.util.concurrent.TimeUnit);
	  public E poll();
	  public E take() throws java.lang.InterruptedException;
	  public E poll(long, java.util.concurrent.TimeUnit) throws java.lang.InterruptedException;
	  public E peek();
	  public java.util.Comparator<? super E> comparator();
	  public int size();
	  public int remainingCapacity();
	  public boolean remove(java.lang.Object);
	  void removeEq(java.lang.Object);
	  public boolean contains(java.lang.Object);
	  public java.lang.String toString();
	  public int drainTo(java.util.Collection<? super E>);
	  public int drainTo(java.util.Collection<? super E>, int);
	  public void clear();
	  public java.lang.Object[] toArray();
	  public <T> T[] toArray(T[]);
	  public java.util.Iterator<E> iterator();
	  public java.util.Spliterator<E> spliterator();
	  public boolean removeIf(java.util.function.Predicate<? super E>);
	  public boolean removeAll(java.util.Collection<?>);
	  public boolean retainAll(java.util.Collection<?>);
	  public void forEach(java.util.function.Consumer<? super E>);
	  static {};
	}
*/

//	Same as ArrayBlockingQueue

/*
 	Only Difference is
	
	          public java.util.concurrent.PriorityBlockingQueue();
*/

import java.util.concurrent.*;

class PriorityBlockingQueueDemo{
	
	public static void main(String[] args){
			
		PriorityBlockingQueue pq = new PriorityBlockingQueue(3);

		pq.offer(10);

		pq.offer(20);

		pq.offer(30);
	
		System.out.println(pq);

		pq.offer(40);

		System.out.println(pq);
	}	
}

/*
 	[10, 20, 30]
	[10, 20, 30, 40]
*/
