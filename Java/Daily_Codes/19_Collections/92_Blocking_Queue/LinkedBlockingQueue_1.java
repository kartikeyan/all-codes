
/*
 	katikeyan@kartikeyan:~/javacodes/Codes/19_Collections/92_Blocking_Queue$ javap java.util.concurrent.LinkedBlockingQueue
	Compiled from "LinkedBlockingQueue.java"
	public class java.util.concurrent.LinkedBlockingQueue<E> extends java.util.AbstractQueue<E> implements java.util.concurrent.BlockingQueue<E>, java.io.Serializable {
	  transient java.util.concurrent.LinkedBlockingQueue$Node<E> head;
	  void fullyLock();
	  void fullyUnlock();
	  public java.util.concurrent.LinkedBlockingQueue();
	  public java.util.concurrent.LinkedBlockingQueue(int);
	  public java.util.concurrent.LinkedBlockingQueue(java.util.Collection<? extends E>);
	  public int size();
	  public int remainingCapacity();
	  public void put(E) throws java.lang.InterruptedException;
	  public boolean offer(E, long, java.util.concurrent.TimeUnit) throws java.lang.InterruptedException;
	  public boolean offer(E);
	  public E take() throws java.lang.InterruptedException;
	  public E poll(long, java.util.concurrent.TimeUnit) throws java.lang.InterruptedException;
	  public E poll();
	  public E peek();
	  void unlink(java.util.concurrent.LinkedBlockingQueue$Node<E>, java.util.concurrent.LinkedBlockingQueue$Node<E>);
	  public boolean remove(java.lang.Object);
	  public boolean contains(java.lang.Object);
	  public java.lang.Object[] toArray();
	  public <T> T[] toArray(T[]);
	  public java.lang.String toString();
	  public void clear();
	  public int drainTo(java.util.Collection<? super E>);
	  public int drainTo(java.util.Collection<? super E>, int);
	  java.util.concurrent.LinkedBlockingQueue$Node<E> succ(java.util.concurrent.LinkedBlockingQueue$Node<E>);
	  public java.util.Iterator<E> iterator();
	  public java.util.Spliterator<E> spliterator();
	  public void forEach(java.util.function.Consumer<? super E>);
	  void forEachFrom(java.util.function.Consumer<? super E>, java.util.concurrent.LinkedBlockingQueue$Node<E>);
	  public boolean removeIf(java.util.function.Predicate<? super E>);
	  public boolean removeAll(java.util.Collection<?>);
	  public boolean retainAll(java.util.Collection<?>);
	  java.util.concurrent.LinkedBlockingQueue$Node<E> findPred(java.util.concurrent.LinkedBlockingQueue$Node<E>, java.util.concurrent.LinkedBlockingQueue$Node<E>);
	}
*/

//	Same as ArrayBlockingQueue
