
import java.util.concurrent.*;

class Producer implements Runnable{
	
	BlockingQueue bq = null;

	Producer(BlockingQueue bq){
		
		this.bq = bq;
	}

	public void run(){
		
		for(int i = 1; i <= 10; i++){
			
			try{
				bq.put(i);

				System.out.println("Producer " + i);
			}catch(InterruptedException ie){
				
			}

			try{
				Thread.sleep(3000);
			}catch(InterruptedException ie){
				
			}
		}
	}
};

class Consumer implements Runnable{

        BlockingQueue bq = null;

        Consumer(BlockingQueue bq){

                this.bq = bq;
        }

        public void run(){

                for(int i = 1; i <= 10; i++){

                        try{
                                bq.take();

                                System.out.println("Consumer " + i);
                        }catch(InterruptedException ie){

                        }

                        try{
                                Thread.sleep(7000);
                        }catch(InterruptedException ie){

                        }
                }
        }
};

class ProducerConsumer{
	
	public static void main(String[] s){
		
		BlockingQueue bq = new ArrayBlockingQueue(3);

		Producer produce = new Producer(bq);

		Consumer consumer = new Consumer(bq);
		
		Thread prodThread = new Thread(produce);

		Thread consThread = new Thread(consumer);
		
		prodThread.start();

		consThread.start();
	}
}

/*
 	Producer 1
	Consumer 1
	Producer 2
	Producer 3
	Consumer 2
	Producer 4
	Producer 5
	Consumer 3
	Producer 6
	Consumer 4
	Producer 7
	Consumer 5
	Producer 8
	Consumer 6
	Producer 9
	Consumer 7
	Producer 10
	Consumer 8
	Consumer 9
	Consumer 10
*/
