
/*
 	katikeyan@kartikeyan:~/javacodes/Codes/19_Collections/92_Blocking_Queue$ javap java.util.concurrent.BlockingQueue
	Compiled from "BlockingQueue.java"
	public interface java.util.concurrent.BlockingQueue<E> extends java.util.Queue<E> {
	  public abstract boolean add(E);
	  public abstract boolean offer(E);
	  public abstract void put(E) throws java.lang.InterruptedException;
	  public abstract boolean offer(E, long, java.util.concurrent.TimeUnit) throws java.lang.InterruptedException;
	  public abstract E take() throws java.lang.InterruptedException;
	  public abstract E poll(long, java.util.concurrent.TimeUnit) throws java.lang.InterruptedException;
	  public abstract int remainingCapacity();
	  public abstract boolean remove(java.lang.Object);
	  public abstract boolean contains(java.lang.Object);
	  public abstract int drainTo(java.util.Collection<? super E>);
	  public abstract int drainTo(java.util.Collection<? super E>, int);
	}
*/

import java.util.concurrent.*;

class BlockingQueueDemo{
	
	public static void main(String[] args){
		
		BlockingQueue bq = new ArrayBlockingQueue(3);

		bq.offer(10);

		bq.offer(20);

		bq.offer(30);
		
		System.out.println(bq);		// [10,20,30]
		
		try{
			bq.put(40);	
		}catch(InterruptedException ie){
			
			System.out.println("Catch");
		}

		System.out.println(bq);
	}
}

/*
 	[10, 20, 30]
	cursor is waiting...................................
*/
