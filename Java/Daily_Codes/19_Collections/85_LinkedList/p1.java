
/*
 	1] public E getFirst()

	2] public E getLast()

	3] public E removeFirst()

	4] public E removeLast()

	5] public E addFirst()

	6] public E addLast()

	7] java.util.LinkedList$Node<E> node(int)

*/

import java.util.*;

class LinkedListDemo{
	
	public static void main(String[] args){
		
		LinkedList ll = new LinkedList();

		ll.add(20);

		ll.addFirst(10);

		ll.addLast(30);

		System.out.println(ll);
	}
}

/*
 	[10, 20, 30]
*/
