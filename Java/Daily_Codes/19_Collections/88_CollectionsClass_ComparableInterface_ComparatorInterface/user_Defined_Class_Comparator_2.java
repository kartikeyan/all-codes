/*
	A comparator interface is used to order the objects of user-defined classes. A comparator object is capable of comparing two objects of the same class. Following function compare obj1 with obj2.

	Syntax: 

	public int compare(Object obj1, Object obj2):

	Suppose we have an Array/ArrayList of our own class type, containing fields like roll no, name, address, DOB, etc, and we need to sort the array based on Roll no or name?

	Method 1: One obvious approach is to write our own sort() function using one of the standard algorithms. This solution requires rewriting the whole sorting code for different criteria like Roll No. and Name.

	Method 2: Using comparator interface- Comparator interface is used to order the objects of a user-defined class. This interface is present in java.util package and contains 2 methods compare(Object obj1, Object obj2) and equals(Object element). Using a comparator, we can sort the elements based on data members. For instance, it may be on roll no, name, age, or anything else.

	Method of Collections class for sorting List elements is used to sort the elements of List by the given comparator.

	public void sort(List list, ComparatorClass c)

*/

import java.util.*;

class Movies{
	
	String mName = null;
	
	double tColl = 0.0;

	float imdb = 0.0f;

	Movies(String mName, double tColl ,float imdb){
		
		this.mName = mName;
		
		this.tColl = tColl;

		this.imdb = imdb;
	}

	public String toString(){
		
		return "{" + mName + ":" + tColl + ":" + imdb + "}";
	}
}

class SortByName implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		return ((((Movies)obj1).mName.compareTo(((Movies)obj2).mName)));
	}
}

class SortByTColl implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		return (int) (((Movies)obj1).tColl - ((Movies)obj2).tColl);
	}
}

class SortByimdb implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		return (int) (((Movies)obj1).imdb - ((Movies)obj2).imdb);
	}
}

class ListSortDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(new Movies("OMG2", 2000.50, 8.5f));
		
		al.add(new Movies("Gadar 2", 3000.50, 9.5f));
		
		al.add(new Movies("Fast X", 7000.50, 9.1f));
		
		al.add(new Movies("John Wick", 9000.50, 9.6f));
		
		al.add(new Movies("Wall Street", 10000.50, 8.9f));
		
		System.out.println(al);

		Collections.sort(al, new SortByName());
		
		System.out.println(al);

		Collections.sort(al, new SortByTColl());
		
		System.out.println(al);

		Collections.sort(al, new SortByimdb());
		
		System.out.println(al);
	}
}

/*
 	[{OMG2:2000.5:8.5}, {Gadar 2:3000.5:9.5}, {Fast X:7000.5:9.1}, {John Wick:9000.5:9.6}, {Wall Street:10000.5:8.9}]
	[{Fast X:7000.5:9.1}, {Gadar 2:3000.5:9.5}, {John Wick:9000.5:9.6}, {OMG2:2000.5:8.5}, {Wall Street:10000.5:8.9}]
	[{OMG2:2000.5:8.5}, {Gadar 2:3000.5:9.5}, {Fast X:7000.5:9.1}, {John Wick:9000.5:9.6}, {Wall Street:10000.5:8.9}]
	[{OMG2:2000.5:8.5}, {Gadar 2:3000.5:9.5}, {Fast X:7000.5:9.1}, {John Wick:9000.5:9.6}, {Wall Street:10000.5:8.9}]
*/
