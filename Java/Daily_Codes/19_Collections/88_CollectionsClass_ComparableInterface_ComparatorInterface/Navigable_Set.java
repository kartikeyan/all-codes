
/*
 	Compiled from "NavigableSet.java"
	public interface java.util.NavigableSet<E> extends java.util.SortedSet<E> {
	  public abstract E lower(E);
	  public abstract E floor(E);
	  public abstract E ceiling(E);
	  public abstract E higher(E);
	  public abstract E pollFirst();
	  public abstract E pollLast();
	  public abstract java.util.Iterator<E> iterator();
	  public abstract java.util.NavigableSet<E> descendingSet();
	  public abstract java.util.Iterator<E> descendingIterator();
	  public abstract java.util.NavigableSet<E> subSet(E, boolean, E, boolean);
	  public abstract java.util.NavigableSet<E> headSet(E, boolean);
	  public abstract java.util.NavigableSet<E> tailSet(E, boolean);
	  public abstract java.util.SortedSet<E> subSet(E, E);
	  public abstract java.util.SortedSet<E> headSet(E);
	  public abstract java.util.SortedSet<E> tailSet(E);
	}
*/

import java.util.*;

class NavigableSetDemo{
	
	public static void main(String[] args){
	
		NavigableSet vs = new TreeSet();

		vs.add("Kanha");

		vs.add("Ashish");

		vs.add("Rajesh");

		vs.add("Badhe");

		vs.add("Rahul");

		System.out.println(vs);

		System.out.println(vs.lower("Rajesh"));

		System.out.println(vs.floor("Badhe"));

		System.out.println(vs.ceiling("Rajesh"));

		System.out.println(vs.higher("Rajesh"));

		System.out.println(vs.pollFirst());
		
		System.out.println(vs.pollLast());
	}
}

/*
 	[Ashish, Badhe, Kanha, Rahul, Rajesh]
	Rahul
	Badhe
	Rajesh
	null
	Ashish
	Rajesh
*/
