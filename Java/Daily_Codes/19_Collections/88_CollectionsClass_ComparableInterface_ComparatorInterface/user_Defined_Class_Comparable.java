
//	Comparable can only be used in Set

/*	1] The Comparable interface is used to compare an object of the same class with an instance of that class, it provides ordering of data for objects of the user-defined class. 
 
 	2]The class has to implement the java.lang.Comparable interface to compare its instance, it provides the compareTo method that takes a parameter of the object of that class.
*/


import java.util.*;

class Movies implements Comparable{
	
	String mName = null;

	float tolColl = 0.0f;

	Movies(String mName, float tolColl){
		
		this.mName = mName;

		this.tolColl = tolColl;
	}

	public int compareTo(Object obj){
		
		return mName.compareTo(((Movies)obj).mName);
	}

	public String toString(){
		
		return mName + ":" + tolColl;
	}
}

class TreeSetDemo{
	
	public static void main(String[] args){
		
		TreeSet ts = new TreeSet();

		ts.add(new Movies("Gadar2", 15.5f));

		ts.add(new Movies("OMG2", 5.5f));

		ts.add(new Movies("Fast X", 15.5f));

		ts.add(new Movies("Gadar2", 15.5f));
		
		System.out.println(ts);
	}
}

/*
 	[Fast X:15.5, Gadar2:15.5, OMG2:5.5]
*/
