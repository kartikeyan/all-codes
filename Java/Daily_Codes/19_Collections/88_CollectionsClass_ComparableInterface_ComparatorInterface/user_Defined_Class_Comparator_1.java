/*
	A comparator interface is used to order the objects of user-defined classes. A comparator object is capable of comparing two objects of the same class. Following function compare obj1 with obj2.

	Syntax: 

	public int compare(Object obj1, Object obj2):

	Suppose we have an Array/ArrayList of our own class type, containing fields like roll no, name, address, DOB, etc, and we need to sort the array based on Roll no or name?

	Method 1: One obvious approach is to write our own sort() function using one of the standard algorithms. This solution requires rewriting the whole sorting code for different criteria like Roll No. and Name.

	Method 2: Using comparator interface- Comparator interface is used to order the objects of a user-defined class. This interface is present in java.util package and contains 2 methods compare(Object obj1, Object obj2) and equals(Object element). Using a comparator, we can sort the elements based on data members. For instance, it may be on roll no, name, age, or anything else.

	Method of Collections class for sorting List elements is used to sort the elements of List by the given comparator.

	public void sort(List list, ComparatorClass c)

*/

import java.util.*;

class Employee{
	
	String empName = null;

	float sal = 0.0f;

	Employee(String empName, float sal){
		
		this.empName = empName;

		this.sal = sal;
	}

	public String toString(){
		
		return "{" + empName + ":" + sal + "}";
	}
}

class SortByName implements Comparator<Employee>{
	
	public int compare(Employee obj1, Employee obj2){
		
		return obj1.empName.compareTo(obj2.empName);
	}
}

class SortBySal implements Comparator<Employee>{
	
	public int compare(Employee obj1, Employee obj2){
		
		return (int) (obj1.sal - obj2.sal);
	}
}

class ListSortDemo{
	
	public static void main(String[] args){
		
		ArrayList<Employee> al = new ArrayList<Employee>();
		al.add(new Employee("Shree", 200000.50f));

		al.add(new Employee("Harshal", 100000.50f));

		al.add(new Employee("Kartik", 250000.50f));

		al.add(new Employee("Gaurav", 10000.50f));
		
		System.out.println(al);

		Collections.sort(al, new SortByName());
		
		System.out.println(al);

		Collections.sort(al, new SortBySal());
		
		System.out.println(al);
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/19_Collections/88_CollectionsClass_ComparableInterface_ComparatorInterface$ java ListSortDemo 
	[{Shree:200000.5}, {Harshal:100000.5}, {Kartik:250000.5}, {Gaurav:10000.5}]
	[{Gaurav:10000.5}, {Harshal:100000.5}, {Kartik:250000.5}, {Shree:200000.5}]
	[{Gaurav:10000.5}, {Harshal:100000.5}, {Shree:200000.5}, {Kartik:250000.5}]
*/
