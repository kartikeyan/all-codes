import java.util.*;

class CricPlayer{
	
	int jerNo = 0;

	String name = null;

	CricPlayer(int jerNo, String name){
		
		this.jerNo = jerNo;

		this.name = name;
	}

	public String toString(){
		
		return jerNo + " : " + name;
	}
}

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add("99 : Kartik"); 			// ithe toh internally toString() la call kartoy mahnun output yetoy

		al.add(new CricPlayer(18, "Virat"));

		al.add(new CricPlayer(45, "Rohit"));

		al.add(new CricPlayer(07, "Dhoni"));

		System.out.println(al);
	}
}

/*
 	[99 : Kartik, CricPlayer@6ff3c5b5, CricPlayer@3764951d, CricPlayer@4b1210ee]
*/
