
//	int lastIndexOf(object)

import java.util.ArrayList;

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(10.5f);

		al.add(10.5f);

		al.add(10);

		al.add("kot");

		System.out.println(al.lastIndexOf(10.5f));	// 2	

		System.out.println(al.lastIndexOf(10));		// 3
	}
}

