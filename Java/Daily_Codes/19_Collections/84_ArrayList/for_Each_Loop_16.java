
import java.util.*;

class ArrayListDemo{
		
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add("Kartik");

		al.add(10.5f);
		
		for(var obj : al){

			System.out.print(obj + " ");
		}

		System.out.println();
	}
}	

