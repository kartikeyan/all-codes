
//	E remove(index);

import java.util.ArrayList;

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(10.5f);

		al.add(10.5f);

		al.add(10);

		al.add("kot");

		System.out.println(al);

		al.remove(3);

		System.out.println(al);
	}
}

/*
 	[10, 10.5, 10.5, 10, kot]
	[10, 10.5, 10.5, kot]
*/
