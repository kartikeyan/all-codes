
//	int indexOf(object)

import java.util.ArrayList;

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(20.5);

		al.add(10.5f);

		al.add("Fun");

		al.add("kot");

		System.out.println(al.indexOf("Fun"));	// 3

		System.out.println(al.indexOf(10));	// 0
	}
}

