
//	protected void removeRange(int, int);

import java.util.ArrayList;

class ArrayListDemo extends ArrayList{
	
	public static void main(String[] args){
		
		ArrayListDemo al = new ArrayListDemo();

		al.add(10);

		al.add(10.5f);

		al.add(10.5f);

		al.add(10);

		al.add("kot");

		System.out.println(al);

		al.removeRange(3, 5);

		System.out.println(al);
	}
}

/*
 	[10, 10.5, 10.5, 10, kot]
	[10, 10.5, 10.5]
*/
