
//	void add(int, E);

import java.util.ArrayList;

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(20.5);

		al.add(10.5f);

		al.add("Fun");

		al.add("kot");

		System.out.println(al);
		
		al.add(5 ,"gun");

		System.out.println(al);
	}
}

/*
 	[10, 20.5, 10.5, Fun, kot]
	[10, 20.5, 10.5, Fun, kot, gun]
*/
