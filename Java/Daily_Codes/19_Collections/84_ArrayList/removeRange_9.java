
//	protected void removeRange(int, int);

import java.util.ArrayList;

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(10.5f);

		al.add(10.5f);

		al.add(10);

		al.add("kot");

		System.out.println(al);

		al.removeRange(3, 5);

		System.out.println(al);
	}
}

/*
 	removeRange_9.java:24: error: removeRange(int,int) has protected access in ArrayList
		al.removeRange(3, 5);
		  ^
	Note: removeRange_9.java uses unchecked or unsafe operations.
	Note: Recompile with -Xlint:unchecked for details.
	1 error
*/
