
//	object[] toArray();

import java.util.ArrayList;

class ArrayListDemo{
	
	public static void main(String[] args){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(10.5f);

		al.add(10.5f);

		al.add(10);

		al.add("kot");

		System.out.println(al);

		Object arr[] = al.toArray();

		System.out.println(arr);

		for(int i = 0; i < arr.length; i++){
			
			System.out.print(arr[i]);
		}

		System.out.println();
	}
}

/*
 	[10, 10.5, 10.5, 10, kot]
	[Ljava.lang.Object;@49476842
	1010.510.510kot
*/
