
import java.util.*;

class VectorDemo{
	
	public static void main(String[] s){
		
		Vector vc = new Vector();

		vc.addElement(10);

		vc.addElement(20);

		vc.addElement(30);
		
		System.out.println(vc);

		System.out.println(vc.capacity());

		vc.addElement(40);
		vc.addElement(50);
		vc.addElement(60);
		vc.addElement(70);
		vc.addElement(80);
		vc.addElement(90);
		vc.addElement(100);
		vc.addElement(110);
		
		System.out.println(vc);

		System.out.println(vc.capacity());
	}
}

/*
 	[10, 20, 30]
	10
	[10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110]
	20
*/
