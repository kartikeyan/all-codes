/*
 	Compiled from "Stack.java"
	public class java.util.Stack<E> extends java.util.Vector<E> {
	  public java.util.Stack();
	  public E push(E);
	  public synchronized E pop();
	  public synchronized E peek();
	  public boolean empty();
	  public synchronized int search(java.lang.Object);
	}
*/

import java.util.*;

class StackDemo{
	
	public static void main(String[] args){

		Stack s = new Stack();

		s.push(10);

		s.push(20);

		s.push(30);
		
		System.out.println(s);	// [10, 20, 30]	==> Insertion order preserve karto mahnun output asa ahe.
					// Pan top ithe 30 element var ahe

		System.out.println(s.pop());

		System.out.println(s.peek());

		System.out.println(s.empty());
		
		System.out.println(s);

		System.out.println(s.search(20));

	}
}

/*
 	[10, 20, 30]
	30
	20
	false
	[10, 20]
	1
*/
