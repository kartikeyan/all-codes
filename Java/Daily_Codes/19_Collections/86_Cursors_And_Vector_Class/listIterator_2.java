/*
 	getClass exists in Object class and in object class we have class named type Class
*/

import java.util.*;

class CursorDemo{
	
	public static void main(String[] s){
		
		ArrayList al = new ArrayList();

		al.add("kotgiri");

		al.add("Takade");

		al.add("Aditya");

		al.add("Abhinav");

		System.out.println(al);

		for(var obj : al){
			
			System.out.println(obj.getClass().getName());
		}

		ListIterator cursor = al.listIterator();
		
		System.out.println(cursor.getClass().getName());

		while(cursor.hasNext()){
			
			System.out.println(cursor.next());
		}

		while(cursor.hasPrevious()){
			
			System.out.println(cursor.previous());
		}
	}
}

/*
 	[kotgiri, Takade, Aditya, Abhinav]
	java.lang.String
	java.lang.String
	java.lang.String
	java.lang.String
	java.util.ArrayList$ListItr
	kotgiri
	Takade
	Aditya
	Abhinav
	Abhinav	
	Aditya
	Takade
	kotgiri
*/

/*
 	Compiled from "Iterator.java"
	public interface java.util.Iterator<E> {
	  public abstract boolean hasNext();
	  public abstract E next();
	  public default void remove();
	  public default void forEachRemaining(java.util.function.Consumer<? super E>);
	}
*/

/*
 	Compiled from "ListIterator.java"
	public interface java.util.ListIterator<E> extends java.util.Iterator<E> {
	  public abstract boolean hasNext();
	  public abstract E next();
	  public abstract boolean hasPrevious();
	  public abstract E previous();
	  public abstract int nextIndex();
	  public abstract int previousIndex();
	  public abstract void remove();
	  public abstract void set(E);
	  public abstract void add(E);
	}
*/

/*
 	Compiled from "Spliterator.java"
	public interface java.util.Spliterator<T> {
	  public static final int ORDERED;
	  public static final int DISTINCT;
	  public static final int SORTED;
	  public static final int SIZED;
	  public static final int NONNULL;
	  public static final int IMMUTABLE;
	  public static final int CONCURRENT;
	  public static final int SUBSIZED;
	  public abstract boolean tryAdvance(java.util.function.Consumer<? super T>);
	  public default void forEachRemaining(java.util.function.Consumer<? super T>);
	  public abstract java.util.Spliterator<T> trySplit();
	  public abstract long estimateSize();
	  public default long getExactSizeIfKnown();
	  public abstract int characteristics();
	  public default boolean hasCharacteristics(int);
	  public default java.util.Comparator<? super T> getComparator();
	}
*/
