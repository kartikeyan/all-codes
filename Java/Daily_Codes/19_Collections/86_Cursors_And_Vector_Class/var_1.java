
/*
 	getClass exists in Object class and in object class we have class named type Class
*/

import java.util.*;

class CursorDemo{
	
	public static void main(String[] s){
		
		ArrayList al = new ArrayList();

		al.add(10);

		al.add(20.5f);

		al.add(31.1);

		al.add("Kartik");

		System.out.println(al);

		for(var obj : al){
			
			System.out.println(obj.getClass().getName());
		}
	}
}

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/19_Collections/86_Cursors_And_Vector_Class$ javap java.lang.Object
	Compiled from "Object.java"
	public class java.lang.Object {
	  public java.lang.Object();
	  public final native java.lang.Class<?> getClass();
	  public native int hashCode();
	  public boolean equals(java.lang.Object);
	  protected native java.lang.Object clone() throws java.lang.CloneNotSupportedException;
	  public java.lang.String toString();
	  public final native void notify();
	  public final native void notifyAll();
	  public final void wait() throws java.lang.InterruptedException;
	  public final void wait(long) throws java.lang.InterruptedException;
	  public final void wait(long, int) throws java.lang.InterruptedException;
	  protected void finalize() throws java.lang.Throwable;
	}
*/
