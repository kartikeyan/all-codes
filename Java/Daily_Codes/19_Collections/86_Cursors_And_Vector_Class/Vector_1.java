/*
 	1] The Vector class implements a growable array of objects. Vectors fall in legacy classes, but now it is fully compatible with collections. It is found in java.util package and implement the List interface

	2] Vector implements a dynamic array which means it can grow or shrink as required. Like an array, it contains components that can be accessed using an integer index.

    	3] They are very similar to ArrayList, but Vector is synchronized and has some legacy methods that the collection framework does not contain.

    	4] It also maintains an insertion order like an ArrayList. Still, it is rarely used in a non-thread environment as it is synchronized, and due to this, it gives a poor performance in adding, searching, deleting, and updating its elements.

    	5] The Iterators returned by the Vector class are fail-fast. In the case of concurrent modification, it fails and throws the ConcurrentModificationException.

*/

import java.util.*;

class VectorDemo{
	
	public static void main(String[] args){
		
		//   public java.util.Vector(int, int);	 ==> parameters	(int initialCapacity, int capacityIncrement)	
		Vector vr = new Vector(10, 20);
		
		System.out.println("Initial Capacity is " + vr.capacity());

		vr.add(1);
		vr.add(2);
		vr.add(3);
		vr.add(4);
		vr.add(5);
		vr.add(6);
		vr.add(7);
		vr.add(8);
		vr.add(9);
		vr.add(10);
		vr.add(11);

		System.out.println(vr);
		System.out.println("Capacity Increment is " + vr.capacity());
	}
}
/*
 	Initial Capacity is 10
	[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
	Capacity Increment is 30
*/
