

Types of Cursors in Java

There are three cursors in Java as mentioned below:

	a] Iterator
    	b] Enumeration
    	c] ListIterator

    	Note: SplitIterator can also be considered as a cursor as it is a type of Iterator only.


	1. Iterator

	1] Iterators in Java are used in the Collection framework to retrieve elements one by one. 
	2] It is a universal iterator as we can apply it to any Collection object. 
	3] By using Iterator, we can perform both read and remove operations. 
	4] It is an improved version of Enumeration with the additional functionality of removing an element.
	5] Iterator must be used whenever we want to enumerate elements in all Collection framework implemented interfaces like Set, List, Queue, Deque, and all implemented classes of Map interface. 
	6] The iterator is the only cursor available for the entire collection framework. An iterator object can be created by calling the iterator() method present in the Collection interface.

	Syntax :-

	Iterator itr = c.iterator();

    	Note: Here “c” is any Collection object. itr is of type Iterator interface and refers to “c”.


	Methods of Iterator Interface in Java :-

	The iterator interface defines three methods as listed below:

	1. hasNext(): Returns true if the iteration has more elements.

	public boolean hasNext();

	2. next(): Returns the next element in the iteration. It throws NoSuchElementException if no more element is present.

	public Object next();

	3. remove(): Removes the next element in the iteration. This method can be called only once per call to next().

	public void remove();

    	Note: remove() method can throw two exceptions namely as follows:

        	a] UnsupportedOperationException : If the remove operation is not supported by this iterator
        	b] IllegalStateException : If the next method has not yet been called, or the remove method has already been called after the last call to the next method.

	
	SplitIterator :-

	Spliterators, like other Iterators, are for traversing the elements of a source. A source can be a Collection, an IO channel, or a generator function. It is included in JDK 8 for support of efficient parallel traversal(parallel programming) in addition to sequential traversal. Java Spliterator interface is an internal iterator that breaks the stream into smaller parts. These smaller parts can be processed in parallel. 

    	Note: In real life programming, we may never need to use Spliterator directly. Under normal operations, it will behave exactly the same as Java Iterator.

	2] 2. Enumeration

	1] It is an interface used to get elements of legacy collections(Vector, Hashtable). 
	2] Enumeration is the first iterator present from JDK 1.0, rests are included in JDK 1.2 with more functionality. 
	3] Enumerations are also used to specify the input streams to a SequenceInputStream. 
	4] We can create an Enumeration object by calling elements() method of the vector class on any vector object  

	Syntax

	// Here "v" is an Vector class object. e is of
	// type Enumeration interface and refers to "v"
	Enumeration e = v.elements();

	There are two methods in the Enumeration interface namely : 

	1. public boolean hasMoreElements(): This method tests if this enumeration contains more elements or not.

	2. public Object nextElement(): This method returns the next element of this enumeration. It throws NoSuchElementException if no more element is present

	3] 3. ListIterator

	1] It is only applicable for List collection implemented classes like ArrayList, LinkedList, etc. 
	2] It provides bi-directional iteration. 
	3] ListIterator must be used when we want to enumerate elements of List. 
	4] This cursor has more functionality(methods) than iterator. 
	5] ListIterator object can be created by calling listIterator() method present in the List interface.

	Syntax

	ListIterator ltr = l.listIterator();

    	Note: Here “l” is any List object, ltr is of type. ListIterator interface and refers to “l”. ListIterator interface extends the Iterator interface. So all three methods of Iterator interface are available for ListIterator. In addition, there are six more methods. 

	1. Forward direction

	    1.1 hasNext(): Returns true if the iteration has more elements

	    public boolean hasNext();

	    1.2 next(): Same as next() method of Iterator. Returns the next element in the iteration. 

	    public Object next(); 

	    1.3 nextIndex(): Returns the next element index or list size if the list iterator is at the end of the list. 

	    public int nextIndex();

	2. Backward direction

	    2.1 hasPrevious(): Returns true if the iteration has more elements while traversing backward.

    public boolean hasPrevious();

	    2.2 previous(): Returns the previous element in the iteration and can throw NoSuchElementException if no more element present.

	    public Object previous(); 

	    2.3 previousIndex(): Returns the previous element index or -1 if the list iterator is at the beginning of the list, 

	    public int previousIndex();

	3. Other Methods

	    3.1 remove(): Same as remove() method of Iterator. Removes the next element in the iteration.

	    public void remove();

	    3.2 set(Object obj): Replaces the last element returned by next() or previous() with the specified element.

	    public void set(Object obj); 

	    3.3 add(Object obj): Inserts the specified element into the list at the position before the element that would be returned by next()

	    public void add(Object obj);


