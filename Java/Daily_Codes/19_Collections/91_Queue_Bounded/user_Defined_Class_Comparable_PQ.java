
import java.util.*;

class Project implements Comparable{
	
	String projName = null;
	int team = 0;
	int duration = 0;

	Project(String projName, int team, int duration){
		
		this.projName = projName;

		this.team = team;

		this.duration = duration;
	}

	public String toString(){
		
		return "{" + projName + ":" + team + ":" + duration + "}";
	}

	public int compareTo(Object obj){
		
		System.out.println("Hello");		
		return (int) (this.projName).compareTo(((Project)obj).projName);
	}
}

class PriorityQueueDemo{
	
	public static void main(String[] args){
		
		PriorityQueue pq = new PriorityQueue();

		pq.offer(new Project("C", 5, 2));

		pq.offer(new Project("Java", 1, 4));

		pq.offer(new Project("A", 2, 7));
		
		System.out.println(pq);		// [{C:5:2}, {Java:1:4}, {Flutter:2:7}]
	}
}
