
import java.util.*;

class ArrayDequeDemo extends ArrayDeque{

	/*
	 	The inc method is used to increment an index while maintaining circular behavior. 
		It ensures that the index wraps around when it reaches the end of the array or buffer, allowing for continuous traversal.
	*/

	static final int inc(int i, int n) {

		return (++i < n) ? i : 0;	// 1
    	}
	
	/*
		The dec method is used to decrement an index while also preserving the circular behavior.		 
		It ensures that the index wraps around when it goes below zero, allowing for smooth backward traversal in a circular structure.
	*/

    	static final int dec(int i, int n) {

		return ((i > 0) ? i : n) - 1;	// 2
    	}

	/*
	 	the mask helps keep the resulting index within the valid range of indices, 
		making sure that circular behavior is achieved without causing index overflows or underflows.
	*/
    	static final int inc(int i, int n, int mask) {

        	return (i + 1) & mask;		// 0
    	}

    	static final int sub(int i, int n, int mask) {

        	return (i - 1) & mask;		// 2
    	}

	public static void main(String[] args){
		
		ArrayDeque al = new ArrayDeque();

		al.add(1);
		al.add(2);
		al.add(3);
		
		System.out.println(al);

		int index = 0;

		int size = al.size();

		int mask = size - 1;

		System.out.println(inc(index, size));

		System.out.println(dec(index, size));

		System.out.println(inc(index, size, mask));

		System.out.println(sub(index, size, mask));

		System.out.println(al);
	}
}

/*
	[1, 2, 3]
	1
	2
	0
	2
	[1, 2, 3]
*/
