
/*
 	Compiled from "Queue.java"
	public interface java.util.Queue<E> extends java.util.Collection<E> {
	  public abstract boolean add(E);
	  public abstract boolean offer(E);
	  public abstract E remove();
	  public abstract E poll();
	  public abstract E element();
	  public abstract E peek();
	}
*/

import java.util.*;

class QueueDemo{
	
	public static void main(String[] args){
		
		Queue que = new LinkedList();

		que.offer(10);
		que.offer(20);
		que.offer(30);
		que.offer(40);
		que.offer(50);
		
		System.out.println(que);	// [10, 20, 30, 40, 50]

		que.poll();

//		que.remove();		==> poll and remove are same, but poll return[] is queue is empty and 
//					    remove throws runtime exception i.e NoSuchElementException

		System.out.println(que);

		System.out.println(que.peek());		// return null if queue is empty

		System.out.println(que.remove());	// throws NoSuchElementException

		System.out.println(que);

	}
}
