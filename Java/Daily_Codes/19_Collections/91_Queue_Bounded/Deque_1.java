
import java.util.*;

class DequeDemo{
	
	public static void main(String[] args){
		
		Deque obj = new ArrayDeque();

		obj.offer(10);

		obj.offer(40);

		obj.offer(30);

		obj.offer(20);

		System.out.println(obj);

		obj.offerFirst(5);

		obj.offerLast(50);

		System.out.println(obj.pollFirst());

		System.out.println(obj.pollLast());
			
		System.out.println(obj);

		System.out.println(obj.peekFirst());

		System.out.println(obj.peekLast());
	}
}

/*
 	[10, 40, 30, 20]
	5
	50
	[10, 40, 30, 20]
	10
	20
*/
