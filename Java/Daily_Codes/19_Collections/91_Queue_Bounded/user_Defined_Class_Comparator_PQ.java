
import java.util.*;

class Project{
	
	String projName = null;
	int team = 0;
	int duration = 0;

	Project(String projName, int team, int duration){
		
		this.projName = projName;

		this.team = team;

		this.duration = duration;
	}

	public String toString(){
		
		return "{" + projName + ":" + team + ":" + duration + "}";
	}
}

class SortByProjName implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		System.out.println("Hello");		
		return (int) ((Project)obj1).projName.compareTo(((Project)obj2).projName);
	}
}

class PriorityQueueDemo{
	
	public static void main(String[] args){
		
		PriorityQueue pq = new PriorityQueue(new SortByProjName());

		pq.offer(new Project("C", 5, 2));

		pq.offer(new Project("Java", 1, 4));

		pq.offer(new Project("Flutter", 2, 7));
		
		System.out.println(pq);		// [{C:5:2}, {Java:1:4}, {Flutter:2:7}]
	}
}
