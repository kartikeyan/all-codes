
//	entrySet() helps us to convert map into set and after that we can iterate using iterator


import java.util.*;

class IterateMapDemo{
	
	public static void main(String[] args){

		SortedMap tm = new TreeMap();
		
		tm.put("Ind", "India");

		tm.put("Aus", "Australia");

		tm.put("Pakistan", "Pakistan");

		tm.put("Ban", "Bangladesh");

		tm.put("SL", "Sri Lanka");
		
		System.out.println(tm);

		Set<Map.Entry> data = tm.entrySet();

		Iterator<Map.Entry> itr = data.iterator();

		while(itr.hasNext()){
			
			Map.Entry obj = itr.next().getKey();
			
			// Prefer this

	//		System.out.println(obj.getKey() + ":" + obj.getValue());
			
			System.out.println(obj);			
		}
	}
}

/*
 	{Aus=Australia, Ban=Bangladesh, Ind=India, Pakistan=Pakistan, SL=Sri Lanka}
	{Aus=Australia, Ban=Bangladesh, Ind=India}
	{Aus=Australia, Ban=Bangladesh, Ind=India}
	{Pakistan=Pakistan, SL=Sri Lanka}
	Aus
	SL
	[Aus, Ban, Ind, Pakistan, SL]
	[Australia, Bangladesh, India, Pakistan, Sri Lanka]
*/
