
/*
 	1] Object(Class)

		2] Dictionary(Class)

			3] Hashtable(Legacy Class)

	Difference between Hashtable and hashMap is 

	Hashtable is Synchronized and HashMap is Not Synchronized
*/

import java.util.*;

class HashtableDemo{
	
	public static void main(String[] args){
		
		Dictionary ht = new Hashtable();

		ht.put(10, "Sachin");

		ht.put(7, "MSD");

		ht.put(18, "Virat");

		ht.put(45, "Rohit");

		ht.put(1, "KL");
		
		System.out.println(ht);

		Enumeration itr1 = ht.keys();

		while(itr1.hasMoreElements()){
			
			System.out.println(itr1.nextElement());

		}
		
		Enumeration itr2 = ht.elements();

		while(itr2.hasMoreElements()){
			
			System.out.println(itr2.nextElement());

		}

		System.out.println(ht.get(10));

		ht.remove(1);

		System.out.println(ht);
	}
}

/*
 	{10=Sachin, 18=Virat, 7=MSD, 1=KL, 45=Rohit}
	10
	18
	7
	1
	45
	Sachin
	Virat
	MSD
	KL
	Rohit
	Sachin
	{10=Sachin, 18=Virat, 7=MSD, 45=Rohit}
*/
