
import java.util.*;

class SortedMapDemo{
	
	public static void main(String[] args){

		SortedMap tm = new TreeMap();
		
		tm.put("Ind", "India");

		tm.put("Aus", "Australia");

		tm.put("Pakistan", "Pakistan");

		tm.put("Ban", "Bangladesh");

		tm.put("SL", "Sri Lanka");
		
		System.out.println(tm);

		System.out.println(tm.subMap("Aus","Pak"));

		System.out.println(tm.headMap("Pak"));

		System.out.println(tm.tailMap("Pak"));

		System.out.println(tm.firstKey());

		System.out.println(tm.lastKey());

		System.out.println(tm.keySet());

		System.out.println(tm.values());
	}
}

/*
 	{Aus=Australia, Ban=Bangladesh, Ind=India, Pakistan=Pakistan, SL=Sri Lanka}
	{Aus=Australia, Ban=Bangladesh, Ind=India}
	{Aus=Australia, Ban=Bangladesh, Ind=India}
	{Pakistan=Pakistan, SL=Sri Lanka}
	Aus
	SL
	[Aus, Ban, Ind, Pakistan, SL]
	[Australia, Bangladesh, India, Pakistan, Sri Lanka]
*/
