import java.util.*;

class IterateMapDemo{
	
	public static void main(String[] args){

		SortedMap tm = new TreeMap();
		
		tm.put("Ind", "India");

		tm.put("Aus", "Australia");

		tm.put("Pakistan", "Pakistan");

		tm.put("Ban", "Bangladesh");

		tm.put("SL", "Sri Lanka");
		
		System.out.println(tm);

		Set<Map.Entry> data = tm.entrySet();

		Iterator<Map.Entry> itr = data.iterator();

		while(itr.hasNext()){
			
			Map.Entry obj = itr.next().getKey();
			
			System.out.println(obj);	
		}
	}
}

/*
IteratingOverMap.java:28: error: incompatible types: Object cannot be converted to Entry
	Map.Entry obj = itr.next().getKey();
			                 ^
Note: IteratingOverMap.java uses unchecked or unsafe operations.
Note: Recompile with -Xlint:unchecked for details.
1 error
	
*/
