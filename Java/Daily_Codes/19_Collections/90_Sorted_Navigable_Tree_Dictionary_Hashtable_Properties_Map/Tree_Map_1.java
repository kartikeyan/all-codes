
import java.util.*;

class TreeMapDemo{
	
	public static void main(String[] args){
		
		TreeMap tm = new TreeMap();

		tm.put("Ind", "India");

		tm.put("Aus", "Australia");

		tm.put("Pak", "Pakistan");

		tm.put("Ban", "Bangladesh");

		tm.put("Nep", "Nepal");
		
		System.out.println(tm);
	}
}

//	{Aus=Australia, Ban=Bangladesh, Ind=India, Nep=Nepal, Pak=Pakistan}
