
//	A comparator interface is used to order the objects of user-defined classes. A comparator object is capable of comparing two objects of the same class


import java.util.*;

class Platform{
	
	String str = null;

	int foundYear = 0;

	Platform(String str, int foundYear){
		
		this.str = str;

		this.foundYear = foundYear;
	}

	public String toString(){
		
		return "{" + str + ":" + foundYear + "}";
	}
}

class SortByName implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		return ((Platform)obj1).str.compareTo(((Platform)obj2).str);
	}
}

class SortBySort implements Comparator{
	
	public int compare(Object obj1, Object obj2){
		
		return ((Platform)obj1).foundYear - ((Platform)obj2).foundYear;
	}
}

class TreeMapDemo{

	public static void main(String[] args){
		
		TreeMap tm = new TreeMap(new SortByName());

//		TreeMap tm = new TreeMap(new SortBySort());

		tm.put(new Platform("Youtube", 2005), "Google");

		tm.put(new Platform("Instagram", 2010), "Meta");

		tm.put(new Platform("Fb", 2004), "Meta");

		tm.put(new Platform("ChatGPT", 2022), "OpenAI");
		
		System.out.println(tm);
	}
}

//	{{Fb:2004}=Meta, {Youtube:2005}=Google, {Instagram:2010}=Meta, {ChatGPT:2022}=OpenAI}
