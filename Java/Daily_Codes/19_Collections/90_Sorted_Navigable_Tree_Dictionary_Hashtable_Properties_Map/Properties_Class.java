
import java.util.*;
import java.io.*;

class PropertiesDemo{
	
	public static void main(String[] args) throws IOException{
		
		Properties obj = new Properties();

		FileInputStream fobj = new FileInputStream("friends.properties");

		obj.load(fobj);

		String name = obj.getProperty("Ashish");

		System.out.println(name);

		obj.setProperty("Kartik", "Zerodha");

		FileOutputStream outObj = new FileOutputStream("friends.properties");

		obj.store(outObj, "Updated by Kartik");
	}
}

//	Barclays

/*
 	Before Output :
				
	kanha=Infosys
	Rahul=BMC
	Shashi=Biencaps
	Ashish=Barclays
	Badhe=CarPro

	After Output :
	
	#Updated by Kartik
	#Sat Aug 19 00:28:48 IST 2023
	kanha=Infosys
	Rahul=BMC
	Shashi=Biencaps
	Ashish=Barclays
	Badhe=CarPro
	Kartik=Zerodha
*/
