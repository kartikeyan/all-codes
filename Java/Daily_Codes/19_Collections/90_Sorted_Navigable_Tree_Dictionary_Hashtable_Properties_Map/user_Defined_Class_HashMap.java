
import java.util.*;

class Platform{
	
	String str = null;

	int foundYear = 0;

	Platform(String str, int foundYear){
		
		this.str = str;

		this.foundYear = foundYear;
	}

	public int getfoundYear(){
		
		return foundYear;
	}

	public String toString(){
		
		return "{" + str + ":" + foundYear + "}";
	}
}

class TreeMapDemo{

	public static void main(String[] args){
		
		HashMap tm = new HashMap();

		tm.put(new Platform("Youtube", 2005), "Google");
		tm.put(new Platform("Instagram", 2010), "Meta");
		tm.put(new Platform("Fb", 2004), "Meta");
		tm.put(new Platform("ChatGPT", 2022), "OpenAI");

		List<Platform> ll = new ArrayList<>(tm.keySet());
		
		System.out.println(ll);

		for(int i = 0; i < ll.size() - 1; i++){
			
			for(int j = i + 1; j < ll.size(); j++){
				
				if(ll.get(i).getfoundYear() > ll.get(j).getfoundYear()){

					Platform temp = ll.get(i);
				
					ll.set(i, ll.get(j));

					ll.set(j, temp);
				}
			}
		}

		System.out.println(ll);
	}
	
}

/*
 	[{Youtube:2005}, {ChatGPT:2022}, {Instagram:2010}, {Fb:2004}]
	[{Fb:2004}, {Youtube:2005}, {Instagram:2010}, {ChatGPT:2022}]
*/
