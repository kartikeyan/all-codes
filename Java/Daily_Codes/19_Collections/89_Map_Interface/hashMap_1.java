//	HashSet internally is HashMap

import java.util.*;

class HashMapDemo{
	
	public static void main(String[] args){
		
		HashSet hs = new HashSet();

		hs.add("Kanha");

		hs.add("Ashish");

		hs.add("Badhe");

		hs.add("Rahul");
		
		System.out.println(hs);

		HashMap hm = new HashMap();

		hm.put("Kanha", "Infosys");

		hm.put("Ashish", "Barclays");

		hm.put("Badhe", "CarPro");

		hm.put("Rahul", "BMC");

		System.out.println(hm);
	}
}

/*
 	[Rahul, Ashish, Badhe, Kanha]
	{Rahul=BMC, Ashish=Barclays, Badhe=CarPro, Kanha=Infosys}
*/

/*
 	public V put(K key, V value)
 343:   {
 344:     int idx = hash(key);
 345:     HashEntry<K, V> e = buckets[idx];
 346: 
 347:     while (e != null)
 348:       {
 349:         if (equals(key, e.key))
 350:           {
 351:             e.access(); // Must call this for bookkeeping in LinkedHashMap.
 352:         V r = e.value;
 353:             e.value = value;
 354:             return r;
 355:           }
 356:         else
 357:           e = e.next;
 358:       }
 359: 
 360:     // At this point, we know we need to add a new entry.
 361:     modCount++;
 362:     if (++size > threshold)
 363:       {
 364:         rehash();
 365:         // Need a new hash value to suit the bigger table.
 366:         idx = hash(key);
 367:       }
 368: 
 369:     // LinkedHashMap cannot override put(), hence this call.
 370:     addEntry(key, value, idx, true);
 371:     return null;
 372:   }

*/

/*
 
 	final int hash(Object key)
 687:   {
 688:     return key == null ? 0 : Math.abs(key.hashCode() % buckets.length);
 689:   }

*/

/*	
 	String hashCode method :-

 	public int hashCode()
1066:   {
1067:     if (cachedHashCode != 0)
1068:       return cachedHashCode;
1069: 
1070:     // Compute the hash code using a local variable to be reentrant.
1071:     int hashCode = 0;
1072:     int limit = count + offset;
1073:     for (int i = offset; i < limit; i++)
1074:       hashCode = hashCode * 31 + value[i];
1075:     return cachedHashCode = hashCode;
1076:   }

*/

/*
	Integer HashCode method :-	

	public int hashCode()
 394:   {
 395:     return value;
 396:   }

*/


