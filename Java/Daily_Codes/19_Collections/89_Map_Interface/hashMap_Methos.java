

import java.util.*;

class HashMapDemo{
	
	public static void main(String[] args){
		
		HashMap hm = new HashMap();

		hm.put("python", ".py");

		hm.put("java", ".java");

		hm.put("C", ".c");

		hm.put("Dart", ".dart");
		
		System.out.println(hm);

		System.out.println(hm.get("python"));

		System.out.println(hm.keySet());

		System.out.println(hm.values());

		System.out.println(hm.entrySet());
	}
} 

/*
 	{python=.py, java=.java, C=.c, Dart=.dart}
	.py
	[python, java, C, Dart]
	[.py, .java, .c, .dart]
	[python=.py, java=.java, C=.c, Dart=.dart]
*/
