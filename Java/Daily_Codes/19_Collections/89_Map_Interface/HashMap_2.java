
import java.util.*;

class HashMapDemo{
	
	public static void main(String[] args){
		
		String str1 = "RAM";

		String str2 = "KAM";

		String str3 = "SAM";

		System.out.println("RAM " + str1.hashCode());

		System.out.println("KAM " + str2.hashCode());

		System.out.println("SAM " +  str3.hashCode());

		HashMap hm = new HashMap();

		hm.put("RAM", 1);

		hm.put("KAM", 2);

		hm.put("SAM", 3);
		
		System.out.println(hm);
	}
}

/*
 	RAM 80894
	KAM 74167
	SAM 81855
	
	{KAM=2, SAM=3, RAM=1}
*/
