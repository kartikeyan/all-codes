
//	if you need duplicate objects in map then u should use IdentityhashMap

import java.util.*;

class IdentityDemo{
	
	public static void main(String[] args){
		
		IdentityHashMap hm = new IdentityHashMap();

		hm.put(new Integer(10), "Kanha");	// compulsory have to use "new" then only desired output will be created

		hm.put(new Integer(10), "Rahul");

		hm.put(new Integer(10), "Badhe");
	
		System.out.println(hm);
	}
}

//	{10=Kanha, 10=Rahul, 10=Badhe}
