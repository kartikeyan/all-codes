

import java.util.*;

class HashMapDemo{
	
	public static void main(String[] args){
		
		HashMap hm = new HashMap();

		hm.put(300, "BMC");

		hm.put(200, "Barcalys");

		hm.put(900, "CarPro");

		hm.put(100, "BMC");
		
		System.out.println(hm);
	}
} 

//	{900=CarPro, 100=BMC, 200=Barcalys, 300=BMC}

