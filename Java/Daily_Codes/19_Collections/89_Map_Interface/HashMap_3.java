
//	Duplicate key's value gets replaced by updated value

import java.util.*;

class HashMapDemo{
	
	public static void main(String[] args){
		
		HashMap hm = new HashMap();

		hm.put("Kanha", "BMC");

		hm.put("Ashish", "Barcalys");

		hm.put("Kanha", "CarPro");

		hm.put("Rahul", "BMC");
		
		System.out.println(hm);
	}
} 

//	{Rahul=BMC, Ashish=Barcalys, Kanha=CarPro}
