
//	Garbage Collector cannot control HashMap

import java.util.*;

class Demo{
	
	String str;

	Demo(String str){

		this.str = str;
	}

	public void finalize(){
		
		System.out.println("Notify");
	}

	public String toString(){

		return str;
	}
}

class GCDemo{
	
	public static void main(String[] args){
		
		Demo obj1 = new Demo("C2W");

		Demo obj2 = new Demo("Incubator");

		Demo obj3 = new Demo("Biencaps");

		System.out.println(obj1);

		System.out.println(obj2);

		System.out.println(obj3);
		
		WeakHashMap hm = new WeakHashMap();

		hm.put(obj1, "2016");

		hm.put(obj2, "2019");

		hm.put(obj3, "2023");

		obj1 = null;

		obj2 = null;

		System.gc();

		System.out.println("In main");
	}
}

/*
 	C2W
	Incubator
	Biencaps
	In main
	Notify
	Notify
*/
