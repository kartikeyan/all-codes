
class switchDemo{
	
	public static void main(String[] s){
		
		int x = 3;

		switch(x){
			
			case 1:
				System.out.println("One");
			case 2:
				System.out.println("Two");
			case 3:
				System.out.println("Three");
			case 4:
				System.out.println("four");
			case 5:
				System.out.println("five");
			default:
				System.out.println("No match");

			System.out.println("After Switch");
		}
	}
}
