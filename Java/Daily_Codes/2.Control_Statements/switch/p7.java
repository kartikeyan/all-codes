
//	Duplicate Cases

class switchDemo{
	
	public static void main(String[] s){
		
		int x = 5;

		switch(x){
			
			case 1:
				System.out.println("One");
				break;
			case 1+1:
				System.out.println("Two");
				break;
			case 1+2:
				System.out.println("Three");
				break;
			case 5:
				System.out.println("four");
				break;
			case 2:
				System.out.println("five");
				break;
			default:
				System.out.println("No Match");	
		}

		System.out.println("After Switch");
	}
}

/*	error : duplicate case lable
*/

