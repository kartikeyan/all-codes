
class switchDemo{
	
	public static void main(String[] s){
		
		int x = 6;

		switch(x){
			
			default:					// least priority 
				System.out.println("No match");
				break;
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;
			case 3:
				System.out.println("Three");
				break;
			case 4:
				System.out.println("four");
				break;
			case 5:
				System.out.println("five");
				break;
		}
	}
}

/*	o/p : default la least priority asto mahnun pahila case bagto ani nasel tar default kade jato
*/
