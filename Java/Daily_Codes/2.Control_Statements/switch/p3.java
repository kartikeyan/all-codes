
class switchDemo{
	
	public static void main(String[] s){
		
		int x = 3;

		switch(x){
			
			case 1:
				System.out.println("One");
				break;
			case 2:
				System.out.println("Two");
				break;
			case 3:
				System.out.println("Three");
				break;
			case 4:
				System.out.println("four");
				break;
			case 5:
				System.out.println("five");
				break;
			default:
				System.out.println("No match");
				break;


			System.out.println("After Switch");

		}
	}
}

/*	error : unreachable statement
*/	
