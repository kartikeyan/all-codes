
class switchDemo{
	
	public static void main(String[] s){
		
		String str = "Mon";

		switch(str){
			
			case "Mon":
				System.out.println("Monday");
				break;
			case "Tue":
				System.out.println("Tuesday");
				break;
			default:
				System.out.println("No Match");	
		}

		System.out.println("After Switch");
	}
}

/*	Javac 1.6 chya agodhar chya version astil tar error yeil
 *
 *	Javac 1.7 chya nantar astil tar chalto
 *
 *	Maja javac cha version 11 ahe 
*/

