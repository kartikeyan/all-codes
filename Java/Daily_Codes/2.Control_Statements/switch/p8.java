
//	Duplicate Cases

class switchDemo{
	
	public static void main(String[] s){
		
		int ch = 65;

		switch(ch){
			
			case 'A':					// A internally ASCII value madhe jato
				System.out.println("One");
				break;
			case 65:
				System.out.println("Two");
				break;
			case 'B':
				System.out.println("Three");
				break;
			case 66:
				System.out.println("four");
				break;
			default:
				System.out.println("No Match");	
		}

		System.out.println("After Switch");
	}
}

/*	error : duplicate case lable
*/

