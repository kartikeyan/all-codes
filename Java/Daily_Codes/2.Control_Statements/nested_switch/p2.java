
/*	Prog : Nested Switch
*/

class assign{
	
	public static void main(String[] args){
		
		String str = "Veg";

		switch(str){
			
			case "Veg":
				{
					String str1 = "Starter";

					switch(str1){
						
						case "Starter":
								System.out.println("Paneer Pepper fry");
								break;
						case "Main Course":
								System.out.println("Veg Biryani");
								break;

					}
				}
				break;
			
			case "Non Veg":
				{				
					String str2 = "Main Course";

					switch(str2){
						
						case "Starter":
								System.out.println("Kabab");
								break;
						case "Main Course":
								System.out.println("Biryani");
								break;

					}
				}
				break;	
		}
	}
}
