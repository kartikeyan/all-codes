
class assign{
	
	static public int factorial(int rem){
		
		int prod = 1;
		while(rem > 1){
			
			prod = prod * rem;
			rem--;
		}

		return prod;
	}

	public static void main(String[] args){
		
		int n = 145, temp = n, rem;
		
		for(int i = 1; i <= 1000; i++){
			
			int k = i, sum = 0;

			while(k != 0){
			
				rem = k % 10;
				sum = sum + factorial(k);
				k = k / 10;
			}

			if(sum == i){
			
				System.out.println("Strong Number : " + i);
			}else{
				//System.out.println("Not a Strong : " + i);
				continue;
			}
		}
	}
}
