
class assign{
	
	static public int factorial(int rem){
		
		int prod = 1;
		while(rem > 1){
			
			prod = prod * rem;
			rem--;
		}

		return prod;
	}

	public static void main(String[] args){
		
		int n = 145, temp = n, rem, sum = 0;

		while(n != 0){
			
			rem = n % 10;
			sum = sum + factorial(rem);
			n = n / 10;
		}

		if(temp == sum){
			
			System.out.println("Strong Number");
		}else{
			System.out.println("Not a Strong");
		}
	}
}
