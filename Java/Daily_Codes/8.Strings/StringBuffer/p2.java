
class Sol{
	
	public static void main(String[] s){
		
		String str1 = "shashi";

		str1 = str1.concat("Bagal");

		System.out.println(str1);	// shashiBagal
	}
}

/*
 	str1 got memory in SCP, after concat str1 creates new object on heap
	and new object reference is passed to str1, wipping previous str1 reference
*/
