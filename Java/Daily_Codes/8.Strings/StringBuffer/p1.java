
class Sol{
	
	public static void main(String[] s){
		
		String str1 = "shashi";

		String str2 = str1.concat("Bagal");

		System.out.println(str1);	// shashi

		System.out.println(str2);	// shashiBagal
	}
}

/*
 	str1 got memory in SCP
	str2 got memory in heap
*/
