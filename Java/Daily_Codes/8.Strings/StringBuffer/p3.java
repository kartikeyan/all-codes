
class Sol{
	
	public static void main(String[] s){
		
		StringBuffer str = new StringBuffer("Shashi");

		System.out.println(System.identityHashCode(str));
		
		System.out.println(str.capacity());

		str.append("Bagal");

		System.out.println(str);	// shashiBagal
						
		System.out.println(System.identityHashCode(str));
		
		System.out.println(str.capacity());
	}
}

//	StringBuffer Default String length : 16 character + input String length

/*
 	str got memory in heap, after append operation no new object is created, rather string is appended at the end of string

	225534817
	22
	ShashiBagal
	225534817
	22

*/
