
/*
 	method : public boolean equalsIgnoreCase(String anotherString)

	description : Compares string to another string using IgnoreCase

	parameters : String(str2)

	returntype : boolean

*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str1 = "Core2Web";

		String str2 = "CORE2WEB";

		System.out.println(str1.equalsIgnoreCase(str2));	// true
	}
}
