
/*	
 	method : public int compareToIgnoreCase(String str)

	description : It compares str1 and str2 (case Insensitive)

	parameters : String

	return type : integer
*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str1 = "Kartik";

		String str2 = "Kartikeyan";

		String str3 = "kartik";
		
		System.out.println(str1.compareToIgnoreCase(str3));	// 0
		
		System.out.println(str1.compareToIgnoreCase(str2));	// -4	returns the difference of length

		System.out.println(str2.compareToIgnoreCase(str3));	// 4

		System.out.println(str3.compareToIgnoreCase(str2));	// -4
	}
}

