
/*
 	method : public String subString(int index);

	description : creates a substring of the given string starting at a specified index
		      and ending at the end of the given string

	parameters : Integer(index of the String)

	returnType : String
*/	

class MethodsDemo{
	
	public static void main(String[] s){

		String str = "Core2Web Tech";

		System.out.println(str.substring(5));	

		System.out.println(str.substring(0,3));	
	}
}

/*
	Web Tech
	Cor
*/
