
/*
 	method : public int lastIndexOf(int ch, int fromIndex)

	description : Finds the last instance of the character in given string

	parameters : Character(ch to find), Integer(index to starts the search)

	returntype : Integer

*/	

class MethodsDemo{
	
	public static void main(String[] s){

		String str1 = "Shashi";

		System.out.println(str1.lastIndexOf('h',0));	// -1

		System.out.println(str1.lastIndexOf('h',1));	// 1

		System.out.println(str1.lastIndexOf('h',2));	// 1
	}
}

//	checks upto Index
