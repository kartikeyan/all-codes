
/*	
 	method : public int length();

	description : It returns the no. of characters contained in given string

	parameters : No parameters

	return type : Integer
*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str1 = "Core2web";

		System.out.println(str1.length());		// 8
	}
}
