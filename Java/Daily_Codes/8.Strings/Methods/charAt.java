
/*	
 	method : public char charAt(int index);

	description : It returns the character located at specified index within the given string

	parameters : integer(index)

	return type : character
*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str = "Core2web";
		
		System.out.println(str.length());

		System.out.println(str.charAt(0));

		System.out.println(str.charAt(4));

		System.out.println(str.charAt(8));

//		System.out.println(str.charAt(7));		// b

	}
}

/*
 	8
 	C
	2
	Exception in thread "main" java.lang.StringIndexOutOfBoundsException: String index out of range: 8
		at java.base/java.lang.StringLatin1.charAt(StringLatin1.java:47)
		at java.base/java.lang.String.charAt(String.java:693)
		at MethodsDemo.main(charAt.java:22)
*/
