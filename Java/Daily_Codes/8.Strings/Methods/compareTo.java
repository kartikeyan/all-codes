
/*	
 	method : public int compareTo(String str2)

	description : It compares str1 and str2 (case sensitive), if both the strings are equal it returns 0 otherwise returns the comparison

	parameters : String(second string)

	return type : integer
*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str1 = "Kartik";

		String str2 = "Kartik";

		String str3 = "kartik";
		
		System.out.println(str1.compareTo(str2));	// 0

		System.out.println(str2.compareTo(str3));	// -32

		System.out.println(str3.compareTo(str2));	// 32
	}
}

