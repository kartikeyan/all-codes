
class MethodsDemo{

	static int myStrLen(String str){
		
		char arr[] = str.toCharArray();

		int count = 0;

		for(int i = 0; i < arr.length; i++){
			
			count++;
		}

		return count;
	}
	
	public static void main(String[] s){
		
		String str = "Core2Web";

		int len = myStrLen(str);

		System.out.println(len);
	}
}
