
/*
 	method : public int indexOf(char ch, int fromIndex)

	description : Finds the first instance of the character in given string

	parameters : Character(ch to find), Integer(index to starts the search)

	returntype : Integer

*/	

class MethodsDemo{
	
	public static void main(String[] s){

		String str1 = "Shashi";

		System.out.println(str1.indexOf('h',0));	// 1

		System.out.println(str1.indexOf('h',1));	// 1

		System.out.println(str1.indexOf('h',2));	// 4
	}
}
