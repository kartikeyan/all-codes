
/*	
 	method : public boolean equals(object anObject)

	description : Predicate which compares and object to this. This is true only for strings with the same character sequence.
		      Returns true if an object is semantically equal to this.

	parameters : object(anObject)

	return type : boolean
*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str1 = "Kartik";

		String str2 = new String("Kartik");

		System.out.println(str1.equals(str2));		// true
	}
}

