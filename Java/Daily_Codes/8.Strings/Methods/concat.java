
/*	
 	method : public String concat(String str)

	description : Another String is concatinated to the first string.
		      Implements new Array of character whose length is sum of str1.length and str2.length

	parameters : String

	return type : String
*/	

class MethodsDemo{
	
	public static void main(String[] s){
		
		String str1 = "Core2";

		String str2 = "web";

		String str3 = str1.concat(str2);

		System.out.println(str3);		// Core2web
	}
}
