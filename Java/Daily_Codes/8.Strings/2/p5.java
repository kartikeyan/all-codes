
class Sol{
	
	public static void main(String[] s){
		
		String str1 = "kanha";			// SCP
		String str2 = "kanha";			// Already there on SCP, no new object is created
		String str3 = new String("Kanha");	// new object is created
		String str4 = new String("Kanha");	// as already on heap object is created, because of new another object is created
		String str5 = new String("Rahul");	// new object
		String str6 = "Rahul";			// new object on SCP
	
	}
}
