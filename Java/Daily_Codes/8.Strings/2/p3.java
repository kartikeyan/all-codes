
class Sol{
	
	public static void main(String[] s){
		
		int arr1[] = {10,200,300};

		int arr2[] = {10,200,300};

		System.out.println(System.identityHashCode(arr1[0]));
		System.out.println(System.identityHashCode(arr1[1]));
		System.out.println(System.identityHashCode(arr1[2]));

		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));

		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));

	}
}

/*	
 	1304217686
	225534817
	1878246837

	1304217686
	929338653
	1259475182

	1304217686
	1300109446
	1020371697
*/

/*	
 	katikeyan@kartikeyan:~/javacodes/Codes/8.Strings/2$ javap -c Sol.class 
Compiled from "p3.java"
class Sol {
  Sol();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_3
       1: newarray       int
       3: dup
       4: iconst_0
       5: bipush        10
       7: iastore
       8: dup
       9: iconst_1
      10: sipush        200
      13: iastore
      14: dup
      15: iconst_2
      16: sipush        300
      19: iastore
      20: astore_1
      21: iconst_3
      22: newarray       int
      24: dup
      25: iconst_0
      26: bipush        10
      28: iastore
      29: dup
      30: iconst_1
      31: sipush        200
      34: iastore
      35: dup
      36: iconst_2
      37: sipush        300
      40: iastore
      41: astore_2
      42: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      45: aload_1
      46: iconst_0
      47: iaload
      48: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      51: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      54: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      57: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      60: aload_1
      61: iconst_1
      62: iaload
      63: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      66: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      69: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      72: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      75: aload_1
      76: iconst_2
      77: iaload
      78: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      81: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      84: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      87: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      90: aload_2
      91: iconst_0
      92: iaload
      93: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      96: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      99: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
     102: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
     105: aload_2
     106: iconst_1
     107: iaload
     108: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
     111: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     114: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
     117: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
     120: aload_2
     121: iconst_2
     122: iaload
     123: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
     126: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     129: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
     132: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
     135: aload_2
     136: iconst_0
     137: iaload
     138: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
     141: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     144: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
     147: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
     150: aload_2
     151: iconst_1
     152: iaload
     153: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
     156: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     159: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
     162: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
     165: aload_2
     166: iconst_2
     167: iaload
     168: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
     171: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     174: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
     177: return
}

*/
