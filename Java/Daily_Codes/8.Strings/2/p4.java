
class Sol{
	
	public static void main(String[] s){
		
		int arr1[] = {10,200,300};

		Integer arr2[] = {10,200,300};

		System.out.println(System.identityHashCode(arr1[0]));
		System.out.println(System.identityHashCode(arr1[1]));
		System.out.println(System.identityHashCode(arr1[2]));

		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));

		System.out.println(System.identityHashCode(arr2[0]));
		System.out.println(System.identityHashCode(arr2[1]));
		System.out.println(System.identityHashCode(arr2[2]));
	}
}

/*
 	JVM primitive data type la autoboxing karto, pan object sarkha feature det nahi,
	Primitive data type valueOf() la call karto, pan Class valueOf() la call karat nahi.
*/	

/*
 	1304217686
	225534817
	1878246837
	1304217686
	929338653
	1259475182
	1304217686
	929338653
	1259475182
*/

/*
 	katikeyan@kartikeyan:~/javacodes/Codes/8.Strings/2$ javap -c Sol.class 
Compiled from "p4.java"
class Sol {
  Sol();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_3
       1: newarray       int
       3: dup
       4: iconst_0
       5: bipush        10
       7: iastore
       8: dup
       9: iconst_1
      10: sipush        200
      13: iastore
      14: dup
      15: iconst_2
      16: sipush        300
      19: iastore
      20: astore_1
      21: iconst_3
      22: anewarray     #2                  // class java/lang/Integer
      25: dup
      26: iconst_0
      27: bipush        10
      29: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      32: aastore
      33: dup
      34: iconst_1
      35: sipush        200
      38: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      41: aastore
      42: dup
      43: iconst_2
      44: sipush        300
      47: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      50: aastore
      51: astore_2
      52: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
      55: aload_1
      56: iconst_0
      57: iaload
      58: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      61: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      64: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
      67: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
      70: aload_1
      71: iconst_1
      72: iaload
      73: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      76: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      79: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
      82: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
      85: aload_1
      86: iconst_2
      87: iaload
      88: invokestatic  #3                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      91: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      94: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
      97: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
     100: aload_2
     101: iconst_0
     102: aaload
     103: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     106: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
     109: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
     112: aload_2
     113: iconst_1
     114: aaload
     115: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     118: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
     121: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
     124: aload_2
     125: iconst_2
     126: aaload
     127: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     130: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
     133: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
     136: aload_2
     137: iconst_0
     138: aaload
     139: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     142: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
     145: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
     148: aload_2
     149: iconst_1
     150: aaload
     151: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     154: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
     157: getstatic     #4                  // Field java/lang/System.out:Ljava/io/PrintStream;
     160: aload_2
     161: iconst_2
     162: aaload
     163: invokestatic  #5                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
     166: invokevirtual #6                  // Method java/io/PrintStream.println:(I)V
     169: return
}

*/

