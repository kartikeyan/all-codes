
class Sol{
	
	public static void main(String[] s){
		
		char ch = 'A';

		int x = 65;

		System.out.println(System.identityHashCode(ch));	// internally Character.valueOf(ch) la call karto
									// Character.valueOf(ch) internally new Character la call karto
		System.out.println(System.identityHashCode(x));
	}
}

/*
 	Integer Cache madhe memory bhetla hota dogana pan, idetity.HashCode check karto apan fakt Obejct sathi
	pan ithe primitive data type sathi apan check kartoy tevha toh internally Character.valueOf() la call kartoy
	ani Character.valueOf() internally new Character la call detoy

/*	
 	225534817
	1173310781


Compiled from "p2.java"
class Sol {
  Sol();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: bipush        65
       2: istore_1
       3: bipush        65
       5: istore_2
       6: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
       9: iload_1
      10: invokestatic  #3                  // Method java/lang/Character.valueOf:(C)Ljava/lang/Character;
      13: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      16: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      19: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      22: iload_2
      23: invokestatic  #6                  // Method java/lang/Integer.valueOf:(I)Ljava/lang/Integer;
      26: invokestatic  #4                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      29: invokevirtual #5                  // Method java/io/PrintStream.println:(I)V
      32: return
}
	
*/
