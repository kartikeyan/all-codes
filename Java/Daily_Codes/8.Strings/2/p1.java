
class Sol{
	
	public static void main(String[] s){
		
		char arr[] = {'A','B','C'};

		System.out.println(arr);

		System.out.println("Array " + arr);		// arr.toString() la call karto

		System.out.println(arr.toString());
	}
}

/*
 	ABC
	Array [C@4517d9a3
	[C@4517d9a3

katikeyan@kartikeyan:~/javacodes/Codes/8.Strings/2$ javap -c Sol.class
Compiled from "p1.java"
class Sol {
  Sol();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: iconst_3
       1: newarray       char
       3: dup
       4: iconst_0
       5: bipush        65
       7: castore
       8: dup
       9: iconst_1
      10: bipush        66
      12: castore
      13: dup
      14: iconst_2
      15: bipush        67
      17: castore
      18: astore_1
      19: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      22: aload_1
      23: invokevirtual #3                  // Method java/io/PrintStream.println:([C)V
      26: getstatic     #2                  // Field java/lang/System.out:Ljava/io/PrintStream;
      29: aload_1
      30: invokedynamic #4,  0              // InvokeDynamic #0:makeConcatWithConstants:([C)Ljava/lang/String;
      35: invokevirtual #5                  // Method java/io/PrintStream.println:(Ljava/lang/String;)V
      38: return
}
	



katikeyan@kartikeyan:~/javacodes/Codes/8.Strings/2$ javap -c java.lang.Object
Compiled from "Object.java"
public class java.lang.Object {
  public java.lang.Object();
    Code:
       0: return

  public final native java.lang.Class<?> getClass();

  public native int hashCode();

  public boolean equals(java.lang.Object);
    Code:
       0: aload_0
       1: aload_1
       2: if_acmpne     9
       5: iconst_1
       6: goto          10
       9: iconst_0
      10: ireturn

  protected native java.lang.Object clone() throws java.lang.CloneNotSupportedException;

  public java.lang.String toString();
    Code:
       0: new           #1                  // class java/lang/StringBuilder
       3: dup
       4: invokespecial #2                  // Method java/lang/StringBuilder."<init>":()V
       7: aload_0
       8: invokevirtual #3                  // Method getClass:()Ljava/lang/Class;
      11: invokevirtual #4                  // Method java/lang/Class.getName:()Ljava/lang/String;
      14: invokevirtual #5                  // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
      17: ldc           #6                  // String @
      19: invokevirtual #5                  // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
      22: aload_0
      23: invokevirtual #7                  // Method hashCode:()I
      26: invokestatic  #8                  // Method java/lang/Integer.toHexString:(I)Ljava/lang/String;
      29: invokevirtual #5                  // Method java/lang/StringBuilder.append:(Ljava/lang/String;)Ljava/lang/StringBuilder;
      32: invokevirtual #9                  // Method java/lang/StringBuilder.toString:()Ljava/lang/String;
      35: areturn

  public final native void notify();

  public final native void notifyAll();

  public final void wait() throws java.lang.InterruptedException;
    Code:
       0: aload_0
       1: lconst_0
       2: invokevirtual #10                 // Method wait:(J)V
       5: return

  public final native void wait(long) throws java.lang.InterruptedException;

  public final void wait(long, int) throws java.lang.InterruptedException;
    Code:
       0: lload_1
       1: lconst_0
       2: lcmp
       3: ifge          16
       6: new           #11                 // class java/lang/IllegalArgumentException
       9: dup
      10: ldc           #12                 // String timeoutMillis value is negative
      12: invokespecial #13                 // Method java/lang/IllegalArgumentException."<init>":(Ljava/lang/String;)V
      15: athrow
      16: iload_3
      17: iflt          26
      20: iload_3
      21: ldc           #14                 // int 999999
      23: if_icmple     36
      26: new           #11                 // class java/lang/IllegalArgumentException
      29: dup
      30: ldc           #15                 // String nanosecond timeout value out of range
      32: invokespecial #13                 // Method java/lang/IllegalArgumentException."<init>":(Ljava/lang/String;)V
      35: athrow
      36: iload_3
      37: ifle          44
      40: lload_1
      41: lconst_1
      42: ladd
      43: lstore_1
      44: aload_0
      45: lload_1
      46: invokevirtual #10                 // Method wait:(J)V
      49: return

  protected void finalize() throws java.lang.Throwable;
    Code:
       0: return

  static {};
    Code:
       0: invokestatic  #16                 // Method registerNatives:()V
       3: return
}

*/
