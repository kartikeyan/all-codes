
class Sol{
	
	public static void main(String[] s){
		
		String str1 = new String("Shashi");

		String str2 = new String("Shashi");

		if(str1 == str2){
			
			System.out.println("Equal");
		}else{
			System.out.println("Not Equal");
		}
	}
}

/*
 	str1 == str2 here it is actually comparing System.identityHashCode(str1) == System.identityHashCode(str2) as it not same it gives not equal as answer
*/	
