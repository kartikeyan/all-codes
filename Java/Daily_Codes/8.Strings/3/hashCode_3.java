
class Sol{
	
	public static void main(String[] s){
		
		String str1 = "Onkar";

		String str2 = new String("Onkar");

		String str3 = "Onkar";

		String str4 = new String("Onkar");

		System.out.println(str1.hashCode());

		System.out.println(str2.hashCode());

		System.out.println(str3.hashCode());

		System.out.println(str4.hashCode());
	}
}

/*	
 	for shashi		
 	-903565176		
	-903565176
	-903565176
	-903565176
	
	for kartik
	-1138555590
	-1138555590
	-1138555590
	-1138555590

	for Onkar
	76341117
	76341117
	76341117
	76341117
*/
