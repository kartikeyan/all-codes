
class Sol{
	
	public static void main(String[] s){
		
		String str1 = "kartik";

		String str2 = "Charkupalli";

		String str3 = str1 + str2;		// StringBuilder class

		String str4 = str1.concat(str2);	// string class

		System.out.println(System.identityHashCode(str3));

		System.out.println(System.identityHashCode(str4));
	}
}

/*
 	1555009629
	41359092
*/

/*	
 	Question : Does str3 internally calls concat method ?

	Ans] No

	     It internally calls String Builder Class, in String Builder class it calls append() method, in append() method new object is being created
	

	katikeyan@kartikeyan:~/javacodes/Codes/8.Strings/3$ javap -c Sol.class
Compiled from "p2.java"
class Sol {
  Sol();
    Code:
       0: aload_0
       1: invokespecial #1                  // Method java/lang/Object."<init>":()V
       4: return

  public static void main(java.lang.String[]);
    Code:
       0: ldc           #2                  // String kartik
       2: astore_1
       3: ldc           #3                  // String Charkupalli
       5: astore_2
       6: aload_1
       7: aload_2
       8: invokedynamic #4,  0              // InvokeDynamic #0:makeConcatWithConstants:(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
      13: astore_3
      14: aload_1
      15: aload_2
      16: invokevirtual #5                  // Method java/lang/String.concat:(Ljava/lang/String;)Ljava/lang/String;
      19: astore        4
      21: getstatic     #6                  // Field java/lang/System.out:Ljava/io/PrintStream;
      24: aload_3
      25: invokestatic  #7                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      28: invokevirtual #8                  // Method java/io/PrintStream.println:(I)V
      31: getstatic     #6                  // Field java/lang/System.out:Ljava/io/PrintStream;
      34: aload         4
      36: invokestatic  #7                  // Method java/lang/System.identityHashCode:(Ljava/lang/Object;)I
      39: invokevirtual #8                  // Method java/io/PrintStream.println:(I)V
      42: return
}

*/
