
/*	Strings in Java :
 *
 *		1] Predefined Methods
 *		2] User Defined Methods
*/

//	Java 1.6 nantar Method Area, Heap madhe transfer kela due to Garbage Collector

class StringDemo{
	
	public static void main(String[] s){
		
		String str1 = "Core2Web";		// Gets Memory in String Constant Pool

		String str2 = new String("Core2Web");	// Gets Memory in Heap

		String str3 = "Core2Web";		// As "Core2Web" string is already in SCP, new object is not created

		String str4 = new String("Core2Web");	// New object is created in heap irrespective of "Core2Web" is already present

		System.out.println(System.identityHashCode(str1));

		System.out.println(System.identityHashCode(str2));

		System.out.println(System.identityHashCode(str3));

		System.out.println(System.identityHashCode(str4));
	}
}

/*	
 	225534817
	1878246837
	225534817
	929338653
*/

