
/*	Strings in Java :
 *
 *		1] Predefined Methods
 *		2] User Defined Methods
*/

//	Java 1.6 nantar Method Area, Heap madhe transfer kela due to Garbage Collector

class StringDemo{
	
	public static void main(String[] s){
		
		String str1 = "Core2Web";		// Gets Memory in String Constant Pool

		String str2 = new String("Core2Web");	// Gets Memory in Heap

		char str3[] = {'C','2','W'};		// Objects gets created in heap

		System.out.println(str1);

		System.out.println(str2);

		System.out.println(str3);
	}
}
