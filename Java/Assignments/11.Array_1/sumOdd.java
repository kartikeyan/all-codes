
//	Prog 1 : Print Sum of odd Elements only

import java.io.*;

class sol{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		int oddSum = 0;
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] % 2 == 1){
				
				oddSum += arr[i];
			}
		}
		
		System.out.println("Odd Sum is " + oddSum);
	}
}
