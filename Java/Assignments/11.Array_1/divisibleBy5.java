
//	Prog 3 : Print Product of odd Indexes only

import java.io.*;

class sol{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			
		}
		
		System.out.println("Elements divisible by 5 are");
		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] % 5 == 0){
				
				System.out.println(arr[i]);
			}
			
		}
	}
}
