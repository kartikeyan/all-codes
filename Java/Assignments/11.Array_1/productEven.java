
//	Prog 2 : Print Product of even Elements only

import java.io.*;

class sol{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		int evenProd = 1;
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] % 2 == 0){
				
				evenProd *= arr[i];
			}
		}
		
		System.out.println("Even Product is " + evenProd);
	}
}
