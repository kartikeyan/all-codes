
//	Prog 3 : Print Product of odd Indexes only

import java.io.*;

class sol{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		char arr[] = new char[size];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = br.readLine().charAt(0);		
			
		}
			
		System.out.println("Vowels Are ");
		for(int i = 0; i < arr.length; i++){	
			if(arr[i] == 'a' || arr[i] == 'e' || arr[i] == 'i' || arr[i] == 'o' || arr[i] == 'u'){
				
				System.out.println(arr[i]);		
			}
		}
	}
}
	
