
/*	Pattern 
 *
 *		10  10  10  10
 *
 *		10  10  10
 *
 *		10  10
 *
 *		10
*/

class assign{
	
	public static void main(String[] s){
		
		int n = 4;
	
		for(int i = 1; i <= n; i++){
			
			for(int j = 1; j <= n - i + 1; j++){
				
				System.out.print("10" + " ");
			}

			System.out.println();
		}
	}
}
