
/*	Pattern 
 *
 *		1
 *
 *		2 3
 *
 *		4 5 6
 *
 *		7 8 9 10
*/

class assign{
	
	public static void main(String[] s){
		
		int row = 4, x = 1;

		for(int i = 1; i <= row; i++){
			
			for(int j = 1; j <= i; j++){
				
				System.out.print(x++ + " ");
			}

			System.out.println();
		}
	}
}
