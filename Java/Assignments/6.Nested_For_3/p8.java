
/*	Pattern 
 *
 *		J
 *		I H
 *		G F E
 *		D C B A
*/

class assign{
	
	public static void main(String[] s){
		
		int n = 4, ch = 74;
	
		for(int i = 1; i <= n; i++){

			for(int j = 1; j <=  i ; j++){
				
				System.out.print((char)ch + " ");
				ch--;
			}
	
			System.out.println();
		}
	}
}
