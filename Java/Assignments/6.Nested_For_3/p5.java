
/*	Pattern 
 *
 *		10  10  10  10
 *
 *		11  11  11
 *
 *		12  12
 *
 *		13
*/

class assign{
	
	public static void main(String[] s){
		
		int n = 4, x = n * (n + 1) / 2;
	
		for(int i = 1; i <= n; i++){
			
			for(int j = 1; j <= n - i + 1; j++){
				
				System.out.print(x + " ");
			}
			
			x++;
			System.out.println();
		}
	}
}
