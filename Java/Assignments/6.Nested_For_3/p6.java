
/*	Pattern 
 *
 *		9
 *
 *		9 8 
 *
 *		9 8 7
 *
 *		9 8 7 6
*/

class assign{
	
	public static void main(String[] s){
		
		int n = 4;
	
		for(int i = 1; i <= n; i++){

			int x = n * (n + 1) / 2 - 1;
			
			for(int j = 1; j <=  i ; j++){
				
				System.out.print(x + " ");
				x--;
			}
			
			System.out.println();
		}
	}
}
