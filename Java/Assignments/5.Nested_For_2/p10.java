
/*	Pattern
 *
 * 		F 5 D 3 B 1
 * 		F 5 D 3 B 1
 * 		F 5 D 3 B 1
 * 		F 5 D 3 B 1
 * 		F 5 D 3 B 1
 * 		F 5 D 3 B 1
 *
*/

class assign{
	
	public static void main(String[] args){
		
		
		for(int i = 1; i <= 6; i++){
			
			int ch = 70, x = 6;

			for(int j = 1; j <= 6; j++){
				
				if(j % 2 == 1){
					System.out.print((char)ch + " ");
				
				}else{
					System.out.print(x + " ");		
				}
				ch--;
				x--;
			}
			
			System.out.println();
		}
	}
}
