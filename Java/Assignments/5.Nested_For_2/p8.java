
/*	Pattern
 *
 * 		A b C d
 * 		E f G h
 * 		I j K I
 * 		M n O P
*/

class assign{
	
	public static void main(String[] args){
		
		int ch1 = 65, ch2 = 97;
		
		for(int i = 1; i <= 4; i++){

			for(int j = 1; j <= 4; j++){
				
				if(j % 2 == 1){	

					System.out.print((char)ch1 +" ");
				}else{
					System.out.print((char)ch2 + " ");
				}
				ch1++;
				ch2++;
			}
			
			System.out.println();
		}
	}
}
