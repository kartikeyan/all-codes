
/*	Pattern
 *
 *		14 15 16 17
 *		15 16 17 18
 *		16 17 18 19
 *		17 18 19 20
*/

class assign{
	
	public static void main(String[] args){
		
		int x = 14, y = 14;

		for(int i = 1; i <= 4; i++){
			
			x = y;

			for(int j = 1; j <= 4; j++){
				
				System.out.print(x + " ");
				x++;

			}
			
			y++;
			System.out.println();
		}
	}
}
