
/*	Pattern
 *
 *		A B C D
 *		B C D
 *		C D
 *		D
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4;

		char ch1 = 'A', ch2 = 'A';

		for(int i = 1; i <= n; i++){
			
			ch1 = ch2;

			for(int j = 1; j <= n - i + 1; j++){
				
				System.out.print(ch1 + " ");
				ch1++;

			}
			
			ch2++;
			System.out.println();
		}
	}
}
