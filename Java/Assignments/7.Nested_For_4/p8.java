
/*	Pattern
 *
 *		10
 *		I  H
 *		7  6 5
 *		D  C B A
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = n * (n + 1) / 2;

		char ch = 'J';

		for(int i = 1; i <= n; i++){

			for(int j = 1; j <= i; j++){
				
				if(i % 2 == 1){

					System.out.print(x + " ");
				}else{
					System.out.print(ch + " ");
					
				}

				x--;
				ch--;

			}
			
			System.out.println();
		}
	}
}
