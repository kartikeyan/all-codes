
/*	Pattern
 *
 *		F
 *		E 1
 *		D 2 E
 *		C 3 D 4
 *		B 5 C 6 D
 *		A 7 B 8 C 9
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = 1, y = 1;

		for(int i = 1; i <= n; i++){
			
			x = y;

			for(int j = 1; j <= i; j++){
				
				System.out.print(x + " ");
				x++;

			}
			
			y++;
			System.out.println();
		}
	}
}
