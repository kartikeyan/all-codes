
/*	Pattern
 *
 *		1 2 3 4
 *		2 3 4
 *		3 4
 *		4
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = 1, y = 1;

		for(int i = 1; i <= n; i++){
			
			x = y;

			for(int j = 1; j <= n - i + 1; j++){
				
				System.out.print(x + " ");
				x++;

			}
			
			y++;
			System.out.println();
		}
	}
}
