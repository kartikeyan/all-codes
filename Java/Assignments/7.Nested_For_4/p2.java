
/*	Pattern
 *
 *		1
 *
 *		3  4
 *
 *		6  7  8
 *
 *		10 11 12 13
 *
 *		15 16 17 18 19
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 5, x = 1, y = 1;

		for(int i = 1; i <= n; i++){

			x = y;
			
			for(int j = 1; j <= i; j++){
				
				System.out.print(x + " ");
				x++;

			}
			
			y+=2;
			System.out.println();
		}
	}
}
