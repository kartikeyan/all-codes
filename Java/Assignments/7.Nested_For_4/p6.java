
/*	Pattern
 *
 *		1
 *		2 3
 *		3 4 5
 *		4 5 6 7
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = 1, y = 1;

		for(int i = 1; i <= n; i++){
			
			x = y;

			for(int j = 1; j <= i; j++){
				
				System.out.print(x + " ");
				x++;

			}
			
			y++;
			System.out.println();
		}
	}
}
