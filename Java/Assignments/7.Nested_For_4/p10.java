
/*	Pattern
 *
 *		1
 *		8   9
 *		9   64  25
 *		64  25  216  49
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = 1, y = 1;

		for(int i = 1; i <= n; i++){
			
			x = y;

			for(int j = 1; j <= i; j++){
				
				if(x % 2 == 1){

					System.out.print(x*x+ " ");
				}else{
					System.out.print(x*x*x + " ");
					
				}

				x++;

			}
			
			y++;
			System.out.println();
		}
	}
}
