
/*	Pattern
 *
 *		10
 *
 *		10  9
 *
 *		9   8  7
 *
 *		7   6  5  4
*/

class assign{
	
	public static void main(String[] args){
		
		int n = 4, x = n * (n + 1) / 2, y;

		for(int i = 1; i <= n; i++){
			
			for(int j = 1; j <= i; j++){
				
				System.out.print(x + " ");
				x--;

			}
			
			y = x+1;
			System.out.println();
		}
	}
}
