
/*	Prog : WAP to print the numbers divisible by 5 from 1 to 50 & the number is even also print the
 *	count of even numbers
 *
 *	Input : Enter a lower limit : 1
 *		Enter a upper limit : 50
 *
 *	Ouput : 10,20,30,40,50
 *		count = 5
*/		

import java.io.*;

class sol{

	static void divisibleBy5(int lower, int upper){
		
		int count = 0;

		for(int i = lower; i <= upper; i++){
			
			if(i % 5 == 0 && i % 2 == 0){
				
				System.out.println(i);
				count++;
			}
		}

		System.out.println("count is " + count);
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Lower limit");
		int lower = Integer.parseInt(br.readLine());

	
		System.out.println("Enter Upper limit");
		int upper = Integer.parseInt(br.readLine());

		divisibleBy5(lower,upper);
	}
}
