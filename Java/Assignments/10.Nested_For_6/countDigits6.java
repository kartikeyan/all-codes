
/*	Prog : WAP to print count of digits in those numbers
 *
*/

import java.io.*;

class sol{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter How many numbers do u want");
		int input = Integer.parseInt(br.readLine());
	
		int count = 0;
		
		do{
			
			System.out.println("Enter Number : ");
			int num = Integer.parseInt(br.readLine());
			
			int temp = num;
			
			int cnt1 = 0;

			while(temp != 0){
				
				cnt1++;
				temp /= 10;
			}

			System.out.println("Number " + num + " has " + cnt1 + "digits ");
			
			count++;
		}while(count != input);
	}
}
