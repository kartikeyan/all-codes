
/*	Prog : WAP to take range as input from the user and print composite numbers
 *
 *	Input : start = 1
 *		end = 20
 *	Ouput : Composite numbers between start and end is
 *		4 6 8 9 10 12 14 15 16 18 20
*/

import java.io.*;

class sol{

	static void composite(int start, int end){
		
		for(int i = start; i <= end; i++){
			
			int count = 0;
			
			int n = i;

			for(int j = 1; j <= n; j++){
				
				if(i % j == 0){
					
					count++;
				}
			}

			if(count > 2){

				System.out.println("Composite " + n);
			}
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End");
		int end = Integer.parseInt(br.readLine());

		composite(start, end);
	}
}
