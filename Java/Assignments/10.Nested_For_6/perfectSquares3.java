
/*	Prog : WAP to take range as input from the user and print perfect squzres
 *
 *	Input : start = 1
 *		end = 20
 *	Ouput : Composite numbers between start and end is
 *		1 4 9 16 
*/

import java.io.*;

class sol{

	static void squares(int start, int end){
		
		for(int i = start; i <= end; i++){
			
			System.out.println("Squares " + i*i);	
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End");
		int end = Integer.parseInt(br.readLine());

		squares(start, end);
	}
}
