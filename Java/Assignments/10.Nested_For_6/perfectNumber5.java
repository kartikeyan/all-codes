
/*	Prog : WAP to take range as input from the user and print perfect numbers
 *
 *	Input : start = 1
 *		end = 100
 *	Ouput : Perfect numbers between start and end is
 *		
*/

import java.io.*;

class sol{

	static void perfect(int start, int end){
		
		for(int i = start; i <= end; i++){
			
			int sum = 0;
			
			int n = i;

			for(int j = 1; j < n; j++){
				
				if(n % j == 0){
					
					sum += j;
				}
			}

			if(sum == n){

				System.out.println("Perfect " + n);
			}
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End");
		int end = Integer.parseInt(br.readLine());

		perfect(start, end);
	}
}
