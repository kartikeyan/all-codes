
/*	Prog : WAP to take range as input from the user and print reverse of all numbers
 *
 *	Input : start = 1
 *		end = 20
 *	
*/

import java.io.*;

class sol{

	static void reverse(int start, int end){
		
		for(int i = start; i <= end; i++){
			
			int num = i;

			int rev = 0;

			while(num != 0){
				
				rev = rev * 10 + num % 10;
				num /= 10;
			}

			System.out.println(rev);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End");
		int end = Integer.parseInt(br.readLine());

		reverse(start, end);
	}
}
