
/*	Prog : WAP to take range as input from the user and print palindrome.
 *
 *	Input : start = 1
 *		end = 20
 *	
*/

import java.io.*;

class sol{

	static void strong(int start, int end){
		
		for(int i = start; i <= end; i++){
			
			int num = i;

			int sum = 0;

			while(num != 0){
				
				int rem = num % 10;

				int fact = 1;

				for(int j = 1; j <= rem; j++){
					
					fact *= j;
				}

				sum += fact;
				num /= 10;
			}
			
			if(sum == i){
				System.out.println(i);
			}
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Start");
		int start = Integer.parseInt(br.readLine());

		System.out.println("Enter End");
		int end = Integer.parseInt(br.readLine());

		strong(start, end);
	}
}
