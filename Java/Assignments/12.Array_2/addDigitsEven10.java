
//	WAP to merge 2 given arrays

import java.io.*;

class sol{
                       
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array 1 Size");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr1.length; i++){
			
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array 1 elements are ");
                for(int i = 0; i < arr1.length; i++){

                        System.out.println(arr1[i]);
                }
		
		System.out.println("Elements havin additon of Digits is even are");		
		for(int i = 0; i < arr1.length; i++){

			if(addDigits(arr1[i]) % 2 == 0){
				
				System.out.println(arr1[i]);
			}
		}
	}

	static int addDigits(int n){
			
		int sum = 0;

		while(n != 0){
				
			sum += n % 10;
			n = n / 10;
		}

		return sum;
	}
}
