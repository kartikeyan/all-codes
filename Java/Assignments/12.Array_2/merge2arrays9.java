
//	WAP to merge 2 given arrays

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array 1 Size");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr1.length; i++){
			
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array 1 elements are ");
                for(int i = 0; i < arr1.length; i++){

                        System.out.println(arr1[i]);
                }
			
		System.out.println("Enter Array 2 Size");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[size2];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr2.length; i++){
			
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array 2 elements are");
		for(int i = 0; i < arr2.length; i++){
			
			System.out.println(arr2[i]);
		}
		
		int size3 = size1+size2;

		int arr3[] = new int[size3];

		for(int i = 0; i < arr1.length; i++){
			
			arr3[i] = arr1[i];
		}

		for(int i = 0; i < arr2.length; i++){
			
			arr3[arr1.length + i] = arr2[i];
		}

		System.out.println("Arrays 3 elements are ");
		for(int i = 0; i < arr3.length; i++){
			
			System.out.println(arr3[i]);
		}
	}
}
