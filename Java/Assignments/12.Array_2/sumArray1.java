
import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		int sum = 0;

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array Elements Are");
		for(int i = 0; i < arr.length; i++){
			
			System.out.println(arr[i]);
			sum += arr[i];
		}

		System.out.println("Array Sum is " + sum);
	}
}
