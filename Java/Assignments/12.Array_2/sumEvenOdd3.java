
//	WAP to find the number the sum of even and odd numbers in array

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		int evenSum = 0, oddSum = 0;

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] % 2 == 0){
				
				evenSum += arr[i];
			}else{
				oddSum += arr[i];
			}
		}

	
		System.out.println("Even Sum is " + evenSum + " Odd Sum is " + oddSum);
	}
}
