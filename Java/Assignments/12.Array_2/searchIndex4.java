
//	WAP to search a specific element from an array and return its index.

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());

		}

		System.out.println("Enter Search Element");
		int search = Integer.parseInt(br.readLine());

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == search){
					
				System.out.println("Element found at index " + i);
				break;
			}

		}
	}
}
