
//	WAP to find the uncommon elements from the two arrays

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array 1 Size");
		int size1 = Integer.parseInt(br.readLine());

		int arr1[] = new int[size1];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr1.length; i++){
			
			arr1[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array 1 elements are ");
                for(int i = 0; i < arr1.length; i++){

                        System.out.println(arr1[i]);
                }
			
		System.out.println("Enter Array 2 Size");
		int size2 = Integer.parseInt(br.readLine());

		int arr2[] = new int[size2];
		
		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr2.length; i++){
			
			arr2[i] = Integer.parseInt(br.readLine());
		}

		System.out.println("Array 2 elements are");
		for(int i = 0; i < arr2.length; i++){
			
			System.out.println(arr2[i]);
		}

	
		
/*		int i = 0, j = 0, k = 0;

		while(i < arr1.length && j < arr2.length){
			
			if(arr1[i] < arr2[j]){
				
				System.out.println(arr1[i]);
				i++;
				k++;
			}
			else if(arr2[j] < arr1[i]){
				
				System.out.println(arr2[j]);
				j++;
				k++;
			}
			else{
				i++;
				j++;
			}
		}

		while(i < arr1.length){
			
			System.out.println(arr1[i]);
			i++;
			k++;
		}

		while(j < arr2.length){
			
			System.out.println(arr2[j]);
			j++;
			k++;
		}
*/
		int arr3[] = new int[arr1.length + arr2.length];
		int k = 0;

		for(int i = 0; i < arr1.length; i++){

			int flag = 0;
			
			for(int j = 0; j < arr2.length; j++){
				
				if(arr1[i] == arr2[j]){
					
					flag = 1;
				}
			}
			
			if(flag == 0){
				arr3[k++] = arr1[i];
			}
		}

		for(int j = 0; j < arr2.length; j++){
			
			int flag = 0;

			for(int i = 0; i < arr1.length; i++){
				
				if(arr2[j] == arr1[i]){
					
					flag = 1;
				}
			}
			
			if(flag == 0){
				arr3[k++] = arr2[j];
			}
		}
		
		System.out.println("UnCommon Elements are");		
		for(int i = 0; i < arr3.length; i++){
			
			System.out.println(arr3[i]);
		}
	}
}
