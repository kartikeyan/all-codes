
//	WAP to find the number of even and odd integers in given array

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter Array Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];
		
		int evenCount = 0, oddCount = 0;

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
			if(arr[i] % 2 == 0){
				
				evenCount++;
			}else{
				oddCount++;
			}
		}

	
		System.out.println("Even count is " + evenCount + " Odd Count is " + oddCount);
	}
}
