
/*	D4 C3 B2 A1
 *	A1 B2 C3 D4
 *	D4 C3 B2 A1
 *	A1 B2 C3 D4
*/

import java.io.*;

class sol{
	
	public static void main(String[] args) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Rows");

		int row = Integer.parseInt(br.readLine());

		for(int i = 1; i <= row; i++){

			int x = row, y = row-3; 
			char ch1 = 68, ch2 = 65;
			
			for(int j = 1; j <= row; j++){
				
				if(i % 2 == 0){
					
					System.out.print(ch2 +""+ y + " ");
					ch2++; y++;
				}else{
					System.out.print(ch1 +""+ x + " ");
					ch1--; x--;	
				}
			}

			System.out.println();
		}
	}
}
