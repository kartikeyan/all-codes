
//	Prog 4 : Min Max

class Sol{
	
	public static void main(String[] s){
		
		int arr[] = {5,2,1,6,7,9};

		int max = arr[0], min = arr[0];

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] > max){
				
				max = arr[i];
			}
			if(arr[i] < min){
				
				min = arr[i];
			}
		}

		System.out.println("Max is " + max);

		System.out.println("Min is " + min);
	}
}
