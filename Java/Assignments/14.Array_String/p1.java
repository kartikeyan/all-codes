
class Sol{
	
	public static void main(String[] s){
		
		int row = 4;

		for(int i = 1; i <= 4; i++){
			
			int ch = 65 + i - 1;

			for(int j = 1; j <= row - i + 1; j++){
				
				System.out.print((char)ch + " ");
				
				ch+=2;
			}
	
			System.out.println();
		}
	}
}
