
//	Prog 6 : Reverse a String

class Sol{

	static String myReverse(String str){
		
		StringBuffer sb = new StringBuffer(str);

		str = sb.reverse().toString();

		return str;
	}
	
	public static void main(String[] s){
		
		String str = "kartik";

		System.out.println(myReverse(str));
	}
}
