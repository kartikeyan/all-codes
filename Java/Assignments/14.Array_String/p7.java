
//	7 - Perfect Divisors

class Sol{
	
	public static void main(String[] s){
		
		int n = 124, temp = n;

		int rev = 0;

		while(n != 0){
			
			int rem = n % 10;
			
			if(temp % rem == 0){
				
				System.out.println("Perfect Divisor is " + rem);
			}

			n = n / 10;
		}
	}
}
