
//	8 - 	

class Sol{

	public static int[] twoSum(int arr[], int newArr[], int target){
		
		int k = 0;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] + arr[j] == target){
					
					newArr[k] = i;
					newArr[k+1] = j;
					break;
				}
			}
		}

		return newArr;
	}
	
	public static void main(String[] s){
		
		int arr[] = {3,6,16,15};

		int target = 9;

		int newArr[] = new int[2];

		int arr1[] = twoSum(arr, newArr, target);

		for(int i = 0; i < arr1.length; i++){
			
			System.out.println(arr1[i]);
		}
	}
}
