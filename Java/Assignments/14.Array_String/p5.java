
//	Prog 5 : Remove occurances

class Sol{

	static String myReplace(char old, char newC, String str){
		
		char arr[] = str.toCharArray();

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] == old){
				
				arr[i] = newC;
			}
		}

		str = new String(arr);

		return str;
	}
	
	public static void main(String[] s){
		
		String str = "xartix";

		char old = 'x';

		char newC = 'k';

		System.out.println(myReplace(old, newC, str));
	}
}
