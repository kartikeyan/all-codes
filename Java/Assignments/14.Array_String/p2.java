
class Sol{
	
	public static void main(String[] s){
		
		int row = 4;

		int higher = 69;
		int lower = 97;

		for(int i = 1; i <= row; i++){
			
			for(int space = 1; space < i; space++){
				
				System.out.print("");
			}

			for(int j = 1; j <= row - i + 1; j++){
					
				if(j % 2 == 0){
					System.out.print((char)higher + " ");
					higher--;
				}else{
					
					System.out.print((char)lower + " ");
					lower++;
				}
			}	
			System.out.println();
		}
	}
}
