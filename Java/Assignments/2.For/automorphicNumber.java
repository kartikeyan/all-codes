
class assign{
	
	public static void main(String[] args){
		
		int n = 25;

		int square = n * n;

		while(n != 0){
			
			if(n % 10 != square % 10){
				
				System.out.println("Not a automorhic number");
				break;
			}

			square = square / 10;

			n = n / 10;
		}
		
		System.out.println("Automorphic Number");

	}
}
