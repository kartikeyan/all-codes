
/*
 	Take a choice from the user, Show this to user

		Whats your interest?
			1] Movies
			2] Series

		If user enters 1:
			Movies you wish to watch today
				1] Founder
				2] Snowden
				3] Jobs
				4] Her
				5] Social Network
				6] Wall-E
				7] AI

				Enter Your Choice : 2
					
					2:Snowden

		If user enters 2:
			Best Series to watch
				1] Silicon Valley
				2] Devs
				3] The IT crowd
				4] Mr Robot

		Print USers Choice


	Write a Real Time Example like above 
*/

import java.io.*;

class Result{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int choice;

		System.out.println("Welcome to Bajaj Showroom");

		System.out.println("Enter bike type you want to buy");

		System.out.println("1.Fully Faired");

		System.out.println("2.Naked Bikes");

		System.out.println("Enter Your Choice 1 or 2 : ");

		choice = Integer.parseInt(br.readLine());

		switch(choice){
			
			case 1:
				{	
					int ch;
					System.out.println("Fully Faired Options are");
					System.out.println("1.Bajaj pulsar 220 F");
					System.out.println("2.Baja pulsar RS 200");
					System.out.println("3.Bajaf pulsar F 250");
					
					System.out.println("Enter Your Choice");

					ch = Integer.parseInt(br.readLine());

					switch(ch){
						
						case 1:
							System.out.println("You selected Bajaj Pulsar 220 F");
							break;
						case 2:
							System.out.println("You selected Bajaj Pulsar RS 200");
							break;
						case 3:
							System.out.println("You selected Bajaj Pulsar F 250");
							break;
			
						default:
							System.out.println("Invalid Choice");	
					}
				}
				break;

			case 2:{
				
					int ch;
					System.out.println("Naked Bikes Options");
					System.out.println("1.Pulsar NS 200");
					System.out.println("2.Pulsar NS 125");
					System.out.println("3.Pulsar NS 160");
					System.out.println("4.Pulsar N 160");
	
					System.out.println("Enter Your Choice");

					ch = Integer.parseInt(br.readLine());

					switch(ch){
						
						case 1:
							System.out.println("You selected Pulsar NS 200");
							break;
						case 2:
							System.out.println("You selected Pulsar Ns 125");
							break;
						case 3:
							System.out.println("You selected Pulsar NS 160");
							break;
						case 4:
							System.out.println("You selected Pulsar N 160");
							break;
						default:
							System.out.println("InValid Choice");
					}
			}
			break;

			default:
				System.out.println("INvalid Choice");
		}
	}
}
