
/*
 	WAP in which students should enter marks of 5 different subjects.If all subject have above passing marks add them and 
	provide to switch case to print grades(first class, second class,....).If student get fail in any subject program 
	should print "You failed in exam"
*/

import java.io.*;

class Result{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int mathsMarks;
		System.out.println("Enter Maths Marks");
		mathsMarks = Integer.parseInt(br.readLine());

		int engMarks;
		System.out.println("Enter English Marks");
		engMarks = Integer.parseInt(br.readLine());

		int histMarks;
		System.out.println("Enter History Marks");
		histMarks = Integer.parseInt(br.readLine());

		int scienceMarks;
		System.out.println("Enter Science Marks");
		scienceMarks = Integer.parseInt(br.readLine());

		int marathiMarks;
		System.out.println("Enter Marathi Marks");
		marathiMarks = Integer.parseInt(br.readLine());

		if(mathsMarks < 35 || engMarks < 35 || histMarks < 35 || scienceMarks < 35 || marathiMarks < 35){
			
			System.out.println("You Failed");
		}else{
			
			double avg = (mathsMarks + engMarks + histMarks + scienceMarks + marathiMarks) / 5;

			char ch;
			
			if(avg > 75){
				
				ch = 'A';
			}
			else if(avg > 60){
				
				ch = 'B';
			}
			else if(avg > 50){
				
				ch = 'C';
			}
			else if(avg > 40){
				
				ch = 'D';
			}
			else if(avg >= 35){
				
				ch = 'E';
			}else{
				ch = 'F';
			}

			switch(ch){
				
				case 'A':
					System.out.println("First Class with Distiction");
					break;
				case 'B':
					System.out.println("First Class");
					break;
				case 'C':
					System.out.println("Second Class");
					break;
				case 'D':
					System.out.println("Third Class");
					break;
				case 'E':
					System.out.println("Passs");
					break;
				case 'F':
					System.out.println("Fail");
					break;
			}
		}
	}
}
