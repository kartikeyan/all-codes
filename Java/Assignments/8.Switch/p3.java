
/*
 	WAP that asks the user should enter two numbers if both the numbers are positive multiply then provide to switch case
	to verify whether the number is even or odd, if the user enters any negative number then program should terminate by
	saying "Sorry Negative Numbers not allowed"
*/

import java.io.*;

class Result{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int num1, num2;

		System.out.println("Enter Number 1");
		num1 = Integer.parseInt(br.readLine());

		System.out.println("Enter Number 2");
		num2 = Integer.parseInt(br.readLine());

		if(num1 < 0 || num2 < 0){
			
			System.out.println("Negative Numbers not allowed");
		}else{
		
			switch((num1 * num2) % 2){
			
				case 0:
					System.out.println("Multiplication is even");
					break;
				case 1:
					System.out.println("Multiplication is odd");
					break;
			}
		}
	}
}
