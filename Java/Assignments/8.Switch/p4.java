
/*
 	Take a choice from the user, Show this to user

		Whats your interest?
			1] Movies
			2] Series

		If user enters 1:
			Movies you wish to watch today
				1] Founder
				2] Snowden
				3] Jobs
				4] Her
				5] Social Network
				6] Wall-E
				7] AI

				Enter Your Choice : 2
					
					2:Snowden

		If user enters 2:
			Best Series to watch
				1] Silicon Valley
				2] Devs
				3] The IT crowd
				4] Mr Robot

		Print USers Choice
*/

import java.io.*;

class Result{
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		int choice;

		System.out.println("What's Your Interest ?");

		System.out.println("1.Movies");

		System.out.println("2.Series");

		System.out.println("Enter Your Choice 1 or 2 : ");

		choice = Integer.parseInt(br.readLine());

		switch(choice){
			
			case 1:
				{	
					int wish;
					System.out.println("Movie you wish to watch");
					System.out.println("1.Founder");
					System.out.println("2.Snowden");
					System.out.println("3.Jobs");
					System.out.println("4.Her");
					System.out.println("5.Social Network");
					System.out.println("6.Wall-E");
					System.out.println("7.AI");
					System.out.println("Enter Your Choice");

					wish = Integer.parseInt(br.readLine());

					switch(wish){
						
						case 1:
							System.out.println("You wished to watch Founder");
							break;
						case 2:
							System.out.println("You wished to watch Snowden");
							break;
						case 3:
							System.out.println("You wished to watch Jobs");
							break;
						case 4:
							System.out.println("You wished to watch Her");
							break;
						case 5:
							System.out.println("You wished to watch Social Network");
							break;
						case 6:
							System.out.println("You wished to watch Wall E");
							break;
						case 7:
							System.out.println("You wished to watch AI");
							break;
						default:
							System.out.println("Invalid Choice");	
					}
				}
				break;

			case 2:{
				
					int wish;
					System.out.println("Best Series to watch");
					System.out.println("1.Silicon Valley");
					System.out.println("2.Devs");
					System.out.println("3.The IT crowd");
					System.out.println("4.Mr Robot");
	
					System.out.println("Enter Your Choice");

					wish = Integer.parseInt(br.readLine());

					switch(wish){
						
						case 1:
							System.out.println("You wished to watch Silicon Valley");
							break;
						case 2:
							System.out.println("You wished to watch Devs");
							break;
						case 3:
							System.out.println("You wished to watch The IT crowd");
							break;
						case 4:
							System.out.println("You wished to watch Mr. Robot");
							break;
						default:
							System.out.println("InValid Choice");
					}
			}
			break;

			default:
				System.out.println("INvalid Choice");
		}
	}
}
