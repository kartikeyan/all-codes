
/*	Prog : WAP to print count of digits in elements of Array
 *
 *	Input : [02,255,2,1554]
 *	Ouput : 2 3 1 4
*/

import java.io.*;

class sol{

	static void countDigits(int arr[], int size){
		
		for(int i = 0; i < size; i++){
			
			int count = 0;
			
			int n = arr[i];

			while(n != 0){
				
				count++;
				n = n / 10;
			}

			System.out.println("Digits are " + count);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		countDigits(arr, size);
	}
}
