
/*	Prog : WAP to reverse each element of Array
 *
 *	Input : [10,25,252,36,564]
 *	Ouput : 01 52 252 63 465
*/

import java.io.*;

class sol{

	static void reverse(int arr[], int size){
		
		for(int i = 0; i < size; i++){
			
			int rev = 0;
			
			int n = arr[i];

			while(n != 0){
				
				rev = rev * 10 + n % 10;
				n = n / 10;
			}

			System.out.println("Reverse elements are " + rev);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		reverse(arr, size);
	}
}
