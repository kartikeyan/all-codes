
/*	Prog : WAP to find prime number from an array and return it's index.
 *
 *	Input : [10,25,36,566,34,53,50,100]
 *	Ouput : Prime No 53 found at index : 5
*/

import java.io.*;

class sol{

	static void prime(int arr[], int size){
		
		for(int i = 0; i < size; i++){
			
			int count = 0;
			
			int n = arr[i];

			for(int j = 1; j <= n; j++){
				
				if(n % j == 0){
					
					count++;
				}
			}

			if(count == 2){

				System.out.println("Prime " + n + " found at index " + i);
			}
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		prime(arr, size);
	}
}
