
/*	Prog : WAP to print second min element in array
 *
 *	Input : [2,255,2,1554,15,65]
 *	Ouput : 15
*/

import java.io.*;

class sol{

	static void secondMax(int arr[], int size){
		
		int min1 = arr[0];

		for(int i = 0; i < size; i++){
			
			if(arr[i] < min1){
				
				min1 = arr[i];
			}
		}

		int min2 = Integer.MAX_VALUE;
		
		for(int i = 0; i < size; i++){
			
			if(arr[i] < min2 && arr[i] > min1){
				
				min2 = arr[i];
			}
		}

		System.out.println("Second Min is " + min2);
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		secondMax(arr, size);
	}
}
