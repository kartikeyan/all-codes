
/*	Prog : WAP to find palindrome number from an array and return it's index.
 *
 *	Input : [10,545]
 *	Ouput : Perfect no 545 found at index : 1
*/

import java.io.*;

class sol{

	static void palindrome(int num, int j){
		
		int rev = 0;
		int temp = num;

		while(num != 0){
			
			int rem = num % 10;
			rev = rev * 10 + rem;
			num /= 10;
		}

		if(rev == temp){
			
			System.out.println("Palindrome No " + temp + " found at index " + j);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i = 0; i < arr.length; i++){

			palindrome(arr[i], i);
		}
	}
}
