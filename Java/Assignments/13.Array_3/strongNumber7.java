
/*	Prog : WAP to find strong number from an array and return it's index.
 *
 *	Input : [10,25,252,36,564,145]
 *	Ouput : strong no 145 found at index : 5
*/

import java.io.*;

class sol{

	static void strong(int num, int j){

		int temp = num;
		int sum = 0;

		while(num != 0){
			
			int rem = num % 10;
			int fact = 1;

			for(int i = 1; i <= rem; i++){
				
				fact *= i;
			}

			sum += fact;
			num = num / 10;
		}
		
		if(sum == temp){
			
			System.out.println("Strong No " + temp + " found at index " + j);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i = 0; i < arr.length; i++){
			
			strong(arr[i], i);
		}
	}
}
