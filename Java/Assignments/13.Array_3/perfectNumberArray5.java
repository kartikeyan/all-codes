
/*	Prog : WAP to find perfect number from an array and return it's index.
 *
 *	Input : [10,25,252,496,564]
 *	Ouput : Perfect no 496 found at index : 3
*/

import java.io.*;

class sol{

	static void perfect(int num, int j){
		
		int sum = 0;

		for(int i = 1; i < num; i++){
			
			if(num % i == 0){
				
				sum += i;
			}
		}

		if(sum == num){
			
			System.out.println("Perfect No " + num + " found at index " + j);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}
		
		for(int i = 0; i < arr.length; i++){

			perfect(arr[i], i);
		}
	}
}
