
/*	Prog : WAP to find composite number from an array and return it's index.
 *
 *	Input : [1,2,3,5,6,7]
 *	Ouput : Composite 6 found at index : 4
*/

import java.io.*;

class sol{

	static void composite(int arr[], int size){
		
		for(int i = 0; i < size; i++){
			
			int count = 0;
			
			int n = arr[i];

			for(int j = 1; j <= n; j++){
				
				if(i % j == 0){
					
					count++;
				}
			}

			if(count > 2){

				System.out.println("Composite " + n + " found at index " + i);
			}
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		composite(arr, size);
	}
}
