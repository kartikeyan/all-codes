
/*	Prog : WAP to print second max element in array
 *
 *	Input : [2,255,2,1554,15,65]
 *	Ouput : 255
*/

import java.io.*;

class sol{

	static void secondMax(int arr[], int size){
		
		int max1 = arr[0], max2 = 0;

		for(int i = 0; i < size; i++){
			
			if(arr[i] > max2){
				
				max2 = max1;
				max1 = arr[i];
			}
		}

		System.out.println("Second Max is " + max2);
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		secondMax(arr, size);
	}
}
