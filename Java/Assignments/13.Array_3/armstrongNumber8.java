
/*	Prog : WAP to find armstrong number from an array and return it's index.
 *
 *	Input : [10,25,252,36,564,153,55,89]
 *	Ouput : Armstrong no 153 found at index : 4
*/

import java.io.*;

class sol{

	static void armstrong(int num, int j){
		
		int temp = num;
		int sum = 0;
		int count = 0;

		while(num != 0){
			
			count++;
			num = num / 10;
		}

		num = temp;

		while(num != 0){
			
			int rem = num % 10;
			
			int mult = 1;

			for(int i = 1; i <= count; i++){
				
				mult *= rem;
			}

			sum = sum + mult;

			num = num / 10;
		}

		if(temp == sum){
			
			System.out.println("Armstrong No " + temp + " is found at index " + j);
		}
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter Size");
		int size = Integer.parseInt(br.readLine());

		int arr[] = new int[size];

		System.out.println("Enter Array Elements");
		for(int i = 0; i < arr.length; i++){
			
			arr[i] = Integer.parseInt(br.readLine());
		}

		for(int i = 0; i < arr.length; i++){
			
			armstrong(arr[i], i);
		}
	}
}
