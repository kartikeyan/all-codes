
/*	Pattern
 *
 *		d d d d
 *		c c c c
 *		b b b b
 *		a a a a
 *
*/

class assign{
	
	public static void main(String[] args){
		
		int ch = 100;

		for(int i = 1; i <= 4; i++){
			
			for(int j = 1; j <= 4; j++){
				
				System.out.print((char) ch + " ");
				
			}
			
			ch--;
			System.out.println();
		}
	}
}
