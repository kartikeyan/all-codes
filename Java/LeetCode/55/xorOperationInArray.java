
class sol{
	
	static int xorOperation(int n, int start){
		
		int ans = 0;

		for(int i = 0; i < n; i++){
			
			ans = ans ^ (start + 2 * i);
		}

		return ans;
	}

	public static void main(String[] s){
		
		int n = 5, start = 0;

		int ret = xorOperation(n, start);

		System.out.println(ret);
	}
}
