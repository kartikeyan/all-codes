
/*
 	144. Binary Tree PostOrder Traversal

	Given the root of a binary tree, return the postorder traversal of its nodes' values.

	Input: root = [1,null,2,3]
	Output: [3,2,1]

	Input: root = []
	Output: []

	Input: root = [1]
	Output: [1]

	Constraints:

	    The number of the nodes in the tree is in the range [0, 100].
	    100 <= Node.val <= 100.
*/

class Node{
	
	int data;

	Node left, right;

	Node(int item){
		
		data = item;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	BinaryTree(){
		
		root = null;
	}

	void printPostOrder(Node node){
		
		if(node == null){
			
			return;
		}

		printPostOrder(node.left);

		printPostOrder(node.right);

		System.out.println(node.data + " ");

	}
	
	void printPostOrder(){
		
		printPostOrder(root);
	}

	public static void main(String[] s){
		
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);
//		tree.root.left = new Node(2);
		tree.root.right = new Node(2);
	
		tree.root.right.left = new Node(3);
//		tree.root.left.left = new Node(4);
//		tree.root.left.right = new Node(5);

		System.out.println("\nPostOrder traversal of binary tree is ");

		tree.printPostOrder();
	}
}
