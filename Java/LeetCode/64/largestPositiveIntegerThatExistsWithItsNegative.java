
/*
 
   	2441. Largest Positive Integer That Exists With Its Negative
	
	Given an integer array nums that does not contain any zeros, find the largest positive integer k such that -k also exists in the array.

	Return the positive integer k. If there is no such integer, return -1.


	Input: nums = [-1,2,-3,3]
	Output: 3
	Explanation: 3 is the only valid k we can find in the array.

	Input: nums = [-1,10,6,7,-7,1]
	Output: 7
	Explanation: Both 1 and 7 have their corresponding negative values in the array. 7 has a larger value.

	Input: nums = [-10,8,6,7,-2,-3]
	Output: -1
	Explanation: There is no a single valid k, we return -1.

	Constraints:

	    1 <= nums.length <= 1000
	    -1000 <= nums[i] <= 1000
	    nums[i] != 0
*/

class Sol{

	static void merge(int nums[], int start, int mid, int end){

	    int ele1 = mid - start + 1, ele2 = end - mid;

//	    int nums1[ele1], nums2[ele2];
		
	    int nums1[] = new int[ele1];
	    int nums2[] = new int[ele2];

	    for(int i = 0; i < ele1; i++){
        	nums1[i] = nums[start + i];
	    }

	    for(int j = 0; j < ele2; j++){
	        nums2[j] = nums[mid + 1 + j];
	    }

	    int itr1 = 0, itr2 = 0, itr3 = start;

	    while(itr1 < ele1 && itr2 < ele2){

	        if(nums1[itr1] < nums2[itr2]){
        	    nums[itr3] = nums1[itr1];
	            itr1++;
        	}else{
	            nums[itr3] = nums2[itr2];
        	    itr2++;
	        }

        	itr3++;
	    }	

	    while(itr1 < ele1){

	        nums[itr3] = nums1[itr1];
	        itr1++;
        	itr3++;
	    }

	    while(itr2 < ele2){
	        nums[itr3] = nums2[itr2];
	        itr2++;
        	itr3++;
	    }
	}

	static void mergeSort(int nums[],  int start, int end){

	    if(start < end){
	        int mid = start + (end - start) / 2;

        	mergeSort(nums, start, mid);
	        mergeSort(nums, mid+1, end);

        	merge(nums, start, mid, end);
	    }
	}	

	static int max(int x, int y){
		
		if(x > y){
			
			return x;
		}

		return y;
	}

	static int findMaxK(int arr[]){
		
		// Approach 1

/*	
		int x = -1;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				if(arr[i] == -arr[j] && Math.abs(arr[i]) > x){
					
					x = Math.abs(arr[i]);
				}
			}
		}

		return x;
*/
		
		// Sorting and 2 Pointer Approach

		mergeSort(arr, 0, arr.length-1);

		int left = 0, right = arr.length-1;

		int x = 0;

		while(left < right){
			
			int sum = arr[left] + arr[right];

			if(sum == 0){
				
				x = max(x, max(arr[left], arr[right]));
			}
			else if(sum < 0){
				
				left++;
			}
			else{
				right--;
			}
		}

		return x == 0 ? -1 : x;
	}
	
	public static void main(String[] s){
		
		int arr[] = {-1,2,-3,3};

		int ret = findMaxK(arr);

		System.out.println(ret);
	}
}
