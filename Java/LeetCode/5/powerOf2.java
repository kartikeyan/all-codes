
/*	Program 1 : Power of Two
 *
 *	Given an integer n, return "true" if it is a power of two. Otherwise return "false".
 *	An integer n is a power of two, if there exists an integer x such that n == 2^x.
 *
 *	Input : n == 1			Input : n == 16			Input : n = 3
 *	Output : true			Output : true			Output : false
 *	Explanation : 2^0 = 1 		Explanation : 2^4 = 16
*/

import java.io.*;

class sol{

	static boolean isPowerOfTwo(int n){
		
		if(n == 1){
			
			return true;
		}
		if(n < 1){
			
			return false;
		}

		return isPowerOfTwo(n / 2);
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter num");

		int num = Integer.parseInt(br.readLine());

		boolean ret = isPowerOfTwo(num);

		System.out.println(ret);
	}
}
