
/*	Program 2 : Power of Three
 *
 *	Given an integer n, return "true" if it is a power of three. Otherwise return "false".
 *	An integer n is a power of two, if there exists an integer x such that n == 3^x.
 *
 *	Input : n == 27			Input : n == 0			Input : n = -1
 *	Output : true			Output : false			Output : false
 *	Explanation : 27 = 3^3 		Explanation : 36x = 0
*/

import java.io.*;

class sol{

	static boolean isPowerOfTwo(int n){
		
		if(n == 1){
			
			return true;
		}
		if(n < 1){
			
			return false;
		}

		return isPowerOfTwo(n / 3);
	}
	
	public static void main(String[] s) throws IOException{
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Enter num");

		int num = Integer.parseInt(br.readLine());

		boolean ret = isPowerOfTwo(num);

		System.out.println(ret);
	}
}
