
/*	Program 2 : Check if Array is Sorted and rotated
 *
 *	Given an array "nums", return "true", if the array was originally sorted in non-decreasing
 *	order, then rotated some number of positions(including zero).Otherwise, return "false"
 *
 *	Input : nums[] = {3,4,5,1,2};
 *	Ouput : true
*/

class sol{
	
	static boolean check(int arr[], int size){
		
		int count = 0;

		for(int i = 1; i < size; i++){
			
			if(arr[i-1] > arr[i]){
				
				count++;
			}
		}

		if(arr[size-1] > arr[0]){
			
			count++;
		}

		return count <= 1;
	}

	public static void main(String[] args){
		
		int arr[] = {0,1,0,3,12};

		int size = arr.length;

		boolean ret = check(arr, size);

		if(ret == true){
			
			System.out.println("Array is sorted and rotated");
		}else{
			System.out.println("Array is not Sorted and rotated");
		}
		
	}
}
