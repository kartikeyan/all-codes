
/*
 	100. Same Tree

	Companies

	Given the roots of two binary trees p and q, write a function to check if they are the same or not.

	Two binary trees are considered the same if they are structurally identical, and the nodes have the same value.

	Input: p = [1,2,3], q = [1,2,3]
	Output: true

	Input: p = [1,2], q = [1,null,2]
	Output: false
	
	Input: p = [1,2,1], q = [1,1,2]
	Output: false

	Constraints:

	    The number of nodes in both trees is in the range [0, 100].
	    -104 <= Node.val <= 104

*/

class Node{
	
	int data;
	Node left, right;

	Node(int val){

		data = val;
		left = right = null;
	}
}

class BinaryTree{

	Node root1, root2;

	static boolean sameTree(Node root1, Node root2){
		
		if(root1 == null && root2 == null){
			
			return true;
		}

		if(root1 == null || root2 == null){
			
			return false;
		}

		if(root1.data != root2.data){
			
			return false;
		}

		return sameTree(root1.left, root2.left) && sameTree(root1.right, root2.right);
	}

	public static void main(String[] s){
	
		BinaryTree tree = new BinaryTree();

	        tree.root1 = new Node(1);
	
	        tree.root1.left = new Node(2);
        	tree.root1.right = new Node(3);

	        tree.root2 = new Node(1);

		tree.root2.left = new Node(2);
		tree.root2.right = new Node(3);

	        boolean ret = sameTree(tree.root1, tree.root2);

		if(ret == true){
			
			System.out.println("Same Tree");
		}else{
			
			System.out.println("Not Same Tree");
		}
	}
}
