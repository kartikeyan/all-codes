
/*
 	700. Search in a Binary Search Tree

	You are given the root of a binary search tree (BST) and an integer val.

	Find the node in the BST that the node's value equals val and return the subtree rooted with that node. If such a node does not exist, return null.

	Input: root = [4,2,7,1,3], val = 2
	Output: [2,1,3]

	Input: root = [4,2,7,1,3], val = 5
	Output: []

	Constraints:

	    The number of nodes in the tree is in the range [1, 5000].
	    1 <= Node.val <= 107
	    root is a binary search tree.
	    1 <= val <= 107
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

bool searchInBST(node *root, int ele){

	if(root == NULL){

		return false;
	}

	if(root->data == ele){
		
		return true;
	}

	if(root->data > ele){
		
		return searchInBST(root->left, ele);
	}else
		return searchInBST(root->right, ele);
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int find;
	printf("Enter Num to find\n");
	scanf("%d", &find);

	bool ret = searchInBST(root, find);
	if(ret == true){
		
		printf("Number found\n");
	}else
		printf("Not found\n");
}
