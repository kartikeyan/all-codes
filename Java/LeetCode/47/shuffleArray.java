
class recDemo{

	static void shuffle(int arr[], int size, int n, int nums[]){
		
		int i, j = 0;

		for(i = 0; i < size; i++){
			
			nums[j] = arr[i];
			nums[j+1] = arr[i+n];

			j += 2;
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {2,5,1,3,4,7};

		int size = arr.length;

//		System.out.println(size);

		int n = 3;

		int nums[] = new int[size];

//		System.out.println(size1);

		shuffle(arr, size, n, nums);

		for(int i = 0; i < size; i++){
			
			System.out.println(nums[i]);
		}
	}
}
