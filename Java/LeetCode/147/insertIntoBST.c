
/*
 	701. Insert into a Binary Search Tree

	You are given the root node of a binary search tree (BST) and a value to insert into the tree. Return the root node of the BST after the insertion. It is guaranteed that the new value does not exist in the original BST.

	Notice that there may exist multiple valid ways for the insertion, as long as the tree remains a BST after insertion. You can return any of them.

	Input: root = [4,2,7,1,3], val = 5
	Output: [4,2,7,1,3,5]

	Input: root = [40,20,60,10,30,50,70], val = 25
	Output: [40,20,60,10,30,50,70,null,null,25]


	Input: root = [4,2,7,1,3,null,null,null,null,null,null], val = 5
	Output: [4,2,7,1,3,5]

	Constraints:

	    The number of nodes in the tree will be in the range [0, 104].
	    -108 <= Node.val <= 108
	    All the values Node.val are unique.
	    -108 <= val <= 108
	    It's guaranteed that val does not exist in the original BST.
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{
	
	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){
	
	if(root == NULL){
		
		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;
		
		root = newNode;

		return root;
	}

	if(root->data > ele){
		
		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted
	
	if(root == NULL){
		
		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

node* insertInBST(node *root, int val){

	if(root == NULL){
		
		node* newNode = (node*)malloc(sizeof(node));

		newNode->data = val;

		newNode->left = NULL;

		newNode->right = NULL;

		root = newNode;

		return root;
	}

	if(val < root->data){
		
		root->left = insertInBST(root->left, val);
	}else{
		root->right = insertInBST(root->right, val);
	}

	return root;
}

void main(){
	
	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){
		
		scanf("%d", &ele);

		root = constructBST(root, ele);
	}
	
	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int nodeData;
	printf("Enter node data\n");
	scanf("%d", &nodeData);

	root = insertInBST(root, nodeData);
	
	printf("After Adding Node\n");
	printBST(root);
	printf("\n");
}
