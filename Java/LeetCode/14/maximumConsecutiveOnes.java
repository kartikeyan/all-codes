
/*	Program 1 : Maximum Consecutive Ones
 *
 *	Givan a binary array "nums", return the maximum number of consecutive 1 s in the array
 *
 *	Input : nums[] = {1,1,0,1,1,1},		Input : nums[] = {1,0,1,1,0,1}
 *	Ouput : 3				Ouput : 2
*/

class sol{
	
	static int findMaxConse(int arr[], int size){
		
		int count = 0, max = 0;

		for(int i = 0; i < size; i++){
			
			if(arr[i] == 1){
				
				count++;

				if(count > max){
					
					max = count;
				}
			}else{
				count = 0;
			}
		}

		return max;
	}

	public static void main(String[] s){
		
		int arr[] = {1,0,1,1,0,1};

		int size = arr.length;

		int ret = findMaxConse(arr,size);

		System.out.println(ret);
	}
}
