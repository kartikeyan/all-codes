
/*
 	867. Transpose Matrix

	Given a 2D integer array matrix, return the transpose of matrix.

	The transpose of a matrix is the matrix flipped over its main diagonal, switching the matrix's row and column indices.

	Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
	Output: [[1,4,7],[2,5,8],[3,6,9]]

	Input: matrix = [[1,2,3],[4,5,6]]
	Output: [[1,4],[2,5],[3,6]]

	Constraints:

	    m == matrix.length
	    n == matrix[i].length
	    1 <= m, n <= 1000
	    1 <= m * n <= 105
	    -109 <= matrix[i][j] <= 109
*/

class Sol{

	static void transpose(int arr[][], int size){
		
		int newArr[][] = new int[arr[0].length][arr.length];
		// arr[i].length == gives no. of elements in 1D array
		// arr.length == gives no. of rows

		for(int i = 0; i < arr.length; i++){
			
			for(int j = 0; j < arr[i].length; j++){
				
				newArr[j][i] = arr[i][j];
			}
		}
		
		for(int i = 0; i < arr.length; i++){

                        for(int j = 0; j < arr[i].length; j++){
 
                                System.out.println(newArr[i][j] + " ");
                        }
                }
	}
	
	public static void main(String[] s){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9}};

		int size = arr.length;

		transpose(arr, size);
	}
}
