
/*	Prog : Given an array os positive integers arr, return the sum of all possible odd-length subarrays of arr.
 *
 *	A subarray is a contiguous subsequence of the array
 *
 *	Input : arr = [1,4,2,5,3]
 *	Ouput : 58
 *	Expla :	The odd length subarrays of arr and their sums are :
 *
 *		[1] = 1
 *		[4] = 4
 *		[2] = 2
 *		[5] = 5
 *		[3] = 3
 *		[1,4,2] = 7
 *		[4,2,5] = 11
 *		[2,5,3] = 10
 *		[1,4,2,5,3] = 15
 *
 *		If we add all these together we get 58
 *
 *
 *	Input : arr = [1,2]
 *	Ouput : 3
 *	Expla : there are only 2 subarrays of odd length, [1] and [2].Their sum is 3
 *
 *
 *	Input : arr = [10,11,12]
 *	Ouput : 66
 *
 *	Constraints :
 *
 *		1 <= arr.length <= 100
 *
 *		1 <= arr[i] <= 1000
*/		

class odd{

	// Appraoch 1
/*	
	static int sumOddLength(int arr[]){
		
		int size = arr.length;

		int ans = 0;

		for(int i = 0; i < size; i++){

			for(int j = i; j < size; j += 2){
				
				for(int k = i; k <= j; k++){
					
					ans += arr[k];
				}
			}
		}

		return ans;

	}
*/

	// Apprach 2

/*	static int sumOddLength(int arr[]){
		
		int size = arr.length;

		int ans = 0;

		for(int i = 0; i < size; i++){
			
			int sum = 0;

			for(int j = i; j < size; j++){
				
				sum = sum + arr[j];

				if((j - i + 1) % 2 == 1){
					
					ans = ans + sum;
				}
			}
		}

		return ans;
	}

*/
	// Apprach 3

	static int sumOddLength(int arr[]){
		
		int size = arr.length;

		int ans = 0;

		for(int i = 0; i < size; i++){
			
			int sum = 0, len = 0;

			for(int j = i; j < size; j++){
				
				len++;
				sum = sum + arr[j];

				if(len % 2 == 1){
					
					ans = ans + sum;
				}
			}
		}

		return ans;

	}

	public static void main(String[] args){
		
		int arr[] = {10,11,12};

		int ret = sumOddLength(arr);

		System.out.println(ret);
	}
}
