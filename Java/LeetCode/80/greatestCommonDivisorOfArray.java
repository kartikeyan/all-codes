
/*
 	1979. Find Greatest Common Divisor of Array

	Given an integer array nums, return the greatest common divisor of the smallest number and largest number in nums.

	The greatest common divisor of two numbers is the largest positive integer that evenly divides both numbers.

	Input: nums = [2,5,6,9,10]
	Output: 2
	Explanation:
		The smallest number in nums is 2.
		The largest number in nums is 10.
		The greatest common divisor of 2 and 10 is 2.

	Input: nums = [7,5,6,8,3]
	Output: 1
	Explanation:
		The smallest number in nums is 3.
		The largest number in nums is 8.
		The greatest common divisor of 3 and 8 is 1.

	Input: nums = [3,3]
	Output: 3
	Explanation:
		The smallest number in nums is 3.
		The largest number in nums is 3.
		The greatest common divisor of 3 and 3 is 3.

	Constraints:

	    2 <= nums.length <= 1000
	    1 <= nums[i] <= 1000
*/

class Sol{

	static int findGCD(int arr[]){
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] > max){

				max = arr[i];
			}

			if(arr[i] < min){
				
				min = arr[i];
			}
		}

		int Gcd = 1;

		for(int i = 1; i <= min; i++){
			
			if(min % i == 0 && max % i == 0){
				
				Gcd = i;
			}
		}

		return Gcd;
	}
	
	public static void main(String[] s){
		
		int arr[] = {2,5,6,9,10};

		int ret = findGCD(arr);

		System.out.println(ret);
	}
}
