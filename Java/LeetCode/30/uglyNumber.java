
/*	Prog : Ugly Number
 *
 *	An "Ugly Number" is a positive intger whose prime factors are limited to 2,3 and 5.
 *	Given an integer n, return true if n is an ugly number
 *
 *	Input : n = 6 		Input : n = 1
 *	Ouput : true		Ouptu : true
 *	Expla : 6 = 2 * 3	Expla : 1 has no prime factors, therefore all og its prime factors are limited to 4
 *
 *	Input : n = 14
 *	Ouput : false
 *	Expla : 14 is not ugly since it includes the prime factor 7
*/

class sol{

	static boolean isUgly(int n){
		
		if(n == 1 || n == 2 || n == 3 || n == 5){
			
			return true;
		}

		if(n <= 0){
			
			return false;
		}

		if(n % 2 == 0){
			
			return isUgly(n / 2);

		}else if(n % 3 == 0){
			
			return isUgly(n / 3);

		}else if(n % 5 == 0){
			
			return isUgly(n / 5);
		}else{
			
			return false;
		}
	}
	
	public static void main(String[] args){
		
		int n = 6;

		boolean ret = isUgly(n);

		if(ret == true){
			
			System.out.println("Ugly Number");
		}else{
			
			System.out.println("Not an Ugly Number");
		}
	}
}
