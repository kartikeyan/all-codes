
/*	Prog : Single Element in a Sorted Array
 *
 *	You are given a sorted array consisting of only integers where every element exactly appears exactly twice, except for one element which appears exactly once.
 *
 *	Return the single element that appears only once.
 *
 *	Your solution must run in 0(logn) time and 0(1) space.
 *
 *	Input : nums = [1,1,2,3,3,4,4,8,8]
 *	Ouput : 2
 *
 *	Input : nums = [3,3,7,7,10,11,11]
 *	Ouput : 10
 *
 *	Constraints :
 *
 *		1 <= nums.length <= 10^5
 *
 *		0 <= nums[i] <= 10^5
*/

#include<stdio.h>

int singleNonDuplicate(int *arr, int size){
	
	int ans = arr[0];

	for(int i = 1; i < size; i++){
		
		ans = ans ^ arr[i];
	}

	return ans;
}

void main(){
	
	int arr[] = {1,1,2,3,3,4,4,8,8};

	int size = sizeof(arr) / sizeof(arr[0]);

	int ret = singleNonDuplicate(arr, size);

	printf("%d\n", ret);
}

