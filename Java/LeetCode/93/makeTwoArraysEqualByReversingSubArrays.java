
/*
 	1460. Make Two Arrays Equal by Reversing Subarrays

	You are given two integer arrays of equal length target and arr. In one step, you can select any non-empty subarray of arr and reverse it. You are allowed to make any number of steps.

	Return true if you can make arr equal to target or false otherwise.

	Input: target = [1,2,3,4], arr = [2,4,1,3]
	Output: true
	Explanation: You can follow the next steps to convert arr to target:
		1- Reverse subarray [2,4,1], arr becomes [1,4,2,3]
		2- Reverse subarray [4,2], arr becomes [1,2,4,3]
		3- Reverse subarray [4,3], arr becomes [1,2,3,4]
		There are multiple ways to convert arr to target, this is not the only way to do so.

	
	Input: target = [7], arr = [7]
	Output: true
	Explanation: arr is equal to target without any reverses.
	Input: target = [3,7,9], arr = [3,7,11]
	Output: false
	Explanation: arr does not have value 9 and it can never be converted to target.

	Constraints:

	    target.length == arr.length
	    1 <= target.length <= 1000
	    1 <= target[i] <= 1000
	    1 <= arr[i] <= 1000
*/

class Sol{

	static void merge(int arr[], int start, int mid, int end){
		
		int ele1 = mid - start + 1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i = 0; i < ele1; i++){
			
			arr1[i] = arr[start + i];
		}

		for(int i = 0; i < ele2; i++){
			
			arr2[i] = arr[mid + 1 + i];
		}

		int itr1 = 0, itr2 = 0, itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){
			
			if(arr1[itr1] < arr2[itr2]){
				
				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
				arr[itr3] = arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1 < ele1){
			
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){
			
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	static void mergeSort(int arr[], int start, int end){
		
		if(start < end){
			
			int mid = (start + end) / 2;

			mergeSort(arr, start, mid);
			mergeSort(arr, mid+1, end);

			merge(arr, start, mid, end);
		}
	}

	static boolean canBeEqual(int arr[], int target[]){
		
		int size1 = arr.length;

                int size2 = target.length;

                mergeSort(arr, 0, size1 - 1);

                mergeSort(target, 0, size2 - 1);

                for(int i = 0; i < size1; i++){

                        if(arr[i] != target[i]){

                                return false;
                        }
                }

		return true;

	}

	public static void main(String[] s){
		
		int arr[] = {2,4,3,1};

		int target[] = {3,4,1,2};

		int size1 = arr.length;

		int size2 = target.length;

		boolean ret = canBeEqual(arr, target);

		if(ret == true){
			
			System.out.println("Two Array are Equal");
		}else{
			System.out.println("Two Array not Equal");
		}
	}
}
