
/*
 	450. Delete Node in a BST

	Given a root node reference of a BST and a key, delete the node with the given key in the BST. Return the root node reference (possibly updated) of the BST.

	Basically, the deletion can be divided into two stages:

	    Search for a node to remove.
	    If the node is found, delete the node.

	Input: root = [5,3,6,2,4,null,7], key = 3
	Output: [5,4,6,2,null,null,7]
	Explanation: Given key to delete is 3. So we find the node with value 3 and delete it.
		One valid answer is [5,4,6,2,null,null,7], shown in the above BST.
		Please notice that another valid answer is [5,2,6,null,4,null,7] and it's also accepted.

	Example 2:

	Input: root = [5,3,6,2,4,null,7], key = 0
	Output: [5,3,6,2,4,null,7]
	Explanation: The tree does not contain a node with value = 0.

	Example 3:

	Input: root = [], key = 0
	Output: []

	Constraints:

	    The number of nodes in the tree is in the range [0, 104].
	    -105 <= Node.val <= 105
	    Each node has a unique value.
	    root is a valid binary search tree.
	    -105 <= key <= 105
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

typedef struct BSTNode{

	int data;

	struct BSTNode *left;
	struct BSTNode *right;
}node;

struct BSTNode* constructBST(struct BSTNode *root, int ele){

	if(root == NULL){

		struct BSTNode *newNode = (node*)malloc(sizeof(node));

		newNode->data = ele;

		newNode->left = NULL;

		newNode->right = NULL;

		root = newNode;

		return root;
	}

	if(root->data > ele){

		root->left = constructBST(root->left, ele);
	}else{

		root->right = constructBST(root->right, ele);
	}

	return root;	// add compulsory
}

void printBST(node *root){	// Inorder traversal in BST is always sorted

	if(root == NULL){

		return;
	}

	printBST(root->left);

	printf("%d ", root->data);

	printBST(root->right);
}

struct BSTNode* findLastRight(node *root){

        if(root->right == NULL){

                return NULL;
        }

        return findLastRight(root->right);
}


struct BSTNode* helper(node *root){

	if(root->left == NULL){

		return root->right;
	}else if(root->right == NULL){

		return root->left;
	}

	node *rightChild = root->right;

	node *lastRight = findLastRight(root->left);

	lastRight->right = rightChild;

	return root->left;
}
/*
int inOrderSucc(node *root){

	node *temp = root;

	while(temp->left != NULL){

		root = root->left;
	}

	return root->data;
}


struct BSTNode* deleteInBST(node *root, int key){

	if(root == NULL){

		return NULL;
	}

	else if(root->data > key){

		root->left = deleteInBST(root->left, key);
	}

	else if(root->data < key){

		root->right = deleteInBST(root->right, key);
	}

	else{

		if(root->right == NULL){

			node *temp = root->left;

			free(root);

			return temp;
		}

		else if(root->left == NULL){

			node *temp = root->right;

			free(root);

			return temp;
		}

		else{
			int temp = inOrderSucc(root->right);

			root->data = temp;

			root->right = deleteInBST(root->right, temp);
		}
	}

	return root;
}
*/

int inOrderSucc(node *root){

        node *temp = root;

        while(temp->left != NULL){

                temp = temp->left;
        }

        return temp->data;
}

struct BSTNode* deleteInBST(node *root, int key){

/*	
        if(root == NULL){

                return NULL;
        }

       	if(root->data == key){

		if(root->left == NULL && root->right == NULL){

			free(root);

			return NULL;
		}

		if(root->left != NULL && root->right == NULL){

			node *temp = root->left;

			free(root);

			return temp;
		}

		if(root->left == NULL && root->right != NULL){

			node *temp = root->right;

			free(root);

			return temp;
		}

		if(root->left != NULL && root->right != NULL){

			int min = inOrderSucc(root->right);

			root->data = min;

			root->right = deleteInBST(root->right, min);	// replace kelela value delete karayla pahije mahnun parat call kelay

			return root;
		}
	}

	else if(root->data > key){

		root->left = deleteInBST(root->left, key);

		return root;
	}

	else if(root->data < key){

		root->right = deleteInBST(root->right, key);

		return root;
	}

	return root;
*/

	if(root == NULL){
		
		return NULL;
	}

	if(root->data > key){
		
		root->left = deleteInBST(root->left, key);
	}

	if(root->data < key){
		
		root->right = deleteInBST(root->right, key);

	}else{
		
		if(root->left == NULL && root->right == NULL){
			
			free(root);

			return NULL;

		}else if(root->left != NULL && root->right == NULL){
		
			node *temp = root->right;

			free(root);

			return temp;

		}else if(root->left == NULL && root->right != NULL){
			
			node *temp = root->left;

			free(root);

			return temp;

		}else if(root->left != NULL && root->right != NULL){
			
			int min = inOrderSucc(root->right);

			root->data = min;

			root->right = deleteInBST(root->right, min);

			return root;
		}
	}
	return root;
}

void main(){

	struct BSTNode *root = NULL;

	int nodeCount, ele;

	printf("Enter Node Count\n");
	scanf("%d", &nodeCount);

	for(int i = 1; i <= nodeCount; i++){

		scanf("%d", &ele);

		root = constructBST(root, ele);
	}

	printf("BST Traversal is\n");
	printBST(root);
	printf("\n");

	int nodeData;
	printf("Enter node data to be deleted\n");
	scanf("%d", &nodeData);

	root = deleteInBST(root, nodeData);

	printf("After Deleting Node\n");
	printBST(root);
	printf("\n");
}
