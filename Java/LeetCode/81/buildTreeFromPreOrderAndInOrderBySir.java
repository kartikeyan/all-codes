
/*
	105. Construct Binary Tree from Preorder and Inorder Traversal

	Given two integer arrays preorder and inorder where preorder is the preorder traversal of a binary tree and inorder is the inorder traversal of the same tree, construct and return the binary tree.

	Input: preorder = [3,9,20,15,7], inorder = [9,3,15,20,7]
	Output: [3,9,20,null,null,15,7]
	
	Input: preorder = [-1], inorder = [-1]
	Output: [-1]

	Constraints:

    	1 <= preorder.length <= 3000
    	inorder.length == preorder.length
    	-3000 <= preorder[i], inorder[i] <= 3000
    	preorder and inorder consist of unique values.
    	Each value of inorder also appears in preorder.
    	preorder is guaranteed to be the preorder traversal of the tree.
    	inorder is guaranteed to be the inorder traversal of the tree.
*/


class Node {
	int data;
	Node left, right;

	Node(int item)
	{
		data = item;
		left = right = null;
	}
}

class BinaryTree {
	Node root;
	static int preIndex = 0;

	Node buildTree(int in[], int pre[], int inStart, int inEnd, int preStart, int preEnd)
	{
		if (inStart > inEnd)
			return null;

			
//		Node tNode = new Node(pre[preIndex++]);
		int rootData = pre[preStart];

		Node temp = new Node(rootData);

		
//		int inIndex = search(in, inStrt, inEnd, tNode.data);
		int idx;
		for(idx = inStart; idx <= inEnd; idx++){
			
			if(rootData == in[idx]){
				
				break;
			}
		}

		int lLength = idx - inStart;
	
		temp.left = buildTree(in, pre, inStart, idx - 1, preStart+1 ,preStart+lLength);

		temp.right = buildTree(in, pre, idx + 1, inEnd, preStart + lLength + 1, preEnd);

		return temp;
	}

	
	void printInorder(Node node)
	{
		if (node == null)
			return;

		
		printInorder(node.left);

		System.out.print(node.data + " ");

		printInorder(node.right);
	}

	
	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();
		int inOrder[] = {3,9,20,15,7};

		int preOrder[] = {9,3,15,20,7};

		int len = inOrder.length;

		Node root = tree.buildTree(inOrder, preOrder, 0, len-1, 0, len - 1);
		
		System.out.println("Inorder traversal of constructed tree is : ");
		tree.printInorder(root);

		System.out.print("\n");
	}
}



