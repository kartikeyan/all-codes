
/*
 *	Program 1 : Given a "non-empty" array of integers nums.every element appears twice except for one.
 *		    Find That single one.
 *
 *		    You must implement a sloution with a linear runtime complexity and use only constant extra space
 *
 *		    Input : nums = [2,2,1]
 *		    Ouput : 1
 *
 *		    Input : nums = [4,1,2,1,2]
 *		    Output : 4
*/

class sol{

	static int single(int arr[], int size){
		
		int ans = 0;

		for(int i = 0; i < size; i++){
			
			ans = ans ^ arr[i];
		}

		return ans;
	}
	
	public static void main(String[] s){
		
		int arr[] = {2,2,1};

		int size = arr.length;

		int ret = single(arr, size);

		System.out.println(ret);
	}
}
