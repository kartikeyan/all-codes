/*	
 *	Prog 2 : Given an integer n, return true if it is a power of four.Otherwise return false.
 *		 An integer n is a power of foue, if there exists an integer x such that n == 4^x
 *
 *		 Input : n = 16,	Input : 1
 *		 Ouput : true		Output : true
 *
 *		 Input : n = 5
 *		 Ouput : false
*/

class sol{
	
	static boolean isPower(int n){
		
		if(n == 1){
			return true;
		}
		if(n <= 0){
			return false;
		}
		if(n % 4 != 0){
			return false;
		}
		return isPower(n / 4);
	}

	public static void main(String[] s){
		
		int n = 16;

		boolean ret = isPower(n);

		System.out.println(ret);
	}
}
