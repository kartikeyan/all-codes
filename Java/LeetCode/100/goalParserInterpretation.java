
/*
 	1678. Goal Parser Interpretation

	You own a Goal Parser that can interpret a string command. The command consists of an alphabet of "G", "()" and/or "(al)" in some order. The Goal Parser will interpret "G" as the string "G", "()" as the string "o", and "(al)" as the string "al". The interpreted strings are then concatenated in the original order.

	Given the string command, return the Goal Parser's interpretation of command.

	Input: command = "G()(al)"
	Output: "Goal"
	Explanation: The Goal Parser interprets the command as follows:
	G -> G
	() -> o
	(al) -> al
	The final concatenated result is "Goal".

	Input: command = "G()()()()(al)"
	Output: "Gooooal"

	Input: command = "(al)G(al)()()G"
	Output: "alGalooG"

	Constraints:

    		1 <= command.length <= 100
    		command consists of "G", "()", and/or "(al)" in some order.

*/

class Sol{

	static String interpret(String command){
		
		StringBuilder str = new StringBuilder();

		for(int i = 0; i < command.length(); i++){
			
			if(command.charAt(i) == 'G'){
				
				str.append("G");
			}
			if(command.charAt(i) == '(' && command.charAt(i+1) == ')'){
				str.append("o");
			}
			if(command.charAt(i) == '(' && command.charAt(i+1) == 'a'){
				str.append("al");
			}
		}

		return str.toString();
	}
	
	public static void main(String[] s){
		
		String str = "G()()()()(al)";

		String ret = interpret(str);

		System.out.println("returned is " + ret);
	}
}
