
/*	Prog : The kth Factor of N
 *
 *	You are given two positive integers n and k. A factor of an integer n is defined as an integer i where n % i == 0.
 *
 *	Consider a list of all factors of n sorted in ascending order, return the kTh factor in this list or return -1 if n has less than k factors.
 *
 *	Input : n = 12, k = 3
 *	Ouput : 3
 *	Expla : Factors list is [1,2,3,4,6,12], the 3rd factor is 3.
 *
 *	Input : n = 7, k = 2
 *	Ouput : 7
 *	Expla : Factors list is [1,7], the 2nd factor is 7.
*/

class codeDemo{
	
	static int kThFactor(int n, int k){
		
		int count = 0;

		for(int i = 1; i <= n; i++){
			
			if(n % i == 0){
				
				count++;
			}

			if(count == k){
				
				return i;
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		int n = 12, k = 3;

		int ret = kThFactor(n, k);
		
		System.out.println(ret);	
	}
}
