
/*	Program 1 : Diiference between Element Sum and Digit Sum of an array
 *
 *	You are given a positive integer array "nums".
 *
 *	The "element sum" is the sum of all the elements in "nums"
 *
 *	The "digit sum" is the sum of all the digits (not necessarily distinct) that appear in "nums".
 *
 *	Return the "absolute" differece between the "element sum" and "digit sum" of nums
 *
 *	Input : nums[1,15,6,3]
 *	Ouput : 9
 *	Explanation :
 *	The element sum of nums is 1 + 15 + 6 + 3 = 25
 *	The digit sum of nums is 1 + 1 + 5 + 6 + 3 = 16
 *	Absolute difference between the element sum and digit sum if [25-16] = 9
*/

class sol{

	static int diffSum(int arr[], int size){
		
		int eleSum = 0, digitSum = 0;

		for(int i = 0; i < size; i++){

			eleSum = eleSum + arr[i];
			
			while(arr[i] > 0){
				
				int rem = arr[i] % 10;
				digitSum = digitSum + rem;
				arr[i] = arr[i] / 10;
			}
		}

		return eleSum - digitSum;
	}
	
	public static void main(String[] s){
		
		int arr[] = {1,15,6,3};

		int size = arr.length;

		int ret = diffSum(arr, size);

		System.out.println(ret);
	}
}
