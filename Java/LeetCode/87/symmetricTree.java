
/*
 	101. Symmetric Tree

	Given the root of a binary tree, check whether it is a mirror of itself (i.e., symmetric around its center).

	Input: root = [1,2,2,3,4,4,3]
	Output: true

	Input: root = [1,2,2,null,3,null,3]
	Output: false

	Constraints:

	    The number of nodes in the tree is in the range [1, 1000].
	    -100 <= Node.val <= 100
*/

class Node {
	int key;
	Node left, right;
	Node(int item)
	{
		key = item;
		left = right = null;
	}
}

class BinaryTree {

	Node root;

	boolean isMirror(Node left, Node right)
	{
		if(left == null && right == null){
			
			return true;
		}

		if(left == null && right != null){
			
			return false;
		}

		if(left != null && right == null){
			
			return false;
		} 

		if(left.key != right.key){
			
			return false;
		}

		return isMirror(left.left, right.right) && isMirror(left.right, right.left);

	}
	
	boolean isSymmetric()
	{
		// check if tree is mirror of itself
		return isMirror(root, root);
	}

	
	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);

		tree.root.left = new Node(2);
		tree.root.right = new Node(2);

		tree.root.left.left = new Node(3);
		tree.root.left.right = new Node(4);

		tree.root.right.left = new Node(4);
		tree.root.right.right = new Node(3);

		boolean output = tree.isSymmetric();

		if (output == true)
			System.out.println("Symmetric");
		else
			System.out.println("Not symmetric");
	}
}


