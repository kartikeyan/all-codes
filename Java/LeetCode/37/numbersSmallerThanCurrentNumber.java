
/*	Prog : Given an array nums, for each nums[i] find out how many numbers in the array are smaller than it.That is, for each nums[i] you have to count the number of valid's such that j != i and nums[j] < nums[i].
 *
 *	Input : nums = [8,1,2,2,3]
 *	Ouput : [4,0,1,1,3]
 *	Expla :
 *		for nums[0] = 8 there exist four smaller numbers than it (1,2,2 and 3)
 *		for nums[1] = 1 does not exist any smaller number than it.
 *
 *		for nums[2] = 2 there exist one smaller number than it(1).
 *
 *		for nums[3] = 2 there exist one smaller numbert than it(1).
 *
 *		for nums[4] = 3 there exist three smaller numbers than it(1, 2 and 3)
 *
 *
 *	Input : nums = [6,5,4,8]
 *	Ouput : [2,1,0,3]
 *
 *	Input : nums = [7,7,7,7]
 *	Ouput : [0,0,0,0]
 *
 *	Constraints :
 *
 *		2 <= nums.lenght <= 500
 *
 *		0 <= nums[i] <= 100
*/

class recDemo{

	static void smallerNumber(int arr[], int size, int newArr[]){
		
		int i, j, k = 0;

		for(i = 0; i < size; i++){
			
			int count = 0;

			for(j = 0; j < size; j++){
				
				if(j != i && arr[j] < arr[i]){
					
					count++;
				}
			}

			newArr[k++] = count;
		}
	}

	public static void main(String[] args){
	
		int arr[] = {8,1,2,2,3};

		int size = arr.length;

		int newArr[] = new int[size];

		smallerNumber(arr, size, newArr);

		for(int i = 0; i < size; i++){
		
			System.out.println(newArr[i]);
		}
	}
}
