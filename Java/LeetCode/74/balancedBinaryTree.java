
/*
 	110. Balanced Binary Tree

	Given a binary tree, determine if it is
height-balanced

	Input: root = [3,9,20,null,null,15,7]
	Output: true
	
	Input: root = [1,2,2,3,3,null,null,4,4]
	Output: false

	Input: root = []
	Output: true

	Constraints:

	    The number of nodes in the tree is in the range [0, 5000].
	    -104 <= Node.val <= 104
*/

class Node{
	
	int data;
	Node left, right;

	Node(int val){

		data = val;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	static int height(Node root){
		
		if(root == null){
			
			return 0;
		}

		int lh = height(root.left);
		int rh = height(root.right);

		if(lh == -1){
			
			return -1;
		}

		if(rh == -1){
			
			return -1;
		}

		if(Math.abs(lh - rh) > 1){
			
			return -1;
		}

		return (Math.max(lh, rh) + 1);
	}

	boolean isBalanced(Node root){
		
		if(root == null){
			
			return true;
		}

		if(height(root) == -1){
			
			return false;
		}

		return true;
	}

	public static void main(String[] s){
	
		BinaryTree tree = new BinaryTree();
	        tree.root = new Node(1);
	        tree.root.left = new Node(2);
        	tree.root.right = new Node(3);
	        tree.root.left.left = new Node(4);
	        tree.root.left.right = new Node(5);
        	tree.root.left.left.left = new Node(8);

	        if (tree.isBalanced(tree.root))
	            System.out.println("Tree is balanced");
	        else
        	    System.out.println("Tree is not balanced");
	}
}
