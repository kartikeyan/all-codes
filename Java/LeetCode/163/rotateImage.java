
/*
 	48. Rotate Image

	You are given an n x n 2D matrix representing an image, rotate the image by 90 degrees (clockwise).

	You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.	

 	Input: matrix = [[5,1,9,11],[2,4,8,10],[13,3,6,7],[15,14,12,16]]
	Output: [[15,13,2,5],[14,3,4,1],[12,6,8,9],[16,7,10,11]]

	Constraints:

	    n == matrix.length == matrix[i].length
	    1 <= n <= 20
	    -1000 <= matrix[i][j] <= 1000
*/

class Sol{
	
	public static void main(String[] args){
		
		int arr[][] = {{1,2,3},{4,5,6},{7,8,9}};
	
		// 1 - Transpose Martix
		for(int i = 0; i < arr.length; i++){
			
			for(int j = i + 1; j < arr.length; j++){
				
				int temp = arr[i][j];
				arr[i][j] = arr[j][i];
				arr[j][i] = temp;
			}
		}
		
		// 2 - Reverse every row
		for(int i = 0; i < arr.length; i++){
			
			int start = 0, end = arr.length - 1;

			while(start < end){
				
				int temp = arr[i][start];
				arr[i][start] = arr[i][end];
				arr[i][end] = temp;

				start++;
				end--;
			}
		}

		System.out.println("After Rotating");
		for(int i = 0; i < arr.length; i++){
			
			for(int j = 0; j < arr[0].length; j++){
				
				System.out.print(arr[i][j] + " ");
			}

			System.out.println();
		}
	}
}

/*
 	After Rotating
	7 4 1 
	8 5 2 
	9 6 3 
*/
