
/*
 	199. Binary Tree Right Side View

	Given the root of a binary tree, imagine yourself standing on the right side of it, return the values of the nodes you can see ordered from top to bottom.

	Input: root = [1,2,3,null,5,null,4]
	Output: [1,3,4]

	Input: root = [1,null,3]
	Output: [1,3]

	Input: root = []
	Output: []

	Constraints:

	    The number of nodes in the tree is in the range [0, 100].
	    -100 <= Node.val <= 100
*/

import java.io.*;
import java.util.LinkedList;
import java.util.Queue;


class Node {
	int data;
	Node left, right;
	public Node(int d)
	{
		data = d;
		left = right = null;
	}
}

class BinaryTree {
	Node root;

	void rightView(Node root)
	{
		if (root == null) {
			return;
		}

		Queue<Node> q = new LinkedList<>();
		q.add(root);

		while (!q.isEmpty()) {

			
			int n = q.size();

			
			for (int i = 0; i < n; i++) {
				Node curr = q.peek();
				q.remove();

				
				if (i == n - 1) {
					System.out.print(curr.data);
					System.out.print(" ");
				}

				
				if (curr.left != null) {
					q.add(curr.left);
				}

				
				if (curr.right != null) {
					q.add(curr.right);
				}
			}
		}
	}

	public static void main(String[] args)
	{

		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);
		tree.root.right.left = new Node(6);
		tree.root.right.right = new Node(7);
		tree.root.right.left.right = new Node(8);

		tree.rightView(tree.root);

		System.out.println();
	}
}


