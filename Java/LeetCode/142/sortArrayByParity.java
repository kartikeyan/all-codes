
/*
 	905. Sort Array By Parity

	Given an integer array nums, move all the even integers at the beginning of the array followed by all the odd integers.

	Return any array that satisfies this condition.

	Example 1:

	Input: nums = [3,1,2,4]
	Output: [2,4,3,1]
	Explanation: The outputs [4,2,3,1], [2,4,1,3], and [4,2,1,3] would also be accepted.

	Example 2:

	Input: nums = [0]
	Output: [0]

	Constraints:

	    1 <= nums.length <= 5000
	    0 <= nums[i] <= 5000
*/

class Sol{

	static int[] sortArrayByParity(int arr[]){
		
		int[] n = new int[arr.length];

		int l = 0;
		int r = arr.length - 1;

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] % 2 == 0){
				
				n[l++] = arr[i];
			}else{
				n[r--] = arr[i];
			}
		}

		return n;
	}
	
	public static void main(String[] args){
		
		int nums[] = {3,1,2,4};

		int ret[] = sortArrayByParity(nums);

		for(int i = 0; i < nums.length; i++){
			
			System.out.print(ret[i]);
		}

		System.out.println();
	}
}
