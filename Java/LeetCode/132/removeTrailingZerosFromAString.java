
/*
 	2710. Remove Trailing Zeros From a String

	Given a positive integer num represented as a string, return the integer num without trailing zeros as a string.


	Example 1:

	Input: num = "51230100"
	Output: "512301"
	Explanation: Integer "51230100" has 2 trailing zeros, we remove them and return integer "512301".

	Example 2:

	Input: num = "123"
	Output: "123"
	Explanation: Integer "123" has no trailing zeros, we return integer "123".

	Constraints:

	    1 <= num.length <= 1000
	    num consists of only digits.
	    num doesn't have any leading zeros.
*/

class Sol{

	/*
	static String removeTrailingZeros(String num){
		
		int n = num.length() - 1;

		for(int i = n; i > 0; i--){
			
			if(num.charAt(i) != '0'){
				
				break;
			}

			n--;
		}

		return num.substring(0, n+1);
	}
	*/
	
	/*
	static String removeTrailingZeros(String num){
		
		int n = num.length() - 1;

		StringBuilder str = new StringBuilder();

		int flag = 0;

		for(int i = n; i >= 0; i--){
			
			if(num.charAt(i) != '0'){
				
				flag = 1;
			}

			if(flag == 1)
				str.append(num.charAt(i));
		}

		str.reverse();

		return str.toString();
	}
	*/

	static String removeTrailingZeros(String num){
		
		int n = num.length() - 1;

		StringBuilder str = new StringBuilder(num);

		for(int i = n; i > 0; i--){
			
			if(num.charAt(i) == '0'){
				
				str.deleteCharAt(i);
			}else
				break;
		}

		return str.toString();
	}
	
	public static void main(String[] args){
		
		String num = "51230100";

		String ret = removeTrailingZeros(num);

		System.out.println("After Removing Trailing Zeros Are " + ret);
	}
}
