
/*
 	107. Binary Tree Level Order Traversal II

	Given the root of a binary tree, return the bottom-up level order traversal of its nodes' values. (i.e., from left to right, level by level from leaf to root).

	Input: root = [3,9,20,null,null,15,7]
	Output: [[15,7],[9,20],[3]]

	Input: root = [1]
	Output: [[1]]

	Input: root = []
	Output: []

	Constraints:

	    The number of nodes in the tree is in the range [0, 2000].
	    -1000 <= Node.val <= 1000
*/

//	Using Recursion

/*

class Node
{
	int data;
	Node left, right;

	Node(int item)
	{
		data = item;
		left = right;
	}
}

class BinaryTree
{
	Node root;

	void reverseLevelOrder(Node node)
	{
		int h = height(node);
		int i;
		for (i = h; i >= 1; i--)
		//THE ONLY LINE DIFFERENT FROM NORMAL LEVEL ORDER
		{
			printGivenLevel(node, i);
		}
	}

	void printGivenLevel(Node node, int level)
	{
		if (node == null)
			return;
		if (level == 1)
			System.out.print(node.data + " ");
		else if (level > 1)
		{
			printGivenLevel(node.left, level - 1);
			printGivenLevel(node.right, level - 1);
		}
	}

	int height(Node node)
	{
		if (node == null)
			return 0;
		else
		{

			int lheight = height(node.left);
			int rheight = height(node.right);

			// use the larger one 
			if (lheight > rheight)
				return (lheight + 1);
			else
				return (rheight + 1);
		}
	}

	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();

		
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		System.out.println("Level Order traversal of binary tree is : ");
		tree.reverseLevelOrder(tree.root);

		System.out.println();
	}
}
*/

/*
	Time Complexity: O(N2), where N is the number of nodes in the skewed tree. So time complexity of printLevelOrder() is O(n) + O(n-1) + O(n-2) + .. + O(1) which is O(N2). 
	Auxiliary Space:  O(N) in the worst case. For a skewed tree, printGivenLevel() uses O(n) space for the call stack. For a Balanced tree, the call stack uses O(log n) space, (i.e., the height of the balanced tree). 

*/

//	Using Queue and Stack

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class Node
{
	int data;
	Node left, right;

	Node(int item)
	{
		data = item;
		left = right;
	}
}

class BinaryTree
{
	Node root;

	/* Given a binary tree, print its nodes in reverse level order */
	void reverseLevelOrder(Node node)
	{
		Stack<Node> S = new Stack();
		Queue<Node> Q = new LinkedList();
		Q.add(node);

		// Do something like normal level order traversal order.Following
		// are the differences with normal level order traversal
		// 1) Instead of printing a node, we push the node to stack
		// 2) Right subtree is visited before left subtree
		while (Q.isEmpty() == false)
		{
			/* Dequeue node and make it root */
			node = Q.peek();
			Q.remove();
			S.push(node);

			/* Enqueue right child */
			if (node.right != null)
				// NOTE: RIGHT CHILD IS ENQUEUED BEFORE LEFT
				Q.add(node.right);

			/* Enqueue left child */
			if (node.left != null)
				Q.add(node.left);
		}

		// Now pop all items from stack one by one and print them
		while (S.empty() == false)
		{
			node = S.peek();
			System.out.print(node.data + " ");
			S.pop();
		}
	}

	// Driver program to test above functions
	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);
		tree.root.right.left = new Node(6);
		tree.root.right.right = new Node(7);

		System.out.println("Level Order traversal of binary tree is :");
		tree.reverseLevelOrder(tree.root);
	}
}
