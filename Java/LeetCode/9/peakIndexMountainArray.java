
/*	Prog 1 : Peak Index in Mountain Array
 *
 *	Input : [0,1,0]  ,  Input : [0,2,1,0]
 *	Ouput : 1	 ,  Ouput : 1
*/

class sol{

	static int peakIndex(int arr[], int size){
	
		int start = 0, end = size - 1, mid;

		while(start < end){
		
			mid = start + (end - start) / 2;

			if(arr[mid] < arr[mid+1]){
			
				start = mid + 1;
			}else{
				end = mid;		// if end = mid - 1 then, it will check in another side but we want to check in mid
			}
		}

		return start;	//end or mid
	}

	public static void main(String[] s){
	
		int arr[] = {0,1,0};

		int size = arr.length;

		int ret = peakIndex(arr, size);

		System.out.println( ret);
	}
}
