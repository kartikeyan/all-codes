
/*	Prog 2 : Find Peak Element
 *	
 *	A peak Element is an element that is strictly greater than it's neighbors
 *
 *	Input : [1,2,3,1]  ,  Input : [1,2,1,3,5,6,4]
 *	Ouput : 2	   ,  Ouput : 5
 *	Expla :		   ,  Expla : ur function can return either index no. 1 where peak element is 2,
 *	3 is a peal element and	      or index no. 5 where peak element is 6
 *	ur fuction should 
 *	return index no. 2
*/

class sol{

	static int peakIndex(int arr[], int size){
	
		int start = 0, end = size - 1, mid;

		while(start < end){
		
			mid = start + (end - start) / 2;

			if(arr[mid] < arr[mid+1]){
			
				start = mid + 1;
			}else{
				end = mid;		// if end = mid - 1 then, it will check in another side but we want to check in mid
			}
		}

		return start;	//end or mid
	}

	public static void main(String[] s){
	
		int arr[] = {1,2,1,3,5,6,4};

		int size = arr.length;

		int ret = peakIndex(arr, size);

		System.out.println( ret);
	}
}
