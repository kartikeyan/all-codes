
/*	PRogram 1 : Kth Largest Element in an Array
 *
 *	Given an integer array "nums" and an integer "k", return the "Kth" largest element in the array
 *	Not that it is the "Kth" largest element in the sorted order, not the "Kth" distinct element.
 *	You must solve it in "0(n)" time complexity
 *
 *	Input : nums = [3,2,1,5,6,4],  k = 2
 *	Output : 5
 *
 *	Input : nums = [3,2,3,1,2,4,5,5,6], k = 4
 *	Ouput :  4
*/

class sol{
	
	static void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

//	int arr1[ele1], arr2[ele2];
	int arr1[] = new int[ele1];
	int arr2[] = new int[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr[itr1] < arr[ele1]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

	static void mergeSort(int arr[], int start, int end){
	
		if(start < end){
		
			int mid = (start + end) / 2;

			mergeSort(arr, start, mid);
			mergeSort(arr, mid + 1, end);
	
			merge(arr, start, mid, end);
		}
	}

	static int kSmallest(int arr[], int size, int k){
		
		return arr[k-1];
	}

	static int kLargest(int arr[], int size, int k){
		
		return arr[size-k-1];
	}

	public static void main(String[] s){
		
		int arr[] = {3,2,1,5,6,4};

		int size = arr.length;

		mergeSort(arr, 0, size-1);

		int k = 2;

		int ret1 = kSmallest(arr, size, k);
		System.out.println("Kth smallest is " + ret1);

		int ret2 = kLargest(arr, size, k);
		System.out.println(ret2);
	}
}
