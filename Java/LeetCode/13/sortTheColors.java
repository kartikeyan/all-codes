
/*	PRogram 2 : Sort Colors
 *
 *	Given an array "nums" with "n" objects colored red, white, or blue, sort them "in-place" so that
 *	objects of the same color are adjacent, with the colors in he order red, white, and blue
 *
 *	We will use the intgers 0, 1, and 2 to represent the color red, white, and blue, respectively
 *
 *	You must solve this problem without using the library's sort function
 *
 *	Input : nums = [2,0,2,1,1,0]
 *	Output : [0,0,1,1,2,2]
 *
 *	Input : nums = [2,0,1]
 *	Ouput :  [0,1,2]
*/

class sol{
	
	static void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

//	int arr1[ele1], arr2[ele2];
	int arr1[] = new int[ele1];
	int arr2[] = new int[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr[itr1] < arr[ele1]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

static void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

	static void sortTheColors(int arr[], int size){
		
		mergeSort(arr, 0, size-1);
	}

	public static void main(String[] s){
		
		int arr[] = {0,1,0,2,1,0};

		int size = arr.length;

		sortTheColors(arr, size);

		for(int i = 0; i < size; i++){
			
			System.out.println(arr[i]);
		}
	}
}
