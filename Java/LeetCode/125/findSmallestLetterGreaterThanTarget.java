
/*
 	744. Find Smallest Letter Greater Than Target

	You are given an array of characters letters that is sorted in non-decreasing order, and a character target. There are at least two different characters in letters.

	Return the smallest character in letters that is lexicographically greater than target. If such a character does not exist, return the first character in letters

	Input: letters = ["c","f","j"], target = "a"
	Output: "c"
	Explanation: The smallest character that is lexicographically greater than 'a' in letters is 'c'.

	Input: letters = ["c","f","j"], target = "c"
	Output: "f"
	Explanation: The smallest character that is lexicographically greater than 'c' in letters is 'f'.

	Input: letters = ["x","x","y","y"], target = "z"
	Output: "x"
	Explanation: There are no characters in letters that is lexicographically greater than 'z' so we return letters[0].

	Constraints:

	    2 <= letters.length <= 104
	    letters[i] is a lowercase English letter.
	    letters is sorted in non-decreasing order.
	    letters contains at least two different characters.
	    target is a lowercase English letter.
*/

class Sol{
	
	//	1 - Brute Force
	/*
	static char nextGreatestLetter(char letters[], char target){
		
		for(int i = 0; i < letters.length; i++){
			
			if(letters[i] != target && letters[i] > target){
				return letters[i];
			}
		}

		return letters[0];
	}
	*/

	//	Approach 2
	/*
	static char nextGreatestLetter(char letters[], char target){
		int start = 0, end = letters.length - 1;

		while(start <= end){
			
			int mid = start + (end - start) / 2;

			if(letters[mid] > target){
				
				end = mid - 1;
			}else
				start = mid + 1;
		}

		return start == letters.length ? letters[0] : letters[start];

	}
	*/

/*	
	static char nextGreatestLetter(char letters[], char target){
		
		int n = letters.length;

		if(letters[n - 1] <= target){
			
			return letters[0];
		}	

		int left = 0, right = letters.length - 1;
		
		while(left < right){
			
			int mid = left + (right - left) / 2;

			if(letters[mid] <= target){
				
				left = mid - 1;
			}else
				right = mid;
		}

		return letters[left];
	}

*/

	static char nextGreatestLetter(char letters[], char target){
		int start = 0, end = letters.length - 1;

		while(start <= end){
			
			int mid = start + (end - start) / 2;

			if(letters[mid] <= target){
				
				start = mid + 1;
			}else
				end = mid - 1;
		}

		return letters[start % letters.length];
	}

	public static void main(String[] args){
		
		char letters[] = {'c','f','j'};

		char target = 'a';

		char ret = nextGreatestLetter(letters, target);

		System.out.println("The smallest character in letters that is lexicographically greater than target is " + ret); 
	}
}
