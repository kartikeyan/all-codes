
class assign{
	
	static public int mySqrt(int num){

		int start = 0, end = num, mid;

		long int ans = -1;

		while(start <= end){
		
			mid = (start + end) / 2;

			long int square = mid * mid;

			if(square == num){
				
				return mid;

			}else if(square < num){
				
				ans = mid;
				start = mid + 1;
			}else{
				
				end = mid - 1;
			}
		}

		return ans;
	}

	public static void main(String[] args){
		
		int num = 16;

		int ret = mySqrt(num);

		System.out.println(ret);
	}
}
