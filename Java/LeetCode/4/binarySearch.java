
class assign{
	
	static public int binarySearch(int arr[], int search){

		int start = 0, end = arr.length - 1, mid;

		while(start <= end){
		
			mid = (start + end) / 2;

			if(arr[mid] == search){
				
				return mid;

			}else if(arr[mid] > search){
				
				end = mid - 1;
			}else{
				
				start = mid + 1;
			}
		}

		return -1;
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,5,6};

		int search = 1;

		int ret = binarySearch(arr, search);

		System.out.println(ret);
	}
}
