
class sol{

	static int countDigits(int num){
		
		int count = 0, temp = num;

		while(temp != 0){
			
			int rem = temp % 10;

			if(num % rem == 0){
				
				count++;
			}

			temp = temp / 10;
		}

		return count;
	}
	
	public static void main(String[] args){
		
		int num = 121;

		int ret = countDigits(num);

		System.out.println(ret);
	}
}
