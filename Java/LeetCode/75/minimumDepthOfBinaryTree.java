
/*
 	111. Minimum Depth of Binary Tree

	Given a binary tree, find its minimum depth.

	The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

	Note: A leaf is a node with no children.

	Input: root = [3,9,20,null,null,15,7]
	Output: 2

	Input: root = [2,null,3,null,4,null,5,null,6]
	Output: 5

	Constraints:

	    The number of nodes in the tree is in the range [0, 105].
	    -1000 <= Node.val <= 1000
*/

class Node{
	
	int data;
	Node left, right;

	Node(int val){

		data = val;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	static int min(int x, int y){
		
		if(x > y){
			
			return y;
		}

		return x;
	}

	static int minDepth (Node root){
		
		if(root == null){
			
			return 0;
		}

		int leftDepth = minDepth(root.left);

		int rightDepth = minDepth(root.right);

		if(root.left == null && root.right == null){
			
			return 1;
		}

		if(root.left == null){
			
			return rightDepth + 1;
		}

		if(root.right == null){
			
			return leftDepth + 1;
		}

		return min(leftDepth, rightDepth) + 1;
	}

	public static void main(String[] s){
	
		BinaryTree tree = new BinaryTree();

	        tree.root = new Node(3);
	
	        tree.root.left = new Node(9);
        	tree.root.right = new Node(20);

	        tree.root.right.left = new Node(15);
	        tree.root.right.right = new Node(7);

	        int ret = minDepth(tree.root);

		System.out.println(ret);
	}
}
