
/*
 	344. Reverse String

	Write a function that reverses a string. The input string is given as an array of characters s.

	You must do this by modifying the input array in-place with O(1) extra memory.

	Input: s = ["h","e","l","l","o"]
	Output: ["o","l","l","e","h"]

	Input: s = ["H","a","n","n","a","h"]
	Output: ["h","a","n","n","a","H"]

	Constraints:

	    1 <= s.length <= 105
	    s[i] is a printable ascii character.
*/

class Sol{

	static void reverseString(char str[]){
		
		int start = 0, end = str.length - 1;

		while(start < end){
			
			char temp = str[start];
			str[start] = str[end];
			str[end] = temp;

			start++;
			end--;
		}
	}
	
	public static void main(String[] s){
		
		char str[] = {'h','e','l','l','o'};

		reverseString(str);

		for(int i = 0; i < str.length; i++){
			
			System.out.println(str[i]);
		}
	}
}
