
/*
 	144. Binary Tree Preorder Traversal

	Given the root of a binary tree, return the preorder traversal of its nodes' values.

	Input: root = [1,null,2,3]
	Output: [1,2,3]
	
	Input: root = []
	Output: []

	Input: root = [1]
	Output: [1]
*/

class Node{
	
	int data;

	Node left, right;

	Node(int item){
		
		data = item;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	BinaryTree(){
		
		root = null;
	}

	void printPreOrder(Node node){
		
		if(node == null){
			
			return;
		}

		System.out.println(node.data + " ");

		printPreOrder(node.left);

		printPreOrder(node.right);
	}
	
	void printPreOrder(){
		
		printPreOrder(root);
	}

	public static void main(String[] s){
		
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);

		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		System.out.println("\nPreorder traversal of binary tree is ");

		tree.printPreOrder();
	}
}
