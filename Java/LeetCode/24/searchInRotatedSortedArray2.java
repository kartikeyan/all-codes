
/*	Prog : Search in Rotated Sorted Array II
 *
 *	There is an integer array "nums" sorted in non-decresing order (not necessarily with "distinct" values).
 *
 *	Before being passed to your function, "nums is rotated" at an unknown pivot index k (0 <= k < nums.length) such that resulting array is [nums[k], nums[k+1],......,nums[0],nums[1],......,nums[k-1]] (0-indexed).For example, [0,1,2,4,4,4,5,6,6,7] might be rotated at pivot index 5 and become [4,5,6,6,7,0,1,2,4,4].
 *
 *	Given the array nums "after" the rotation and an integer "target", return "true" if "target" is in nums, or "false" if it is not in nums.
 *
 *	You must decrease the overall operation steps as much as possible.
 *
 *	Input : nums = [2,5,6,0,0,1,2] , target = 0
 *	Ouput : true
 *
 *	Input : nums = [2,5,6,0,0,1,2] , target = 3
 *	Ouput : false
*/

class recDemo{
	
	static boolean search(int arr[], int size, int target){
		
		for(int i = 0; i < size; i++){
			
			if(arr[i] == target){
				
				return true;
			}
		}

		return false;
	}

	public static void main(String[] args){
		
		int arr[] = {2,5,6,0,0,1,2};

		int size = arr.length;

		int target = 0;

		boolean ret = search(arr, size, target);

		if(ret == true){

			System.out.print("found\n");
		}else{

			System.out.print("Not found\n");
		}
	}
}
