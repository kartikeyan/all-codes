
/*	Program 1 : Remove Duplicates from Sorted Array
 *
 *	Given an Integer array nums sorted in "non-decreasing order",remove tge duplicates "in-place"
 *	such that each unique element appears only "once".The "relative order" of the elements should
 *	be kept "same".
 *
 *	Input : nums = [1,1,2]
 *	Ouput : 2,  nums = [1,2,.....]
 *
 *	Input : nums = [0,0,1,1,1,2,2,3,3,4]
 *	Ouput : 5,  nums = [0,1,2,3,4,......]
*/

//	We are using Two Pointer Approach

class sol{

	static int removeDuplicates(int arr[], int size){
		
		int i = 0;

		for(int j = 1; j < size; j++){
			
			if(arr[i] != arr[j]){
				
				i++;
				arr[i] = arr[j];
			}
		}

		return i+1;
	}
	
	public static void main(String[] s){
		
		int arr[] = {1,1,2};

		int size = arr.length;

		int ret = removeDuplicates(arr, size);

		System.out.println(ret);
	}
}
