
/*
 	222. Count Complete Tree Nodes

	Given the root of a complete binary tree, return the number of the nodes in the tree.

	According to Wikipedia, every level, except possibly the last, is completely filled in a complete binary tree, and all nodes in the last level are as far left as possible. It can have between 1 and 2h nodes inclusive at the last level h.

	Design an algorithm that runs in less than O(n) time complexity.

	Input: root = [1,2,3,4,5,6]
	Output: 6

	Input: root = []
	Output: 0

	Input: root = [1]
	Output: 1

	Constraints:

	    	The number of nodes in the tree is in the range [0, 5 * 104].
		0 <= Node.val <= 5 * 104
		The tree is guaranteed to be complete.
*/

class Node{
	
	int data;
	Node left, right;

	Node(int val){

		data = val;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	static int countNodes (Node root){
		
		if(root == null){
			
			return 0;
		}

		int leftDepth = countNodes(root.left);

		int rightDepth = countNodes(root.right);

		return leftDepth + rightDepth + 1;
	}

	public static void main(String[] s){
	
		BinaryTree tree = new BinaryTree();

	        tree.root = new Node(3);
	
	        tree.root.left = new Node(9);
        	tree.root.right = new Node(20);

	        tree.root.right.left = new Node(15);
	        tree.root.right.right = new Node(7);

	        int ret = countNodes(tree.root);

		System.out.println(ret);
	}
}
