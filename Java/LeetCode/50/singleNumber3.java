/*
260. Single Number III

Given an integer array nums, in which exactly two elements appear only once and all the other elements appear exactly twice. Find the two elements that appear only once. You can return the answer in any order.

You must write an algorithm that runs in linear runtime complexity and uses only constant extra space.

Input: nums = [1,2,1,3,2,5]
Output: [3,5]
Explanation:  [5, 3] is also a valid answer.

Input: nums = [-1,0]
Output: [-1,0]

Input: nums = [0,1]
Output: [1,0]

Constraints:

    2 <= nums.length <= 3 * 104
    -231 <= nums[i] <= 231 - 1
    Each integer in nums will appear twice, only two integers will appear once
*/

class recDemo{

	static void singleNumber(int arr[], int size, int newArr[]){
		
		int i, j, k = 0;

		for(i = 0; i < size; i++){
			
			int count = 0;

			for(j = 0; j < size; j++){
				
				if(arr[j] == arr[i]){
					
					count++;
				}

				if(count > 1){
					
					break;
				}
			}

			if(count == 1){
				
				newArr[k++] = arr[i];
			}
		}
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,1,3,2,5};

		int size = arr.length;

		int newArr[] = new int[2];

		singleNumber(arr, size, newArr);

		for(int i = 0; i < 2; i++){
			
			System.out.println(newArr[i]);
		}
	}
}

