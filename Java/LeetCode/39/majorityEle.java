
/*	Prog : Given an array nums of size n, return the majority element
 *
 *	The majority element is the element that appears more than [n/2] times. you may assume that majority element always exists in the array
 *
 *	Input : arr[] = [3,2,3]
 *	Ouput : 3
 *
 *	Input : arr[] = [2,2,1,1,1,2,2]
 *	Ouput : 2
 *
 *	Constraints :
 *
 *		n == nums.length
 *
 *		1 <= n <= 5 * 10^4
 *
 *		-10^9 <= nums[i] <= 10^9
*/

class major{

	static int majorityEle(int arr[]){
		
		int size = arr.length;

		// Moore's Voting Algorithm
		
/*		int count1 = 0, ele = 0;

		for(int i = 0; i < size; i++){
			
			if(count1 == 0){
				
				count1 = 1;
				ele = arr[i];

			}else if(arr[i] == ele){
				
				count1++;
			}else{
				
				count1--;
			}
		}

		int count2 = 0;

		for(int i = 0; i < size; i++){
			
			if(arr[i] == ele){
				
				count2++;
			}
		}

		if(count2 > (size/2)){
			
			return ele;
		}

		return -1;
*/
		
		int count = 0, num = 0;
		for(int i = 0; i < size; i++){
			
			if(count == 0){
				
				count = 1;
				num = arr[i];
			}else{
				
				if(arr[i] == num){
					
					count++;
				}else{
					
					count--;
				}
			}
		}

		return num;
	}
	
	public static void main(String[] args){
		
		int arr[] = {2,2,1,1,1,2,2};

		int ret = majorityEle(arr);

		System.out.println(ret);
	}
}
