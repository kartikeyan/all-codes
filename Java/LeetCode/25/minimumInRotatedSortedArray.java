
/*	Prog : Find Minimum in Rotated Sorted Array
 *
 *	Suppose an array of length "n" sorted in ascending order is "rotated" between 1 and "n" times. For example, the array nums = [0,1,2,4,5,6,7] might become :
 *		[4,5,6,7,0,1,2] if it was rotated "4" times
 *
 *		[0,1,2,4,5,6,7] if it was rotated "7" times
 *
 *
 *	Notice that "rotating" an array [a[0], a[1],a[2],.....,a[n-2]]
 *
 *	Given the sorted rotated array "nums" of "unique" elements, return the minimum element of this array.
 *
 *	You must write an algorithm that runs in "0(log n) time".
 *
 *
 *	Input : nums = [3,4,5,1,2].
 *	Ouput : 1
 *	Expla : The original array was [1,2,3,4,5] rotated 3 times
 *
 *	Input : nums = [4,5,6,7,0,1,2].
 *	Output : 11
 *	Expla : The original array was [11,13,15,17] and it was rotated 4 times
 *
 *	Constraints :
 *
 *		1 <= n <= 5000
 *
 *		-5000 <= nums[i] <= 5000
*/

class sol{

	static 	void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - mid;

//	int arr1[ele1], arr2[ele2];
	int arr1[] = new int[ele1];
	int arr2[] = new int[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

static void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

	static int findMin(int arr[], int size){
		
		mergeSort(arr, 0, size-1);

		return arr[0];
	}

	public static void main(String[] args){
		
		int arr[] = {3,4,5,1,2};

		int size = arr.length;

		int ret = findMin(arr, size);

		System.out.println(ret);
	}
}
