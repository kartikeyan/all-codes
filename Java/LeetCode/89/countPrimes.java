
/*
 	204. Count Primes

	Given an integer n, return the number of prime numbers that are strictly less than n.

	Input: n = 10
	Output: 4
	Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.

	Input: n = 0
	Output: 0

	Input: n = 1
	Output: 0

	Constraints:

	    0 <= n <= 5 * 106
*/

/*
 	Sieve Of EratoSthenes Algorithm

	Approach :-

		This code implements a solution to count the number of prime numbers less than a given integer n using the Sieve of Eratosthenes algorithm.

		The algorithm works as follows:

    		1] Initialize an array isPrime of boolean values to store whether each number from 0 to n is prime or not. Initially, all numbers are assumed to be prime.
    		2] Mark 0 and 1 as not prime, as they are not prime numbers (According to test cases).
	    	3] Loop through the array from 2 to the square root of n.
	    	4] For each number, if it is marked as prime, then mark all of its multiples as not prime by updating - isPrime[j] = false where j = 2 * i, 3 * i, ..., n
	    	5] Finally, loop through the isPrime array and count the number of prime numbers.

	Time and Space Complexity :-

		The time complexity of this algorithm is O(n * log(log(n))), as the loop only needs to run through the numbers up to the square root of n and marking the multiples of each prime number. The space complexity is O(n), as a boolean array of size n + 1 is used to store the prime numbers.

*/

import java.util.Arrays;

class Sol{

	static int countPrimes(int n){
		
		if(n < 2){
		
			return 0;	
		}

		boolean isPrime[] = new boolean[n+1];

		Arrays.fill(isPrime, true);

		isPrime[0] = false;
		isPrime[1] = false;

		for(int i = 2; i < n; i++){
			
			if(isPrime[i] == true){

				for(int j = i*i; j < n; j+=i){
					
					isPrime[j] = false;
				}
			}
		}

		int count = 0;

		for(int i = 2; i < n; i++){
			
			if(isPrime[i] == true){
				
				count++;
			}
		}

		return count;
	}
	
	public static void main(String[] s){
		
		int n = 10;

		int ret = countPrimes(n);

		System.out.println(ret);
	}
}
