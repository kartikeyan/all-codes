
/* 	PRog : Count good Triplets
 *
 *  	Given an array if of integers arr, and three integers a,b, and c.You need to find the number of good triplets.
 *
 *  	A triplet (arr[i], arr[j], arr[k]) is good if the following conditions are true:
 *
 *  		- 0 <= i < j < k < arr.length
 *
 *  		- |arr[i] - arr[j] <= a|
 *
 *  		- |arr[j] - arr[k] <= b|
 *
 *  		- |arr[i] - arr[k] <= c|
 *
 *  	Where |x| denotes the absolute value of x.
 *
 *  	Return the number of good triplets
 *
 *  	Input : arr = [3,0,1,1,9,7], a = 7, b = 2, c = 3
 *  	Ouptu : 4
 *  	Expla :
 *  		There are 4 good triplets : [(3,0,1), (3,0,1), (3,1,1), (0,1,1)]
 *
 *  	Input : arr = [1,1,2,2,3], a = 0, b = 0, c = 1
 *  	Ouput : 0
 *  	Expla : No triplet satisfies all conditions
*/

class solution{

     static public int countGood(int arr[], int a, int b, int c){

		int count = 0, size = arr.length;

		for(int i = 0; i < size; i++){

			for(int j = i + 1; j < size; j++){

				if((Math.abs(arr[i] - arr[j])) <= a){

					for(int k = j + 1; k < size; k++){

						if((Math.abs(arr[i] - arr[k]) <= c) && (Math.abs(arr[j] - arr[k]) <= b)){

							count++;
						}
					}
				}
			}
		}

		return count;
	}

	public static void main(String[] args){

		int arr[] = {3,0,1,1,9,7};

		int a = 7, b = 2, c = 3;

		int ret = countGood(arr, a, b, c);

		System.out.println(ret);
	}
}

