
/*
 	1394. Find Lucky Integer in an Array

	Given an array of integers arr, a lucky integer is an integer that has a frequency in the array equal to its value.

	Return the largest lucky integer in the array. If there is no lucky integer return -1.

	Input: arr = [2,2,3,4]
	Output: 2
	Explanation: The only lucky number in the array is 2 because frequency[2] == 2.

	Input: arr = [1,2,2,3,3,3]
	Output: 3
	Explanation: 1, 2 and 3 are all lucky numbers, return the largest of them.

	Input: arr = [2,2,2,3,3]
	Output: -1
	Explanation: There are no lucky numbers in the array.

	Constraints:

	    1 <= arr.length <= 500
	    1 <= arr[i] <= 500
*/

class Sol{
	
	static int max(int x, int y){
		
		if(x > y){
			
			return x;
		}

		return y;
	}

	static int findLucky(int arr[], int size){
		
		// 1 - Brute
		
		int ret = -1;

		for(int i = 0; i < size; i++){
			
			int count = 0;

			for(int j = 0; j < size; j++){
				
				if(arr[j] == arr[i]){
					
					count++;
				}
			}

			if(arr[i] == count){
				
				ret = max(arr[i], ret);
			}
		}
			
		return ret;
	}
	
		// 2 - Better - Counting Sort
/*

		int counts[] = new int[501];

		for(int i = 0; i < 501; i++){
			
			counts[i] = 0;
		}

		for(int i = 0; i < size; i++){
			
			counts[arr[i]]++;
		}

		for(int i = 500; i > 0; i--){
			
			if(counts[i] == i){
				
				return i;
			}
		}

		return -1;		
	}
*/
	public static void main(String[] s){
		
		int arr[] = {2,2,3,4};

		int size = arr.length;

		int ret = findLucky(arr, size);

		System.out.println(ret);
	}
}
