
/*
 	1351. Count Negative Numbers in a Sorted Matrix

	Given a m x n matrix grid which is sorted in non-increasing order both row-wise and column-wise, return the number of negative numbers in grid.


	Example 1:

	Input: grid = [[4,3,2,-1],[3,2,1,-1],[1,1,-1,-2],[-1,-1,-2,-3]]
	Output: 8
	Explanation: There are 8 negatives number in the matrix.

	Example 2:

	Input: grid = [[3,2],[1,0]]
	Output: 0

	Constraints:

	    m == grid.length
	    n == grid[i].length
	    1 <= m, n <= 100
	    -100 <= grid[i][j] <= 100
*/

class Sol{

	static int countNegatives(int arr[][]){
		
		int c = 0;

		for(int i = 0; i < arr.length; i++){
			
			for(int j = 0; j < arr[0].length; j++){
				
				if(arr[i][j] < 0){
					
					c++;
				}
			}
		}

		return c;
	}
	
	public static void main(String[] args){
		
		int arr[][] = {{4,3,2,-1},{3,2,1,-1},{1,1,-1,-2},{-1,-1,-2,-3}};
		int ret = countNegatives(arr);

		System.out.println("Negative Numbers in Matrix are " + ret);
	}
}
