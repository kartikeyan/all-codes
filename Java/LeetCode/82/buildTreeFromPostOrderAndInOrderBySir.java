/*
	106. Construct Binary Tree from Inorder and Postorder Traversal

	Given two integer arrays inorder and postorder where inorder is the inorder traversal of a binary tree and postorder is the postorder traversal of the same tree, construct and return the binary tree.

	Input: inorder = [9,3,15,20,7], postorder = [9,15,7,20,3]
	Output: [3,9,20,null,null,15,7]	

	Input: inorder = [-1], postorder = [-1]
	Output: [-1]

	Constraints:

    		1 <= inorder.length <= 3000
    		postorder.length == inorder.length
    		-3000 <= inorder[i], postorder[i] <= 3000
    		inorder and postorder consist of unique values.
    		Each value of postorder also appears in inorder.
    		inorder is guaranteed to be the inorder traversal of the tree.
    		postorder is guaranteed to be the postorder traversal of the tree.

*/


class Node {
	int data;
	Node left, right;

	Node(int item)
	{
		data = item;
		left = right = null;
	}
}

class BinaryTree {
	Node root;
	static int preIndex = 0;

	Node buildTree(int in[], int post[], int inStart, int inEnd, int postStart, int postEnd)
	{
		if (inStart > inEnd)
			return null;

			
//		Node tNode = new Node(pre[preIndex++]);
		int rootData = post[postEnd];

		Node temp = new Node(rootData);

		
//		int inIndex = search(in, inStrt, inEnd, tNode.data);
		int idx;
		for(idx = inStart; idx <= inEnd; idx++){
			
			if(rootData == in[idx]){
				
				break;
			}
		}

		int lLength = idx - inStart;
	
		temp.left = buildTree(in, post, inStart, idx - 1, postStart ,postStart+lLength - 1);

		temp.right = buildTree(in, post, idx + 1, inEnd, postStart + lLength, postEnd - 1);

		return temp;
	}

	
	void printInorder(Node node)
	{
		if (node == null)
			return;

		
		printInorder(node.left);

		System.out.print(node.data + " ");

		printInorder(node.right);
	}

	
	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();

		int inOrder[] = {9,3,15,20,7};

		int postOrder[] = {9,15,7,20,3};

		int len = inOrder.length;

		Node root = tree.buildTree(inOrder, postOrder, 0, len-1, 0, len - 1);
		
		System.out.println("Inorder traversal of constructed tree is : ");
		tree.printInorder(root);

		System.out.print("\n");
	}
}



