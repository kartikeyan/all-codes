
/*
 	2348. Number of Zero-Filled Subarrays

	Given an integer array nums, return the number of subarrays filled with 0.

	A subarray is a contiguous non-empty sequence of elements within an array.

	Input: nums = [1,3,0,0,2,0,0,4]
	Output: 6
	Explanation:
		There are 4 occurrences of [0] as a subarray.
		There are 2 occurrences of [0,0] as a subarray.
		There is no occurrence of a subarray with a size more than 2 filled with 0. Therefore, we return 6.

	Input: nums = [0,0,0,2,0,0]
	Output: 9
	Explanation:
		There are 5 occurrences of [0] as a subarray.
		There are 3 occurrences of [0,0] as a subarray.
		There is 1 occurrence of [0,0,0] as a subarray.
		There is no occurrence of a subarray with a size more than 3 filled with 0. Therefore, we return 9.

	Input: nums = [2,10,2019]
	Output: 0
	Explanation: There is no subarray filled with 0. Therefore, we return 0.

	Constraints:

	    1 <= nums.length <= 105
	    -109 <= nums[i] <= 109
*/

class Sol{

	static int zeroFilledSub(int arr[], int size){
		
		int count = 0, subLength = 0;

		for(int i = 0; i < size; i++){
			
			if(arr[i] == 0){
				
				subLength++;

				count += subLength;
			}else{
				
				subLength = 0;
			}
		}

		return count;
	}
	
	public static void main(String[] s){
		
		int arr[] = {1,3,0,0,2,0,0,4};

		int size = arr.length;

		int ret = zeroFilledSub(arr, size);

		System.out.println(ret);
	}
}
