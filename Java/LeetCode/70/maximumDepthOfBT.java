
/*
 	104. Maximum Depth of Binary Tree

	Given the root of a binary tree, return its maximum depth.

	A binary tree's maximum depth is the number of nodes along the longest path from the root node down to the farthest leaf node.

	Input: root = [3,9,20,null,null,15,7]
	Output: 3

	Input: root = [1,null,2]
	Output: 2


	Constraints:

	    The number of nodes in the tree is in the range [0, 104].
	    -100 <= Node.val <= 100
*/

class Node{
	
	int data;

	Node left, right;

	Node(int item){
		
		data = item;
		left = right = null;
	}
}

class BinaryTree{
	
	Node root;

	int maxDepth(Node node){
		
		if(node == null){
			
			return 0;
		}else{
			
			int lDepth = maxDepth(node.left);
			int rDepth = maxDepth(node.right);

			if(lDepth > rDepth){
				
				return (lDepth + 1);
			}else{
				return (rDepth + 1);
			}
		}
	}

	public static void main(String[] s){
		
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);

		tree.root.left = new Node(2);
		tree.root.right = new Node(3);

		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		System.out.println("Height of tree is " + tree.maxDepth(tree.root));
	}
}
