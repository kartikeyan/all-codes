
/*	Prog : Check If N and it's Double Exist
 *
 *	Given an array of integers, check if there exists two indices "i and j" such that :
 *
 *	1] i != j
 *	2] 0 <= i, j < arr.length
 *	3] arr[i] == 2 * arr[j]
 *
 *	Input : arr = [10,2,5,3]
 *	Ouput : true
 *	Expla : for i = 0 and j = 2, arr[i] == 10 == 2 * 5 == 2 * arr[j]
 *
 *	Input : arr = [3,1,7,11]
 *	Ouput : false
 *	Expla : There is no i and j that satisfy the conditions.
*/

class sol{
	
	static boolean checkIfExist(int arr[], int size){
		
		for(int i = 0; i < size; i++){
			
			for(int j = i + 1; j < size; j++){
				
				if(i != j){
					
					if(arr[i] == 2 * arr[j] || arr[j] == 2* arr[i]){
						
						return true;
					}
				}
			}
		}

		return false;
	}

	public static void main(String[] args){
		
		int arr[] = {10,2,5,3};

		int size = arr.length;

		boolean ret = checkIfExist(arr, size);

		System.out.println(ret);
	}
}
