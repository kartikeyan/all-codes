
/*	Prog : Sort an array
 *
 *	Given an array of integers nums, sort the array in ascending order and return it.
 *
 *	You must solve the problem "without using any built-in" functions in "O(nlogn)" time complexity and with the smallest space complexity possible.
 *
 *	Input : arr = [5,2,3,1]
 *	Ouput : [1,2,3,5]
 *
 *	Constraints :
 *
 *		1 <= nums.length <= 5 * 10 ^ 4
 *
 *		-5 * 10 ^ 4 <= nums[i] <= 5 * 10 ^ 4
*/

class sort{
	
	static void merge(int arr[], int start, int mid, int end){
		
		int ele1 = mid - start + 1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i = 0; i < ele1; i++){
			
			arr1[i] = arr[start + i];
		}

		for(int j = 0; j < ele2; j++){
		
			arr2[j] = arr[mid + 1 + j];
		}

		int itr1= 0, itr2 = 0, itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){
			
			if(arr1[itr1] < arr2[itr2]){
				
				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
				
				arr[itr3] = arr2[itr2];
				itr2++;
			}

			itr3++;
		}

		while(itr1 < ele1){
			
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){
			
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}


	static void mergeSort(int arr[], int start, int end){
		
		if(start < end){
			
			int mid = (start + end) / 2;

			mergeSort(arr, start, mid);
			mergeSort(arr, mid+1, end);

			merge(arr, start, mid, end);
		}
	}

	static void sortArray(int arr[], int size){
		
		int start = 0, end = size - 1;

		mergeSort(arr, start, end);
	}

	public static void main(String[] s){

		int arr[] = {5,2,3,1};

		int size = arr.length;

		for(int i = 0; i < size; i++){
			
			System.out.println(arr[i]);
		}

		System.out.println();

		sortArray(arr, size);

		for(int i = 0; i < size; i++){
			
			System.out.println(arr[i]);
		}
	}
}
