
class sol{
	
	static boolean isSameAfterReversal(int num){
		
		int n = num, rev1 = 0, rev2 = 0;

		int rem = 0;

		while(n > 0){
			
			rem = n % 10;
			rev1 = rev1 * 10 + rem;
			n = n / 10;
		}

		rem = 0;

		while(rev1 > 0){
			
			rem = rev1 % 10;
			rev2 = rev2 * 10 + rem;
			rev1 = rev1 / 10;
		}

		if(num == rev2){
			
			return true;
		}
		return false;
	}

	public static void main(String[] args){
		
		int num = 1800;

		boolean ret = isSameAfterReversal(num);

		System.out.println(ret);
	}
}
