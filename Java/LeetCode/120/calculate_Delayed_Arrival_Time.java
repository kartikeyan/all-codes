
/*
 	2651. Calculate Delayed Arrival Time

	You are given a positive integer arrivalTime denoting the arrival time of a train in hours, and another positive integer delayedTime denoting the amount of delay in hours.

	Return the time when the train will arrive at the station.

	Note that the time in this problem is in 24-hours format.

	Input: arrivalTime = 15, delayedTime = 5
	Output: 20
	Explanation: Arrival time of the train was 15:00 hours. It is delayed by 5 hours. Now it will reach at 15+5 = 20 (20:00 hours).

	Input: arrivalTime = 13, delayedTime = 11
	Output: 0
	Explanation: Arrival time of the train was 13:00 hours. It is delayed by 11 hours. Now it will reach at 13+11=24 (Which is denoted by 00:00 in 24 hours format so return 0).

	Constraints:

	    1 <= arrivaltime < 24
	    1 <= delayedTime <= 24
*/

class Sol{

	static int findDelayedArrivalTime(int arrivalTime, int delayedTime){
		
		if(arrivalTime + delayedTime >= 24){
			
			return (arrivalTime + delayedTime) - 24;
		}

		return arrivalTime + delayedTime;
	}
	
	public static void main(String[] args){
		
		int arrivalTime = 15, delayedTime = 5;

		int ret = findDelayedArrivalTime(arrivalTime, delayedTime);

		System.out.println("Time Delayed is " + ret);
	}
}

//	Time Delayed is 20

