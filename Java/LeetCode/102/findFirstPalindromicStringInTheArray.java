
/*
 	2108. Find First Palindromic String in the Array

	Given an array of strings words, return the first palindromic string in the array. If there is no such string, return an empty string "".

	A string is palindromic if it reads the same forward and backward.

	Input: words = ["abc","car","ada","racecar","cool"]
	Output: "ada"
	Explanation: The first string that is palindromic is "ada".
	Note that "racecar" is also palindromic, but it is not the first.

	Input: words = ["notapalindrome","racecar"]
	Output: "racecar"
	Explanation: The first and only string that is palindromic is "racecar".

	Input: words = ["def","ghi"]
	Output: ""
	Explanation: There are no palindromic strings, so the empty string is returned.



	Constraints:

	    1 <= words.length <= 100
	    1 <= words[i].length <= 100
	    words[i] consists only of lowercase English letters.
*/

class Sol{

	static String firstPalindrome(String[] words){
		
		for(int i = 0; i < words.length; i++){
			
			StringBuilder str = new StringBuilder(words[i]);

			str.reverse();

			if(words[i].equals(str.toString())){
				
				return words[i];
			}
		}

		return " ";
	}
	
	public static void main(String[] args){
		
		String words[] = {"abc","car","ada","racecar","cool"};
		String ret = firstPalindrome(words);

		System.out.println("returned string is " + ret);
	}
}
