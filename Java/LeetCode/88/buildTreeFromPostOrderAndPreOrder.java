/*
	889. Construct Binary Tree from Preorder and Postorder Traversal

	Given two integer arrays, preorder and postorder where preorder is the preorder traversal of a binary tree of distinct values and postorder is the postorder traversal of the same tree, reconstruct and return the binary tree.

	If there exist multiple answers, you can return any of them.

	Input: preorder = [1,2,4,5,3,6,7], postorder = [4,5,2,6,7,3,1]
	Output: [1,2,3,4,5,6,7]

	Input: preorder = [1], postorder = [1]
	Output: [1]

	Constraints:

    		1 <= preorder.length <= 30
    		1 <= preorder[i] <= preorder.length
    		All the values of preorder are unique.
    		postorder.length == preorder.length
    		1 <= postorder[i] <= postorder.length
    		All the values of postorder are unique.
    		It is guaranteed that preorder and postorder are the preorder traversal and postorder traversal of the same binary tree.
*/


class Node {
	int data;
	Node left, right;

	Node(int item)
	{
		data = item;
		left = right = null;
	}
}

class BinaryTree {

	Node root;

	Node buildTree(int pre[], int post[], int preStart, int preEnd, int postStart, int postEnd)
	{
		if (preStart > preEnd)
			return null;

			
//		Node tNode = new Node(pre[preIndex++]);
		int rootData = pre[preStart];

		Node temp = new Node(rootData);

		if(preStart == preEnd){
			
			return temp;
		}

		
//		int inIndex = search(in, inStrt, inEnd, tNode.data);
		int idx;
		for(idx = postStart; idx <= postEnd; idx++){
			
			if(post[idx] == pre[preStart+1]){
				
				break;
			}
		}

		int lLength = idx - postStart + 1;
	
		temp.left = buildTree(pre, post, preStart+1, preStart + lLength, postStart , idx);

		temp.right = buildTree(pre, post, preStart + lLength + 1, preEnd, idx + 1, postEnd);

		return temp;
	}

	
	void printInorder(Node node)
	{
		if (node == null)
			return;

		
		printInorder(node.left);

		System.out.print(node.data + " ");

		printInorder(node.right);
	}

	
	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();

		int preOrder[] = {1,2,4,5,3,6,7};

		int postOrder[] = {4,5,2,6,7,3,1};

		int len = preOrder.length;

		Node root = tree.buildTree(preOrder, postOrder, 0, len-1, 0, len - 1);
		
		System.out.println("Inorder traversal of constructed tree is : ");
		tree.printInorder(root);

		System.out.print("\n");
	}
}



