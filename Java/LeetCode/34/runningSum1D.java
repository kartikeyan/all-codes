
/*	Prog : Running Sum of 1d Array
 *
 *	Given an array nums.We define a running sum of an arrays as runningSum[i] = sum(nums[0].......nums[i]).
 *
 *	Return the running sum of nums
 *
 *	Input : nums = [1,2,3,4]
 *	Ouput : [1,3,6,10]
 *	Expla : Running sum is obtained as follows : [1, 1+2, 1+2+3, 1+2+3+4].
 *
 *	Input : nums = [1,1,1,1,1]
 *	Ouput : [1,2,3,4,5]
 *	Expla : Running sum is obtained as follows : [1, 1+1, 1+1+1, 1+1+1+1]
 *
 *	Input : nums = [3,1,2,10,1]
 *	Ouput : [3,4,6,16,17]
 *
 *
 *	Constraints :
 *
 *		1 <= nums.length <= 1000
 *
 *		-10^6 <= nums[i] <= 10^6
*/

class recDemo{

	static void runningSum(int arr[], int size, int newArr[]){

		int sum = 0;

		newArr[0] = arr[0];

		for(int i = 1; i < size; i++){
			
			newArr[i] = newArr[i-1] + arr[i];
		}
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4};

		int size = arr.length;

		int newArr[] = new int[size];

		runningSum(arr, size, newArr);

		for(int i = 0; i < size; i++){
			
			System.out.println(newArr[i]);
		}
	}
}
