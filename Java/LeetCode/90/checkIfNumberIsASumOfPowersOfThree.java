
/*
 	1780. Check if Number is a Sum of Powers of Three

	Given an integer n, return true if it is possible to represent n as the sum of distinct powers of three. Otherwise, return false.

	An integer y is a power of three if there exists an integer x such that y == 3x.

	Input: n = 12
	Output: true
	Explanation: 12 = 3^1 + 3^2

	Input: n = 91
	Output: true
	Explanation: 91 = 3^0 + 3^2 + 3^4

	Input: n = 21
	Output: false

	Constraints:

	    1 <= n <= 107
*/

class Sol{

	// Approach 1
	
	/*
	static boolean checkPowersOfThree(int n){

		if(n == 1){
			
			return true;
		}

		if(n % 3 == 2){
			
			return false;
		}

		return checkPowersOfThree(n / 3);
	}
*/	
	// Approach - 2
/*	
	static  boolean checkPowersOfThree(int n){

		int rem;
			
		while(n > 0){
			
			rem = n % 3;

			if(rem == 2){
				
				return false;
			}

			n = n / 3;
		}

		return true;
	}
*/
	// Appraoch 3
	
	static  boolean checkPowersOfThree(int n){

                while(n > 0){

                        if(n % 3 > 1){

                                return false;
                        }

                        n = n / 3;
                }

                return true;
        }

	public static void main(String[] s){
		
		int n = 21;

		boolean ret = checkPowersOfThree(n);

		System.out.println(ret);
	}
}
