
/*
 	896. Monotonic Array

	An array is monotonic if it is either monotone increasing or monotone decreasing.

	An array nums is monotone increasing if for all i <= j, nums[i] <= nums[j]. An array nums is monotone decreasing if for all i <= j, nums[i] >= nums[j].

	Given an integer array nums, return true if the given array is monotonic, or false otherwise.

	Input: nums = [1,2,2,3]
	Output: true

	Input: nums = [6,5,4,4]
	Output: true

	Input: nums = [1,3,2]
	Output: false

	Constraints:

	    1 <= nums.length <= 105
	    -105 <= nums[i] <= 105
*/

class Sol{

	static boolean isMonotonic(int arr[]){
		
		boolean increasing = false, decreasing = false;

		for(int i = 1; i < arr.length; i++){
			
			if(arr[i] > arr[i-1]){
				
				increasing = true;
			}else if(arr[i] < arr[i-1]){
				
				decreasing = true;
			}
		}

		boolean result = (increasing && decreasing) ? false : true;

		return result;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,2,3};

		boolean ret = isMonotonic(arr);
		
		if(ret == true){
			System.out.println("YEs Monotonic");
		}else{
			System.out.println("No Monotonic");
		}
	}
}
