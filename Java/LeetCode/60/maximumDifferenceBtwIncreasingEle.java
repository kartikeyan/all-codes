
/*
 	2016. Maximum Difference Between Increasing Elements

	Given a 0-indexed integer array nums of size n, find the maximum difference between nums[i] and nums[j] (i.e., nums[j] - nums[i]), such that 0 <= i < j < n and nums[i] < nums[j].

	Return the maximum difference. If no such i and j exists, return -1.

	Input: nums = [7,1,5,4]
	Output: 4
	Explanation:
		The maximum difference occurs with i = 1 and j = 2, nums[j] - nums[i] = 5 - 1 = 4.
		Note that with i = 1 and j = 0, the difference nums[j] - nums[i] = 7 - 1 = 6, but i > j, so it is not valid.
	
	Input: nums = [9,4,3,2]
	Output: -1
	Explanation:
		There is no i and j such that i < j and nums[i] < nums[j].

	Input: nums = [1,5,2,10]
	Output: 9
	Explanation:
		The maximum difference occurs with i = 0 and j = 3, nums[j] - nums[i] = 10 - 1 = 9.

	Constraints:

	    n == nums.length
	    2 <= n <= 1000
	    1 <= nums[i] <= 109
*/

class sol{

	static int maxDiff(int arr[], int size){
		
		int max = -1;

		for(int i = 0; i < size; i++){
			
			for(int j = i+1; j < size; j++){
				
				if(i < j && arr[i] < arr[j]){
					
					if(arr[j] - arr[i] > max){
						max = arr[j] - arr[i];
					}
				}
			}
		}

		return max;
	}
	
	public static void main(String[] s){
		
		int arr[] = {7,1,5,4};

		int size = arr.length;

		int ret = maxDiff(arr, size);

		System.out.println(ret);
	}
}
