
/*	Prog : Count Operations to obtain Zero
 *
 *	In One operation, if num1 >= num2, u must subtract num2 from num1, 
 *	otherwise subtract num1 from num2.
 *
 *	Return the number of operations required to make either
 *	num1 = 0 or num2 = 0
 *
 *	Input : num1 = 2, num2 = 3
 *	Ouput : 3
*/

class assign{
	
	public static void main(String[] args){
		
		int num1 = 2, num2 = 3, count = 0;

		while(num1 != 0 && num2 != 0){
			
			if(num1 >= num2){
				
				num1 = num1 - num2;
			}else{
				
				num2 = num2 - num1;
			}

			count++;
		}

		System.out.println(count);
	}
}
