
/*	Prog : Number of steps to reduce a Number Zero
 *
 *	Given an integer num, return the number of steps to reduce a number to zero.
 *	In one step, if the current number is even, you have to divide by 2, otherwise, you have to subtract 1 from it.
 *
 *	Input : num = 14
 *	Ouput : 6
*/

class assign{
	
	public static void main(String[] args){
		
		int num = 14, count = 0;

		while(num != 0){
			
			if(num % 2 == 0){
				
				num = num / 2;
			}else{
				
				num = num - 1;
			}

			count++;
		}

		System.out.println(count);
	}
}
