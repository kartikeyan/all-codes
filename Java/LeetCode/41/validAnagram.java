
/*	Prog : Valid Anagram
 *
 *	Given two strings s and t, return true if t is an anagram of s, and false otherwise
 *
 *	An anagram is a word or phrase formed by rearranging the letters of a different word or phrase, typically using all the original letters exactly once.
 *
 *	Input : s = "anagram", t = "nagaram"
 *	Ouput : true
 *
 *	Input : s = "rat", t = "car"
 *	Ouput : false
 *
 *	Constraaints :
 *
 *		1 <= s.length, t.length <= 5 * 10 ^ 4
 *
 *		s and t consist of lowercase English letters
*/

import java.io.*;
import java.util.*;

class anagram{
	
	static int NO_OF_CHARS = 256;

	static boolean areAnagram(char str1[], char str2[]){
		
		int count1[] = new int[NO_OF_CHARS];
		Arrays.fill(count1, 0);

		int count2[] = new int[NO_OF_CHARS];
		Arrays.fill(count2, 0);

		int i;

		for(i = 0; i < str1.length && i < str2.length; i++){
			
			count1[str1[i]]++;
			count2[str2[i]]++;
		}

		if(str1.length != str2.length){
			
			return false;
		}

		for(i = 0; i < NO_OF_CHARS; i++){
			
			if(count1[i] != count2[i]){
				
				return false;
			}
		}

		return true;
	}

	public static void main(String[] args){
		
		char str1[] = ("gram").toCharArray();
		char str2[] = ("armg").toCharArray();

		if(areAnagram(str1, str2)){
			
			System.out.println("The two strings are " + "anagram of each other");
		}else{
			
			System.out.println("The two strings are not" + "anagram of each other");

		}
	}
}
