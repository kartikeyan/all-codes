
/*
 	94. Binary Tree Inorder Traversal

	Given the root of a binary tree, return the inorder traversal of its nodes' values.

	Input: root = [1,null,2,3]
	Output: [1,3,2]

	Input: root = []
	Output: []

	Input: root = [1]
	Output: [1]

	Constraints:

	    The number of nodes in the tree is in the range [0, 100].
	    100 <= Node.val <= 100
*/

class Node{
	
	int data;

	Node left, right;

	Node(int item){
		
		data = item;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	BinaryTree(){
		
		root = null;
	}

	void printInOrder(Node node){
		
		if(node == null){
			
			return;
		}

		printInOrder(node.left);

		printInOrder(node.right);

		System.out.println(node.data + " ");

	}
	
	void printInOrder(){
		
		printInOrder(root);
	}

	public static void main(String[] s){
		
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(1);
//		tree.root.left = new Node(2);
		tree.root.right = new Node(2);
	
		tree.root.right.left = new Node(3);
//		tree.root.left.left = new Node(4);
//		tree.root.left.right = new Node(5);

		System.out.println("\nInOrder traversal of binary tree is ");

		tree.printInOrder();
	}
}
