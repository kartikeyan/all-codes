
/*
 	2236. Root Equals Sum of Children

	You are given the root of a binary tree that consists of exactly 3 nodes: the root, its left child, and its right child.

	Return true if the value of the root is equal to the sum of the values of its two children, or false otherwise.

	Input: root = [10,4,6]
	Output: true
	Explanation: The values of the root, its left child, and its right child are 10, 4, and 6, respectively.
	10 is equal to 4 + 6, so we return true.

	Input: root = [5,3,1]
	Output: false
	Explanation: The values of the root, its left child, and its right child are 5, 3, and 1, respectively.
	5 is not equal to 3 + 1, so we return false.

	Constraints:

	    The tree consists only of the root, its left child, and its right child.
	    -100 <= Node.val <= 100

*/

class Node{
	
	int data;

	Node left, right;

	Node(int item){
		
		data = item;
		left = right = null;
	}
}

class BinaryTree{

	Node root;

	BinaryTree(){
		
		root = null;
	}

	static boolean checkTree(Node node){
		
		return root.data == root.left.data + root.right.val;

	}
	
	static boolean checkTree(){
		
		return checkTree(root);
	}

	public static void main(String[] s){
		
		BinaryTree tree = new BinaryTree();

		tree.root = new Node(10);
		tree.root.left = new Node(4);
		tree.root.right = new Node(6);
	
//		tree.root.right.left = new Node(3);
//		tree.root.left.left = new Node(4);
//		tree.root.left.right = new Node(5);

//		System.out.println("\nInOrder traversal of binary tree is ");

		boolean x = tree.checkTree();

		if(x == true){
			
			System.out.println("true");
		}else{
			System.out.println("false");
		}
	}
}
