/*	Prog : Keep Multiplying Found Values by Two
 *
 *	You are given an array of integers "num".You are also given an integer "original" which is the first number that needs to be searched for in "nums".
 *
 *	You then do following steps :
 *
 *		If "original" if found in "nums", "multiply" it by two (i.e set "original = 2 * original").
 *
 *		Otherwise,"stop" the process.
 *
 *		"Repeat" this process with the new number a long as you keepfinding the number.
 *
 *		Return the "final" value of "original".
 *
 *		Input : nums = [5,3,6,1,12], original = 3
 *		Ouput : 24
 *		Expla : 
 *			3 found in nums.3 is multiplied by 2 to obtain 6.
 *			6 found in nums.6 is multiplied by 2 to obtain 12.
 *			12 found in nums.12 is muliplied by 2 to obtain 24.
 *			24 is not found in nums.Thus, 24 is returned.
 *
 *		
 *		Input : nums = [2,7,9], original = 4
 *		ouput : 4 
 *		Expla : 
 *			4 is not found in nums.Thus, 4 is returned
*/

//	Approach - 1 = Sort the array and then search
//
//	Approach - 2 = Search directly in the array

class sol{

	static void merge(int arr[], int start, int mid, int end){
	
	int ele1 = mid - start + 1;
	int ele2 = end - start;

//	int arr1[ele1] , arr2[ele2];
	int arr1[] = new int[ele1];
	int arr2[] = new int[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + 1 + j];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2 < ele2){
	
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr3 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

static void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

	static int findValues(int arr[], int size, int original){
		
		mergeSort(arr, 0, size-1);

		for(int i = 0; i < size; i++){
			
			if(arr[i] == original){
				
				original *= 2;
			}
		}

		return original;
	}

	public static void main(String[] args){
		
		int arr[] = {5,3,6,1,12};

		int size = arr.length;

		int original = 3;

		int ret = findValues(arr, size, original);

		System.out.println(ret);
	}
}
