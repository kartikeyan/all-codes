

/*	Program 1 : Number of Good Pairs
 *
 *		    Given an array of integers nums, return the number of "good pairs".
 *
 *		    A pair (i,j) is called good if nums[i] == nums[j] and i < j.
 *
 *		    Input : nums = [1,2,3,1,1,3]  ,   Input : nums = [1,1,1,1]
 *		    Ouput : 4			  ,   Output : 6
*/

class sol{

	static int good(int arr[], int size){
		
		int count = 0;

		for(int i = 0; i < size; i++){
			
			for(int j = i + 1; j < size; j++){
				
				if(arr[i] == arr[j]){
					
					count++;
				}
			}
		}

		return count;
	}
	
	public static void main(String[] s){

		int arr[] = {1,2,3,1,1,3};

		int size = arr.length;

		int ret = good(arr, size);

		System.out.println(ret);
	}
}
