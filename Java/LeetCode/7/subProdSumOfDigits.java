
/*	Program 2 : Subtract the Product and Sum of Digits of an integer
 *
 *		    Given an integer number n, return the difference between
 *		    the product of its digits and the sum of its digits.
 *
 *		    Input : n = 234  ,  	 Input :  = 4421
 *		    Ouput : 15	     ,   	 Output : 21
 *	Explanation:				 Expalna: Prod = 4 * 4 * 2 * 1 = 32
 *		    Product of digits 			  Sum = 4 + 4 + 2 + 1 = 11
 *		    = 2 * 3 * 4 = 24			  Result = 32 - 11
 *		    Sum of digits				 = 21
 *		    = 2 + 3 + 4 = 9
 *
 *		    Result : 24 - 9 = 15
*/

class sol{

	static int sub(int n){
		
		int sum = 0, prod = 1, digit;

		while(n != 0){
			
			digit = n % 10;
			sum = sum + digit;
			prod = prod * digit;

			n = n / 10;
		}

		return prod - sum;
	}
	
	public static void main(String[] s){
		
		int n = 234;

		int ret = sub(n);

		System.out.println(ret);
	}
}
