
/*	Prog : Given an array of nums sorted in non-decreasing order, return an array of the squares of each number sorted in non-decreasing order.
 *
 *	Input : nums = [-4,-1,0,3,10]
 *	Ouput : [0,1,9,16,100]
 *	Expla : After squaring, the array becomes [16,1,0,9,100].
 *		After sorting, it becomes [0,1,9,16,100]
 *
 *
 *	Input : nums = [-7,-3,2,3,11]
 *	Ouput : [4,9,9,49,121]
 *
 *	Constraints :
 *
 *		1 <= nums.length <= 10^4
 *
 *		-10^4 <= nums[i] <= 10^4
 *
 *		nums is sorted in non-decreasing order.
*/

class recDemo{
	
	static void merge(int arr[], int start, int mid, int end){
		
		int ele1 = mid - start + 1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i = 0; i < ele1; i++){
			
			arr1[i] = arr[start + i];
		}

		for(int j = 0; j < ele2; j++){
			
			arr2[j] = arr[mid + 1 + j];
		}

		int itr1 = 0, itr2 = 0, itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){
			
			if(arr1[itr1] < arr2[itr2]){
				
				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
				
				arr[itr3] = arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1 < ele1){
			
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){
			
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	static void mergeSort(int arr[], int start, int end){
		
		if(start < end){
			
			int mid = (start + end) / 2;

			mergeSort(arr, start, mid);
			mergeSort(arr, mid+1, end);

			merge(arr, start, mid, end);
		}
	}

	static void sortedSquare(int arr[], int size, int newArr[]){
		
		for(int i = 0; i < size; i++){
			
			newArr[i] = arr[i] * arr[i];
		}

		mergeSort(newArr, 0, size-1);
	}


	public static void main(String[] args){
		
		int arr[] = {-4,-1,0,3,10};

		int size = arr.length;

		int newArr[] = new int[size];

		sortedSquare(arr, size, newArr);

		for(int i = 0; i < size; i++){
			
			System.out.println(newArr[i]);
		}
	}
}
