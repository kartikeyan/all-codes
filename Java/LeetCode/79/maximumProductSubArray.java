
/*
 	152. Maximum Product Subarray

	Given an integer array nums, find a
subarray that has the largest product, and return the product.

	The test cases are generated so that the answer will fit in a 32-bit integer.

	Input: nums = [2,3,-2,4]
	Output: 6
	Explanation: [2,3] has the largest product 6.

	Input: nums = [-2,0,-1]
	Output: 0
	Explanation: The result cannot be 2, because [-2,-1] is not a subarray.

	Constraints:

	    1 <= nums.length <= 2 * 104
	    -10 <= nums[i] <= 10
	    The product of any prefix or suffix of nums is guaranteed to fit in a 32-bit integer.

*/

class Sol{

	static int max(int x, int y){
		
		if(x > y){
			
			return x;
		}

		return y;
	}

	static int maxProduct(int arr[]){
		
		int pre = 1, suff = 1;

		int size = arr.length;

		int ans = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			if(pre == 0){
				
				pre = 1;
			}

			if(suff == 0){
				
				suff = 1;
			}

			pre *= arr[i];

			suff *= arr[size - i - 1];

			ans = max(ans, max(pre, suff));
		}

		return ans;
	}
	
	public static void main(String[] s){
		
		int arr[] = {2,3,-2,4};

		int ret = maxProduct(arr);

		System.out.println(ret);
	}
}
