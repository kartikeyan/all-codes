
/*
 	125. Valid Palindrome

	A phrase is a palindrome if, after converting all uppercase letters into lowercase letters and removing all non-alphanumeric characters, it reads the same forward and backward. Alphanumeric characters include letters and numbers.

	Given a string s, return true if it is a palindrome, or false otherwise.

	Input: s = "A man, a plan, a canal: Panama"
	Output: true
	Explanation: "amanaplanacanalpanama" is a palindrome.

	Input: s = "race a car"
	Output: false
	Explanation: "raceacar" is not a palindrome.

	Input: s = " "
	Output: true
	Explanation: s is an empty string "" after removing non-alphanumeric characters.
	Since an empty string reads the same forward and backward, it is a palindrome.

	Constraints:

	    1 <= s.length <= 2 * 105
	    s consists only of printable ASCII characters.
*/

class Sol{

	static boolean isPalindrome(String s){
		
		int i = 0;
		int j = s.length() - 1;

		while(i < j){
			
			Character start = s.charAt(i);

			Character end = s.charAt(j);

			if(!Character.isLetterOrDigit(start)){
				
				i++;
				continue;
			}

			if(!Character.isLetterOrDigit(end)){
				
				j--;
				continue;
			}

			if(Character.toLowerCase(start) != Character.toLowerCase(end)){
				return false;
			}

			i++;
			j--;
		}

		return true;
	}
	
	public static void main(String[] args){
		
		String s = "A man, a plan, a canal: Panama";

		boolean ret = isPalindrome(s);

		if(ret == true){
			
			System.out.println("Yes Palindrome");
		}else{
			System.out.println("Not Palindrome");
		}
	}
}
