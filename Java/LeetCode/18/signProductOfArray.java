
/*	Prog : Sign Of the Product of an Array
 *
 *	There is a function signFunc(x) that returns:
 *
 *		- 1 if x is positive.
 *		- -1 if x is negative.
 *		- 0 if x is equal to 0
 *
 *	You are given an integer array "nums".Let "product" be the product of all values in the array "nums".
 *
 *	Return "signFunc(product)"
 *
 *
 *	Input : nums = [-1,-2,-3,-4,3,2,1]
 *	Output : 1
 *	Expla : The product of all values in the array is 144, and
 *		signFunc(144) = 1
 *
 *
 *	Input : nums = [1,5,0,2,-3]
 *	Output : 0
 *	Expla : The product of all values in the array is 0, and
 *		signFunc(0) = 0
*/

class sol{
/*	
	static int arraySign(int arr[], int size){
	
		int prod = 1;

		for(int i = 0; i < size; i++){
			
			if(arr[i] == 0){
				
				return 0;
			}

			if(arr[i] < 0){
				
				prod *= -1;
			}
		}

		return prod;	
	}
*/
	static int arraySign(int arr[], int size){
	
		int prod = 1;

		for(int i = 0; i < size; i++){
			
			prod = prod * arr[i];
		}

		if(prod < 0){
			return 0;
		}

		else if(prod > 0){
				
			return 1;
		}else{
			return 0;
		}
	}

	public static void main(String[] args){
			
		int arr[] = {1,5,0,2,-3};

		int size = arr.length;

		int ret = arraySign(arr, size);

		System.out.print("Sign is " + ret);
	}
}
