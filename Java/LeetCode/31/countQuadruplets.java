
/*	Prog : Count Special Quadruplets
 *
 *	Given a 0-indexed integer array nums, return the number of distinct quadruplets (a,b,c,d) such that
 *
 *	nums[a] + nums[b] + nums[c] == nums[d], and
 *
 *	a < b < c < d
*/

class sol{

	static int countQuad(int arr[], int size){
		
		int count = 0;
		
		for(int i = 0; i < size; i++){
			
			for(int j = i + 1; j < size; j++){
				
				for(int k = j + 1; k < size; k++){
					
					for(int m = k + 1; m < size; m++){
						
						if(arr[i] + arr[j] + arr[k] == arr[m]){
							
							count++;
						}
					}
				}
			}
		}

		return count;
	}
	
	public static void main(String[] s){
		
		int arr[] = {1,2,3,6};

		int size = arr.length;

		int ret = countQuad(arr, size);

		System.out.println(ret);
	}
}
