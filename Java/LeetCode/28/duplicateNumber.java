
/*	Prog : First Duplicate Number
 *
 *	Given an array of integers "nums" containing "n+1" integrs where each integer is in the range [1,n] inclusive.
 *
 *	There is only "one repeated number" in nums, return this repeated number.
 *
 *	You must solve the problem without modifying the array "nums" and uses only constant extra space.
 *
 *	Input : nums = [1,3,4,2,2]
 *	Ouput : 2
 *
 *	Input : nums = [3,1,3,4,2]
 *	Ouput : 3
*/

class sol{
	
	static void merge(int arr[], int start, int mid, int end){
		
		int ele1 = mid - start + 1;
		int ele2 = end - mid;

		int arr1[] = new int[ele1];
		int arr2[] = new int[ele2];

		for(int i = 0; i < ele1; i++){
			
			arr1[i] = arr[start + i];
		}

		for(int j = 0; j < ele2; j++){
			
			arr2[j] = arr[mid + 1 + j];
		}

		int itr1 = 0, itr2 = 0, itr3 = start;

		while(itr1 < ele1 && itr2 < ele2){
			
			if(arr1[itr1] < arr2[itr2]){
				
				arr[itr3] = arr1[itr1];
				itr1++;
			}else{
				
				arr[itr3] = arr2[itr2];
				itr2++;
			}
			itr3++;
		}

		while(itr1 < ele1){
			
			arr[itr3] = arr1[itr1];
			itr1++;
			itr3++;
		}

		while(itr2 < ele2){
			
			arr[itr3] = arr2[itr2];
			itr2++;
			itr3++;
		}
	}

	static void mergeSort(int arr[], int start, int end){
		
		if(start < end){
			
			int mid = (start + end) / 2;

			mergeSort(arr, start, mid);
			mergeSort(arr, mid+1, end);

			merge(arr, start, mid, end);
		}
	}


	static int findDuplicates(int arr[], int size){
		
		mergeSort(arr, 0, size-1);

		for(int i = 0; i < size; i++){
			
			if(arr[i] == arr[i+1]){
				
				return arr[i];
			}
		}

		return -1;
	}
	
	public static void main(String[] args){
		
		int arr[] = {1,2,3,4,2};

		int size = arr.length;

		int ret = findDuplicates(arr, size);

		System.out.println(ret);
	}
}
