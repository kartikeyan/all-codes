
/*	Program 1 : Number of Unequal Triplets in Array
 *
 *	You are given a 0-indexed array of positive integers "nums".
 *
 *	Find the number of triplets (i,j,k) that meet the follo. cond.
 *	1] 0 <= i < j < k < nums.length
 *	2] nums[i] , nums[j], and nums[k] are "pairwise distinct"
 *	3] In other words, nums[i] != nums[j] != nums[k], and
 *	   nums[i] != nums[k]
 *
 *	Return the number of triplets that meet the condition
 *
 *	Input : nums[4,4,2,4,3]
 *	Ouput : 3
 *	Expla : the following triplets meet the conditions:
 *		- (0,2,4) because 4 != 2 != 3
 *		- (1,2,4) because 4 != 2 != 3
 *		- (2,3,4) because 2 != 4 != 3
 *	Since there are 3 triplets, we return 3
*/

class sol{

	static int unequalTriplets(int arr[], int size){
		
		int count = 0;

		for(int i = 0; i < size; i++){
	
			for(int j = i + 1; j < size; j++){

				for(int k = j + 1; k < size; k++){

					if(i < j && j < k && i < k){

						if(arr[i] != arr[j] && arr[j] != arr[k] && arr[i] != arr[k]){

							count++;
						}
					}
				}
			}
		}

		return count;
	}
	
	public static void main(String[] args){
		
		int arr[] = {4,4,2,4,3};

		int size = arr.length;

		int ret = unequalTriplets(arr, size);

		System.out.println(ret);
	}
}
