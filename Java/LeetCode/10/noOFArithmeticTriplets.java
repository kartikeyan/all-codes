
/*	Prog 2 : Number of Arithmetic Triplets
 *
 *	You are given a "0-indexed, strictly increasing" integer array "nums" and 
 *	a positive integer "diff". A triplet (i,j,k) is an "arithmetic triplet" if
 *	the following conditions met :
 *		1] i < j < k
 *		2] nums[j] - nums[i] == diff
 *		3] nums[k] - nums[j] == diff.
 *
 *	Return the no. of unique "arithmetic triplets"
 *
 *	Input : nums[] = {0,1,4,6,7,10}, diff = 3
 *	Ouput : 3
 *	Expla : 
 *		(1,2,4) is an arithmetic triplet because
 *		both 7-4 == 3 and 4-1 == 3
 *
 *		(2,4,5) is an arithemtic triplet because
 *		both 10-7 == 3 and 7-4 == 3
*/

class sol{

	static int arithmeticTriplets(int arr[], int size, int diff){
		
		int count = 0;

		for(int i = 0; i < size; i++){
	
			for(int j = i + 1; j < size; j++){

				for(int k = j + 1; k < size; k++){

					if(i < j && j < k && i < k){

						if(arr[j] - arr[i] == diff && arr[k] - arr[j] == diff){

							count++;
						}
					}
				}
			}
		}

		return count;
	}
	
	public static void main(String[] args){
		
		int arr[] = {4,4,2,4,3};

		int size = arr.length;

		int diff = 3;

		int ret = arithmeticTriplets(arr, size, diff);

		System.out.println(ret);
	}
}
