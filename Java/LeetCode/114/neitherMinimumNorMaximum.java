
/*
 	2733. Neither Minimum nor Maximum

	Given an integer array nums containing distinct positive integers, find and return any number from the array that is neither the minimum nor the maximum value in the array, or -1 if there is no such number.

	Return the selected integer.

	Input: nums = [3,2,1,4]
	Output: 2
	Explanation: In this example, the minimum value is 1 and the maximum value is 4. Therefore, either 2 or 3 can be valid answers.

	Input: nums = [1,2]
	Output: -1
	Explanation: Since there is no number in nums that is neither the maximum nor the minimum, we cannot select a number that satisfies the given condition. Therefore, there is no answer.
	Input: nums = [2,1,3]
	Output: 2
	Explanation: Since 2 is neither the maximum nor the minimum value in nums, it is the only valid answer.

	Constraints:

	    1 <= nums.length <= 100
	    1 <= nums[i] <= 100
	    All values in nums are distinct
*/

class Sol{

	static int findNonMinOrMax(int[] arr){
		
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;

		for(int i = 0; i < arr.length; i++){
			
			if(min > arr[i]){
				
				min = arr[i];
			}

			if(max < arr[i]){
				
				max = arr[i];
			}
		}

		for(int i = 0; i < arr.length; i++){
			
			if(arr[i] != min && arr[i] != max){
				
				return arr[i];
			}
		}

		return -1;
	}
	
	public static void main(String[] s){
		
		int arr[] = {3,2,1,4};

		int ret = findNonMinOrMax(arr);

		System.out.println("ret is : " + ret);
	}
}
