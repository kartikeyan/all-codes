
/*	Program 2 : Three Divisors
 *
 *	Given an integer "n", return "true" if n has "exactly three positive integers".
 *	Otherwise return "false".
 *
 *	An integer m is a "divisor" of n if there exists an integer "k" such that
 *	n = k * m
 *
 *	Input : n = 2
 *	Ouput : false
 *	Explnation : 2 has only two divisors 1 and 2
 *
 * 	Input : n = 4
 * 	Ouput : true
 * 	expla : 4 has three divisors : 1,2 and 4
*/

class sol{
	
	static boolean isThree(int n){
		
		int count = 0;

		for(int i = 1; i <= n; i++){

			if(n % i == 0){
				
				count++;
			}
		}

		if(count == 3){
			
			return true;
		}

		return false;
	}

	public static void main(String[] args){

		int n = 2;

		boolean ret = isThree(n);

		System.out.println(ret);
	}
}
