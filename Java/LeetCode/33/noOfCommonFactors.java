
/*	Prog : Number of Common Factors
 *
 *	Given two positive integers a and b, return the number of "common" factors of a and b
 *
 *	An integer x is a "common factor" of a and b if x divides both a and b
 *
 *	Input : a = 12, b = 6
 *	Ouput : 4
 *	Expla : The common factors of 12 and 6 are 1, 2, 3, 6
 *
 *	Input : a = 25, b = 30
 *	Output : 2
 *	Expla : The common factors of 25 and 30 are 1, 5.
*/

class recDemo{

	static int commonFactors(int a, int b){
		
		int max;

		if(a > b){
			
			max = a;
		}else{
			max = b;
		}

		int count = 0;

		for(int i = 1; i <= max; i++){
			
			if(a % i == 0 && b % i == 0){
				
				count++;
			}
		}

		return count;
	}
	
	public static void main(String[] args){

		int a = 12, b = 6;

		int ret = commonFactors(a, b);

		System.out.println(ret);
	}
}
