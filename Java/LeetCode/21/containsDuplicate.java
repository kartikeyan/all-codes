
/*	Prog : Contains Duplicate
 *
 *	Given an integer array "nums", return true if any value appears "at least twice" in the array, and return "false" if every element is distinct
 *
 *	Input : nums = [1,2,3,1]	Input : nums = [1,1,1,3,3,4,3,2,4,2]
 *	Output : true			Output : true
 *
 *	Input : nums = [1,2,3,4]
 *	Ouput : false
*/

class sol{
	
static void merge(int arr[], int start, int mid, int end){

	int ele1 = mid - start + 1;
	int ele2 = end - mid;

//	int arr1[ele1], arr2[ele2];
	int arr1[] = new int[ele1];
	int arr2[] = new int[ele2];

	for(int i = 0; i < ele1; i++){
		
		arr1[i] = arr[start + i];
	}

	for(int j = 0; j < ele2; j++){
		
		arr2[j] = arr[mid + j + 1];
	}

	int itr1 = 0, itr2 = 0, itr3 = start;

	while(itr1 < ele1 && itr2  < ele2){
		
		if(arr1[itr1] < arr2[itr2]){
			
			arr[itr3] = arr1[itr1];
			itr1++;
		}else{
			
			arr[itr3] = arr2[itr2];
			itr2++;
		}

		itr3++;
	}

	while(itr1 < ele1){
		
		arr[itr3] = arr1[itr1];
		itr1++;
		itr3++;
	}

	while(itr2 < ele2){
		
		arr[itr3] = arr2[itr2];
		itr2++;
		itr3++;
	}
}

static void mergeSort(int arr[], int start, int end){
	
	if(start < end){
		
		int mid = (start + end) / 2;

		mergeSort(arr, start, mid);
		mergeSort(arr, mid + 1, end);

		merge(arr, start, mid, end);
	}
}

	static 	boolean containsDuplicates(int arr[], int size){
		
		mergeSort(arr, 0, size-1);

		for(int i = 0; i < size - 1; i++){
			
			if(arr[i] == arr[i+1]){
				
				return true;
			}
		}

		return false;
	}

	public static void main(String[] args){
		
		int arr[] = {1,2,3,1};

		int size = arr.length;

		boolean ret = containsDuplicates(arr, size);

		if(ret == true){
	
			System.out.print("Array Contains Duplicates\n");	
		}else{
		
			System.out.print("Array Does not Contains Duplicates\n");	
		}
	}
}
