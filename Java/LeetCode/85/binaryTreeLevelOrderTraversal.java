
/*
 	102. Binary Tree Level Order Traversal

	Given the root of a binary tree, return the level order traversal of its nodes' values. (i.e., from left to right, level by level).

	Input: root = [3,9,20,null,null,15,7]
	Output: [[3],[9,20],[15,7]]

	Input: root = [1]
	Output: [[1]]

	Input: root = []
	Output: []
	
	Constraints:

	    The number of nodes in the tree is in the range [0, 2000].
	    -1000 <= Node.val <= 1000
*/

// Using Recurison

class Node {
	int data;
	Node left, right;
	public Node(int item)
	{
		data = item;
		left = right = null;
	}
}

class BinaryTree {
	
	Node root;

	public BinaryTree() { root = null; }

	/* function to print level order traversal of tree*/
	void printLevelOrder()
	{
		int h = height(root);
		int i;
		for (i = 1; i <= h; i++)
			printCurrentLevel(root, i);
	}

	/* Compute the "height" of a tree -- the number of
	nodes along the longest path from the root node
	down to the farthest leaf node.*/
	int height(Node root)
	{
		if (root == null)
			return 0;
		else {
			/* compute height of each subtree */
			int lheight = height(root.left);
			int rheight = height(root.right);

			/* use the larger one */
			if (lheight > rheight)
				return (lheight + 1);
			else
				return (rheight + 1);
		}
	}

	/* Print nodes at the current level */
	void printCurrentLevel(Node root, int level)
	{
		if (root == null)
			return;
		if (level == 1)
			System.out.print(root.data + " ");
		else if (level > 1) {
			printCurrentLevel(root.left, level - 1);
			printCurrentLevel(root.right, level - 1);
		}
	}

	public static void main(String args[])
	{
		BinaryTree tree = new BinaryTree();
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);

		System.out.println("Level order traversal of"
						+ "binary tree is ");
		tree.printLevelOrder();

		print("\n");
	}
}



//	Using Queue

/*
import java.util.LinkedList;
import java.util.Queue;

class Node {
	int data;
	Node left, right;

	public Node(int item)
	{
		data = item;
		left = null;
		right = null;
	}
}

class Main {

	Node root;

	void printLevelOrder()
	{
		Queue<Node> queue = new LinkedList<Node>();
		queue.add(root);

		while (!queue.isEmpty()) {
			
			// poll removes the present head
			Node tempNode = queue.poll();

			System.out.print(tempNode.data + " ");

			if (tempNode.left != null) {
				queue.add(tempNode.left);
			}

			if (tempNode.right != null) {
				queue.add(tempNode.right);
			}
		}
	}

	public static void main(String args[])
	{
		Main tree_level = new Main();

		tree_level.root = new Node(1);

		tree_level.root.left = new Node(2);
		tree_level.root.right = new Node(3);

		tree_level.root.left.left = new Node(4);
		tree_level.root.left.right = new Node(5);

		tree_level.printLevelOrder();

		System.out.println();
	}
}

*/

/*
 	Time Complexity: O(N) where n is the number of nodes in the binary tree.
	Auxiliary Space: O(N) where n is the number of nodes in the binary tree.
*/
